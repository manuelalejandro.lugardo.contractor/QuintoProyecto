<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.bbva.jee.arq.spring.core.contexto.ArqSpringContext"%>
<%@ page import="com.bbva.msca.front.util.Utilidad"%>
<%
    Utilidad utilidad = new Utilidad();
    String URL_FRONT = utilidad.getUrlBase();
    String URL_BACK = utilidad.getUrlServicios();
%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script src="../js/vendor/jquery-1.11.2.js"></script>
<script src="../js/perfilado.js"></script>
<script src="../js/main.js"></script>
<link rel="stylesheet" href="../css/main.css">
<script type="text/javascript">
baseUrl = '<%=URL_BACK%>/services/rest';
baseUrlFront = '<%=URL_FRONT%>';
cdUsuarioTemporal = sessionStorage.getItem("cdUsuarioTemporal");
cdPerfil = sessionStorage.getItem("cdPerfil");
nbPerfil = sessionStorage.getItem("nbPerfil");

function redirectLogin(){
	document.location.href='<%=URL_FRONT%>/site/logout.jsp';
	return false;
}

</script>
</head>
<body>
	<form onsubmit="return redirectLogin()">
	<table border="0" width="100%" style="vertical-align:middle;">
		<tr>
			<td style="border-top:0px;">
				<div class="error msg" id="mensajeErrorModal" style="display:none;">
				    <label id="mensajeError" style="font-size:20px;color:#C8175E;"></label>
				</div>
			</td>
		</tr>
		<tr>
			<td style="border-top:0px;">
				
			</td>
		</tr>
	</table>
	</form>
<script>
jQuery(document).ready(function () {
	sessionStorage.clear();
	$("#btnAceptar").focus();
	sessionStorage.setItem("cdPerfil",cdPerfil);
	sessionStorage.setItem("nbPerfil",nbPerfil);
	sessionStorage.setItem("cdUsuarioTemporal", cdUsuarioTemporal);
	sessionStorage.setItem("baseUrl", baseUrl);
    sessionStorage.setItem("baseUrlFront", baseUrlFront);
    
    var nbCveRed = "";
    
    if (sessionStorage.getItem("cdUsuarioTemporal") == null || sessionStorage.getItem("cdUsuarioTemporal") == '' || sessionStorage.getItem("cdUsuarioTemporal") == 'null') {
        console.log("TEST ARQUITECTURA");
        nbCveRed = "<%=ArqSpringContext.getUsuario().trim().toUpperCase().replaceAll("^MX.", "")%>";
    } else {
        console.log("TEST LOCAL");
        nbCveRed = sessionStorage.getItem("cdUsuarioTemporal");
    }
    
    obtenerUsuario(nbCveRed).done(function(data) {
    	if(data.cdMensaje == "0"){
    		var elementos = $();
			elementos = elementos.add('<input type="submit" value="Aceptar" id="btnAceptar" style="position: relative; left: 47%; top: 75px;"/>');
			$('#mensajeErrorModal').append(elementos);
		}
		if(cargarMensaje(data,false,0,false)){
			
			if(sessionStorage.cdPerfil == null || sessionStorage.cdPerfil == '' || sessionStorage.cdPerfil == 'null'){
				sessionStorage.cdPerfil = data.response.cdPerfil1;
				sessionStorage.nbPerfil = data.response.nbPerfil1;
			}
			
			sessionStorage.setItem("cdUsuario", data.response.cdUsuario);
	        sessionStorage.setItem("cdPerfil1", data.response.cdPerfil1);
	        sessionStorage.setItem("nbPerfil1", data.response.nbPerfil1);
	        sessionStorage.setItem("cdPerfil2", data.response.cdPerfil2);
	        sessionStorage.setItem("nbPerfil2", data.response.nbPerfil2);
	        sessionStorage.setItem("nbMaterno", data.response.nbMaterno);
	        sessionStorage.setItem("nbPaterno", data.response.nbPaterno);
	        sessionStorage.setItem("nbCveRed", data.response.nbCveRed);
	        sessionStorage.setItem("nbPersona", data.response.nbPersona);
	        sessionStorage.setItem("cdSupervisor", data.response.cdSupervisor);
	        sessionStorage.setItem("cdGerencia", data.response.cdGerencia);
	        
	        var modulos = "";
	        var contador = 1;
	        
	        if(sessionStorage.cdPerfil == data.response.cdPerfil1){
	        	 $.each(data.response.lstModulos1, function(id, valor){
	 	            if (contador != 1){
	 	                modulos = modulos + " , ";
	 	            }
	 	            modulos = modulos + valor.cdModulo;
	 	            contador = contador + 1;
	 	        });
	        }else if(sessionStorage.cdPerfil == data.response.cdPerfil2){
	        	 $.each(data.response.lstModulos2, function(id, valor){
	 	            if (contador != 1){
	 	                modulos = modulos + " , ";
	 	            }
	 	            modulos = modulos + valor.cdModulo;
	 	            contador = contador + 1;
	 	        });
	        }
	       
	        sessionStorage.setItem("cdModulos", modulos);
	        console.log(sessionStorage.getItem("cdModulos"));
	        
	        //REDIRECT
	        document.location.href='<%=URL_FRONT%>/site/admon.html';
		}
		
 	});
});
</script>
</body>
</html>

