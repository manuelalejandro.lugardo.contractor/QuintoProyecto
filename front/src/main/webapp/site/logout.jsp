<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import="com.bbva.jee.arq.spring.core.contexto.ArqSpringContext" %>
<%@ page import="com.bbva.msca.front.util.Utilidad"%>
<%
    Utilidad utilidad = new Utilidad();
    String URL_LOGOUT = utilidad.getUrlLogout();
%>
<%
	ArqSpringContext.eliminarSesion();
%>

<script>
	sessionStorage.clear();
	document.location.href='<%=URL_LOGOUT%>';
</script>