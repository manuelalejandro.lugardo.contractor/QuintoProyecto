function obtenerUsuario(nbCveRed){
	 return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/PerfiladoService/consultarUsuario/" + nbCveRed,
		type : 'GET',
		contentType: 'application/json',
		dataType: 'json',
		async: false
	});
}
