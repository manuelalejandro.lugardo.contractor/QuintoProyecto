/** VARIABLES DE CONTROL **/
var modificar = false;
var eliminar = false;
var alta = true;
var acepto = false;
var CD_ST_SIS_A = '11';
var CD_ST_SIS_I = '12';
var objTmp = new Object();
var PERFIL_COORDINA = 3;
var PERFIL_SUPERVISOR = 2;
var PERFILADO = (sessionStorage.cdPerfil == PERFIL_COORDINA) || (sessionStorage.cdPerfil == PERFIL_SUPERVISOR);

/** Funciones de Datos **/
function borrarDatos() {
	modificar = false;
	acepto = false;
	eliminar = false;
	alta = true;
	objTmp = new Object();
	$('#cdTipologia').val('');
	$('#nbTipologia').val('');
	$('#cdGerencia').val('-1');
	$('#nuUmbralMC').val('');
	$('#cdUsrAlta').val('');
	$('#fhAlta').val('');
	$('#cdStSistema').val('');
	habilCtrl('#cdTipologia');
	resetVal();
}

function fncModificar(valor){
	acepto = valor;
	validacion();
	if(!valor){
		resetVal();
	}
}

function fncEliminar(valor){
	acepto = valor;
	eliminar = valor;
	validacion();
	borrarDatos();
}

function resetVal(){
	$('#MSCAVE01012_2_FORM input').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
	$('#MSCAVE01012_2_FORM select').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
}

/** Funciones de eventos **/
var actionEvents = {
	'click .delete' : function(e, value, row, index) {
		eliminar = true;
		modificar = false;
		alta = false;
		llenarTipologia(row);
		objTmp.cdStSistema = CD_ST_SIS_I;
		$('#dialogEliminarTipologia').modal('show');
	},
	'click .actualizar' : function(e, value, row, index) {
		modificar = true;
		eliminar = false;
		alta = false;
		llenarTipologia(row);
		deshCtrl('#cdTipologia');
	}
};

/** Funciones de Tablas **/
function llenadoTablaTipologia(data) {
	$('#tablaTipologia').bootstrapTable(
		{
			pagination : true,
			pageSize : 5,
			data : data,
			clickToSelect : false,
			singleSelect : false,
			maintainSelected : true,
			sortable : true,
			checkboxHeader : false,
			formatShowingRows : function(pageFrom, pageTo, totalRows) {
				return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
			},
			formatRecordsPerPage : function(pageNumber) {
				return pageNumber + ' registros por p\u00e1gina';
			},
			formatLoadingMessage : function() {
				return 'Cargando, espere por favor...';
			},
			formatSearch : function() {
				return 'Buscar';
			},
			formatNoMatches : function() {
				return 'No se encontr\u00f3 informaci\u00f3n';
			}
		}
	);
}

/** Funciones de Formato **/
function convFec(fecha) {
	var fec = fecha.split("/");
	return new Date(fec[2], fec[1] - 1, fec[0]);
}
/*
function formatterDate(date) {
	var d = new Date(date), month = '' + (d.getUTCMonth() + 1), day = ''
			+ d.getUTCDate(), year = d.getUTCFullYear();

	if (month.length < 2)
		month = '0' + month;
	if (day.length < 2)
		day = '0' + day;
	return [ day, month, year ].join('/');
}
*/

function formatterTipologia(value, row, index) {
	if(PERFILADO){
		return [
				'<a class="delete" href="javascript:void(0)" title="Eliminar Tipolog\u00eda">',
				'<i class="tabla_registro_eliminar">.</i>',
				'</a>',
				'<a class="actualizar" href="javascript:void(0)" title="Actualizar">',
				'<i class="tabla_registro_editar">.</i>', '</a>'

		].join('');
	}else{
		return ['',''].join('');
	}
}

function deshCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).attr('disabled','disabled');
	$(selector).addClass('ignore');
}

function habilCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).removeAttr('disabled');
	$(selector).removeClass('ignore');
}

/** Funciones de Formulario **/
function llenarObjeto(data, apartado) {
	$.each(data, function(key, value) {
		if(key=='cdStSistema'){
			objTmp.cdStSistema = value;
		}else if(key=='cdUsrAlta'){
			objTmp.cdUsrAlta = value;
		}else if(key=='fhAlta'){
			objTmp.fhAlta = value;
		}
	});
}

function llenarTipologia(data) {
	llenarFormulario(data, 'tipologia');
	llenarObjeto(data, 'tipologia');
}

function validaPerfil(){
	if (PERFILADO) {
		$("#abcTipologia").show();
	} else {
		$("#abcTipologia").hide();
	}
}


/** *** OBJETOS **** */
function crearObjetoTipologia() {
	tipologia = new Object();
	tipologia.cdTipologia = $("#cdTipologia").val().toUpperCase();
	tipologia.nbTipologia = $("#nbTipologia").val().toUpperCase();
	tipologia.nuUmbralMC = $("#nuUmbralMC").val();
	tipologia.cdGerencia = $("select#cdGerencia option").filter(":selected").val();
	if (alta) {
		tipologia.cdUsrAlta = sessionStorage.getItem("cdUsuario");
		tipologia.fhAlta = new Date();
		tipologia.cdStSistema = CD_ST_SIS_A;
	} else {
		tipologia.cdUsrAlta = objTmp.cdUsrAlta;
		tipologia.fhAlta = objTmp.fhAlta;
		tipologia.cdStSistema = objTmp.cdStSistema;
	}
	return tipologia;
}

/** Funciones de validación **/
function validarTipologia() {
	var validador = $('#MSCAVE01012_2_FORM').validate({
		rules : {
			cdGerencia : {
				required : true,
				min : 1
			},
			nbTipologia : {
				required : true,
				maxlength:200
				
			},
			cdTipologia : {
				required : true,
				maxlength: 2
			},
			nuUmbralMC : {
				digits: true,
				maxlength: 3
			}
		},
		errorPlacement : function(error, element) {
			$(element).prop('style', 'border: 1px solid #C8175E;');
		},
		success : function(label, element) {
			$(element).prop('style', 'border: 1px solid #86C82D;');
		}
	});
}

/** Inicialización **/
(function($, window, document) {
	$(function() {
		
		$.validator.addMethod('noAceptaCaracteresEspeciales', function(value, element){
			
			var valor = value.toUpperCase();
		
			if (valor == CD_TP_PERSONA_FISICA || valor == CD_TP_PERSONA_MORAL || CD_TP_PERSONA_GOBIERNO) {
				return true;  
			} else {
				return false;   
			};
		},'');
		
		
		validaPerfil();
		if (PERFILADO) {
			llenarSelectPorTabla('Gerencia', $('#cdGerencia'), true);
			validarTipologia();
			$('#nuUmbralMC').blur(function(){
				if($(this).val() == ''){
					habilCtrl('#nuUmbralMC');
				}
			});
		}
		
		var cdGerencia = 0;
		if(sessionStorage.cdPerfil != null && sessionStorage.cdPerfil != '' && sessionStorage.cdPerfil != undefined && sessionStorage.cdPerfil != "null"){
			if(sessionStorage.cdPerfil == 2){
				cdGerencia = sessionStorage.cdGerencia;
			}
		}
		
		obtenerTipologias(cdGerencia).done(function(data) {
			if (cargarMensaje(data, false)) {
				llenadoTablaTipologia(data.response);
			} else {
				llenadoTablaTipologia([]);
			}
		});
		$('.date').datepicker({
			language : 'en',
			format : "dd/mm/yyyy",
			autoclose : true
		});
	});
}(window.jQuery, window, document));

/** Funcionalidad **/
function obtenerTipologias(cdGerencia) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true)+ "/TipologiaService/consultarTipologias/"+cdGerencia,
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		async : false
	});
}

/*
 * ABC Tipología
 */
function abcTipologia(tipologia) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/TipologiaService";
	var _type = "POST";
	if (alta) {
		_url += "/altaTipologia";
	} else {
		_url += "/modificaTipologia";
	}
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(tipologia)
	});
}

function validacion() {
	var tipologiaValida = $('#MSCAVE01012_2_FORM').valid();
	if (tipologiaValida) {
		var tipologia = crearObjetoTipologia();
		if (modificar && !acepto) {
			$('#dialogActualizaTipologia').modal('show');
		}
		if ((modificar && acepto)||(eliminar && acepto) || (alta)) {
			abcTipologia(tipologia).done(function(data) {
				if (cargarMensaje(data, true)) {
					obtenerTipologias(cdGerencia).done(function(data) {
						if (cargarMensaje(data, false)) {
							$('#tablaTipologia').bootstrapTable('load', data.response);
						} else {
							$('#tablaTipologia').bootstrapTable('load', []);
						}
					});					
					borrarDatos();
				}
			});
		}
	}
}