/** VARIABLES DE CONTROL **/
var modificar = false;
var eliminar = false;
var alta = true;
var acepto = false;
var CD_ST_SIS_A = '11';
var CD_ST_SIS_I = '12';
var objTmp = new Object();
var GERENCIA = sessionStorage.getItem("cdGerencia");
var PERFIL_COORDINA = 3;
var PERFIL_SUPERVISOR = 2;
var PERFIL = (PERFIL_SUPERVISOR == sessionStorage.getItem("cdPerfil1"));
var PERFILADO = (sessionStorage.cdPerfil == PERFIL_COORDINA) || (sessionStorage.cdPerfil == PERFIL_SUPERVISOR);

/** Funciones de Datos **/
function borrarDatos() {
	modificar = false;
	acepto = false;
	eliminar = false;
	alta = true;
	objTmp = new Object();
	$('#cdTipologia').val('-1');
	$('#cdCriterio').val('-1');
	$('#cdSector').val('-1');
	$('#nuRangoMin').val('');
	$('#nuRangoMax').val('');
	$('#nbValor').val('');
	$('#nuPuntos').val('');
	$('#nbDescripcion').val('');
	habilCtrl('#nuRangoMin');
	habilCtrl('#nuRangoMax');
	habilCtrl('#nbDescripcion');
	habilCtrl('#nbValor');
	resetVal();
}

function fncModificar(valor){
	acepto = valor;
	validacion();
	if(!valor){
		resetVal();
	}
}

function fncEliminar(valor){
	acepto = valor;
	eliminar = valor;
	validacion();
	borrarDatos();
}

function resetVal(){
	$('#MSCAVE01012_5_FORM input').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
	$('#MSCAVE01012_5_FORM select').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
}

/** Funciones de eventos **/
var actionEvents = {
	'click .delete' : function(e, value, row, index) {
		eliminar = true;
		modificar = false;
		alta = false;
		llenarMatriz(row);
		objTmp.cdStSistema = CD_ST_SIS_I;
		valRanMaxMin();
		valDesVal();
		$('#dialogEliminarMatrizCriterios').modal('show');
	},
	'click .actualizar' : function(e, value, row, index) {
		modificar = true;
		eliminar = false;
		alta = false;
		llenarMatriz(row);
		valRanMaxMin();
		valDesVal();
		resetVal();
	}
};

/** Funciones de Tablas **/
function llenadoTablaMatriz(data) {
	$('#tablaMatriz').bootstrapTable(
		{
			pagination : true,
			pageSize : 5,
			data : data,
			clickToSelect : false,
			singleSelect : false,
			maintainSelected : true,
			sortable : true,
			checkboxHeader : false,
			formatShowingRows : function(pageFrom, pageTo, totalRows) {
				return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
			},
			formatRecordsPerPage : function(pageNumber) {
				return pageNumber + ' registros por p\u00e1gina';
			},
			formatLoadingMessage : function() {
				return 'Cargando, espere por favor...';
			},
			formatSearch : function() {
				return 'Buscar';
			},
			formatNoMatches : function() {
				return 'No se encontr&oacute; informaci&oacute;n';
			}
		}
	);
}

/** Funciones de Formato **/
function formatterImageDel(value, row, index) {
	if(PERFILADO){
		return [
				'<a class="delete" href="javascript:void(0)" title="Eliminar">',
				'<i class="tabla_registro_eliminar">.</i>',
				'</a>',
				'<a class="actualizar" href="javascript:void(0)" title="Actualizar">',
				'<i class="tabla_registro_editar">.</i>', '</a>'
			].join('');
	}else{
		return ['',''].join('');
	}
}

function llenarObjeto(data, apartado) {
	$.each(data, function(key, value) {
		if(key == 'cdStSistema'){
			objTmp.cdStSistema = value;
		}else if (key == 'cdMatCrit'){
			objTmp.cdMatCrit = value;
		}
	});
}

function llenarMatriz(data) {
	llenarFormulario(data, 'cabeceraMC');
	llenarObjeto(data, 'cabeceraMC');
}

function deshCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).attr('disabled','disabled');
	$(selector).addClass('ignore');
}

function habilCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).removeAttr('disabled');
	$(selector).removeClass('ignore');
}

function validaPerfil(){
	if (PERFILADO) {
		$("#contenedorMSCAVE01012_5").show();
	} else {
		$("#contenedorMSCAVE01012_5").hide();
	}
}

/** *** OBJETOS **** */
function crearObjetoMatrizCriterios() {
	matriz = new Object();
	matriz.cdTipologia = $("select#cdTipologia option").filter(":selected").val();
	matriz.cdCriterio = $("select#cdCriterio option").filter(":selected").val();
	matriz.cdSector = $("select#cdSector option").filter(":selected").val();
	matriz.nuRangoMin = $("#nuRangoMin").val();
	matriz.nuRangoMax = $("#nuRangoMax").val();
	matriz.nbValor = $("#nbValor").val();
	matriz.nuPuntos = $("#nuPuntos").val();
	matriz.nbDescripcion = $("#nbDescripcion").val().toUpperCase();
	if (alta) {
		matriz.cdStSistema = CD_ST_SIS_A;
	} else {
		matriz.cdStSistema = objTmp.cdStSistema;
		matriz.cdMatCrit = objTmp.cdMatCrit;
	}
	matriz.cdUsuario = sessionStorage.cdUsuario; 
	return matriz;
}

/** Funciones de validación **/
function valRanMaxMin(){
	if($('#nuRangoMin').val() != '' || $('#nuRangoMax').val() != ''){
		$('#nuRangoMin').valid();
		$('#nuRangoMax').valid();
		deshCtrl('#nbDescripcion');
		deshCtrl('#nbValor');
	}else{
		habilCtrl('#nbDescripcion');
		habilCtrl('#nbValor');
	}
}

function valDesVal(){
	if($('#nbDescripcion').val() != '' || $('#nbValor').val() != ''){
		$('#nbDescripcion').valid();
		$('#nbValor').valid();
		deshCtrl('#nuRangoMin');
		deshCtrl('#nuRangoMax');
	}else{
		habilCtrl('#nuRangoMin');
		habilCtrl('#nuRangoMax');
	}
}

function validarMatrizCriterios() {
	$.validator.addMethod('validaMaxMin',function (value){
		return $('#nuRangoMin').val() != '' && $('#nuRangoMax').val() != ''
		       && (parseInt($('#nuRangoMin').val()) < parseInt($('#nuRangoMax').val()));
	},'error!');
	$.validator.addMethod('validaValor',function (value){
		return ($('#nbValor').val() != '' && $('#nbDescripcion').val() != '');
	},'error!');
	$.validator.addMethod('validaSelect',function (value, element, arg){
		return arg != value;
	},'error!');
	var validador = $('#MSCAVE01012_5_FORM').validate(
		{
			rules : {
				cdTipologia : {
					required : true,
					validaSelect: '-1'
				},
				cdCriterio : {
					required : true,
					validaSelect: '-1'
				},
				nuRangoMin : {
					validaMaxMin: true,
					digits: true
				},
				nuRangoMax : {
					validaMaxMin: true,
					digits: true
				},
				nuPuntos : {
					required : true,
					digits: true
				},
				nbValor : {
					required : true,
					validaValor: true,
					maxlength: 60
				},
				nbDescripcion : {
					validaValor: true,
					required: true,
					maxlength: 60
				}
			},
			errorPlacement : function(error, element) {
				$(element).prop('style', 'border: 1px solid #C8175E;');
			},
			success : function(label, element) {
				$(element).prop('style', 'border: 1px solid #86C82D;');
			}
		}
	);
}

/** Inicialización **/
(function($, window, document) {
	$(function() {
		validaPerfil();
		if(PERFILADO){
			if(PERFIL){
				llenarSelectPorTablaClaveParametro('TipologiaGerencia', parseInt(GERENCIA), $('#cdTipologia'), true);
			}else{
				llenarSelectPorTablaClave('Tipologia', $('#cdTipologia'), true);
			}
			llenarSelectPorTabla('Criterio', $('#cdCriterio'),true);
			llenarSelectPorTabla('Sector', $('#cdSector'),true);
			validarMatrizCriterios();
			$('#nuRangoMin').blur(function(){
				valRanMaxMin();
			});
			$('#nuRangoMax').blur(function(){
				valRanMaxMin();
			});
			$('#nbValor').blur(function (){
				valDesVal();
			});
			$('#nbDescripcion').blur(function (){
				valDesVal();
			});
		}
		obtenerMatrizCriteriosTipologia().done(function(data) { 
			if(cargarMensaje(data,false)){
				llenadoTablaMatriz(data.response);
			}else{
				llenadoTablaMatriz([]);
			}
		});
		$('.date').datepicker({
			language : 'en',
			format : "dd/mm/yyyy",
			autoclose : true
		});
	});
}(window.jQuery, window, document));

/** Funcionalidad **/
function obtenerMatrizCriteriosTipologia(){
	var _url = obtenerDeSessionStorage("baseUrl", true);
	if(PERFIL){
		_url += "/matrizService/consultarMatricesGerencia/" + GERENCIA;
	}else{
		_url += "/matrizService/consultarMatrices"
	}
	 return $.ajax({
		url : _url,
		type : 'GET',
		contentType: 'application/json',
		dataType: 'json',
		async: false
	});
}

/*
* ABC matriz de criterios
*/
function abcMatrizCriterios(matriz) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/matrizService";
	var _type = "POST";
	if (alta) {
		_url += "/altaMatriz";
	} else {
		_url += "/modificaMatriz";
	}
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(matriz)
	});
}

function validacion() {
	var matrizValida = $('#MSCAVE01012_5_FORM').valid();
	if(matrizValida){
		var matriz = crearObjetoMatrizCriterios();
		if(modificar && !acepto){
		    $('#dialogActualizaMatrizCriterios').modal('show');
		}
		if ((modificar && acepto)||(eliminar && acepto) || (alta)) {
			abcMatrizCriterios(matriz).done(function(data) {
				if (cargarMensaje(data, true)) {
					obtenerMatrizCriteriosTipologia().done(function(data) {
						if (cargarMensaje(data, false)) {
							$('#tablaMatriz').bootstrapTable('load',data.response);
						} else {
							$('#tablaMatriz').bootstrapTable('load',[]);
						}
					});
					borrarDatos();
				}
			});
		}
	}
}