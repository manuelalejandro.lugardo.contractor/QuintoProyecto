/** VARIABLES DE CONTROL **/
var modificar = false;
var eliminar = false;
var alta = true;
var acepto = false;
var CD_ST_SIS_A = '11';
var CD_ST_SIS_I = '12';
var CAT_PRIORIDADES = 3;
var objTmp = new Object();
var PERFIL_COORDINA = 3;
var PERFIL_SUPERVISOR = 2;
var PERFILADO = (sessionStorage.cdPerfil == PERFIL_COORDINA) || (sessionStorage.cdPerfil == PERFIL_SUPERVISOR);

/** Funciones de Datos **/
function borrarDatos(){
	modificar = false;
	acepto = false;
	eliminar = false;
	alta = true;
	objTmp = new Object();
	$('#nbTipoFuente').val('');
	$('#cdPrioridad').val('-1');
	resetVal();
}

function fncModificar(valor){
	acepto = valor;
	validacion();
	if(!valor){
		resetVal();
	}
}

function fncEliminar(valor){
	acepto = valor;
	eliminar = valor;
	validacion();
	borrarDatos();
}

function resetVal(){
	$('#MSCAVE01012_7_FORM input').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
	$('#MSCAVE01012_7_FORM select').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
}

/** Funciones de eventos **/
var actionEvents = { 
    'click .delete': function (e, value, row, index) {
		eliminar = true;
		modificar = false;
		alta = false;
		llenarTipoPrioridad(row);
		objTmp.cdStatus = CD_ST_SIS_I;
		$('#dialogEliminarTipoPrioridad').modal('show');
    },
    'click .actualizar': function (e, value, row, index){
		modificar = true;
		eliminar = false;
		alta = false;
		llenarTipoPrioridad(row);
    }
};

/** Funciones de Tablas **/
function llenadoTipoPrioridad(data) {
	$('#tablaTipoPrioridad').bootstrapTable(
		{
			pagination: true,
			pageSize: 5,
			pageList: [5, 25, 50],
	        data: data,
	        clickToSelect: false,
	        singleSelect: false,
	        maintainSelected: true,
	        sortable: true,
	        checkboxHeader: false,
	        formatShowingRows: function (pageFrom, pageTo, totalRows) {
	            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
	        },
	        formatRecordsPerPage: function (pageNumber) {
	        	return pageNumber + ' registros por p\u00e1gina';
	        },
	        formatLoadingMessage: function () {
	        	return 'Cargando, espere por favor...';
	        },
	        formatSearch: function () {
	        	return 'Buscar';
	        },
	        formatNoMatches: function () {
	        	return 'No se encontr&oacute; informaci&oacute;n';
	        }      
	    }
	);
}

/** Funciones de Formato **/
function formatterImageDel(value, row, index) {
	if(PERFILADO){
		return [
			    '<a class="delete" href="javascript:void(0)" title="Eliminar">',
			    '<i class="tabla_registro_eliminar">.</i>',
			    '</a>',
			    '<a class="actualizar" href="javascript:void(0)" title="Actualizar">',
			    '<i class="tabla_registro_editar">.</i>',
			    '</a>'
		    ].join('');
	}else{
		return ['',''].join('');
	}
}

function validaPerfil(){
	if (PERFILADO) {
		$("#abcTipoPrioridad").show();
	} else {
		$("#abcTipoPrioridad").hide();
	}
}

/** Funciones de Formulario **/
function llenarObjecto(data){
	$.each(data, function(key, value) {
		if(key=='cdStatus'){
			objTmp.cdStatus = value;
		}else if(key=='cdNumtipftePri'){
			objTmp.cdNumtipftePri = value;
		}
	});
}

function llenarTipoPrioridad(data){
	llenarFormulario(data,'prioridad');
	llenarObjecto(data);
}

/** *** OBJETOS **** */
function crearObjetoTipoPrioridad() {
	tipoPrioridad = new Object();
	tipoPrioridad.cdPrioridad = $("select#cdPrioridad option").filter(":selected").val();
	tipoPrioridad.nbTipoFuente = $("#nbTipoFuente").val().toUpperCase();
	if (alta) {
		tipoPrioridad.cdStatus = CD_ST_SIS_A;
	} else {
		tipoPrioridad.cdStatus = objTmp.cdStatus;
		tipoPrioridad.cdNumtipftePri = objTmp.cdNumtipftePri;
	}
	tipoPrioridad.cdUsuario = sessionStorage.cdUsuario;
	return tipoPrioridad;
}

/** Funciones de validación **/
function validarTipoPrioridad()
{
	var validador = $('#MSCAVE01012_7_FORM').validate( {
		rules: {
			nbTipoFuente: {
				required: true,
				maxlength: 20,
				minlength:1
			},
			cdPrioridad: {
				required: true,
				min: 1
			}
		},
		errorPlacement: function (error, element) {
            $(element).prop('style', 'border: 1px solid #C8175E;');
        },
        success: function (label, element) {
            $(element).prop('style', 'border: 1px solid #86C82D;');
        }
	});
}

/** Inicialización **/
(function($, window, document) {
	$(function() {
		validaPerfil();
		if(PERFILADO){
			llenarSelectPorCdCatalogo(CAT_PRIORIDADES,$('#cdPrioridad'),true);
			validarTipoPrioridad();
		}
		$('.date').datepicker({
			language : 'en',
			format : "dd/mm/yyyy",
			autoclose : true,
			orientation : 'top'
		});
		$('.money').maskMoney({
			prefix : '$',
			precision : 2
		});
		obtenerTipoPrioridad().done(function(data) {
			if (cargarMensaje(data, false)) {
				llenadoTipoPrioridad(data.response);
			} else {
				llenadoTipoPrioridad([]);
			}
		});
	});
}(window.jQuery, window, document));

/** Funcionalidad **/
function obtenerTipoPrioridad(){
	 return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/TipoPrioridadService/consultarTipoPrioridad",
		type : 'GET',
		contentType: 'application/json',
		dataType: 'json',
		async: false
	});
}

/*
 * ABC Tipo - Prioridad
 */
function abcTipoPrioridad(tipoPrioridad) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/TipoPrioridadService";
	var _type = "POST";

	if (alta) {
		_url += "/altaTipoPrioridad";
	} else {
		_url += "/modificaTipoPrioridad";
	}
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(tipoPrioridad)
	});
}

function validacion() {
	var tipoPrioridadValida = $('#MSCAVE01012_7_FORM').valid(); 
	if(tipoPrioridadValida){
		var tipoPrioridad = crearObjetoTipoPrioridad();
		if(modificar && !acepto){
		    $('#dialogActualizaTipoPrioridad').modal('show');
		}
		if ((modificar && acepto)||(eliminar && acepto) || (alta)) {
			abcTipoPrioridad(tipoPrioridad).done(function(data) {
				if (cargarMensaje(data, true)) {
					obtenerTipoPrioridad().done(function(data) {
						if (cargarMensaje(data, false)) {
							$('#tablaTipoPrioridad').bootstrapTable('load',data.response);
						} else {
							$('#tablaTipoPrioridad').bootstrapTable('load',[]);
						}
					});
					borrarDatos();
				}
			});
		}
	}
}