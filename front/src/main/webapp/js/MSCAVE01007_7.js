(function($, window, document) {
	$(function() {	
		
		llenaTablaCuatri(sessionStorage.cdCaso,sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) {
			if(cargarMensaje(data,false)){    
				llenadoTablaCuatrimestral(data.response);
					 
			}});
		
		llenaTablaMesOne(sessionStorage.cdCaso,sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) {
			if(cargarMensaje(data,false)){    
				llenadoTablaMes1(data.response);
					 
			}});
		
		llenaTablaMesDos(sessionStorage.cdCaso,sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) {
			if(cargarMensaje(data,false)){    
				llenadoTablaMes2(data.response);
					 
			}});
		
		llenaTablaMesTres(sessionStorage.cdCaso,sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) {
			if(cargarMensaje(data,false)){    
				llenadoTablaMes3(data.response);
					 
			}});
		
		llenaTablaMesCuatro(sessionStorage.cdCaso,sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) {
			if(cargarMensaje(data,false)){    
				llenadoTablaMes4(data.response);					 
			}});
		
		cabeceraCuatrimestral(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var inicio, fin;
		    			if(val.inicio != undefined && val.inicio != null && val.inicio != ' '){
		    				if(val.inicio == 1){inicio = 'Enero';}
			    			if(val.inicio == 2){inicio = 'Febrero';}
			    			if(val.inicio == 3){inicio = 'Marzo';}
			    			if(val.inicio == 4){inicio = 'Abril';}
			    			if(val.inicio == 5){inicio = 'Mayo';}
			    			if(val.inicio == 6){inicio = 'Junio';}
			    			if(val.inicio == 7){inicio = 'Julio';}
			    			if(val.inicio == 8){inicio = 'Agosto';}
			    			if(val.inicio == 9){inicio = 'Septiembre';}
			    			if(val.inicio == 10){inicio = 'Octubre';}
			    			if(val.inicio == 11){inicio = 'Noviembre';}
			    			if(val.inicio == 12){inicio = 'Diciembre';}
		    			}else{
		    				inicio = '';
		    			}
		    			if(val.fin != undefined && val.fin != null && val.fin != ' '){
		    				if(val.fin == 1){fin = 'Enero';}
			    			if(val.fin == 2){fin = 'Febrero';}
			    			if(val.fin == 3){fin = 'Marzo';}
			    			if(val.fin == 4){fin = 'Abril';}
			    			if(val.fin == 5){fin = 'Mayo';}
			    			if(val.fin == 6){fin = 'Junio';}
			    			if(val.fin == 7){fin = 'Julio';}
			    			if(val.fin == 8){fin = 'Agosto';}
			    			if(val.fin == 9){fin = 'Septiembre';}
			    			if(val.fin == 10){fin = 'Octubre';}
			    			if(val.fin == 11){fin = 'Noviembre';}
			    			if(val.fin == 12){fin = 'Diciembre';}
		    			}else{
		    				fin = '';
		    			}
		    			
		    			$("#conceptoCuatrimestral").append('Cuadro por Concepto Cuatrimestral   ' + fin + ' - ' + inicio);
					});
			}else{
				$("#conceptoCuatrimestral").html('Ocurri\u00f3 un error');
			}
		});
		
		cabeceraMesUno(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var mes;
		    			if(val.mes != undefined && val.mes != null && val.mes != ' '){
		    				if(val.mes == 1){mes = 'Enero';}
			    			if(val.mes == 2){mes = 'Febrero';}
			    			if(val.mes == 3){mes = 'Marzo';}
			    			if(val.mes == 4){mes = 'Abril';}
			    			if(val.mes == 5){mes = 'Mayo';}
			    			if(val.mes == 6){mes = 'Junio';}
			    			if(val.mes == 7){mes = 'Julio';}
			    			if(val.mes == 8){mes = 'Agosto';}
			    			if(val.mes == 9){mes = 'Septiembre';}
			    			if(val.mes == 10){mes = 'Octubre';}
			    			if(val.mes == 11){mes = 'Noviembre';}
			    			if(val.mes == 12){mes = 'Diciembre';}
		    			}else{
		    				mes = '';
		    			}
		    			
		    			$("#cabeceraMesUno").append('Cuadro por Concepto - ' + mes);
					});
			}else{
				$("#cabeceraMesUno").html('Ocurri\u00f3 un error');
			}
		});
		
		cabeceraMesDos(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var mes;
		    			if(val.mes2 != undefined && val.mes2 != null && val.mes2 != ' '){
		    				if(val.mes2 == 1){mes = 'Enero';}
			    			if(val.mes2 == 2){mes = 'Febrero';}
			    			if(val.mes2 == 3){mes = 'Marzo';}
			    			if(val.mes2 == 4){mes = 'Abril';}
			    			if(val.mes2 == 5){mes = 'Mayo';}
			    			if(val.mes2 == 6){mes = 'Junio';}
			    			if(val.mes2 == 7){mes = 'Julio';}
			    			if(val.mes2 == 8){mes = 'Agosto';}
			    			if(val.mes2 == 9){mes = 'Septiembre';}
			    			if(val.mes2 == 10){mes = 'Octubre';}
			    			if(val.mes2 == 11){mes = 'Noviembre';}
			    			if(val.mes2 == 12){mes = 'Diciembre';}
		    			}else{
		    				mes = '';
		    			}
		    			
		    			$("#cabeceraMesDos").append('Cuadro por Concepto - ' + mes);
					});
			}else{
				$("#cabeceraMesDos").html('Ocurri\u00f3 un error');
			}
		});
		
		cabeceraMesTres(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var mes;
		    			if(val.mes3 != undefined && val.mes3 != null && val.mes3 != ' '){
		    				if(val.mes3 == 1){mes = 'Enero';}
			    			if(val.mes3 == 2){mes = 'Febrero';}
			    			if(val.mes3 == 3){mes = 'Marzo';}
			    			if(val.mes3 == 4){mes = 'Abril';}
			    			if(val.mes3 == 5){mes = 'Mayo';}
			    			if(val.mes3 == 6){mes = 'Junio';}
			    			if(val.mes3 == 7){mes = 'Julio';}
			    			if(val.mes3 == 8){mes = 'Agosto';}
			    			if(val.mes3 == 9){mes = 'Septiembre';}
			    			if(val.mes3 == 10){mes = 'Octubre';}
			    			if(val.mes3 == 11){mes = 'Noviembre';}
			    			if(val.mes3 == 12){mes = 'Diciembre';}
		    			}else{
		    				mes = '';
		    			}
		    			
		    			
		    			$("#cabeceraMesTres").append('Cuadro por Concepto - ' + mes);
					});
			}else{
				$("#cabeceraMesTres").html('Ocurri\u00f3 un error');
			}
		});
		
		cabeceraMesCuatro(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var mes;
		    			if(val.mes4 != undefined && val.mes4 != null && val.mes4 != ' '){
		    				if(val.mes4 == 1){mes = 'Enero';}
			    			if(val.mes4 == 2){mes = 'Febrero';}
			    			if(val.mes4 == 3){mes = 'Marzo';}
			    			if(val.mes4 == 4){mes = 'Abril';}
			    			if(val.mes4 == 5){mes = 'Mayo';}
			    			if(val.mes4 == 6){mes = 'Junio';}
			    			if(val.mes4 == 7){mes = 'Julio';}
			    			if(val.mes4 == 8){mes = 'Agosto';}
			    			if(val.mes4 == 9){mes = 'Septiembre';}
			    			if(val.mes4 == 10){mes = 'Octubre';}
			    			if(val.mes4 == 11){mes = 'Noviembre';}
			    			if(val.mes4 == 12){mes = 'Diciembre';}
		    			}else{
		    				mes = '';
		    			}
		    			
		    			
		    			$("#cabeceraMesCuatro").append('Cuadro por Concepto - ' + mes);
					});
			}else{
				$("#cabeceraMesCuatro").html('Ocurri\u00f3 un error');
			}
		});
	});
}(window.jQuery, window, document));

function llenadoTablaCuatrimestral(data) {
	$('#tablaCuatrimestral').bootstrapTable({
		pagination: false,
		pageSize: 3,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaMes1(data) {
	$('#tablaMes1').bootstrapTable({
		pagination: false,
		pageSize: 3,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaMes2(data) {
	$('#tablaMes2').bootstrapTable({
		pagination: false,
		pageSize: 3,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaMes3(data) {
	$('#tablaMes3').bootstrapTable({
		pagination: false,
		pageSize: 3,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaMes4(data) {
	$('#tablaMes4').bootstrapTable({
		pagination: false,
		pageSize: 3,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function formatterFooterTotalMecanica(data) {
    return 'Total:';
}

function formatterFooterTotalMontoOperaciones(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    var formatTotalMontoOperaciones = formatterMoney(total);
    var lblTotalMontoOperaciones = "";
    if(formatTotalMontoOperaciones.length >= 16){
    	lblTotalMontoOperaciones = '<span style="width:75px;overflow:hidden;display:inline-block;vertical-align:middle;">'+formatTotalMontoOperaciones+'</span><img src="../img/componentes/flecha3.png" title="'+formatTotalMontoOperaciones+'" style="margin-top:1px;margin-top: 3px;"/>';
    }else{
    	lblTotalMontoOperaciones = formatTotalMontoOperaciones
    }
    return lblTotalMontoOperaciones;
}

function formatterFooterPromedioOperaciones(data, field) {
    var total = 0;
    var contador = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
        contador++;
    });
    
    var formatTotalMontoOperaciones = formatterMoney(total);
    var lblTotalMontoOperaciones = "";
    if(formatTotalMontoOperaciones.length >= 16){
    	lblTotalMontoOperaciones = '<span style="width:75px;overflow:hidden;display:inline-block;vertical-align:middle;">'+formatTotalMontoOperaciones+'</span><img src="../img/componentes/flecha3.png" title="'+formatTotalMontoOperaciones+'" style="margin-top:1px;margin-top: 3px;"/>';
    }else{
    	lblTotalMontoOperaciones = formatTotalMontoOperaciones
    }
    
    return lblTotalMontoOperaciones;
}

function formatterFooterTotalOperaciones(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    return ''+total;
}

function formatterFooterTotalPorcentaje(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    total = Math.round(total);
    return total+'%';
}

function detalleMovimientos(){
	$('#MSCAVE01005').load('MSCAVE01005.html', function (response, status, xhr){
        if (status == "success") {
            $(this).modal('show');
        }else{
        	data.cdMensaje = 0;
        	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
        	cargarMensaje(data,false);
        }
    });
}


function llenaTablaCuatri(cdCaso,cdAlerta,cdSesion) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultarTablaCuatrimestralAnalisis/'+cdCaso+'/'+cdAlerta+'/'+cdSesion; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		datatype: 'json',
		async: true
		 
	}); 
}

function llenaTablaMesOne(cdCaso,cdAlerta,cdSesion) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultarConceptosMesUnoAnalisis/'+cdCaso+'/'+cdAlerta+'/'+cdSesion; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		datatype: 'json',
		async: true
		 
	}); 
} 

function llenaTablaMesDos(cdCaso,cdAlerta,cdSesion) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultarConceptosMesDosAnalisis/'+cdCaso+'/'+cdAlerta+'/'+cdSesion; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		datatype: 'json',
	    async: true
	}); 
} 

function llenaTablaMesTres(cdCaso,cdAlerta,cdSesion) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultarConceptosMesTresAnalisis/'+cdCaso+'/'+cdAlerta+'/'+cdSesion; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		datatype: 'json',
		async: true 
	}); 
} 

function llenaTablaMesCuatro(cdCaso,cdAlerta,cdSesion) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultarConceptosMesCuatroAnalisis/'+cdCaso+'/'+cdAlerta+'/'+cdSesion; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		datatype: 'json',
		async: true
	}); 
} 

function cabeceraCuatrimestral(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraCuatrimestral/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}

function cabeceraMesUno(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraMesUno/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}
function cabeceraMesDos(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraMesDos/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}
function cabeceraMesTres(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraMesTres/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}
function cabeceraMesCuatro(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraMesCuatro/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}
