var TP_OPERACION_ENVIADAS = 'E';
var TP_OPERACION_RECIBIDAS = 'R';


(function($, window, document) {
	$(function() {

		$('#operacionesInternacionalesHead').html('Transferencias Internacionales Enviadas  ' + fechaDosAniosAnteriores() );		
		$('#operacionesInternacionalesHead2').html( 'Transferencias Internacionales Recibidas  ' + fechaDosAniosAnteriores() );
		$('#anioDosE').html( getAnio( 2 ) );
		$('#anioUnoE').html( getAnio( 1 ) );
		$('#anioActualE').html( getAnio( 0 ) );
		$('#anioDosR').html( getAnio( 2 ) );
		$('#anioUnoR').html( getAnio( 1 ) );
		$('#anioActualR').html( getAnio( 0 ) );		
		
		llenadoTablaTransferenciasEnviadas( [] );
		llenadoTablaTransferenciasRecibidas( [] );
		llenadoTablaDetalleTrasferenciasInternacionalesEnviadas([]);
		llenadoTablaDetalleTrasferenciasInternacionalesRecibidas([]);
		//cdSistema, nuFolioAlerta, numCuenta, tipoOperacion, anioActual
    	//sessionStorage.cdCaso = row.cdCaso;
		//sessionStorage.nuFolioAlerta = row.nuFolioAlerta;
		//sessionStorage.cdSistema = row.cdSistema;
		//sessionStorage.cdCliente = row.nuCliente;
		//sessionStorage.nuCuenta = row.nuCuenta;		
		
		// Consulta Transferencias Internacionales Enviadas
		llenaTransferencia(sessionStorage.cdSistema,sessionStorage.nuFolioAlerta,sessionStorage.nuCuenta, TP_OPERACION_ENVIADAS, getAnio( 0 ) ).done(function(data) {
			if (cargarMensaje(data, false)) 
			{
				$('#tablaTransferenciasEnviadas').bootstrapTable('load', data.response );
			}
		});
		
		// Consulta Transferencias Internacionales Recibidas
		llenaTransferencia(sessionStorage.cdSistema,sessionStorage.nuFolioAlerta,sessionStorage.nuCuenta, TP_OPERACION_RECIBIDAS, getAnio( 0 ) ).done(function(data) {
			if (cargarMensaje(data, false)) 
			{
				$('#tablaTransferenciasRecibidas').bootstrapTable('load', data.response );
			}
		});		
		
		/*
		llenaTransferencia(sessionStorage.cdSistema,sessionStorage.nuFolioAlerta).done(function(data) {
			if (cargarMensaje(data, false)) 
			{
				llenadoTablaTransferenciasEnviadas(data.response.tablaEnviadas);
				llenadoTablaTransferenciasRecibidas(data.response.tablaRecibidas);
			}
		});
		*/
	});
}(window.jQuery, window, document));

// ===== TABLAS =====
function llenadoTablaTransferenciasEnviadas(data) {
	$('#tablaTransferenciasEnviadas').bootstrapTable(
			{
				pagination : false,
				pageSize : 5,
				data : data,
				clickToSelect : false,
				singleSelect : false,
				maintainSelected : true,
				sortable : false,
				checkboxHeader : false,
				formatShowingRows : function(pageFrom, pageTo, totalRows) {
					return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
				},
				formatRecordsPerPage : function(pageNumber) {
					return pageNumber + ' registros por p\u00e1gina';
				},
				formatLoadingMessage : function() {
					return 'Cargando, espere por favor...';
				},
				formatSearch : function() {
					return 'Buscar';
				},
				formatNoMatches : function() {
					return 'No se encontr\u00f3 informaci\u00f3n';
				}
			});
}

function llenadoTablaTransferenciasRecibidas(data) {
	$('#tablaTransferenciasRecibidas').bootstrapTable(
			{
				pagination : false,
				pageSize : 3,
				data : data,
				clickToSelect : false,
				singleSelect : false,
				maintainSelected : true,
				sortable : false,
				checkboxHeader : false,
				formatShowingRows : function(pageFrom, pageTo, totalRows) {
					return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de '
					+ totalRows + ' registros';
				},
				formatRecordsPerPage : function(pageNumber) {
					return pageNumber + ' registros por p\u00e1gina';
				},
				formatLoadingMessage : function() {
					return 'Cargando, espere por favor...';
				},
				formatSearch : function() {
					return 'Buscar';
				},
				formatNoMatches : function() {
					return 'No se encontr\u00f3 informaci\u00f3n';
				}
			});
}

function llenadoTablaDetalleTrasferenciasInternacionalesEnviadas(data) {
	$('#tablaDetalleTrasferenciasInternacionalesEnviadas').bootstrapTable(
			{
				pagination : true,
				pageSize : 5,
				data : data,
				clickToSelect : false,
				singleSelect : false,
				maintainSelected : true,
				sortable : false,
				checkboxHeader : false,
				formatShowingRows : function(pageFrom, pageTo, totalRows) {
					return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de '
					+ totalRows + ' registros';
				},
				formatRecordsPerPage : function(pageNumber) {
					return pageNumber + ' registros por p\u00e1gina';
				},
				formatLoadingMessage : function() {
					return 'Cargando, espere por favor...';
				},
				formatSearch : function() {
					return 'Buscar';
				},
				formatNoMatches : function() {
					return 'No se encontr\u00f3 informaci\u00f3n';
				}
			});
}

function llenadoTablaDetalleTrasferenciasInternacionalesRecibidas(data) {
	$('#tablaDetalleTrasferenciasInternacionalesRecibidas').bootstrapTable(
			{
				pagination : true,
				pageSize : 5,
				data : data,
				clickToSelect : false,
				singleSelect : false,
				maintainSelected : true,
				sortable : false,
				checkboxHeader : false,
				formatShowingRows : function(pageFrom, pageTo, totalRows) {
					return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de '
					+ totalRows + ' registros';
				},
				formatRecordsPerPage : function(pageNumber) {
					return pageNumber + ' registros por p\u00e1gina';
				},
				formatLoadingMessage : function() {
					return 'Cargando, espere por favor...';
				},
				formatSearch : function() {
					return 'Buscar';
				},
				formatNoMatches : function() {
					return 'No se encontr\u00f3 informaci\u00f3n';
				}
			});
}


// ===== DETALLES =====
function abrirDetalleTrasferenciasInternacionalesRecibidas() {
	$('#dialogDetalleTrasferenciasInternacionalesRecibidas').modal('show');
	llenaTransferenciaDetalle(sessionStorage.cdSistema,sessionStorage.nuFolioAlerta, null,TP_OPERACION_RECIBIDAS).done(function(data) {
		if (cargarMensaje(data, false)) {
			$('#tablaDetalleTrasferenciasInternacionalesRecibidas').bootstrapTable('load', data.response );
		}
	});
	
}
function abrirDetalleTrasferenciasInternacionalesEnviadas() {
	$('#dialogDetalleTrasferenciasInternacionalesEnviadas').modal('show');
	llenaTransferenciaDetalle(sessionStorage.cdSistema,	sessionStorage.nuFolioAlerta, null, TP_OPERACION_ENVIADAS).done(function(data) {
		if (cargarMensaje(data, false)) {
			$('#tablaDetalleTrasferenciasInternacionalesEnviadas').bootstrapTable('load', data.response );
		}
	});	
}

// ===== FORMATTERS =====
function formatterDetailTrasferenciasInternacionales(value, row, index) {

	return [ '<table class="tableForm">'
	         + '<tr><td class="tdDetail"><b>Dato Ordenante:</b> '
	         + row.nbDatoOrdenante
	         + '</td></tr>'
	         + '<tr><td class="tdDetail"><b>Operaci&oacute;n Origen:</b> '
	         + row.nbOperacionOrigen
	         + '</td></tr>'
	         + '<tr><td class="tdDetail"><b>Operaci&oacute;n Destino:</b> '
	         + row.nbOperacionDestino
	         + '</td></tr>'
	         + '<tr><td class="tdDetail"><b>Moneda:</b> '
	         + row.nbMoneda
	         + '</td></tr>'
	         + '<tr><td class="tdDetail"><b>Importe Instrumento:</b> '
	         + formatterMoney(row.imInstrumento)
	         + '</td></tr>'
	         + '<tr>/*<td class="tdDetail"><b>Importe Dolarizado:</b> '
	         + formatterMoney(row.imDolarizado)
	         + '</td></tr>'
	         + '<tr><td class="tdDetail"><b>Pa&iacute;s Nacio. FIS:</b> '
	         + row.nbPaisFIS
	         + '</td></tr>'
	         + '<tr><td class="tdDetail"><b>Raz&oacute;n Social:</b> '
	         + row.nbRazonSocial + '</td></tr>' + '</table>' ].join('');
}

function formatterFooterVacio(data) {
	return '';
}

function formatterFooterTotal(data) {
	return 'Total General:';
}

function formatterFooterTotalMecanica(data) {
	return 'Total:';
}

function formatterFooterTotalMontoOperaciones(data, field) {
	var total = 0;
	field = this.field;
	$.each(data, function(i, row) {
		total += row[field];
	});
	//return formatterMoney(total);
	var formatTotalMontoOperaciones = formatterMoney(total);
    var lblTotalMontoOperaciones = "";
    if(formatTotalMontoOperaciones.length >= 18){
    	lblTotalMontoOperaciones = '<span style="width:75px;overflow:hidden;display:inline-block;vertical-align:middle;">'+formatTotalMontoOperaciones+'</span><img src="../img/componentes/flecha3.png" title="'+formatTotalMontoOperaciones+'" style="margin-top:1px;margin-top: 3px;"/>';
    }else{
    	lblTotalMontoOperaciones = formatTotalMontoOperaciones
    }
    return lblTotalMontoOperaciones;
}

function formatterFooterPromedioOperaciones(data, field) {
	var total = 0;
	var contador = 0;
	field = this.field;
	$.each(data, function(i, row) {
		total += row[field];
		contador++;
	});
	total = total / contador;
	return formatterMoney(total);
}

function formatterFooterTotalOperaciones(data, field) {
	var total = 0;
	field = this.field;
	$.each(data, function(i, row) {
		total += row[field];
	});
	return '' + total;
}

function formatterFooterTotalPorcentaje(data, field) {
	var total = 0;
	field = this.field;
	$.each(data, function(i, row) {
		total += row[field];
	});
	return total + '%';
}

// ===== INVOCACION DE SERVICIOS =====
function llenaTransferencia(cdSistema, nuFolioAlerta, numCuenta, tipoOperacion, anioActual ) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/CasosService/";
	_url += 'consultarTranferenciaInternacionalRecEnvDet/' + cdSistema + "/"+ nuFolioAlerta + "/" + numCuenta + "/" + tipoOperacion + "/" + anioActual;
	return $.ajax({
		url : _url,
		type : "GET",
		datatype : 'json'
	});
}
function llenaTransferenciaDetalle(cdSistema, nuFolioAlerta, numCuenta, tipoOperacion) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/CasosService/";
	_url += 'consultarDetalleTranferenciaInternacionalRecEnvDet/' + cdSistema + "/" + nuFolioAlerta + "/" + numCuenta + "/" + tipoOperacion;
	return $.ajax({
		url : _url,
		type : "GET",
		datatype : 'json'

	});
}

// ===== FUNCIONES ANIOS =====
function fechaDosAniosAnteriores() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; // enero es 0
	var yyyy = today.getFullYear() - 2;
	var yyyyAct = today.getFullYear();

	if (dd < 10) {
		dd = '0' + dd
	}
	if (mm < 10) {
		mm = '0' + mm
	}

	today = dd + '/' + mm + '/' + yyyyAct;
	lastYear = dd + '/' + mm + '/' + yyyy;
	fullDate = (' (Del ' + lastYear + ' Al ' + today + ')');

	return fullDate;
}

function getAnio( numAniosRes ){
	var fhDia = new Date();
	var anio = fhDia.getFullYear() - numAniosRes;
	return anio;
}
