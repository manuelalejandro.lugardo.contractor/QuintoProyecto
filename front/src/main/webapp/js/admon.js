(function($, window, document) {
    $(function() {
        cargarModulosPorPerfil();
    
        $("#nbCveRedHeader").html(obtenerDeSessionStorage("nbCveRed", false));
        var nombreCompleto = 
            obtenerDeSessionStorage("nbPersona", false) + " " +
            obtenerDeSessionStorage("nbPaterno", false) + " " +
            obtenerDeSessionStorage("nbMaterno", false);
        $("#nombreCompleto").html(nombreCompleto);
        $("#nbPerfil").html(obtenerDeSessionStorage("nbPerfil", false));
        
        if(obtenerDeSessionStorage("cdPerfil2", false) != null && obtenerDeSessionStorage("cdPerfil2", false)!= '' && obtenerDeSessionStorage("cdPerfil2", false)!= 'null'){
       	 	$("#cambioPerfilSeparador").show();
            $("#cambioPerfilBoton").show();
        }else{
       	 	$("#cambioPerfilSeparador").hide();
            $("#cambioPerfilBoton").hide();
        }
        
    });
}(window.jQuery, window, document));

function habilitarMenu(idMenu, nombreObjeto) {
    var modulosEnLocal = obtenerDeSessionStorage("cdModulos", true).split(",");
    var arr = jQuery.grep(modulosEnLocal, function(n, i) {
        return (n == idMenu);
    });
    if (arr.length == 0) {
        nombreObjeto.hide();
    }
}

function cargarModulosPorPerfil() {

	
	habilitarMenu(1, $("#menu_admon_1"));
    habilitarMenu(2, $("#menu_admon_2"));
    habilitarMenu(3, $("#menu_admon_3"));
    habilitarMenu(4, $("#menu_admon_4"));
    habilitarMenu(8, $("#menu_admon_8"));
    habilitarMenu(9, $("#menu_admon_9"));
    habilitarMenu(10, $("#menu_admon_10"));
    
    if(sessionStorage.cdGerencia != '5'){
        $("#menu_admon_5").hide();
        $("#menu_admon_6").hide();
        $("#menu_admon_7").hide();   	
    }
    if(sessionStorage.cdGerencia != '4'){
    	$("#menu_admon_8").hide();
    }
}


