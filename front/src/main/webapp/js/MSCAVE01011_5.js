/*var data = {"response":[
                        {"nuCliente":19458921,"nbCliente":"JUAN LUIS LIRA NAVARRO","nbRFC":"LINJ840208JC5","nbDomicilio":"MADRE RAQUEL 438 COL. 1O DE MAYO C.P. 37549","nbSector":"F32"},
                        {"nuCliente":19458922,"nbCliente":"JUAN LUIS LIRA NAVARRO","nbRFC":"LINJ840208JC5","nbDomicilio":"MADRE RAQUEL 438 COL. 1O DE MAYO C.P. 37549","nbSector":"F32"},
                        {"nuCliente":19458923,"nbCliente":"JUAN LUIS LIRA NAVARRO","nbRFC":"LINJ840208JC5","nbDomicilio":"MADRE RAQUEL 438 COL. 1O DE MAYO C.P. 37549","nbSector":"F32"},
                        {"nuCliente":19458924,"nbCliente":"JUAN LUIS LIRA NAVARRO","nbRFC":"LINJ840208JC5","nbDomicilio":"MADRE RAQUEL 438 COL. 1O DE MAYO C.P. 37549","nbSector":"F32"},
                        {"nuCliente":19458925,"nbCliente":"JUAN LUIS LIRA NAVARRO","nbRFC":"LINJ840208JC5","nbDomicilio":"MADRE RAQUEL 438 COL. 1O DE MAYO C.P. 37549","nbSector":"F32"},
                        {"nuCliente":31646316,"nbCliente":"JUAN LUIS LIRA NAVARRO","nbRFC":"LINJ840208JC5","nbDomicilio":"MADRE RAQUEL 438 COL. 1O DE MAYO C.P. 37549","nbSector":"F32"}]
						,"cdMensaje":1,"nbMensaje":"OK"};*/

var data1 = {"response":[
                        {"nbTabla":"Basicos","nuTipoModificacion":"L","fhModificacion":1434690000000,"nuCrModificacion":"0172"},
                        {"nbTabla":"Complementarios","nuTipoModificacion":"L","fhModificacion":1434690000000,"nuCrModificacion":"0172"},
                        {"nbTabla":"Avisos","nuTipoModificacion":"L","fhModificacion":1434690000000,"nuCrModificacion":"0172"},
                        {"nbTabla":"Domicilio Alternativo","nuTipoModificacion":"L","fhModificacion":1434690000000,"nuCrModificacion":"0172"},
                        {"nbTabla":"Informacion Reservada","nuTipoModificacion":"L","fhModificacion":1434690000000,"nuCrModificacion":"0172"},
                        {"nbTabla":"Personas Relacionadas","nuTipoModificacion":"L","fhModificacion":1434690000000,"nuCrModificacion":"0172"}]
						,"cdMensaje":1,"nbMensaje":"OK"};

/*var dataForm1= {"response":{
	"nbsCliente"			:"JUAN LUIS LIRA NAVARRO",
	"nuCliente"			:"A0998249",
	"nbCiudad"			:"MADRE RAQUEL 438 COL.10 DE MAYO C.P. 37549",
	"nbDomicilio"		:"LEON, GUANAJUATO",
	"nbRFC"				:"LINJ840208JC5",
	"nbTelParticular"	:"044 5500000000",
	"nbTelOficina"		:"0000",
	"nbNacionalidad"	:"MEXICANA",
	"fhNacimiento"		:531640800000,
	"nbSector"			:"F32",
	"nuAntiguedad"		:1434776400000,
	"nbActBanxico"		:"CMPR-VTA-REFACC P/APARAT ELEC"	
	},"cdMensaje":1,"nbMensaje":"OK"};*/

/*var dataForm2= {"response":{
	"nbCR"				:"0333 LEON PINO SUAREZ",
	"nbRiesgo"			:"MEDIO",
	"nbMercado"			:"DZ LEON",
	"nbDivision"		:"DIV. BAJIO",
	"nbFuncionario"		:"CHRISTIAN ESPINOZA CRUZ"
	},"cdMensaje":1,"nbMensaje":"OK"};*/

(function($, window, document) {
	
	$(function() {		
		//llenarFormulario(data.response,'datosGenerales');
		//llenarFormulario(dataForm2.response,'oficinaGestora');
		//llenadoTablaOtrosClientes(data.response);
		llenadoTablaDetalleUltimaModificacion(data1.response);
		
		consultarOtrosClientesPersonaReportada(sessionStorage.cdCliente).done(function(data) { 
			if(cargarMensaje(data,false)){
				llenadoTablaOtrosClientes(data.response);
			}else{
				llenadoTablaOtrosClientes([]);
			}
		});	
		
		consultarDatosPersonaReportada(sessionStorage.nuCuenta).done(function(data) { 
			if(cargarMensaje(data,false)){
				console.log(data.response);
				llenarFormulario(data.response,'datosGenerales');
			}else{
				llenarFormulario([]);
			}
		});	
		
		consultarDatosOficinaPersonaReportada(sessionStorage.cdCliente).done(function(data) { 
			if(cargarMensaje(data,false)){
				llenarFormulario(data.response,'oficinaGestora');
			}else{
				llenarFormulario([]);
			}
		});	
		
		
		
	});
}(window.jQuery, window, document));

function llenadoTablaOtrosClientes(data) {
	$('#tablaOtrosClientes').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaDetalleUltimaModificacion(data) {
	$('#tablaDetalleUltimaModificacion').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function consultarOtrosClientesPersonaReportada(cdCliente) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/ConsultaClienteRiesgoService/consultarOtrosClientesPersonaReportada/"+ cdCliente,
		type : 'GET',
		async: true
	});
} 

function consultarDatosPersonaReportada(nuCuenta) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/ConsultaClienteRiesgoService/consultarDatosPersonaReportada/"+ nuCuenta,
		type : 'GET',
		async: false
	});
} 

function consultarDatosOficinaPersonaReportada(cdCliente) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/ConsultaClienteRiesgoService/consultarDatosOficinaPersonaReportada/"+ cdCliente,
		type : 'GET',
		async: false
	});
} 



function abrirDetalleUltimaModificacion(){
	$('#dialogDetalleUltimaModificacion').modal('show');
}