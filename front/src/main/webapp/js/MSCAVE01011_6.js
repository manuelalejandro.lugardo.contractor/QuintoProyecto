/** VARIABLES DE CONTROL **/
var modificar = false;
var acepto = false;
var CD_ST_A = '1';
var CD_ST_I = '0';
var objTmp = new Object();
var CD_USUARIO = $.trim(sessionStorage.getItem("cdUsuario"));
var indicesCliAltRie = '';
var idsChk = ['_nbLiberadoASia', '_nbCorreoPreAnalisis', '_nbCorreoFolioPld', 
                '_nbCalificacion', '_nbSolInfoFiduciario', '_nbRelevantes', 
                '_nbOpis', '_nbListaBbva', '_nbValidarBloqCtas'];

/** Funciones de Datos **/
function borrarDatos() {
	modificar = false;
	acepto = false;
	objTmp = new Object();
	$('#cdTipologia').val('-1');
	$('#cdOficio').val('');
	$('#fhRecepcion').val('');
	$('#fhRecepcion').datepicker('setDate', null);
	$('#tablaPersonaReportada').bootstrapTable({});
	$('#tablaPersonaReportada').bootstrapTable('load',[]);
	indicesCliAltRie='';
	resetVal();
}

function fncModificar(valor){
	acepto = valor;
	validacion();
	if(!valor){
		resetVal();
	}
}

function resetVal(){
	$('#MSCAVE01011 input').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
	$('#MSCAVE01011 select').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
}

/** Funciones de Tablas **/

function mostrarvalores(value){
	var bootTable = $('#tablaPersonaReportada');
	switch(value){
	case 'L3':
		bootTable.bootstrapTable('showColumn','nbClienteBancomer');
		bootTable.bootstrapTable('showColumn','nbSolInfoFiduciario');
		bootTable.bootstrapTable('showColumn','nbRelevantes');
		bootTable.bootstrapTable('showColumn','nbOpis');
		bootTable.bootstrapTable('showColumn','nbListaBbva');
	break;
	case 'LD':
		bootTable.bootstrapTable('hideColumn','nbClienteBancomer');
		bootTable.bootstrapTable('hideColumn','nbSolInfoFiduciario');
		bootTable.bootstrapTable('hideColumn','nbRelevantes');
		bootTable.bootstrapTable('hideColumn','nbOpis');
		bootTable.bootstrapTable('hideColumn','nbListaBbva');
	break;
	}
}

function formatterCheckLiberadaSIA(value, row, index) {
	return creaCtrl(index + idsChk[0], row.nbLiberadoASia, true); 
}

function formatterCheckCorreoPreAnalisis(value, row, index) {
	return creaCtrl(index + idsChk[1],row.nbCorreoPreAnalisis, false);
}

function formatterCheckCorreoFolioPld(value, row, index) {
	return creaCtrl(index + idsChk[2], row.nbCorreoFolioPld, false);
}

function formatterCheckCalificacion(value, row, index) {
	return creaCtrl(index + idsChk[3], row.nbCalificacion, false);
}

function formatterCheckSolInfoFiduciario(value, row, index) {
	return creaCtrl(index + idsChk[4], row.nbSolInfoFiduciario, false); 
}

function formatterCheckRelevantes(value, row, index) {
	return creaCtrl(index + idsChk[5], row.nbRelevantes, false);
}

function formatterCheckOpis(value, row, index) {
	return creaCtrl(index + idsChk[6], row.nbOpis, false); 
}

function formatterCheckListaBbva(value, row, index) {
	return creaCtrl(index + idsChk[7], row.nbListaBbva, false);
}

function formatterCheckValidarBloqCtas(value, row, index) {
	return creaCtrl(index + idsChk[8], row.nbValidarBloqCtas, false);
}

function creaCtrl(idChk, val, disabled){
	var selected = '';
	if (val == '1') {
		selected = 'checked = "true"';
    }
	if(disabled){
		selected += ' disabled';
	}
	return ['<input type="checkbox" onChange="agregaCliente(\'' 
	        + idChk + '\');" name="' + idChk + '" id="' + idChk + '" '
	        + selected + '/><label for="' + idChk + '"></label>'].join('');
}




//|===============================================================|
//|   Validacion de los campos vacios y validos	        	      |
//|===============================================================|
function validarCapturaPersonaReportada(){
	
	$.validator.addMethod("valueNotEquals", function(value, element, arg){
		return arg != value;
	}, "Debe seleccionar una opcion");
	
	$('#MSCAVE01011').validate( {
		rules: {
			cdOficio:{
				maxlength: 20,
				required: true
			},
			cdTipologia:{
				valueNotEquals: -1,
				required: true
			},				
			fhRecepcion: {
				date:false,
				required: true
			}
		},
		messages: {
			cdOficio: {
				
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres.",
						required: 'El campo Numero de Oficio es requerido.'
			},
			cdTipologia: {
				required: 'El campo Tipologia es requerido.',
				valueNotEquals: "Por favor seleccione una opcion"
			},
			fhRecepcion: {
				required: 'El campo Fecha Recepci\u00F3n es requerido.'
			}
        },	
        errorPlacement: function (error, element) {
            $(element).prop('style', 'border: 1px solid #C8175E;');
        },
        success: function (label, element) {
            $(element).prop('style', 'border: 1px solid #86C82D;');
        },	        
        showErrors: function(errorMap, errorList) {
            $.each(this.validElements(), function (index, element) {
                var $element = $(element);
                $(element).prop('style', 'border: 1px solid #86C82D;');
                $element.data("title", "") 
                .tooltip("destroy");
            });
            // Create new tooltips for invalid elements
            $.each(errorList, function (index, error) {
                var $element = $(error.element);
                $element.prop('style', 'border: 1px solid #C8175E;');
                $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
                .data("title", error.message)
                .tooltip();
                     // Create a new tooltip based on the error messsage we just set in the title
            });
        }	  	               
	});
}

//|======================================================================|
//|    Llamada al servicio y paso de objeto con los valores de busqueda  |
//|======================================================================|
function obtenerChecklist(checkList){
	var _type = 'POST';
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/ChecklistService/consultarChecklist/"
	return $.ajax({
		url : _url,
		type : _type,
		contentType: 'application/json',
		dataType: 'JSON',
		data: JSON.stringify(checkList),
		async: false
	});
}
//|===============================================================|
//|    Creacion del objeto con los valores de busqueda		      |
//|    Invocacion de la funcion y llenado de la tabla		      |
//|===============================================================|
function crearObjetoPR(){
	var checkList = new Object();

	checkList.cdTipologia = $('#cdTipologia').val();
	checkList.cdOficio = $('#cdOficio').val();
	checkList.fhRecepcion = $('#fhRecepcion').datepicker('getDate');
    
	obtenerChecklist(checkList).done(function(dataCheck){
		if(cargarMensajeError(dataCheck, false)){
			
			if(checkList.cdTipologia == "L3"){
				$('#tablaL3').show();
				llenadoTablaL3(dataCheck.response);
			}else{
				$('#tablaLD').show();
				llenadoTablaLD(dataCheck.response);
			}
			
		}
	});
}
//|===============================================================|
//|   Envio de parametros de busqueda, Validacion de los campos,  |
//|	  hace la llamada a la funcion para crear un objeto con los   |
//|	  valores obtenidos											  |
//|===============================================================|
$("#btnBuscar").click(function() {
	if(!$('#MSCAVE01011').valid()){
	}else{
		return crearObjetoPR();
	}
});
//|===============================================================|
//|   Llenado de la tabla con los valores de la busqueda          |
//|===============================================================|
function llenadoTablaL3(data) {

	$('#tablaLD').hide();
	$('#tablaPersonaReportadaL3').bootstrapTable('load',data);
	
	 $('#tablaPersonaReportadaL3').bootstrapTable({
			pagination: true,
			pageSize: 5,
			pageList: [5, 25, 50],
	        data: data,
	        clickToSelect: false,
	        singleSelect: false,
	        maintainSelected: true,
	        sortable: true,
	        checkboxHeader: false,
	        formatShowingRows: function (pageFrom, pageTo, totalRows) {
	            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
	        },
	        formatRecordsPerPage: function (pageNumber) {
	        	return pageNumber + ' registros por p\u00e1gina';
	        },
	        formatLoadingMessage: function () {
	        	return 'Cargando, espere por favor...';
	        },
	        formatSearch: function () {
	        	return 'Buscar';
	        },
	        formatNoMatches: function () {
	        	return 'No se encontr&oacute; informaci&oacute;n';
	        }      
	    });
}

function llenadoTablaLD(data) {
	
	$('#tablaL3').hide();
	$('#tablaPersonaReportadaLD').bootstrapTable('load',data);
	
	 $('#tablaPersonaReportadaLD').bootstrapTable({
			pagination: true,
			pageSize: 5,
			pageList: [5, 25, 50],
	        data: data,
	        clickToSelect: false,
	        singleSelect: false,
	        maintainSelected: true,
	        sortable: true,
	        checkboxHeader: false,
	        formatShowingRows: function (pageFrom, pageTo, totalRows) {
	            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
	        },
	        formatRecordsPerPage: function (pageNumber) {
	        	return pageNumber + ' registros por p\u00e1gina';
	        },
	        formatLoadingMessage: function () {
	        	return 'Cargando, espere por favor...';
	        },
	        formatSearch: function () {
	        	return 'Buscar';
	        },
	        formatNoMatches: function () {
	        	return 'No se encontr&oacute; informaci&oacute;n';
	        }      
	    });
}



function actualizaChecklists(checklistsDTO){
	var _type = 'POST';
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/ChecklistService/modificaChecklists"
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(checklistsDTO)
	});
}

(function($, window, document) {
	$(function() {


		
		$('.date').datepicker({
		    language: 'es',
			format : "dd/mm/yyyy",
			autoclose: true,
			orientation: 'top'
		});
		
		$('.money').maskMoney({
			prefix: '$',
			precision: 2
		});
		
		validarCapturaPersonaReportada();
		borrarDatos();
		$('#tablaL3').hide();
		$('#tablaLD').hide();
		
		
	});
}(window.jQuery, window, document));



function validacionBusqueda(){
	var busquedaValida = $('#MSCAVE01011').valid(); 
	if(busquedaValida){
		obtenerChecklist(creaBusquedaDTO()).done(function (data){
			if (cargarMensaje(data, false)) {
				$('#tablaPersonaReportada').bootstrapTable('load',data.response);
			}else{
				$('#tablaPersonaReportada').bootstrapTable('load',[]);
			}
		});
		mostrarvalores($('#cdTipologia').val());
	}
}

function validacion(){
	actualizaChecklists(generaListaClientes('tablaPersonaReportada')).done(function (data){
		if (cargarMensaje(data, true)) {
			obtenerChecklist(creaBusquedaDTO()).done(function (data){
				if (cargarMensaje(data, false)) {
					$('#tablaPersonaReportada').bootstrapTable('load',data.response);
				}else{
					$('#tablaPersonaReportada').bootstrapTable('load',[]);
				}
			});
		}
	});
}

function creaBusquedaDTO(){
	var busquedaDTO = new Object();
	if($('#fhRecepcion').datepicker('getDate') != null){
		busquedaDTO.fhRecepcion = $('#fhRecepcion').datepicker('getDate');
	}
	if($('#cdOficio').val().length > 0){
		busquedaDTO.cdOficio = $('#cdOficio').val();
	}
	if($('#cdTipologia').val() != '-1'){
		busquedaDTO.cdTipologia = $('#cdTipologia').val();
	}
	return busquedaDTO;
}

function agregaCliente(cdCliAltRie){
	var indiceCli = cdCliAltRie.split('_');
	if(indicesCliAltRie.indexOf(indiceCli[0]) == -1){
		indicesCliAltRie += '_' + indiceCli[0];
	}
}

function generaListaClientes(idTblCli){
	var indices = indicesCliAltRie.split('_');
	var lstCliAltRie = [];
	var bootTable = $('#' + idTblCli).bootstrapTable('getData');
	$.each(indices, function(index, rowCliente){
		if(rowCliente.length >0){
			var clienteDTO = new Object();
			var row = bootTable[rowCliente];
			clienteDTO.nuId = row.nuId;
			clienteDTO.cdChecklist = row.cdChecklist;
			clienteDTO.cdCliAltRie = row.cdCliAltRie;
			clienteDTO.cdTipologia = row.cdTipologia;
			clienteDTO.cdUsrAlta = CD_USUARIO; 
			$.each(idsChk, function(idx,idChk){
				switch(idx){
				case 0:
					clienteDTO.nbLiberadoASia = getcheckedValue(rowCliente + idChk);
				break;
				case 1:
					clienteDTO.nbCorreoPreAnalisis = getcheckedValue(rowCliente + idChk);
				break;
				case 2:
					clienteDTO.nbCorreoFolioPld = getcheckedValue(rowCliente + idChk);
				break;
				case 3:
					clienteDTO.nbCalificacion = getcheckedValue(rowCliente + idChk);
				break;
				case 4:
					clienteDTO.nbSolInfoFiduciario = getcheckedValue(rowCliente + idChk);
				break;
				case 5:
					clienteDTO.nbRelevantes = getcheckedValue(rowCliente + idChk);
				break;
				case 6:
					clienteDTO.nbOpis = getcheckedValue(rowCliente + idChk);
				break;
				case 7:
					clienteDTO.nbListaBbva = getcheckedValue(rowCliente + idChk);
				break;
				case 8:
					clienteDTO.nbValidarBloqCtas = getcheckedValue(rowCliente + idChk);
				break;
				}
			});
			lstCliAltRie.push(clienteDTO);
		}
	});
	return lstCliAltRie;
}

function getcheckedValue(idChk){
	var bndCliente = '';
	if($('#' + idChk).is(':checked')){
		bndCliente = '1';
	}else{
		bndCliente = '0'
	}
	return bndCliente;
}