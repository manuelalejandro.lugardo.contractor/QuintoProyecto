

(function($, window, document) {
	$(function() {	
		$('.date').datepicker({
		    language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true
		});
		
		$('.money').maskMoney({precision:2}); 
		
		MovimientosClienteRiesgo(sessionStorage.nuCuenta).done(function(data) { 
			if(cargarMensaje(data,false)){
				llenadoTablaMovimientos(data.response);
			}else{
				llenadoTablaMovimientos([]);
			}
		});	
	});
}(window.jQuery, window, document));

function llenadoTablaMovimientos(data) {
	$('#tablamovimientos').bootstrapTable({
		pagination: false,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function MovimientosClienteRiesgo(nuCuenta) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/MovimientosClienteRiesgo/"+ nuCuenta,
		type : 'GET',
		async: true
	});
} 

function MovimientosClienteRiesgoFiltro(nuCuenta,data) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/MovimientosClienteRiesgoFiltro/"+ nuCuenta,
		type : 'POST',
		contentType:'application/json',
		dataType: 'JSON',
		data: JSON.stringify(data),
		async: false
	});
} 

function getFormularioBusquedaMovimiento(){
	var formBusqueda = new Object();
	
	if($('#imMovimientoDesde').val() != ""){
		formBusqueda.imMovimientoDesde = $('#imMovimientoDesde').val().replace(/,/g,'');
	}
	if($('#imMovimientoHasta').val() != ""){
		formBusqueda.imMovimientoHasta = $('#imMovimientoHasta').val().replace(/,/g,'');
	}
	if($('#fhMovimientoDel').val() != ""){
		formBusqueda.fhMovimientoDel = $('#fhMovimientoDel').datepicker('getDate');
	}
	if($('#fhMovimientoAl').val() != ""){
		formBusqueda.fhMovimientoAl = $('#fhMovimientoAl').datepicker('getDate');
	}
	return formBusqueda;
}

$("#btnBuscar").click(function(){
	MovimientosClienteRiesgoFiltro(sessionStorage.nuCuenta,getFormularioBusquedaMovimiento()).done(function(data) { 
		if(cargarMensaje(data,false)){
			$('#tablamovimientos').bootstrapTable('load',data.response);
		}else{
			$('#tablamovimientos').bootstrapTable('load',[]);
		}
	});	
});

function borrarDatos() {
	$('#imMovimientoDesde').val("");
	$('#imMovimientoHasta').val("");
	$('#fhMovimientoDel').val("");
	$('#fhMovimientoAl').val("");

}

function abrirModalConfirm(){
	$('#dialogComfirm').modal('show');
}