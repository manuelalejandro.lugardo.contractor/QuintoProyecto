(function($, window, document) {
	
	$(function() {		
		llenaDatosGenerales(sessionStorage.cdSistema, sessionStorage.nuFolioAlerta).done(function(data) {
			if(cargarMensajeError(data,false)){ 
				if(data.response.nbTelParCR2 != null && data.response.nbTelParCR2 != "" && data.response.nbTelParCR2 != undefined){
					data.response.nbTelParCR2 = data.response.nbTelParCR2.replace(/(\d{3})(\d{7})(\d{4})/, '($1) $2 $3');
				}
				
				if(data.response.nbTelOfiCR2 != null && data.response.nbTelOfiCR2 != "" && data.response.nbTelOfiCR2 != undefined){
					data.response.nbTelOfiCR2 = data.response.nbTelOfiCR2.replace(/(\d{3})(\d{7})(\d{4})/, '($1) $2 $3');
				}
				
				llenarFormulario(data.response,'datosGenerales');
			}});
		 llenaUnidadGestora(sessionStorage.nuFolioAlerta, sessionStorage.cdSistema).done(function(data) {
				if(cargarMensajeError(data,false)){ 
					llenarFormulario(data.response,'oficinaGestora');
				}}); 
		 
		 llenaOtrosClientes(sessionStorage.cdCaso).done(function(data) {
				if(cargarMensajeError(data,false)){ 
					llenadoTablaOtrosClientes(data.response);
				}});  
		//llenadoTablaDetalleUltimaModificacion(data1.response);
	});
}(window.jQuery, window, document));

function llenadoTablaOtrosClientes(data) {
	$('#tablaOtrosClientes').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaDetalleUltimaModificacion(data) {
	$('#tablaDetalleUltimaModificacion').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function abrirDetalleUltimaModificacion(){
	$('#dialogDetalleUltimaModificacion').modal('show');
}

function llenaDatosGenerales(cdSistema,nuFolioAlerta) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarDatosGeneralesClienteReportado2/"+ cdSistema + "/" + nuFolioAlerta,
		type : "GET",
		async: false
	});
}

function llenaUnidadGestora(nuFolioAlerta,cdSistema) { 
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarIdentificacionReportes2/"+ nuFolioAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}


function llenaOtrosClientes(cdCaso) { 
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultaOtrosClientesCaso/"+cdCaso,
		type : "GET",
		async: false
	});
}
 