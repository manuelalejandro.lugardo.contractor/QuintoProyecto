(function($, window, document) {
	$(function() {		
  
 		datosLog(sessionStorage.cdCaso, sessionStorage.cdCliente).done(function(data) {  
				llenadoTablaLog(data.response); 
		}); 
	});
}(window.jQuery, window, document));


function datosLog(cdCaso,cdUsuario) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarLogAnalisis/"+cdCaso+ "/" + cdUsuario,
		type : "GET",
		async: false
	});
}

function llenadoTablaLog(data){ 
	$('#tablaLog').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [20, 30, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    }); 
}

