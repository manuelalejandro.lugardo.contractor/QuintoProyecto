var encender;
var parametros;
var filtro="";
var formBusqueda = '';

var actionEvents = { 
    'click .detailAlerta': function (e, value, row, index) {
    	sessionStorage.nuFolioAlerta = row.nuFolioAlerta;
    	sessionStorage.cdSistema = row.cdSistema;
    	sessionStorage.nuCuenta = row.nuCuenta;
    	$('#MSCAVE01002').load('MSCAVE01002.html', function (response, status, xhr){
            if (status == "success") {
                $(this).modal('show');
            }else{
            	data.cdMensaje = 0;
            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
            	cargarMensaje(data,false);
            }
        });
    },
    'click .detailCaso': function (e, value, row, index) {
    	
    	sessionStorage.cdCaso = row.cdCaso;
    	sessionStorage.nuFolioAlerta = row.nuFolioAlerta;
    	sessionStorage.cdDivisa = row.cdDivisa;
    	sessionStorage.cdTipologia = row.cdTipologia;
    	sessionStorage.cdSistema = row.cdSistema;
    	sessionStorage.cdCliente = row.nuCliente;
    	sessionStorage.pantalla = ('MSCAVE01006');
    	cargarContenido('MSCAVE01007','Gesti&oacute;n de Alertas > Analisis de Casos');
    },
    'click .dictamenPreliminar': function (e, value, row, index) {
    	sessionStorage.cdCaso = row.cdCaso;
    	sessionStorage.cdSistema = row.cdSistema;
    	sessionStorage.opDictamen = 'P';//Preliminar
    	abrirModalDictamen();
    }
};

(function($, window, document) {

	 
	
	$(function() {

		$('.date').datepicker({
		    language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true
		});
		llenadoTablaCasosAsignados();
		
		
		encender=0;
		parametros=0;
		$('.moneda').maskMoney({precision:2}); 
	
		// Validar Numero de Cuenta
		$.validator.addMethod('formatoNumeroCuenta', function(value, element){
			var valor = value.toUpperCase();
			var tamanio = 0;
			if( valor != null && valor != '')
			{
				tamanio = valor.length;
				if( tamanio > 0 && tamanio < 10)
				{
					return false;
				}
				else if( tamanio > 10 && tamanio < 20 )
				{
					return false;
				}
				else{
					return true;
				}
			}
			else if( valor == null || valor == ''){
				return true;
			}
		}, "error");

		jQuery.validator.addMethod("isValidateRangoFechas", function(value, element) {
			return validarElementos( element.name );
			
		}, "error");

		jQuery.validator.addMethod("okValidator", function(value, element, param) {
			  return true;
		}, "error");

		validacionFormularioBusquedaCasos();
		 
	});
	

	 
}(window.jQuery, window, document));

function llenadoTablaCasosAsignados() {
	$('#tablaCasosAsignados').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        //data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sidePagination: 'server',
        url: obtenerDeSessionStorage("baseUrl", true) + "/CasosPendientesService/consultaCasosAsignadosConsultor/"+sessionStorage.cdUsuario,
        queryParams: 'queryParams',
        responseHandler : "responseHandler",
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function queryParams(params) {
    return {
        limit: params.limit,
        offset: params.offset,
        name:  params.name,
        order: params.order
    };
}

function responseHandler(res) {
	var datos = new Object();
	datos.rows = res.response.rows;
	datos.total = res.response.total;
    return datos;
}
function formatterDetailCasosAsignados(value, row, index){
	var nbSistema=row.nbSistema==null?"":row.nbSistema;
	var nbSegmento=row.nbSegmento==null?"":row.nbSegmento;
	var nbPrioriAsig=row.nbPrioriAsig==null?"":row.nbPrioriAsig;
	var nbTpPersona=row.nbTpPersona==null?"":row.nbTpPersona;
	var cdSesion=row.cdSesion==null?"":row.cdSesion;
	var nuScoreMc=row.nuScoreMc==null?"":row.nuScoreMc;
	var nuScoreMdl=row.nuScoreMdl==null?"":row.nuScoreMdl;
	var nbRespuesta=row.nbRespuesta==null?"":row.nbRespuesta;
	var imAlerta=row.imAlerta==null?"":formatterMoney(row.imAlerta);
	var fhAsignacion=row.fhAsignacion==null?"":formatterDate(row.fhAsignacion);
	var fhEnvio=row.fhEnvio==null?"":formatterDate(row.fhEnvio);
	var fhAlerta=row.fhAlerta==null?"":formatterDate(row.fhAlerta);

	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Sistema Origen:</b> '+nbSistema+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Segmento PLD:</b> '+nbSegmento+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha de Asignaci&oacute;n:</b> '+fhAsignacion+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Prioridad:</b> '+nbPrioriAsig+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Tipo Persona:</b> '+nbTpPersona+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Respuesta Funcionario:</b> '+nbRespuesta+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha Env&iacute;o:</b> '+fhEnvio+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha Alerta:</b> '+fhAlerta+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Sesi&oacute;n:</b> '+cdSesion+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Importe:</b> '+imAlerta+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Calificaci&oacute;n Modelo:</b> '+nuScoreMdl+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Score Matriz:</b> '+nuScoreMc+'</td></tr>'+
	        '</table>'].join('');
}

function checkFormatterCasosAsignados(value, row, index) {
	var selected = '';
	if (row.stCasoAsignado) {
		selected = 'selected';
    } else {
    	selected = '';
    }
	return ['<input type="checkbox" onChange="validarCasosAsignados();" name="checkCasoAsignado" id="CA_'+row.nuFolioAlerta+'_'+row.cdSistema+'" value="'+row.nuFolioAlerta+'_'+row.cdSistema+'" '+selected+'/><label for="CA_'+row.nuFolioAlerta+'_'+row.cdSistema+'" ></label>'].join(''); 
}

function checkAll(nameCheck,obj){
	$('input[name="'+nameCheck+'"]').each(function () {
			$(this).removeAttr('checked');
			$(this).prop('checked',obj.checked);
	});
}

function validarCasosAsignados(){
	var numCasos = 0;
	$('input[name="checkCasoAsignado"]').each(function () {
		if($(this).prop('checked')){
			numCasos++;
		}
	});
	
	if(numCasos > 0){
		$("#btnDictamen").prop("disabled", false);
	}else{
		$("#btnDictamen").prop("disabled", true);
	}
}

function formatterRegistroCasosAsignados(value, row, index) {
	return [
            '<a class="detailAlerta" href="javascript:void(0)" title="Detalle de Alerta">',
            '<i class="tabla_registro_detalle">.</i>',
            '</a>',
            '<a class="detailCaso" href="javascript:void(0)" title="An&aacute;lisis del Caso">',
            '<i class="tabla_registro_detalle_caso">.</i>',
            '</a>',
            '<a class="dictamenPreliminar" href="javascript:void(0)" title="Dictamen Preliminar">',
            '<i class="tabla_registro_detalle_dictamen">.</i>',
            '</a>'
    ].join(''); 
}

function abrirModalDictamen(){
	
	$('#MSCAVE01008').load('MSCAVE01008.html', function (response, status, xhr, elemento){
		if (status == "success") {
	        $(this).modal('show');
	    }else{
	    	data.cdMensaje = 0;
	    	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	    	cargarMensaje(data,false);
	    } 
	});
}

function consultarCatalogoSegmento() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoSegmento",
		type : "GET",
		async: true
	});
}

function consultarCatalogoTipoPersona() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoTipoPersona",
		type : "GET",
		async: true
	});
}

function consultarCatalogoTipologia() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoTipologia",
		type : "GET",
		async: true
	});
}

function consultarCatalogoSesion() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoSesion",
		type : "GET",
		async: true
	});
}

function consultarCatalogoEscenarios() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoEscenarios",
		type : "GET",
		async: true
	});
}

function consultarCatalogoSegmento() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoSegmento",
		type : "GET",
		async: true
	});
}

function consultarCatalogoRespuesta() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoRespuesta",
		type : "GET",
		async: true
	});
}

function consultarCatalogoTipologiaGerencia(cdGerencia) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoTipologiaGerencia/"+cdGerencia,
		type : "GET",
		async: true
	});
}

function getFormularioBusquedaCaso(){
	
	
	formBusqueda = $('#nuCaso').val();
	if($('#nuFolio').val()!= ""){	
		formBusqueda +='|'+ $('#nuFolio').val();	
	}else{
		formBusqueda +='| '
	}
	if($('#numCliente').val()!= ""){	
		formBusqueda +='|'+ $('#numCliente').val();	
	}else{
		formBusqueda +='| '
	}	
	if($('#nomCliente').val()!= ""){	
		formBusqueda +='|'+ $('#nomCliente').val();	
	}else{
		formBusqueda +='| '
	}
	if($('#numCuenta').val()!= ""){	
		formBusqueda +='|'+ $('#numCuenta').val();	
	}else{
		formBusqueda +='| '
	}
	if($('#cdTPersona').val()!= "-1"){
		formBusqueda +='|'+ $('#cdTPersona').val();
	} else{
		formBusqueda +='| ';
	}
	
	if($('#cmdTipologia').val()!= "-1"){
		formBusqueda +='|'+ $('#cmdTipologia').val();
	} else{
		formBusqueda +='| ';
	}
	
	if($('#cdSegmento').val()!= "-1"){
		 formBusqueda +='|'+$('#cdSegmento').val();
	}else{
		formBusqueda +='| ';
	}
	
	if($('#cdSesion').val()!= "-1"){
		formBusqueda +='|'+ $('#cdSesion').val();
	} else{
		formBusqueda +='| ';
	}
	
	if($('#cdRespuesta').val()!= "-1"){
		formBusqueda +='|'+ $('#cdRespuesta').val();	
	} else{
		formBusqueda +='| ';
	}
	if($('#cdEscenario').val()!= "-1"){
		formBusqueda +='|'+ $('#cdEscenario').val();
	} else{
		formBusqueda +='| ';
	}	
	if($('#nuCalificacion').val()!= ""){
		formBusqueda +='|'+ $('#cdCalificacion option:selected').text() +','+ $('#nuCalificacion').val();
	}else{
		formBusqueda +='| ' + $('#nuCalificacion').val();
	}
	if( $('#nuPtsMatriz').val()!= ""){
		formBusqueda +='|'+ $('#cdPtsMatriz option:selected').text() +','+ $('#nuPtsMatriz').val();
	}else{
		formBusqueda +='| '+ $('#nuPtsMatriz').val();
	}
	if( $('#imImporte').val() != ""){
		var importe = replaceAll($('#imImporte').val() ,",", "" );
		formBusqueda +='|'+ $('#cdImporte option:selected').text() +','+  importe;
	}else{
		formBusqueda +='| '+ importe ;
	}
	
		
	
	formBusqueda +='| '+ ($('#fhAlertaDel').datepicker('getDate')==null?'':$('#fhAlertaDel').datepicker('getDate'));
	formBusqueda +='| '+ ($('#fhAlertaAl').datepicker('getDate')==null?'':$('#fhAlertaAl').datepicker('getDate'));
	formBusqueda +='| '+ ($('#fhEnvioDel').datepicker('getDate')==null?'':$('#fhEnvioDel').datepicker('getDate'));	
	formBusqueda +='| '+ ($('#fhEnvioAl').datepicker('getDate')==null?'':$('#fhEnvioAl').datepicker('getDate'));	
	formBusqueda +='| '+ ($('#fhVencRevisionDel').datepicker('getDate')==null?'':$('#fhVencRevisionDel').datepicker('getDate'));		
	formBusqueda +='| '+ ($('#fhVencRevisionAl').datepicker('getDate')==null?'':$('#fhVencRevisionAl').datepicker('getDate'));
	formBusqueda +='| '+ ($('#fhVencLiberacionDel').datepicker('getDate')==null?'':$('#fhVencLiberacionDel').datepicker('getDate'));	
	formBusqueda +='| '+ ($('#fhVencLiberacionAl').datepicker('getDate')==null?'':$('#fhVencLiberacionAl').datepicker('getDate'));
	formBusqueda +='| '+ ($('#fhVencReporteDel').datepicker('getDate')==null?'':$('#fhVencReporteDel').datepicker('getDate'));	
	formBusqueda +='| '+ ($('#fhVencReporteAl').datepicker('getDate')==null?'':$('#fhVencReporteAl').datepicker('getDate'));	
	formBusqueda +='| '+ ($('#fhAsignacionDel').datepicker('getDate')==null?'':$('#fhAsignacionDel').datepicker('getDate'));
	formBusqueda +='| '+ ($('#fhAsignacionAl').datepicker('getDate')==null?'':$('#fhAsignacionAl').datepicker('getDate'));				
	
	return formBusqueda;	
}


$("#btnBuscar").click(function(){
	
	filtro = '?search='+getFormularioBusquedaCaso();
	var isValid = $('#MSCAVE01006').valid();
	if( isValid )
	{
	
		$('#tablaCasosAsignados').bootstrapTable('refresh', {
			url: obtenerDeSessionStorage("baseUrl", true) + "/CasosPendientesService/consultaCasosAsignadosConsultor/"+sessionStorage.cdUsuario+filtro
		});

	}
});

$("#btnLimpiar").click(function(){
	$('#tablaCasosAsignados').bootstrapTable('refresh', {
		url: obtenerDeSessionStorage("baseUrl", true) + "/CasosPendientesService/consultaCasosAsignadosConsultor/"+sessionStorage.cdUsuario
	});	

});

function borrarDatos() {
	$('#nuCaso').val("");
	$('#nuFolio').val("");
	$('#numCliente').val("");
	$('#nomCliente').val("");
	$('#numCuenta').val("");
	$('#cdTPersona').val("-1");
	$('#cmdTipologia').val("-1");
	$('#cdSegmento').val("-1");
	$('#cdSesion').val("-1");
	$('#cdRespuesta').val("-1");
	$('#cdEscenario').val("-1");
	$('#nuPtsMatriz').val("");
	$('#nuCalificacion').val("");
	$('#imImporte').val("");
	$('#fhAlertaDel').val("");
	$('#fhAlertaDel').datepicker('setDate', null);
	$('#fhAlertaAl').val("");
	$('#fhAlertaAl').datepicker('setDate', null);
	$('#fhEnvioDel').val("");
	$('#fhEnvioDel').datepicker('setDate', null);
	$('#fhEnvioAl').val("");
	$('#fhEnvioAl').datepicker('setDate', null);
	$('#fhVencRevisionDel').val("");
	$('#fhVencRevisionDel').datepicker('setDate', null);
	$('#fhVencRevisionAl').val("");
	$('#fhVencRevisionAl').datepicker('setDate', null);
	$('#fhVencLiberacionDel').val("");
	$('#fhVencLiberacionDel').datepicker('setDate', null);
	$('#fhVencLiberacionAl').val("");
	$('#fhVencLiberacionAl').datepicker('setDate', null);
	$('#fhVencReporteDel').val("");
	$('#fhVencReporteDel').datepicker('setDate', null);
	$('#fhVencReporteAl').val("");
	$('#fhVencReporteAl').datepicker('setDate', null);
	$('#fhVencRevisionDel').val("");
	$('#fhVencRevisionDel').datepicker('setDate', null);
	$('#fhVencRevisionAl').val("");
	$('#fhVencRevisionAl').datepicker('setDate', null);
	$('#fhAsignacionDel').val("");
	$('#fhAsignacionDel').datepicker('setDate', null);
	$('#fhAsignacionAl').val("");
	$('#fhAsignacionAl').datepicker('setDate', null);
	
	jQuery.validator.addMethod("isValidateRangoFechas", function(value, element) {
		if(value =! null && value != ''){
		return validarElementos( element.name );
		}
		else {
			return true;
			
		}
	}, "error");
}

function Mostrar(){
	if( encender==0){
		$('#btnLimpiar').show();
		$('#btnBuscar').show();
	encender = 1;
}else if( encender==1){
	$('#btnLimpiar').hide();
	$('#btnBuscar').hide();
		encender = 0;
	}	
}

function validacionFormularioBusquedaCasos(){
	$('#MSCAVE01006').validate( {
		rules: {
			nuCaso: {
				required: false,
				digits: true,
				maxlength:10
			},
			nuFolio: {
				required: false,
				digits: true,
				maxlength:10
			},
			numCliente: {
				required: false,
				digits: false,
				maxlength:8
			},
			nomCliente: {
				required: false,
				okValidator: true
			},			
			numCuenta: {
				required: false,
				formatoNumeroCuenta:true
			},
			cmdTipologia: {
				required: false,
				okValidator: true
			},
			cdSegmento: {
				required: false,
				okValidator: true
			},
			
			nuCalificacion: {
				required: false,
				digits: false,
				maxlength:10
			},
			nuPtsMatriz: {
				required: false,
				digits: false,
				maxlength:10
			},
			cdRespuesta: {
				required: false,
				okValidator: true				
			},
			cdSesion: {
				required: false,
				okValidator: true				
			},
			cdEscenario: {
				required: false,
				okValidator: true				
			},
			imImporte: {
				required: false,
				okValidator: true
			},						
			fhAlertaDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhAlertaAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhEnvioDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhEnvioAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhVencRevisionDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhVencRevisionAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},			
			fhAsignacionDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhAsignacionAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},		
			fhVencLiberacionDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhVencLiberacionAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},	
			fhVencReporteDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhVencReporteAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
		},
		errorPlacement: function (error, element) {
			$(element).prop('style', 'border: 1px solid #C8175E;');
		},
		success: function (label, element) {
			$(element).prop('style', 'border: 1px solid #86C82D;');
		}
	});

}

function validarElementos( nombreElemento )
{
	var isValid = false;

	if( nombreElemento == 'fhAlertaDel' || nombreElemento == 'fhAlertaAl' )
	{
		isValid = validaCamposFecha( $("#fhAlertaDel").datepicker('getDate'), $("#fhAlertaAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhEnvioDel' || nombreElemento == 'fhEnvioAl' )
	{
		isValid = validaCamposFecha( $("#fhEnvioDel").datepicker('getDate'), $("#fhEnvioAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhVencRevisionDel' || nombreElemento == 'fhVencRevisionAl' )
	{
		isValid = validaCamposFecha( $("#fhVencRevisionDel").datepicker('getDate'), $("#fhVencRevisionAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhAsignacionDel' || nombreElemento == 'fhAsignacionAl' )
	{
		isValid = validaCamposFecha( $("#fhAsignacionDel").datepicker('getDate'), $("#fhAsignacionAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhVencLiberacionDel' || nombreElemento == 'fhVencLiberacionAl' )
	{
		isValid = validaCamposFecha( $("#fhVencLiberacionDel").datepicker('getDate'), $("#fhVencLiberacionAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhLiberacionDel' || nombreElemento == 'fhLiberacionAl' )
	{
		isValid = validaCamposFecha( $("#fhLiberacionDel").datepicker('getDate'), $("#fhLiberacionAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhVencReporteDel' || nombreElemento == 'fhVencReporteAl' )
	{
		isValid = validaCamposFecha( $("#fhVencReporteDel").datepicker('getDate'), $("#fhVencReporteAl").datepicker('getDate') );
	}		
	return isValid;
}


function invocarServicios(){
	if(parametros==0){
		consultarCatalogoTipoPersona().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdTPersona").html('');
			    $("#cdTPersona").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdTPersona").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdTPersona").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
			
		consultarCatalogoSesion().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdSesion").html('');
			    $("#cdSesion").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdSesion").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdSesion").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoEscenarios().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdEscenario").html('');
			    $("#cdEscenario").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdEscenario").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdEscenario").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoSegmento().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdSegmento").html('');
			    $("#cdSegmento").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdSegmento").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdSegmento").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoTipologiaGerencia(sessionStorage.cdGerencia).done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cmdTipologia").html('');
			    $("#cmdTipologia").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cmdTipologia").append('<option value="' + val.cdValor + '">'+ val.cdValor+'-'+val.nbValor+ '</option>');
				});
			}else{
				$("#cmdTipologia").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoRespuesta().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdRespuesta").html('');
			    $("#cdRespuesta").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdRespuesta").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdRespuesta").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		
		llenarSelectPorCdCatalogo('10',$('#cdCalificacion'),false);
		llenarSelectPorCdCatalogo('10',$('#cdPtsMatriz'),false);
		llenarSelectPorCdCatalogo('10',$('#cdImporte'),false);	
		parametros =1;
	}
	else if(parametros==1){
		parametros =0;
		
	}
}