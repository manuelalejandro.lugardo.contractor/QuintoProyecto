(function($, window, document) {
	$(function() {	
		$('#nuCaso').html(sessionStorage.cdCaso);
		
		if(sessionStorage.cdPerfil == "1"){
			
			$('#10').attr('disabled','disabled');
			$('#15').removeAttr('disabled');
			$('#1').attr('disabled','disabled');
			$('#6').removeAttr('disabled');
			
		}else 
			{
			
			$('#15').attr('disabled','disabled');
			$('#10').removeAttr('disabled');
			$('#6').attr('disabled','disabled');
			$('#1').removeAttr('disabled');
			
			
			}
		
		if(sessionStorage.cdPerfil == "1"){
			
			$('#10').hide();
			$('#11').hide();
			$('#12').hide();
			$('#13').hide();
			$('#14').hide();
			$('#15').show();
			$('#16').hide();
			loadPanel('MSCAVE01012_6');
		
		}else{
			$('#10').show();
			$('#11').show();
			$('#12').show();
			$('#13').show();
			$('#14').show();
			$('#15').show();
			$('#16').show();
			loadPanel('MSCAVE01012_1');
		}
		
		
		

	
		
	});
}(window.jQuery, window, document));

function emptyPanel(){
	
		
		$('#MSCAVE01012_1').empty();
		$('#MSCAVE01012_2').empty();
		$('#MSCAVE01012_3').empty();
		$('#MSCAVE01012_4').empty();
		$('#MSCAVE01012_5').empty();
		$('#MSCAVE01012_7').empty();
		$('#MSCAVE01012_6').empty();

}
function loadPanel(idPanel){
	emptyPanel();
	$('#'+idPanel).load(idPanel+'.html', function (response, status, xhr){
		
	    if (status != "success") {
	    	data.cdMensaje = 0;
	    	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	    	cargarMensaje(data,false);
	    }
	});
}



var widthOfList = function(){
  var itemsWidth = 0;
  $('.list li').each(function(){
    var itemWidth = $(this).outerWidth();
    itemsWidth+=itemWidth;
  });
  return itemsWidth;
};

var widthOfHidden = function(){
  return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
};

var getLeftPosi = function(){
  return $('.list').position().left;
};

var reAdjust = function(){
  if (($('.wrapper').outerWidth()) < widthOfList()) {
    $('.scroller-right').show();
  }
  else {
    $('.scroller-right').hide();
  }
  
  if (getLeftPosi()<0) {
    $('.scroller-left').show();
  }
  else {
    $('.item').animate({left:"-="+getLeftPosi()+"px"},'slow');
  	$('.scroller-left').hide();
  }
}

reAdjust();

$(window).on('resize',function(e){  
  	reAdjust();
});

$('.scroller-right').click(function() {
  
  $('.scroller-left').fadeIn('slow');
  $('.scroller-right').fadeOut('slow');
  
  $('.list').animate({left:"+="+(-685)+"px"},'slow',function(){

  });
});

$('.scroller-left').click(function() {
  
	$('.scroller-right').fadeIn('slow');
	$('.scroller-left').fadeOut('slow');
  
  	$('.list').animate({left:"-="+(-685)+"px"},'slow',function(){
  	
  	});
});    