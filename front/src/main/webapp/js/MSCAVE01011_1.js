var lstClientesAgregados = new Array();
var objTmpQuitar;
var clienteReportado;
var indexCteRep;

var CD_TP_PERSONA_FISICA = 'F';
var CD_TP_PERSONA_MORAL = 'M';
var CD_TP_PERSONA_GOBIERNO = 'G';

var actionEventsCheckCliente = {
	    'click .check': function (e, value, row, index) {
	    	
	    	//console.log( "== Evento Check ==" );
	    	//console.log( "Indice : " + index + " | Estatus : " + row.stClienteLoc );
	    	
	    	// Agrega el cliente localizado
	    	if( row.stClienteLoc == 0){
	    		row.stClienteLoc = 1;
	    	// Elimina el cliente localizado
	    	}else if( row.stClienteLoc == 1 ){
	    		row.stClienteLoc = 0;
	    	}
    		var datos = $('#tablaLocalizacionClientes').bootstrapTable('getData');
    		$('#tablaLocalizacionClientes').bootstrapTable('load', datos);
	    }   
};

var actionEventsClientesAgregados = { 
	    'click .detailCliente': function (e, value, row, index) {
	    	/*
	    	$('#MSCAVE01011_2').load('MSCAVE01011_2.html', function (response, status, xhr){
	            if (status == "success") {
	                $(this).modal('show');
	            }else{
	            	data.cdMensaje = 0;
	            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	            	cargarMensaje(data,false);
	            }
	        });
	        */
	    },
	    'click .detailDatos': function (e, value, row, index) {
	    	/*
	    	$('#MSCAVE01011_5').load('MSCAVE01011_5.html', function (response, status, xhr){
	            if (status == "success") {
	                $(this).modal('show');
	            }else{
	            	data.cdMensaje = 0;
	            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	            	cargarMensaje(data,false);
	            }
	        });
	        */
	    },
	    'click .delete': function (e, value, row, index) {
	    	//console.log( "== Quitar Registro ==" );
	    	objTmpQuitar = row;
	    	$('#dialogQuitarClienteAgregado').modal('show');
	    }	    
	    
	};

(function($, window, document) {
	$(function() {			
		console.log( "== Load pantalla localizacion del cliente ==" );
		
		$('.date').datepicker({
		    language: 'es',
			format : "dd/mm/yyyy",
			autoclose: true,
			orientation: 'top'
		});
		$.validator.addMethod('tipoPersonaRule', function(value, element){			
			var valor = value.toUpperCase();		
			if (valor == CD_TP_PERSONA_FISICA || valor == CD_TP_PERSONA_MORAL || CD_TP_PERSONA_GOBIERNO) {
				return true;  
			} else {
				return false;   
			};
		},'error');

		clienteReportado = JSON.parse( sessionStorage.getItem( 'clienteReportado' ) );
		console.log( "=============== CLIENTE RECIBIDO ===============" );
		console.log( clienteReportado );
		console.log( "===============" );
		indexCteRep = sessionStorage.getItem( 'indexCteRep' );
		llenarSelectPorTabla('TipoPersona',$('#cdTpPersonaL'),true);
		llenadoTablaLocalizacionClientes([]);
		llenadoTablaClientesAgregados([]);	
		iniciarPantalla(  );
		
	});
}(window.jQuery, window, document));

function llenadoTablaLocalizacionClientes(data) {
	$('#tablaLocalizacionClientes').bootstrapTable({
		pagination: false,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}
function llenadoTablaClientesAgregados(data) {
	$('#tablaClientesAgregados').bootstrapTable({
		pagination: false,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function iniciarPantalla(  ){
	lstClientesAgregados = new Array();
	limpiarFormularioBusqueda();
	deshabilitarCmpFechaNac();
	deshabilitarCmpTpPersona();
	setDatosClienteReportado(  );
}

function setDatosClienteReportado(  ){
	if( clienteReportado.cdTpPersona == 'M'){
		$( "#nbRazonSocial" ).val( clienteReportado.nbNombre );
	}else if( clienteReportado.cdTpPersona == 'F'){
		$( "#nbNombre" ).val( clienteReportado.nbNombre );
	}	
	$( "#nbPaterno" ).val( clienteReportado.nbApPaterno );
	$( "#nbMaterno" ).val( clienteReportado.nbApMaterno );
	$( "#cdRFC" ).val( clienteReportado.cdRFC );
	$( "#nuCliente" ).val( clienteReportado.cdCliente );
	$( "#cdTpPersonaL" ).val( clienteReportado.cdTpPersona );
	var fechaTmp = new Date( clienteReportado.fhNacimiento );
    if( clienteReportado.cdClienteBancomer  == 'SI' )
    {
    	document.getElementById( 'Y' ).checked = true;
    	setClienteBancomerRY();    	    	
    }else if( clienteReportado.cdClienteBancomer  == 'NO' )
    {
    	document.getElementById( 'N' ).checked = true;
    	setClienteBancomerRN();
    }	
    
    $( '#fhNacimientoLoc' ).datepicker( 'setDate', fechaTmp );    
}




function checkFormatterClientesLocalizados(value, row, index) {
	if(row.stClienteLoc == 1){
		return [
	            '<a class="check" href="javascript:void(0)" title="Deseleccionar">',
	            '<i class="tabla_registro_deshabilitar">.</i>',
	            '</a>'
	    ].join('');
	}else{
		return [
	            '<a class="check" href="javascript:void(0)" title="Seleccionado">',
	            '<i class="tabla_registro_habilitar">.</i>',
	            '</a>'		        
	    ].join('');
	}	
	
}


function seleccionarTodos(  ){
	var stCheck = $("#checkAllCasosPorAsignar").is(':checked');
	var lstClientesCheck =$('#tablaLocalizacionClientes').bootstrapTable('getData');
	if( stCheck == true)
	{
		$.each( lstClientesCheck, function(index, val) {
			val.stClienteLoc = 1;
		});			
	}else if( stCheck == false )
	{
		$.each( lstClientesCheck, function(index, val) {
			val.stClienteLoc = 0;
		});					
	}	
	$('#tablaLocalizacionClientes').bootstrapTable('load', lstClientesCheck );	    		
}

function formatterClientesAgregados(value, row, index) {
	return [
            '<a class="delete" href="javascript:void(0)" title="Eliminar">',
            '<i class="tabla_registro_eliminar">&nbsp;</i>',
            '</a>',
            '<a class="detailCliente" href="javascript:void(0)"  title="Cuentas del Cliente">',
            '<i class="tabla_registro_Cuentas_Cliente">.</i>',
            '</a>',
            '<a class="detailDatos" href="javascript:void(0)" title="Datos del Cliente">',
            '<i class="tabla_registro_Datos_Cliente">.</i>',
            '</a>'
    ].join('');
}

function formatterDetailClientesLocalizados(value, row, index){
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Nombre Raz&oacute;n Social</b> '+row.nbNombre+'</td></tr>' +
	        '</table>'].join('');
}


	// ==================== ACCIONES / BOTONES ====================
	
	$('#cdTpPersonaL').change(function (e) {
		
		var cdTipoPersona = $("#cdTpPersonaL").val();
		var rdbEscliente = $('input[name=esCliente]');
		var checkedValue = rdbEscliente.filter(':checked').val();
		
		if( cdTipoPersona == CD_TP_PERSONA_MORAL || cdTipoPersona == CD_TP_PERSONA_GOBIERNO ) 
		{
			$('#nbNombre').val( '' );
			$('#nbPaterno').val( '' );
			$('#nbMaterno').val( '' );
			$('#nbRazonSocial').val( '' );			
			deshabilitarCampoNombre();
			deshabilitarCampoAPaterno();
			deshabilitarCampoAMaterno();
			habilitarCampoRazonSocial();
			if( checkedValue == 'SI' )
			{
				validaFormBusquedaMoral();
			}
			else if( checkedValue == 'NO' )
			{
				validaFormAltaMoral();	
			}									
		}else if( cdTipoPersona == CD_TP_PERSONA_FISICA ){
			habilitarCampoNombre();
			$('#nbNombre').val( '' );
			$('#nbPaterno').val( '' );
			$('#nbMaterno').val( '' );
			$('#nbRazonSocial').val( '' );
			habilitarCampoAPaterno();
			habilitarCampoAMaterno();
			deshabilitarCampoRazonSocial();	
			if( checkedValue == 'SI' )
			{
				validaFormBusquedaFisica();
			}
			else if( checkedValue == 'NO' )
			{
				validaFormAltaFisica();	
			}									
		}
	});
	
	$('#rdbCteBancomer').change(function (e) {
		var rdbCteBancomer = $('input[name=rdbCteBancomer]');
		var checkedValueTp = rdbCteBancomer.filter(':checked').val();
	});	
	
	$('#btnLimpiarLC').click(function (e) {
		limpiarFormularioBusqueda();
	});	
	
	$('#btnBuscarLC').click(function (e) {
		lstClientesAgregados = new Array();
		var cliente = crearObjetoClienteLoc();
		consultarClientesLoc( cliente ).done(function(data) {
			$('#tablaLocalizacionClientes').bootstrapTable('load',data.response);
		});	
		
		$('#tablaClientesAgregados').bootstrapTable('load',[]);
		var stCheck = $("#checkAllCasosPorAsignar").is(':checked');
		if( stCheck == true ){
			$('#checkAllCasosPorAsignar').prop('checked', false );
		}
	});
	
	$('#btnAddClienteLoc').click(function (e) {
		var lstClientesLocalizados =$('#tablaLocalizacionClientes').bootstrapTable('getData');
		lstClientesAgregados = $('#tablaClientesAgregados').bootstrapTable('getData');
		$.each( lstClientesLocalizados, function(index, val) {
			if( val.stClienteLoc == 1 ){
				lstClientesAgregados.push( val );				
			}			
		});	
		
		// Eliminar Clientes Agregados de Localizados
		$.each( lstClientesAgregados, function(index, val) {
			var indexLigados = lstClientesLocalizados.indexOf( val );
			if( indexLigados != -1) {
				lstClientesLocalizados.splice( indexLigados, 1);
			}			
		});		
		
		// Total de Clientes localizados
		//console.log( "== TABLA DE CLIENTES LOCALIZADOS==" );		
		$.each( lstClientesLocalizados, function(index, val) {									
			//console.log( ">> " + JSON.stringify( val ) );
		});
		if( lstClientesLocalizados !=null && lstClientesLocalizados.length > 0 )
		{
			$('#tablaLocalizacionClientes').bootstrapTable('load',lstClientesLocalizados);
		}
		else
		{
			$('#tablaLocalizacionClientes').bootstrapTable('load',[]);
		}		
		
		
		//console.log( "== TABLA DE CLIENTES AGREGADOS==" );		
		$.each( lstClientesAgregados, function(index, val) {									
			//console.log( ">> " + JSON.stringify( val ) );
		});		
		
		$('#tablaClientesAgregados').bootstrapTable('load',lstClientesAgregados);
		
		var stCheck = $("#checkAllCasosPorAsignar").is(':checked');
		if( stCheck == true ){
			$('#checkAllCasosPorAsignar').prop('checked', false );
		}
		
	});
	
	$('#btnGuardarLC').click(function (e) {
		//console.log( "== Guardar ==");
		var isValidFA = $('#MSCAVE01011_1_Form').valid(); 
		if( isValidFA == true )
		{
			updateRowPersonaReportadaFromLoc();
		}
	});		
	
	$('#btnOkQuitarCliente').click(function (e) {
		//console.log( "== Quitar Cliente Agregado ==");
		var lstClientesLocalizados =$('#tablaLocalizacionClientes').bootstrapTable('getData');
		objTmpQuitar.stClienteLoc = 0;
		lstClientesLocalizados.push( objTmpQuitar );
		
		$('#tablaLocalizacionClientes').bootstrapTable('load',lstClientesLocalizados);
		
		$('#tablaClientesAgregados').bootstrapTable('remove', {
			field: 'nuCliente',
			values: [ objTmpQuitar.nuCliente ]
		});			
	});
	
	
	$('#btnReportarCliente').click(function (e) {
		//console.log( "== Evento Reportar Clientes ==" );
		//console.log( "== Muestra cuadro de dialogo de confirmacion ==" );
		$('#dialogConfirmarReportarClientes').modal( 'show' );
		
	});
	
	$('#btnOkRepCliente').click(function (e) {
		//console.log( "== Evento Reportar OK ==" );
		//console.log( "== Agrega Clientes Reportados ==" );		
		addPersonasReportadasFromLoc();
	});
	
	// ==================== Habilitar / Deshabilitar ====================
	function habilitarCampoNombre(){
		$('#nbNombre').prop("disabled", false);
	}
	
	function deshabilitarCampoNombre(){
		$('#nbNombre').prop("disabled", true);
	}
	
	function habilitarCampoAPaterno(){
		$('#nbPaterno').prop("disabled", false);
	}
	
	function deshabilitarCampoAPaterno(){
		$('#nbPaterno').prop("disabled", true);
	}
	
	function habilitarCampoAMaterno(){
		$('#nbMaterno').prop("disabled", false);
	}
	
	function deshabilitarCampoAMaterno(){
		$('#nbMaterno').prop("disabled", true);
	}		
	
	function habilitarCampoRazonSocial(){
		$('#nbRazonSocial').prop("disabled", false);
	}
	
	function deshabilitarCampoRazonSocial(){
		$('#nbRazonSocial').prop("disabled", true);
	}		
	
	function habilitarBotonReportar(){		
		$('#btnReportarCliente').prop("disabled", false);
	}

	function deshabilitarBotonReportar(){		
		$('#btnReportarCliente').prop("disabled", true);
	}
	
	function habilitarBotonAgregar(){		
		$('#btnAddClienteLoc').prop("disabled", false);
	}
	
	function deshabilitarBotonAgregar(){		
		$('#btnAddClienteLoc').prop("disabled", true);
	}		
	
	function habilitarBotonBuscar()
	{
		$('#btnBuscar').prop("disabled", false);	
	}
	
	function deshabilitarBotonBuscar()
	{
		$('#btnBuscar').prop("disabled", true);	
	}
	
	function habilitarBotonGuardar()
	{
		$('#btnGuardar').prop("disabled", false);	
	}
	
	function deshabilitarBotonGuardar()
	{
		$('#btnGuardar').prop("disabled", true);	
	}	
	
	function habilitarCmpFechaNac()
	{
		$('#fhNacimientoLoc').prop("disabled", false);	
	}
	
	function deshabilitarCmpFechaNac()
	{
		$('#fhNacimientoLoc').prop("disabled", true);	
	}	
	
	function habilitarCmpTpPersona()
	{
		$('#cdTpPersonaL').prop("disabled", false);	
	}
	
	function deshabilitarCmpTpPersona()
	{
		$('#cdTpPersonaL').prop("disabled", true);	
	}		
	
	function setClienteBancomerRY(){
		//console.log( "== Evento cliente bancomer SI ==" );
		//console.log( "Cte Rep : " + JSON.stringify( clienteReportado ) );
		habilitarBotonBuscar();
		mostrarBotonBuscar();
		deshabilitarBotonGuardar();		
		ocultarBotonGuardar();
		habilitarBotonReportar();
		habilitarBotonAgregar();
		deshabilitarCmpFechaNac();
		deshabilitarCmpTpPersona();
		$('#fhNacimientoLoc').val( "" );
		$('#cdTpPersonaL').val( "-1" );
		
		$('#tablaLocalizacionClientes').bootstrapTable('load',lstClientesLocalizados);
		$('#tablaClientesAgregados').bootstrapTable('load',lstClientesLocalizados);		
		var stCheck = $("#checkAllCasosPorAsignar").is(':checked');
		if( stCheck == true ){
			$('#checkAllCasosPorAsignar').prop('checked', false );
		}	

		var tipoPersonaSel = $( "#cdTpPersonaL" ).val();
		//console.log( ">> EL TIPO DE PERSONA DEL CLIENTE ES : "  + tipoPersonaSel );
		
		if( tipoPersonaSel == CD_TP_PERSONA_MORAL || tipoPersonaSel == CD_TP_PERSONA_GOBIERNO ) 
		{
			//console.log( "== Set Validacion Busqueda persona Moral o Gobierno ==" );
			validaFormBusquedaMoral();
			
		}else if( tipoPersonaSel == CD_TP_PERSONA_FISICA ){
			//console.log( "== Set Validacion Busqueda persona Fisica" );	
			validaFormBusquedaFisica();
		}
		$('#MSCAVE01011_1_Form').valid();		
	}
	
	function setClienteBancomerRN(){
		//console.log( "== Evento cliente bancomer NO ==" );
		//console.log( "Cte Rep : " + JSON.stringify( clienteReportado ) );
		
		habilitarBotonGuardar();
		mostrarBotonGuardar();
		deshabilitarBotonBuscar();
		ocultarBotonBuscar();
		deshabilitarBotonReportar();
		deshabilitarBotonAgregar();
		habilitarCmpFechaNac();
		habilitarCmpTpPersona();
		$('#fhNacimientoLoc').val( "" );
		
		var stCheck = $("#checkAllCasosPorAsignar").is(':checked');
		if( stCheck == true ){
			$('#checkAllCasosPorAsignar').prop('checked', false );
		}			
		$('#tablaLocalizacionClientes').bootstrapTable('load',lstClientesLocalizados);
		$('#tablaClientesAgregados').bootstrapTable('load',lstClientesLocalizados);
		

		var tipoPersonaSel = $( "#cdTpPersonaL" ).val();
		//console.log( ">> EL TIPO DE PERSONA DEL CLIENTE ES : "  + tipoPersonaSel );
		
		if( tipoPersonaSel == CD_TP_PERSONA_MORAL || tipoPersonaSel == CD_TP_PERSONA_GOBIERNO ) 
		{
			//console.log( "== Set Validacion Alta persona Moral o Gobierno ==" );
			validaFormAltaMoral();
			
		}else if( tipoPersonaSel == CD_TP_PERSONA_FISICA ){
			//console.log( "== Set Validacion Alta persona Fisica" );	
			validaFormAltaFisica();
		}
		$('#MSCAVE01011_1_Form').valid();
	}	
	
	// ==================== Mostrar / Ocultar ====================
	function ocultarBotonBuscar()
	{
		//console.log( "== Ocultar Boton Buscar ==");
		$( "#btnBuscarLC" ).hide();	
	}
	
	function mostrarBotonBuscar()
	{
		//console.log( "== Mostrar Boton Buscar ==");
		$( "#btnBuscarLC" ).show();	
	}
	
	function ocultarBotonGuardar()
	{
		//console.log( "== Ocultar Boton Guardar ==");
		$( "#btnGuardarLC" ).hide();
	}
	
	function mostrarBotonGuardar()
	{
		//console.log( "== Mostrar Boton Guardar ==");
		$( "#btnGuardarLC" ).show();	
	}		
	
	// ==================== CREACION DE OBETOS ====================
	
	function crearObjetoClienteLoc(){
		var objCliente = new Object();
		objCliente.nbNombre = $( "#nbNombre" ).val();
		objCliente.nbPaterno = $( "#nbPaterno" ).val();
		objCliente.nbMaterno = $( "#nbMaterno" ).val();
		objCliente.nbRazonSocial = $( "#nbRazonSocial" ).val();
		objCliente.cdRFC = $( "#cdRFC" ).val();
		objCliente.nuCliente = $( "#nuCliente" ).val();
		var fhNacimientoTmp = $("#fhNacimientoLoc").datepicker('getDate');		
		objCliente.fhNacimiento = fhNacimientoTmp;
		if( $( "#cdTpPersonaL" ).val() != '-1' ){
			objCliente.cdTpPersona = $( "#cdTpPersonaL" ).val();	
		}else{
			objCliente.cdTpPersona = '';
		}
		return objCliente;
	}	

	// ==================== LIMPIAR FORMULARIOS ====================
	function limpiarFormularioBusqueda()
	{
		console.log( "=== Limpiar Formulario de Busqueda ===" );
		$( "#nbNombre").val( '' );
		$( "#nbPaterno").val( '' );
		$( "#nbMaterno").val( '' );
		$( "#nbRazonSocial").val( '' );
		$( "#cdRFC").val( '' );
		$( "#nuCliente").val( '' );
		$( "#fhNacimientoLoc").val( '' );
		$( "#cdTpPersonaL").val( '-1' );	
		document.getElementById( 'Y' ).checked = true;
		mostrarBotonBuscar();
		ocultarBotonGuardar();		
		lstClientesLocalizados = new Array();
	}
	
	// ==================== INVOCACION DE SERVICIOS ====================
	function consultarClientesLoc( cliente ){
		var _url = obtenerDeSessionStorage("baseUrl", true) + "/TransaccionPEE5Service/localizarClientes"		
		 return $.ajax({
			url : _url,
			type : 'POST',
			contentType: 'application/json',
			dataType: 'json',
			data: JSON.stringify(cliente),
			async: false
		});
	}	
	
	// ==================== FUNCIONES 
	/**
	 * Elimina una fila de la tabla Personas Reportadas
	 */
	/*
	function removeRowPersonaReportadaFromLoc( nuIdPersonaReportada ){
		//console.log( "== Remove Row Personas Reportadas desde Localizacion del cliente : " + nuIdPersonaReportada );
		$('#tablaPersonaReportada').bootstrapTable('remove', {
			field: 'nuId',
			values: [nuIdPersonaReportada]
		});
	}
	*/
	
	/**
	 * Actualiza los datos de una persona 'Es cliente Bancomer = NO'
	 */
	function updateRowPersonaReportadaFromLoc(  ){
		//console.log( "== Update Row Persona Reportada desde Localizacion del cliente ==" );
		
		var vClienteReportadoTmp = crearObjetoClienteLoc();
		var vClienteReportadoUpdate = new Object();
		vClienteReportadoUpdate.nuId = clienteReportado.nuId;
		
		if( vClienteReportadoTmp.cdTpPersona == CD_TP_PERSONA_FISICA )
		{
			vClienteReportadoUpdate.nbNombre = vClienteReportadoTmp.nbNombre;
			vClienteReportadoUpdate.nbApPaterno = vClienteReportadoTmp.nbPaterno;
			vClienteReportadoUpdate.nbApMaterno = vClienteReportadoTmp.nbMaterno;
		}
		else if( vClienteReportadoTmp.cdTpPersona == CD_TP_PERSONA_MORAL ||  vClienteReportadoTmp.cdTpPersonaL == CD_TP_PERSONA_GOBIERNO )
		{
			vClienteReportadoUpdate.nbNombre = vClienteReportadoTmp.nbRazonSocial;
			vClienteReportadoUpdate.nbApPaterno = '';
			vClienteReportadoUpdate.nbApMaterno = '';			
		}
		vClienteReportadoUpdate.cdRFC = vClienteReportadoTmp.cdRFC;
		vClienteReportadoUpdate.cdCliente = vClienteReportadoTmp.nuCliente;
		vClienteReportadoUpdate.fhNacimiento = vClienteReportadoTmp.fhNacimiento;
		vClienteReportadoUpdate.cdTpPersona = vClienteReportadoTmp.cdTpPersona;
		vClienteReportadoUpdate.nbTpPersona = vClienteReportadoTmp.cdTpPersona;
		vClienteReportadoUpdate.cdCaracter = clienteReportado.cdCaracter;
		vClienteReportadoUpdate.nbDomicilio = clienteReportado.nbDomicilio;
		vClienteReportadoUpdate.nbComplementarios = clienteReportado.nbComplementarios;
		vClienteReportadoUpdate.nuCuenta = clienteReportado.nuCuenta;
		vClienteReportadoUpdate.cdDictamen = clienteReportado.cdDictamen;
		
		var rdbEsclienteL = $('input[name=rdbCteBancomer]');
		var checkedValueL = rdbEsclienteL.filter(':checked').val();		
		
		vClienteReportadoUpdate.cdClienteBancomer = checkedValueL,
		
		actualizarPersonaReportadaFromLoc( clienteReportado, vClienteReportadoUpdate, indexCteRep );
		
		
		sessionStorage.removeItem( 'clienteReportado' );				
		sessionStorage.removeItem( 'indexCteRep' );		
		$(this).modal('hide');
	}
	
	function addPersonasReportadasFromLoc()
	{

		//console.log( "== AGREGA NUEVAS PERSONAS REPORTADAS DESDE LA PANTALLA DE LOCALIZACION ==" );
		var lstClientesAgregados = $('#tablaClientesAgregados').bootstrapTable('getData');
		
		//console.log( "== CLIENTES AGREGADOS ==" );
		//console.log( JSON.stringify( lstClientesAgregados ) );
		agregarPersonasReportadasFromLoc( lstClientesAgregados );
		$(this).modal('hide');

	}
		
	// ==================== VALIDACIONES ====================
	
	/**
	 * Valida el formulario para un Alta de una persona de tipo Fisica
	 */
	function validaFormAltaFisica()
	{
		//console.log( "== Validaciones Persona Reportada Fisica desde Localizacion del cliente ==" );
		$('#MSCAVE01011_1_Form').validate( {
			rules: {
				fhNacimientoLoc: {
					required: true,
					date:false
				},				
				nbNombre:{
					required: true,
					maxlength: 20
				},					
				nbPaterno:{
					required: false,
					maxlength: 20
				},	
				nbMaterno:{
					required: false,
					maxlength: 20
				},
				cdTpPersonaL:{
					notEqual: "-1"
				},								
			},
			errorPlacement: function (error, element) {
	            $(element).prop('style', 'border: 1px solid #C8175E;');
	        },
	        success: function (label, element) {
	            $(element).prop('style', 'border: 1px solid #86C82D;');
	        }
		});
	}	
	
	/**
	 * Valida el formulario para un alta de una persona de tipo Moral
	 */
	function validaFormAltaMoral()
	{
		//console.log( "== Validaciones Alta Persona Reportada Moral desde Localizacion del cliente ==" );
		$('#MSCAVE01011_1_Form').validate( {
			rules: {
				nbRazonSocial:{
					required: true,
					maxlength: 60
				},
				nbNombre:{
					required: false,
					maxlength: 20
				},					
				nbPaterno:{
					required: false,
					maxlength: 20
				},	
				nbMaterno:{
					required: false,
					maxlength: 20
				},				
				fhNacimientoLoc: {
					required: false,
					date:false
				},
				cdTpPersonaL:{
					notEqual: "-1"
				},
			},
			errorPlacement: function (error, element) {
	            $(element).prop('style', 'border: 1px solid #C8175E;');
	        },
	        success: function (label, element) {
	            $(element).prop('style', 'border: 1px solid #86C82D;');
	        }
		});
	}	
		
	/**
	 * Valida el formulario para una busqueda de una persona de tipo Fisica 
	 */
	function validaFormBusquedaFisica()
	{
		//console.log( "== Validaciones Busqueda Persona Fisica desde Localizacion del cliente ==" );
		$('#MSCAVE01011_1_Form').validate( {
			rules: {
				nbRazonSocial:{
					required: false,
					maxlength: 60
				},
				nbNombre:{
					required: true,
					maxlength: 20
				},					
				nbPaterno:{
					required: true,
					maxlength: 20
				},	
				nbMaterno:{
					required: true,
					maxlength: 20
				},				
				fhNacimientoLoc: {
					required: false
				},
				cdTpPersonaL:{
					required: false,
				},
			},
			errorPlacement: function (error, element) {
	            $(element).prop('style', 'border: 1px solid #C8175E;');
	        },
	        success: function (label, element) {
	            $(element).prop('style', 'border: 1px solid #86C82D;');
	        }
		});
	}		
	
	/**
	 * Valida el formulario para una busqueda de una persona de tipo Fisica
	 */
	function validaFormBusquedaMoral()
	{
		//console.log( "== Validaciones Busqueda Persona Fisica desde Localizacion del cliente ==" );
		$('#MSCAVE01011_1_Form').validate( {
			rules: {
				nbRazonSocial:{
					required: true,
					maxlength: 60
				},
				nbNombre:{
					required: false,
					maxlength: 20
				},					
				nbPaterno:{
					required: false,
					maxlength: 20
				},	
				nbMaterno:{
					required: false,
					maxlength: 20
				},				
				fhNacimientoLoc: {
					required: false
				},
				cdTpPersonaL:{
					required: false,
				},
			},
			errorPlacement: function (error, element) {
	            $(element).prop('style', 'border: 1px solid #C8175E;');
	        },
	        success: function (label, element) {
	            $(element).prop('style', 'border: 1px solid #86C82D;');
	        }
		});
	}		
		
