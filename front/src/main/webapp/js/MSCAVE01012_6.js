/** VARIABLES DE CONTROL **/
var modificar = false;
var eliminar = false;
var alta = true;
var acepto = false;
var CD_ST_SIS_A = '11';
var CD_ST_SIS_I = '12';
var KEY_ENTER = 13;
var objTmp = new Object();
var PERFIL_CONSULTOR = 4;
var PERFIL_SUPERVISOR = 2;
var PERFIL_COORDINADOR = 3;
var PERFIL_ADMINISTRADOR = 1;
var PERFILADO = sessionStorage.cdPerfil == PERFIL_ADMINISTRADOR;

/** Funciones de Datos **/
function borrarDatos() {
	modificar = false;
	acepto = false;
	eliminar = false;
	alta = true;
	objTmp = new Object();
	$('#nbCveRed').val('');
	$('#nbCliente').val('');
	$('#nbEmail').val('');
	$('#cdPerfil1').val('-1');
	$('#cdPerfil2').val('-1');
	$('#cdStSistema').val('-1');
	$('#cdSupervisor').val('-1');
	$('#cdGerencia').val('-1');
	deshCtrl('#cdPerfil2');
	habilCtrl('#nbCveRed');
	habilCtrl('#cdSupervisor');
	habilCtrl('#cdGerencia');
	resetVal();
}

function fncModificar(valor){
	acepto = valor;
	validacion();
	if(!valor){
		resetVal();
	}
}

function fncEliminar(valor){
	acepto = valor;
	eliminar = valor;
	validacion();
	borrarDatos();
}

function resetVal(){
	$('#MSCAVE01012_6_FORM input').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
	$('#MSCAVE01012_6_FORM select').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
}

/** Funciones de eventos **/
var actionEvents = {
	'click .delete' : function(e, value, row, index) {
		eliminar = true;
		modificar = false;
		alta = false;
		llenarUsuario(row);
		objTmp.cdStSistema = CD_ST_SIS_I;
		$('#dialogEliminarUsuario').modal('show');
	},
	'click .actualizar' : function(e, value, row, index) {
		modificar = true;
		eliminar = false;
		alta = false;
		llenarUsuario(row);
		deshCtrl('#nbCveRed');
	}
};

/** Funciones de Tablas **/
function llenadoControlAcces(data) {
	$('#tablaControlAcces').bootstrapTable(
		{
			pagination : true,
			pageSize : 5,
			pageList : [ 5, 25, 50 ],
			data : data,
			clickToSelect : false,
			singleSelect : false,
			maintainSelected : true,
			sortable : true,
			checkboxHeader : false,
			formatShowingRows : function(pageFrom, pageTo, totalRows) {
				return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
			},
			formatRecordsPerPage : function(pageNumber) {
				return pageNumber + ' registros por p\u00e1gina';
			},
			formatLoadingMessage : function() {
				return 'Cargando, espere por favor...';
			},
			formatSearch : function() {
				return 'Buscar';
			},
			formatNoMatches : function() {
				return 'No se encontr&oacute; informaci&oacute;n';
			}
		}
	);
}

/** Funciones de Formato **/
function formatterImageDel(value, row, index) {
	if (PERFILADO) {
		return [
                  //'<a class="delete" href="javascript:void(0)" title="Eliminar Usuario">',
                  //'<i class="tabla_registro_eliminar">.</i>',
                  //'</a>',
                  '<a class="actualizar" href="javascript:void(0)" title="Actualizar">',
                  '<i class="tabla_registro_editar">.</i>', '</a>'
        ].join('');
	}
	else{
		return ['',''].join('');
	}
}

function formatterDetailUsuarios(value, row, index) {
	return [ 
	    '<table class="tableForm">',
	    '<tr><td class="tdDetail"><b>Nombre :</b> ',
	    row.nbCliente,
	    '</td></tr>',
	    '<tr><td class="tdDetail"><b>Email :</b> ',
	    row.nbEmail,
	    '</td></tr>',
	    '</table>' 
	].join('');
}

function deshCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).attr('disabled','disabled');
	$(selector).addClass('ignore');
}

function habilCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).removeAttr('disabled');
	$(selector).removeClass('ignore');
}

function validaPerfil(){
	if (PERFILADO) {
		$("#abcUsuario").show();
	} else {
		$("#abcUsuario").hide();
	}
}

/** Funciones de Formulario **/
function llenarObjeto(data, apartado) {
	$.each(data, function(key, value) {
		if(key == 'cdUsuario'){
			objTmp.cdUsuario = value;
		}else if(key == 'nbCveRedSupervisor'){
			objTmp.nbCveRedSupervisor = value;
		}else if(key == 'cdGerenciaSupervisor'){
			objTmp.cdGerenciaSupervisor = value;
		}else if(key == 'cdPerfil2'){
			objTmp.cdPerfil2 = value;
		}
	});
}

function llenarUsuario(data) {
	llenarFormulario(data, 'auten');
	llenarObjeto(data, 'auten');
	if(objTmp.cdPerfil2  == null){
		$('#cdPerfil2').val('-1');
	}
	$('#MSCAVE01012_6_FORM').valid();
	deshCtrl('#nbCveRed');
	resetVal();
	
}

function cargaInfoUsuario(){
	$('#nbCliente').val(objTmp.nbPersona + ' ' + objTmp.nbPaterno + ' ' + objTmp.nbMaterno);
	$('#nbEmail').val(objTmp.nbEmail);
}


/** *** OBJETOS **** */
function crearObjetoUsuario() {
	usuario = new Object();
	usuario.cdUsuario = objTmp.cdUsuario;
	usuario.nbCveRed = $("#nbCveRed").val().toUpperCase();
	usuario.cdPerfil1 = $("select#cdPerfil1 option").filter(":selected").val();
	usuario.cdStSistema = $("select#cdStSistema option").filter(":selected").val();
	var perBValido = $("select#cdPerfil2 option").filter(":selected").val();
	if(perBValido != '-1'){
		usuario.cdPerfil2 = perBValido; 
	}
	var superValido = $("select#cdSupervisor option").filter(":selected").val();
	if(superValido != '-1'){
		usuario.cdSupervisor = superValido;
		usuario.nbCveRedSupervisor = objTmp.nbCveRedSupervisor;
	}
	var gerenValida = $("select#cdGerencia option").filter(":selected").val();
	if(gerenValida != '-1' && superValido == '-1'){
		usuario.cdGerencia =  gerenValida;
	}
	usuario.cdUsrAlta = sessionStorage.cdUsuario;
	return usuario;
}

/** Funciones de validación **/
function validarUsuario()
{
	$.validator.addMethod('validaCbPerfil',function (value, element, arg){
		var valSelPerfilA = $("select#cdPerfil1 option").filter(":selected").val();
		var valSelPerfilB = $("select#cdPerfil2 option").filter(":selected").val();
		if(valSelPerfilA != valSelPerfilB && valSelPerfilA != '-1' && valSelPerfilB != '-1'){
			switch(parseInt(valSelPerfilA)){
			case PERFIL_CONSULTOR:
				habilCtrl('#cdPerfil2');
				habilCtrl('#cdSupervisor');
				deshCtrl('#cdGerencia');
				switch(parseInt(valSelPerfilB)){
				case PERFIL_SUPERVISOR:
					return true;
					break;
				case PERFIL_COORDINADOR:
					return true;
					break;
				}
				break;
			case PERFIL_SUPERVISOR:
				deshCtrl('#cdSupervisor');
				habilCtrl('#cdPerfil2');
				habilCtrl('#cdGerencia');
				switch(parseInt(valSelPerfilB)){
				case PERFIL_COORDINADOR:
					return true;
					break;
				}
				break;
			case PERFIL_COORDINADOR:
			case PERFIL_ADMINISTRADOR:
				$('#cdPerfil2').val('-1')
				$('#cdSupervisor').val('-1');
				$('#cdGerencia').val('-1');
				deshCtrl('#cdPerfil2');
				deshCtrl('#cdSupervisor');
				deshCtrl('#cdGerencia');
				return true;
				break;
			}
		}else if(valSelPerfilA != '-1' && valSelPerfilB == '-1'){
			habilCtrl('#cdPerfil2');
			switch(parseInt(valSelPerfilA)){
			case PERFIL_CONSULTOR:
				habilCtrl('#cdPerfil2');
				habilCtrl('#cdSupervisor');
				deshCtrl('#cdGerencia');
				return true;
				break;
			case PERFIL_SUPERVISOR:
				$('#cdSupervisor').val('-1');
				deshCtrl('#cdSupervisor');
				habilCtrl('#cdPerfil2');
				habilCtrl('#cdGerencia');
				return true;
				break;
			case PERFIL_COORDINADOR:
			case PERFIL_ADMINISTRADOR:
				$('#cdPerfil2').val('-1')
				$('#cdSupervisor').val('-1');
				$('#cdGerencia').val('-1');
				deshCtrl('#cdPerfil2');
				deshCtrl('#cdSupervisor');
				deshCtrl('#cdGerencia');
				return true;
				break;
			}
		}else if(valSelPerfilA == '-1'){
			$('#cdPerfil2').val('-1')
			deshCtrl('#cdPerfil2');
			return false;
		}
		return false;
	},'error!');
	$.validator.addMethod('validaSelect',function (value, element, arg){
		return arg != value;
	},'error!');
	var validador = $('#MSCAVE01012_6_FORM').validate(
		{
			rules: {
				nbCveRed: {
					required: true,
					maxlength: 8
				},
				cdPerfil1: {
					required: true,
					validaCbPerfil: true,
					validaSelect: '-1'
				},
				cdPerfil2: {
					validaCbPerfil: true
				},
				cdStSistema: {
					required: true,
					validaSelect: '-1'
				},
				cdSupervisor: {
					required: true,
					validaSelect: '-1'
				},
				cdGerencia: {
					required: true,
					validaSelect: '-1'
				}
			},
			errorPlacement: function (error, element) {
				$(element).prop('style', 'border: 1px solid #C8175E;');
			},
			success: function (label, element) {
				$(element).prop('style', 'border: 1px solid #86C82D;');
			}
		}
	);
}

/** Inicialización **/
(function($, window, document) {
	$(function() {
		validaPerfil();
		if(PERFILADO){
			llenarSelectPorCdCatalogo('2',$('#cdStSistema'),true);
			llenarSelectPorTabla('Perfil',$('#cdPerfil1'),true);
			llenarSelectPorTabla('Perfil',$('#cdPerfil2'),true);
			llenarSelectPorTablaParametro('UsuariosPerfil',PERFIL_SUPERVISOR,$('#cdSupervisor'),true);
			llenarSelectPorTabla('Gerencia', $('#cdGerencia'), true);
			validarUsuario();
			$("#cdPerfil1").change(function (){
				$(this).valid();
			});
			$("#cdPerfil2").change(function (){
				$(this).valid();
			});
			$("#nbCveRed").keydown(function (event){
				if(event.which == KEY_ENTER){
					if($(this).val().length < 9 ){
						getInfoEmpleado($(this).val().toUpperCase()).done(function (data){
							if (cargarMensaje(data, false)) {
								$.each(data.response, function(key, val){
									if(key == 'cdUsuario'){
										objTmp.cdUsuario = val;
									}else if(key == 'nbCveRed'){
										objTmp.nbCveRed = val;
									}else if(key == 'nbPersona'){
										objTmp.nbPersona = val;
									}else if(key == 'nbPaterno'){
										objTmp.nbPaterno = val;
									}else if(key == 'nbMaterno'){
										objTmp.nbMaterno = val;
									}else if(key == 'nbEmail'){
										objTmp.nbEmail = val;
									}
								});
								cargaInfoUsuario();
							} else {
								objTmp = new Object();
							}
						});
					}
				}
			});
		}
		deshCtrl('#cdPerfil2');
		$('.date').datepicker({
			language : 'en',
			format : "dd/mm/yyyy",
			autoclose : true,
			orientation : 'top'
		});
		$('.money').maskMoney({
			prefix : '$',
			precision : 2
		});
		obtenerUsuarios().done(function(data) {
			if (cargarMensaje(data, false)) {
				llenadoControlAcces(data.response);
			} else {
				llenadoControlAcces([]);
			}
		});
	});
}(window.jQuery, window, document));

/** Funcionalidad **/
function obtenerUsuarios() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/PerfiladoService/consultarUsuarios",
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		async : false
	});
}

function getInfoEmpleado(nbCveRed) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/PerfiladoService/getInfoEmpleado/" + nbCveRed,
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		async : false
	});
}

/*
 * ABC Usuario
 */
function abcUsuario(usuario) {
	var _url = obtenerDeSessionStorage("baseUrl", true)	+ "/PerfiladoService";
	var _type = "POST";
	if(alta){
		_url += "/altaUsuario";
	}else{
		_url += "/modificaUsuario";
	}
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(usuario)
	});
}

function validacion() {
	var usuarioValido = $('#MSCAVE01012_6_FORM').valid(); 
	if(usuarioValido){
		var usuario = crearObjetoUsuario();
		if(modificar && !acepto){
		    $('#dialogActualizaUsuario').modal('show');
		}
		if ((modificar && acepto)||(eliminar && acepto) || (alta)) {
		    abcUsuario(usuario).done(function(data) {
		    	if (cargarMensaje(data, true)) {
		    		obtenerUsuarios().done(function(data) {
			    		if (cargarMensaje(data, false)) {
			    			$('#tablaControlAcces').bootstrapTable('load',data.response);
			    		} else {
			    			$('#tablaControlAcces').bootstrapTable('load',[]);
			    		}
			    	});
		    		borrarDatos();
		    	}
		    });
		}
	}
}