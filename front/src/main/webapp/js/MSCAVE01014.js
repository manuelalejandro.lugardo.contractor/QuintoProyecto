/*
var arregloPersonasReportadas = new Array();
var idTipoPersona = null;
var indiceEliminar = null;
var contador = 0;
var idPersonaReportada;
var cdRequerimiento = 0;
var nuFolioArchivo;
var cdFolioArchivoDelete;
var LONGITUD_MAXIMA = 20;
var CD_TP_PERSONA_FISICA = 'F';
var CD_TP_PERSONA_MORAL = 'M';
var CD_TP_PERSONA_GOBIERNO = 'G';
var DET_TP_PERSONA_FISICA = "Fisica";
var DET_TP_PERSONA_MORAL = "Moral";
var DET_TP_PERSONA_GOBIERNO = "Gobierno";
var TP_ALTA_MANUAL = 'M';
var TP_ALTA_AUTOMATICA = 'A';
var TP_ALTA;
var ES_CLIENTE_SI = 'SI';
var ES_CLIENTE_NO = 'NO';
var TIENE_ASEGURAMIENTO_SI = '36';
var TIENE_ASEGURAMIENTO_NO= '37';
var EXTENSION_XML = 'XML';*/

var clienteAREdit;

/*
var msgErrorTipoXML= {"response":"","cdMensaje":0,"nbMensaje":"Tipo de archivo incorrecto, el formato del archivo debe ser .xml"};
var msgErrorLongitud= {"response":"","cdMensaje":0,"nbMensaje":"El nombre del archivo es demasiado largo, el m\u00e1ximo permitido es de 20 caracteres."};
var msgErrorFechaRecepcion= {"response":"","cdMensaje":0,"nbMensaje":"La Fecha de Recpeci\u00f3n no puede ser mayor a la fecha actual."};
var msgErrorTipo= {"response":"","cdMensaje":0,"nbMensaje":"Antes de importar el archivo debe seleccionar el tipo de fuente."};
var msgErrorFaltanTipologias= {"response":"","cdMensaje":0,"nbMensaje":"Debe asignar una tipolog\u00EDa a todas las personas reportadas."};
*/

	// ==================== EVENTOS DE LOS BOTONES DE ACCION ====================	
	/*
	var actionEventsPersonasReportadas = { 
			// Abre la funcion de localizacion del cliente			
			'click .detailLocalizacion': function (e, value, row, index) {
				console.log( "== Muestra pantalla localizacion del cliente ==" );
				console.log( "Cliente Seleccionado : " + JSON.stringify( row ) );
				console.log( "===Indice Seleccionado : " + index );
				
				sessionStorage.clienteReportado = ( 'clienteReportado', JSON.stringify( row ) );				
				sessionStorage.indexCteRep = ( 'indexCteRep', index );
				
				$('#MSCAVE01014_1').load('MSCAVE01014_1.html', function (response, status, xhr){
					if (status == "success") {
						$(this).modal('show');
					}else{
						data.cdMensaje = 0;
						data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
						cargarMensaje(data,false);
					}
				});
				
			},
			// Abre la funcion Detalle del Dictamen
			'click .detailDictamen': function (e, value, row, index) {
				sessionStorage.opDictamen = 'F';
				$('#MSCAVE01008').load('MSCAVE01008.html', function (response, status, xhr){
					if (status == "success") {
						$(this).modal('show');
					}else{
						data.cdMensaje = 0;
						data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
						cargarMensaje(data,false);
					}
				});
			}, 

			// Elimina un registro de la Base de datos
			'click .delete' : function(e, value, row, index) {
				console.log( "== Evento Eliminar Persona Reportada ==" );
				console.log( "e : " + e + " | value : " + value + " | index : " + index );
				console.log( JSON.stringify(row ) );
				idPersonaReportada = null;
				idPersonaReportada = row.nuId;
				console.log( "Eliminar la persona con el ID: " + idPersonaReportada );
				$('#dialogPersonaReportada').modal('show');
			},
			// Elimina un registro de la Base de datos
			'click .edit' : function(e, value, row, index) {
				console.log( "== Editar persona reportada ==" );
				console.log( "== Abre el modal para editar una nueva persona ==" );
				console.log( "== Persona Reportada a Editar ==" );
				console.log( JSON.stringify( row ) );
				limpiarFormularioPersonaRiesgo();
				if( row.cdTpPersona == CD_TP_PERSONA_MORAL || row.cdTpPersona == CD_TP_PERSONA_GOBIERNO )
				{
					$('#auxRazonSocial').val( row.nbNombre );
					$('#auxRazonSocial').prop( 'disabled', false );
					$('#auxRazonSocial').show( );					
					$('#auxNombre').val( );
					$('#auxNombre').hide( );
				}
				else if( row.cdTpPersona == CD_TP_PERSONA_FISICA )
				{
					$('#auxNombre').val( row.nbNombre );
					$('#auxNombre').prop( 'disabled', false );
					$('#auxNombre').show();
					$('#auxRazonSocial').val( '' );
					$('#auxRazonSocial').hide();
	                $("#auxAP").val( row.nbApPaterno );
	                $('#auxAP').prop( 'disabled', false );
	                $("#auxAM").val( row.nbApMaterno );
	                $('#auxAM').prop( 'disabled', false );
					console.log( "Nombre PF : " + row.nbNombre );
				}
				$("#nuIdHdn").val( row.nuId );
				$("#nuIndexHdn" ).val( index );
				//$("#auxNombre").val( row.nbNombre );						                
                var fhNacimientoTmp = row.fhNacimiento;
                if( fhNacimientoTmp != null && ( fhNacimientoTmp != '' ))
                {     	
                	 $( '#auxFechaN' ).datepicker( 'setDate', row.fhNacimiento );
                }
                $('#auxFechaN').prop( 'disabled', false );
                
                $('#auxDom').val( row.nbDomicilio ); 
                $('#auxDom').prop( 'disabled', false );
                $('#auxIdTipoP').val( row.cdTpPersona );
                $('#auxIdTipoP').prop( 'disabled', false );
                
                $('#auxTipologia').prop( 'disabled', false );
                if( row.cdTipologia == null || row.cdTipologia == '' ){
                	$('#auxTipologia').val( "-1" );	
                }
                else
                {
                	$('#auxTipologia').val( row.cdTipologia );	
                }
                
                $('#auxRFC').prop( 'disabled', false );
                $("#auxRFC").val( row.cdRFC );
                //$('#auxIauxRFCdTipoP').prop( 'disabled', false );
                $("#auxCaracter").val( row.cdCaracter );
                $('#auxCaracter').prop( 'disabled', false );
                $('#esCliente').val( row.cdClienteBancomer );
                if( row.cdClienteBancomer  == 'SI' )
                {
                	document.getElementById( 'SI' ).checked = true;
                	$('#auxNumCli').prop( 'disabled', false );
                	$('#auxNumCuenta').prop( 'disabled', false );                               	
                }else if( row.cdClienteBancomer  == 'NO' )
                {
                	document.getElementById( 'NO' ).checked = true;
                	$('#auxNumCli').prop( 'disabled', true );
                	$('#auxNumCli').val( '' );
                	$('#auxNumCuenta').prop( 'disabled', true );
                	$('#auxNumCuenta').val( '' );                	
                }                
                console.log( "Es cliente : " + row.cdClienteBancomer  );
                $('#auxNumCli').val( row.cdCliente );
                $('#auxNumCuenta').val( row.nuCuenta );
                $('#auxDictamen').val( row.cdDictamen );
                $('#auxDictamen').prop( 'disabled', false );
                $('#auxComp').val( row.nbComplementarios );
                $('#auxComp').prop( 'disabled', false );
                $('#btnAgregarPersona').hide(  );	
                $('#btnEditgregarPersona').show(  );
                $('#MSCAVE01014_Personas').modal( 'show' );
                //$('#MSCAVE01014_Personas').modal( { closeOnEscape: false } );		
                $( '#MSCAVE01014_Personas').dialog({ closeOnEscape: false });
				$('#MSCAVE01014_Personas').valid();				
			},			
	};
	*/
	/*
	var actionEventsDownloadFile = { 
			// Abre la funcion de localizacion del cliente
			'click .detailLocalizacion': function (e, value, row, index) {
				console.log( "== Descargar Archivo Evento ==" );
				console.log( "File : " + JSON.stringify( row ) );
			},
			// Abre la funcion de localizacion del cliente
			'click .delete': function (e, value, row, index) {
				console.log( "== Evento Eliminar Archivo ==" );
				console.log( "File : " + JSON.stringify( row ) );
				cdFolioArchivoDelete = row.cdFolioArchivo;
				$('#dialogDeleteFile').modal( 'show' );	
			},			
	};	
	 */
	// ==================== PRINCIPAL ====================
	(function($, window, document) {
		$(function() {
			
			console.log( '== Modulo Actualizacion de Clientes de Alto Riesgo ==' );
			console.log( '\n' );
			console.log( '==============================================================' );
			clienteAREdit = JSON.parse( sessionStorage.getItem( 'clienteAREdit' ) );
			console.log( JSON.stringify( clienteAREdit ) );
			console.log( '==============================================================' );
			
			$('.date').datepicker({
			    language: 'es',
				format : "dd/mm/yyyy",
				autoclose: true,
				orientation: 'top'
			});
			
			/*
			$( '#btnNuevo' ).show();
			$( '#btnGuardar' ).show();	
			configBtnImportar();
			configBtnAtachment();
			$( '#btnAdjuntarArchivos' ).show();			
			*/
						

					
			// Valida tipos de aseguramiento v�lidos
			$.validator.addMethod('aseguramientoValido', function(value, element){
		
				var valido1 = "SI";
				var valido2 = "NO";
				var valor = value.toUpperCase();
			
				if (valor == valido1 || valor == valido2) {
					return true;  
				} else {
					return false;   
				};
			}, "S");
			
			// Valida tipos de aseguramiento v�lidos
			$.validator.addMethod('tipoPersonaRule', function(value, element){		
				var valor = value.toUpperCase();			
				if (valor == CD_TP_PERSONA_FISICA || valor == CD_TP_PERSONA_MORAL || CD_TP_PERSONA_GOBIERNO) {
					return true;  
				} else {
					return false;   
				};
			}, 'error');		
			
			$.validator.addMethod('formatoRFCRule', function(value, element){
				//console.log( "Valida Formato : " );
				var rfcStr = $("#auxRFC").val();
				var tpPersona = $("#auxIdTipoP").val();
				var rfcEsValido = false;
				if( rfcStr == null || rfcStr == ''){
					rfcEsValido = true;
				}else{
					rfcEsValido = validaRfc(rfcStr, tpPersona);
				}				
				console.log( "RFC Valido = " + rfcEsValido);
				return rfcEsValido;
			}, "error");	
			
			$.validator.addMethod('onlyLetters', function(value, element){
				console.log( "Valor : " + value );
				return this.optional(element) || /^[a-z\s]+$/i.test(value);
			}, "error");
			
			$.validator.addMethod('onlyNumbers', function(value, element){
				console.log( "Valor : " + value );
				return this.optional(element) || /^[0-9]+$/i.test(value);
			}, "error");
			
			$.validator.addMethod('alfanumericos', function(value, element){
				console.log( "Valor : " + value );
				return this.optional(element) || /^[A-Z0-9\s]+$/i.test(value);
			}, "error");
			
			$.validator.addMethod('lettersAndEspCar', function(value, element){
				console.log( "Valor : " + value );
				return this.optional(element) || /^[a-zA-Z!@#\$%\^&\*\?.,()/"/'_~\s]+$/i.test(value);
			}, "error");		
											
			jQuery.validator.addMethod("notEqual", function(value, element, param) {
				  return this.optional(element) || value != param;
			}, "");			
						
			//$( '#fhRecepcionReq' ).datepicker( 'setDate', 'today' );
			
			//$( '#nbTipo' ).val( '-1' );
			//$( '#nbPrioridad' ).val( '' );
			//$( '#cdPrioridad' ).val( '' );
			
			$( '#fhRecepcionReq' ).datepicker('setEndDate', 'today');			
			$( '#fhPublicacion' ).datepicker('setEndDate', 'today');
			
			
			// Tipos de oficios
			llenarSelectPorTabla('Tipos',$('#nbTipo'),true);
		
			// Tipos personas
			llenarSelectPorTabla('TipoPersona',$('#auxIdTipoP'),true);
			
			var cdGerencia = 5;
			llenarSelectTipologiaGerencia($('#auxTipologia'), cdGerencia, true);
			
			var cdCatRespuestaRapida = 12;
			llenarSelectPorCdCatalogoNbCatalogo( cdCatRespuestaRapida, $('#nbAseguramiento'), true);
			
			llenadoTablaPersonaReportada([]);
			llenadoTablaArchivosAdjuntos([]);
			
			validarRequerimiento();
			validarCapturaPersonaReportada();
			validarArchivo();
			
			iniciarPantalla();			
			
		});
	}(window.jQuery, window, document));
	
	// ==================== FORMATEADORES ====================
	/*
	function formatterDetailPersonaReportada(value, row, index){	
		
		var nbDomicilioF=row.nbDomicilio==null?"":row.nbDomicilio;
		var cdCaracterF=row.cdCaracter==null?"":row.cdCaracter;
		var nbComplementariosF=row.nbComplementarios==null?"":row.nbComplementarios;
		var nuCuentaF=row.nuCuenta==null?"":row.nuCuenta;
		var cdDictamenF=row.cdDictamen==null?"":row.cdDictamen;
				
		return ['<table class="tableForm">'+
		        '<tr><td class="tdDetail"><b>Caracter:</b> '+cdCaracterF+'</td></tr>'+
		        '<tr><td class="tdDetail"><b>Domicilio:</b> '+nbDomicilioF+'</td></tr>'+		        
		        '<tr><td class="tdDetail"><b>Complementarios:</b> '+nbComplementariosF+'</td></tr>'+
		        '<tr><td class="tdDetail"><b>Cuenta:</b> '+nuCuentaF+'</td></tr>'+
		        '<tr><td class="tdDetail"><b>Dictamen:</b> '+cdDictamenF+'</td></tr>'+
		        '</table>'].join('');		
	}
	
	*/
	/*
	function replaceAllWS(Source, stringToFind, stringToReplace) {
	    var temp = Source;
	    var index = temp.indexOf(stringToFind);

	    while (index != -1) {
	        temp = temp.replace(stringToFind, stringToReplace);
	        index = temp.indexOf(stringToFind);
	    }

	    return temp;
	}
	*/	
	/*
	function formatterRegistroPersonasReportadas(value, row, index) {
		var prioridad = $( '#cdPrioridad' ).val();
		console.log( ">>> Init Prioridad : " + prioridad );
		if( prioridad == 14 && cdRequerimiento == 0){
			return [
		            '<a class="delete" href="javascript:void(0)" title="Eliminar">',
		            '<i class="tabla_registro_eliminar">.</i>',
		            '</a>',
		            '<a class="detailLocalizacion" href="javascript:void(0)"  title="Localizacion Clientes">',
		            '<i class="glyphicon glyphicon-search icon-search"></i>',
		            '</a>',
		            '<a class="edit" href="javascript:void(0)" title="Actualizar">',
		            '<i class="tabla_registro_editar">&nbsp;</i>',
		            '</a>',
		            '<a  class="detailDictamen"  style="display:none" href="javascript:void(0)" title="Dictamen Final">',
		            '<i class="tabla_registro_detalle_dictamen"  >.</i>',
		            '</a>'    
		    ].join(''); 			
		}
		else if( prioridad == 14 && cdRequerimiento != 0){
			return [
		            '<a class="delete" href="javascript:void(0)" title="Eliminar">',
		            '<i class="tabla_registro_eliminar">.</i>',
		            '</a>',
		            '<a class="detailLocalizacion" href="javascript:void(0)"  title="Localizacion Clientes">',
		            '<i class="glyphicon glyphicon-search icon-search"></i>',
		            '</a>',
		            '<a class="edit" href="javascript:void(0)" title="Actualizar">',
		            '<i class="tabla_registro_editar">&nbsp;</i>',
		            '</a>',
		            '<a  class="detailDictamen" href="javascript:void(0)" title="Dictamen Final">',
		            '<i class="tabla_registro_detalle_dictamen"  >.</i>',
		            '</a>'  
		    ].join(''); 			
		}else if( prioridad == 13){
			return [
		            '<a class="delete" href="javascript:void(0)" title="Eliminar">',
		            '<i class="tabla_registro_eliminar">.</i>',
		            '</a>',
		            '<a class="detailLocalizacion" href="javascript:void(0)"  title="Localizacion Clientes">',
		            '<i class="glyphicon glyphicon-search icon-search"></i>',
		            '</a>',
		            '<a class="edit" href="javascript:void(0)" title="Actualizar">',
		            '<i class="tabla_registro_editar">&nbsp;</i>',
		            '</a>',
		            '<a  class="detailDictamen"  style="display:none" href="javascript:void(0)" title="Dictamen Final">',
		            '<i class="tabla_registro_detalle_dictamen"  >.</i>',
		            '</a>',            
		    ].join(''); 
		}else if( prioridad == -1 || prioridad == '' || prioridad == null){
			return [
		            '<a class="delete" href="javascript:void(0)" title="Eliminar">',
		            '<i class="tabla_registro_eliminar">.</i>',
		            '</a>',
		            '<a class="detailLocalizacion" href="javascript:void(0)"  title="Localizacion Clientes">',
		            '<i class="glyphicon glyphicon-search icon-search"></i>',
		            '</a>',
		            '<a class="edit" href="javascript:void(0)" title="Actualizar">',
		            '<i class="tabla_registro_editar">&nbsp;</i>',
		            '</a>'
		    ].join(''); 
		}

	}	
	*/
	/*
	function formatterRegistroDescargaArchivo(value, row, index) {
		return [
	            '<a class="detailLocalizacion" href="javascript:void(0)"  title="Descargar Archivo">',
	            '<i class="glyphicon glyphicon-download-alt"></i>',
	            '</a>',
	            '<a class="delete" href="javascript:void(0)" title="Eliminar">',
	            '<i class="tabla_registro_eliminar">.</i>',
	            '</a>'	            
	    ].join(''); 
	}	
	*/
	/*
	function formatterDetailPersonasReportadas(value, row, index){
		
		return ['<table class="tableForm">'+		
		'<tr><td class="tdDetail"><b>Domicilio:</b>'+row.nbDomicilio+'</td></tr>'+								 
		'<tr><td class="tdDetail"><b>Complementarios:</b>'+row.nbComplementarios+'</td></tr>'+
		'<tr><td class="tdDetail"><b>Cliente Bancomer:</b>'+row.cdClienteBancomer+'</td></tr>'+
		'<tr><td class="tdDetail"><b>No. Cliente:</b>'+row.cdCliente+'</td></tr>'+									
		'<tr><td class="tdDetail"><b>No. Cuenta:</b>'+row.nuCuenta+'</td></tr>'+
		'<tr><td class="tdDetail"><b>Tipolog&iacute;a:</b>'+row.cdTipologia+'</td></tr>'+
		'<tr><td class="tdDetail"><b>Dictamen:</b>'+row.cdDictamen+'</td></tr>'+
		'</table>'].join('');

	}
	*/
	

	// ==================== VALIDACION DE FORMULARIOS ====================
	
	/**
	 * Valida los campos de captura de una persona reportada
	 */
	/*
	function validarCapturaPersonaReportada()
	{
		console.log( "Validaciones Persona Reportada Fisica" );
		$('#MSCAVE01014_Personas').validate( {
			rules: {
				auxNombre:{
					required: true,
					maxlength: 20
				},
				auxRazonSocial:{
					required: true,
					maxlength: 60
				},					
				auxAP:{
					required: false,
					maxlength: 20,
					onlyLetters:true
				},	
				auxAM:{
					required: false,
					maxlength: 20,
					onlyLetters:true
				},				
				auxDom:{
					required: false,
					maxlength: 20,
					lettersAndEspCar:true
				},		
				auxIdTipoP:{					
					notEqual: "-1",
					tipoPersonaRule: "Tipo de persona incorrecto."
				},				
				auxTipologia:{
					required: true,
					notEqual: "-1"
				},	
				auxRFC:{
					required: false,
					maxlength: 13,
					formatoRFCRule:true
				},	
				auxCaracter:{
					required: false,
					maxlength: 20,
					alfanumericos:true
				},	
				auxFechaN: {
					required: true,
					date:false
				},					
				auxNumCli:{
					required: false,
					maxlength: 8,
					alfanumericos: true
				},
				auxNumCuenta:{
					required: false,
					maxlength: 20,
					onlyNumbers:true
				},
				auxDictamen:{
					required: false,
					maxlength: 2
				},
				auxComp:{
					required: false,
					maxlength: 1000
				},				
			},
			messages: {
				auxNombre:{
					required: 'El campo Nombre / Raz\u00F3n Social es requerido.',
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres."
				},
				auxRazonSocial:{
					required: 'El campo Nombre / Raz\u00F3n Social es requerido.',
					maxlength: "La longitud m\u00E1xima del campo es de 60 caracteres."
				},					
				auxAP:{
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres.",
					onlyLetters: 'No se permiten caracteres num\u00E9ricos o especiales.'
				},	
				auxAM:{
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres.",
					onlyLetters: 'No se permiten caracteres num\u00E9ricos o especiales.'
				},				
				auxDom:{
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres.",
					lettersAndEspCar: "No se permiten caracteres num\u00E9ricos."
				},		
				auxIdTipoP:{
					notEqual: "Seleccione un tipo de persona",
					tipoPersonaRule: true
				},				
				auxTipologia:{
				},	
				auxRFC:{
					maxlength: "La longitud m\u00E1xima del campo es de 13 caracteres.",
					formatoRFCRule: "El formato del RFC es incorrecto."
				},	
				auxCaracter:{
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres.",
					alfanumericos: "No se permiten caracteres especiales."
				},	
				auxFechaN: {
					required: 'El campo Fecha Nacimiento es requerido.',
					date:false
				},					
				auxNumCli:{
					maxlength: "La longitud m\u00E1xima del campo es de 8 caracteres.",
					alfanumericos: "No se permiten caracteres especiales."
				},
				auxNumCuenta:{
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres.",
					onlyNumbers: "Solo se permiten caracteres num\u00E9ricos."
				},
				auxDictamen:{
					maxlength: "La longitud m\u00E1xima del campo es de 2 caracteres."
				},
				auxComp:{
					maxlength: "La longitud m\u00E1xima del campo es de 1000 caracteres.",
				}
	        },	
			errorPlacement: function (error, element) {
	            $(element).prop('style', 'border: 1px solid #C8175E;');
	        },
	        success: function (label, element) {
	            $(element).prop('style', 'border: 1px solid #86C82D;');
	        },
	        showErrors: function(errorMap, errorList) {
	            $.each(this.validElements(), function (index, element) {
	                var $element = $(element);
	                $(element).prop('style', 'border: 1px solid #86C82D;');
	                $element.data("title", "") // Clear the title - there is no error associated anymore
	                .tooltip("destroy");	                    
	            });
	            // Create new tooltips for invalid elements
	            $.each(errorList, function (index, error) {
	                var $element = $(error.element);
	                $element.prop('style', 'border: 1px solid #C8175E;');
	                $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
	                .data("title", error.message)
	                .tooltip();
	            });
	        },		        
		});
	}
	*/
	
	
	/**
	 * Valida el Requerimiento
	 * @returns
	 */
	/*
	function validarRequerimiento()
	{
		$('#MSCAVE01014').validate( {
			rules: {
				fhRecepcionReq: {
					required: true,
					date:false
				},
				nbTipo: {
					required: true,
					min:1
				},
				nuOficio: {
					required: false,
					digits: false,
					maxlength: 20
				},
				nuExpediente: {
					required: false,
					digits:false,
					maxlength: 20
				},
				nuFolio: {
					required: false,
					digits:true,
					maxlength: 20
				},
				nbDescripcion:{
					required: false,
					digits:false,
					maxlength: 60
				},
				nuDiasPlazo: {
					required: false,
					digits: true,
					maxlength: 3
				},
				nbAutoridad:{
					required: false,
					maxlength: 60
				},
				nbReferencia:{
					required: false,
					maxlength: 60
				},
				nbAseguramiento: {
					required: true,
					min: 1
				},
				fhPublicacion: {
					required: true,
					date:false
				},				
				nbSolicitudEspecifica: {
					maxlength: 5000
				}
			},
			messages: {
				fhRecepcionReq: {
					required: "El campo Fecha Recepci\u00F3n es requerido."
				},
				nbTipo: {
					required: "El campo Tipo es requerido.",
					min: "El campo Tipo es requerido."
				},
				nuOficio: {
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres."
				},
				nuExpediente: {
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres."
				},
				nuFolio: {
					digits: "Solo se permite introducir valores num\u00E9ricos.",
					maxlength: "La longitud m\u00E1xima del campo es de 20 caracteres."
				},
				nbDescripcion:{
					maxlength: "La longitud m\u00E1xima del campo es de 60 caracteres."
				},				
				nuDiasPlazo: {
					digits: "Solo se permite introducir valores num\u00E9ricos.",
					maxlength: "La longitud m\u00E1xima del campo es de 3 caracteres."
				},
				nbAutoridad:{
					maxlength: "La longitud m\u00E1xima del campo es de 60 caracteres."
				},
				nbReferencia:{
					maxlength: "La longitud m\u00E1xima del campo es de 60 caracteres."
				},				
				nbAseguramiento: {
					required: 'El campo Tiene Aseguramiento es requerido.',					
					notEqual: "Seleccione una opci\u00F3n.",
					min: "El campo Tiene Aseguramiento es requerido."
				}
				,
				fhPublicacion: {
					required: 'El campo Fecha Publicaci\u00F3n es requerido.'
				},
				nbSolicitudEspecifica: {
					maxlength: 'La longitud m\u00E1xima del campo es de 4000 caracteres.'
				}
	        },	
			errorPlacement: function (error, element) {
	            $(element).prop('style', 'border: 1px solid #C8175E;');
	        },
	        success: function (label, element) {
	            $(element).prop('style', 'border: 1px solid #86C82D;');
	        },	        
	        showErrors: function(errorMap, errorList) {
	            $.each(this.validElements(), function (index, element) {
	                var $element = $(element);
	                $(element).prop('style', 'border: 1px solid #86C82D;');
	                $element.data("title", "") // Clear the title - there is no error associated anymore
	                .tooltip("destroy"); // Create a new tooltip based on the error messsage we just set in the title
	                    
	            });
	            // Create new tooltips for invalid elements
	            $.each(errorList, function (index, error) {
	                var $element = $(error.element);
	                $element.prop('style', 'border: 1px solid #C8175E;');
	                $element.tooltip("destroy") // Destroy any pre-existing tooltip so we can repopulate with new tooltip content
	                .data("title", error.message)
	                .tooltip(); // Create a new tooltip based on the error messsage we just set in the title

	            });
	        }	  	               
		});
	}
	*/
	
	/**
	 * Valida el Requerimiento
	 * @returns
	 */
	/*
	function validarArchivo()
	{
		$('#MSCAVE01014_ArchivosAdjuntos').validate( {
			rules: {
				btnAttachment: {
					required: true,
					accept: "audio/*",
					maxlength: 20
				}
			},
			errorPlacement: function (error, element) {
	            $(element).prop('style', 'border: 1px solid #C8175E;');
	        },
	        success: function (label, element) {
	            $(element).prop('style', 'border: 1px solid #86C82D;');
	        }
		});
	}	
	*/
	
	// ==================== INVOCACION DE SERVICIOS ====================
	
	/**
	 * Invoca el servicio para dar de alta un nuevo cliente de alto riesgo
	 */
	function editarClienteAltoRiesgo(clienteRiesgo){
		return $.ajax({
			url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/editarClienteRiesgo",
			type : "PUT",
			contentType: 'application/json',
			dataType: 'json',
			data: JSON.stringify(clienteRiesgo),
			async: false
		});
	}
	
	/**
	 * Invoca el servicio para dar de alta un nuevo cliente de alto riesgo
	 */
	/*
	function nuevoClienteAltoRiesgoTransac(clienteRiesgo){
		var cdPrioridadAlta = $( '#cdPrioridad' ).val();	
		var cdUsuarioTmp = sessionStorage.cdUsuario;
		return $.ajax({
			url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/altaClienteRiesgoAutomatica/"+ cdPrioridadAlta + "/" + cdUsuarioTmp,
			type : "PUT",
			contentType: 'application/json',
			dataType: 'json',
			data: JSON.stringify(clienteRiesgo),
			async: false
		});
	}	
	*/
	/**
	 * Invoca el servicio para dar de alta una persona reportada de forma manual
	 */
	function nuevaPersonaReportada(personaReportada){
		console.log( "== EXAMINAR ==" );
		console.log( personaReportada );
		console.log( "CD Usuario : " + sessionStorage.cdUsuario );
		var cdUsuarioTmp = sessionStorage.cdUsuario;
		var cdPrioridadAlta = $( '#cdPrioridad' ).val();		
		console.log( "Prioridad : " + cdPrioridadAlta );
		return $.ajax({
			url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/altaPersonaReportada/" + cdPrioridadAlta + "/" + cdUsuarioTmp,
			type : "PUT",
			contentType: 'application/json',
			dataType: 'json',
			data: JSON.stringify(personaReportada),
			async: false
		});
	}
	
	/**
	 * Invoca el servicio para dar de alta una persona reportada de forma manual
	 */
	/*
	function nuevaPersonaReportadaTransac( listaPersonasReportadasAgregadas ){
		console.log( "== Persona Reportada Transac ==" );
		console.log( JSON.stringify( listaPersonasReportadasAgregadas ) );
		console.log( "CD Usuario : " + sessionStorage.cdUsuario );
		var cdUsuarioTmp = sessionStorage.cdUsuario;
		var cdPrioridadAlta = $( '#cdPrioridad' ).val();	
		return $.ajax({
			url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/altaPersonaReportadaTransac/" + cdPrioridadAlta + "/" + cdUsuarioTmp,
			type : "PUT",
			contentType: 'application/json',
			dataType: 'json',
			data: JSON.stringify( listaPersonasReportadasAgregadas),
			async: false
		});
	}	
	*/
	
	/**
	 * Actualiza la informaci�n de una persona reportada
	 * @param personaReportada
	 * @returns
	 */
	function actualizarPersonaReportada( personaReportada ){
		var _url = obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/actualizarPersonaReportada";	
		var _type = "PUT";	
		return $.ajax({
			url : _url,
			type : _type,
			contentType: 'application/json',
	        dataType: 'json',
	        async: false,
			data : JSON.stringify(personaReportada)
		});	
	}
	
	/**
	 * Invoca el servicio para adjuntar un archivo al Requerimiento
	 * @returns
	 */	
	/*
	function insertarArchivo( )
	{
		console.log( "== Attachment File Service ==" );
		var uploadObject = $("#btnAttachment");
		var nombreArchivo = uploadObject[0].files[0].name;
		console.log( "Nombre Archivo : " + nombreArchivo );
		var longitudNombre = nombreArchivo.length;
		console.log( "Longitud : " + longitudNombre );
		if( longitudNombre > LONGITUD_MAXIMA )
		{
			cargarMensaje( msgErrorLongitud,false,5);
		}
		else
		{
			var fileData = new FormData();
			fileData.append('file', uploadObject[0].files[0]);
			console.log( "Usuario : " + sessionStorage.cdUsuario );
			var cdUsuarioCreacion = sessionStorage.cdUsuario;
			console.log( "No. Folio Archivo : " + nuFolioArchivo );	
			if( nuFolioArchivo != null)
			{
				console.log( ">> Ejecutar Servicio..." );
				return $.ajax({
					url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/adjuntarArchivo/" + nuFolioArchivo + "/" + nombreArchivo + "/" + cdUsuarioCreacion,
					type : "POST",
					cache:false,
					contentType: 'multipart/form-data',
					data: fileData,
					processData: false,
					async: false
				});			
			}			
		}
		console.log( "== Finaliza Invocacion de Servicio para Adjuntar Archivos ==" );
	}
	*/
	
	/**
	 * Invoca el servicio para eliminar un archivo asociado al requerimiento
	 */
	/*
	function eliminarArchivo(){
		return $.ajax({
			url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/eliminarArchivo/" + cdFolioArchivoDelete,
			type : "DELETE",
			contentType: 'application/json',
			dataType: 'json',
			async: false
		});
	}	
	*/
	
	/**
	 * Invoca el servicio para eliminar un archivo asociado al requerimiento
	 */
	function eliminarPersonaReportada( cdCliAltRie, nuId ){
		return $.ajax({
			url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/eliminarPersonaReportada/" + cdCliAltRie + "/" + nuId,
			type : "DELETE",
			contentType: 'application/json',
			dataType: 'json',
			async: false
		});
	}	
	
	
	/**
	 * Invoca el servicio para adjuntar un archivo al Requerimiento
	 * @returns
	 */
	/*
	function insertarArchivo( )
	{
		console.log( "== Invocar servicio insertar archivo ==" );
		var uploadObject = $("#btnAttachment");
		var fileData = new FormData();
		fileData.append('file', uploadObject[0].files[0]);
		console.log( "Archivo : " + uploadObject[0].files[0].name );
		if( nuFolioArchivo != null)
		{
			return $.ajax({
				url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/adjuntarArchivo/" + nuFolioArchivo + "/" + uploadObject[0].files[0].name,
				type : "POST",
				dataType : 'json',
				contentType: 'application/json',
				data: fileData,
				processData: false,
				async: false
			});	
		}
	}	
	*/
	
	function consultarArchivos(){
		var _url = obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/consultarArchivos/" + nuFolioArchivo		
		 return $.ajax({
			url : _url,
			type : 'GET',
			contentType: 'application/json',
			dataType: 'json',
			async: false
		});
	}
	
	function consultarPersonasReportadas(){
		var _url = obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/consultarPersonasReportadas/" + cdRequerimiento		
		 return $.ajax({
			url : _url,
			type : 'GET',
			contentType: 'application/json',
			dataType: 'json',
			async: false
		});
	}	
	
	function consultarRequerimiento( cdRequerimiento ){
		console.log( "CD Requerimiento : " + cdRequerimiento );
		var _url = obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/consultarRequerimiento/" + cdRequerimiento		
		 return $.ajax({
			url : _url,
			type : 'GET',
			contentType: 'application/json',
			dataType: 'json',
			async: false
		});
	}		
	
	
	/**
	 * Invoca el servicio para consultar las prioridades de acuerdo al tipo
	 * @param value
	 * @returns
	 */
	function obtenerPrioridad(value){
		 return $.ajax({
			url : obtenerDeSessionStorage("baseUrl", true) + "/TipoPrioridadService/consultarPrioridadPorTipo/"+value,
			type : 'GET',
			contentType: 'application/json',
			dataType: 'json',
			async: false
		});
	}
	
	// ==================== CONFIGURACION DE TABLAS ====================
	function llenadoTablaArchivosAdjuntos(data) {
		$('#tablaArchivosAdjuntos').bootstrapTable({
			pagination: true,
			pageSize: 5,
			pageList: [5, 25, 50],
			data: data,
			clickToSelect: false,
			singleSelect: false,
			maintainSelected: true,
			sortable: true,
			checkboxHeader: false,
			formatShowingRows: function (pageFrom, pageTo, totalRows) {
				return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
			},
			formatRecordsPerPage: function (pageNumber) {
				return pageNumber + ' registros por p\u00e1gina';
			},
			formatLoadingMessage: function () {
				return 'Cargando, espere por favor...';
			},
			formatSearch: function () {
				return 'Buscar';
			},
			formatNoMatches: function () {
				return 'No se encontr&oacute; informaci&oacute;n';
			}      
		});
	}
	
	/**
	 * Configuracion Inicial Tabla de Personas Reportadas
	 */
	function llenadoTablaPersonaReportada(data) {
		$('#tablaPersonaReportada').bootstrapTable({
			pagination: true,
			pageSize: 5,
			pageList: [5, 25, 50],
			data: data,
			clickToSelect: false,
			singleSelect: false,
			maintainSelected: true,
			sortable: true,
			checkboxHeader: false,
			formatShowingRows: function (pageFrom, pageTo, totalRows) {
				return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
			},
			formatRecordsPerPage: function (pageNumber) {
				return pageNumber + ' registros por p\u00e1gina';
			},
			formatLoadingMessage: function () {
				return 'Cargando, espere por favor...';
			},
			formatSearch: function () {
				return 'Buscar';
			},
			formatNoMatches: function () {
				return 'No se encontr&oacute; informaci&oacute;n';
			}      
		});
	}
		
	// ==================== LIMPIAR CAMPOS ====================
	function limpiarFormularioRequerimiento()
	{
		console.log( "== Limpiar Formulario Requerimiento ==" );
		$('#nuOficio').val( '' );
		$('#nuExpediente').val( '' );
		$('#nuFolio').val( '' );
		$('#nbDescripcion').val( '' );
		$('#fhPublicacion').val( '' );
		$('#nuDiasPlazo').val( '' );
		$('#nbAutoridad').val( '' );
		$('#nbReferencia').val( '' );
		$('#nbAseguramiento').val( '-1' );
		$('#nbSolicitudEspecifica').val( '' );
	}
	
	function limpiarFormularioFuente()
	{
		console.log( "== Limpiar Formulario Requerimiento ==" );
		$('#nbTipo').val( '-1' );
		$('#nbPrioridad').val( '' );
		$('#cdPrioridad').val( '' );
		$('#fhRecepcionReq').val( '' );
	}	
	
	/**
	 * Limpia la pantalla Alta de Clientes de Riesgo
	 */
	function limpiarFormularioPersonaRiesgo()
	{
		console.log( "== Limpiar Formulario Persona Riesgo ==" );
		$('#auxNombre').hide(  );
		$('#auxRazonSocial').show(  );
		$('#auxNombre').val( '' );
		$('#auxRazonSocial').val( '' );
		$('#auxAP').val( '' );
		$('#auxAM').val( '' );
		$('#auxFechaN').val( '' );
		$('#auxDom').val( '' );
		$('#auxIdTipoP').val( '' );
		$('#auxTipologia').val( '' );
		$('#auxRFC').val( '' );
		$('#auxCaracter').val( '' );
		$("#auxIdTipoP option:first").prop("selected", "selected");
		$("#auxTipologia option:first").prop("selected", "selected");
		$('#auxNumCli').val( '' );
		$('#auxNumCuenta').val( '' );
		$('#auxDictamen').val( '' );
		$('#auxComp').val( '' );
		var $radios = $('input:radio[name=esCliente]');
		$radios.filter('[value=SI]').prop('checked', true);
	}	
	
	function getCdTipoPersona( nbTipoPersona ){
		var cdTipoPersona;
		if( nbTipoPersona == DET_TP_PERSONA_FISICA )
		{
			cdTipoPersona = CD_TP_PERSONA_FISICA;
		}
		else if( nbTipoPersona == DET_TP_PERSONA_MORAL ){
			cdTipoPersona = CD_TP_PERSONA_MORAL;
		}
		else if( nbTipoPersona == DET_TP_PERSONA_GOBIERNO ){
			cdTipoPersona = CD_TP_PERSONA_GOBIERNO;
		}
		return cdTipoPersona
	}
	
	// ==================== CREACION DE OBJETOS ====================	
	/**
	 * Crea un nuevo objeto de tipo Cliente de Alto Riesgo
	 */
	/*
	function creacionClienteRiesgo()
	{
		console.log( "== CREA OBJETO CLIENTE DE ALTO RIESGO ==" );
		clienteRiesgo = new Object();
		
		clienteRiesgo.nbTipo = $("#nbTipo").val().toUpperCase();
		clienteRiesgo.fhRecepcion =$("#fhRecepcionReq").datepicker('getDate');
		console.log( 'Fecha : ' + $("#fhRecepcionReq").datepicker('getDate') );
		clienteRiesgo.nuOficio =  $("#nuOficio").val().toUpperCase();
		clienteRiesgo.nuExpediente =  $("#nuExpediente").val().toUpperCase();
		clienteRiesgo.nuFolio = $('#nuFolio').val().toUpperCase(); 
		clienteRiesgo.nbDescripcion= $('#nbDescripcion').val().toUpperCase();
		clienteRiesgo.fhPublicacion = crearFecha($("#fhPublicacion").val());
		clienteRiesgo.nuDiasPlazo = $('#nuDiasPlazo').val(); 
		clienteRiesgo.nbAutoridad = $('#nbAutoridad').val().toUpperCase();
		clienteRiesgo.nbReferencia = $('#nbReferencia').val().toUpperCase();
		clienteRiesgo.nbAseguramiento = $('#nbAseguramiento').val().toUpperCase();
		clienteRiesgo.nbSolicitudEspecifica = $('#nbSolicitudEspecifica').val().toUpperCase();
		
		console.log( "== CLIENTE DE ALTO RIESGO ==" );
		console.log( JSON.stringify( clienteRiesgo ) );		
		return clienteRiesgo;
	}	
	*/
	/*
	function creacionClienteRiesgoAutom()
	{
		console.log( "== CREA OBJETO CLIENTE DE ALTO RIESGO ==" );
		clienteRiesgo = new Object();
		
		clienteRiesgo.nbTipo = $("#nbTipo").val().toUpperCase();
		clienteRiesgo.fhRecepcion =$("#fhRecepcionReq").datepicker('getDate');
		console.log( 'Fecha : ' + $("#fhRecepcionReq").datepicker('getDate') );
		clienteRiesgo.nuOficio =  $("#nuOficio").val().toUpperCase();
		clienteRiesgo.nuExpediente =  $("#nuExpediente").val().toUpperCase();
		clienteRiesgo.nuFolio = $('#nuFolio').val().toUpperCase(); 
		clienteRiesgo.nbDescripcion= $('#nbDescripcion').val().toUpperCase();
		clienteRiesgo.fhPublicacion = crearFecha($("#fhPublicacion").val());
		clienteRiesgo.nuDiasPlazo = $('#nuDiasPlazo').val(); 
		clienteRiesgo.nbAutoridad = $('#nbAutoridad').val().toUpperCase();
		clienteRiesgo.nbReferencia = $('#nbReferencia').val().toUpperCase();
		clienteRiesgo.nbAseguramiento = $('#nbAseguramiento').val().toUpperCase();
		clienteRiesgo.nbSolicitudEspecifica = $('#nbSolicitudEspecifica').val().toUpperCase();
		
		// Lista de Personas Reportadas
		var $table = $('#tablaPersonaReportada');
		clienteRiesgo.lstClientesRiesgo = $table.bootstrapTable('getData');
		return clienteRiesgo;
	}	
	*/
	
	/**
	 * Lee los datos para agregar una nueva persona a la tabla de Personas Reportadas
	 * @returns
	 */
	/*
	function crearObjetoPR(){
		console.log( "== Crear objeto persona reportada ==" );
		contador += 1;
		var rdbEscliente = $('input[name=esCliente]');
		var checkedValue = rdbEscliente.filter(':checked').val();		
		var personaReportada = new Object();
		personaReportada.cdClialtrie = cdRequerimiento;
		personaReportada.nuId = contador;
		personaReportada.cdCaracter = $("#auxCaracter").val().toUpperCase();
		personaReportada.cdTpPersona = $("#auxIdTipoP").val().toUpperCase();
		personaReportada.nbTpPersona = $("#auxIdTipoP").val().toUpperCase();
		var tpPersona = personaReportada.cdTpPersona;
		if( tpPersona == CD_TP_PERSONA_MORAL || tpPersona == CD_TP_PERSONA_GOBIERNO )
		{
			personaReportada.nbNombre = $("#auxRazonSocial").val().toUpperCase();
		}else if( tpPersona == CD_TP_PERSONA_FISICA  )
		{
			personaReportada.nbNombre = $("#auxNombre").val().toUpperCase();
			personaReportada.nbApPaterno = $("#auxAP").val().toUpperCase();
			personaReportada.nbApMaterno = $("#auxAM").val().toUpperCase();			
		}
		var fhNacimientoTmp = $("#auxFechaN").datepicker('getDate');
		personaReportada.fhNacimiento = fhNacimientoTmp;
		personaReportada.cdRFC = $("#auxRFC").val().toUpperCase();       
		personaReportada.nbDomicilio = $('#auxDom').val().toUpperCase();  
		personaReportada.nbComplementarios = $('#auxComp').val().toUpperCase();
		personaReportada.cdTipologia = $('#auxTipologia').val().toUpperCase();               
		personaReportada.cdClienteBancomer = checkedValue;
		personaReportada.cdCliente = $('#auxNumCli').val().toUpperCase();
		personaReportada.nuCuenta = $('#auxNumCuenta').val().toUpperCase();
		personaReportada.cdDictamen = $('#auxDictamen').val().toUpperCase();
		console.log( "== PERSONA REPORTADA OBJ == " );
		console.log( JSON.stringify( personaReportada ) );
		return personaReportada;
	}
	*/
	/*
	function crearObjetoPREdit(){
		console.log( "== Crear objeto persona reportada ==" );
		var rdbEscliente = $('input[name=esCliente]');
		var checkedValue = rdbEscliente.filter(':checked').val();		
		var personaReportada = new Object();
		personaReportada.cdClialtrie = cdRequerimiento;
		personaReportada.nuId = $("#nuIdHdn").val().toUpperCase()
		personaReportada.cdCaracter = $("#auxCaracter").val().toUpperCase();
		personaReportada.cdTpPersona = $("#auxIdTipoP").val().toUpperCase();
		personaReportada.nbTpPersona = $("#auxIdTipoP").val().toUpperCase();
		var tpPersona = personaReportada.cdTpPersona;
		if( tpPersona == CD_TP_PERSONA_MORAL || tpPersona == CD_TP_PERSONA_GOBIERNO )
		{
			personaReportada.nbNombre = $("#auxRazonSocial").val().toUpperCase();
		}else if( tpPersona == CD_TP_PERSONA_FISICA  )
		{
			personaReportada.nbNombre = $("#auxNombre").val().toUpperCase();
			personaReportada.nbApPaterno = $("#auxAP").val().toUpperCase();
			personaReportada.nbApMaterno = $("#auxAM").val().toUpperCase();			
		}
		var fhNacimientoTmp = $("#auxFechaN").datepicker('getDate');	
		personaReportada.fhNacimiento = fhNacimientoTmp;
		personaReportada.cdRFC = $("#auxRFC").val().toUpperCase();       
		personaReportada.nbDomicilio = $('#auxDom').val().toUpperCase();  
		personaReportada.nbComplementarios = $('#auxComp').val().toUpperCase();
		personaReportada.cdTipologia = $('#auxTipologia').val().toUpperCase();               
		personaReportada.cdClienteBancomer = checkedValue;
		personaReportada.cdCliente = $('#auxNumCli').val().toUpperCase();
		personaReportada.nuCuenta = $('#auxNumCuenta').val().toUpperCase();
		personaReportada.cdDictamen = $('#auxDictamen').val().toUpperCase();
		console.log( "== PERSONA REPORTADA OBJ == " );
		console.log( JSON.stringify( personaReportada ) );
		return personaReportada;
	}
	*/
		
	// ==================== MODAL'S ====================	
	
	/**
	 * Muestra el modal para confirmar el alta del cliente de alto riesgo
	 */
	/*
	function abrirModalConfirmaClienteRiesgo(){
		$('#dialogComfirmaRiesgoClientes').modal('show');
	}
	*/
	// ==================== ELIMINAR PERSONA REPORTADA ====================	
	
	/**
	 * Elimina un registro de la tabla personas reportadas
	 */
	/*
	function removeRowPersonaReportada(){
		console.log( "== Remove Row Personas Reportadas : " + idPersonaReportada );
		$('#tablaPersonaReportada').bootstrapTable('remove', {
			field: 'nuId',
			values: [idPersonaReportada]
		});		
		// Valida si existen registros en la tabla, si no existen registros deshabilita el boton de Guardar
		arregloPersonasReportadas = $('#tablaPersonaReportada').bootstrapTable('getData');
		if( arregloPersonasReportadas !=null && arregloPersonasReportadas.length > 0 )
		{
			console.log( "Arreglo Personas D : " + JSON.stringify( arregloPersonasReportadas ) );
		}
		else
		{
			deshabilitarBotonGuardar();
		}
	}
	*/	
	
	// ==================== INICIAR PANTALLA ====================
	function iniciarPantalla()
	{
		console.log( 'Iniciar Pantalla...' );
		
		limpiarFormularioRequerimiento();
		iniciarPantalla();
		var cdRequerimiento = clienteAREdit.cdRequerimiento;
		// Consulta el expediente recibido		
		consultarRequerimiento( cdRequerimiento ).done(function(data) {
			if( cargarMensaje(data,false)){				
				console.log( "Requerimiento : " + JSON.stringify( data.response ) );
			}			
	 	});
		/*
		deshabilitarFormularioRequerimiento();
		deshabilitarBotonGuardar();
		deshabilitarBotonAddPersona();
		deshabilitarBotonImportar();
		deshabilitarBotonArchivosAdjuntos();
		deshabilitarFormularioRequerimiento();
		deshabilitarBotonUploadFile();
		deshabilitarBotonCancelUpload();
		deshabilitarBotonLocalizacionCliente();	
		
		// Oculta Elementos Personas Reportadas
		hideBtnLocalizacionCliente();
		hideBtnAddPersonasReportadas();
		hideTitleTablaPersonasReportadas();
		hideTablaPersonasReportadas();
		hideBtnAdjuntarArchivos();
		hideTitleTablaArchivosAdjuntos();
		hideTablaArchivosAdjuntos();
		
		// Establece propiedad para que el campo solicitud especifica no cambie de tama�o		
		$('#nbSolicitudEspecifica').click(function () {
		    $(this).css('resize', 'none');
		});				
		
		
		arregloPersonas = new Array();
		idTipoPersona = null;
		indiceEliminar = null;
		cdFolioArchivoDelete = null;
		nuFolioArchivo = null;
		cdRequerimiento = 0;
		contador = 0;
		
		console.log( "Limpiar Tablas" );
		$('#tablaPersonaReportada').bootstrapTable('load',[]);
		$('#tablaArchivosAdjuntos').bootstrapTable('load',[]);	
		$('#nbTipo').prop("disabled", true);
		deshabilitarFormularioTipoFuente();
	
		consultarArchivos(  ).done(function(data) {
			if( cargarMensaje(data,false)){				
				console.log( "Resultado : " + JSON.stringify( data.response ) );
				$('#tablaArchivosAdjuntos').bootstrapTable( 'load', data.response );
			}			
	 	});
		*/
	  		
	}
	
	// ==================== HABILITAR/DESHABILITAR COMPONENTES ====================		
	
	/**
	 * Habilitar el Boton Guardar
	 */
	/*
	function habilitarBotonGuardar(){
		console.log( 'Habilita Boton Guardar..' );		
		$('#btnGuardar').prop("disabled", false);
	}
	*/
	
	/**
	 * Deshabilita el Boton Guardar
	 * @returns
	 */
	/*
	function deshabilitarBotonGuardar(){
		console.log( 'Deshabilita Boton Guardar..' );
		$('#btnGuardar').prop("disabled", true);
	}
	 */
	/**
	 * Habilita el Boton Agregar Persona
	 * @returns
	 */
	/*
	function habilitarBotonAddPersona(){
		console.log( 'Habilita Boton Agregar Persona..' );		
		$('#btnAgregar').prop("disabled", false);
	}
	*/
	/**
	 * Desabilita el Boton Agregar Persona
	 * @returns
	 */
	/*
	function deshabilitarBotonAddPersona(){
		console.log( 'Deshabilita Boton Agregar Persona..' );		
		$('#btnAgregar').prop("disabled", true);
	}	
	*/	
	
	/**
	 * Habilita el Boton Archivos Adjuntos
	 * @returns
	 */
	/*
	function habilitarBotonArchivosAdjuntos(){
		console.log( 'Habilita Boton Archivos Adjuntos..' );		
		$('#btnAdjuntarArchivos').prop("disabled", false);
	}
	*/
	/**
	 * Desabilita el Boton Adjuntar Archivo
	 * @returns
	 */
	/*
	function deshabilitarBotonArchivosAdjuntos(){
		console.log( 'Deshabilita Boton Archivos Adjuntos..' );		
		$('#btnAdjuntarArchivos').prop("disabled", true);
	}
	*/	
	
	/**
	 * Habilita el Boton Archivos Adjuntos
	 * @returns
	 */
	/*
	function habilitarBotonImportar(){
		console.log( 'Habilita Boton Importar..' );	
	}
	*/
	
	/**
	 * Desabilita el Boton Adjuntar Archivo
	 * @returns
	 */
	/*
	function deshabilitarBotonImportar(){
		console.log( 'Deshabilita Boton Importar..' );	
		$('#btnImportar').prop("disabled", true);
        if ($('#btnImportar').attr('disabled')) {
        	console.log( "El atributo disabled si existe" );
        	$('#btnImportar').removeAttr('disabled')
        } else {
        	console.log( "El atributo disabled no existe" );
        }
	}
	*/
	
	/**
	 * Habilita el Boton Upload File
	 * @returns
	 */
	/*
	function habilitarBotonUploadFile(){
		console.log( 'Habilita Boton Upload File..' );		
		$('#btnUploadFile').prop("disabled", false);
	}
	*/
	/**
	 * Desabilita el Boton Upload File
	 * @returns
	 */
	/*
	function deshabilitarBotonUploadFile(){
		console.log( 'Deshabilita Boton Upload File..' );		
		$('#btnUploadFile').prop("disabled", true);
	}
	*/		
	
	/**
	 * Habilita el Boton Cancel Upload
	 * @returns
	 */
	/*
	function habilitarBotonCancelUpload(){
		console.log( 'Habilita Boton Cancel Upload..' );		
		$('#btnCancelUpload').prop("disabled", false);
	}
	*/
	
	/**
	 * Desabilita el Boton Cancel Upload
	 * @returns
	 */
	/*
	function deshabilitarBotonCancelUpload(){
		console.log( 'Deshabilita Boton Cancel Upload..' );		
		$('#btnCancelUpload').prop("disabled", true);
	}	
	*/
	
	/**
	 * Habilita el Boton Localizacion del Cliente
	 * @returns
	 */
	/*
	function habilitarBotonLocalizacionCliente(){
		console.log( 'Habilita Boton Localizacion del Cliente' );	
		$('#btnLocalizacionCliente').prop("disabled", false);
	}
	*/
	
	/**
	 * Desabilita el Boton Localizacion del Cliente
	 * @returns
	 */
	/*
	function deshabilitarBotonLocalizacionCliente(){
		console.log( 'Deshabilita Localizacion del Cliente' );		
		$('#btnLocalizacionCliente').prop("disabled", true);		
	}	
	*/	
	
	// ==================== MOSTRAR / OCULTAR ====================	
		
	/**
	 * Oculta Boton Agregar Persona Reportada Edit / Save
	 */
	/*
	function hideBtnAgregarPersonaSave()
	{
		console.log( "== Oculta Boton Agregar Persona Reportada Save ==" );
		$( "#btnEditgregarPersona" ).hide();
	}
	*/
	
	/**
	 * Muestra Boton Agregar Persona Reeportada Edit/ Save
	 * @returns
	 */
	/*
	function showBtnAgregarPersonaSave()
	{
		console.log( "== Muestra Boton Agregar Persona Reportada Save ==" );
		$( "#btnEditgregarPersona" ).show();		
	}
	*/
	
	/**
	 * Oculta Boton Agregar Persona Reportada / Save
	 */
	/*
	function hideBtnAgregarPersonaSave()
	{
		console.log( "== Oculta Boton Agregar Persona Reportada Save ==" );
		$( "#btnAgregarPersona" ).hide();
	}
	*/
	
	/**
	 * Muestra Boton Agregar Persona Reportada / Save
	 * @returns
	 */
	/*
	function showBtnAgregarPersonaSave()
	{
		console.log( "== Muestra Boton Agregar Persona Reportada Save ==" );
		$( "#btnAgregarPersona" ).show();		
	}
	*/
	
	/**
	 * Oculta Boton Localizacion del cliente
	 */
	/*
	function hideBtnLocalizacionCliente()
	{
		console.log( "== Oculta Boton Localizacion Cliente ==" );
		$( "#btnLocalizacionCliente" ).hide();
	}
	*/
	
	/**
	 * Muestra Boton Localizacion del cliente
	 * @returns
	 */
	/*
	function showBtnLocalizacionCliente()
	{
		console.log( "== Muestra Boton Localizacion Cliente ==" );
		$( "#btnLocalizacionCliente" ).show();		
	}
	*/
	
	/**
	 * Oculta Boton Add Personas Reportadas
	 * @returns
	 */
	/*
	function hideBtnAddPersonasReportadas()
	{
		console.log( "== Oculta Boton Add Personas Reportadas ==" );
		$( "#btnAgregar" ).hide();		
	}
	*/
	
	/**
	 * Muestra Boton Add Personas Reportadas
	 * @returns
	 */
	/*
	function showBtnAddPersonasReportadas()
	{
		console.log( "== Muestra Boton Add Personas Reportadas ==" );
		$( "#btnAgregar" ).show();		
	}
	*/
	
	/**
	 * Oculta titulo tabla personas reportadas
	 * @returns
	 */
	/*
	function hideTitleTablaPersonasReportadas()
	{
		console.log( "== Oculta Titulo Tabla Personas Reportadas ==" );
		$( "#titlePersonasReportadas" ).hide();			
	}
	*/
	
	/**
	 * Muestra titulo tabla personas reportadas
	 * @returns
	 */
	/*
	function showTitleTablaPersonasReportadas()
	{
		console.log( "== Muestra Titulo Tabla Personas Reportadas ==" );
		$( "#titlePersonasReportadas" ).show();					
	}
	*/	
	
	/**
	 * Oculta tabla personas reportadas
	 * @returns
	 */
	/*
	function hideTablaPersonasReportadas()
	{
		console.log( "== Oculta Tabla Personas Reportadas ==" );
		$( "#PersonasReportadas" ).hide();			
	}
	*/
	
	/**
	 * Muestra tabla personas reportadas
	 * @returns
	 */
	/*
	function showTablaPersonasReportadas()
	{
		console.log( "== Muestra Tabla Personas Reportadas ==" );
		$( "#PersonasReportadas" ).show();					
	}	
	*/
	
	/**
	 * Oculta Boton Adjuntar Archivos
	 * @returns
	 */
	/*
	function hideBtnAdjuntarArchivos()
	{
		console.log( "== Oculta Boton Adjuntar Archivos ==" );
		$( "#btnAdjuntarArchivos" ).hide();		
	}
	*/
	
	/**
	 * Muestra Boton Adjuntar Archivos
	 * @returns
	 */
	/*
	function showBtnAddAdjuntarArchivos()
	{
		console.log( "== Muestra Boton Adjuntar Archivos ==" );
		$( "#btnAdjuntarArchivos" ).show();		
	}
	*/
		
	/**
	 * Oculta titulo Archivos adjuntos
	 * @returns
	 */
	/*
	function hideTitleTablaArchivosAdjuntos()
	{
		console.log( "== Oculta Titulo Tabla Archivos Adjuntos ==" );
		$( "#titleArchivosAdjuntos" ).hide();			
	}
	*/
	
	/**
	 * Muestra titulo tabla personas reportadas
	 * @returns
	 */
	/*
	function showTitleTablaArchivosAdjuntos()
	{
		console.log( "== Muestra Titulo Tabla Archivos adjuntos ==" );
		$( "#titleArchivosAdjuntos" ).show();					
	}
	*/
	
	/**
	 * Oculta tabla archivos adjuntos
	 * @returns
	 */
	/*
	function hideTablaArchivosAdjuntos()
	{
		console.log( "== Oculta Tabla Archivos Adjuntos ==" );
		$( "#tablaArchivosAdjuntos" ).hide();			
	}
	*/
	
	/**
	 * Muestra tabla archivos adjuntos
	 * @returns
	 */
	/*
	function showTablaArchivosAdjuntos()
	{
		console.log( "== Muestra Tabla Archivos adjuntos ==" );
		$( "#tablaArchivosAdjuntos" ).show();					
	}	
	*/
	
	
	// ==================== ACCIONES BOTONES ====================
	/*
	$("input[name='esCliente']").click(function() {
	    console.log( "Valor : " + this.value );
	    if( this.value == ES_CLIENTE_SI ){
	    	habilitarEsClienteBancomerSI();
	    }else if( this.value == ES_CLIENTE_NO ){
	    	habilitarEsClienteBancomerNO();
	    }
	});	
	*/
	/*
	$('#btnCleanScreen').click(function (e) {
		cleanScreen();
	});
	*/
	/*
	function cleanScreen(){
		console.log( "::: Limpiar Pantalla :::" );
		$('#nbTipo').val( "-1" );
		$('#nbPrioridad').val( "" );
		$('#cdPrioridad').val( "" );
		$( '#fhRecepcionReq' ).datepicker( 'setDate', 'today' )
		$( '#fhRecepcionReq' ).datepicker('setEndDate', 'today');
		$( '#fhPublicacion' ).datepicker('setEndDate', 'today');
		$( '#auxFechaN' ).datepicker('setEndDate', 'today');
		iniciarPantalla();	
		deshabilitarFormularioRequerimiento();
		$( "#btnNuevo" ).prop( "disabled", false);
		$( "#btnImportar" ).prop( "disabled", false);
	}
	*/
	/*
	$('#btnNuevo').click(function (e) {
		iniciarPantalla();
		habilitarFormularioRequerimiento();
		limpiarFormularioRequerimiento();
		habilitarBotonAddPersona();
		habilitarFormularioTipoFuente();
		$('#tablaArchivosAdjuntos').bootstrapTable('load',[]);
		cdRequerimiento = 0;
		nuFolioArchivo = null;
		$('#MSCAVE01014').valid();		
		$('#nuOficio').focus();
		contador = 0;
		habilitarBotonGuardar();
		TP_ALTA = TP_ALTA_MANUAL;
	});
	*/
	/*
	$('#btnGuardar').click(function (e) {
		console.log( "Valida Datos Cliente de Alto Riesgo" );
		cdRequerimiento = 0;
		nuFolioArchivo = null;
		var isValid = $('#MSCAVE01014').valid(); 
		console.log( "Formulario Valido : " + isValid );
		if(isValid){
			console.log( "== El cliente es valido, continua el alta del requerimiento ==" );
			console.log( "== Valida fecha de recepcion ==" );
			
			var fechaDia = new Date();
		    fechaDia.setHours(0);
		    fechaDia.setMinutes(0);
		    fechaDia.setSeconds(0);
			console.log( "Fecha Dia : " + fechaDia );
			
			var fechaRecepcionTmp = $("#fhRecepcionReq").datepicker('getDate');	
			fechaRecepcionTmp.setHours(0);
			fechaRecepcionTmp.setMinutes(0);
			fechaRecepcionTmp.setSeconds(0);			
			console.log( "Fecha Recepcion : " + fechaRecepcionTmp );
			if( fechaRecepcionTmp > fechaDia ){
				console.log( "== Mostrar mensaje de error, la fecha de recepcion no puede ser mayor a la fecha actual ==" );
				cargarMensaje( msgErrorFechaRecepcion, false, 5 );
			}
			else
			{
				//console.log( "== Continuar proceso de alta ==" );

				if( TP_ALTA == TP_ALTA_MANUAL ){
					//console.log( ">> ALTA MANUAL <<");
					objetoCliente = creacionClienteRiesgo();
					nuevoClienteAltoRiesgo(objetoCliente).done(function(data)	{
						cargarMensaje(data,true);
						cdRequerimiento = data.response.cdClialtrie;		
						nuFolioArchivo = data.response.nuFolioArchivo;
						console.log( "ID Requerimiento : " + data.response.cdClialtrie );
						console.log( "Nu Folio Archivo : " + data.response.nuFolioArchivo );
						deshabilitarBotonGuardar();
						showBtnLocalizacionCliente();
						showBtnAddPersonasReportadas();
						showTitleTablaPersonasReportadas();
						showTablaPersonasReportadas();
						showBtnAddAdjuntarArchivos();
						showTitleTablaArchivosAdjuntos();
						showTablaArchivosAdjuntos();
						habilitarBotonLocalizacionCliente();
						habilitarBotonArchivosAdjuntos();
						$('#nbTipo').prop("disabled", true);
						$('#fhRecepcionReq').prop("disabled", true);
						deshabilitarFormularioRequerimiento();
						deshabilitarFormularioTipoFuente();
					});
					console.log( "== Finaliza Alta de Cliente de Alto RiesgoManual ==" );	
				}else if( TP_ALTA == TP_ALTA_AUTOMATICA )
				{
					console.log( ">> ALTA AUTOMATICA <<" );
					objetoCliente = creacionClienteRiesgoAutom();
					// Valida que todos las personas reportadas tengan asignado una tipologia
					
					var tieneTipologia = false;
					$.each( objetoCliente.lstClientesRiesgo, function(index, val) {						
						console.log( "PR : " + JSON.stringify(val) );
						if( val.cdTipologia == '' || val.cdTipologia == null || val.cdTipologia == undefined){
							console.log( "No tiene tipologia" );
							tieneTipologia = false;
							return false;
						}
						else{
							console.log( "Si tiene tipologia" );
							tieneTipologia=true;
							return true;
						}
					});						
					if( tieneTipologia == true){
						nuevoClienteAltoRiesgoTransac(objetoCliente).done(function(data)	{
							cargarMensaje(data,true);
							cdRequerimiento = data.response.cdClialtrie;		
							nuFolioArchivo = data.response.nuFolioArchivo;
							console.log( "ID Requerimiento : " + data.response.cdClialtrie );
							console.log( "Nu Folio Archivo : " + data.response.nuFolioArchivo );
							consultarPersonasReportadas().done(function(data) {
								$('#tablaPersonaReportada').bootstrapTable('load',data.response);
								deshabilitarBotonGuardar();
								showBtnLocalizacionCliente();
								showBtnAddPersonasReportadas();
								showTitleTablaPersonasReportadas();
								showTablaPersonasReportadas();
								showBtnAddAdjuntarArchivos();
								showTitleTablaArchivosAdjuntos();
								showTablaArchivosAdjuntos();
								habilitarBotonLocalizacionCliente();
								habilitarBotonArchivosAdjuntos();
								deshabilitarFormularioRequerimiento();
								deshabilitarFormularioTipoFuente();							
							});						
						});
						console.log( "== Finaliza Alta de Cliente de Alto Riesgo Automatica ==" );							
					}
					else{
						cargarMensaje( msgErrorFaltanTipologias,false,5);
					}
				}							
			}

		}		
	});
	*/
	/*
	$('#btnAgregar').click(function (e) {
		console.log( "== Abre el modal para agregar una nueva persona ==" );
		limpiarFormularioPersonaRiesgo();
		deshabilitarFormularioPersona();
		$('#MSCAVE01014_Personas').modal( 'show' );
		$('#btnEditgregarPersona').hide();
		$('#btnAgregarPersona').show();
		$('#MSCAVE01014_Personas').valid();		
	});
	*/
	/*
	$('#btnAgregarPersona').click(function (e) {
		console.log( "== Agrega un nuevo registro a la tabla de personas de forma manual ==" );
		var isValidPR = $('#MSCAVE01014_Personas').valid();
		console.log( "Tipo de Persona : " + $( "#auxIdTipoP" ).val() );
		console.log( '> Save | Persona Reportada Valida : ' + isValidPR ); 
		if(isValidPR){
			if( TP_ALTA == TP_ALTA_MANUAL ){
				var personaReportada = crearObjetoPR();
				nuevaPersonaReportada(personaReportada).done(function(data)	{				
					cargarMensaje(data,true);
					consultarPersonasReportadas().done(function(data) {
						$('#MSCAVE01014_Personas').modal( 'hide' );
						$('#tablaPersonaReportada').bootstrapTable('load',data.response);
					});
				});					
			}			
			if( cdRequerimiento == 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) )
			{
				addRowPersonaReportada();
				$('#MSCAVE01014_Personas').modal( 'hide' )
			}

			else if( cdRequerimiento != 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) ){
				var personaReportada = crearObjetoPR();
				nuevaPersonaReportada(personaReportada).done(function(data)	{				
					cargarMensaje(data,true);
					consultarPersonasReportadas().done(function(data) {
						$('#MSCAVE01014_Personas').modal( 'hide' );
						$('#tablaPersonaReportada').bootstrapTable('load',data.response);
					});
				});				
			}
		

		}
	});
	*/
	/*
	$('#btnEditgregarPersona').click(function (e) {
		console.log( "== Evento del boton editar persona ==" );
		var isValidPR = $('#MSCAVE01014_Personas').valid();
		if(isValidPR){
			if( TP_ALTA == TP_ALTA_MANUAL ){
				console.log( "EDITAR PERSONA 20161228 ALTA MANUAL" );
				var persona = crearObjetoPREdit();
				actualizarPersonaReportada(persona).done(function(data)	{				
					cargarMensaje(data,true);
					consultarPersonasReportadas().done(function(data) {
						$('#MSCAVE01014_Personas').modal( 'hide' );
						console.log( "== ACTUALIZAR TABLA PERSONAS REPORTADAS ==" );
						console.log( JSON.stringify( data.response ) );
						$('#tablaPersonaReportada').bootstrapTable('load',data.response);
					});
				});			
			}			
			if( cdRequerimiento == 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) )
			{
				console.log( "EDITAR PERSONA 20161228 UPDATE ROW" );
				updateRowPersonaReportada();
			}

			else if( cdRequerimiento != 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) ){
				console.log( "EDITAR PERSONA 20161228 ALTA AUTOMATICA" );
				var personaReportada = crearObjetoPREdit();
				actualizarPersonaReportada(personaReportada).done(function(data)	{				
					cargarMensaje(data,true);
					consultarPersonasReportadas().done(function(data) {
						console.log( "== ACTUALIZAR TABLA PERSONAS REPORTADAS ==" );
						console.log( JSON.stringify( data.response ) );						
						$('#MSCAVE01014_Personas').modal( 'hide' );
						$('#tablaPersonaReportada').bootstrapTable('load',data.response);
					});
				});	
			}		
		}
		
		console.log( "Finaliza" );
	});	
	*/
	/*
	$('#btnOkDeletePR').click(function (e) {
		console.log( "== Evento del boton confirmar eliminado de registro ==" );
		console.log( "TP_ALTA : " + TP_ALTA );
		console.log( "Cd Requerimiento : " + cdRequerimiento );
		
		if( TP_ALTA == TP_ALTA_MANUAL )
		{
			eliminarPersonaReportada( cdRequerimiento, idPersonaReportada  ).done(function(data)	{
				if( cargarMensaje(data,true) ){
					consultarPersonasReportadas().done(function(data) {
						if( cargarMensaje(data,false)){		                            
							$('#tablaPersonaReportada').bootstrapTable('load',data.response);									
						}
				 	});
				}
			});	
		}else if( TP_ALTA == TP_ALTA_AUTOMATICA && cdRequerimiento == 0 )
		{
			removeRowPersonaReportada();
		}
		else if( TP_ALTA == TP_ALTA_AUTOMATICA && cdRequerimiento != 0 )
		{
			eliminarPersonaReportada( cdRequerimiento, idPersonaReportada  ).done(function(data)	{
				if( cargarMensaje(data,true) ){
					consultarPersonasReportadas().done(function(data) {
						if( cargarMensaje(data,false)){		                            
							$('#tablaPersonaReportada').bootstrapTable('load',data.response);									
						}
				 	});
				}
			});				
		}
	});		
	*/
	
	//insertarArchivo
	/*
	$('#btnAdjuntarArchivos').click(function (e) {
		console.log( "== Evento del boton Archivos Adjuntos ==" );
		$('#MSCAVE01014_ArchivosAdjuntos').modal( 'show' );	
		$("#MSCAVE01014_ArchivosAdjuntos")[0].reset();
		deshabilitarBotonUploadFile();
		deshabilitarBotonCancelUpload();		
	});
	*/
	
	//insertarArchivo
	/*
	$('#btnAttachment').change(function (e) {
		console.log( "== Evento del boton Attachment ==" );		
		habilitarBotonUploadFile();
		habilitarBotonCancelUpload();
	});
	*/
	/*
	$('#btnLocalizacionCliente').click( function (e){
		('#MSCAVE01014_1').load('MSCAVE01014_1.html', function (response, status, xhr){
			if (status == "success") {
				$(this).modal('show');
			}else{
				data.cdMensaje = 0;
				data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
				cargarMensaje(data,false);
			}
		});		
	});
	*/
	/*
	$('#btnCancelUpload').click(function (e) {
		console.log( "== Evento del boton Attachment ==" );		
		deshabilitarBotonUploadFile();
		deshabilitarBotonCancelUpload();
	});			
	*/
	//$('#imageFile').change(function(){ uploadFile(); });
	/*
	$( '#btnOkDeleteFile' ).click(function (e){
		eliminarArchivo().done(function(data)	{
			if( cargarMensaje(data,true) ){
				consultarArchivos().done(function(data) {
					if( cargarMensaje(data,false)){		                            
						$('#tablaArchivosAdjuntos').bootstrapTable( 'load', data.response );									
					}
			 	});
			}
		});	
		cdFolioArchivoDelete = null;
	});
	*/
	/*
	$('#btnImportar').change(function (e) {
		console.log( "== Evento del boton Importar Archivo ==" );		
		limpiarFormularioRequerimiento();
		contador = 0;
		$('#tablaPersonaReportada').bootstrapTable('load',[]);
		$('#tablaArchivosAdjuntos').bootstrapTable('load',[]);
		arregloPersonasReportadas = new Array();
		importarXML();
		cdRequerimiento = 0;
		nuFolioArchivo = null;
		habilitarBotonGuardar();
		showBtnAddPersonasReportadas();
		showTitleTablaPersonasReportadas();
		showTablaPersonasReportadas();
		habilitarBotonAddPersona();
		hideBtnAdjuntarArchivos();
		hideTitleTablaArchivosAdjuntos();
		hideTablaArchivosAdjuntos();
		habilitarFormularioRequerimiento();
		habilitarFormularioTipoFuente();
		$('#nbTipo').focus();
		TP_ALTA = TP_ALTA_AUTOMATICA;
		//$( "#btnNuevo" ).prop( "disabled", true);
	});	
	*/
	/*
	$('#btnLocalizacionCliente').click(function (e) {
		console.log( "== Evento del boton Importar Archivo ==" );
	});
	*/
	/*
	$('#auxIdTipoP').change(function (e) {
		console.log( "== Evento del combo tipo Persona ==" );
		var valTpPersona = $('#auxIdTipoP').val();
		console.log( "Valor : " + valTpPersona );
		limpiarFormularioPersonaRiesgo();
		if( valTpPersona == "-1" ){			
			deshabilitarFormularioPersona();
		}
		else if( valTpPersona == CD_TP_PERSONA_FISICA )
		{
			$('#auxIdTipoP').val( CD_TP_PERSONA_FISICA  );
			habilitarFormularioPersonaFisica();
		}else if( valTpPersona == CD_TP_PERSONA_MORAL){
			$('#auxIdTipoP').val( CD_TP_PERSONA_MORAL  );
			habilitarFormularioPersonaMoral();
		}else if( valTpPersona == CD_TP_PERSONA_GOBIERNO){
			$('#auxIdTipoP').val( CD_TP_PERSONA_GOBIERNO  );
			habilitarFormularioPersonaMoral();
		}		
	});
	*/

	// ==================== HABILITAR / DESHABILITAR FORMULARIOS ====================
	/*
	function habilitarFormularioTipoFuente()
	{
		console.log( "== Habilita Formulario Tipo Fuente ==" );
		$('#nbTipo').prop("disabled", false);
		$('#nbTipo').val( "-1" );
		$('#nbPrioridad').val( "" );
		$('#fhRecepcionReq').prop("disabled", false);		
		$( '#fhRecepcionReq' ).datepicker( 'setDate', 'today' )
	}
	*/
	/*
	function deshabilitarFormularioTipoFuente()
	{
		$('#nbTipo').prop("disabled", true);
		//$('#nbTipo').val( "-1" );
		$('#fhRecepcionReq').prop("disabled", true);
		//$( '#fhRecepcionReq' ).datepicker( 'setDate', 'today' );	
		//$('#fhRecepcionReq').val( "" );
	}
	*/
	/*
	function deshabilitarFormularioRequerimiento()
	{
		$('#nuOficio').prop("disabled", true);
		$('#nuExpediente').prop("disabled", true);
		$('#nuFolio').prop("disabled", true);
		$('#nbDescripcion').prop("disabled", true);
		$('#fhPublicacion').prop("disabled", true);
		$('#nuDiasPlazo').prop("disabled", true);
		$('#nbAutoridad').prop("disabled", true);
		$('#nbReferencia').prop("disabled", true);
		$('#nbAseguramiento').prop("disabled", true);
		$('#nbSolicitudEspecifica').prop("disabled", true);
	}
	*/
	/*
	function habilitarFormularioRequerimiento()
	{
		//console.log( "== Deshabilitar Formulario Requerimiento ==" );
		$('#nuOficio').prop("disabled", false);
		$('#nuExpediente').prop("disabled", false);
		$('#nuFolio').prop("disabled", false);
		$('#nbDescripcion').prop("disabled", false);
		$('#fhPublicacion').prop("disabled", false);
		$('#nuDiasPlazo').prop("disabled", false);
		$('#nbAutoridad').prop("disabled", false);
		$('#nbReferencia').prop("disabled", false);
		$('#nbAseguramiento').prop("disabled", false);
		$('#nbSolicitudEspecifica').prop("disabled", false);
	}
	*/
	/*
	function deshabilitarFormularioPersona()
	{
		console.log( "== Deshabilitar Formulario Persona Reportada ==" );
		$('#auxNombre').prop("disabled", true);
		$('#auxRazonSocial').prop("disabled", true);
		$('#auxAP').prop("disabled", true);
		$('#auxAM').prop("disabled", true);
		$('#auxFechaN').prop("disabled", true);
		$('#auxDom').prop("disabled", true);
		$('#auxTipologia').prop("disabled", true);
		$('#auxRFC').prop("disabled", true);
		$('#auxCaracter').prop("disabled", true);
		$('#auxNumCli').prop("disabled", true);
		$('#auxNumCuenta').prop("disabled", true);
		$('#auxDictamen').prop("disabled", true);
		$('#auxComp').prop("disabled", true);
		$('#btnAgregarPersona').prop("disabled", true);
		$('#btnCancelarPersonaRep').prop("disabled", true);
		$('input[name=esCliente]').attr("disabled",true);
	}	
	*/
	/*
	function habilitarFormularioPersonaFisica()
	{
		console.log( "== Habilitar Formulario Persona Fisica ==" );
		$('#auxNombre').prop("disabled", false);
		$('#auxNombre').show(  );
		$('#auxNombre').focus(  );
		
		$('#auxRazonSocial').prop("disabled", true);
		$('#auxRazonSocial').hide(  );
		
		habilitaCamposComunesAltaPersona();		
	}	
	*/
	/*
	function habilitaCamposComunesAltaPersona(){
		$('#auxAP').prop("disabled", false);
		$('#auxAM').prop("disabled", false);
		$('#auxFechaN').prop("disabled", false);
		$('#auxDom').prop("disabled", false);
		$('#auxTipologia').prop("disabled", false);
		$('#auxRFC').prop("disabled", false);
		$('#auxCaracter').prop("disabled", false);
		$('#auxNumCli').prop("disabled", false);
		$('#auxNumCuenta').prop("disabled", false);
		$('#auxDictamen').prop("disabled", false);
		$('#auxComp').prop("disabled", false);
		$('#btnAgregarPersona').prop("disabled", false);
		$('#btnCancelarPersonaRep').prop("disabled", false);
		$('input[name=esCliente]').attr("disabled",false);		
	}
	*/
	/*
	function habilitarFormularioPersonaMoral()
	{
		console.log( "== Habilitar Formulario Persona Moral ==" );
		habilitaCamposComunesAltaPersona();
		$('#auxNombre').prop("disabled", true);
		$('#auxNombre').hide(  );		
		$('#auxAP').prop("disabled", true);
		$('#auxAP').val( '' );
		$('#auxAM').prop("disabled", true);		
		$('#auxAM').val( '' );
		$('#auxRazonSocial').prop("disabled", false);
		$('#auxRazonSocial').show(  );
		$('#auxRazonSocial').focus(  );
	}
	*/
	/*
	function habilitarEsClienteBancomerSI(){
		$('#auxNumCli').prop("disabled", false);
		$('#auxNumCuenta').prop("disabled", false);
	}
	*/
	/*
	function habilitarEsClienteBancomerNO(){
		$('#auxNumCli').prop("disabled", true);
		$('#auxNumCli').val('');
		$('#auxNumCuenta').prop("disabled", true);
		$('#auxNumCuenta').val('');
	}	
	*/
	
	// ==================== CONFIGURACION BOTONES TIPO FILE ====================
	/*
	function configBtnImportar()
	{
		console.log( 'Configura Boton Importar' );
		$('#btnImportar').fileinput({
			showUpload: false,
			showCaption: false,
			showPreview: false,
			showRemove: false,
	        showCancel: false,
	        showClose: false,
	        showUploadedThumbs: false,		
			browseClass: 'btn btn-primary btn-lg',
			browseLabel: 'Importar Archivo',
			uploadLabel: 'Importar Archivo',
			fileType: 'xml'
		});			
	}
	*/
	/*
	function configBtnAtachment()
	{
		console.log( 'Configura Boton Atachment Archivo' );
		$('#btnAttachment').fileinput({
			showUpload: false,
			showCaption: true,
			showPreview: false,
			showRemove: false,
	        showCancel: false,
	        showClose: false,
	        showUploadedThumbs: false,
			browseClass: 'btn btn-primary btn-lg',
			browseLabel: 'Adjuntar Archivo',
			uploadLabel: 'Adjuntar Archivo',
			fileType: 'any'
		});	
		console.log( "Finaliza configuracion" );
	}
	*/ 
	/*
	function submitAdjuntarArchivo()
	{
		console.log( "== Evento del boton UploadFile ==" );
		$('#MSCAVE01014_ArchivosAdjuntos').modal( 'hide' );
		insertarArchivo().done(function(data) {
		console.log( "Respuesa Insertar Archivo Service : "  + JSON.stringify( data.response ) );
		cargarMensaje(data,true);
			console.log( "El archivo se ha insertado correctamente" );
			console.log( "Archivo : " + data.response );
			consultarArchivos(  ).done(function(data) {
				cargarMensaje(data,false);					                            
				$('#tablaArchivosAdjuntos').bootstrapTable( 'load', data.response );
				$("#MSCAVE01014_ArchivosAdjuntos")[0].reset();
				deshabilitarBotonUploadFile();
				deshabilitarBotonCancelUpload();
			 });
		});		
	}
	*/	
	
	// ==================== OTROS ====================
	
	/**
	 * Lee los datos para agregar una nueva persona a la tabla de Personas Reportadas
	 * @returns
	 */
	/*
	function addRowPersonaReportada(){
		console.log( "== AddRow Crear objeto persona reportada ==" );
		contador += 1;	
		var rdbEscliente = $('input[name=esCliente]');
		var checkedValue = rdbEscliente.filter(':checked').val();
		var tipoPersonaTmp = $("#auxIdTipoP").val();
		var nombreTmp;
		if( tipoPersonaTmp == CD_TP_PERSONA_FISICA )
		{
			console.log( "Es una persona Fisica | Nombre APaterno AMaterno" );
			nombreTmp = $("#auxNombre").val();
		}
		else if( tipoPersonaTmp == CD_TP_PERSONA_MORAL || tipoPersonaTmp == CD_TP_PERSONA_GOBIERNO )
		{
			console.log( "Es una persona Moral o Gobierno | Razon Social " );
			nombreTmp = $("#auxRazonSocial").val();
		}
		var fhNacimientoTmp = $("#auxFechaN").datepicker('getDate');
		
        $("#tablaPersonaReportada").bootstrapTable('insertRow', {
            index: 0,
            row: {
                nuId: contador,
                cdCaracter : $("#auxCaracter").val(),
                nbTpPersona: $("#auxIdTipoP").val(),
                cdTpPersona: $("#auxIdTipoP").val(),
                nbNombre : nombreTmp,
                nbApPaterno: $("#auxAP").val(),
                nbApMaterno: $("#auxAM").val(),
                fhNacimiento: fhNacimientoTmp,
                cdRFC:  $("#auxRFC").val(),       
                nbDomicilio : $('#auxDom').val(),  
                nbComplementarios: $('#auxComp').val(),
                cdTipologia: $('#auxTipologia').val(),               
                cdClienteBancomer : checkedValue,
                cdCliente : $('#auxNumCli').val(),
                nuCuenta : $('#auxNumCuenta').val(),
                cdDictamen : $('#auxDictamen').val()
            }
        });		
        habilitarBotonGuardar();
	}
	*/
	/*
	function updateRowPersonaReportada(){
		
		var rdbEscliente = $('input[name=esCliente]');
		var checkedValue = rdbEscliente.filter(':checked').val();
		var tipoPersonaTmp = $("#auxIdTipoP").val();
		
		var nombreTmp;		
		if( tipoPersonaTmp == CD_TP_PERSONA_FISICA )
		{
			nombreTmp = $("#auxNombre").val();
		}
		else if( tipoPersonaTmp == CD_TP_PERSONA_MORAL || tipoPersonaTmp == CD_TP_PERSONA_GOBIERNO )
		{
			nombreTmp = $("#auxRazonSocial").val();
		}
		
		var fhNacimientoTmp = $("#auxFechaN").datepicker('getDate');
		$("#tablaPersonaReportada").bootstrapTable('updateRow', 
        	{
				index: $("#nuIndexHdn").val(),
        		row: {
                    nuId: $("#nuIdHdn").val(),
                    cdCaracter : $("#auxCaracter").val().toUpperCase(),
                    cdTpPersona: tipoPersonaTmp.toUpperCase(),
                    nbTpPersona: tipoPersonaTmp.toUpperCase(),
                    nbNombre : nombreTmp.toUpperCase(),
                    nbApPaterno: $("#auxAP").val().toUpperCase(),
                    nbApMaterno: $("#auxAM").val().toUpperCase(),
                    fhNacimiento: fhNacimientoTmp,
                    cdRFC:  $("#auxRFC").val().toUpperCase(),
                    nbDomicilio : $('#auxDom').val().toUpperCase(),
                    nbComplementarios: $('#auxComp').val().toUpperCase(),
                    cdClienteBancomer : checkedValue,
                    cdCliente : $('#auxNumCli').val().toUpperCase(),
                    nuCuenta : $('#auxNumCuenta').val().toUpperCase(),
                    cdTipologia: $('#auxTipologia').val().toUpperCase()
                    //cdDictamen : $('#auxDictamen').val().toUpperCase()
        		}
        	}		
        );
		$('#MSCAVE01014_Personas').modal( 'hide' )
		console.log( "Finaliza Update" );
	}	
	*/

	/**
	 * Mostrar prioridades
	 * @param value
	 * @returns
	 */
	/*
	function mostrarPrioridad(value)
	{
		var idPrioridad;
		var indiceError = -1;
		
		console.log( "== Prioridad : " + value );
		
		// Se cargan prioridades en una tabla oculta
		if( value != indiceError)
		{
			obtenerPrioridad(value).done(function(data) {
				if (cargarMensaje(data, false)) {
					idPrioridad = data.response.cdPrioridad;
					$('#nbPrioridad').val(data.response.nbValor);
					$('#cdPrioridad').val(idPrioridad);
				} 
			});
		}else{
			$('#nbPrioridad').val( '' );			
		}
		arregloPersonasReportadas = new Array();
		arregloPersonasReportadas = $('#tablaPersonaReportada').bootstrapTable('getData');
		$('#tablaPersonaReportada').bootstrapTable('load',arregloPersonasReportadas);
	}
	*/
	/*
	function getFechaNacimientoByRFC( rfcRecived )
	{		
		var rfc = rfcRecived.trim();
		var rfcSubstr;
		var fechaCadena;
		var fechaNacimiento;
		var anio;
		var mes;
		var dia;
		if( rfc  == null || ( rfc == '' ) || ( rfc == 'NO SE CUENTA' ) || ( rfc == '             ' ) )
		{
			//console.log( "RFC VACIO" );
			return '';
		}
		else
		{
			//
			if( rfc.length == 12 )
			{
				rfcSubstr = rfc.substr( 3, 6 );		
			}else if( rfc.length == 13 )
			{
				rfcSubstr = rfc.substr( 4, 6 );
				if( rfcSubstr.includes( '-') )
				{
					rfcSubstr = rfc.substr( 3, 6 );	
				}
			}
			else if( rfc.length == 10 )
			{
				rfcSubstr = rfc.substr( 4, 6 );
			}
			anio = rfcSubstr.substr(0,2)
			mes = rfcSubstr.substr(2,2)
			dia = rfcSubstr.substr(4,2)
			fechaNacimiento = getDate( anio, mes, dia );
		}
		return fechaNacimiento;
	}	
	*/
	/*
	function getDate( anio, mes, dia ){
		var fechaNacimiento = new Date( );
		var iniY = anio.substr( 0, 1 );
		//console.log( "Ini Y : " + iniY );
		if( iniY == '0' || iniY == '1' || iniY == '2' || iniY == '3'){
			anio = '20' + anio;
		}else if( iniY == '9' || iniY == '8' || iniY == '7' || iniY == '6' || iniY == '5'){
			anio = '20' + anio;
		}
		fechaNacimiento.setFullYear( anio );
		fechaNacimiento.setMonth( mes );
		fechaNacimiento.setDate( dia );
		fechaNacimiento.setHours( 0 );
		fechaNacimiento.setMinutes( 0 );
		fechaNacimiento.setSeconds( 0 );
		return fechaNacimiento;	
	}
	*/
	/*
	function getValor( cadena ){
		if( cadena == null || cadena == 'NO SE CUENTA' || cadena == undefined ){
			cadena = '';
		}
		return cadena;
	}
	*/
	/*
	function limpiarRFC( valRFC ){
		//console.log( "==============" );
		if( valRFC != ''){
			console.log( valRFC );
			valRFC = valRFC.replace(/\s+/g, '');
			valRFC = valRFC.replace( '-', '');
			valRFC = valRFC.replace( '_', '');
		}
		else{
			//console.log( "Cadena Vacia" );
		}
		//console.log( valRFC );
		//console.log( "==============" );
		return valRFC;
	}
	*/
	/*
	function validaRfc(rfcStr, tpPersona) {
		var esValido = false;
		var strCorrecta;
		var rfcSinHomoClave;
		var patronValidacion;
		strCorrecta = rfcStr;	
		//console.log( "==== TIPO PERSONA====" );
		//console.log( tpPersona );
		if( tpPersona == CD_TP_PERSONA_FISICA ){
			//console.log( "=== FISICA ===");
			rfcSinHomoClave = rfcStr.substr( 0, 10 );
			patronValidacion = '^(([A-Z]|[a-z]|\s){4})([0-9]{6})'
			//console.log( rfcSinHomoClave );
		}else if( tpPersona == CD_TP_PERSONA_MORAL ){
			//console.log( "=== MORAL ===");
			patronValidacion = '^(([A-Z]|[a-z]){3})([0-9]{6})';			
			rfcSinHomoClave = rfcStr.substr( 0, 9 );
			//console.log( rfcSinHomoClave );			
		}else if( tpPersona == CD_TP_PERSONA_GOBIERNO ){
			esValido = true;
		}
		//console.log( "Resultado : " + rfcSinHomoClave );
		//console.log( "Es Valido = " + esValido ) ;
		if( esValido == false )
		{
			var validRfc = new RegExp( patronValidacion );
			var matchArray = rfcSinHomoClave.match( validRfc );
			if ( matchArray == null ) {
				//console.log('XXXXXXX Cadena incorrecta : ' + rfcSinHomoClave );
				esValido = false;
			}
			else
			{
				//console.log('>>>>>>> Cadena correcta : ' + rfcSinHomoClave);
				esValido = true;
			}			
		}
		return esValido;
	}
	*/
	
	/*
	function actualizarPersonaReportadaFromLoc(  vClienteReportadoOrigen, vClienteReportadoUpdate, indexCteRep  )
	{
		console.log( "== Actualiza una persona reportada desde la pantalla de Localizaci�n del cliente ==" );
		if( TP_ALTA == TP_ALTA_MANUAL ){
			console.log( "== Invocar servicio para actualizar los datos desde la pantalla localizacion del cliente ==" );
			vClienteReportadoUpdate.cdClialtrie = cdRequerimiento;
			actualizarPersonaReportada( vClienteReportadoUpdate ).done(function(data)	{				
				cargarMensaje(data,true);
				consultarPersonasReportadas().done(function(data) {
					$('#tablaPersonaReportada').bootstrapTable('load',data.response);
					//console.log( "== ACTUALIZAR TABLA PERSONAS REPORTADAS ==" );
					//console.log( JSON.stringify( data.response ) );
				});
			});
		}			
		if( cdRequerimiento == 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) )
		{
			console.log( "== Actualizar el registro en el grid de personas reportadas ==" );
			$("#tablaPersonaReportada").bootstrapTable('updateRow', 
			        {
						index: indexCteRep,
			       		row: {
			       			nuId: vClienteReportadoUpdate.nuId,
			                cdCaracter : vClienteReportadoUpdate.cdCaracter,
			                cdTpPersona: vClienteReportadoUpdate.nbTpPersona,
			                nbTpPersona: vClienteReportadoUpdate.nbTpPersona,
			                nbNombre : vClienteReportadoUpdate.nbNombre,
			                nbApPaterno: vClienteReportadoUpdate.nbApPaterno,
			                nbApMaterno: vClienteReportadoUpdate.nbApMaterno,
			                fhNacimiento: vClienteReportadoUpdate.fhNacimiento,
			                cdRFC: vClienteReportadoUpdate.cdRFC,
			                nbDomicilio : vClienteReportadoUpdate.nbDomicilio,
			                nbComplementarios: vClienteReportadoUpdate.nbComplementarios,
			                cdClienteBancomer : vClienteReportadoUpdate.cdClienteBancomer,
			                cdCliente : vClienteReportadoUpdate.cdCliente,
			                nuCuenta : vClienteReportadoUpdate.nuCuenta,
			                cdTipologia: vClienteReportadoUpdate.cdTipologia
			                //cdDictamen : vClienteReportadoUpdate.cdDictamen
			       		}
			       }
			     );
		}

		else if( cdRequerimiento != 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) )
		{
			console.log( "== Es una alta automatica pero previamente ya han guardado el registro ==" );
			vClienteReportadoUpdate.cdClialtrie = cdRequerimiento;
			actualizarPersonaReportada( vClienteReportadoUpdate ).done(function(data)	{				
				cargarMensaje(data,true);
				consultarPersonasReportadas().done(function(data) {
					$('#tablaPersonaReportada').bootstrapTable('load',data.response);
				});
			});
		}		
		$('#MSCAVE01014_1').modal( 'hide' );			
	}
	*/
	/*
	function agregarPersonasReportadasFromLoc( lstClientesAgregados )
	{
		
		console.log( "== Lista de Personas Recibidas ==" );
		console.log( JSON.stringify( lstClientesAgregados ) );
		var lstPersonasReportadasActuales = $('#tablaPersonaReportada').bootstrapTable('getData');
		console.log( "== Lista de Personas Actuales ==" );
		console.log( JSON.stringify( lstPersonasReportadasActuales ) );		
		var objCteAdd;
		
		var lstPersonasRiesgoAgregadas = new Array();
		
		console.log( "== Tansforma lista de clientes agregados a objetos de personas reportadas ==" );
		$.each( lstClientesAgregados, function(index, val) {
			console.log( "== Cliente Agregado == "  );
			console.log( JSON.stringify( val ) );
			objCteAdd = new Object();
			contador += 1;
			if( cdRequerimiento != 0 ){
				objCteAdd.cdClialtrie = cdRequerimiento;
			}
			
			objCteAdd.nuId = contador;
			objCteAdd.cdCaracter = '';
			objCteAdd.cdTpPersona =  val.cdTpPersona;
			objCteAdd.nbTpPersona = val.cdTpPersona;
			
			if( val.cdTpPersona == CD_TP_PERSONA_FISICA)
			{
				objCteAdd.nbNombre = val.nbNombre;
				objCteAdd.nbApPaterno = val.nbPaterno;
				objCteAdd.nbApMaterno = val.nbMaterno;				
			}
			else if( val.cdTpPersona == CD_TP_PERSONA_MORAL || val.cdTpPersona == CD_TP_PERSONA_GOBIERNO )
			{
				objCteAdd.nbNombre = val.nbNombre;
				objCteAdd.nbApPaterno = '';
				objCteAdd.nbApMaterno = '';
			}
			
			objCteAdd.fhNacimiento = '';
			objCteAdd.cdRFC = val.cdRFC;
			
			objCteAdd.fhNacimiento  = getValor ( getFechaNacimientoByRFC( objCteAdd.cdRFC.toUpperCase() ) );
			
			
			objCteAdd.nbDomicilio = val.domicilio;
			objCteAdd.nbComplementarios= '';
			objCteAdd.cdClienteBancomer = 'SI';
			objCteAdd.cdCliente = val.nuCliente;
			objCteAdd.nuCuenta = '';
			objCteAdd.cdTipologia = '';
			objCteAdd.cdDictamen = '';			
			lstPersonasReportadasActuales.push( objCteAdd );
			lstPersonasRiesgoAgregadas.push( objCteAdd );
			
			
			if( cdRequerimiento == 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) ){
				console.log( "== Solo Agrega los clientes agregados a la tabla de personas Reportadas ==" );
				lstPersonasReportadasActuales.push( objCteAdd );
			}
			else if( (TP_ALTA == TP_ALTA_MANUAL ) || ( cdRequerimiento != 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) ) ){
				console.log( "== Inserta los nuevos clientes a la tabla de personas reportads ==" );
				nuevaPersonaReportada(objCteAdd).done(function(data){
					cargarMensaje(data,true);
				});				
			}
			
			
		});	
		
		if( cdRequerimiento == 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) ){
			console.log( "== Aun no se ha guardado el alta automatica ==" );
			console.log( JSON.stringify( lstPersonasReportadasActuales ) );
			$( "#btnGuardar" ).prop( "disabled", false );
			$('#tablaPersonaReportada').bootstrapTable('load', lstPersonasReportadasActuales );
		}		
		else if( cdRequerimiento != 0 && ( TP_ALTA == TP_ALTA_AUTOMATICA ) ) {
			console.log( "== Ya se ha dado de alta el Requerimiento de los clientes de alto riesgo ==" );
			console.log( JSON.stringify( lstPersonasRiesgoAgregadas ) );
			
			nuevaPersonaReportadaTransac(lstPersonasRiesgoAgregadas).done(function(data)	{
				cargarMensaje(data,true);
				consultarPersonasReportadas().done(function(data) {
					$('#MSCAVE01014_Personas').modal( 'hide' );
					$('#tablaPersonaReportada').bootstrapTable('load',data.response);
				});
			});	
		}		
		
		console.log( "== Finaliza Transformacion ==" );
		$('#MSCAVE01014_1').modal( 'hide' );		
	}
	*/
	/*
	function llenarSelectTipologiaGerencia( selector, cdGerencia, valorDefault ) {
		$select = selector;
		var url = obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoTipologiaGerencia/" + cdGerencia;
		$.ajax({
		  url: url,
		  dataType:'JSON',
		  async: false,
		  success:function(data){
			data = data.response;
		    //Se limpia el selector
		    $select.html('');
		    //Se itera sobre el data y se agregara la opcion del select
		    if(valorDefault){
		    	$select.append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    }
	    	$.each(data, function(key, val){
			      $select.append('<option value="' + val.cdValor + '">' + val.cdValor + ' - ' + val.nbValor+ '</option>');
			})    
		  },
		  error:function(){
		    //Si existe un error, coloca un valor negativo, y agrega la leyenda
		    $select.html('<option value="-1">Ocurri\u00f3 un error</option>');
		  }
		});
	}
	*/	
