var descripcion = new Object();
var perfil = sessionStorage.cdPerfil;
var pantalla = sessionStorage.pantalla;
var scroll;
(function($, window, document) {
	$(function() {	
		scroll = 0;
		
		$('#cdCaso').html(sessionStorage.cdCaso);
		loadPanel('MSCAVE01007_1'); 
		llenaDescripCaso(sessionStorage.cdCaso).done(function(data) {
			if(cargarMensaje(data,false)){ 
					$('#nbDesOpeReal').val(data.response.nbDesOpeReal);
					$('#nbDesRazInv').val(data.response.nbDesRazInv); 
			}});
		
		
	//numeroAlertasCaso
		numeroAlertasCaso(sessionStorage.cdCaso).done(function(data2){
		var numAlertas = data2.response.numAlertasAC;
			if(cargarMensaje(data2,false)){
				$('#numAlertasAC').html(numAlertas);
			}
		});
		
	});
}(window.jQuery, window, document));

function loadPanel(idPanel){
	$('#'+idPanel).empty();
	$('#'+idPanel).load(idPanel+'.html', function (response, status, xhr){
		
	    if (status != "success") {
	    	data.cdMensaje = 0;
	    	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	    	cargarMensaje(data,false);
	    }
	});
}

function detalleCaso(){	
	$('#MSCAVE01003').load('MSCAVE01003.html', function (response, status, xhr){
	    if (status == "success") {
	        $(this).modal('show');
	    }else{
	    	data.cdMensaje = 0;
	    	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	    	cargarMensaje(data,false);
	    }
	});
}

function alertasPrevias(){
	cargarContenido('MSCAVE01004','Gesti&oacute;n de Alertas > An&aacute;lisis del Caso > Alertas Previas');
}

function dictamenPreliminar(){
	sessionStorage.opDictamen = 'P';//Preliminar
	$('#MSCAVE01008').load('MSCAVE01008.html', function (response, status, xhr){
		
		if (status == "success") {
	        $(this).modal('show');
	    }else{
	    	data.cdMensaje = 0;
	    	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	    	cargarMensaje(data,false);
	    }	    	    
	});
}

function regresar(){
	if(perfil == "3" || perfil == "2"){
		cargarContenido('MSCAVE01010','Gesti&oacute;n de Alertas > Consulta de Casos');
	}else if(perfil == "4"){
		
		if(pantalla == 'MSCAVE01006'){
			cargarContenido('MSCAVE01006','Gesti&oacute;n de Alertas > Casos pendientes');
		}
		else if(pantalla == 'MSCAVE01010'){
			cargarContenido('MSCAVE01010','Gesti&oacute;n de Alertas > Consulta de Casos');
		}
		
	}
}

var widthOfList = function(){
  var itemsWidth = 0;
  $('.list li').each(function(){
    var itemWidth = $(this).outerWidth();
    itemsWidth+=itemWidth;
  });
  return itemsWidth;
};

var widthOfHidden = function(){
  return (($('.wrapper').outerWidth())-widthOfList()-getLeftPosi())-scrollBarWidths;
};

var getLeftPosi = function(){
  return $('.list').position().left;
};

var reAdjust = function(){
  if (($('.wrapper').outerWidth()) < widthOfList()) {
    $('.scroller-right').show();
  }
  else {
    $('.scroller-right').hide();
  }
  
  if (getLeftPosi()<0) {
    $('.scroller-left').show();
  }
  else {
    $('.item').animate({left:"-="+getLeftPosi()+"px"},'slow');
  	$('.scroller-left').hide();
  }
}

reAdjust();

$(window).on('resize',function(e){  
  	reAdjust();
});

$('.scroller-right').click(function() {
	
	if(scroll == 0){
		$('.scroller-left').fadeIn('slow');
		$('.scroller-right').fadeOut('slow');
		  	  
		$('.list').animate({left:"+="+(-685)+"px"},'slow',function(){
		
		});
		scroll = 1;
	}
});

$('.scroller-left').click(function() {
	
	if(scroll == 1){
		
		$('.scroller-right').fadeIn('slow');
		$('.scroller-left').fadeOut('slow');
	  
	  	$('.list').animate({left:"-="+(-685)+"px"},'slow',function(){
	  	
	  	});
	  	scroll = 0;
	}	
});    

function guardarDescripcion(){ 
	descripcion.txDesOpeReal = $('#nbDesOpeReal').val();
	descripcion.txDesRazInv = $('#nbDesRazInv').val();
	if(descripcion.txDesOpeReal != "" && descripcion.txDesRazInv != ""){ 
		addtxDescripcion(descripcion,sessionStorage.cdCaso).done(function(data) { 
	 	});  
	}else if(descripcion.txDesOpeReal != "" || descripcion.txDesRazInv != ""){ 
		addtxDescripcion(descripcion,sessionStorage.cdCaso).done(function(data) { 
	 	});  
	}
	else if(descripcion.txDesOpeReal == "" && descripcion.txDesRazInv == ""){ 
		addtxDescripcion(descripcion,sessionStorage.cdCaso).done(function(data) { 
	 	});  
	}
}

function addtxDescripcion(descripcion,cdCaso){
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/CasosService/CrearDescripCaso/"+cdCaso;
	var _type = "POST";
	
	return $.ajax({
		url : _url,
		type : _type,
		contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(descripcion),
        async: false
	});
}

$("#txOpeAnalizada").keyup(function() {
	if($(this).valid()){
		$("#guardarDescripcion").prop('disabled',false);
	}else{
		$("#guardarDescripcion").prop('disabled',true);
	}
});

$("#txRazInvestigar").keyup(function() {
	if($(this).valid()){
		$("#guardarDescripcion").prop('disabled',false);
	}else{
		$("#guardarDescripcion").prop('disabled',true);
	}
});


function llenaDescripCaso(cdCaso) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultaDescripcion/"+ cdCaso,
		type : "GET",
		async: false
	});
}

function numeroAlertasCaso(cdCaso) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarAlertas/"+ cdCaso,
		type : "GET",
		async: false
	});
}



