var nota = new Object();
var prueba = new Object();
var actionEvents = { 
	    'click .detailEdit': function (e, value, row, index) {
	    	$('#txInvestigacion').val(row.txInvestigacion);
	    	nota.cdFolioArchivo = row.cdFolioArchivo; 
	    	$('#dialogNotaInvestigacion').modal('show');
	    }
};

(function($, window, document) {
	$(function() { 
		  var x; 
		    var y;
		 validarNbTitulo(); 
		 $("#button_OK").prop('disabled',true);
		 validarTxInvestigacion();
		 $("#btnOkAsignar").prop('disabled',true);
		llenadoTablaNotasInvestigacion([]); 
		
		llenaTablaNotasInvestigacion().done(function(data) {
			if(cargarMensaje(data,false)){    
				$('#tablaNotasInvestigacion').bootstrapTable('load', data.response);
			}
		});
		
		
	});
}(window.jQuery, window, document));

function llenadoTablaNotasInvestigacion(data){ 
	$('#tablaNotasInvestigacion').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function formatterNotasInvestigacion(value, row, index) {
	return [
            '<a class="detailEdit" href="javascript:void(0)" title="Editar Nota de Investigaci&oacute;n">',
            '<i class="tabla_registro_editar">.</i>',
            '</a>'
    ].join(''); 
}

function llenaTablaNotasInvestigacion() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarNotasInvestigacion/"+sessionStorage.cdCaso,
		type : "GET",
		 dataType: "json",
		async: false 
	});
	 
}
 

function addNota(){
	var isvalidate=$("#txInvestigacion").valid();
	nota.txInvestigacion = $('#txInvestigacion').val();
	if(nota.txInvestigacion != ""){ 
		addtxNotaCaso(nota,sessionStorage.cdUsuario).done(function(data) {
			if(cargarMensaje(data,true)){
				llenaTablaNotasInvestigacion().done(function(data) {
					$('#tablaNotasInvestigacion').bootstrapTable('load', data.response);
				});
			}
	 	});  
	}
}

function addtxNotaCaso(nota,cdUsuario){
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/CasosService/addNota/"+cdUsuario;
	var _type = "POST";
	
	return $.ajax({
		url : _url,
		type : _type,
		contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(nota),
        async: false
	});
}

function AgregarArchivo(){  
	$('#adjunta_archivo').modal('show'); 
	$('#nbTitulo').val("");
}
 
function addArchivoNota() {
	var isvalidate=$("#nbTitulo").valid();
	var archivo = new  Object();
	archivo.nbTitulo = $('#nbTitulo').val(); 
	archivo.cdExpediente = sessionStorage.cdCaso;
	if( $('#nbTitulo').val()  != ""){ 
		addArchivo(archivo, sessionStorage.cdUsuario).done(function(data) { 
			if(cargarMensaje(data,true)){
			 	llenaTablaNotasInvestigacion().done(function(data) {
					$('#tablaNotasInvestigacion').bootstrapTable('load', data.response);
				});
			}
	}); 
	}  
}



function addArchivo(archivo,cdUsuario){
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/CasosService/addArchivo/"+cdUsuario;
	var _type = "PUT";
	
	return $.ajax({
		url : _url,
		type : _type,
		contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(archivo) 
	});
}

function validarNbTitulo() { 
	$("#formDialogNbTitulo").validate({
		rules: {
			nbTitulo: {
				required: true
			}
		},
		errorPlacement: function (error, element) {
            $(element).prop('style', 'border: 1px solid #C8175E;');
        },
        success: function (label, element) {
            $(element).prop('style', 'border: 1px solid #86C82D;');
        }
	});
}

function validarTxInvestigacion() { 
	$("#formNotaInvestigacion").validate({
		rules: {
			txInvestigacion: {
				required: true
			}
		},
		errorPlacement: function (error, element) {
            $(element).prop('style', 'border: 1px solid #C8175E;');
        },
        success: function (textarea, element) {
            $(element).prop('style', 'border: 1px solid #86C82D;');
        }
	});
}
$("#nbTitulo").keyup(function() {
	if($(this).valid()){
		$("#button_OK").prop('disabled',false);
	}else{
		$("#button_OK").prop('disabled',true);
	}
});

$("#txInvestigacion").keyup(function() {
	if($(this).valid()){
		$("#btnOkAsignar").prop('disabled',false);
	}else{
		$("#btnOkAsignar").prop('disabled',true);
	}
});

function formatterDetailNotasDeInvestigacion(value, row, index){
	var txInvestigacion=row.txInvestigacion==null?"":row.txInvestigacion;
	return ['<textarea class="form-control noresize" rows="3" cols="200" disabled>' + txInvestigacion +
	        '</textarea>'].join('');
}