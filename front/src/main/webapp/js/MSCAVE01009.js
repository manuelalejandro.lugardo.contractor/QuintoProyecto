var encender;
var parametros;

var actionEvents = { 
    'click .detailAlerta': function (e, value, row, index) {
    	sessionStorage.nuFolioAlerta=row.nuFolioAlerta;
    	sessionStorage.cdSistema=row.cdSistema;
    	sessionStorage.cdTipologia = row.cdTipologia;
    	sessionStorage.cdDivisa = row.cdDivisa;
    	$('#MSCAVE01002').load('MSCAVE01002.html', function (response, status, xhr){
            if (status == "success") {
                $(this).modal('show');
            }else{
            	data.cdMensaje = 0;
            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
            	cargarMensaje(data,false);
            }
        });
    },
    'click .detailCaso': function (e, value, row, index) {
    	sessionStorage.nuFolioAlerta=row.nuFolioAlerta
    	sessionStorage.cdCaso = row.cdCaso;
    	sessionStorage.cdSistema=row.cdSistema;
    	sessionStorage.cdTipologia = row.cdTipologia;
    	sessionStorage.cdDivisa = row.cdDivisa;
    	sessionStorage.cdCliente = row.nuCliente;
    	sessionStorage.nuCuenta = row.nuCuenta
    	$('#MSCAVE01003').load('MSCAVE01003.html', function (response, status, xhr){
            if (status == "success") {
                $(this).modal('show');
            }else{
            	data.cdMensaje = 0;
            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
            	cargarMensaje(data,false);
            }
        });
    },
    'click .dictamenFinal': function (e, value, row, index) {
    	sessionStorage.cdSesion = row.cdSesion;
    	sessionStorage.cdCaso = row.cdCaso;
    	sessionStorage.cdSistema=row.cdSistema;
    	sessionStorage.opDictamen = 'F';//FINAL
    	dictamenFinal();
    }
    
};

function dictamenFinal(){
	$('#MSCAVE01008').load('MSCAVE01008.html', function (response, status, xhr, elemento){
		if (status == "success") {
	        $(this).modal('show');
	    }else{
	    	data.cdMensaje = 0;
	    	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	    	cargarMensaje(data,false);
	    }
	    
	    
	});
	
	
}
(function($, window, document) {
	$(function() {
		$('.date').datepicker({
		    language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true
		});
		
		encender=0;
		parametros=0;
		$('.moneda').maskMoney({precision:2}); 
		
		consultarCasosDictamen(sessionStorage.cdUsuario).done(function(data) { 
			if(cargarMensaje(data,false)){
				llenadoTablaDictamendeCasos(data.response);
			}else{
				llenadoTablaDictamendeCasos([]);
			}
		});
		
		$('#btnExportarDictamen').on('click',function (){
			generaExcelDictamen();
		});
		
		// Validar Numero de Cuenta
		$.validator.addMethod('formatoNumeroCuenta', function(value, element){
			var valor = value.toUpperCase();
			var tamanio = 0;
			
			if( valor != null && valor != '')
			{
				tamanio = valor.length;
				if( tamanio > 0 && tamanio < 10)
				{
					return false;
				}
				else if( tamanio > 10 && tamanio < 20 )
				{
					return false;
				}
				else{
					return true;
				}
			}
			else if( valor == null || valor == ''){
				return true;
			}
		}, "error");

		jQuery.validator.addMethod("isValidateRangoFechas", function(value, element) {
			return validarElementos( element.name );
			
		}, "error");

		jQuery.validator.addMethod("okValidator", function(value, element, param) {
			  return true;
		}, "error");

		validacionFormularioBusquedaDictamen();
	});
}(window.jQuery, window, document));


function llenadoTablaDictamendeCasos(data) {
	$('#tablaDictamendeCasos').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function formatterDetailDictamendeCasos(value, row, index){
	
	var cdSesion=row.cdSesion==null?"":row.cdSesion;
	var nbSegmento=row.nbSegmento==null?"":row.nbSegmento;
	var nbConsultor=row.nbConsultor==null?"":row.nbConsultor;
	var fhEnvio=row.fhEnvio==null?"":formatterDate(row.fhEnvio);
	var nbDictamenP=row.nbDictamenP==null?"":row.nbDictamenP;
	var fhAsignacion=row.fhAsignacion==null?"":formatterDate(row.fhAsignacion);
	var fhAlerta=row.fhAlerta==null?"":formatterDate(row.fhAlerta);
	var nuScoreMdl=row.nuScoreMdl==null?"":row.nuScoreMdl;
	var nuScoreMc=row.nuScoreMc==null?"":row.nuScoreMc;
	var imAlerta=row.imAlerta==null?"":formatterMoney(row.imAlerta);
	var nbRespuesta=row.nbRespuesta==null?"":row.nbRespuesta;
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Segmento PLD:</b> '+nbSegmento+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Sesi&oacute;n:</b> '+cdSesion+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Consultor:</b> '+nbConsultor+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Dictamen Consultor:</b> '+nbDictamenP+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Calificaci&oacute;n Modelo:</b> '+nuScoreMdl+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Score Matriz:</b> '+nuScoreMc+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Importe:</b> '+imAlerta+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Respuesta Funcionario:</b> '+nbRespuesta+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha de Env&iacute;o:</b> '+fhEnvio+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha de Alerta:</b> '+fhAlerta+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha de Asignaci&oacute;n:</b> '+fhAsignacion+'</td></tr>'+
	  
	        
	        '</table>'].join('');
}

function checkFormatterCasosAsignados(value, row, index) {
	return ['<input type="checkbox" onChange="validarCasosAsignados();" name="checkCasoAsignado" id="CA_'+row.nuFolioAlerta+'_'+row.cdSistema+'" value="CA_'+row.nuFolioAlerta+'_'+row.cdSistema+'"/><label for="CA_'+row.nuFolioAlerta+'_'+row.cdSistema+'"></label>'].join(''); 
}

function checkAll(nameCheck,obj){
	$('input[name="'+nameCheck+'"]').each(function () {
			$(this).removeAttr('checked');
			$(this).prop('checked',obj.checked);
	});
}

function validarCasosAsignados(){
	sessionStorage.cdCaso = row.nuCaso;
	var numCasos = 0;
	$('input[name="checkCasoAsignado"]').each(function () {
		if($(this).prop('checked')){
			numCasos++;
		}
	});
	
	if(numCasos > 0){
		$("#btnReasignar").prop("disabled", false);
	}else{
		$("#btnReasignar").prop("disabled", true);
	}
}

function validarCasosPorAsignar(){
	sessionStorage.cdCaso = row.nuCaso;
	var numCasos = 0;
	$('input[name="checkCasosPorAsignar"]').each(function () {
		if($(this).prop('checked')){
			numCasos++;
		}
	});
	
	if(numCasos > 0){
		$("#btnAsignar").prop("disabled", false);
		$("#btnDescartar").prop("disabled", false);
	}else{
		$("#btnAsignar").prop("disabled", true);
		$("#btnDescartar").prop("disabled", true);
	}
}

function formatterRegistroCasosAsignados(value, row, index) {
	return [
            '<a class="detailAlerta" href="javascript:void(0)" title="Detalle de Alerta">',
            '<i class="tabla_registro_detalle">.</i>',
            '</a>',
            '<a class="detailCaso" href="javascript:void(0)" title="Detalle del Caso">',
            '<i class="tabla_registro_detalle_caso">.</i>',
            '</a>',
            '<a class="dictamenFinal" href="javascript:void(0)" title="Dictamen Final">',
            '<i class="tabla_registro_detalle_dictamen">.</i>',
            '</a>'
    ].join(''); 
}

function consultarCasosDictamen(cdSupervisor) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/DictamenService/consultarCasosDictamen/"+cdSupervisor,
		type : 'GET',
		async: true
	});
}
function consultarCasosDictamenFiltro(cdSupervisor,data) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/DictamenService/consultarCasosDictamenFiltro/"+cdSupervisor,
		type : 'POST',
		contentType:'application/json',
		dataType: 'JSON',
		data: JSON.stringify(data),
		async: false
	});
}


function consultarConsultoresSupervisor(cdSupervisor) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/consultarConsultoresSupervisor/"+cdSupervisor,
		type : "GET",
		async: true
	});
}

function consultarCatalogoSesion() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoSesion",
		type : "GET",
		async: true
	});
}

function consultarCatalogoEscenarios() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoEscenarios",
		type : "GET",
		async: true
	});
}

function consultarCatalogoDictamenPreliminar() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoDictamenPreliminar",
		type : "GET",
		async: true
	});
}

function consultarCatalogoDictamenFinal() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoDictamenFinal",
		type : "GET",
		async: true
	});
}

function consultarCatalogoSegmento() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoSegmento",
		type : "GET",
		async: true
	});
}

function consultarCatalogoTipologiaGerencia(cdGerencia) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoTipologiaGerencia/"+cdGerencia,
		type : "GET",
		async: true
	});
}

function consultarCatalogoRespuesta() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoRespuesta",
		type : "GET",
		async: true
	});
}

function getFormularioBusquedaCaso(){
	var formBusqueda = new Object();
	
	if($('#nuCaso').val() != ""){
	    formBusqueda.nuCaso = $('#nuCaso').val();
	}
	if($('#nuFolio').val() != ""){
		formBusqueda.nuFolio = $('#nuFolio').val();
	}
	if($('#numCliente').val() != ""){
		formBusqueda.nuCliente = $('#numCliente').val();
	}
	if($('#nomCliente').val() != ""){
		formBusqueda.nbCliente = $('#nomCliente').val();
	}
	if($('#numCuenta').val() != ""){
		formBusqueda.nuCuenta = $('#numCuenta').val();
	}
	if($('#cdTPersona').val()!= "-1"){
		 formBusqueda.cdTPersona = $('#cdTPersona').val();
	} 
	if($('#cmdTipologia').val()!= "-1"){
		 formBusqueda.cdTipologia = $('#cmdTipologia').val();
	}
	if($('#cdSegmento').val()!= "-1"){
		 formBusqueda.cdSegmento = $('#cdSegmento').val();
	}
	if($('#cdSesion').val()!= "-1"){
		formBusqueda.cdSesion = $('#cdSesion').val();
	} 
	if($('#cdRespuesta').val()!= "-1"){
		 formBusqueda.cdRespuesta = $('#cdRespuesta').val();
	}
	if($('#cdEscenario').val()!= "-1"){
		 formBusqueda.cdEscenario = $('#cdEscenario').val();
	}
	if($('#cdConsultor').val() != "-1"){
		formBusqueda.cdConsultor = $('#cdConsultor').val();
	}
	if($('#cdDictamenP').val() != "-1"){
		formBusqueda.cdDictamenP = $('#cdDictamenP').val();
	}
	if($('#cdDictamenF').val() != "-1"){
		formBusqueda.cdDictamenF = $('#cdDictamenF').val();
	}
	
	if($('#nuCalificacion').val() != ""){
		formBusqueda.nuCalificacion = $('#nuCalificacion').val();
		formBusqueda.cdCalificacion = $('#cdCalificacion option:selected').text();
	}
	
	if($('#nuPtsMatriz').val() != ""){
		formBusqueda.nuPtsMatriz = $('#nuPtsMatriz').val();
		formBusqueda.cdPtsMatriz = $('#cdPtsMatriz option:selected').text();
	}
	if($('#imImporte').val() != ""){
		var importe = replaceAll($('#imImporte').val() , ",", "" );
		formBusqueda.imImporte = importe ;
		formBusqueda.cdImporte = $('#cdImporte option:selected').text();
	}
	if($('#fhAlertaDel').val() != ""){
		formBusqueda.fhAlertaDel = $('#fhAlertaDel').datepicker('getDate');
	}
	if($('#fhRevisionDel').val() != ""){
		formBusqueda.fhRevisionDel = $('#fhRevisionDel').datepicker('getDate');
	}
	if($('#fhRevisionAl').val() != ""){
		formBusqueda.fhRevisionAl = $('#fhRevisionAl').datepicker('getDate');
	}
	if($('#fhAlertaAl').val() != ""){
		formBusqueda.fhAlertaAl = $('#fhAlertaAl').datepicker('getDate');
	}
	if($('#fhEnvioDel').val() != ""){
		formBusqueda.fhEnvioDel = $('#fhEnvioDel').datepicker('getDate');
	}
	if($('#fhEnvioAl').val() != ""){
		formBusqueda.fhEnvioAl = $('#fhEnvioAl').datepicker('getDate');
	}
	if($('#fhVencRevisionDel').val() != ""){
		formBusqueda.fhVencRevisionDel = $('#fhVencRevisionDel').datepicker('getDate');
	}
	if($('#fhVencRevisionAl').val() != ""){
		formBusqueda.fhVencRevisionAl = $('#fhVencRevisionAl').datepicker('getDate');
	}
	if($('#fhVencLiberacionDel').val() != ""){
		formBusqueda.fhVencLiberacionDel = $('#fhVencLiberacionDel').datepicker('getDate');
	}
	if($('#fhVencLiberacionAl').val() != ""){
		formBusqueda.fhVencLiberacionAl = $('#fhVencLiberacionAl').datepicker('getDate');
	}
	if($('#fhVencReporteDel').val() != ""){
		formBusqueda.fhVencReporteDel = $('#fhVencReporteDel').datepicker('getDate');
	}
	if($('#fhVencReporteAl').val() != ""){
		formBusqueda.fhVencReporteAl = $('#fhVencReporteAl').datepicker('getDate');
	}
	if($('#fhLiberacionDel').val() != ""){
		formBusqueda.fhLiberacionDel = $('#fhLiberacionDel').datepicker('getDate');
	}
	if($('#fhLiberacionAl').val() != ""){
		formBusqueda.fhLiberacionAl = $('#fhLiberacionAl').datepicker('getDate');
	}
	return formBusqueda;
}

$("#btnBuscar").click(function(){
	var isValid = $('#MSCAVE01009').valid();
	if( isValid )
	{
	consultarCasosDictamenFiltro(sessionStorage.cdUsuario,getFormularioBusquedaCaso()).done(function(data) { 
		if(cargarMensaje(data,false)){
			$('#tablaDictamendeCasos').bootstrapTable('load',data.response);
		}else{
			$('#tablaDictamendeCasos').bootstrapTable('load',[]);
		}
	});	
	}
});

$("#btnLimpiar").click(function(){

	consultarCasosDictamen(sessionStorage.cdUsuario).done(function(data) { 
		if(cargarMensaje(data,false)){
			$('#tablaDictamendeCasos').bootstrapTable('load',data.response);
		}else{
			$('#tablaDictamendeCasos').bootstrapTable('load',[]);
		}
	});	
	
});

function borrarDatos() {
	$('#nuCaso').val("");
	$('#nuFolio').val("");
	$('#numCliente').val("");
	$('#nomCliente').val("");
	$('#numCuenta').val("");
	$('#cdTPersona').val("-1");
	$('#cmdTipologia').val("-1");
	$('#cdSegmento').val("-1");
	$('#cdSesion').val("-1");
	$('#cdRespuesta').val("-1");
	$('#cdEscenario').val("-1");
	$('#cdConsultor').val("-1");
	$('#cdDictamenP').val("-1");
	$('#cdDictamenF').val("-1");
	$('#nuCalificacion').val("");
	$('#nuPtsMatriz').val("");
	$('#imImporte').val("");
	$('#fhAlertaDel').val("");
	$('#fhRevisionDel').val("");
	$('#fhRevisionAl').val("");
	$('#fhAlertaAl').val("");
	$('#fhEnvioDel').val("");
	$('#fhEnvioAl').val("");
	$('#fhVencRevisionDel').val("");
	$('#fhVencRevisionAl').val("");
	$('#fhVencLiberacionDel').val("");
	$('#fhVencLiberacionAl').val("");
	$('#fhVencReporteDel').val("");
	$('#fhVencReporteAl').val("");
	$('#fhVencRevisionDel').val("");
	$('#fhVencRevisionAl').val("");
	$('#fhLiberacionDel').val("");
	$('#fhLiberacionAl').val("");
	
	
	
	jQuery.validator.addMethod("isValidateRangoFechas", function(value, element) {
		if(value =! null && value != ''){
		return validarElementos( element.name );
		}
		else {
			return true;
			
		}
	}, "error");
}

function Mostrar(){
	if( encender==0){
		$('#btnLimpiar').show();
		$('#btnBuscar').show();
	encender = 1;
}else if( encender==1){
	$('#btnLimpiar').hide();
	$('#btnBuscar').hide();
		encender = 0;
	}	
}

function generaExcelDictamen(){
	exportaExcel(creaTblExcel('tablaDictamendeCasos', 5, '#0766ab', 'white', 'bold'),'Dictamen'); 
			
}

function invocarServicios(){
	if(parametros==0){
		consultarCatalogoSesion().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdSesion").html('');
			    $("#cdSesion").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdSesion").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdSesion").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoEscenarios().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdEscenario").html('');
			    $("#cdEscenario").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdEscenario").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdEscenario").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoSegmento().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdSegmento").html('');
			    $("#cdSegmento").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdSegmento").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdSegmento").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoDictamenPreliminar().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdDictamenP").html('');
			    $("#cdDictamenP").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdDictamenP").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdDictamenP").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
				
		consultarConsultoresSupervisor(sessionStorage.cdUsuario).done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdConsultor").html('');
		        $("#cdConsultor").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdConsultor").append('<option value="' + val.cdUsuario + '">' + val.nbPersona + ' ' + val.nbPaterno+ ' ' + val.nbMaterno+ '</option>');
				});    
			}else{
				$("#cdConsultor").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});	
		
		consultarCatalogoDictamenFinal().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdDictamenF").html('');
			    $("#cdDictamenF").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdDictamenF").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdDictamenF").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});

	consultarCatalogoTipologiaGerencia(sessionStorage.cdGerencia).done(function(data) { 
		if(cargarMensaje(data,false)){
			$("#cmdTipologia").html('');
		    $("#cmdTipologia").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
	    	$.each(data.response, function(key, val){
	    		$("#cmdTipologia").append('<option value="' + val.cdValor + '">'+ val.cdValor+'-'+val.nbValor+ '</option>');
			});
		}else{
			$("#cmdTipologia").html('<option value="-1">Ocurri\u00f3 un error</option>');
		}
	});
	
	consultarCatalogoRespuesta().done(function(data) { 
		if(cargarMensaje(data,false)){
			$("#cdRespuesta").html('');
		    $("#cdRespuesta").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
	    	$.each(data.response, function(key, val){
	    		$("#cdRespuesta").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
			});
		}else{
			$("#cdRespuesta").html('<option value="-1">Ocurri\u00f3 un error</option>');
		}
	});
	
	
	llenarSelectPorCdCatalogo('10',$('#cdCalificacion'),false);
	llenarSelectPorCdCatalogo('10',$('#cdPtsMatriz'),false);
	llenarSelectPorCdCatalogo('10',$('#cdImporte'),false);
	parametros =1;
	}
	else if(parametros==1){
		parametros =0;
		
	}
}

function validacionFormularioBusquedaDictamen(){
	$('#MSCAVE01009').validate( {
		rules: {
			nuCaso: {
				required: false,
				digits: true,
				maxlength:10
			},
			nuFolio: {
				required: false,
				digits: true,
				maxlength:10
			},
			numCliente: {
				required: false,
				digits: false,
				maxlength:8
			},
			nomCliente: {
				required: false,
				okValidator: true
			},			
			numCuenta: {
				required: false,
				formatoNumeroCuenta:true
			},
			cmdTipologia: {
				required: false,
				okValidator: true
			},
			cdSegmento: {
				required: false,
				okValidator: true
			},
			cdConsultor: {
				required: false,
				okValidator: true
			},
			cdDictamenP: {
				required: false,
				okValidator: true
			},
			nuCalificacion: {
				required: false,
				digits: true,
				maxlength:3
			},
			nuPtsMatriz: {
				required: false,
				digits: true,
				maxlength:3
			},
			cdRespuesta: {
				required: false,
				okValidator: true				
			},
			cdSesion: {
				required: false,
				okValidator: true				
			},
			cdEscenario: {
				required: false,
				okValidator: true				
			},
			imImporte: {
				required: false,
				okValidator: true
			},						
			fhAlertaDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhAlertaAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhEnvioDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhEnvioAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhVencRevisionDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhVencRevisionAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},			
			fhRevisionDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhRevisionAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},		
			fhVencLiberacionDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhVencLiberacionAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},	
			fhLiberacionDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhLiberacionAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhVencReporteDel: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
			fhVencReporteAl: {
				required: false,
				isValidateRangoFechas: true,
				date:false
			},
		},
		errorPlacement: function (error, element) {
			$(element).prop('style', 'border: 1px solid #C8175E;');
		},
		success: function (label, element) {
			$(element).prop('style', 'border: 1px solid #86C82D;');
		}
	});

}

function validarElementos( nombreElemento )
{
	var isValid = false;

	if( nombreElemento == 'fhAlertaDel' || nombreElemento == 'fhAlertaAl' )
	{
		isValid = validaCamposFecha( $("#fhAlertaDel").datepicker('getDate'), $("#fhAlertaAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhEnvioDel' || nombreElemento == 'fhEnvioAl' )
	{
		isValid = validaCamposFecha( $("#fhEnvioDel").datepicker('getDate'), $("#fhEnvioAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhVencRevisionDel' || nombreElemento == 'fhVencRevisionAl' )
	{
		isValid = validaCamposFecha( $("#fhVencRevisionDel").datepicker('getDate'), $("#fhVencRevisionAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhRevisionDel' || nombreElemento == 'fhRevisionAl' )
	{
		isValid = validaCamposFecha( $("#fhRevisionDel").datepicker('getDate'), $("#fhRevisionAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhVencLiberacionDel' || nombreElemento == 'fhVencLiberacionAl' )
	{
		isValid = validaCamposFecha( $("#fhVencLiberacionDel").datepicker('getDate'), $("#fhVencLiberacionAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhLiberacionDel' || nombreElemento == 'fhLiberacionAl' )
	{
		isValid = validaCamposFecha( $("#fhLiberacionDel").datepicker('getDate'), $("#fhLiberacionAl").datepicker('getDate') );
	}
	else if( nombreElemento == 'fhVencReporteDel' || nombreElemento == 'fhVencReporteAl' )
	{
		isValid = validaCamposFecha( $("#fhVencReporteDel").datepicker('getDate'), $("#fhVencReporteAl").datepicker('getDate') );
	}		
	return isValid;
}

