
var actionEvents = { 
		 /*
		 'click .detailCliente': function (e, value, row, index) {
				sessionStorage.cdCliente= row.cdCliente;
		    	$('#MSCAVE01011_2').load('MSCAVE01011_2.html', function (response, status, xhr){
		            if (status == "success") {
		                $(this).modal('show');
		            }else{
		            	data.cdMensaje = 0;
		            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
		            	cargarMensaje(data,false);
		            }
		        });
		    },
		  */
		/*
		    'click .detailDatos': function (e, value, row, index) {
		    	sessionStorage.nuCuenta = row.nuCuenta;
		    	sessionStorage.cdCliente = row.cdCliente;
		    	$('#MSCAVE01011_5').load('MSCAVE01011_5.html', function (response, status, xhr){
		            if (status == "success") {
		                $(this).modal('show');
		            }else{
		            	data.cdMensaje = 0;
		            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
		            	cargarMensaje(data,false);
		            }
		        });
		    },
		 */		
	    'click .detailPersona': function (e, value, row, index) {
	    	console.log( "Detalle de personas de alto riesgo" );
	    	sessionStorage.cdClialtrie = row.cdClialtrie;
	    	getPersonasReportadas()			
	    },
		'click .edit': function (e, value, row, index) {
			/*
			console.log( "== Mostrar pantalla Edicion de Clientes de Alto riesgo ==" );
			console.log( "== Registro Seleccionado ==" );
			console.log( JSON.stringify( row ) );
			//resetSessionStorage();
			cargarContenido('MSCAVE01014','Actualizaci\u00f3n Clientes de alto Riesgo')
			sessionStorage.clienteAREdit = ( 'clienteAREdit', JSON.stringify( row ) );
			$('#MSCAVE01014').load('MSCAVE01014.html', function (response, status, xhr){
				console.log( "== Detrmina si abre modal ==" );
				if (status == "success") {
					console.log( "== Abre Modal ==");
					$(this).modal('show');
				}else{
					console.log( "== No abre el modal ocurrio un error ==");
					data.cdMensaje = 0;
					data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
					cargarMensaje(data,false);
				}
			});
			*/			
		}
};

(function($, window, document) {
	$(function() {

		$('.date').datepicker({
		    language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true
		});
		cdClialtrie = sessionStorage.cdClialtrie;
		$('.money').maskMoney({prefix:'$',precision:2}); 
		consultarCatalogoTipos().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdTipo").html('');
			    $("#cdTipo").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdTipo").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdTipo").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		llenarSelectPorTablaClave('TipologiaCAR', $('#cdTipologia'), true);
		llenarSelectPorCdCatalogo('3',$('#cdPrioridad'),true);
		
		consultarRiesgos().done(function(data) { 
			if(cargarMensaje(data,false)){
		
				llenadoTablaRequerimiento(data.response);
				llenadoTablaPersonaReportada([]);
			
			}else{
				llenadoTablaRequerimiento([]);
			}
		});	
		$.validator.addMethod('formatoNumeroCuenta', function(value, element){
			var valor = value.toUpperCase();
			var tamanio = 0;
			if( valor != null && valor != '')
			{
				tamanio = valor.length;
				if( tamanio > 0 && tamanio < 10)
				{
					return false;
				}
				else if( tamanio > 10 && tamanio < 20 )
				{
					return false;
				}
				else{
					return true;
				}
			}
			else if( valor == null || valor == ''){
				return true;
			}
		}, "error");
		jQuery.validator.addMethod("okValidator", function(value, element, param) {
			  return true;
		}, "error");	
	
		validacionFormularioBusquedaRiesgo();
		
	});
}(window.jQuery, window, document));


function llenadoTablaPersonaReportada(data) {
	$('#tablaPersonaReportada').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}
function llenadoTablaRequerimiento(data) {
	$('#tablaRequerimiento').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}



function formatterDetailPersonaReportada(value, row, index){
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Domicilio:</b> '+row.nbDomicilio+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Complementarios:</b> '+row.nbComplementarios+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Cliente Bancomer:</b> '+row.cdClienteB+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>No. Cliente:</b> '+row.cdCliente+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>No. Cuenta:</b> '+row.nuCuenta+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Dictamen:</b> '+row.cdDictamen+'</td></tr>'+
	        '</table>'].join('');
}

function formatterPersonasReportadas(value, row, index) {
	return [
            '<a class="detailPersona" href="javascript:void(0)" title="Personas Reportadas">',
            	'<i class="tabla_registro_detalle">.</i>',
            '</a>'
            /*
            '<a class="edit" href="javascript:void(0)" title="Actualizar">',
            	'<i class="tabla_registro_editar">&nbsp;</i>',
            '</a>' 
            */    
    ].join(''); 
}

function formatterDetailRequerimiento(value, row, index){
	var nuDiasPlazo=row.nuDiasPlazo==null?"":row.nuDiasPlazo;
	var nbReferencia=row.nbReferencia==null?"":row.nbReferencia;
	var nbAseguramiento=row.nbAseguramiento==null?"":row.nbAseguramiento;
	var nbAutoridad=row.nbAutoridad==null?"":row.nbAutoridad;
	var nbSolicitud=row.nbSolicitud==null?"":row.nbSolicitud;
	var txtSolicitud=row.txtSolicitud==null?"":row.txtSolicitud;
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Dias de Plazo:</b> '+nuDiasPlazo+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Referencia:</b> '+nbReferencia+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Tiene Aseguramiento:</b> '+nbAseguramiento+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Autoridad:</b> '+nbAutoridad+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Solicitud Especifica:</b></td></tr>'+
	        '<tr><td class=""><div class="panel-body" style="height: auto px;width: auto px;"><textarea class="form-control" name="comentario" id="txtSolicitud1" rows="5" cols="40" style="width:auto px; height: auto px;" disabled> '+nbSolicitud +" "+ txtSolicitud +' </textarea></div></td></tr>'+
	       
	        
	        '</table>'].join('');
}

function formatterRegistroPersonasReportadas(value, row, index) {
	return [
            
	        '<a class="detailCliente" href="javascript:void(0)"  title="Cuentas del Cliente">',
            '<i class="tabla_registro_Cuentas_Cliente">.</i>',
            '</a>',
            '<a class="detailDatos" href="javascript:void(0)" title="Datos del Cliente">',
            '<i class="tabla_registro_Datos_Cliente">.</i>',
            '</a>'         
    ].join(''); 
}

function habilitar(value){
	if(value=="1"){

		// habilitamos
		
		$('#nuOficio').removeAttr('disabled');
		$('#nuExpediente').removeAttr('disabled');
		$('#nuFolio').removeAttr('disabled');
		$('#nbDescripcion').removeAttr('disabled');
		$('#fhPublicacion').removeAttr('disabled');
		$('#nuDias').removeAttr('disabled');
		$('#nbAutoridad').removeAttr('disabled');
		$('#nbReferencia').removeAttr('disabled');
		$('#nbAseguramiento').removeAttr('disabled');
		$('#txtSolicitud1').removeAttr('disabled');
		

	}

}

function abrirModalConfirmaPersonasReportadas11_4(){
	$('#dialogConfirmaPersonaReportada').modal('show');
}

function validacionPersonasReportadas11_4() {
	abrirModalConfirmaPersonasReportadas11_4();
	}

function consultarRiesgos() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/ConsultaClienteRiesgoService/consultarRiesgos",
		type : 'GET',
		async: true
	});
}

function consultarPersonasReportadas(cdClialtrie) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/ConsultaClienteRiesgoService/consultarPersonasReportadas/"+ cdClialtrie,
		type : 'GET',
		async: true
	});
}
function consultarRiesgosFiltro(data) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/ConsultaClienteRiesgoService/consultarRiesgosFiltro",
		type : 'POST',
		contentType:'application/json',
		dataType: 'JSON',
		data: JSON.stringify(data),
		async: false
	});
} 




function consultarCatalogoTipos() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoTipos",
		type : "GET",
		async: true
	});
}

function getFormularioBusquedaReporte(){
	var formBusqueda = new Object();
	
	
	if($('#cdTipo').val()!= "-1"){
		 formBusqueda.cdTipo = $('#cdTipo').val();
	}
	
	if($('#cdPrioridad').val()!= "-1"){
		 formBusqueda.cdPrioridad = $('#cdPrioridad').val();
	}
	
	if($('#cdTipologia').val()!= "-1"){
		formBusqueda.cdTipologia = $('#cdTipologia').val();
	}
	
	if($('#fhRecepcion').val() != ""){
		formBusqueda.fhRecepcion = $('#fhRecepcion').datepicker('getDate');
	}
	
	if($('#fhPublicacion').val() != "-1"){
		formBusqueda.fhPublicacion = $('#fhPublicacion').datepicker('getDate');
	}
	
	if($('#nuOficio').val() != ""){
		formBusqueda.nuOficio = $('#nuOficio').val();
	}
	
	if($('#nuExpediente').val() != ""){
		formBusqueda.nuExpediente = $('#nuExpediente').val();
	}
	
	if($('#nuFolio').val() != ""){
		formBusqueda.nuFolio = $('#nuFolio').val();
	}
	
	if($('#nuCuenta').val() != ""){
		formBusqueda.nuCuenta = $('#nuCuenta').val();

	}
	
	if($('#nbAutoridad').val() != ""){
		formBusqueda.nbAutoridad = $('#nbAutoridad').val();
	}
	if($('#nbReferencia').val() != ""){
		formBusqueda.nbReferencia = $('#nbReferencia').val();
	}
	if($('#nuCliente').val() != ""){
		formBusqueda.cdCliente = $('#nuCliente').val();
	}
	if($('#nbCliente').val() != ""){
		formBusqueda.nbCliente = $('#nbCliente').val();
	}
	
	return formBusqueda;
}



$("#btnBuscar").click(function(){
	var isValid = $('#MSCAVE01011_4').valid();
	if( isValid )
	{	
	consultarRiesgosFiltro(getFormularioBusquedaReporte()).done(function(data) { 
		if(cargarMensaje(data,false)){
			$('#tablaRequerimiento').bootstrapTable('load',data.response);
		}else{
			$('#tablaRequerimiento').bootstrapTable('load',[]);
		}
	});
	}
});

$("#btnLimpiar").click(function(){
	consultarRiesgosFiltro(getFormularioBusquedaReporte()).done(function(data) { 
		if(cargarMensaje(data,false)){
			$('#tablaRequerimiento').bootstrapTable('load',data.response);
		}else{
			$('#tablaRequerimiento').bootstrapTable('load',[]);
		}
	});
	consultarPersonasReportadas(sessionStorage.cdClialtrie).done(function(data) { 
		if(cargarMensaje(data,false)){
			$('#tablaPersonaReportada').bootstrapTable('load',[]);
		}
	});	
	
});


function getPersonasReportadas(cdClialtrie){
	
	consultarPersonasReportadas(sessionStorage.cdClialtrie).done(function(data) { 
		if(cargarMensaje(data,false)){
			$('#tablaPersonaReportada').bootstrapTable('load',data.response);
		}else{
			$('#tablaPersonaReportada').bootstrapTable('load',[]);
		}
	});	
	
}

function borrarDatos() {
	
	$('#cdTipo').val("-1");
	$('#cdTipologia').val("-1");
	$('#cdPrioridad').val("-1");
	$('#fhRecepcion').val("");
	$('#fhRecepcion').datepicker('setDate', null);
	$('#fhPublicacion').val("");
	$('#fhPublicacion').datepicker('setDate', null);
	$('#nuOficio').val("");
	$('#nuExpediente').val("");
	$('#nuFolio').val("");
	$('#nuCuenta').val("");
	$('#nbAutoridad').val("");
	$('#nbReferencia').val("");
	$('#nuCliente').val("");
	$('#nbCliente').val("");
	
	jQuery.validator.addMethod("okValidator", function(value, element, param) {
		  return true;
	}, "error");	
	
}

function validacionFormularioBusquedaRiesgo(){
	$('#MSCAVE01011_4').validate( {
		rules: {
	
			nuFolio: {
				required: false,
				digits: true,
				maxlength:20
			},		
			nuOficio: {
				required: false,
				okValidator: true
			},
			nuExpediente: {
				required: false,
				okValidator: true
			},
			nbReferencia: {
				required: false,
				okValidator: true
			},
			nbCliente: {
				required: false,
				okValidator: true
			},
			nuCliente: {
				required: false,
				okValidator: true
			},
			nuExpediente: {
				required: false,
				okValidator: true
			},
			
			nbAutoridad: {
				required: false,
				okValidator: true
			},
			nuCuenta: {
				required: false,
				formatoNumeroCuenta:true
			},
			cdTipologia: {
				required: false,
				okValidator: true
			},
			cdTipo: {
				required: false,
				okValidator: true
			},

			cdPrioridad: {
				required: false,
				okValidator: true				
			},				
			fhPublicacion: {
				required: false,
				date:false,
				okValidator: true
			},
			fhRecepcion: {
				required: false,
				date:false,
				okValidator: true
			},
			
		},
		errorPlacement: function (error, element) {
			$(element).prop('style', 'border: 1px solid #C8175E;');
		},
		success: function (label, element) {
			$(element).prop('style', 'border: 1px solid #86C82D;');
		}
	});

}