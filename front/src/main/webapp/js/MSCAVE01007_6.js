var data2= {"response":"","cdMensaje":0,"nbMensaje":"No existe n&ucute;mero de funcionario"};
(function($, window, document) {
	$(function( ) {	 
		var estatusCuest = null;
		var fechaVencimiento = null;
		var fechaActual = new Date();
		var dd = fechaActual.getDate();
		var mm = fechaActual.getMonth()+1;
		var yy = fechaActual.getFullYear().toString();
			if( dd < 10 ){
				dd = '0' + dd;
			}
			if( mm < 10 ){
				mm = '0' + mm;
			}
		fechaActual = yy+'-'+mm+'-'+dd;
		
		llenaOpinionAnalisisEnc(sessionStorage.cdSistema, sessionStorage.nuFolioAlerta).done(function(data) {
			$('#btnEnviar').hide();
	    	//$('#btnExportar').hide();
	    	$('#btnEnviarSinResp').hide();
	    	
			if(cargarMensaje(data,false)){    
				llenadoTablaCuestionarioEnc(data.response);
				
				
				var tamObjeto = Object.keys(data.response).length;
				$('#btnEnviarSinResp').show();//tablas vacias
				if(tamObjeto > 0){
					  estatusCuest = data.response[0].estatusCuest;
					  fechaVencimiento = data.response[0].fhVencimientoCuest2;
					  fechaRespuesta = data.response[0].fhRespuestaCuest;
					  
					  if(estatusCuest == "CERRADO SIN RESPUESTA FUNCIONARIO" || estatusCuest == "PENDIENTE"){
						if(fechaRespuesta != null){
						  $('#btnEnviar').show();
						  $('#btnEnviarSinResp').hide();
							if(fechaActual > fechaVencimiento){
								$('#btnEnviar').hide();
								$('#btnEnviarSinResp').hide();
							}
						}else{
							  $('#btnEnviar').hide();
							  $('#btnEnviarSinResp').show();
								if(fechaActual > fechaVencimiento){
									$('#btnEnviar').hide();
									$('#btnEnviarSinResp').hide();
								}
						  }
					  }
					  else if(estatusCuest == "EN REVISION DE PLD" || estatusCuest == "CERRADO CON RESPUESTA FUNCIONARIO"){
						  if(fechaRespuesta != null){
							  $('#btnEnviar').show();
							  $('#btnEnviarSinResp').hide();
								if(fechaActual > fechaVencimiento){
									$('#btnEnviar').hide();
									$('#btnEnviarSinResp').hide();
								}
						  }else{
							  $('#btnEnviar').hide();
							  $('#btnEnviarSinResp').show();
								if(fechaActual > fechaVencimiento){
									$('#btnEnviar').hide();
									$('#btnEnviarSinResp').hide();
								}
						  }
					  }
				}//fin if tamObjeto
		     }
		}); 
		
	
		
		llenaOpinionAnalisis(sessionStorage.cdSistema, sessionStorage.nuFolioAlerta).done(function(data) {
			if(cargarMensaje(data,false)){    
				llenadoTablaCuestionario(data.response);
			}});
	});
}(window.jQuery, window, document));

function llenadoTablaCuestionarioEnc(data){
	$('#tablaCuestionarioEnc').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}


function llenadoTablaCuestionario(data){
	$('#tablaCuestionario').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}


function llenaOpinionAnalisisEnc(cdSistema, nuFolioAlerta) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultarOpinionFuncionarioEnc/'+cdSistema + "/"+ nuFolioAlerta; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		  datatype: 'json'  
	}); 
}

function llenaOpinionAnalisis(cdSistema, nuFolioAlerta) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultarOpinionFuncionario/'+cdSistema + "/"+ nuFolioAlerta; 
	return $.ajax({
		url : _url,
		type : "GET",	
		  datatype: 'json'  
	}); 
}

//===============        agregar opinion funcionario        =================


function agregarOpinionFuncionarioLlenado() {
	agregarOpinionFuncionario(sessionStorage.cdSistema, sessionStorage.nuFolioAlerta, setOpinionFuncionario()).done(function(data) {
		if(cargarMensaje(data,true)){
			loadPanel('MSCAVE01007_6');
		}
	});		
}

function agregarOpinionFuncionario(cdSistema, nuFolioAlerta, opinionFuncionarioDTO) {
	
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/agregarOpinionFuncionario/"+ cdSistema + "/" + nuFolioAlerta,
		type : 'PUT',
		contentType: 'application/json',
		dataType: 'JSON',
		data: JSON.stringify(opinionFuncionarioDTO),
		async: false
	});	
}

function setOpinionFuncionario(){
	
	var opinionFuncionarioDTO = new Object();
	opinionFuncionarioDTO.tpCuestionario = sessionStorage.tpCuestionario;
	opinionFuncionarioDTO.cdAlerta = sessionStorage.nuFolioAlerta;	
	opinionFuncionarioDTO.cdSistema = sessionStorage.cdSistema;
	opinionFuncionarioDTO.cdOficinaGest = sessionStorage.cdOficinaGest;
	opinionFuncionarioDTO.txtComentarioCuest = $('#txtComentariosFuncionario').val();

	return opinionFuncionarioDTO;
}

function EnviarCuestionario(){  
	$('#dialogEnvioDeCuestionario').modal('show'); 
	$('#nbTitulo').val("");
}

function EnviarCuestionarioSinResp(){  
	$('#dialogEnvioDeCuestionarioSinResp').modal('show'); 
	$('#nbTituloSinResp').val("");
	
}

function limpiarCuestionario(){
	$('#txtComentariosFuncionario').val("");
}

function generaExcelEnviarCuestionario(){
	exportaExcel(
			creaTblExcel('tablaCuestionarioEnc', 9, '#0766ab', 'white', 'bold')
			+ '<br>' + creaTblExcel('tablaCuestionario', 2, '#0766ab', 'white', 'bold'), 'Alerta_Detalle_Escenarios');
};

