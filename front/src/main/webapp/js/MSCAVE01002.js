(function($, window, document) {
	$(function() {
		$('#btnExportExcel').on('click',function (){
			generaExcelAlerta();
		});
		loadPanel('MSCAVE01002_1');
	});
}(window.jQuery, window, document));

function loadPanel(idPanel){
	$('#'+idPanel).load(idPanel+'.html', function (response, status, xhr){
	    if (status != "success") {
	    	data.cdMensaje = 0;
	    	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	    	cargarMensaje(data,false);
	    }
	});
}

function generaExcelAlerta(){
	exportaExcel(creaTblExcel('tablaAlerta', 1, '#0766ab', 'white', 'bold') 
			+ '<br>' + creaTblExcel('tablaTipologia', 1, '#0766ab', 'white', 'bold') 
			+ '<br>' + creaTblExcel('tablaEscenarios', 2, '#0766ab', 'white', 'bold' )
			+ '<br>' + creaTblExcel('tablaMatrizCriterios', 2, '#0766ab', 'white', 'bold'), 'Detalle_Alerta');
};