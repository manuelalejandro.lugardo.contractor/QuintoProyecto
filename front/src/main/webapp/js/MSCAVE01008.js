var data2= {"response":"","cdMensaje":0,"nbMensaje":"Ha alcanzado el l&iacute;mite de Casos a Liberar a SIA"};
//var data3= {"response":"","cdMensaje":0,"nbMensaje":"Faltaron campos por llenar"};

(function($, window, document) {
	$(function() {
		tipoDictamen(sessionStorage.opDictamen);
		$('#headOpeInusual').html('Registro de Operaci&oacute;n inusual');
		$('#headRazones').html('Razones por las cuales se considera inusual');
		$('#lblCausaOperacion').html('Causas por las que considera inusual esta operaci&oacute;n:');
		
		consultarCatalogoTipoCuenta().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#tCuentas").html('');
			    $("#tCuentas").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#tCuentas").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#tCuentas").html('<option value="-1i">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoTipoOperacionMonetaria().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#tMonetario").html('');
			    $("#tMonetario").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#tMonetario").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#tMonetario").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoInstrumentoMonetario().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#iMonetario").html('');
			    $("#iMonetario").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#iMonetario").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#iMonetario").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoMoneda().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#moneda").html('');
			    $("#moneda").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#moneda").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#moneda").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		setCausaDescarte($('input[name=rdbDictamen]:checked').val());
		
		consultarDictamenPreliminar(sessionStorage.cdCaso).done(function(data) { 
			if(cargarMensaje(data,false)){
				setLlenadoFormularioDicatemenPreliminar(data.response);
			}
		});
		validarDictamen();
		habilitar($('input[name=rdbDictamen]:checked').val());
		
		jQuery.validator.addMethod("okValidator", function(value, element, param) {
			  return true;
		}, "error");
	});
}(window.jQuery, window, document));

function setCausaDescarte(tipoDictamen){
	
	if(tipoDictamen == "LI"){
		consultarCatalogoOperacionInusual("LI").done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cOperacion").html('');
			    $("#cOperacion").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cOperacion").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cOperacion").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		consultarDictamenPreliminarTxtComentario(sessionStorage.cdCaso).done(function(data) { 
			if(cargarMensaje(data,false)){
				var txt = "";
		    	$.each(data.response, function(key, val){
		    		txt += val.otrasGerencias;
		    		$("#txtcomentario").val(txt);
				});
			}else{
				$("#txtcomentario").html('Ocurri\u00f3 un error');
			}
		});
	}else if(tipoDictamen == "DE"){
		
		consultarCatalogoOperacionInusual("DE").done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cOperacion").html('');
			    $("#cOperacion").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cOperacion").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cOperacion").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		$("#txtcomentario").val('');
		
	}else if(tipoDictamen == "RE"){
		
		$('#causasRegresarConsultorSelect').val("-1");
		$('#razonesRegreso').val("");
		$('#panelRegresaConsultor').show();
		$('#causasRegresarConsultorSelect').removeAttr('disabled');
		$('#razonesRegreso').removeAttr('disabled');
		$('#panelRegresaConsultor').removeClass('panel panel-default');
		$('#panelRegresaConsultor').addClass('panel panel-primary');
		
		consultarCatalogoOperacionInusual("RE").done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#causasRegresarConsultorSelect").html('');
			    $("#causasRegresarConsultorSelect").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#causasRegresarConsultorSelect").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#causasRegresarConsultorSelect").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
	}
}

function habilitar(value){
	
	if(value=="LI"){
		// habilitamos
		$('#tCuentas').removeAttr('disabled');
		$('#tMonetario').removeAttr('disabled');
		$('#iMonetario').removeAttr('disabled');
		$('#moneda').removeAttr('disabled');
		$('#cOperacion').removeAttr('disabled');
		$('#SID').removeAttr('disabled');
		$('#NOD').removeAttr('disabled');
		$('#txtcomentario').removeAttr('disabled');
		$('#txtcomentario1').removeAttr('disabled');
		$('#headOpeInusual').html('Registro de Operaci&oacute;n Inusual');
		$('#headRazones').html('Razones por las cuales se considera inusual');
		$('#lblCausaOperacion').html('Causas por las que considera Inusual esta operaci&oacute;n:');
		$('#enviar').removeAttr('disabled');
		$('#panelRegresaConsultor').hide();
		$('#oculto').addClass('panel panel-primary');
	
	}else if(value=="DE"){
		// deshabilitamos
		$('#tCuentas').attr('disabled','disabled');
		$('#tMonetario').attr('disabled','disabled');
		$('#iMonetario').attr('disabled','disabled');
		$('#moneda').attr('disabled','disabled');
		$('#cOperacion').removeAttr('disabled');
		$('#SID').attr('disabled','disabled');
		$('#NOD').attr('disabled','disabled');
		$('#txtcomentario').attr('disabled','disabled');
		$('#txtcomentario1').removeAttr('disabled');
		$('#headOpeInusual').html('Registro de Operaci&oacute;n No Inusual');
		$('#headRazones').html('Razones por las cuales se considera No inusual');
		$('#lblCausaOperacion').html('Causas por las que considera No Inusual esta operaci&oacute;n:');
		$('#enviar').removeAttr('disabled');
		$('#panelRegresaConsultor').hide();
		$('#oculto').addClass('panel panel-primary');
	}
	else if(value=="RE"){
		$('#tCuentas').attr('disabled','disabled');
		$('#tMonetario').attr('disabled','disabled');
		$('#iMonetario').attr('disabled','disabled');
		$('#moneda').attr('disabled','disabled');
		$('#cOperacion').attr('disabled','disabled');
		$('#SID').attr('disabled','disabled');
		$('#NOD').attr('disabled','disabled');
		$('#txtcomentario').attr('disabled','disabled');
		$('#txtcomentario1').attr('disabled','disabled');
		$('#enviar').attr('disabled','disabled');
		$('#causasRegresarConsultorSelect').attr('disabled','disabled');
		$('#razonesRegreso').attr('disabled','disabled');
		$('#panelRegresaConsultor').show();
		$('#oculto').removeClass('panel panel-primary');
		$('#oculto').addClass('panel panel-default');
		$('#panelRegresaConsultor').removeClass('panel panel-primary');
		$('#panelRegresaConsultor').addClass('panel panel-default');
	}
}



function tipoDictamen(opDictamen){
	if(opDictamen == 'P'){
		$('#ZO').attr('disabled','disabled');
		$('#dPreliminar1').show();
		$('#enviar').show();
		$('#nbRegresar').show();
		$('#radioFinal').show();
		$('#Z01').show();
		$('#ZO').display="block";
	}else if(opDictamen == 'F'){
		$('#dFinal1').show(); 
		$('#dPreliminar1').hide();
		$('#nbRegresar').show();
		$('#radioFinal').show();
		$('#enviar').hide();
		$('#enviar1567').show();
		$('#Z01').show();
		$('#ZO').display="block";
	}
}
function abrirModalConfirmaSupervisor(){
	$('#dialogComfirmaSupervisor').modal('show');
}

function abrirModalConfirmaEnviarSupervisor(){
	$('#dialogComfirmaEnviarSupervisor').modal('show');
}

function regresar(){
$('#dictamenFinal').modal('hide');
}

function consultarCatalogoTipoCuenta() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoTipoCuenta",
		type : "GET",
		async: false
	});
}

function consultarCatalogoTipoOperacionMonetaria() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoTipoOperacionMonetaria",
		type : "GET",
		async: false
	});
}

function consultarCatalogoInstrumentoMonetario() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoInstrumentoMonetario",
		type : "GET",
		async: false
	});
}

function consultarCatalogoMoneda() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoMoneda",
		type : "GET",
		async: false
	});
}

function consultarCatalogoOperacionInusual(cOperacion) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoOperacionInusual/" + cOperacion,
		type : "GET",
		async: false
	});
}

//Funciones de Dictamen Preliminar

function insertarDictamenPreliminarLlenado() {
	insertarDictamenPreliminar(sessionStorage.cdCaso,setFormularioDicatemenPreliminar()).done(function(data) {
		if(cargarMensaje(data,true)){
			consultarCasosAsignadosConsultor(sessionStorage.cdUsuario).done(function(data) { 
				if(cargarMensaje(data,false)){
					$('#tablaCasosAsignados').bootstrapTable('load',data.response);
				}else{
					$('#tablaCasosAsignados').bootstrapTable('load',[]);
				}
			});	
		}
	});
}

function insertarDictamenPreliminar(cdCaso,dictamenPreliminarDTO) {
	
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/DictamenService/asignarDictamenPreliminar/" + cdCaso,
		type : 'PUT',
		contentType: 'application/json',
		dataType: 'JSON',
		data: JSON.stringify(dictamenPreliminarDTO),
		async: false
	});	
}

function setFormularioDicatemenPreliminar(){
	
	var dictamenPreliminarDTO = new Object();
	
	if($('#tCuentas').val()!= "-1"){
		dictamenPreliminarDTO.tCuentas = $('#tCuentas').val();
	} 
	
	if($('#tMonetario').val()!= "-1"){
		dictamenPreliminarDTO.tMonetario = $('#tMonetario').val();
	}
	
	if($('#iMonetario').val()!= "-1"){
		dictamenPreliminarDTO.iMonetario = $('#iMonetario').val();
	} 
	
	if($('#moneda').val()!= "-1"){
		dictamenPreliminarDTO.moneda = $('#moneda').val();
	}
	
	if($('#cOperacion').val()!= "-1"){
		dictamenPreliminarDTO.cOperacion = $('#cOperacion').val();
	}
	if($('#causasRegresarConsultorSelect').val()!= "-1"){
		dictamenPreliminarDTO.causasRegresarConsultorSelect = $('#causasRegresarConsultorSelect').val();
	}
	
	dictamenPreliminarDTO.stCuentaCancelada = $('input[name=rdbOp]:checked').val();
	dictamenPreliminarDTO.cdDictamen = $('input[name=rdbDictamen]:checked').val();
	
	
	dictamenPreliminarDTO.txtcomentario = $('#txtcomentario').val();
	dictamenPreliminarDTO.txtcomentario1 = $('#txtcomentario1').val();
	dictamenPreliminarDTO.razonesRegreso = $('#razonesRegreso').val();
	
	return dictamenPreliminarDTO;
}

//Funciones de Consultar Dictamenes

function consultarDictamenPreliminar(cdCaso) {
	
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/DictamenService/consultarDictamenPreliminar/" + cdCaso,
		type : 'GET',
		contentType: 'application/json',
		dataType: 'JSON',
		async: false
	});	
}

function setLlenadoFormularioDicatemenPreliminar(dictamenPreliminarDTO){
	
	//var dictamenPreliminarDTO = new Object();
	
	if(dictamenPreliminarDTO.tCuentas != null){
		$('#tCuentas').val(dictamenPreliminarDTO.tCuentas);
	}
	
	if(dictamenPreliminarDTO.tMonetario != null){
		$('#tMonetario').val(dictamenPreliminarDTO.tMonetario);
	} 
	
	if(dictamenPreliminarDTO.iMonetario != null){
		$('#iMonetario').val(dictamenPreliminarDTO.iMonetario);
	}
	
	if(dictamenPreliminarDTO.moneda != null){
		$('#moneda').val(dictamenPreliminarDTO.moneda);
	}
	
	$('input[name=rdbOp]').each(function(){
		if($(this).val()== dictamenPreliminarDTO.stCuentaCancelada){
			$(this).prop('checked',true);
		}
	});
	
	$('input[name=rdbDictamen]').each(function(){
		if($(this).val()== dictamenPreliminarDTO.cdDictamen){
			$(this).prop('checked',true);
			
			setCausaDescarte($('input[name=rdbDictamen]:checked').val());
			
			if(dictamenPreliminarDTO.cOperacion >= 1 && dictamenPreliminarDTO.cOperacion <= 28){
				setCausaDescarte("LI");
				if(dictamenPreliminarDTO.cOperacion != null){
					$('#cOperacion').val(dictamenPreliminarDTO.cOperacion);
				}
			}else if (dictamenPreliminarDTO.cOperacion >= 29 && dictamenPreliminarDTO.cOperacion <= 39){
				setCausaDescarte("DE");
				if(dictamenPreliminarDTO.cOperacion != null){
					$('#cOperacion').val(dictamenPreliminarDTO.cOperacion);
				}
			}
			
			if(dictamenPreliminarDTO.causasRegresarConsultorSelect != null){
				$('#causasRegresarConsultorSelect').val(dictamenPreliminarDTO.causasRegresarConsultorSelect);
			}
			habilitar($(this).val());
		}
	});
	
	$('#txtcomentario').val(dictamenPreliminarDTO.txtcomentario);	
	$('#txtcomentario1').val(dictamenPreliminarDTO.txtcomentario1);
	$('#razonesRegreso').val(dictamenPreliminarDTO.razonesRegreso);
	
	return dictamenPreliminarDTO;
}

//Consulta Tope de Casos

function llamadoConsultaTope() {
	
	if($('input[name=rdbDictamen]:checked').val() == "LI"){
		
		consultarTopeCasos(sessionStorage.cdSesion,sessionStorage.cdGerencia).done(function(data) {
			if(cargarMensaje(data,false)){
				setConsultaTopeCasos(data.response);
			}
		});
	}
	else{
		insertarDictamenFinal(sessionStorage.cdCaso,setFormularioDicatemenPreliminar()).done(function(data) {
			if(cargarMensaje(data,true)){
				consultarCasosDictamen(sessionStorage.cdUsuario).done(function(data) { 
					if(cargarMensaje(data,false)){
						$('#tablaDictamendeCasos').bootstrapTable('load',data.response);
					}else{
						$('#tablaDictamendeCasos').bootstrapTable('load',[]);
					}
				});	
			}
		});	
	}
		
	
	
}

function consultarTopeCasos(cdSesion,cdGerencia) {
	
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/DictamenService/getConsultaDictamenesLiberados/" + cdSesion + '/' + cdGerencia,
		type : 'GET',
		contentType: 'application/json',
		dataType: 'JSON',
		async: false
	});	
}

function setConsultaTopeCasos(dictamenPreliminarDTO){
	
	if(dictamenPreliminarDTO.liberadosSIA >= dictamenPreliminarDTO.topeCasos){
			cargarMensaje(data2,false);
	}
	else{
			insertarDictamenFinal(sessionStorage.cdCaso,setFormularioDicatemenPreliminar()).done(function(data) {
				if(cargarMensaje(data,true)){
					consultarCasosDictamen(sessionStorage.cdUsuario).done(function(data) { 
						if(cargarMensaje(data,false)){
							$('#tablaDictamendeCasos').bootstrapTable('load',data.response);
						}else{
							$('#tablaDictamendeCasos').bootstrapTable('load',[]);
						}
					});	
				}
			});		
	}	
	return dictamenPreliminarDTO;
}

//Funciones Dictamen Final



function insertarDictamenFinal(cdCaso,dictamenPreliminarDTO) {
	
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/DictamenService/asignarDictamenFinal/" + cdCaso,
		type : 'PUT',
		contentType: 'application/json',
		dataType: 'JSON',
		data: JSON.stringify(dictamenPreliminarDTO),
		async: false
	});	
}

function borrarDatosDictamen() {
	
	$('#tCuentas').val("-1");
	$('#tMonetario').val("-1");
	$('#iMonetario').val("-1");
	$('#moneda').val("-1");
	$('#cOperacion').val("-1");
	//$('#txtcomentario').val("");
	$('#txtcomentario1').val("");
	
}

function validarDictamen()
{
	$.validator.addMethod('validaSelect',function (value, element, arg){
		return arg != value;
	},'error!');
	var validador = $('#MSCAVE01008_FORM').validate(
		{
			rules: {
				tCuentas: {
					required: true,
					validaSelect: '-1'
				},
				tMonetario: {
					required: true,
					validaSelect: '-1'
				},
				iMonetario: {
					required: true,
					validaSelect: '-1'
				},
				moneda: {
					required: true,
					validaSelect: '-1'
				},
				cOperacion: {
					required: true,
					validaSelect: '-1'
				},
				causasRegresarConsultorSelect: {
					required: true,
					validaSelect: '-1'
				},
				txtcomentario1: {
					required: true,
					okValidator: true				
				},
				txtcomentario: {
					required: true,
					okValidator: true				
				},
				razonesRegreso: {
					required: true,
					okValidator: true				
				}
			},
			errorPlacement: function (error, element) {
				$(element).prop('style', 'border: 1px solid #C8175E;');
			},
			success: function (label, element) {
				$(element).prop('style', 'border: 1px solid #86C82D;');
				$('#enviar').removeAttr('disabled');
				$('#enviar1').removeAttr('disabled');
			}
		}
	);
}

function validacionDictamenFin() {
	var dictamenValido = $('#MSCAVE01008_FORM').valid();
	
	if(dictamenValido){
		$('#dialogComfirmaSupervisor').modal('show');
	}else{
		$('#enviar1').attr('disabled','disabled');
	}
}

function validacionDictamenPre() {
	var dictamenValido = $('#MSCAVE01008_FORM').valid();
	
	if(dictamenValido){
		$('#dialogComfirmaEnviarSupervisor').modal('show');
	}else{
		$('#enviar').attr('disabled','disabled');
	}
}

//Funciones para llenar txtcomentario
function consultarDictamenPreliminarTxtComentario(cdCaso) {
	
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/DictamenService/consultarDictamenPreliminarTxtComentario/" + cdCaso,
		type : 'GET',
		contentType: 'application/json',
		dataType: 'JSON',
		async: false
	});	
}