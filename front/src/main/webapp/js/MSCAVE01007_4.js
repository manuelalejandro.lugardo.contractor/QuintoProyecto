(function($, window, document) {
	$(function() {		 
		$('.money').maskMoney({precision:2});
		//llenarFormulario(dataForm1_1.response,'contenedorMSCAVE01007_4');
 
		 			//llenarFormulario(data.response,'contenedorMSCAVE01007_4');
			llenaKYC(sessionStorage.cdCaso,sessionStorage.cdCliente).done(function(data) {
				if(cargarMensaje(data,false)){    
						llenarFormulario(data.response,'contenedorMSCAVE01007_4');
						 
				}});
			
			llenaAccionista(sessionStorage.cdCaso).done(function(data) {
				if(cargarMensaje(data,false)){     
				llenadoTablaIAEE(data.response);
			}});
			 
			 
				//llenadoTablaTDC(data.response);
				//llenadoTablaIAEE(data.response);
			 
		 
		//llenadoTablaTDC(data.response);
		//llenadoTablaIAEE(data1.response);
	});
}(window.jQuery, window, document));

function llenadoTablaTDC(data) {
	$('#tablaTDC').bootstrapTable({
		pagination: false,
		pageSize: 3,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaIAEE(data) {
	$('#tablaIAEE').bootstrapTable({
		pagination: false,
		pageSize: 3,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}
function llenaKYC(cdCaso,cdCliente, data,apartado) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultaKYC/'+cdCaso+'/'+cdCliente; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		  datatype: 'json' 
		 
	}); 
	$.each(data, function(key, value) {
		var input = $('#'+key);
		var dataFormat = $('#'+apartado).find(input).attr('data-format');
		if(dataFormat != "" && dataFormat != undefined){
			value  = eval(dataFormat+'('+$.trim(value)+')');
			$('#'+apartado).find(input).val($.trim(value));
		}else{
			$('#'+apartado).find(input).val($.trim(value));
		}
	});
} 

function llenaAccionista(cdCaso) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultaAccionistasDetCaso/'+cdCaso; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		  datatype: 'json' 
		 
	}); 
} 
