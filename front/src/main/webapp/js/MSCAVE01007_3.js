 var actionEvents = { 
	    'click .detailMovimientos': function (e, value, row, index) {
	    	$('#MSCAVE01011_3').load('MSCAVE01011_3.html', function (response, status, xhr){
	            if (status == "success") {
	                $(this).modal('show');
	            }else{
	            	data.cdMensaje = 0;
	            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	            	cargarMensaje(data,false);
	            }
	        });
	    },
	    'click .detailFideicomiso': function (e, value, row, index) { 
				 	 $('#nuReferenciaDet').val(row.nuReferencia);
				 	 $('#nbFinalidadDet').val(row.nbFinalidad);
				 	 $('#nbLugConstitucionDet').val(row.nbLugConstitucion);
				 	 $('#fhConstitucionDet').val(row.fhConstitucion);
				 	 $('#nbInsFiduciariaDet').val(row.nbInsFiduciaria);
					 $('#nbPatFideicomitidoDet').val(row.nbPatFideicomitido);
					 $('#imAportacionesDet').val(row.imAportaciones);  
	    	
	    	$('#dialogDetalleFideicomiso').modal('show');
	    }
	};

(function($, window, document) {
	
	$(function() {
		llenaCuentasCliente(sessionStorage.cdSistema, sessionStorage.nuFolioAlerta, sessionStorage.cdCaso).done(function(data) {
			if(cargarMensaje(data,false)){ 
				llenadoTablaCCR(data.response);		
			}}); 
		
		llenaOtrosProductos(sessionStorage.cdSistema, sessionStorage.nuFolioAlerta, sessionStorage.cdCaso).done(function(data) {
			if(cargarMensaje(data,false)){ 
				llenadoTablaOPCRI(data.response.tablaCredito);		
			}});  
		llenaFideicomiso(sessionStorage.cdCaso).done(function(data) {
			if(cargarMensaje(data,false)){ 
				llenadoTablaFideicomiso(data.response);		
			}});  
		llenaDetalleFideicomiso(sessionStorage.cdCaso).done(function(data) {
			if(cargarMensaje(data,false)){ 
				llenadoTablaPersonasRelacionadas(data.response);		
			}});   
	});
}(window.jQuery, window, document));

function llenadoTablaOPCRI(data) {
	$('#tablaOPCRI').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaCCR(data) {
	$('#tablaCCR').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaFideicomiso(data) {
	$('#tablaFideicomiso').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaPersonasRelacionadas(data) {
	$('#tablaPersonasRelacionadas').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function abrirDetalleFideicomiso(){
	$('#dialogDetalleFideicomiso').modal('show');
}

function formatterDetailCCR(value, row, index){
	
	var nuCtePartCCR=row.nuCtePartCCR==null?"":row.nuCtePartCCR;
	var nbParticipesCCR=row.nbParticipesCCR==null?"":row.nbParticipesCCR;
	var imSaldoCCR=row.imSaldoCCR==null?"":row.imSaldoCCR;
	var imSaldoPromCCR=row.imSaldoPromCCR==null?"":row.imSaldoPromCCR;
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>No. Cte. Part.:</b> '+nuCtePartCCR+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Participe:</b> '+nbParticipesCCR+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Saldo:</b> '+formatterMoney(Number(imSaldoCCR))+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Saldo Prom.:</b> '+formatterMoney(imSaldoPromCCR)+'</td></tr>'+
	        '</table>'].join('');
}

function formatterDetailOPCRI(value, row, index){ 
	
	var nuCtePartOPCR=row.nuCtePartOPCR==null?"":row.nuCtePartOPCR;
	var nbParticipesOPCR=row.nbParticipesOPCR==null?"":row.nbParticipesOPCR;
	var imProductoOPCR=row.imProductoOPCR==null?"":row.imProductoOPCR;
	var imProPromOPCR=row.imProPromOPCR==null?"":row.imProPromOPCR;
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>No. Cte. Part.:</b> '+nuCtePartOPCR+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Participe:</b> '+nbParticipesOPCR+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Saldo:</b> '+formatterMoney(imProductoOPCR)+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Saldo Prom.:</b> '+formatterMoney(imProPromOPCR)+'</td></tr>'+
	        //'<tr><td class="tdDetail"><b>L&iacute;mite de Cr&eacute;dito.:</b> '+formatterMoney(row.imProPromOPCR)+'</td></tr>'+
	        '</table>'].join('');
}

function formatterDetailFideicomiso(value, row, index){
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Patrimonio Fideicomitido:</b> '+row.nbPatFideicomitido+'</td></tr>'+
	        '</table>'].join('');
}

function formatterPersonasRelacionadas(value, row, index){
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Nombre:</b> '+row.nbCliente+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>No. de Identificaci&oacute;n Fiscal (NIF):</b> '+row.nuFiscal+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Pa&iacute;s que Asigna el NIF:</b> '+row.cdPais+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>No. de Serie de la Firma Electr&oacute;nica Avanzada:</b> '+row.nuFea+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Sexo:</b> '+row.cdSexo+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha de Nacimiento o Constituci&oacute;n:</b> '+row.fhNacimiento+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Entidad de Nacimiento o Constituci&oacute;n:</b> '+row.cdEntidad+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Nacionalidad:</b> '+row.cdNacionalidad+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Ocupaci&oacute;n o Profesi&oacute;n, Actividad o Giro:</b> '+row.cdOcupacion+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>CURP:</b> '+row.cdCurp+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>RFC:</b> '+row.cdRFCDet+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Correo Electr&oacute;nico:</b> '+row.cdCorreo+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Tel&eacute;fono:</b> '+row.cdTelefono+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Calle:</b> '+row.nbCalle+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>No. Exterior:</b> '+row.nuExterior+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>No. Interior:</b> '+row.nuInterior+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>C&oacute;digo Postal:</b> '+row.cdPostal+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Colonia:</b> '+row.nbColonia+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Delegaci&oacute;n o Municipio:</b> '+row.nbDelegacion+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Entidad Federativa:</b> '+row.nbEntidad+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Pa&iacute;s:</b> '+row.cdPaisDet+'</td></tr>'+
	        '</table>'].join('');
	        
}

function formatterCuentasCliente(value, row, index) {
	return [
            '<a class="detailMovimientosS" href="javascript:void(0)" title="Movimientos">',
            '<i class="tabla_registro_detalle_movimientos">.</i>',
            '</a>'
    ].join(''); 
}

function formatterFideicomiso(value, row, index) {
	return [
            '<a class="detailFideicomiso" href="javascript:void(0)" title="Movimientos">',
            '<i class="tabla_registro_detalle_fideicomiso">.</i>',
            '</a>'
    ].join(''); 
}




function llenaCuentasCliente(cdSistema, nuFolioAlerta, cdCaso) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarCuentasClienteReportado/"+ cdSistema +"/" + nuFolioAlerta  +"/" + cdCaso,
		type : "GET",
		async: false
	});
}


function llenaOtrosProductos(cdSistema, nuFolioAlerta, cdCaso) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarOtrosProductosCR/"+ cdSistema +"/" + nuFolioAlerta +"/" + cdCaso,
		type : "GET",
		async: false
	});
}


function llenaFideicomiso(cdCaso) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultaFideicomisoCaso/"+ cdCaso,
		type : "GET",
		async: false
	});
}

function llenaDetalleFideicomiso(cdCaso) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultaDetalleFideicomisoCaso/"+ cdCaso,
		type : "GET",
		async: false
	});
}