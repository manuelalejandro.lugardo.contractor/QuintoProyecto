var personaReportada = new Object();
var actionEvents = { 
	    'click .detailMovimiento': function (e, value, row, index) {
	    	sessionStorage.nuCuenta= row.nuCuenta;
	    	$('#MSCAVE01011_3').load('MSCAVE01011_3.html', function (response, status, xhr){
	            if (status == "success") {
	                $(this).modal('show');
	            }else{
	            	data.cdMensaje = 0;
	            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	            	cargarMensaje(data,false);
	            }
	        });
	    },
	    
	    'click .detailReporte': function (e, value, row, index) {
	    	
	    	personaReportada.cdCliente = row.cdCliente;
	    	personaReportada.nuCuenta = row.nuCuenta;
	    	$('#ReportarC').modal('show');
	    	
	    }  
	};

(function($, window, document) {
	$(function() {	
		$('.date').datepicker({
		    language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true
		});
		
		$('.money').maskMoney({prefix:'$',precision:2}); 
		//$('#nuCaso').html(sessionStorage.cdCaso);
		//llenadoTablaCuentasCliente(data2.response);
		
		llenarSelectPorTablaClave('TipologiaCAR', $('#cdTipologia'), true);
		llenadoTablaCuentasCliente([]);
		cuentasClienteRiesgo(sessionStorage.cdCliente).done(function(data) { 
			
			if(cargarMensaje(data,false)){
				$('#tablaCuentasCliente').bootstrapTable('load',data.response);	
			}
		});	
		
		
		
	
	});
}(window.jQuery, window, document));

function llenadoTablaCuentasCliente(data) {
	$('#tablaCuentasCliente').bootstrapTable({
		pagination: false,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}




function formatterRadioCuentasCliente(value, row, index){
	return ['<input type="radio" name="rdbCuentasCliente" id="CO_'+row.nuCliente+'" class="form-control" /><label for="CO_'+row.nuCliente+'"></label>'].join('');
}


function formatterRegistroCasosAsignados(value, row, index) {
	return [
           
            '<a class="detailMovimiento" href="javascript:void(0)" title="Consulta de Movimientos">',
            '<i class="tabla_registro_CMovimientos">.</i>',
            '</a>',
            '<a class="detailReporte" href="javascript:void(0)" title="Reportar Cuenta">',
            '<i class="tabla_registro_editar">.</i>',
            '</a>'
           
            
            
           
    ].join(''); 
}

function formatterDetailCuentasCliente(value, row, index){
	var nbTparti=row.nbTparti==null?"":row.nbTparti;
	var stEstatus=row.stEstatus==null?"":row.stEstatus;
	var nbDescripcion=row.nbDescripcion==null?"":row.nbDescripcion;
	var fhCancelacion=row.fhCancelacion==null?"":formatterDate(row.fhCancelacion);
	var nbBloqueo=row.nbBloqueo==null?"":row.nbBloqueo
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Tipo:</b> '+nbTparti+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Estado:</b> '+stEstatus+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Descrici&oacute;n:</b> '+nbDescripcion+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha Cancelaci&oacute;n:</b> '+fhCancelacion+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Bloqueo:</b> '+nbBloqueo+'</td></tr>'+
	        '</table>'].join('');
}


function cuentasClienteRiesgo(cdCliente) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CuentasClienteRiesgoService/cuentasClienteRiesgo/"+ cdCliente,
		type : 'GET',
		async: true
	});
} 

function modificarTipologiaPersonaReportada(){
	$('#MSCAVE01011_2').modal("hide");	
	personaReportada.cdTipologia = $('#cdTipologia').val();
	sessionStorage.personaReportada = JSON.stringify(personaReportada);
	cargarContenido('MSCAVE01011','Gesti&oacute;n de Alertas > Alta Cliente de Riesgo');		
}



