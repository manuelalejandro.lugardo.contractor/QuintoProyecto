(function($, window, document) {
    $("#mensajeErrorModal").hide();
    $("#mensajeSuccessModal").hide();
    $(function() {
        $(document).on('click', '.alert-close', function() {
            $(this).parent().hide();
        })
        $("#mensajeErrorModal").hide();
        $("#mensajeSuccessModal").hide();
    });
}(window.jQuery, window, document));

$(document).on({
	ajaxStart: function(){
		$("body").addClass("loading");
	},
	ajaxStop: function(){
		//setTimeout(function(){
			$("body").removeClass("loading"); 
        //}, 500);
	}    
});

function modoSoloLectura() {
    $('.candidato_lectura').replaceWith(function(){
        var estilos = 'deshabilitado ';
        if (this.attributes.getNamedItem("type").nodeValue == "password") {
            return '<span class='+estilos+'>'+'*************'+'</span>';
        } else {
            return '<span class='+estilos+'>'+aMayuscula(this.value)+'</span>';
        }
    });
}

function aMayuscula(cadena) {
    if (cadena) {
        return cadena.toUpperCase();
    }
    return "";
}

function cerrarSucces() {
    $("#mensajeSuccessModal").hide();
}

function ocultarAlertas() {
    $("#mensajeErrorModal").hide();
    $("#mensajeSuccessModal").hide();
}

function cargarContenido(pagina, titulo) {
	$(".container").show();	
	$(".fondo").hide();
    $("#contenido").empty();
    $("#titulo_seccion").html("");
    $("#contenido").load(pagina + ".html");
    $("#titulo_seccion").html(titulo);
    ocultarAlertas();
}

function cargarMensajeError(data, muestraExito) {

    ocultarAlertas();
    var sinError = true;
    if (data == null) {
        $("#mensajeError").html("Ocurri\u00f3 un error");
        $("#mensajeErrorModal").show();
        sinError = false;
    } else if (data != null && data.mensajeError != null && data.mensajeError.length > 0) {
        $("#mensajeError").html(data.mensajeError);
        $("#mensajeErrorModal").show();
        sinError = false;
    }
    
    if (muestraExito && sinError && data != null && data.mensajeExito != null && data.mensajeExito.length > 0) {
        $("#mensajeSuccess").html(data.mensajeExito);
        $("#mensajeSuccessModal").show();
    }
    return sinError;
}

function cargarMensaje(data, muestraExito, timeSegundos,animacion) {
	$("#mensajeErrorModal").finish();
	$("#mensajeSuccessModal").finish();
    var sinError = true;
    
    if (timeSegundos == undefined || timeSegundos=='' || timeSegundos == null ) {
    	var time = 1000;
	}else{
		var time = timeSegundos * 1000;
	}
        
    if (data != null && data.cdMensaje == "0") {
        $("#mensajeError").html(data.nbMensaje);
        $("#mensajeErrorModal").show("slow", function () {
        	setTimeout(function() {
        		if(animacion != false){
        			 $(".error.msg").fadeOut(time);
        	    }
            },100);
        });
        $('html, body').animate({
            scrollTop: $("#div_encabezado_principal").offset().top
        }, 100);
        sinError = false;
    }else if (data != null && data.cdMensaje == "1") {
    	if(muestraExito){
    		$("#mensajeSuccess").html(data.nbMensaje);
            $("#mensajeSuccessModal").show("slow", function () {
            	setTimeout(function() {
            		if(animacion != false){
            			$(".success.msg").fadeOut(time);
            		}
                },100);
            });
            $('html, body').animate({
                scrollTop: $("#div_encabezado_principal").offset().top
            }, 100);
    	}
    }else{
    	 $("#mensajeError").html("Estimado usuario, ocurri\u00f3 un error en el sistema favor de intentar mas tarde.");
         $("#mensajeErrorModal").show("slow", function () {
        	setTimeout(function() {
        		if(animacion != false){
        			$(".error.msg").fadeOut(time);
        		}
            },100);
         });
         $('html, body').animate({
             scrollTop: $("#div_encabezado_principal").offset().top
         }, 100);
         sinError = false;
    }
    return sinError;
}

function activar(indice) {
    var checkBoxSeleccionado = '#checkBoxTable'+indice;
    if ($(checkBoxSeleccionado).hasClass("icon-checkBoxSeleccionado")) {
        $(checkBoxSeleccionado).removeClass('icon-checkBoxSeleccionado').addClass('icon-checkBoxNoSeleccionado');
        $(checkBoxSeleccionado).prop("title", "Se encuentra desactivado");
    } else {
        $(checkBoxSeleccionado).removeClass('icon-checkBoxNoSeleccionado').addClass('icon-checkBoxSeleccionado');
        $(checkBoxSeleccionado).prop("title", "Se encuentra activo");
    }
}

function obtenerDeSessionStorage(atributo, estricto) {
    var obtenerDato;
    if (estricto) {
        obtenerDato = sessionStorage != null && sessionStorage.getItem(atributo) != null;
    } else {
        obtenerDato = sessionStorage != null;
    }
    
    if (obtenerDato) {
        return sessionStorage.getItem(atributo);
    } else {
        sessionStorage.setItem("mensajeError", "No se encontraron atributos en la session: " + atributo);
        //var pathname = window.location.pathname; // Returns path only
        //var url      = window.location.href;     // Returns full URL
        document.location.href= sessionStorage.getItem("baseUrlFront") + '/site/login.html';
        sessionStorage.clear();
    }
}

function homeAdmon(){
    document.location.href= obtenerDeSessionStorage("baseUrlFront", true) + '/site/admon.html';
}

function logout() {
    document.location.href= obtenerDeSessionStorage("baseUrlFront", true) + '/site/logout.jsp';
}

function perfilado() {
	if(sessionStorage.cdPerfil == sessionStorage.cdPerfil1){
		sessionStorage.cdPerfil = sessionStorage.cdPerfil2;
		sessionStorage.nbPerfil = sessionStorage.nbPerfil2;
	}else if(sessionStorage.cdPerfil == sessionStorage.cdPerfil2){
		sessionStorage.cdPerfil = sessionStorage.cdPerfil1;
		sessionStorage.nbPerfil = sessionStorage.nbPerfil1;
	}
    document.location.href= obtenerDeSessionStorage("baseUrlFront", true) + '/site/perfilado.jsp';
}

function irAdmon() {
    document.location.href= obtenerDeSessionStorage("baseUrlFront", true) + '/site/admon.html';
}


/**
 * Metodo encargado de llenar un select de la tabla Thcp009Detcatalogo por el id como valor y la descripcion corta como el label
 * @param idDetCatalogo Identificador del catalogo a llenar
 * @param selector El objeto, selector, donde se desea llenar.
 */
function llenarSelectPorDetCatalogo(idDetCatalogo, selector) {
	llenarSelectPorDetCatalogoConInfoExtra(idDetCatalogo, selector, null,null);
}

function llenarSelectPorDetCatalogoReporte(idDetCatalogo, selector) {
	llenarSelectPorDetCatalogoConInfoExtraReporte(idDetCatalogo, selector, null,null);
}
/**
 * Metodo encargado de llenar un select de la tabla Thcp009Detcatalogo por el id como valor y la descripcion larga como el label
 * @param idDetCatalogo Identificador del catalogo a llenar
 * @param selector El objeto, selector, donde se desea llenar.
 */
function llenarSelectPorDetCatalogoNbLarga(idDetCatalogo, selector) {
	llenarSelectPorDetCatalogoConInfoExtra(idDetCatalogo, selector, null,2);
}

/**
 * Metodo encargado de llenar un select de la tabla Thcp009Detcatalogo por el id como valor y la descripcion corta o larga como el label
 * @param idDetCatalogo Identificador del catalogo a llenar
 * @param selector El objeto, selector, donde se desea llenar.
 * @param infoExtra Lista de un objeto, con atributos cdDetCatalogo y nbCorta.
 * @param tipoDescripcion Identificador del tipo de descripción 1 = nbCorta y 2 = nbLarga 
 */
function llenarSelectPorDetCatalogoConInfoExtra(idDetCatalogo, selector, infoExtra,tipoDescripcion) {
	$select = selector;
	$.ajax({
	  url: obtenerDeSessionStorage("baseUrl", true) + "/Thcp009DetcatalogoService/ObtenerDetCatalogoPorCdCatalogo/" + idDetCatalogo,
	  dataType:'JSON',
	  async: false,
	  success:function(data){
		data = data.response;
	    //Se limpia el selector
	    $select.html('');
	    //Se itera sobre el data y se agregara la opcion del select
	    $select.append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
	    if (infoExtra != null && infoExtra != '') {
	    	$.each(infoExtra, function(key, val){
			      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbCorta + '</option>');
			})
	    }
	    if (tipoDescripcion == 2) {
	    	$.each(data, function(key, val){
			      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbLarga+ '</option>');
			})
	    }else{
	    	 $.each(data, function(key, val){
	   	      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbCorta + '</option>');
	   	    })
	    }
	  },
	  error:function(){
	    //Si existe un error, coloca un valor negativo, y agrega la leyenda
	    $select.html('<option value="-1">Ocurri\u00f3 un error</option>');
	  }
	});
}

/**
 * Metodo encargado de llenar un select de la tabla Thcp009Detcatalogo por el id como valor y la descripcion corta como el label
 * @param idDetCatalogo Identificador del catalogo a llenar
 * @param cdComplemento Identificador del filtro a realizar
 * @param selector El objeto, selector, donde se desea llenar.
 */
function llenarSelectPorDetCatalogoComplemento(idDetCatalogo, cdComplemento, selector) {
	llenarDetCatalogoPorCdCatalogoComplemento(idDetCatalogo, cdComplemento, selector, null,null);
}

/**
 * Metodo encargado de llenar un select de la tabla Thcp009Detcatalogo por el id como valor y la descripcion larga como el label
 * @param idDetCatalogo Identificador del catalogo a llenar
 * @param cdComplemento Identificador del filtro a realizar
 * @param selector El objeto, selector, donde se desea llenar.
 */
function llenarSelectPorDetCatalogoComplementoNbLarga(idDetCatalogo, cdComplemento, selector) {
	llenarDetCatalogoPorCdCatalogoComplemento(idDetCatalogo, cdComplemento, selector, null,2);
}

/**
 * Metodo encargado de llenar un select de la tabla Thcp009Detcatalogo por el id como valor y la descripcion corta o larga como el label
 * @param idDetCatalogo Identificador del catalogo a llenar
 * @param cdComplemento Identificador del filtro a realizar
 * @param selector El objeto, selector, donde se desea llenar.
 * @param infoExtra Lista de un objeto, con atributos cdDetCatalogo y nbCorta.
 * @param tipoDescripcion Identificador del tipo de descripción 1 = nbCorta y 2 = nbLarga 
 */
function llenarDetCatalogoPorCdCatalogoComplemento(idDetCatalogo, cdComplemento, selector, infoExtra, tipoDescripcion) {
	$select = selector;
	$.ajax({
	  url: obtenerDeSessionStorage("baseUrl", true) + "/Thcp009DetcatalogoService/ObtenerDetCatalogoPorCdCatalogoComplemento/" + idDetCatalogo + "/"+cdComplemento,
	  dataType:'JSON',
	  async: false,
	  success:function(data){
	    //Se limpia el selector
	    $select.html('');
	    //Se itera sobre el data y se agregara la opcion del select
	    $select.append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
	    if (infoExtra != null && infoExtra != '') {
	    	$.each(infoExtra, function(key, val){
			      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbCorta + '</option>');
			})
	    }
	    if (tipoDescripcion == 2) {
	    	$.each(data.response, function(key, val){
			      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbLarga+ '</option>');
			})
	    }else{
	    	 $.each(data.response, function(key, val){
	   	      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbCorta + '</option>');
	   	    })
	    }
	  },
	  error:function(){
	    //Si existe un error, coloca un valor negativo, y agrega la leyenda
	    $select.html('<option value="-1">Ocurri\u00f3 un error</option>');
	  }
	});
}

function llenarSelectPorCdCatalogo(cdCatalogo, selector, valorDefault) {
	$select = selector;
	$.ajax({
	  url: obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarDetCatalogoPorCdCatalogo/" + cdCatalogo,
	  dataType:'JSON',
	  async: false,
	  success:function(data){
		data = data.response;
	    //Se limpia el selector
	    $select.html('');
	    //Se itera sobre el data y se agregara la opcion del select
	    if(valorDefault){
	    	$select.append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
	    }
    	$.each(data, function(key, val){
		      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbValor+ '</option>');
		})    
	  },
	  error:function(){
	    //Si existe un error, coloca un valor negativo, y agrega la leyenda
	    $select.html('<option value="-1">Ocurri\u00f3 un error</option>');
	  }
	});
}

function llenarSelectPorCdCatalogoNbCatalogo(cdCatalogo, selector, valorDefault) {
	$select = selector;
	$.ajax({
	  url: obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarDetCatalogoPorCdCatalogo/" + cdCatalogo,
	  dataType:'JSON',
	  async: false,
	  success:function(data){
		data = data.response;
	    //Se limpia el selector
	    $select.html('');
	    //Se itera sobre el data y se agregara la opcion del select
	    if(valorDefault){
	    	$select.append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
	    }
    	$.each(data, function(key, val){
		      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbDescripcion + '</option>');
		})    
	  },
	  error:function(){
	    //Si existe un error, coloca un valor negativo, y agrega la leyenda
	    $select.html('<option value="-1">Ocurri\u00f3 un error</option>');
	  }
	});
}

function llenarSelectPorTablaParametro(nbTabla, parametro, selector, valorDefault) {
	var url = nbTabla + '/' + parametro;
	llenarSelectPorTabla(url, selector, valorDefault)
}

function llenarSelectPorTablaClaveParametro(nbTabla, parametro, selector, valorDefault) {
	var url = nbTabla + '/' + parametro;
	llenarSelectPorTablaClave(url, selector, valorDefault)
}

/**
 * Funcion que obtiene los valores de la tabla especificada.
 * Mantener la ruta /CatalogoService/consultarCatalogo + nbTabla
 * @param nbTabla Segmento: TSCA002_SEGMENTO_PLD, TipoPersona: TSCA043_TP_PERSONA, Tipologia: TSCA007_TIPOLOGIA
 *                Sector: TSCA037_SECTOR, Criterio: TSCA049_CRITERIO, Perfil: TSCA020_PERFIL, Gerencia: TSCA030_GERENCIA
 *                
 * @param selector
 * @param valorDefault
 */
function llenarSelectPorTabla(nbTabla, selector, valorDefault) {
	$select = selector;
	var url = obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogo" + nbTabla;
	$.ajax({
	  url: url,
	  dataType:'JSON',
	  async: false,
	  success:function(data){
		data = data.response;
	    //Se limpia el selector
	    $select.html('');
	    //Se itera sobre el data y se agregara la opcion del select
	    if(valorDefault){
	    	$select.append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
	    }
    	$.each(data, function(key, val){
		      $select.append('<option value="' + val.cdValor + '">' + val.nbValor+ '</option>');
		})    
	  },
	  error:function(){
	    //Si existe un error, coloca un valor negativo, y agrega la leyenda
	    $select.html('<option value="-1">Ocurri\u00f3 un error</option>');
	  }
	});
}

/**
 * Funcion que obtiene los valores de la tabla especificada colocando la clave en la descripción del select.
 * Mantener la ruta /CatalogoService/consultarCatalogo + nbTabla
 * @param nbTabla Segmento: TSCA002_SEGMENTO_PLD, TipoPersona: TSCA043_TP_PERSONA, Tipologia: TSCA007_TIPOLOGIA
 *                Sector: TSCA037_SECTOR, Criterio: TSCA049_CRITERIO, Perfil: TSCA020_PERFIL, Gerencia: TSCA030_GERENCIA
 *                
 * @param selector
 * @param valorDefault
 */
function llenarSelectPorTablaClave(nbTabla, selector, valorDefault) {
	$select = selector;
	var url = obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogo" + nbTabla;
	$.ajax({
	  url: url,
	  dataType:'JSON',
	  async: false,
	  success:function(data){
		data = data.response;
	    //Se limpia el selector
	    $select.html('');
	    //Se itera sobre el data y se agregara la opcion del select
	    if(valorDefault){
	    	$select.append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
	    }
    	$.each(data, function(key, val){
		      $select.append('<option value="' + val.cdValor + '">' + val.cdValor + ' - ' + val.nbValor+ '</option>');
		})    
	  },
	  error:function(){
	    //Si existe un error, coloca un valor negativo, y agrega la leyenda
	    $select.html('<option value="-1">Ocurri\u00f3 un error</option>');
	  }
	});
}

function crearFecha(fecha) {
	if (fecha != null) {
		var fechaArreglo = fecha.split("/");
		console.log(fechaArreglo);
		var dia = fechaArreglo[0];
		console.log(dia);
		var mes = fechaArreglo[1] - 1;
		console.log(mes);
		var anio = fechaArreglo[2];
		console.log(anio);
		return new Date(anio, mes, dia, 0, 0, 0, 0);
	} else {
		return null;
	}
}

function resetSessionStorage(){
	var sesionSICA = new Object();
	
	sesionSICA.baseUrl 				= sessionStorage.baseUrl;
	sesionSICA.baseUrlFront 		= sessionStorage.baseUrlFront;
	sesionSICA.cdModulos 			= sessionStorage.cdModulos;
	sesionSICA.cdUsuario 			= sessionStorage.cdUsuario;
	sesionSICA.cdPerfil 			= sessionStorage.cdPerfil;
	sesionSICA.nbPerfil 			= sessionStorage.nbPerfil;
	sesionSICA.cdPerfil1 			= sessionStorage.cdPerfil1;
	sesionSICA.nbPerfil1 			= sessionStorage.nbPerfil1;
	sesionSICA.cdPerfil2 			= sessionStorage.cdPerfil2;
	sesionSICA.nbPerfil2 			= sessionStorage.nbPerfil2;
	sesionSICA.cdUsuarioTemporal 	= sessionStorage.cdUsuarioTemporal;
	sesionSICA.nbCveRed 			= sessionStorage.nbCveRed;
	sesionSICA.nbMaterno 			= sessionStorage.nbMaterno;
	sesionSICA.nbPaterno 			= sessionStorage.nbPaterno;
	sesionSICA.nbPersona 			= sessionStorage.nbPersona;
	sesionSICA.cdSupervisor 		= sessionStorage.cdSupervisor;
	sesionSICA.cdGerencia 			= sessionStorage.cdGerencia;
	
	sessionStorage.clear();
	
	sessionStorage.baseUrl 				= sesionSICA.baseUrl;
	sessionStorage.baseUrlFront 		= sesionSICA.baseUrlFront;
	sessionStorage.cdModulos 			= sesionSICA.cdModulos;
	sessionStorage.cdUsuario 			= sesionSICA.cdUsuario;
	sessionStorage.cdPerfil 			= sesionSICA.cdPerfil;
	sessionStorage.nbPerfil 			= sesionSICA.nbPerfil;
	sessionStorage.cdPerfil1 			= sesionSICA.cdPerfil1;
	sessionStorage.nbPerfil1 			= sesionSICA.nbPerfil1;
	sessionStorage.cdPerfil2 			= sesionSICA.cdPerfil2;
	sessionStorage.nbPerfil2 			= sesionSICA.nbPerfil2;
	sessionStorage.cdUsuarioTemporal 	= sesionSICA.cdUsuarioTemporal;
	sessionStorage.nbCveRed 			= sesionSICA.nbCveRed;
	sessionStorage.nbMaterno 			= sesionSICA.nbMaterno;
	sessionStorage.nbPaterno 			= sesionSICA.nbPaterno;
	sessionStorage.nbPersona 			= sesionSICA.nbPersona;
	sessionStorage.cdSupervisor 		= sesionSICA.cdSupervisor;
	sessionStorage.cdGerencia 			= sesionSICA.cdGerencia;
}

function llenarSelectPorDetCatalogoConInfoExtraReporte(idDetCatalogo, selector, infoExtra,tipoDescripcion) {
	$select = selector;
	$.ajax({
	  url: obtenerDeSessionStorage("baseUrl", true) + "/Thcp009DetcatalogoService/ObtenerDetCatalogoPorCdCatalogo/" + idDetCatalogo,
	  dataType:'JSON',
	  async: false,
	  success:function(data){
		data = data.response;
	    //Se limpia el selector
	    $select.html('');
	    //Se itera sobre el data y se agregara la opcion del select
	    $select.append('<option value="' + '-2' + '"selected disabled hidden>' + '---Seleccione---' + '</option>');
	    $select.append('<option value="' + '-1' + '">' + 'TODOS' + '</option>');
	      if (infoExtra != null && infoExtra != '') {
	    	$.each(infoExtra, function(key, val){
			      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbCorta + '</option>');
			})
	    }
	    if (tipoDescripcion == 2) {
	    	$.each(data, function(key, val){
			      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbLarga+ '</option>');
			})
	    }else{
	    	 $.each(data, function(key, val){
	   	      $select.append('<option value="' + val.cdDetCatalogo + '">' + val.nbCorta + '</option>');
	   	    })
	    }
	  },
	  error:function(){
	    //Si existe un error, coloca un valor negativo, y agrega la leyenda
	    $select.html('<option value="-2">Ocurri\u00f3 un error</option>');
	  }
	});
}