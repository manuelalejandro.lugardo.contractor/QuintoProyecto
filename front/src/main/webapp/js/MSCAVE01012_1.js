var modificar = false;
var eliminar = false;
var alta = true;
var acepto = false;
var CD_ST_SIS_A = '11';
var CD_ST_SIS_I = '12';
var PERFIL_COORDINA = 3;

/** Funciones de Datos **/
function borrarDatos() {
	modificar = false;
	acepto = false;
	eliminar = false;
	alta = true;
	$('#cdSesion').val("");
	$('#fhLimRepAut').val("");
	$("#fhPeIn").val("");
	$("#fhPeFn").val("");
	$("#cdUsrAlta").val("");
	$("#fhAlta").val("");
	$("#cdStSistema").val("");
	$("#nbUsrRed").val("");
	habilCtrl('#cdSesion');
	resetVal();
}

function fncModificar(valor){
	acepto = valor;
	validacion();
	if(!valor){
		resetVal();
	}
}

function fncEliminar(valor){
	acepto = valor;
	eliminar = valor;
	validacion();
	borrarDatos();
}

function resetVal(){
	$('#MSCAVE01012_1_FORM input').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
	$('#MSCAVE01012_1_FORM select').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
}

/** Funciones de eventos **/
var actionEvents = {
	'click .delete' : function(e, value, row, index) {
		eliminar = true;
		modificar = false;
		alta = false;
		llenarUsuario(row);
		$("#cdStSistema").val(CD_ST_SIS_I);
		$('#dialogEliminarSesion').modal('show');
	},
	'click .actualizar' : function(e, value, row, index) {
		modificar = true;
		eliminar = false;
		alta = false;
		llenarUsuario(row);
		$("#MSCAVE01012_1_FORM").valid();
		deshCtrl('#cdSesion');		
	}
};

/** Funciones de Tablas **/
function llenadoTablaEcenarios(data) {
	$('#tablaSesion').bootstrapTable(
		{
			pagination : true,
			pageSize : 5,
			data : data,
			clickToSelect : false,
			singleSelect : false,
			maintainSelected : true,
			sortable : false,
			checkboxHeader : false,
			formatShowingRows : function(pageFrom, pageTo, totalRows) {
				return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
			},
			formatRecordsPerPage : function(pageNumber) {
				return pageNumber + ' registros por p\u00e1gina';
			},
			formatLoadingMessage : function() {
				return 'Cargando, espere por favor...';
			},
			formatSearch : function() {
				return 'Buscar';
			},
			formatNoMatches : function() {
				return 'No se encontr\u00f3 informaci\u00f3n';
			}
		}
	);
}

/** Funciones de Formato **/
function formatterSesion(value, row, index) {
	if (sessionStorage.cdPerfil == PERFIL_COORDINA) {
	return [
		'<a class="delete" href="javascript:void(0)" title="Eliminar">',
		'<i class="tabla_registro_eliminar">.</i>',
		'</a>',
		'<a class="actualizar" href="javascript:void(0)" title="Actualizar">',
		'<i class="tabla_registro_editar">.</i>', '</a>'
	].join('');
	}else{
		return ['',''].join('');
	}
}

function deshCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).attr('disabled','disabled');
	$(selector).addClass('ignore');
}

function habilCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).removeAttr('disabled');
	$(selector).removeClass('ignore');
}

/** Funciones de Formulario **/
function llenarFormularioEdit(data,apartado){
	$.each(data, function(key, value) {
		var input = $('#'+key);
		var dataFormat = $('#'+apartado).find(input).attr('data-format');
		if(dataFormat != "" && dataFormat != undefined){
			if(dataFormat == 'formatterDate'){
				$('#'+apartado).find(input).datepicker('setDate', formatterDate(value));
			}else{
				value  = eval(dataFormat+'('+value+')');
				$('#'+apartado).find(input).val(value);
			}
		}else{
			$('#'+apartado).find(input).val(value);
		}
	});
}

function llenarUsuario(data) {
	llenarFormularioEdit(data, 'Sesion');
}

function validaPerfil(){
	if (sessionStorage.cdPerfil == PERFIL_COORDINA) {
		$("#abcSesiones").show();
	} else {
		$("#abcSesiones").hide();
	}
}

/** *** OBJETOS **** */
function crearObjetoSesion() {
	var sesion = new Object();
	sesion.cdSesion = $("#cdSesion").val().toUpperCase();
	sesion.fhPeIn = $("#fhPeIn").datepicker('getDate');
	sesion.fhPeFn = $("#fhPeFn").datepicker('getDate');
	sesion.fhLimRepAut = $("#fhLimRepAut").datepicker('getDate');
	if(alta){
		sesion.cdUsrAlta = sessionStorage.getItem("cdUsuario");
		sesion.fhAlta = new Date();
		sesion.cdStSistema = CD_ST_SIS_A;
		sesion.nbUsrRed = sessionStorage.getItem("nbCveRed");
	}else{
		sesion.cdUsrAlta = $("#cdUsrAlta").val().toUpperCase();
		sesion.fhAlta = $("#fhAlta").datepicker('getDate');
		sesion.cdStSistema = $("#cdStSistema").val();
		sesion.nbUsrRed = $("#nbUsrRed").val().toUpperCase();
	}
	return sesion;
}

/** Funciones de validación **/
function validarSesion(){
	var isValidateRangoFechas = function(startDate, endDate) {
		if(startDate != null && endDate != null){
			if(startDate <= endDate) {
				$("#fhPeIn").prop('style', 'border: 1px solid #86C82D;');
				$("#fhPeFn").prop('style', 'border: 1px solid #86C82D;');
	            return true;
	        }else{
	        	$("#fhPeIn").prop('style', 'border: 1px solid #C8175E;');
				$("#fhPeFn").prop('style', 'border: 1px solid #C8175E;');
	        	return false;
	        }
		}else{
			return true;
		}
    };
    
    jQuery.validator.addMethod("isValidateRangoFechas", function(value, element) {
    	return isValidateRangoFechas($("#fhPeIn").datepicker('getDate'),$("#fhPeFn").datepicker('getDate'));
    });
	
	var validador = $('#MSCAVE01012_1_FORM').validate( {
		rules: {
			cdSesion: {
				required: true
			},
			fhPeIn: {
				required: true,
				isValidateRangoFechas: true,
				date:false
			},
			fhPeFn: {
				required: true,
				isValidateRangoFechas: true,
				date: false
			},
			fhLimRepAut: {
				required: true,
				date: false
			}
		},
		errorPlacement: function (error, element) {
            $(element).prop('style', 'border: 1px solid #C8175E;');
        },
        success: function (label, element) {
            $(element).prop('style', 'border: 1px solid #86C82D;');
        }
	});
}

/** Inicialización **/
(function($, window, document) {
	$(function() {
		validaPerfil();
		$('.money').maskMoney({
			prefix : '$',
			precision : 2
		});
		obtenerSesiones().done(function(data) {
			if (cargarMensaje(data, false)) {
				llenadoTablaEcenarios(data.response);
			} else {
				llenadoTablaEcenarios([]);
			}
		});
		$('.date').datepicker({
			language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true
		}).on('hide', function (e) {
			$(this).valid();
		});
		validarSesion();
	});
}(window.jQuery, window, document));

/** Funcionalidad **/
function obtenerSesiones() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true)+ "/SesionService/consultarSesiones",
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		async : false
	});
}

/*
 * Alta nueva Sesion
 */
function abcSesion(sesion) {
	var _url = obtenerDeSessionStorage("baseUrl", true)	+ "/SesionService";
	var _type = "POST";
	if(alta){
		_url += "/altaSesion";
	}else{
		_url += "/modificaSesion";
	}
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(sesion)
	});
}

function validacion() {
	var sesionValida = $('#MSCAVE01012_1_FORM').valid(); 
	if(sesionValida){
		var sesion = crearObjetoSesion();		
		if(modificar && !acepto){
		    $('#dialogActualizaSesion').modal('show');
		}
		if ((modificar && acepto)||(eliminar && acepto) || (alta)) {
			abcSesion(sesion).done(function(data) {
				if (cargarMensaje(data, true)) {
					obtenerSesiones().done(function(data) {
						if (cargarMensaje(data, false)) {
							$('#tablaSesion').bootstrapTable('load',data.response);
						} else {
							$('#tablaSesion').bootstrapTable('load',[]);
						}
					});
					borrarDatos();
				}
			});
		}
	}
}