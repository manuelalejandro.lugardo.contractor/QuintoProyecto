var actionEvents = { 
    'click .detailBloqueo': function (e, value, row, index) {
    	llenarFormulario(row,'dialogDetalleBloqueo');
    	$('#dialogDetalleBloqueo').modal('show');
    	
    }
    
};

(function($, window, document) {
	$(function() {

		$('.date').datepicker({
		    language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true
		});
		
		$('.money').maskMoney({prefix:'$',precision:2}); 
		
		llenarSelectPorTablaClave('Tipologia', $('#cdTipologia'), true);
		
		consultarCatalogoSector().done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cdSector").html('');
			    $("#cdSector").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cdSector").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cdSector").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		llenarSelectPorCdCatalogo('10',$('#cdImporte'),false);
		
		consultarBloqueoCuentas().done(function(data) { 
			if(cargarMensaje(data,false)){
				llenadoTablaBloqueos(data.response);
			}else{
				llenadoTablaBloqueos([]);
			}
		});
		
		//llenadoControlAcces(data2.response);
	});
}(window.jQuery, window, document));

function consultarCatalogoTipologia() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoTipologia",
		type : "GET",
		async: true
	});
}

function consultarCatalogoSector() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoSector",
		type : "GET",
		async: true
	});
}

function llenadoTablaBloqueos(data) {
	$('#tablaBloqueoCuentas').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function formatterDetailUsuarios(value, row, index){
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Sistema / Origen:</b> '+row.cdSistema+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha de Bloqueo:</b> '+formatterDate(row.fhBloqueo)+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Reconsiderado (SI / NO):</b> '+row.nbReconsiderado+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Observaciones:</b> '+row.nbObservaciones+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Clave Operaci&oacute;n:</b> '+row.nbOperacion+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Bloqueo / Desbloqueo:</b> '+row.nbDesbloqueo+'</td></tr>'+
	        '</table>'].join('');
}

function formatterImageDel(value, row, index) {
	return [
            '<a class="detailBloqueo" href="javascript:void(0)" title="Bloquear Usuario">',
            '<i class="tabla_registro_eliminar">.</i>',
            '</a>'  
    ].join(''); 
}

//Busqueda de Bloqueo de Cuentas con Filtros

function getFormularioBusquedaBloqueoCuenta(){
	var formularioBusquedaBloqueoCuenta = new Object();
	
	//Captura
	if($('#cdCaso').val() != ""){
		formularioBusquedaBloqueoCuenta.cdCaso = $('#cdCaso').val();
	}
	if($('#nuFolioAlerta').val() != ""){
		formularioBusquedaBloqueoCuenta.nuFolioAlerta = $('#nuFolioAlerta').val();
	}
	if($('#cdCliente').val() != ""){
		formularioBusquedaBloqueoCuenta.cdCliente = $('#cdCliente').val();
	}
	if($('#nbCliente').val() != ""){
		formularioBusquedaBloqueoCuenta.nbCliente = $('#nbCliente').val();
	}
	if($('#nuCuenta').val() != ""){
		formularioBusquedaBloqueoCuenta.nuCuenta = $('#nuCuenta').val();
	}
	
	//Combobox
	
	if($('#cdTipologia').val() != "-1"){
		formularioBusquedaBloqueoCuenta.cdTipologia = $('#cdTipologia').val();
	}
	if($('#cdSector').val() != "-1"){
		formularioBusquedaBloqueoCuenta.cdSector = $('#cdSector').val();
	}
	
	
	// < , > , = , =<, >=
	if($('#imImporte').val() != ""){
		formularioBusquedaBloqueoCuenta.imImporte = $('#imImporte').val();
		formularioBusquedaBloqueoCuenta.cdImporte = $('#cdImporte option:selected').text();
	}
	
	//Fechas
	if($('#fhAlertaDel').val() != ""){
		formularioBusquedaBloqueoCuenta.fhAlertaDel = $('#fhAlertaDel').datepicker('getDate');
	}
	if($('#fhAlertaAl').val() != ""){
		formularioBusquedaBloqueoCuenta.fhAlertaAl = $('#fhAlertaAl').datepicker('getDate');
	}
	return formularioBusquedaBloqueoCuenta;
}

function consultarBloqueoCuentasFiltro(data) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/BloqueoCuentasService/consultarBloqueoCuentasFiltro/",
		type : 'POST',
		contentType:'application/json',
		dataType: 'JSON',
		data: JSON.stringify(data),
		async: false
	});
}

$("#btnBuscarBloqueo").click(function(){
	consultarBloqueoCuentasFiltro(getFormularioBusquedaBloqueoCuenta()).done(function(data) { 
		if(cargarMensaje(data,false)){
			$('#tablaBloqueoCuentas').bootstrapTable('load',data.response);
		}else{
			$('#tablaBloqueoCuentas').bootstrapTable('load',[]);
		}
	});	
});

//Consulta Bloqueo de Cuentas sin filtros

function consultarBloqueoCuentas() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/BloqueoCuentasService/consultarBloqueoCuentas",
		type : 'GET',
		async: true
	});
}

function borrarDatos() {
	$('#cdCaso').val("");
	$('#nuFolioAlerta').val("");
	$('#cdCliente').val("");
	$('#nbCliente').val("");
	$('#nuCuenta').val("");
	$('#cdTipologia').val("-1");
	$('#cdSector').val("-1");
	$('#imImporte').val("");
	$('#fhAlertaDel').val("");
	$('#fhAlertaAl').val("");
		
	resetVal();
}