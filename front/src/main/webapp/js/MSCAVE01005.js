var dataTotales = new Object();
(function($, window, document) {
	$(function() {
		
		$('.date').datepicker({
		    language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true,
			orientation: 'top'
		});
		$('#btnExportExcel').on('click',function (){
			generaExcelMovimientos();
		});
		
		$('.money').maskMoney({prefix:'$',precision:2,}); 
		llenadoTablaMontoMovimientos([]);
		
		consultarMontosTotales(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){
				dataTotales = data.response;				
			}
		});
		
		llenadoTablaMovimientos();
		$ ('#tablaMovimientos'). on ('load-success.bs.table' , function (data) { 
			getTotalesMovimientosPorPagina(dataTotales);
		});
 
    	
		
	});
}(window.jQuery, window, document));



function llenadoTablaMovimientos() {
	$('#tablaMovimientos').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        sidePagination: 'server',
        url: obtenerDeSessionStorage("baseUrl", true) + "/MovimientosService/consultarMovimientos/" + sessionStorage.nuFolioAlerta +"/"+ sessionStorage.cdSistema,
        queryParams: 'queryParams',
        responseHandler : "responseHandler",	
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function queryParams(params) {	
    return {
        limit: params.limit,
        offset: params.offset,
        search: params.search,
        name: params.sort,
        order: params.order
    };
}

function responseHandler(res) {
	var datos = new Object();
	datos.rows = res.response.rows;
	datos.total = res.response.total;
    return datos;
}

function llenadoTablaMontoMovimientos(data) {
	$('#tablaMontoMovimientos').bootstrapTable({
        data: data,
        maintainSelected: true,
   
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}


function formatterDetailMovimientos(value, row, index){
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Oficina:</b> '+row.nbOficina+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Descripci&oacute;n:</b> '+row.nbDescripcion+'</td></tr>'+
	        '</table>'].join('');
}

function formatterTipoMovimiento(value, row, index){
	if(value == "H"){
		return ['ABONO'].join('');
	}else if(value == "D"){
		return ['CARGO'].join('');
	}else{
		return [''].join('');
	}
}


function getTotalesMovimientosPorPagina(movimientosTotales){
	var totales = new Object();
	var $tblMontos = $( '#tablaMovimientos' );
	var $tblMontos1 = $( '#tablaMovimientos' );
	var datosPagina = $tblMontos.bootstrapTable( 'getData', {useCurrentPage:true} );
	var numMovtosPag = 0;
	var numMovtos = 0;
	var miArreglo = datosPagina;
	var numAbonosPagina = 0;
	var numAbonosTotal= 0;
	var imAbonosTotal= 0;
	var numCargosTotal= 0;
	var imCargosTotal= 0;
	var datosT = $tblMontos;
	var imAbonosPagina =0;
	var numCargosPagina = 0;	
	var imCargosPagina =0;	
	$.each(miArreglo, function (i, row) {
    	numMovtosPag += 1;
    	if( row.nbTipoMovimiento == 'D'){
    	
    		numCargosPagina += 1;
    		imCargosPagina += row.imMonto;
    	}else if( row.nbTipoMovimiento == 'H'){
    		numAbonosPagina += 1;
    		imAbonosPagina += row.imMonto;
    	}
    });	
    
    totales.numAbonosPagina = numAbonosPagina;
    totales.imAbonosPagina = imAbonosPagina;
    totales.numCargosPagina = numCargosPagina;
    totales.imCargosPagina = imCargosPagina;
    totales.numCargosTotal = movimientosTotales.numCargosTotal;
    totales.imCargosTotal = movimientosTotales.imCargosTotal;
    totales.numAbonosTotal = movimientosTotales.numAbonosTotal;
    totales.imAbonosTotal = movimientosTotales.imAbonosTotal;
    var arreglo= new Array();
    arreglo.push(totales);
    
    // Imprime Objeto totales   
    $('#tablaMontoMovimientos').bootstrapTable('load',arreglo);    
}


function consultarMontosTotales(nuFolioAlerta, cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/MovimientosService/consultarMontosTotales/" + nuFolioAlerta +"/"+ cdSistema,
		type : 'GET',
		async: true
	});
} 

function generaExcelMovimientos(){
	exportaExcel(creaTblExcel('tablaMovimientos', 6, '#0766ab', 'white', 'bold') 
			+ '<br>' + creaTblExcel('tablaMontoMovimientos', 3, '#0766ab', 'white', 'bold')
			+ creaTblExcel('tablaMontoMovimientos', 8, '#0766ab', 'white', 'bold'),'Movimientos_Montos'); 
			
}