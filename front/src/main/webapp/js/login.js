var login = {
    datos: ""
};

login.getUrlObtenerTodosPerfilesActivos = function () {
    return obtenerDeSessionStorage("baseUrl", true) + '/AdministracionServicio/ObtenerTodosPerfilesActivos';
};

login.ejecutar = function(_TIPO, _URL, _DATA) {
    $.ajax({
        type: _TIPO,
        url: _URL,
        contentType: 'application/json',
        dataType: 'json',
        async: false,
        data: _DATA,
        success: function(data) {
            login.datos = data;
        },
        error: function(data) {
            login.datos = data;
            cargaMsgError(data.responseText);
        }
    });
};

login.obtenerTodosPerfilesActivos = function () {
    login.ejecutar("GET", login.getUrlObtenerTodosPerfilesActivos(), '');
};

login.llenarPerfiles = function() {
    login.obtenerTodosPerfilesActivos();
    var options = $("#combo_perfil");
    $.each(login.datos, function () {
        options.append($("<option />").val(this.ImageFolderID).text(this.Name));
    });
};
