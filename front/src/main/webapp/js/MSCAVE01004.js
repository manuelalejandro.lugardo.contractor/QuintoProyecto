var actionEvents = {
	    'click .detailAlerta': function (e, value, row, index) {
	    	sessionStorage.nbTipologia = row.nbTipologia;
	    	sessionStorage.nbDivisa = row.nbDivisa;
	    	$('#MSCAVE01002').load('MSCAVE01002.html', function (response, status, xhr){
	            if (status == "success") {
 	                $(this).modal('show');
	            }else{
	            	data.cdMensaje = 0;
	            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	            	cargarMensaje(data,false);
	            }
	        });
	    },
	    'click .detailCaso': function (e, value, row, index) {
	    	sessionStorage.cdCaso = row.nuCaso;
	    	$('#MSCAVE01003').load('MSCAVE01003.html', function (response, status, xhr){
	            if (status == "success") {
	                $(this).modal('show');
	            }else{
	            	data.cdMensaje = 0;
	            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
	            	cargarMensaje(data,false);
	            }
	        });
	    }
	  
	};

(function($, window, document) {
	$(function() {
		//sessionStorage.cdCliente = 'B3364119';
		
		$('.date').datepicker({
		    language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true,
			orientation: 'top'
		});
		
		$('.money').maskMoney({prefix:'$',precision:2}); 
		
		//Llamado de Alertas previas de SIA y SICA
		consultaAlertasPrevias(sessionStorage.cdCliente).done(function(data) { 
			if(cargarMensaje(data,false)){
				llenadoTablaAlertasPrevias(data.response);
			}else{
				llenadoTablaAlertasPrevias([]);
			}
		});
		
		$('#btnExportarAlertas').on('click',function (){
			generaExcelAlertas();
		});
	});
}(window.jQuery, window, document));


function llenadoTablaAlertasPrevias(data) {
	$('#tablaAlertasPrevias').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function formatterDetailAlertasPrevias(value, row, index){
	
	var nbDictamenSIA=row.nbDictamenSIA==null?"":row.nbDictamenSIA;
	var fhDictamenSIA=row.fhDictamenSIA==null?"":row.fhDictamenSIA;
	var nbSistema=row.nbSistema==null?"":row.nbSistema;
	var nbCalificacion=row.nbCalificacion==null?"":row.nbCalificacion;
	var nbReportada=row.nbReportada==null?"":row.nbReportada;
	var fhReporte=row.fhReporte==null?"":row.fhReporte;
	var nbCoincidencia=row.nbCoincidencia==null?"":row.nbCoincidencia;
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail" style="color:#52BCEC;"><b>Dictamen SIA:</b> '+nbDictamenSIA+'</td></tr>'+
	        '<tr><td class="tdDetail" style="color:#52BCEC;"><b>Fecha de Dictamen SIA:</b> '+fhDictamenSIA+'</td></tr>'+
	        '<tr><td class="tdDetail" style="color:#52BCEC;"><b>Sistema:</b> '+nbSistema+'</td></tr>'+
	        '<tr><td class="tdDetail" style="color:#52BCEC;"><b>Calificaci&oacuten:</b> '+nbCalificacion+'</td></tr>'+
	        '<tr><td class="tdDetail" style="color:#52BCEC;"><b>Reportada Autoridad S/N:</b> '+nbReportada+'</td></tr>'+
	        '<tr><td class="tdDetail" style="color:#52BCEC;"><b>Fecha de Reporte:</b> '+formatterDate(fhReporte)+'</td></tr>'+
	        '</table>'+
	        '<table class="tableForm">'+
	        '<tr><td class="tdDetail" style="color:#094FA4;"><b>Coincidencia Fircosoft S/N:</b> '+nbCoincidencia+'</td></tr>'+
	        '</table>'].join('');
}

function formatterRegistroAlertasPrevias(value, row, index) {
	return [
            '<a class="detailAlerta" href="javascript:void(0)" title="Detalle de Alerta">',
            '<i class="tabla_registro_detalle">.</i>',
            '</a>',
            '<a class="detailCaso" href="javascript:void(0)" title="Detalle del Caso">',
            '<i class="tabla_registro_detalle_caso">.</i>',
            '</a>'     
    ].join(''); 
}
//Alertas previas de SIA y SICA
function consultaAlertasPrevias(cdCliente) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/AlertasService/consultarAlertasPrevias/" + cdCliente,
		type : 'GET',
		async: true
	});
}


function generaExcelAlertas(){
	exportaExcel(creaTblExcel('tablaAlertasPrevias', 7, '#0766ab', 'white', 'bold'),'Alertas_Previas'); 
}

function regresar(){
	cargarContenido('MSCAVE01007','Gesti&oacute;n de Alertas > Analisis de Casos');
}