var geocoder, map;
var TP_OPERACION_ENVIADAS = 'E';
var TP_OPERACION_RECIBIDAS = 'R';

(function($, window, document) {
	
	$(function() {
		
		/*$('.money').maskMoney({
			prefix : '',
			precision : 2
		});
		$('.date').datepicker({
			language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true
		});*/
		
		$('#tabCaso').html("C&eacute;dula de Caso "+sessionStorage.cdCaso);
		$('#nuFolioAlerta').html("Folio Alerta: "+sessionStorage.nuFolioAlerta);
		
		cdCaso = sessionStorage.cdCaso;
		nuFolioAlerta = sessionStorage.nuFolioAlerta;
		cdSistema = sessionStorage.cdSistema;
		cdCliente = sessionStorage.cdCliente;
		nuCuenta = sessionStorage.nuCuenta
		
		consultarCedulaCaso().done(function(dataFormCaso){
			if(cargarMensajeError(dataFormCaso, false)){
				llenarFormulario(dataFormCaso.response, 'caso');		
			}
		});
		$('#operacionesInternacionalesHead1').html('Transferencias Internacionales Enviadas  ' + fechaDosAniosAnteriores() );		
		$('#operacionesInternacionalesHead21').html( 'Transferencias Internacionales Recibidas  ' + fechaDosAniosAnteriores() );
		$('#anioDosDE').html( getAnio( 2 ) );
		$('#anioUnoDE').html( getAnio( 1 ) );
		$('#anioActualDE').html( getAnio( 0 ) );
		$('#anioDosRE').html( getAnio( 2 ) );
		$('#anioUnoRE').html( getAnio( 1 ) );
		$('#anioActualRE').html( getAnio( 0 ) );	
		cabeceraCuatrimestral(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var inicio, fin;
		    			if(val.inicio != undefined && val.inicio != null && val.inicio != ' '){
		    				if(val.inicio == 1){inicio = 'Enero';}
			    			if(val.inicio == 2){inicio = 'Febrero';}
			    			if(val.inicio == 3){inicio = 'Marzo';}
			    			if(val.inicio == 4){inicio = 'Abril';}
			    			if(val.inicio == 5){inicio = 'Mayo';}
			    			if(val.inicio == 6){inicio = 'Junio';}
			    			if(val.inicio == 7){inicio = 'Julio';}
			    			if(val.inicio == 8){inicio = 'Agosto';}
			    			if(val.inicio == 9){inicio = 'Septiembre';}
			    			if(val.inicio == 10){inicio = 'Octubre';}
			    			if(val.inicio == 11){inicio = 'Noviembre';}
			    			if(val.inicio == 12){inicio = 'Diciembre';}
		    			}else{
		    				inicio = '';
		    			}
		    			if(val.fin != undefined && val.fin != null && val.fin != ' '){
		    				if(val.fin == 1){fin = 'Enero';}
			    			if(val.fin == 2){fin = 'Febrero';}
			    			if(val.fin == 3){fin = 'Marzo';}
			    			if(val.fin == 4){fin = 'Abril';}
			    			if(val.fin == 5){fin = 'Mayo';}
			    			if(val.fin == 6){fin = 'Junio';}
			    			if(val.fin == 7){fin = 'Julio';}
			    			if(val.fin == 8){fin = 'Agosto';}
			    			if(val.fin == 9){fin = 'Septiembre';}
			    			if(val.fin == 10){fin = 'Octubre';}
			    			if(val.fin == 11){fin = 'Noviembre';}
			    			if(val.fin == 12){fin = 'Diciembre';}
		    			}else{
		    				fin = '';
		    			}
		    			
		    			$("#conceptoCuatrimestral").append('Cuadro por Concepto Cuatrimestral ' + fin + ' - ' + inicio );
					});
			}else{
				$("#conceptoCuatrimestral").html('Ocurri\u00f3 un error');
			}
		});
		
		cabeceraMesUno(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var mes;		    			
		    			if(val.mes != undefined && val.mes != null && val.mes != ' '){
		    				if(val.mes == 1){mes = 'Enero';}
			    			if(val.mes == 2){mes = 'Febrero';}
			    			if(val.mes == 3){mes = 'Marzo';}
			    			if(val.mes == 4){mes = 'Abril';}
			    			if(val.mes == 5){mes = 'Mayo';}
			    			if(val.mes == 6){mes = 'Junio';}
			    			if(val.mes == 7){mes = 'Julio';}
			    			if(val.mes == 8){mes = 'Agosto';}
			    			if(val.mes == 9){mes = 'Septiembre';}
			    			if(val.mes == 10){mes = 'Octubre';}
			    			if(val.mes == 11){mes = 'Noviembre';}
			    			if(val.mes == 12){mes = 'Diciembre';}
		    			}else{
		    				mes = '';
		    			}
		    			
		    			$("#mesActual").append('Cuadro por Concepto - ' + mes);
					});
			}else{
				$("#mesActual").html('Ocurri\u00f3 un error');
			}
		});
		
		cabeceraMesDos(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var mes;	    			
		    			
		    			if(val.mes2 != undefined && val.mes2 != null && val.mes2 != ' '){
		    				if(val.mes2 == 1){mes = 'Enero';}
			    			if(val.mes2 == 2){mes = 'Febrero';}
			    			if(val.mes2 == 3){mes = 'Marzo';}
			    			if(val.mes2 == 4){mes = 'Abril';}
			    			if(val.mes2 == 5){mes = 'Mayo';}
			    			if(val.mes2 == 6){mes = 'Junio';}
			    			if(val.mes2 == 7){mes = 'Julio';}
			    			if(val.mes2 == 8){mes = 'Agosto';}
			    			if(val.mes2 == 9){mes = 'Septiembre';}
			    			if(val.mes2 == 10){mes = 'Octubre';}
			    			if(val.mes2 == 11){mes = 'Noviembre';}
			    			if(val.mes2 == 12){mes = 'Diciembre';}
		    			}else{
		    				mes = '';
		    			}
		    			
		    			$("#mesDos").append('Cuadro por Concepto - ' + mes);
					});
			}else{
				$("#mesDos").html('Ocurri\u00f3 un error');
			}
		});
		
		cabeceraMesTres(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var mes;
		    			if(val.mes3 != undefined && val.mes3 != null && val.mes3 != ' '){
		    				if(val.mes3 == 1){mes = 'Enero';}
			    			if(val.mes3 == 2){mes = 'Febrero';}
			    			if(val.mes3 == 3){mes = 'Marzo';}
			    			if(val.mes3 == 4){mes = 'Abril';}
			    			if(val.mes3 == 5){mes = 'Mayo';}
			    			if(val.mes3 == 6){mes = 'Junio';}
			    			if(val.mes3 == 7){mes = 'Julio';}
			    			if(val.mes3 == 8){mes = 'Agosto';}
			    			if(val.mes3 == 9){mes = 'Septiembre';}
			    			if(val.mes3 == 10){mes = 'Octubre';}
			    			if(val.mes3 == 11){mes = 'Noviembre';}
			    			if(val.mes3 == 12){mes = 'Diciembre';}
		    			}else{
		    				mes = '';
		    			}
		    			
		    			
		    			$("#mesTres").append('Cuadro por Concepto - ' + mes);
					});
			}else{
				$("#mesTres").html('Ocurri\u00f3 un error');
			}
		});
		
		cabeceraMesCuatro(sessionStorage.nuFolioAlerta,sessionStorage.cdSistema).done(function(data) { 
			if(cargarMensaje(data,false)){    		
		    		$.each(data.response, function(key, val){
		    			var mes;
		    			if(val.mes4 != undefined && val.mes4 != null && val.mes4 != ' '){
		    				if(val.mes4 == 1){mes = 'Enero';}
			    			if(val.mes4 == 2){mes = 'Febrero';}
			    			if(val.mes4 == 3){mes = 'Marzo';}
			    			if(val.mes4 == 4){mes = 'Abril';}
			    			if(val.mes4 == 5){mes = 'Mayo';}
			    			if(val.mes4 == 6){mes = 'Junio';}
			    			if(val.mes4 == 7){mes = 'Julio';}
			    			if(val.mes4 == 8){mes = 'Agosto';}
			    			if(val.mes4 == 9){mes = 'Septiembre';}
			    			if(val.mes4 == 10){mes = 'Octubre';}
			    			if(val.mes4 == 11){mes = 'Noviembre';}
			    			if(val.mes4 == 12){mes = 'Diciembre';}
		    			}else{
		    				mes = '';
		    			}
		    			
		    			
		    			$("#mesCuatro").append('Cuadro por Concepto - '+ mes);
					});
			}else{
				$("#mesCuatro").html('Ocurri\u00f3 un error');
			}
		});
		
		
		$('#operacionesMedianasHead').html('Operaciones Medianas  ');//+fechaAnioAnterior());
		$('#operacionesRelevantesHead').html('Operaciones Relevantes  ');//+fechaAnioAnterior());
		$('#operacionesInternacionalesHead').html('Transferencias Internacionales  ');//+fechaDosAniosAnteriores());
		$('#anioDos').html(anioDos());
		$('#anioUno').html(anioUno());
		$('#anioActual').html(anioActual());
		
		llenadoTablaTransferenciasEnviadas([]);
		llenadoTablaTransferenciasRecibidas([]);
	});
	
}(window.jQuery, window, document));



$("#close").click(function() {
	
	$("#caso input").val('');
	$("#reporte input").val('');
	$("#apartado1 input").val('');
	$("#apartado1_1 input").val('');
	$("#apartado1_1_2 input").val('');
	$("#apartado1_1_3 input").val('');
	$("#tablaTDC2 input").val('');
	$("#apartado1_1_4 input").val('');
	$("#tablaTDC td").val('');
	$("#tablaCCR td").val('');
	$("#tablaIAEE td").val('');
	limpiarCuentasClienteReportado().done(function(data3){
			llenadoTablaCCRCedula(data3.response);
	});
	limpiarMecanicaOperacional().done(function(data4){
			llenadoTablaCuatrimestral(data4.response);
	});
	limpiarTablaMesUno().done(function(data4_1){
		llenadoTablaMesUno(data4_1.response);
	});
	limpiarTablaMesDos().done(function(data4_2){
		llenadoTablaMesDos(data4_2.response);
	});
	limpiarTablaMesTres().done(function(data4_3){
		llenadoTablaMesTres(data4_3.response);
	});
	limpiarTablaMesCuatro().done(function(data4_4){
		llenadoTablaMesCuatro(data4_4.response);
	});
	
	
	
});


///invocar servicio detalle CASO PARTE 1
function consultarCedulaCaso(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarCedulas/"+ cdCaso + "/" + cdCliente + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;
}
///invocar servicio detalle CASO PARTE 2
function consultarIdentificacionReporte(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarIdentificacionReportes/"+ nuFolioAlerta + "/" + cdSistema,
		type : "get",
		data: ""
	});
	return null;
}
///invocar servicio detalle CASO PARTE 3 Cliente reportado
function consultarDatosGeneralesClienteReportado(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarDatosGeneralesClienteReportado/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;
}
///invocar servicio detalle CASO PARTE 4 Cliente 
function consultarDatosGeneralesCliente(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarDatosGeneralesCliente/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;
}
///llenado conocimiento del cliente
/*function consultarConocimientoCliente(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarConocimientoCliente/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;	
}*/

function llenaKYC(cdCaso,cdCliente) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultaKYC/'+cdCaso+'/'+cdCliente; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		  datatype: 'json' 
		 
	}); 
} 
///llenado tabla tdc
/*function consultarTransaccionesCliente(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarTransaccionesCliente/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;	
}*/

function llenaAccionista(cdCaso) { 
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/CasosService/";
	_url += 'consultaAccionistasDetCaso/'+cdCaso; 
	return $.ajax({
		url : _url,
		type : "GET",	 
		  datatype: 'json' 
		 
	}); 
} 
///llenado PERSONA RELACIONADA
function consultarRelacionClienteFigura(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarRelacionClienteFigura/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA 2 EN TDC
function consularInfoAccionistasEmpresas(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarInfoAccionistasyEmpresa/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;	
}


///llenado TABLA 3 Otros Productos Cliente Reportado
function consultarOtrosProductosCR(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarOtrosProductosCR/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;	
}



///llenado TABLA 4 CuentasCliente Reportado
function consultarCuentasClienteReportadoCedula(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarCuentasClienteReportado/"+ cdSistema + "/" + nuFolioAlerta + "/" + cdCaso,
		type : "get",
		data: ""
	});
	return null;	
}
function limpiarCuentasClienteReportadoCedula(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarCuentasClienteReportado/"+ 0 + "/" + 0 + "/" + 0,
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA 5 opinion funcionario
function consultarOpinionFuncionario(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarOpinionFuncionario/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA 6 Tabla cuatrimestral
function consultarMecanicaOperacional(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarTablaCuatrimestralAnalisis/"+ cdCaso + "/" + nuFolioAlerta + "/" + cdSistema,
		type : "get",
		data: ""
	});
	return null;	
}
///limpia TABLA 6 Tabla cuatrimestral
function limpiarMecanicaOperacional(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarTablaCuatrimestralAnalisis/"+ 0 + "/" + 0 + "/" + 0,
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA 6 Tabla MES 1
function consultarTablaMesUno(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarConceptosMesUnoAnalisis/"+ cdCaso + "/" + nuFolioAlerta + "/" + cdSistema,
		type : "get",
		data: ""
	});
	return null;	
}
function limpiarTablaMesUno(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarConceptosMesUnoAnalisis/"+ 0 + "/" + 0 + "/" + 0,
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA 6 Tabla MES 2
function consultarTablaMesDos(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarConceptosMesDosAnalisis/"+ cdCaso + "/" + nuFolioAlerta + "/" + cdSistema,
		type : "get",
		data: ""
	});
	return null;	
}
function limpiarTablaMesDos(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarConceptosMesDosAnalisis/"+ 0 + "/" + 0 + "/" + 0,
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA 6 Tabla MES 3
function consultarTablaMesTres(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarConceptosMesTresAnalisis/"+ cdCaso + "/" + nuFolioAlerta + "/" + cdSistema,
		type : "get",
		data: ""
	});
	return null;	
}
function limpiarTablaMesTres(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarConceptosMesTresAnalisis/"+ 0 + "/" + 0 + "/" + 0,
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA 6 Tabla MES 4
function consultarTablaMesCuatro(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarConceptosMesCuatroAnalisis/"+ cdCaso + "/" + nuFolioAlerta + "/" + cdSistema,
		type : "get",
		data: ""
	});
	return null;	
}
function limpiarTablaMesCuatro(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarConceptosMesCuatroAnalisis/"+ 0 + "/" + 0 + "/" + 0,
		type : "get",
		data: ""
	});
	return null;	
}
/*
///llenado TABLA ORIGEN
function consultarTablaOrigenMOCR(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarOrigenMOCR/",
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA DESTINO
function consultarTablaDestinoMOCR(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarDestinoMOCR/",
		type : "get",
		data: ""
	});
	return null;	
}
*/

///llenado TABLA ORIGEN
function consultarTablaOrigenACVO(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarCuentasVinculadasOrigen/",
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA DESTINO
function consultarTablaDestinoACVD(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarCuentasVinculadasDestino/",
		type : "get",
		data: ""
	});
	return null;	
}


///llenado TABLA OPERACIONES MEDIANAS Y RELEVANTES
function consultarOperacionesMEDREL(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarOperacionesMedianasRelevantes/"+ cdCaso + "/" + cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;	
}

///llenado TABLA TRANSFERENCIAS INTERNACIONALES
/*
function consultarTransferenciasRecEnv(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarTranferenciaInternacionalRecEnv/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;	
}
*/

function consultarTransferenciasRecEnv(cdSistema, nuFolioAlerta, numCuenta, tipoOperacion, anioActual) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/CasosService/";
	_url += 'consultarDetalleTranferenciaInternacionalRecEnvDet/' + cdSistema + "/"+ nuFolioAlerta + "/" + numCuenta + "/" + tipoOperacion + "/" + anioActual;
	return $.ajax({
		url : _url,
		type : "GET",
		datatype : 'json'
	});
}

///llenado descripcion transaccion
function consultarDescripcionTransaccion(){
	var dynamicData = {};
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarDescripcionTransaccion/"+ cdSistema + "/" + nuFolioAlerta,
		type : "get",
		data: ""
	});
	return null;	
}


////LLAMADAS LAS FUNCIONES 
////CUANDO SE DA CLICK EN EL APARTADO DE LA MODAL
////EN EL DETALLE DEL CASO



$("#tabCaso").click(function() {
consultarCedulaCaso().done(function(dataFormCaso){
	if(cargarMensajeError(dataFormCaso, false)){
		llenarFormulario(dataFormCaso.response, 'caso');		
	}
});
$("#reporte input").val('');
$("#apartado1 input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');

});

$("#tabReporte").click(function() {
consultarIdentificacionReporte().done(function(dataFormReporte){
	if(cargarMensajeError(dataFormReporte, false)){
		llenarFormulario(dataFormReporte.response, 'reporte');	
	}
});
$("#caso input").val('');
$("#apartado1 input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});

$("#tabApartado1").click(function() {
consultarDatosGeneralesClienteReportado().done(function(dataForm1){
if(cargarMensajeError(dataForm1, false)){
	llenarFormulario(dataForm1.response, 'apartado1');
	}
});
$("#reporte input").val('');
$("#caso input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});


/*$("#tabApartado1_1").click(function() {
consultarDatosGeneralesCliente().done(function(dataForm1_1){
if(cargarMensajeError(dataForm1_1, false)){
	llenarFormulario(dataForm1_1.response, 'apartado1_1');		
	}
});
$("#reporte input").val('');
$("#apartado1 input").val('');
$("#caso input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});
*/





llenaKYC(sessionStorage.cdCaso,sessionStorage.cdCliente).done(function(data) {
	if(cargarMensaje(data,false)){    
			llenarFormulario(data.response,'apartado1_1');
			 
	}});


llenaAccionista(sessionStorage.cdCaso).done(function(data) {
	if(cargarMensaje(data,false)){     
	llenadoTablaIAEE(data.response);
}});


//OPINION FUNCIONARIO ----
$("#tabApartado5").click(function() {
consultarOpinionFuncionario().done(function(dataOpnionFunc){
	if(cargarMensajeError(dataOpnionFunc, false)){
		llenadoOpinionFuncionario(dataOpnionFunc.response);	
	}
});
$("#caso input").val('');
$("#reporte input").val('');
$("#apartado1 input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});





//OTROS PRODUCTOS CLIENTE REPORTADO llenado de tabla
$("#tabApartado2").click(function() {
consultarOtrosProductosCR().done(function(data2){
	if(cargarMensajeError(data2, false)){
		llenadoTablaOPCRI(data2.response.tablaCredito);
	}
});
$("#caso input").val('');
$("#reporte input").val('');
$("#apartado1 input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});
//llenadoTablaOPCRIP
$("#tabApartado2").click(function() {
consultarOtrosProductosCR().done(function(data2_1){
	if(cargarMensajeError(data2_1, false)){
		llenadoTablaOPCRIP(data2_1.response.tablaPatrimonial);
	}
});
$("#caso input").val('');
$("#reporte input").val('');
$("#apartado1 input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});


//LLENADO DESCRIPCION TRANSACCION
$("#tabApartado4").click(function() {
consultarDescripcionTransaccion().done(function(dataForm4){
	if(cargarMensajeError(dataForm4, false)){
		llenarFormulario(dataForm4.response,'apartado4');
	}
});
$("#caso input").val('');
$("#reporte input").val('');
$("#apartado1 input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});

//CUENTAS CLIENTE REPORTADO llenado de tabla
$("#tabApartado3").click(function() {
consultarCuentasClienteReportadoCedula().done(function(data3){
	if(cargarMensajeError(data3, false)){
		llenadoTablaCCR(data3.response);
	}
});
$("#caso input").val('');
$("#reporte input").val('');
$("#apartado1 input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});
///LLENADO TABLA 6 tabla cuatrimestral
$("#tabApartado6").click(function() {
consultarMecanicaOperacional().done(function(data4){
	if(cargarMensajeError(data4, false)){
		llenadoTablaCuatrimestral(data4.response);
	}
	//$( "div" ).removeClass( "th-inner" );
});
$("#caso input").val('');
$("#reporte input").val('');
$("#apartado1 input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});
/// ********** LLENADO  TABLA MES 1, 2, 3 Y 4
$("#tabApartado6").click(function() {
consultarTablaMesUno().done(function(data4_1){
	
	if(cargarMensajeError(data4_1, false)){
		llenadoTablaMesUno(data4_1.response);
		//$( "div" ).removeClass( "th-inner" );
		//$('tr').children().addClass("th-inner");
	}
});

});

$("#tabApartado6").click(function() {
consultarTablaMesDos().done(function(data4_2){
	
	if(cargarMensajeError(data4_2, false)){
		llenadoTablaMesDos(data4_2.response);
		//$( "div" ).removeClass( "th-inner" );
		//$('tr').children().addClass("th-inner");
	}
});

});

$("#tabApartado6").click(function() {
consultarTablaMesTres().done(function(data4_3){
	
	if(cargarMensajeError(data4_3, false)){
		llenadoTablaMesTres(data4_3.response);
		//$( "div" ).removeClass( "th-inner" );
		//$('tr').children().addClass("th-inner");
	}
});

});

$("#tabApartado6").click(function() {
consultarTablaMesCuatro().done(function(data4_4){
	
	if(cargarMensajeError(data4_4, false)){
		llenadoTablaMesCuatro(data4_4.response);
		//$( "div" ).removeClass( "th-inner" );
		//$('tr').children().addClass("th-inner");
	}
});

});
/*
//TABLA DE ORIGEN
$("#tabApartado6").click(function() {
consultarTablaOrigenMOCR().done(function(data5){
	
	if(cargarMensajeError(data5, false)){
		llenadoTablaOrigen(data5.response);
	}
});

});
//TABLA DE DESTINO
$("#tabApartado6").click(function() {
consultarTablaDestinoMOCR().done(function(data6){
	
	if(cargarMensajeError(data6, false)){
		llenadoTablaDestino(data6.response);
	}
});

});
*/


//TABLA DE ORIGEN An�lisis de Cuentas Vinculadas
$("#tabApartado7").click(function() {
consultarTablaOrigenACVO().done(function(data7){
	if(cargarMensajeError(data7, false)){
		llenadoTablaCuentasVinculadasOrigen(data7.response);
	}
});

});
//TABLA DE DESTINO An�lisis de Cuentas Vinculadas
$("#tabApartado7").click(function() {
consultarTablaDestinoACVD().done(function(data8){
	if(cargarMensajeError(data8, false)){
		llenadoTablaCuentasVinculadasDestino(data8.response);
	}
});

});



//TABLA DE OPERACIONES MEDIANAS
$("#tabApartado8").click(function() {
consultarOperacionesMEDREL().done(function(data10){
	if(cargarMensajeError(data10, false)){			
		llenadoTablaOperacionesMedianas(data10.response);
	}
});
$("#caso input").val('');
$("#reporte input").val('');
$("#apartado1 input").val('');
$("#apartado1_1 input").val('');
$("#apartado1_1_2 input").val('');
$("#apartado1_1_3 input").val('');
$("#tablaTDC2 input").val('');
$("#apartado1_1_4 input").val('');
$("#tablaTDC td").val('');
$("#tablaCCR td").val('');
$("#tablaIAEE td").val('');
});


//TABLA DE OPERACIONES RELEVANTES
$("#tabApartado8").click(function() {
consultarOperacionesMEDREL().done(function(data9){
	
	if(cargarMensajeError(data9, false)){			
		llenadoTablaOperacionesRelevantes(data9.response);
	}
});

});

//TABLA DE transferencias internacionales enviadas
$("#tabApartado8").click(function() {

consultarTransferenciasRecEnv(sessionStorage.cdSistema,sessionStorage.nuFolioAlerta,sessionStorage.nuCuenta,TP_OPERACION_ENVIADAS, getAnio( 0 )).done(function(data11){
	if(cargarMensajeError(data11, false)){			
		$('#tablaTransferenciasEnviadas').bootstrapTable('load', data11.response );
	}
});

});

//TABLA DE transferencias internacionales recibidas
$("#tabApartado8").click(function() {
consultarTransferenciasRecEnv(sessionStorage.cdSistema,sessionStorage.nuFolioAlerta, sessionStorage.nuCuenta,TP_OPERACION_RECIBIDAS, getAnio( 0 )).done(function(data12){
	if(cargarMensajeError(data12, false)){			
		$('#tablaTransferenciasRecibidas').bootstrapTable('load', data12.response );
	}
});

});

function llenarFormulario(data,apartado){
	$.each(data, function(key, value) {
		var input = $('#'+key);
		var dataFormat = $('#'+apartado).find(input).attr('data-format');
		if(dataFormat != "" && dataFormat != undefined){
			value  = eval(dataFormat+'('+value+')');
			$('#'+apartado).find(input).val(value);
		}else{
			$('#'+apartado).find(input).val(value);
		}
		/*$('#'+apartado).find(input).val(function( index, value ) {
			  return value + " " + this.className;
		});*/
		
	});
}

function codeAddress(address) {
    geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        'address': address
    }, function(results, status) {
    	
        if (status == google.maps.GeocoderStatus.OK) {
        	
            var myOptions = {
                zoom: 18,
                center: results[0].geometry.location
            }
            map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
            
            marker = new google.maps.Marker({
       		 	map: map,
       		 	position: results[0].geometry.location
       	 	});
            
            var latLng = marker.getPosition();
            map.setCenter(latLng);

        }
    });
}

function llenadoTablaTDC(data) {
	$('#tablaTDC').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaCCR(data) {
	$('#tablaCCRCedula').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}


//********* tabla 2 de  Consulta Sobre Conocimiento del Cliente **********//
function llenadoTablaIAEE(data) {
	$('#tablaIAEE').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}
function llenadoTablaOPCRI(data) {
	$('#tablaOPCRI').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

//tablaOPCRIP

function llenadoTablaOPCRIP(data) {
	$('#tablaOPCRIP').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}





//LLENADO TABLA 6
function llenadoTablaCuatrimestral(data) {
	$('#tablaCuatrimestral').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaMesUno(data) {
	$('#tablaMes1').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaMesDos(data) {
	$('#tablaMes2').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaMesTres(data) {
	$('#tablaMes3').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaMesCuatro(data) {
	$('#tablaMes4').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaOrigen(data) {
	$('#tablaOrigenMOCR').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaDestino(data) {
	$('#tablaDestinoMOCR').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaCuentasVinculadasOrigen(data) {
	$('#tablaCuentasVinculadasOrigen').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaCuentasVinculadasDestino(data) {
	$('#tablaCuentasVinculadasDestino').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaOperacionesRelevantes(data) {
	$('#tablaOperacionesRelevantes').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaOperacionesMedianas(data) {
	$('#tablaOperacionesMedianas').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaTransferenciasEnviadas(data) {
	$('#tablaTransferenciasEnviadas').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoTablaTransferenciasRecibidas(data) {
	$('#tablaTransferenciasRecibidas').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci&oacute;n';
        }      
    });
}

function llenadoOpinionFuncionario(data) {
	$('#txtOpinionFunc').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr\u00f3 informaci\u00f3n';
        }      
    });
}
function formatterDetailCCRCedula(value, row, index){
	
	var nuCtePartCCRCedula=row.nuCtePartCCRCedula==null?"":row.nuCtePartCCRCedula;
	var nbParticipesCCRCedula=row.nbParticipesCCRCedula==null?"":row.nbParticipesCCRCedula;
	var imSaldoCCRCedula=row.imSaldoCCRCedula==null?"":row.imSaldoCCRCedula;
	var imSaldoPromCCRCedula=row.imSaldoPromCCRCedula==null?"":row.imSaldoPromCCRCedula;
	
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>No. Cte. Part.:</b> '+nuCtePartCCRCedula+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Participes:</b> '+nbParticipesCCRCedula+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Saldo:</b> '+formatterMoney(imSaldoCCRCedula)+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Saldo Prom.:</b> '+formatterMoney(imSaldoPromCCRCedula)+'</td></tr>'+
	        '</table>'].join('');
}

function formatterDetailOPCRI(value, row, index){
	
	return ['<table class="tableForm">'+
	      //  '<tr><td class="tdDetail"><b>No. Cte. Part.:</b> '+row.nuCtePartOPCR+'</td></tr>'+
	      //  '<tr><td class="tdDetail"><b>Participes:</b> '+row.nbParticipesOPCR+'</td></tr>'+
	      //  '<tr><td class="tdDetail"><b>Saldo:</b> '+formatterMoney(row.imProductoOPCR)+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>L&iacute;mite de Cr&eacute;dito.:</b> '+formatterMoney(row.imProPromOPCR)+'</td></tr>'+
	        '</table>'].join('');
}

function formatterDetailOPCRP(value, row, index){
	
	return ['<table class="tableForm">'+
	      //  '<tr><td class="tdDetail"><b>No. Cte. Part.:</b> '+row.nuCtePartOPCR+'</td></tr>'+
	      //  '<tr><td class="tdDetail"><b>Participes:</b> '+row.nbParticipesOPCR+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Saldo:</b> '+formatterMoney(row.imProductoOPCR)+'</td></tr>'+
	        //'<tr><td class="tdDetail"><b>Saldo Prom.:</b> '+formatterMoney(row.imProPromOPCRP)+'</td></tr>'+
	        '</table>'].join('');
}




function formatterFooterTotalMecanica(data, field) {
	
    return 'Total';
}

function formatterFooterTotal(data, field) {
	
    return 'Total General:';
}

function formatterFooterVacio(data) {
    return '';
}

function formatterFooterTotalMontoOperaciones(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    var formatTotalMontoOperaciones = formatterMoney(total);
    var lblTotalMontoOperaciones = "";
    if(formatTotalMontoOperaciones.length >= 10){
    	lblTotalMontoOperaciones = '<span style="width:62px;overflow:hidden;display:inline-block;vertical-align:middle;">'+formatTotalMontoOperaciones+'</span><img src="../img/componentes/flecha3.png" title="'+formatTotalMontoOperaciones+'" style="margin-top:1px;margin-top: 3px;"/>';
    }else{
    	lblTotalMontoOperaciones = formatTotalMontoOperaciones
    }
    return lblTotalMontoOperaciones;
}

function formatterFooterPromedioOperaciones(data, field) {
    var total = 0;
    var contador = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
        contador++;
    });
    
    var formatTotalMontoOperaciones = formatterMoney(total);
    var lblTotalMontoOperaciones = "";
    if(formatTotalMontoOperaciones.length >= 10){
    	lblTotalMontoOperaciones = '<span style="width:62px;overflow:hidden;display:inline-block;vertical-align:middle;">'+formatTotalMontoOperaciones+'</span><img src="../img/componentes/flecha3.png" title="'+formatTotalMontoOperaciones+'" style="margin-top:1px;margin-top: 3px;"/>';
    }else{
    	lblTotalMontoOperaciones = formatTotalMontoOperaciones
    }
    
    return lblTotalMontoOperaciones;
}

function formatterFooterTotalOperaciones(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    return ''+total;
}

function formatterFooterTotalPorcentaje(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    total = Math.round(total);
    return total+'%';
}

/*function formatterMoney(value, row, index) {
	var importe = $('#money').maskMoney('mask',parseFloat(value))[0].value;
	$('#money').val("0");
	return [importe].join('');
}

function formatterMoneyDVS(value, row, index){
	
	var importe = $('#money').maskMoney('mask',value)[0].value + '/' + 'MXP';
	$('#money').val("0");
	return [importe].join('');
	
		
}*/
function formatterPorcentaje(value, row, index) {
	if(value !== null){
		value;
	}else{
		value += 0;
	}
	return [value+'%'].join('');
}
/*
function formatterDateNacimiento(value, row, index) {
	if( row != null && row != ''){
	var fechaNacimiento = new Date(value);
	var fechaActual = new Date();
	var fechaCalculada = new Date();
	var ANIOS = fechaActual.getFullYear() - fechaNacimiento.getFullYear();
	var MESES = fechaCalculada.getMonth() - fechaNacimiento.getMonth();
	
	fechaCalculada.setFullYear(ANIOS);
	fechaCalculada.setMonth(MESES);
	
	$("#date").datepicker('setDate', new Date(value));
	var formatoFecha = $("#date").val()+' '+fechaCalculada.getFullYear()+' ANIOS '+(fechaCalculada.getMonth())+' MES(ES)';
	$("#date").val("");
	return [formatoFecha].join('');
	}else
	{
	return '';
	}
}
*/


function fechaAnioAnterior() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //enero es 0
	var yyyy = today.getFullYear()-1;
	var yyyyAct = today.getFullYear();
	
	if(dd<10) {
	    dd='0'+dd
	} 
	if(mm<10) {
	    mm='0'+mm
	} 
	
	today = dd+'/'+mm+'/'+yyyyAct;
	lastYear = dd+'/'+mm+'/'+yyyy;
	fullDate=(' (Del '+lastYear+' Al '+today+')');
	
	return fullDate;
}

function fechaDosAniosAnteriores() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //enero es 0
	var yyyy = today.getFullYear()-2;
	var yyyyAct = today.getFullYear();
	
	if(dd<10) {
	    dd='0'+dd
	} 
	if(mm<10) {
	    mm='0'+mm
	} 
	
	today = dd+'/'+mm+'/'+yyyyAct;
	lastYear = dd+'/'+mm+'/'+yyyy;
	fullDate=(' (Del '+lastYear+' Al '+today+')');
	
	return fullDate;
}

function anioDos() {
	var today = new Date();
	var yyyy = today.getFullYear()-2;
	
	return yyyy;
}

function anioUno() {
	var today = new Date();
	var yyyy = today.getFullYear()-1;
	
	return yyyy;
}

function anioActual() {
	var today = new Date();
	var yyyy = today.getFullYear();
	
	return yyyy;
}

function mesCuatro() {
	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var f = new Date();
	mes = meses[f.getMonth()-3];
	return mes;
}

function mesTres() {
	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var f = new Date();
	mes = meses[f.getMonth()-2];
	return mes;
}

function mesDos() {
	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var f = new Date();
	mes = meses[f.getMonth()-1];
	return mes;
}

function mesActual() {
	var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
	var f = new Date();
	mes = meses[f.getMonth()];
	return mes;
}

function cabeceraCuatrimestral(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraCuatrimestral/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}

function cabeceraMesUno(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraMesUno/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}
function cabeceraMesDos(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraMesDos/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}
function cabeceraMesTres(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraMesTres/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}
function cabeceraMesCuatro(cdAlerta,cdSistema) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/cabeceraMesCuatro/" + cdAlerta + "/" + cdSistema,
		type : "GET",
		async: false
	});
}

function getAnio( numAniosRes ){
	var fhDia = new Date();
	var anio = fhDia.getFullYear() - numAniosRes;
	return anio;
}