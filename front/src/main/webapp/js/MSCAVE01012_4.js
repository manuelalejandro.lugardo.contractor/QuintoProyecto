/** VARIABLES DE CONTROL **/
var modificar = false;
var eliminar = false;
var alta = true;
var acepto = false;
var CD_ST_SIS_A = '11';
var CD_ST_SIS_I = '12';
var CAT_PERIODICIDAD = 8;
var CAT_TIP_ASIG = 9;
var objTmp = new Object();
var GERENCIA = sessionStorage.getItem("cdGerencia");
var CD_USUARIO = $.trim(sessionStorage.getItem("cdUsuario"));
var PERFIL_CONSULTOR = 4;
var PERFIL_COORDINA = 3;
var PERFIL_SUPERVISOR = 2;
var TP_ASIG = '27';
var PERFIL = (PERFIL_SUPERVISOR == sessionStorage.cdPerfil);
var PERFILADO = (sessionStorage.cdPerfil == PERFIL_COORDINA) || (sessionStorage.cdPerfil == PERFIL_SUPERVISOR);
var objDelete;
var objUpdate;
var TP_GRD_ALTA = 1;
var TP_GRD_EDICION = 2;
var tipoGuardado;

/** Funciones de Datos **/
function borrarDatos() {
	modificar = false;
	acepto = false;
	eliminar = false;
	alta = true;
	objTmp = new Object();
	$('#cdTipologia').val('-1');
	$('#cdSegmento').val('-1');
	$('#cdTpAsignacion').val('-1');
	$('#cdUsrConsultor').val('-1');
	$('#cdPeriodicidad').val('-1');
	$('#nuTopeAlertas').val('');
	habilCtrl('select#cdUsrConsultor');
	habilCtrl('#nuTopeAlertas');
	resetVal();
	tipoGuardado = TP_GRD_ALTA;
}

function fncModificar(){
	updateValuesAsigTipologia();
	actualizarAsigTipologia(objUpdate).done(function(data) {
		if (cargarMensaje(data, true)) {
			obtenerAsignacionTipologias().done(function(data) {
				if (cargarMensaje(data, false)) {
					$('#tablaAsignacionTipologia').bootstrapTable('load',data.response);
				} else {
					$('#tablaAsignacionTipologia').bootstrapTable('load',[]);
				}
			});
			borrarDatos();
			objUpdate = null;
		}
	});
	borrarDatos();	
	/*
	acepto = valor;
	validacion();
	if(!valor){
		resetVal();
	}
	*/
}

function fncEliminar( ){
	eliminarAsigTipologia(objDelete).done(function(data) {
		if (cargarMensaje(data, true)) {
			obtenerAsignacionTipologias().done(function(data) {
				if (cargarMensaje(data, false)) {
					$('#tablaAsignacionTipologia').bootstrapTable('load',data.response);
				} else {
					$('#tablaAsignacionTipologia').bootstrapTable('load',[]);
				}
			});
			borrarDatos();
			objDelete = null;
		}
	});
	borrarDatos();
}

function resetVal(){
	$('#MSCAVE01012_4_FORM input').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
	$('#MSCAVE01012_4_FORM select').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
}

/** Funciones de eventos **/
var actionEvents = {
	'click .delete' : function(e, value, row, index) {
		eliminar = true;
		modificar = false;
		alta = false;
		llenarAsigTipo(row);
		objTmp.cdStSistema = CD_ST_SIS_I;
		valTpAsig();
		$('#dialogEliminarAsigTipologia').modal('show');
		objDelete = row;
	},
	'click .actualizar' : function(e, value, row, index) {
		modificar = true;
		eliminar = false;
		alta = false;
		llenarAsigTipo(row);
		valTpAsig();
		resetVal();
		tipoGuardado = TP_GRD_EDICION;
		objUpdate = row;
	}
};

/** Funciones de Tablas **/
function llenarAsignacion(data) {
	$('#tablaAsignacionTipologia').bootstrapTable(
		{
			pagination: true,
			pageSize: 5,
			data: data,
			clickToSelect: false,
			singleSelect: false,
			maintainSelected: true,
			sortable: true,
			checkboxHeader: false,
			formatShowingRows: function (pageFrom, pageTo, totalRows) {
				return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
			},
			formatRecordsPerPage: function (pageNumber) {
				return pageNumber + ' registros por p\u00e1gina';
			},
			formatLoadingMessage: function () {
				return 'Cargando, espere por favor...';
			},
			formatSearch: function () {
			return 'Buscar';
			},
			formatNoMatches: function () {
				return 'No se encontr&oacute; informaci&oacute;n';
			}
		}
	);
}

/** Funciones de Formato **/
function formatterImageDel(value, row, index) {
	if(PERFILADO){
		return [
	            '<a class="delete" href="javascript:void(0)" title="Eliminar">',
	            '<i class="tabla_registro_eliminar">.</i>',
	            '</a>',
	            '<a class="actualizar" href="javascript:void(0)" title="Actualizar">',
	            '<i class="tabla_registro_editar">.</i>', '</a>'
	    ].join('');
	}else{
		return ['',''].join('');
	}
}

/** Funciones de Formulario **/
function llenarObjeto(data, apartado) {
	$.each(data, function(key, value) {
		if(key == 'cdAsigTipologia'){
			objTmp.cdAsigTipologia = value;
		}else if(key == 'cdStSistema'){
			objTmp.cdStSistema = value;
		}else if(key == 'cdUsrAlta'){
			objTmp.cdUsrAlta = value;
		}else if(key == 'fhAlta'){
			objTmp.fhAlta = value;
		}else if(key == 'cdSegmento'){
			objTmp.cdSegmento = value;
		}
	});
}

function llenarAsigTipo(data) {
	llenarFormulario(data, 'AsigTipologia');
	llenarObjeto(data, 'AsigTipologia');
}

function deshCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).attr('disabled','disabled');
	$(selector).addClass('ignore');
}

function habilCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).removeAttr('disabled');
	$(selector).removeClass('ignore');
}

function validaPerfil(){
	if (PERFILADO) {
		$("#abcAsigTipologia").show();
	} else {
		$("#abcAsigTipologia").hide();
	}
}

/** *** OBJETOS **** */
function crearObjetoAsigTipologia() {
	asigTipologia = new Object();
	asigTipologia.cdTipologia = $("select#cdTipologia option").filter(":selected").val();
	asigTipologia.cdSegmento = $("select#cdSegmento option").filter(":selected").val();
	asigTipologia.cdTpAsignacion = $("select#cdTpAsignacion option").filter(":selected").val();
	if($("select#cdUsrConsultor option").filter(":selected").val() != '-1'){
		asigTipologia.cdUsrConsultor = $("select#cdUsrConsultor option").filter(":selected").val();
	}
	asigTipologia.cdPeriodicidad = $("select#cdPeriodicidad option").filter(":selected").val();
	asigTipologia.nuTopeAlertas = $("#nuTopeAlertas").val();
	if (alta) {
		asigTipologia.cdUsrAlta = sessionStorage.getItem("cdUsuario");
		asigTipologia.fhAlta = new Date();
		asigTipologia.cdStSistema = CD_ST_SIS_A;
	} else {
		asigTipologia.cdAsigTipologia = objTmp.cdAsigTipologia;
		asigTipologia.cdUsrAlta = objTmp.cdUsrAlta;
		asigTipologia.fhAlta = objTmp.fhAlta;
		asigTipologia.cdStSistema = objTmp.cdStSistema;
	}
	return asigTipologia;
}

function updateValuesAsigTipologia() {
	objUpdate.cdTipologia = $("select#cdTipologia option").filter(":selected").val();
	objUpdate.cdSegmento = $("select#cdSegmento option").filter(":selected").val();
	objUpdate.cdTpAsignacion = $("select#cdTpAsignacion option").filter(":selected").val();
	if($("select#cdUsrConsultor option").filter(":selected").val() != '-1'){
		objUpdate.cdUsrConsultor = $("select#cdUsrConsultor option").filter(":selected").val();
	}	
	objUpdate.nuTopeAlertas = $("#nuTopeAlertas").val();
	objUpdate.cdUsrAlta = objTmp.cdUsrAlta;
	objUpdate.fhAlta = objTmp.fhAlta;	
	/*	
	objUpdate.cdPeriodicidad = $("select#cdPeriodicidad option").filter(":selected").val();	
	objUpdate.cdStSistema = objTmp.cdStSistema;
	*/
}

/** Funciones de validación **/
function valTpAsig(){
	var opcSel = $("select#cdTpAsignacion option").filter(":selected").val();
	if(opcSel == TP_ASIG){
		$('#cdUsrConsultor').val('-1');
		$('#nuTopeAlertas').val('');
		deshCtrl('select#cdUsrConsultor');
		deshCtrl('#nuTopeAlertas');
	}else{
		habilCtrl('select#cdUsrConsultor');
		habilCtrl('#nuTopeAlertas');
	}
}

function validarAsigTip() {
	$.validator.addMethod('validaSelect',function (value, element, arg){
		return arg != value;
	},'error!');
	var validador = $('#MSCAVE01012_4_FORM').validate(
		{
			rules : {
				cdTipologia : {
					required : true,
					validaSelect: '-1'
				},
				cdTpAsignacion : {
					required : true,
					validaSelect: '-1'
				},
				cdUsrConsultor : {
					required : true,
					validaSelect: '-1'
				},
				cdPeriodicidad : {
					required : true,
					validaSelect: '-1'
				}
				/*
				nuTopeAlertas : {
					required : true,
					digits: true
				}
				*/
			},
			errorPlacement : function(error, element) {
				$(element).prop('style', 'border: 1px solid #C8175E;');
			},
			success : function(label, element) {
				$(element).prop('style', 'border: 1px solid #86C82D;');
			}
		}
	);
}

/** Inicialización **/
(function($, window, document) {
	$(function() {
		validaPerfil();
		tipoGuardado = TP_GRD_ALTA;
		if(PERFILADO){
			if(PERFIL){
				llenarSelectPorTablaClaveParametro('TipologiaGerencia', parseInt(GERENCIA), $('#cdTipologia'), true);
			}else{
				llenarSelectPorTablaClave('Tipologia', $('#cdTipologia'), true);
			}
			if(PERFIL){
				llenarSelectPorTablaParametro('UsuariosPerfilSupervisor', CD_USUARIO + '/' + PERFIL_CONSULTOR, $('#cdUsrConsultor'),true);
			}else{
				llenarSelectPorTablaParametro('UsuariosPerfil', PERFIL_CONSULTOR, $('#cdUsrConsultor'),true);
			}
			llenarSelectPorTabla('Segmento', $('#cdSegmento'), true);
			llenarSelectPorCdCatalogo(CAT_PERIODICIDAD, $('#cdPeriodicidad'), true);
			llenarSelectPorCdCatalogo(CAT_TIP_ASIG, $('#cdTpAsignacion'), true);
	    }
		obtenerAsignacionTipologias().done(function(data) {
			if (cargarMensaje(data, false)) {
				llenarAsignacion(data.response);
			} else {
				llenarAsignacion([]);
			}
		});
		$('.date').datepicker({
			language : 'en',
			format : "dd/mm/yyyy",
			autoclose : true
		});
		validarAsigTip();
		$('#cdTpAsignacion').change(function (){
			valTpAsig();
		});
	});
}(window.jQuery, window, document));

/** Funcionalidad **/
function obtenerAsignacionTipologias(){
	var _url = obtenerDeSessionStorage("baseUrl", true);
	if(PERFIL){
		_url += "/AsignacionTipologiaService/consultarAsigTipologiasGerencia/" + GERENCIA;
	}else{
		_url += "/AsignacionTipologiaService/consultarAsigTipologias"
	}
	 return $.ajax({
		url : _url,
		type : 'GET',
		contentType: 'application/json',
		dataType: 'json',
		async: false
	});
}

/*
 * ABC AsigTipología
 */
function abcAsigTipologia(asigTipologia, tipoOperacion ) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/AsignacionTipologiaService";
	var _type = "POST";
	if ( tipoOperacion == 'alta') {
		_url += "/altaAsigTipologia";
	} else if( tipoOperacion == 'modificacion'){
		_url += "/modificaAsigTipologia";
	}
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(asigTipologia)
	});
}

function validacion()
{
	var asigTipologiaValida = $('#MSCAVE01012_4_FORM').valid();	
	if(asigTipologiaValida){		
		if( tipoGuardado == TP_GRD_ALTA )
		{			
			var asigTipologiaObj = crearObjetoAsigTipologia();		
			insertarAsigTipologia( asigTipologiaObj ).done(function(data) {
				if (cargarMensaje(data, true)) {
					obtenerAsignacionTipologias().done(function(data) {
						if (cargarMensaje(data, false)) {
							$('#tablaAsignacionTipologia').bootstrapTable('load',data.response);
						} else {
							$('#tablaAsignacionTipologia').bootstrapTable('load',[]);
						}
					});
					borrarDatos();
					objUpdate = null;
				}
			});
			borrarDatos();				
		}
		else if( tipoGuardado == TP_GRD_EDICION )
		{
		    $('#dialogActualizaAsigTipologia').modal('show');
		}
	}
}


function actualizarTablas()
{
	obtenerAsignacionTipologias().done(function(data) {
		if (cargarMensaje(data, false)) {
			$('#tablaAsignacionTipologia').bootstrapTable('load',data.response);
		} else {
			$('#tablaAsignacionTipologia').bootstrapTable('load',[]);
		}
	});
	borrarDatos();	
}

/*
 * Eliminar Asignacion Tipología
 */
function eliminarAsigTipologia(asigTipologia) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/AsignacionTipologiaService";
	var _type = "POST";
	_url += "/eliminarAsigTipologia";
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(asigTipologia)
	});
}

/*
 * Actualizar AsigTipología
 */
function actualizarAsigTipologia(asigTipologia ) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/AsignacionTipologiaService";
	var _type = "POST";
	_url += "/modificaAsigTipologia";
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(asigTipologia)
	});
}

function insertarAsigTipologia(asigTipologia ) {
	var _url = obtenerDeSessionStorage("baseUrl", true) + "/AsignacionTipologiaService";
	var _type = "POST";
	_url += "/altaAsigTipologia";
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(asigTipologia)
	});
}