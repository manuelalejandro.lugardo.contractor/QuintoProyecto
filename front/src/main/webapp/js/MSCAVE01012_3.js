/** VARIABLES DE CONTROL **/
var modificar = false;
var eliminar = false;
var alta = true;
var acepto = false;
var CD_ST_SIS_A = '11';
var CD_ST_SIS_I = '12';
var PERFIL_COORDINA = 3;
var PERFILADO = sessionStorage.cdPerfil == PERFIL_COORDINA;
var objTmp = new Object();

/** Funciones de Datos **/
function borrarDatos() {
	modificar = false;
	acepto = false;
	eliminar = false;
	alta = true;
	objTmp = new Object();
	$('#nuAlertas').val('');
	$('#cdSesion').val('-1');
	$('#cdGerencia').val('-1');
	$('#cdDetCatalogo').val('');
	habilCtrl('#cdSesion');
	habilCtrl('#cdGerencia');
	resetVal();
}

function fncModificar(valor){
	acepto = valor;
	validacion();
	if(!valor){
		resetVal();
	}
}

function fncEliminar(valor){
	acepto = valor;
	eliminar = valor;
	validacion();
	borrarDatos();
}

function resetVal(){
	$('#MSCAVE01012_3_FORM input').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
	$('#MSCAVE01012_3_FORM select').each(function() {
		$(this).prop('style', 'border: 1px solid #cccccc;');
	});
}

/** Funciones de eventos **/
var actionEvents = {
	'click .delete' : function(e, value, row, index) {
		eliminar = true;
		modificar = false;
		alta = false;
		llenarAsigSesion(row);
		objTmp.cdDetCatalogo = CD_ST_SIS_I;
		$('#dialogEliminarAsigSesion').modal('show');
	},
	'click .actualizar' : function(e, value, row, index) {
		modificar = true;
		eliminar = false;
		alta = false;
		llenarAsigSesion(row);
		deshCtrl('#cdSesion');
		deshCtrl('#cdGerencia');
	}
};

/** Funciones de Tablas **/
function llenadoAsignacionSesion(data) {
	$('#tablaAsignacionSesion').bootstrapTable(
		{
			pagination : true,
			pageSize : 5,
			data : data,
			clickToSelect : false,
			singleSelect : false,
			maintainSelected : true,
			sortable : false,
			checkboxHeader : false,
			formatShowingRows : function(pageFrom, pageTo, totalRows) {
				return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
			},
			formatRecordsPerPage : function(pageNumber) {
				return pageNumber + ' registros por p\u00e1gina';
			},
			formatLoadingMessage : function() {
				return 'Cargando, espere por favor...';
			},
			formatSearch : function() {
				return 'Buscar';
			},
			formatNoMatches : function() {
				return 'No se encontr&oacute; informaci&oacute;n';
			}
		}
	);
}

/** Funciones de Formato **/
function convFec(fecha){
	var fec = fecha.split("/");
	return new Date(fec[2],fec[1] -1 , fec[0]);
}
/*
function formatterDate(date) {
    var d = new Date( date ),
        month = '' + (d.getUTCMonth() + 1),
        day = '' + d.getUTCDate(),
        year = d.getUTCFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return [day, month, year].join('/');
}
*/

function deshCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).attr('disabled','disabled');
	$(selector).addClass('ignore');
}

function habilCtrl(selector){
	$(selector).prop('style', 'border: 1px solid #cccccc;');
	$(selector).removeAttr('disabled');
	$(selector).removeClass('ignore');
}

function formatterImageDel(value, row, index) {
	if(PERFILADO){
	return [
	        '<a class="delete" href="javascript:void(0)" title="Eliminar">',
			'<i class="tabla_registro_eliminar">.</i>',
			'</a>',
			'<a class="actualizar" href="javascript:void(0)" title="Actualizar">',
			'<i class="tabla_registro_editar">.</i>', '</a>'
	].join('');
	}
	return ['',''].join('');
}

/** Funciones de Formulario **/
function llenarObjeto(data, apartado) {
	$.each(data, function(key, value) {
		if(key=='cdDetCatalogo'){
			objTmp.cdDetCatalogo = value;
		}
	});
}

function llenarAsigSesion(data) {
	llenarFormulario(data, 'sesion');
	llenarObjeto(data, 'sesion');
}

function validaPerfil(){
	if (PERFILADO) {
		$("#abcAsignacionSesiones").show();
	} else {
		$("#abcAsignacionSesiones").hide();
	}
}

/** *** OBJETOS **** */
function crearObjetoAsigSesion() {
	asigSesion = new Object();
	asigSesion.cdSesion = $("#cdSesion").val().toUpperCase();
	asigSesion.cdGerencia = $("#cdGerencia").val();
	asigSesion.nuAlertas = $("#nuAlertas").val();
	if(alta){
    	asigSesion.cdDetCatalogo = CD_ST_SIS_A;
	}else{
		asigSesion.cdDetCatalogo = objTmp.cdDetCatalogo;
	}
	asigSesion.cdUsrAlta = sessionStorage.cdUsuario;
	return asigSesion;
}

/** Funciones de validación **/
function validarAsigSesion()
{
	$.validator.addMethod('validaSelect',function (value, element, arg){
		return arg != value;
	},'error!');
	var validador = $('#MSCAVE01012_3_FORM').validate(
		{
			rules: {
				cdSesion: {
					required: true,
					validaSelect: '-1'
				},
				cdGerencia: {
					required: true,
					validaSelect: '-1'
				},
				nuAlertas: {
					required: true,
					digits: true
				}
			},
			errorPlacement: function (error, element) {
				$(element).prop('style', 'border: 1px solid #C8175E;');
			},
			success: function (label, element) {
				$(element).prop('style', 'border: 1px solid #86C82D;');
			}
		}
	);
}

/** Inicialización **/
(function($, window, document) {
	$(function() {
		validaPerfil();
		validarAsigSesion();
		if(PERFILADO){
			llenarSelectPorTabla('Gerencia', $('#cdGerencia'), true);
			llenarSelectPorTabla('Sesion', $('#cdSesion'), true);
		}
		obtenerAsignacionSesiones().done(function(data) {
			if (cargarMensaje(data, false)) {
				llenadoAsignacionSesion(data.response);
			} else {
				llenadoAsignacionSesion([]);
			}
		});
		$('.date').datepicker({
			language : 'en',
			format : "dd/mm/yyyy",
			autoclose : true,
			orientation : 'top'
		});
		$('.money').maskMoney({
			prefix : '$',
			precision : 2
		});
	});
}(window.jQuery, window, document));

/** Funcionalidad **/
function obtenerAsignacionSesiones() {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true)
				+ "/AsignacionSesionService/consultarAsignacionSesiones",
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		async : false
	});
}

/*
 * ABC Asignación por Sesión
 */
function abcAsigSesion(asigSesion) {
	var _url = obtenerDeSessionStorage("baseUrl", true)	+ "/AsignacionSesionService";
	var _type = "POST";
	if(alta){
		_url += "/altaAsigSesion";
	}else{
		_url += "/modificaAsigSesion";
	}
	return $.ajax({
		url : _url,
		type : _type,
		contentType : 'application/json',
		dataType : 'json',
		async : false,
		data : JSON.stringify(asigSesion)
	});
}

function validacion() {
	var asigSesionValida = $('#MSCAVE01012_3_FORM').valid(); 
	if(asigSesionValida){
		var asigSesion = crearObjetoAsigSesion();
		if(modificar && !acepto){
		    $('#dialogActualizaAsigSesion').modal('show');
		}
		if ((modificar && acepto)||(eliminar && acepto) || (alta)) {
		    abcAsigSesion(asigSesion).done(function(data) {
		    	if (cargarMensaje(data, true)) {
		    		obtenerAsignacionSesiones().done(function(data) {
			    		if (cargarMensaje(data, false)) {
			    			$('#tablaAsignacionSesion').bootstrapTable('load',data.response);
			    		} else {
			    			$('#tablaAsignacionSesion').bootstrapTable('load',[]);
			    		}
			    	});
		    		borrarDatos();
		    	}
		    });
		}
	}
}