(function($, window, document) {
	$(function() {	
		
		$('.money').maskMoney({
			prefix : '$',
			precision : 2
		});
		$('.date').datepicker({
			language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true
		});
		
		
		$('#operacionesMedianasHead').html('Operaciones Medianas  '+fechaAnioAnterior());
		$('#operacionesRelevantesHead').html('Operaciones Relevantes  '+fechaAnioAnterior());
		
		
		llenaTablaOperacionMediana(sessionStorage.cdCaso, sessionStorage.cdSistema,sessionStorage.nuFolioAlerta).done(function(data) {
			if(cargarMensaje(data,false)){     
				llenadoTablaOperacionesMedianas(data.response.tablaMedianas);
				llenadoTablaOperacionesRelevantes(data.response.tablaRelevantes);
		}});
		
		llenadoTablaDetalleOperacionesMedianas();
		llenadoTablaDetalleOperacionesRelevantes();
		
	});
	
}(window.jQuery, window, document));

function llenadoTablaOperacionesRelevantes(data) {
	$('#tablaOperacionesRelevantes').bootstrapTable({
		pagination: false,
		pageSize: 3,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaOperacionesMedianas(data) {
	$('#tablaOperacionesMedianas').bootstrapTable({
		pagination: false,
		pageSize: 3,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: false,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaDetalleOperacionesMedianas() {
	$('#tablaDetalleOperacionesMedianas').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        sidePagination: 'server',
        url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarDetalleOperacionesMedianas/"+ sessionStorage.cdCaso + "/" + sessionStorage.cdSistema + "/" + sessionStorage.nuFolioAlerta ,
        queryParams: 'queryParams',
        responseHandler : "responseHandler",	
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function llenadoTablaDetalleOperacionesRelevantes() {
	$('#tablaDetalleOperacionesRelevantes').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        sidePagination: 'server',
        url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarDetalleOperacionesRelevantes/"+ sessionStorage.cdCaso + "/" + sessionStorage.cdSistema + "/" + sessionStorage.nuFolioAlerta ,
        queryParams: 'queryParams',
        responseHandler : "responseHandler",	
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function queryParams(params) {	
    return {
        limit: params.limit,
        offset: params.offset,
        search: params.search,
        name: params.sort,
        order: params.order
    };
}

function responseHandler(res) {
	var datos = new Object();
	datos.rows = res.response.rows;
	datos.total = res.response.total;
    return datos;
}


function formatterFooterTotalMecanica(data) {
    return 'Total:';
}

function formatterFooterTotalMontoOperaciones(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    return formatterMoney(total);
}

function formatterFooterPromedioOperaciones(data, field) {
    var total = 0;
    var contador = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
        contador++;
    });
    total = total / contador;
    return formatterMoney(total);
}

function formatterFooterTotalOperaciones(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    return ''+total;
}

function formatterFooterTotalPorcentaje(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    return total+'%';
}

function formatterFooterVacio(data) {
    return '';
}

function formatterFooterTotal(data) {
    return 'Total General:';
}

function abrirDetalleOperacionesMedianas(){
	$('#dialogDetalleOperacionesMedianas').modal('show');
}
function abrirDetalleOperacionesRelevantes(){
	$('#dialogDetalleOperacionesRelevantes').modal('show');
}

function formatterDetailOperaciones(value, row, index){
	
	var nbDatoOrdenante=row.nbDatoOrdenante==null?"":row.nbDatoOrdenante;
	var nbOperacionOrigen=row.nbOperacionOrigen==null?"":row.nbOperacionOrigen;
	var nbOperacionDestino=row.nbOperacionDestino==null?"":row.nbOperacionDestino;
	var nbMoneda=row.nbMoneda==null?"":row.nbMoneda;
	var imInstrumento=row.imInstrumento==null?"":row.imInstrumento;
	var imDolarizado=row.imDolarizado==null?"":row.imDolarizado;
	var nbPaisFIS=row.nbPaisFIS==null?"":row.nbPaisFIS;
	var nbRazonSocial=row.nbRazonSocial==null?"":row.nbRazonSocial;
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Dato Ordenante:</b> '+nbDatoOrdenante+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Operaci&oacute;n Origen:</b> '+nbOperacionOrigen+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Operaci&oacute;n Destino:</b> '+nbOperacionDestino+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Moneda:</b> '+nbMoneda+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Importe Instrumento:</b> '+formatterMoney(imInstrumento)+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Importe Dolarizado:</b> '+formatterMoney(imDolarizado)+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Pa&iacute;s Nacio. FIS:</b> '+nbPaisFIS+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Raz&oacute;n Social:</b> '+nbRazonSocial+'</td></tr>'+
	        '</table>'].join('');
}


function llenaTablaOperacionMediana(cdCaso, cdSistema, nuFolioAlerta) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CasosService/consultarOperacionesMedianasRelevantes/"+ cdCaso + "/" +cdSistema + "/" +nuFolioAlerta ,
		type : "GET",
		 dataType: "json",
		async: false 
	});
	 
}
 



function fechaAnioAnterior() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //enero es 0
	var yyyy = today.getFullYear()-1;
	var yyyyAct = today.getFullYear();
	
	if(dd<10) {
	    dd='0'+dd
	} 
	if(mm<10) {
	    mm='0'+mm
	} 
	
	today = dd+'/'+mm+'/'+yyyyAct;
	lastYear = dd+'/'+mm+'/'+yyyy;
	fullDate=(' (Del '+lastYear+' Al '+today+')');
	
	return fullDate;
}