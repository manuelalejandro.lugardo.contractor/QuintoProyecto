(function($, window, document) {
	$(function() {
		$('#panelEscenarios').hide();
		$('#panelMatrizCriterios').hide();		
		obtenerAlertaDetalle(sessionStorage.nuFolioAlerta, sessionStorage.cdSistema).done(function (data){
				if(cargarMensaje(data, false)){
					llenarFormulario(data.response,'cabecera');
					llenarDetalleAlertaTipologia(data.response.camposDetalleAlerta, 4,'tipologia');
					if(data.response.escenariosAlerta.length > 0){
						$('#panelEscenarios').show();
						llenadoTablaEcenarios(data.response.escenariosAlerta);
					}
				}else{
					llenarFormulario([],'cabecera');
				}
			}
		);
		consultarMatrizCriterioAlerta(sessionStorage.nuFolioAlerta, sessionStorage.cdSistema).done(function (data){
				if(cargarMensaje(data, false)){
					if(data.response.length > 0){
						$('#panelMatrizCriterios').show();
						llenadoTablaMatrizCriterios(data.response);
					}
				}else{
					llenadoTablaMatrizCriterios([]);
				}
			}
		);
		
		$('.money').maskMoney({prefix:'$',precision:2});
	});
}(window.jQuery, window, document));

function llenadoTablaEcenarios(data) {
	$('#tablaEscenarios').bootstrapTable({
		pagination: true,
		pageSize: 4,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}


function llenadoTablaMatrizCriterios(data) {
	$('#tablaMatrizCriterios').bootstrapTable({
		pagination: false,
		pageSize: 4,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}


/** Funcionalidad **/
function obtenerAlertaDetalle(nuFolioAlerta, cdSistema) {
	var _url = obtenerDeSessionStorage("baseUrl", true)+ "/AlertasService/";
	_url += 'consultarAlertaDetalle/' + nuFolioAlerta + '/' + cdSistema; 	
	return $.ajax({
		url : _url,
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		async : false
	});
}


function consultarMatrizCriterioAlerta(nuFolioAlerta, cdSistema) {
	var _url = obtenerDeSessionStorage("baseUrl", true)+ '/AlertasService/consultarMatrizCriterioAlerta/' + nuFolioAlerta + '/' + cdSistema; 	
	return $.ajax({
		url : _url,
		type : 'GET',
		contentType : 'application/json',
		dataType : 'json',
		async : false
	});
}
function formatterFooterTotalMatriz(data) {

	return '<div style="text-align: right;width:367px;"> CALIFICACI&Oacute;N </div>'
    //return 'CALIFICACI&Oacute;N:';
}

function formatterFooterTotalMatrizNumero(data, field) {
    var total = 0;
    field = this.field;
    $.each(data, function (i, row) {
        total += row[field];
    });
    if(total == 0){
    	return '0';
    }else{
    	return total;
    }
}