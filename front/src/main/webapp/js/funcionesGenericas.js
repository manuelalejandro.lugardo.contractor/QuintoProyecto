function formatterDate(value, row, index) {
	if( value != null && value != '' && value != undefined && value != 'null'){
	$("#date").datepicker('setDate', new Date(value));
	var formatoFecha = $("#date").val();
	$("#date").val("");
	return [formatoFecha].join('');
	}else{
	return '';
	}
}

function formatterMoney(value, row, index) {
	var importe = '0';
	if(value != null && value != undefined && value !='' && value != 'null' && value != '0'){
		importe = '' + value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
	}else{
		importe = '' + importe;
	}
	$('#money').val("0");
	return [importe].join('');
}

function formatterPorcentaje(value, row, index) {
	return [value+'%'].join('');
}

function formatterDateNacimiento(value, row, index) {
	if( value != null && value != '' && value != undefined && value != 'null'){
	var fechaNacimiento = new Date(value);
	var fechaActual = new Date();
	var fechaCalculada = new Date();
	var ANIOS = fechaActual.getFullYear() - fechaNacimiento.getFullYear();
	var MESES = fechaCalculada.getMonth() - fechaNacimiento.getMonth();
	
	fechaCalculada.setFullYear(ANIOS);
	fechaCalculada.setMonth(MESES);
	
	$("#date").datepicker('setDate', new Date(value));
	var formatoFecha = $("#date").val()+' '+fechaCalculada.getFullYear()+' ANIOS '+(fechaCalculada.getMonth()+1)+' MES(ES)';
	$("#date").val("");
	return [formatoFecha].join('');
	}else{
		return '';
		}
}

function llenarFormulario(data,apartado){
	$.each(data, function(key, value) {
		var input = $('#'+key);
		var dataFormat = $('#'+apartado).find(input).attr('data-format');
		if(dataFormat != "" && dataFormat != undefined){
			value  = eval(dataFormat+'('+$.trim(value)+')');
			$('#'+apartado).find(input).val($.trim(value));
		}else{
			$('#'+apartado).find(input).val($.trim(value));
		}
	});
}

/**
 * Incluye en la secciÃ³n especificada el detalle por tipologÃ­a de una alerta.
 * param campos: mapeo de campos a mapear en la tabla.
 * param intColumnas: numero de columnas en las que se deben acomodar todos los campos.
 * param selector: contenedor donde se incluira la tabla con el detalle de la alerta con base en su tipologÃ­a. 
 * */
function llenarDetalleAlertaTipologia(campos, intColumnas, selector){
	if(campos != null){
		var tabla = '<table class="tableForm" id="tablaTipologia" style="margin:10px;width:895px !important;">';
		var finTabla = '</table>';
		var fila = '<tr>';
		var finFila = '</tr>';
		var colEtiqueta = '', colCtrl = '';
		var i = 1, tope = campos.length;
		while(i <= tope){
			var incrementa = false;
			for(j = 0; j < campos.length; j++){
				var posicion = parseInt(campos[j].posicion); 
				if((posicion != 0) && (i == posicion)){
					colEtiqueta += '<td class="tdLabel"><label for="' + campos[j].idCampo + '">' 
						        + campos[j].lblCampo + ':</label></td>';
					colCtrl += '<td class="tdElement"><input type="text" name="' 
						    + campos[j].idCampo + '" id="' + campos[j].idCampo 
						    + '" class="form-control tm" ';
					if(campos[j].formatter != ''){
						colCtrl += 'data-format="' + campos[j].formatter + '" '; 
					}
					colCtrl += 'readonly>';
					if(((i % intColumnas) == 0) || (i == tope)){
						tabla += fila + colEtiqueta + finFila;
						tabla += fila + colCtrl + finFila;
						colEtiqueta = '';
						colCtrl = '';
					}
					incrementa = true;
				}else if((i == 1) && (posicion == 0)){
					tope--;
				}
			}
			if(incrementa){
				i++;
			}
		}
		tabla += finTabla;
		$('#' + selector).append(tabla);
		for(i = 0; i < campos.length; i++){
			if(parseInt(campos[i].posicion) != 0){
				var input = $('#' + campos[i].idCampo);
				var dataFormat = $('#' + selector).find(input).attr('data-format');
				if(dataFormat != "" && dataFormat != undefined){
					value  = eval(dataFormat+'('+$.trim(campos[i].nbCampo)+')');
					$('#' + selector).find(input).val($.trim(value));
				}else{
					$('#' + selector).find(input).val($.trim(campos[i].nbCampo));
				}
			}
		}
	}

}




function fechaAntiguedad(total_days){
	if( total_days != null && total_days != '' && total_days != undefined && total_days != 'null'){

    var date_current = new Date();
    var utime_target = date_current.getTime() + total_days*86400*1000;
    var date_target = new Date(utime_target);

    var diff_year  = parseInt(date_target.getUTCFullYear() - date_current.getUTCFullYear());
    var diff_month = parseInt(date_target.getUTCMonth() - date_current.getUTCMonth());
    var diff_day   = parseInt(date_target.getUTCDate() - date_current.getUTCDate());

    var days_in_month = [31, (date_target.getUTCFullYear()%4?29:28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    var date_string = "";
    while(true)
    {
        date_string = "";
        date_string += (diff_year>0?diff_year + " ANIO(S) ":"");

        if(diff_month<0){diff_year -= 1; diff_month += 12; continue;}
        date_string += (diff_month>0?diff_month + " Mes(es) ":"");

        if(diff_day<0){diff_month -= 1; diff_day += days_in_month[((11+date_target.getUTCMonth())%12)]; continue;}
        date_string += (diff_day>0?diff_day + " Dia(s) ":"");
        break;
    }
    return date_string;
	}else
		{
		return '';
		}
}

function exportaExcel(tablaHtml, idSheet){
	var dataType = 'data:application/vnd.ms-excel,';
	var tabText = '<html xmlns:x="urn:schemas-microsoft-com:office:excel">';
	tabText += '<head><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet>';
	tabText += '<x:Name>' + idSheet + '</x:Name>';
	tabText += '<x:WorksheetOptions><x:Panes></x:Panes></x:WorksheetOptions>';
	tabText += '</x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml></head><body>';
	tabText += tablaHtml;
	tabText += '</body></html>';

	window.open(dataType + escape(tabText));
}

/**
 * Exporta a excel una tabla bootstrap por medio de su ID.
 *  
 **/
function creaBootstrapTableExcel(idBootTblExcel){
	return '<table>' + $('#' + idBootTblExcel).html() + '</table>';
}

/**
 * Exporta a excel una tabla con encabezado y estilo por medio de su ID indicado, segun sea el caso:
 * param idTblExcel: ID Tabla a exportar
 * param tipoTabla: Tipo de tabla
 * 		1:
 * 					 <table id="idTabla">
 * 						<tr>
 * 							<td><label for="idCtrl">lblCtrl</label></td>
 * 						</tr>
 * 						<tr>
 * 							<td><input type="text" id="idCtrl" name='idCtrl'></td>
 * 						</tr>
 * 					 <table>
 * 
 * 		2: tabla bootstrap
 * param backColor: color del background del encabezado.
 * param fontColor: color de la fuente del encabezado.
 * param fontWeight: tipo de fuente del encabezado
 * 
 * return Tabla para incluirse en el Excel a generar.
 * */
function creaTblExcel(idTblExcel, tipoTabla, backColor, fontColor, fontWeight){
	if($('#' + idTblExcel).html() != undefined || ($('#' + idTblExcel).bootstrapTable('getData').length > 0)){
		var tblExcel = '<table border="1px">';
		var finTblExcel = '</table>';
		var filaHeader = '<thead><tr style="background-color:' 
			+ backColor + ';color:' + fontColor + ';font-weight:' + fontWeight + ';">';
		var finFilaHeader = '</tr></thead>';
		var filabody = '', finFilaBody = '', header = '', body = '', renglon = '';
		var detail = '';
		switch(tipoTabla){
		case 1:
			filabody = '<tbody><tr>';
			finFilaBody = '</tr></tbody>';
			$('#' + idTblExcel + ' input').each(function(){
				var input = $(this);
				var label = $("label[for='" + input.attr('id') + "']");
				header += '<th>' + label.text().replace(':','') + '</th>';
				body += '<td>'+ '&nbsp;' + input.val() + '</td>';
			});
			break;
		case 2:
			filabody = '<tbody>';
			finFilaBody = '</tbody>';
			var bootTable = $('#' + idTblExcel).bootstrapTable('getData');
			var ids = [];
			$('#' + idTblExcel + ' th').each(function (){
				header += '<th>' + $(this).text().trim() + '</th>';
				ids.push($(this).attr('data-field'));
			});
			
			$.each(bootTable, function (index, row){
				$.each(ids, function(index, id){
					if(row[id] != undefined){
						renglon += '<td>' + row[id]+ '</td>';		
					}else{
						renglon += '<td> </td>';
					}
				});
				body += '<tr>' + renglon + '</tr>';
				renglon='';
			});
			break;			
			
		case 3: 
			filabody = '<tbody>';
			finFilaBody = '</tbody>';
			var bootTable = $('#' + idTblExcel).bootstrapTable('getData');
			var ids = [];
			$('#' + idTblExcel + ' th').each(function (){
				var titulo = remover_acentos($(this).text());
				if(titulo == 'Total de Abonos'){
					header += '<th colspan="2">' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				} else if(titulo == 'Total de Abonos Pagina Actual'){
					header += '<th colspan="2">' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				}else if(titulo == 'Total de Cargos'){
					header += '<th colspan="2">' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				}else if(titulo == 'Total de Cargos Pagina Actual'){
					header += '<th colspan="2">' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				}
			});
			break;
			
		case 4: 
			filabody = '<tbody>';
			finFilaBody = '</tbody>';
			var bootTable = $('#' + idTblExcel).bootstrapTable('getData');
			var ids = [];
			$('#' + idTblExcel + ' th').each(function (){
				if($(this).text() != ''){
					header += '<th>' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				}
			});
			
			$.each(bootTable, function (index, row){
				$.each(ids, function(index, id){
					if(row[id] != undefined){
						if(id == "fhAsignacion"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhVenRev"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhVenLibSia"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhRepAut"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "nuCuenta"){
							var cuenta = "&nbsp;"+row[id];
							renglon += '<td>' + cuenta + '</td>';
						}else if(id == "nuCaso"){
							var caso = "&nbsp;"+row[id];
							renglon += '<td>' + caso + '</td>';
						}
						else{
							renglon += '<td>' + row[id]+ '</td>';
						}		
					}else{
						renglon += '<td> </td>';
					}
				});
				body += '<tr>' + renglon + '</tr>';
				renglon='';
			});
			break;
			
		case 5: 
			filabody = '<tbody>';
			finFilaBody = '</tbody>';
			var bootTable = $('#' + idTblExcel).bootstrapTable('getData');
			var ids = [];
			$('#' + idTblExcel + ' th').each(function (){
				if($(this).text() != ''){
					header += '<th>' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				}
			});
			
			$.each(bootTable, function (index, row){
				$.each(ids, function(index, id){
					if(row[id] != undefined){
						if(id == "fhAsignacion"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhVenRev"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhVenLibSia"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhRepAut"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhEnvio"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "nuCuenta"){
							var cuenta = "'"+row[id]+"'";
							renglon += '<td>' + cuenta + '</td>';
						}else if(id == "nuCaso"){
							var caso = "'"+row[id]+"'";
							renglon += '<td>' + caso + '</td>';
						}else if(id == "nuCuenta"){
							var cuenta = "&nbsp;"+row[id];
							renglon += '<td>' + cuenta + '</td>';
						}else if(id == "nuCaso"){
							var caso = "&nbsp;"+row[id];
							renglon += '<td>' + caso + '</td>';
						}
						else{
							renglon += '<td>' + row[id]+ '</td>';
						}		
					}else{
						renglon += '<td> </td>';
					}
				});
				body += '<tr>' + renglon + '</tr>';
				renglon='';
			});
			break;
			
		case 6: 
			filabody = '<tbody>';
			finFilaBody = '</tbody>';
			var bootTable = $('#' + idTblExcel).bootstrapTable('getData');
			var ids = [];
			$('#' + idTblExcel + ' th').each(function (){
				if($(this).text() != ''){
					header += '<th>' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				}
			});
			
			$.each(bootTable, function (index, row){
				$.each(ids, function(index, id){
					if(row[id] != undefined){
						if(id == "fhMovimiento"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "nuCuenta"){
							var cuenta = "&nbsp;"+row[id];
							renglon += '<td>' + cuenta + '</td>';
						}else if(id == "imMonto"){
							var fecha = formatterMoney(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "imEfectivo"){
							var fecha = formatterMoney(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "nbTipoMovimiento"){
							var mov;
							if(row[id] == "H"){
								mov = 'ABONO';
							}else if(row[id] == "D"){
								mov = 'CARGO';
							}else{
								mov = '';
							}
							renglon += '<td>' + mov + '</td>';
						}else{
							renglon += '<td>' + row[id]+ '</td>';
						}		
					}else{
						renglon += '<td> </td>';
					}
				});
				body += '<tr>' + renglon + '</tr>';
				renglon='';
			});
			break;
			
		case 7: 
			filabody = '<tbody>';
			finFilaBody = '</tbody>';
			var bootTable = $('#' + idTblExcel).bootstrapTable('getData');
			var ids = [];
			$('#' + idTblExcel + ' th').each(function (){
				if($(this).text() != ''){
					header += '<th>' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				}
			});
			
			$.each(bootTable, function (index, row){
				$.each(ids, function(index, id){
					if(row[id] != undefined){
						if(id == "fhAlerta"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhDictamenSIA"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhReporte"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "nuCuenta"){
							var cuenta = "&nbsp;"+row[id];
							renglon += '<td>' + cuenta + '</td>';
						}else if(id == "nuCaso"){
							var caso = "&nbsp;"+row[id];
							renglon += '<td>' + caso + '</td>';
						}
						else{
							renglon += '<td>' + row[id]+ '</td>';
						}		
					}else{
						renglon += '<td> </td>';
					}
				});
				body += '<tr>' + renglon + '</tr>';
				renglon='';
			});
			break;
			
		case 8: 
			filabody = '<tbody>';
			finFilaBody = '</tbody>';
			var bootTable = $('#' + idTblExcel).bootstrapTable('getData');
			var ids = [];
			$('#' + idTblExcel + ' th').each(function (){
				var titulo = remover_acentos($(this).text());
				if(titulo == 'Numero'){
					header += '<th>' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				} else if($(this).text() == 'Importe'){
					header += '<th>' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				}
			});
			
			$.each(bootTable, function (index, row){
				$.each(ids, function(index, id){
					if(row[id] != undefined){
						if(id == "imAbonosTotal"){
							var fecha = formatterMoney(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "imAbonosPagina"){
							var fecha = formatterMoney(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "imCargosTotal"){
							var fecha = formatterMoney(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "imCargosPagina"){
							var fecha = formatterMoney(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else{
							renglon += '<td>' + row[id]+ '</td>';
						}		
					}else{
						renglon += '<td> </td>';
					}
				});
				body += '<tr>' + renglon + '</tr>';
				renglon='';
			});
			break;
			
		case 9: 
			filabody = '<tbody>';
			finFilaBody = '</tbody>';
			var bootTable = $('#' + idTblExcel).bootstrapTable('getData');
			var ids = [];
			$('#' + idTblExcel + ' th').each(function (){
				if($(this).text() != ''){
					header += '<th>' + $(this).text().trim() + '</th>';
					ids.push($(this).attr('data-field'));
				}
			});
			
			$.each(bootTable, function (index, row){
				$.each(ids, function(index, id){
					if(row[id] != undefined){
						if(id == "fhEnvioCuest"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhRespuestaCuest"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else if(id == "fhVencimientoCuest"){
							var fecha = formatterDate(row[id]);
							renglon += '<td>' + fecha + '</td>';
						}else{
							renglon += '<td>' + row[id]+ '</td>';
						}		
					}else{
						renglon += '<td> </td>';
					}
				});
				body += '<tr>' + renglon + '</tr>';
				renglon='';
			});
			break;
		}
		tblExcel += filaHeader + header + finFilaHeader;
		tblExcel += filabody + body + finFilaBody;
		tblExcel += finTblExcel;
		return tblExcel;
	}
	return '';
}

function formatterVencimientoReporte(value, row, index) {
	var semaforo = '';
	var contador = 0;
	
	var sysdate = new Date();
	var fechaVencimientoReporteC = new Date(row.fhRepAut);
	var fechaEnvio = new Date(row.fhEnvio);
	
	var fechaLimiteReporteA = new Date(row.fhLimRepAutA);
	var fechaLimiteReporteB = new Date(row.fhLimRepAutB);
	
	var dias = fechaVencimientoReporteC.getTime() - sysdate.getTime();
	contador = Math.round(dias/(1000 * 60 * 60 * 24))+1;
	
	$("#date").datepicker('setDate', new Date(row.fhRepAut));
	var formatoFecha = $("#date").val();
	
	if(fechaVencimientoReporteC.getTime() > fechaLimiteReporteA.getTime() && fechaVencimientoReporteC.getTime() >= fechaLimiteReporteB.getTime() ){
		semaforo = '<label class="semaforoVerde">'+formatoFecha+'</label>';
	}else if((fechaVencimientoReporteC.getTime() == fechaLimiteReporteA.getTime()) || (fechaVencimientoReporteC.getTime() > fechaLimiteReporteA.getTime() && fechaVencimientoReporteC.getTime() < fechaLimiteReporteB.getTime())){
		semaforo = '<label class="semaforoAmarillo">'+formatoFecha+'</label>';
	}else if(fechaVencimientoReporteC.getTime() < fechaLimiteReporteA.getTime()){
		semaforo = '<label class="semaforoRojo">'+formatoFecha+'</label>';
	}else{
		semaforo = '<label>'+formatoFecha+'</label>';
	}
	
	$("#date").val("");
	//if(contador >= 0){
		semaforo = semaforo+'<label class="lblContador">'+contador+'</label>';
	//}
	return [semaforo].join(''); 
}

function formatterVencimientoLiberacionSIA(value, row, index) {
	var semaforo = '';
	var contador = 0;
	
	var sysdate = new Date();
	var fechaVencimientoLiberacion = new Date(row.fhVenLibSia);
	var fechaEnvio = new Date(row.fhEnvio);
	
	var fechaEnvio24 = new Date(row.fhEnvio);
	var fechaEnvio25 = new Date(row.fhEnvio);
	var fechaEnvio28 = new Date(row.fhEnvio);
	var fechaEnvio29 = new Date(row.fhEnvio);
	
	fechaEnvio24.setDate(fechaEnvio24.getDate() + 24);
	fechaEnvio25.setDate(fechaEnvio24.getDate() + 25);
	fechaEnvio28.setDate(fechaEnvio28.getDate() + 28);
	fechaEnvio29.setDate(fechaEnvio29.getDate() + 29);
	
	var dias =  fechaVencimientoLiberacion.getTime() - sysdate.getTime();
	contador = Math.round(dias/(1000 * 60 * 60 * 24))+1;
	
	$("#date").datepicker('setDate', new Date(row.fhVenLibSia));
	var formatoFecha = $("#date").val();
	
	if(fechaVencimientoLiberacion.getTime() >= fechaEnvio.getTime() && fechaVencimientoLiberacion.getTime() <= fechaEnvio24.getTime() ){
		semaforo = '<label class="semaforoVerde">'+formatoFecha+'</label>';
	}else if(fechaVencimientoLiberacion.getTime() >= fechaEnvio25.getTime() && fechaVencimientoLiberacion.getTime() <= fechaEnvio28.getTime() ){
		semaforo = '<label class="semaforoAmarillo">'+formatoFecha+'</label>';
	}else if(fechaVencimientoLiberacion.getTime() >= fechaEnvio29.getTime() ){
		semaforo = '<label class="semaforoRojo">'+formatoFecha+'</label>';
	}else{
		semaforo = '<label>'+formatoFecha+'</label>';
	}
	
	$("#date").val("");
	//if(contador >= 0){
		semaforo = semaforo+'<label class="lblContador">'+contador+'</label>';
	//}
	
	return [semaforo].join(''); 
}

function formatterVencimientoLiberacion(value, row, index) {
	var semaforo = '';
	var contador = 0;
	
	var sysdate = new Date();
	var fechaVencimientoLiberacion = new Date(row.fhVenLibSia);
	var fechaEnvio = new Date(row.fhEnvio);
	
	var fechaEnvio24 = new Date(row.fhEnvio);
	var fechaEnvio25 = new Date(row.fhEnvio);
	var fechaEnvio28 = new Date(row.fhEnvio);
	var fechaEnvio29 = new Date(row.fhEnvio);
	
	fechaEnvio24.setDate(fechaEnvio24.getDate() + 24);
	fechaEnvio25.setDate(fechaEnvio24.getDate() + 25);
	fechaEnvio28.setDate(fechaEnvio28.getDate() + 28);
	fechaEnvio29.setDate(fechaEnvio29.getDate() + 29);
	
	var dias =  fechaVencimientoLiberacion.getTime() - sysdate.getTime();
	contador = Math.round(dias/(1000 * 60 * 60 * 24))+1;
	
	$("#date").datepicker('setDate', new Date(row.fhVenLibSia));
	var formatoFecha = $("#date").val();
	
	if(fechaVencimientoLiberacion.getTime() >= fechaEnvio.getTime() && fechaVencimientoLiberacion.getTime() <= fechaEnvio24.getTime() ){
		semaforo = '<label class="semaforoVerde">'+formatoFecha+'</label>';
	}else if(fechaVencimientoLiberacion.getTime() >= fechaEnvio25.getTime() && fechaVencimientoLiberacion.getTime() <= fechaEnvio28.getTime() ){
		semaforo = '<label class="semaforoAmarillo">'+formatoFecha+'</label>';
	}else if(fechaVencimientoLiberacion.getTime() >= fechaEnvio29.getTime() ){
		semaforo = '<label class="semaforoRojo">'+formatoFecha+'</label>';
	}else{
		semaforo = '<label>'+formatoFecha+'</label>';
	}
	
	$("#date").val("");
	//if(contador >= 0){
		semaforo = semaforo+'<label class="lblContador">'+contador+'</label>';
	//}
	
	return [semaforo].join(''); 
}

function formatterVencimientoRevision(value, row, index) {
	var semaforo = '';
	var contador = 0;
	
	var sysdate = new Date();
	var fechaVencimientoRevision = new Date(row.fhVenRev);
	var fechaEnvio = new Date(row.fhEnvio);
	
	var fechaEnvio14 = new Date(row.fhEnvio);
	var fechaEnvio15 = new Date(row.fhEnvio);
	var fechaEnvio18 = new Date(row.fhEnvio);
	var fechaEnvio19 = new Date(row.fhEnvio);
	
	fechaEnvio14.setDate(fechaEnvio14.getDate() + 14);
	fechaEnvio15.setDate(fechaEnvio15.getDate() + 15);
	fechaEnvio18.setDate(fechaEnvio18.getDate() + 18);
	fechaEnvio19.setDate(fechaEnvio19.getDate() + 19);
	
	var dias = fechaVencimientoRevision.getTime() - sysdate.getTime();
	contador = Math.round(dias/(1000 * 60 * 60 * 24))+1;
	
	$("#date").datepicker('setDate', new Date(row.fhVenRev));
	var formatoFecha = $("#date").val();
	
	if(fechaVencimientoRevision.getTime() >= fechaEnvio.getTime() && fechaVencimientoRevision.getTime() <= fechaEnvio14.getTime() ){
		semaforo = '<label class="semaforoVerde">'+formatoFecha+'</label>';
	}else if(fechaVencimientoRevision.getTime() >= fechaEnvio15.getTime() && fechaVencimientoRevision.getTime() <= fechaEnvio18.getTime() ){
		semaforo = '<label class="semaforoAmarillo">'+formatoFecha+'</label>';
	}else if(fechaVencimientoRevision.getTime() >= fechaEnvio19.getTime() ){
		semaforo = '<label class="semaforoRojo">'+formatoFecha+'</label>';
	}else{
		semaforo = '<label>'+formatoFecha+'</label>';
	}
	$("#date").val("");
	//if(contador >= 0){
		semaforo = semaforo+'<label class="lblContador">'+contador+'</label>';
	//}
	return [semaforo].join(''); 
}


function formatterOcurrency(value, decimals, separators) {
    decimals = decimals >= 0 ? parseInt(decimals, 0) : 2;
    separators = separators || ['.', "'", ','];
    var number = (parseFloat(value) || 0).toFixed(decimals);
    if (number.length <= (4 + decimals))
        return number.replace('.', separators[separators.length - 1]);
    var parts = number.split(/[-.]/);
    value = parts[parts.length > 1 ? parts.length - 2 : 0];
    var result = value.substr(value.length - 3, 3) + (parts.length > 1 ?
        separators[separators.length - 1] + parts[parts.length - 1] : '');
    var start = value.length - 6;
    var idx = 0;
    while (start > -3) {
        result = (start > 0 ? value.substr(start, 3) : value.substr(0, 3 + start))
            + separators[idx] + result;
        idx = (++idx) % 2;
        start -= 3;
    }
    return (parts.length == 3 ? '-' : '') + result;
}

var formatNumber = {
		 separador: ".", // separador para los miles
		 sepDecimal: ',', // separador para los decimales
		 formatear:function (num){
		  num +='';
		  var splitStr = num.split('.');
		  var splitLeft = splitStr[0];
		  var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
		  var regx = /(\d+)(\d{3})/;
		  while (regx.test(splitLeft)) {
		  splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
		  }
		  return this.simbol + splitLeft  +splitRight;
		 },
		 new:function(num, simbol){
		  this.simbol = simbol ||'';
		  return this.formatear(num);
		 }
		}

function limite(e, contenido, caracteres)
{
    // obtenemos la tecla pulsada
    var unicode=e.keyCode? e.keyCode : e.charCode;
    
    var numeros=e.keyCode? e.keyCode : e.charCode;

    // Permitimos las siguientes teclas:
    // 8 backspace
    // 46 suprimir
    // 13 enter
    // 9 tabulador
    // 37 izquierda
    // 39 derecha
    // 38 subir
    // 40 bajar
    if( unicode==8 || unicode==46 || unicode==13 || unicode==9 || unicode==37 || unicode==39 || unicode==38 || unicode==40)
        return true;
   
    if(numeros < 48 || numeros > 57 ) 
    	return false;
    // Si ha superado el limite de caracteres devolvemos false
    if(contenido.length>=caracteres)
        return false;

    
    
    return true;
}

function limiteAL(e, contenido, caracteres)
{
    // obtenemos la tecla pulsada
    var unicode=e.keyCode? e.keyCode : e.charCode;
    

    // Permitimos las siguientes teclas:
    // 8 backspace
    // 46 suprimir
    // 13 enter
    // 9 tabulador
    // 37 izquierda
    // 39 derecha
    // 38 subir
    // 40 bajar
    if( unicode==8 || unicode==46 || unicode==13 || unicode==9 || unicode==37 || unicode==39 || unicode==38 || unicode==40 || unicode==17 || unicode==86)
        return true;
   
        // Si ha superado el limite de caracteres devolvemos false
    if(contenido.length>=caracteres)
        return false;
    
   
    
    return true;
}

function onlyNumbers( nombreCampo )
{
	console.log( "Add Event Only Numbers" );
	$("#" + nombreCampo).keydown(function(event) {
		   if(event.shiftKey)
		   {
		        event.preventDefault();
		   }

		   if (event.keyCode == 46 || event.keyCode == 8)    {
		   }
		   else {
		        if (event.keyCode < 95) {
		          if (event.keyCode < 48 || event.keyCode > 57) {
		                event.preventDefault();
		          }
		        } 
		        else {
		              if (event.keyCode < 96 || event.keyCode > 105) {
		                  event.preventDefault();
		              }
		        }
		   }
	});	

}
/* Enumera las preguntas  */
function runningFormatter(value, row, index){
		index=index+1
	return index;
}

function validaCamposFecha( vFechaDe, vFechaA )
{
	if( (vFechaDe == null && vFechaA == null) || (vFechaDe == '' && vFechaA == '' ) ){
		return true;
	}
	else
	{
		return validarRangoFechas( vFechaDe, vFechaA );
	}
}

function validarRangoFechas(startDate, endDate) {
	if( (startDate != null && endDate != null) && (startDate != '' && endDate != '') ){
		if(startDate <= endDate) {
            return true;
        }else{
        	return false;
        }
	}else{
		return true;
	}
	
}

function replaceAll( text, busca, reemplaza ){
	  while (text.toString().indexOf(busca) != -1)
	      text = text.toString().replace(busca,reemplaza);
	  return text;
}

function remover_acentos(str) {

	var map={'Ã�':'A','Ã‰':'E','Ã�':'I','Ã“':'O','Ãš':'U','Ã¡':'a','Ã©':'e','Ã­':'i','Ã³':'o','Ãº':'u'};
	var res = ''; //EstÃ¡ variable almacenarÃ¡ el valor de str, pero sin acentos y tildes
	
	for (var i=0;i<str.length;i++){
		c=str.charAt(i);
		res+=map[c]||c;
	}
	return res;
}