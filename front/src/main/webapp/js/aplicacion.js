(function($, window, document) {
    $(function() {
        cargarModulosPorPerfil();
        $("#nbCveRed").html(obtenerDeSessionStorage("nbCveRed", false));
        var nombreCompleto = 
            obtenerDeSessionStorage("nbPersona", false) + " " +
            obtenerDeSessionStorage("nbPaterno", false) + " " +
            obtenerDeSessionStorage("nbMaterno", false);
        $("#nombreCompleto").html(nombreCompleto);
        $("#nbPerfil").html(obtenerDeSessionStorage("nbPerfil", false));
    });
}(window.jQuery, window, document));

function habilitarMenu(idMenu, nombreObjeto) {
    var modulosEnLocal = obtenerDeSessionStorage("cdModulos", true).split(",");
    var arr = jQuery.grep(modulosEnLocal, function(n, i) {
        return (n == idMenu);
    });
    if (arr.length == 0) {
        nombreObjeto.hide();
    }
}

function cargarModulosPorPerfil() {
    habilitarMenu(1, $("#menu_admon_1"));
    habilitarMenu(2, $("#menu_admon_2"));
    habilitarMenu(3, $("#menu_admon_3"));
    habilitarMenu(4, $("#menu_admon_4"));
    habilitarMenu(5, $("#menu_admon_5"));
    habilitarMenu(6, $("#menu_admon_6"));
    habilitarMenu(7, $("#menu_admon_7"));
    habilitarMenu(8, $("#menu_admon_8"));
    habilitarMenu(9, $("#menu_admon_9"));
    habilitarMenu(10, $("#menu_admon_10"));
}
