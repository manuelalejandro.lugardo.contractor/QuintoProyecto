var op = '';

var actionEvents = { 
    'click .detailAlerta': function (e, value, row, index) {
    	sessionStorage.nuFolioAlerta = row.nuFolioAlerta;
    	sessionStorage.cdSistema = row.cdSistema;
    	sessionStorage.cdCaso = row.cdCaso;
    	sessionStorage.cdCliente = row.nuCliente;
    	sessionStorage.nuCuenta = row.nuCuenta;
    	$('#MSCAVE01002').load('MSCAVE01002.html', function (response, status, xhr){
            if (status == "success") {
                $(this).modal('show');
            }else{
            	data.cdMensaje = 0;
            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
            	cargarMensaje(data,false);
            }
        });
    },
    'click .detailCaso': function (e, value, row, index) {
    	sessionStorage.cdCaso = row.cdCaso;
    	sessionStorage.nuFolioAlerta = row.nuFolioAlerta;
    	sessionStorage.cdSistema = row.cdSistema;
    	sessionStorage.cdCliente = row.nuCliente;
    	sessionStorage.nuCuenta = row.nuCuenta;
    	
    	$('#MSCAVE01003').load('MSCAVE01003.html', function (response, status, xhr){
            if (status == "success") {
                $(this).modal('show');
            }else{
            	data.cdMensaje = 0;
            	data.nbMensaje = 'Estimado usuario, ocurri\u00f3 un error en el sistema por favor intente mas tarde.';
            	cargarMensaje(data,false);
            }
        });
    }
};
(function($, window, document) {
	$(function() {
		
		$('#tablaCasosPorAsignar').on('page-change.bs.table', function (e, number, size) {
			validarCasosPorAsignar();
			$("#numSelCasosPorAsignar").html('0');	
	    });
		
		$('#tablaCasosAsignados').on('page-change.bs.table', function (e, number, size) {
			validarCasosAsignados();	
	    });
		
		$('.date').datepicker({
		    language: 'en',
			format : "dd/mm/yyyy",
			autoclose: true,
			orientation: 'top'
		});
		
		$('.money').maskMoney({prefix:'$',precision:2}); 
		
		llenadoTablaCasosPorAsignar();
		
		
		consultarConsultoresSupervisor(sessionStorage.cdUsuario).done(function(data) { 
			if(cargarMensaje(data,false)){
				llenadoTablaConsultores(data.response);
			    
				$("#cmbConsultores").html('');
			    $("#cmbConsultores").append('<option value="' + '-1' + '">' + '---Seleccione---' + '</option>');
		    	$.each(data.response, function(key, val){
		    		$("#cmbConsultores").append('<option value="' + val.cdUsuario + '">' + val.nbPersona + ' ' + val.nbPaterno+ ' ' + val.nbMaterno+ '</option>');
				})    
			}else{
				llenadoTablaConsultores([]);
				$("#cmbConsultores").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		consultarCatalogoOperacionInusual('DE').done(function(data) { 
			if(cargarMensaje(data,false)){
				$("#cOperacion").html('');
		    	$.each(data.response, function(key, val){
		    		$("#cOperacion").append('<option value="' + val.cdValor + '">' +val.nbValor+ '</option>');
				});
			}else{
				$("#cOperacion").html('<option value="-1">Ocurri\u00f3 un error</option>');
			}
		});
		
		llenarSelectPorCdCatalogo('3',$('#cmbPrioridad'),false);
		
		llenadoTablaCasosAsignados('0');
	});
}(window.jQuery, window, document));


function formatterRegistroNombre(value, row, index) {
	var lblNombre = value;
	/*
	if(lblNombre != null && lblNombre != 'null'){
		if(lblNombre.length >= 16){
	    	lblNombre = '<label style="width:75px;overflow:hidden;display:inline-block;vertical-align:middle;">'+lblNombre+'</label><img src="../img/componentes/flecha3.png" title="'+lblNombre+'" style="margin-top:1px;margin-top: 3px;"/>';
	    }
	} */
    return [lblNombre].join(''); 
}

function formatterRegistroCasosPorAsignar(value, row, index) {
		return [
	            '<a class="detailAlerta" href="javascript:void(0)" title="Detalle de Alerta">',
	            '<i class="tabla_registro_detalle">.</i>',
	            '</a>'
	    ].join(''); 
}

function formatterRegistroCasosAsignados(value, row, index) {
	if(row.nbCliente != null && row.nbCliente != '' && row.nbCliente != undefined && row.nbCliente != 'null'){
		return [
	            '<a class="detailAlerta" href="javascript:void(0)" title="Detalle de Alerta">',
	            '<i class="tabla_registro_detalle">.</i>',
	            '</a>',
	            '<a class="detailCaso" href="javascript:void(0)" title="Cedula del Caso">',
	            '<i class="tabla_registro_detalle_caso">.</i>',
	            '</a>'
	    ].join(''); 
	} else{
		return [
	            '<a class="detailAlerta" href="javascript:void(0)" title="Detalle de Alerta">',
	            '<i class="tabla_registro_detalle">.</i>',
	            '</a>'
	    ].join(''); 
	}
}

function llenadoTablaCasosPorAsignar() {
	$('#tablaCasosPorAsignar').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        sidePagination: 'server',
        url: obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/consultarAlertasPendientesAsignacionGerencia/"+sessionStorage.cdGerencia,
        queryParams: 'queryParamsCasosPorAsignar',
        responseHandler : "responseHandlerCasosPorAsignar",	
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }
    });
}

function queryParamsCasosPorAsignar(params) {	
    return {
        limit: params.limit,
        offset: params.offset,
        search: params.search,
        name: params.sort,
        order: params.order
    };
}

function responseHandlerCasosPorAsignar(res) {
	var datos = new Object();
	datos.rows = res.response.rows;
	datos.total = res.response.total;
    return datos;
}

function llenadoTablaCasosAsignados(cdConsultor) {
	$('#tablaCasosAsignados').bootstrapTable({
		pagination: true,
		pageSize: 5,
		pageList: [5, 25, 50],
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        sidePagination: 'server',
        url: obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/consultarCasosAsignadosConsultor/"+cdConsultor,
        queryParams: 'queryParamsCasosAsignados',
        responseHandler : "responseHandlerCasosAsignados",
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function queryParamsCasosAsignados(params) {	
    return {
        limit: params.limit,
        offset: params.offset,
        search: params.search,
        name: params.sort,
        order: params.order
    };
}

function responseHandlerCasosAsignados(res) {
	var datos = new Object();
	datos.rows = res.response.rows;
	datos.total = res.response.total;
    return datos;
}

function llenadoTablaConsultores(data) {
	$('#tablaConsultores').bootstrapTable({
		pagination: true,
		pageSize: 5,
        data: data,
        clickToSelect: false,
        singleSelect: false,
        maintainSelected: true,
        sortable: true,
        checkboxHeader: false,
        formatShowingRows: function (pageFrom, pageTo, totalRows) {
            return 'Mostrando ' + pageFrom + ' al ' + pageTo + ' de ' + totalRows + ' registros';
        },
        formatRecordsPerPage: function (pageNumber) {
        	return pageNumber + ' registros por p\u00e1gina';
        },
        formatLoadingMessage: function () {
        	return 'Cargando, espere por favor...';
        },
        formatSearch: function () {
        	return 'Buscar';
        },
        formatNoMatches: function () {
        	return 'No se encontr&oacute; informaci&oacute;n';
        }      
    });
}

function checkFormatterCasosAsignados(value, row, index) {
	
	return ['<input type="checkbox" onChange="validarCasosAsignados();" name="checkCasoAsignado" id="CA_'+row.nuFolioAlerta+'_'+row.cdSistema+'" value="'+row.nuFolioAlerta+'_'+row.cdSistema+'"/><label for="CA_'+row.nuFolioAlerta+'_'+row.cdSistema+'"></label>'].join(''); 
}

function checkFormatterCasosPorAsignar(value, row, index) {
	
	return ['<input type="checkbox" onChange="validarCasosPorAsignar();" name="checkCasosPorAsignar" id="CP_'+row.nuFolioAlerta+'_'+row.cdSistema+'" value="'+row.nuFolioAlerta+'_'+row.cdSistema+'"/><label for="CP_'+row.nuFolioAlerta+'_'+row.cdSistema+'"></label>'].join(''); 
}

function formatterNombreConsultor(value, row, index){
	var nbConsultor = row.nbPersona+' '+row.nbPaterno+' '+row.nbMaterno;
	return [nbConsultor].join(''); 
}

function formatterRadioConsultores(value, row, index){
	return ['<input type="radio" name="rdbConsultores" id="CO_'+row.cdUsuario+'" class="form-control" value="'+row.cdUsuario+'" /><label for="CO_'+row.cdUsuario+'"></label>'].join('');
}

function formatterDetailCasosPorAsignar(value, row, index){
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Folio Alerta:</b> '+row.nuFolioAlerta+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha de Alerta:</b> '+formatterDate(row.fhAlerta)+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Cuenta:</b> '+row.nuCuenta+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Sistema Origen:</b> '+row.nbSistema+'</td></tr>'+
	        '</table>'].join('');
}

function formatterDetailCasosAsignados(value, row, index){
	
	return ['<table class="tableForm">'+
	        '<tr><td class="tdDetail"><b>Sistema Origen:</b> '+row.nbSistema+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Segmento PLD:</b> '+row.nbSegmento+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Fecha de Asignaci&oacute;n:</b> '+formatterDate(row.fhAsignacion)+'</td></tr>'+
	        '<tr><td class="tdDetail"><b>Prioridad:</b> '+row.nbPrioriAsig+'</td></tr>'+
	        '</table>'].join('');
}

function consultarConsultoresSupervisor(cdSupervisor) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/consultarConsultoresSupervisor/"+cdSupervisor,
		type : "GET",
		async: false
	});
}

function abrirModalFormConsultor(title,tpOperacion){
	$('#titleFormConsiltor').html(title);
	op = tpOperacion;

	consultarConsultoresSupervisor(sessionStorage.cdUsuario).done(function(data) { 
		if(cargarMensaje(data,false)){		
			$('#tablaConsultores').bootstrapTable('load',data.response);
		}else{
			$('#tablaConsultores').bootstrapTable('load',[]);
		}
	});
	
	if(op == 'RE'){
		$('#tablaConsultores').bootstrapTable('remove', {
            field: 'cdUsuario',
            values: [$('#cmbConsultores').val()]
        });
		
		$('#cmbPrioridad').hide();
		$('label[for="cmbPrioridad"]').hide();
		$('input[name="rdbConsultores"]')[0].checked=true;
		$("#btnOkAsignar").attr('onClick','js_reasignarCasos();');
	}else{
		
		$('#cmbPrioridad').show();
		$('label[for="cmbPrioridad"]').show();
		$('input[name="rdbConsultores"]')[0].checked=true;
		$("#btnOkAsignar").attr('onClick','js_asignarAlertasCaso();');
	}
	
	$('#dialogFormConsultor').modal('show');
}

function abrirModalConfirm(){
	$('#dialogComfirm').modal('show');
}

function checkAll(nameCheck,obj){
	$('input[name="'+nameCheck+'"]').each(function () {
			$(this).removeAttr('checked');
			$(this).prop('checked',obj.checked);
	});
}

function validarCasosPorAsignar(){
	var numCasos = 0;
	var ban = true;
	
	if($('input[name="checkCasosPorAsignar"]').length > 0){
		$('input[name="checkCasosPorAsignar"]').each(function () {
			if($(this).prop('checked')){
				numCasos++;
			}else{
				ban = false;
			}
		});
	}else{
		ban = false;
	}
	
	if(!ban){
		$('#checkAllCasosPorAsignar').prop('checked',false);
	}else{
		$('#checkAllCasosPorAsignar').prop('checked',true);
	}
	
	$("#numSelCasosPorAsignar").html(numCasos);
	
	
	if(numCasos > 0){
		$("#btnAsignar").prop("disabled", false);
		$("#btnDescartar").prop("disabled", false);
	}else{
		$("#btnAsignar").prop("disabled", true);
		$("#btnDescartar").prop("disabled", true);
	}
}

function validarCasosAsignados(){
	var numCasos = 0;
	var ban = true;
	if($('input[name="checkCasoAsignado"]').length > 0){
		$('input[name="checkCasoAsignado"]').each(function () {
			if($(this).prop('checked')){
				numCasos++;
			}else{
				ban = false;
			}
		});
	}else{
		ban = false;
	}
	
	if(numCasos > 0){
		$("#btnReasignar").prop("disabled", false);
	}else{
		$("#btnReasignar").prop("disabled", true);
	}
	
	if(!ban){
		$('#checkAllCasosAsignados').prop('checked',false);
	}else{
		$('#checkAllCasosAsignados').prop('checked',true);
	}
}

function selectedConsultor(){
	var cdConsultor = $("#cmbConsultores").val();
	if(cdConsultor != "-1" && cdConsultor != ""){
		$('#tablaCasosAsignados').bootstrapTable('refresh', {
			url: obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/consultarCasosAsignadosConsultor/"+cdConsultor
		});
	}
}

function asignarAlertasCasos(cdConsultor,cdPrioriAsig,lstAlertaDTO) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/asignarAlertasCasos/"+cdConsultor+"/"+cdPrioriAsig,
		type : 'PUT',
		contentType:'application/json',
		dataType: 'JSON',
		data: JSON.stringify(lstAlertaDTO),
		async: false
	});
} 

function descartarAlertas(lstAlertaDTO) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/descartarAlertas",
		type : 'POST',
		contentType:'application/json',
		dataType: 'JSON',
		data: JSON.stringify(lstAlertaDTO),
		async: false
	});
} 

function reasignarCasos(lstAlertaDTO,cdConsultor) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/reasignarCasos/"+cdConsultor,
		type : 'POST',
		contentType:'application/json',
		dataType: 'JSON',
		data: JSON.stringify(lstAlertaDTO),
		async: false
	});
}

function consultarCatalogoOperacionInusual(cOperacion) {
	return $.ajax({
		url : obtenerDeSessionStorage("baseUrl", true) + "/CatalogoService/consultarCatalogoOperacionInusual/" + cOperacion,
		type : "GET",
		async: false
	});
}

function js_asignarAlertasCaso(){
	var cdPrioriAsig = $("#cmbPrioridad").val();
	var cdConsultor = $('input[name=rdbConsultores]:checked').val();
	var arrAlertas = [];
	var data = $('#tablaCasosPorAsignar').bootstrapTable('getData');
	
	$.each(data, function (i,row) {
		var idRow = row.nuFolioAlerta+"_"+row.cdSistema;
		$("input[name=checkCasosPorAsignar]:checked").each( function (index) {
			if(idRow == $(this).val()){
				arrAlertas[index] = row;
			}
		});
    });

	asignarAlertasCasos(cdConsultor,cdPrioriAsig,arrAlertas).done(function(data) { 
		if(cargarMensaje(data,true)){
			
			$('#tablaCasosPorAsignar').bootstrapTable('refresh', {
				url: obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/consultarAlertasPendientesAsignacionGerencia/"+sessionStorage.cdGerencia
    		});
			$("#numSelCasosPorAsignar").html('0');		
			$("#cmbConsultores").val(cdConsultor);
			selectedConsultor();	
		}
	});
	validarCasosPorAsignar();
}

function js_reasignarCasos(){
	
	var cdConsultor = $('input[name=rdbConsultores]:checked').val();
	var arrAlertas = [];
	var data = $('#tablaCasosAsignados').bootstrapTable('getData');
	
	$.each(data, function (i,row) {
		var idRow = row.nuFolioAlerta+"_"+row.cdSistema;
		$("input[name=checkCasoAsignado]:checked").each( function (index) {
			if(idRow == $(this).val()){
				arrAlertas[index] = row;
			}
		});
    });
	reasignarCasos(arrAlertas,cdConsultor).done(function(data){
		if(cargarMensaje(data,true)){
			$("#cmbConsultores").val(cdConsultor);
			selectedConsultor();	
		}
	});
	
	validarCasosAsignados();
}



$("#btnOk").click(function(){
	
	var arrAlertas = [];
	var data = $('#tablaCasosPorAsignar').bootstrapTable('getData');
	var txMotivoDescarte = $("#cOperacion option:selected").text(); 
	$.each(data, function (i,row) {
		var idRow = row.nuFolioAlerta+"_"+row.cdSistema;
		$("input[name=checkCasosPorAsignar]:checked").each( function (index) {
			if(idRow == $(this).val()){
				row.txMotivoDescarte = txMotivoDescarte;
				arrAlertas[index] = row;
			}
		});	
    });
	
	

	descartarAlertas(arrAlertas).done(function(data) { 
		if(cargarMensaje(data,true)){
			$('#tablaCasosPorAsignar').bootstrapTable('refresh', {
				url: obtenerDeSessionStorage("baseUrl", true) + "/AsignacionCasosService/consultarAlertasPendientesAsignacionGerencia/"+sessionStorage.cdGerencia
    		});
			$("#numSelCasosPorAsignar").html('0');
		}
	});
	
	validarCasosPorAsignar();
});

