package com.bbva.msca.front.util;


import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;

public class Utilidad {
   
   private static final I18nLog LOGGER = I18nLogFactory.getLogI18n(Utilidad.class);
   
   public Utilidad() {
       //Constructor por Default.
   }

   public String getUrlServicios() {
       String urlServicios = "";
       String modulo;
       String junction;
       String host;
       try {
           host = ArqSpringContext.getPropiedad("VAR.ARQSPRING.URL.HOST");
           modulo = ArqSpringContext.getPropiedad("VAR.ARQSPRING.MODULO.BACK");
           junction = ArqSpringContext.getPropiedad("VAR.ARQSPRING.JUNCTION");
           LOGGER.info(host, host);
           LOGGER.info(modulo, modulo);
           LOGGER.info(junction, junction);
           if(junction != null && !"".equals(junction)){
               urlServicios = host+"/"+junction+"/"+modulo;
           }else{
               urlServicios = host+"/"+modulo;
           }
           LOGGER.info(urlServicios, urlServicios);
       } catch (PropiedadNoEncontradaExcepcion e) {
           LOGGER.error(e, "error.utilidad.01");
       }
       
       return urlServicios;
   }
   
   public String getUrlBase() {
       String urlBase = "";
       String modulo;
       String junction;
       try {
           modulo = ArqSpringContext.getPropiedad("VAR.ARQSPRING.MODULO.FRONT");
           junction = ArqSpringContext.getPropiedad("VAR.ARQSPRING.JUNCTION");
           if(junction != null && !"".equals(junction)){
               urlBase = "/"+junction+"/"+modulo;
           }else{
               urlBase = "/"+modulo;
           }
       } catch (PropiedadNoEncontradaExcepcion e) {
           LOGGER.error(e, "error.utilidad.02");
       }
       
       return urlBase;
   }
   
   public String getUrlLogout() {
	   String urlLogout = "";
	   try {
		   urlLogout =  ArqSpringContext.getPropiedad("VAR.ARQSPRING.URL.LOGOUT");
	   } catch (PropiedadNoEncontradaExcepcion e) {
           LOGGER.error(e, "error.utilidad.03");
       }
	   
	   return urlLogout;
   }
}
