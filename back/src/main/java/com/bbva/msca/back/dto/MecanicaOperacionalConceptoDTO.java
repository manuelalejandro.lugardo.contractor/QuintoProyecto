package com.bbva.msca.back.dto;
import java.math.BigDecimal;
public class MecanicaOperacionalConceptoDTO {
	//
	//TABLA CUATRIMESTRAL
	private String nbConceptoTC;
	private BigDecimal nuMovimientosTC;
	private BigDecimal imMontoTC;
	private BigDecimal nuPorcentajeTC;
	private BigDecimal nuPromedioTC;
	private BigDecimal nuMovimientosATC;
	private BigDecimal imMontoATC;
	private BigDecimal nuPorcentajeATC;
	private BigDecimal nuPromedioATC;
	//
	//TABLA 1 
	private String nbConceptoTM1;
    private BigDecimal nuMovimientosCTM1;
    private BigDecimal imMontoCTM1;
    private BigDecimal nuPorcentajeCTM1;
    private BigDecimal nuPromedioCTM1;
    private BigDecimal nuMovimientosATM1;
    private BigDecimal imMontoATM1;
    private BigDecimal nuPorcentajeATM1;
    private BigDecimal nuPromedioATM1;
	//
	//TABLA 2
	private String nbConceptoTM2;
    private BigDecimal nuMovimientosCTM2;
    private BigDecimal imMontoCTM2;
    private BigDecimal nuPorcentajeCTM2;
    private BigDecimal nuPromedioCTM2;
    private BigDecimal nuMovimientosATM2;
    private BigDecimal imMontoATM2;
    private BigDecimal nuPorcentajeATM2;
    private BigDecimal nuPromedioATM2;
	//
	//TABLA 3
	private String nbConceptoTM3;
    private BigDecimal nuMovimientosCTM3;
    private BigDecimal imMontoCTM3;
    private BigDecimal nuPorcentajeCTM3;
    private BigDecimal nuPromedioCTM3;
    private BigDecimal nuMovimientosATM3;
    private BigDecimal imMontoATM3;
    private BigDecimal nuPorcentajeATM3;
    private BigDecimal nuPromedioATM3;
	//
	//TABLA 4
	private String nbConceptoTM4;
    private BigDecimal nuMovimientosCTM4;
    private BigDecimal imMontoCTM4;
    private BigDecimal nuPorcentajeCTM4;
    private BigDecimal nuPromedioCTM4;
    private BigDecimal nuMovimientosATM4;
    private BigDecimal imMontoATM4;
    private BigDecimal nuPorcentajeATM4;
    private BigDecimal nuPromedioATM4;
    
    //CabeceraCuatrimestral
    private String inicio;
    private BigDecimal fin;
    private String mes;
    private BigDecimal mes2;
    private BigDecimal mes3;
    private BigDecimal mes4;
    
	public BigDecimal getMes2() {
		return mes2;
	}
	public void setMes2(BigDecimal mes2) {
		this.mes2 = mes2;
	}
	public BigDecimal getMes3() {
		return mes3;
	}
	public void setMes3(BigDecimal mes3) {
		this.mes3 = mes3;
	}
	public BigDecimal getMes4() {
		return mes4;
	}
	public void setMes4(BigDecimal mes4) {
		this.mes4 = mes4;
	}
	public String getMes() {
		return mes;
	}
	public void setMes(String mes) {
		this.mes = mes;
	}
	public String getInicio() {
		return inicio;
	}
	public void setInicio(String inicio) {
		this.inicio = inicio;
	}
	public BigDecimal getFin() {
		return fin;
	}
	public void setFin(BigDecimal fin) {
		this.fin = fin;
	}
	public String getNbConceptoTC() {
		return nbConceptoTC;
	}
	public void setNbConceptoTC(String nbConceptoTC) {
		this.nbConceptoTC = nbConceptoTC;
	}
	public BigDecimal getNuMovimientosTC() {
		return nuMovimientosTC;
	}
	public void setNuMovimientosTC(BigDecimal nuMovimientosTC) {
		this.nuMovimientosTC = nuMovimientosTC;
	}
	public BigDecimal getImMontoTC() {
		return imMontoTC;
	}
	public void setImMontoTC(BigDecimal imMontoTC) {
		this.imMontoTC = imMontoTC;
	}
	public BigDecimal getNuPorcentajeTC() {
		return nuPorcentajeTC;
	}
	public void setNuPorcentajeTC(BigDecimal nuPorcentajeTC) {
		this.nuPorcentajeTC = nuPorcentajeTC;
	}
	public BigDecimal getNuPromedioTC() {
		return nuPromedioTC;
	}
	public void setNuPromedioTC(BigDecimal nuPromedioTC) {
		this.nuPromedioTC = nuPromedioTC;
	}
	public BigDecimal getNuMovimientosATC() {
		return nuMovimientosATC;
	}
	public void setNuMovimientosATC(BigDecimal nuMovimientosATC) {
		this.nuMovimientosATC = nuMovimientosATC;
	}
	public BigDecimal getImMontoATC() {
		return imMontoATC;
	}
	public void setImMontoATC(BigDecimal imMontoATC) {
		this.imMontoATC = imMontoATC;
	}
	public BigDecimal getNuPorcentajeATC() {
		return nuPorcentajeATC;
	}
	public void setNuPorcentajeATC(BigDecimal nuPorcentajeATC) {
		this.nuPorcentajeATC = nuPorcentajeATC;
	}
	public BigDecimal getNuPromedioATC() {
		return nuPromedioATC;
	}
	public void setNuPromedioATC(BigDecimal nuPromedioATC) {
		this.nuPromedioATC = nuPromedioATC;
	}
	public String getNbConceptoTM1() {
		return nbConceptoTM1;
	}
	public void setNbConceptoTM1(String nbConceptoTM1) {
		this.nbConceptoTM1 = nbConceptoTM1;
	}
	public BigDecimal getNuMovimientosCTM1() {
		return nuMovimientosCTM1;
	}
	public void setNuMovimientosCTM1(BigDecimal nuMovimientosCTM1) {
		this.nuMovimientosCTM1 = nuMovimientosCTM1;
	}
	public BigDecimal getImMontoCTM1() {
		return imMontoCTM1;
	}
	public void setImMontoCTM1(BigDecimal imMontoCTM1) {
		this.imMontoCTM1 = imMontoCTM1;
	}
	public BigDecimal getNuPorcentajeCTM1() {
		return nuPorcentajeCTM1;
	}
	public void setNuPorcentajeCTM1(BigDecimal nuPorcentajeCTM1) {
		this.nuPorcentajeCTM1 = nuPorcentajeCTM1;
	}
	public BigDecimal getNuPromedioCTM1() {
		return nuPromedioCTM1;
	}
	public void setNuPromedioCTM1(BigDecimal nuPromedioCTM1) {
		this.nuPromedioCTM1 = nuPromedioCTM1;
	}
	public BigDecimal getNuMovimientosATM1() {
		return nuMovimientosATM1;
	}
	public void setNuMovimientosATM1(BigDecimal nuMovimientosATM1) {
		this.nuMovimientosATM1 = nuMovimientosATM1;
	}
	public BigDecimal getImMontoATM1() {
		return imMontoATM1;
	}
	public void setImMontoATM1(BigDecimal imMontoATM1) {
		this.imMontoATM1 = imMontoATM1;
	}
	public BigDecimal getNuPorcentajeATM1() {
		return nuPorcentajeATM1;
	}
	public void setNuPorcentajeATM1(BigDecimal nuPorcentajeATM1) {
		this.nuPorcentajeATM1 = nuPorcentajeATM1;
	}
	public BigDecimal getNuPromedioATM1() {
		return nuPromedioATM1;
	}
	public void setNuPromedioATM1(BigDecimal nuPromedioATM1) {
		this.nuPromedioATM1 = nuPromedioATM1;
	}
	public String getNbConceptoTM2() {
		return nbConceptoTM2;
	}
	public void setNbConceptoTM2(String nbConceptoTM2) {
		this.nbConceptoTM2 = nbConceptoTM2;
	}
	public BigDecimal getNuMovimientosCTM2() {
		return nuMovimientosCTM2;
	}
	public void setNuMovimientosCTM2(BigDecimal nuMovimientosCTM2) {
		this.nuMovimientosCTM2 = nuMovimientosCTM2;
	}
	public BigDecimal getImMontoCTM2() {
		return imMontoCTM2;
	}
	public void setImMontoCTM2(BigDecimal imMontoCTM2) {
		this.imMontoCTM2 = imMontoCTM2;
	}
	public BigDecimal getNuPorcentajeCTM2() {
		return nuPorcentajeCTM2;
	}
	public void setNuPorcentajeCTM2(BigDecimal nuPorcentajeCTM2) {
		this.nuPorcentajeCTM2 = nuPorcentajeCTM2;
	}
	public BigDecimal getNuPromedioCTM2() {
		return nuPromedioCTM2;
	}
	public void setNuPromedioCTM2(BigDecimal nuPromedioCTM2) {
		this.nuPromedioCTM2 = nuPromedioCTM2;
	}
	public BigDecimal getNuMovimientosATM2() {
		return nuMovimientosATM2;
	}
	public void setNuMovimientosATM2(BigDecimal nuMovimientosATM2) {
		this.nuMovimientosATM2 = nuMovimientosATM2;
	}
	public BigDecimal getImMontoATM2() {
		return imMontoATM2;
	}
	public void setImMontoATM2(BigDecimal imMontoATM2) {
		this.imMontoATM2 = imMontoATM2;
	}
	public BigDecimal getNuPorcentajeATM2() {
		return nuPorcentajeATM2;
	}
	public void setNuPorcentajeATM2(BigDecimal nuPorcentajeATM2) {
		this.nuPorcentajeATM2 = nuPorcentajeATM2;
	}
	public BigDecimal getNuPromedioATM2() {
		return nuPromedioATM2;
	}
	public void setNuPromedioATM2(BigDecimal nuPromedioATM2) {
		this.nuPromedioATM2 = nuPromedioATM2;
	}
	public String getNbConceptoTM3() {
		return nbConceptoTM3;
	}
	public void setNbConceptoTM3(String nbConceptoTM3) {
		this.nbConceptoTM3 = nbConceptoTM3;
	}
	public BigDecimal getNuMovimientosCTM3() {
		return nuMovimientosCTM3;
	}
	public void setNuMovimientosCTM3(BigDecimal nuMovimientosCTM3) {
		this.nuMovimientosCTM3 = nuMovimientosCTM3;
	}
	public BigDecimal getImMontoCTM3() {
		return imMontoCTM3;
	}
	public void setImMontoCTM3(BigDecimal imMontoCTM3) {
		this.imMontoCTM3 = imMontoCTM3;
	}
	public BigDecimal getNuPorcentajeCTM3() {
		return nuPorcentajeCTM3;
	}
	public void setNuPorcentajeCTM3(BigDecimal nuPorcentajeCTM3) {
		this.nuPorcentajeCTM3 = nuPorcentajeCTM3;
	}
	public BigDecimal getNuPromedioCTM3() {
		return nuPromedioCTM3;
	}
	public void setNuPromedioCTM3(BigDecimal nuPromedioCTM3) {
		this.nuPromedioCTM3 = nuPromedioCTM3;
	}
	public BigDecimal getNuMovimientosATM3() {
		return nuMovimientosATM3;
	}
	public void setNuMovimientosATM3(BigDecimal nuMovimientosATM3) {
		this.nuMovimientosATM3 = nuMovimientosATM3;
	}
	public BigDecimal getImMontoATM3() {
		return imMontoATM3;
	}
	public void setImMontoATM3(BigDecimal imMontoATM3) {
		this.imMontoATM3 = imMontoATM3;
	}
	public BigDecimal getNuPorcentajeATM3() {
		return nuPorcentajeATM3;
	}
	public void setNuPorcentajeATM3(BigDecimal nuPorcentajeATM3) {
		this.nuPorcentajeATM3 = nuPorcentajeATM3;
	}
	public BigDecimal getNuPromedioATM3() {
		return nuPromedioATM3;
	}
	public void setNuPromedioATM3(BigDecimal nuPromedioATM3) {
		this.nuPromedioATM3 = nuPromedioATM3;
	}
	public String getNbConceptoTM4() {
		return nbConceptoTM4;
	}
	public void setNbConceptoTM4(String nbConceptoTM4) {
		this.nbConceptoTM4 = nbConceptoTM4;
	}
	public BigDecimal getNuMovimientosCTM4() {
		return nuMovimientosCTM4;
	}
	public void setNuMovimientosCTM4(BigDecimal nuMovimientosCTM4) {
		this.nuMovimientosCTM4 = nuMovimientosCTM4;
	}
	public BigDecimal getImMontoCTM4() {
		return imMontoCTM4;
	}
	public void setImMontoCTM4(BigDecimal imMontoCTM4) {
		this.imMontoCTM4 = imMontoCTM4;
	}
	public BigDecimal getNuPorcentajeCTM4() {
		return nuPorcentajeCTM4;
	}
	public void setNuPorcentajeCTM4(BigDecimal nuPorcentajeCTM4) {
		this.nuPorcentajeCTM4 = nuPorcentajeCTM4;
	}
	public BigDecimal getNuPromedioCTM4() {
		return nuPromedioCTM4;
	}
	public void setNuPromedioCTM4(BigDecimal nuPromedioCTM4) {
		this.nuPromedioCTM4 = nuPromedioCTM4;
	}
	public BigDecimal getNuMovimientosATM4() {
		return nuMovimientosATM4;
	}
	public void setNuMovimientosATM4(BigDecimal nuMovimientosATM4) {
		this.nuMovimientosATM4 = nuMovimientosATM4;
	}
	public BigDecimal getImMontoATM4() {
		return imMontoATM4;
	}
	public void setImMontoATM4(BigDecimal imMontoATM4) {
		this.imMontoATM4 = imMontoATM4;
	}
	public BigDecimal getNuPorcentajeATM4() {
		return nuPorcentajeATM4;
	}
	public void setNuPorcentajeATM4(BigDecimal nuPorcentajeATM4) {
		this.nuPorcentajeATM4 = nuPorcentajeATM4;
	}
	public BigDecimal getNuPromedioATM4() {
		return nuPromedioATM4;
	}
	public void setNuPromedioATM4(BigDecimal nuPromedioATM4) {
		this.nuPromedioATM4 = nuPromedioATM4;
	}
    
    


	

	

}
