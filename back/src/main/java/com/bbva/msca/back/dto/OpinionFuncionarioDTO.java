package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class OpinionFuncionarioDTO {
	
	private BigDecimal nuPregunta;
	private String nbPregunta;
	private String txtRespuesta;
	
	///(CD_ALERTA, CD_SISTEMA, FH_ENVIO, FH_RECORDATORIO, FH_VENCIMIENTO, CD_USUARIO, ST_CUESTIONARIO)
	private String cdAlerta;
	private String cdSistema;
	private Date fhRecordatorio;
	private String cdUsuario;
	private Date fhEnvioCuest; 
	private String nbFuncionarioCuest;
	private String estatusCuest;
	private Date fhRespuestaCuest;
	private Date fhVencimientoCuest;
	private Date fhVencimientoCuest2;
	private String cdFuncionarioOF;
	private String txtComentarioCuest;
	private String cdOficinaGest;
	private BigDecimal tpCuestionario;
		
	public BigDecimal getNuPregunta() {
		return nuPregunta;
	}
	public void setNuPregunta(BigDecimal nuPregunta) {
		this.nuPregunta = nuPregunta;
	}
	public String getNbPregunta() {
		return nbPregunta;
	}
	public void setNbPregunta(String nbPregunta) {
		this.nbPregunta = nbPregunta;
	}
	public String getTxtRespuesta() {
		return txtRespuesta;
	}
	public void setTxtRespuesta(String txtRespuesta) {
		this.txtRespuesta = txtRespuesta;
	}
	public Date getFhEnvioCuest() {
		return fhEnvioCuest;
	}
	public void setFhEnvioCuest(Date fhEnvioCuest) {
		this.fhEnvioCuest = fhEnvioCuest;
	}
	public String getNbFuncionarioCuest() {
		return nbFuncionarioCuest;
	}
	public void setNbFuncionarioCuest(String nbFuncionarioCuest) {
		this.nbFuncionarioCuest = nbFuncionarioCuest;
	}
	public String getEstatusCuest() {
		return estatusCuest;
	}
	public void setEstatusCuest(String estatusCuest) {
		this.estatusCuest = estatusCuest;
	}
	public Date getFhRespuestaCuest() {
		return fhRespuestaCuest;
	}
	public void setFhRespuestaCuest(Date fhRespuestaCuest) {
		this.fhRespuestaCuest = fhRespuestaCuest;
	}
	public Date getFhVencimientoCuest() {
		return fhVencimientoCuest;
	}
	public void setFhVencimientoCuest(Date fhVencimientoCuest) {
		this.fhVencimientoCuest = fhVencimientoCuest;
	}
	public Date getFhVencimientoCuest2() {
		return fhVencimientoCuest2;
	}
	public void setFhVencimientoCuest2(Date fhVencimientoCuest2) {
		this.fhVencimientoCuest2 = fhVencimientoCuest2;
	}
	public String getCdAlerta() {
		return cdAlerta;
	}
	public void setCdAlerta(String cdAlerta) {
		this.cdAlerta = cdAlerta;
	}
	public String getCdSistema() {
		return cdSistema;
	}
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}
	public Date getFhRecordatorio() {
		return fhRecordatorio;
	}
	public void setFhRecordatorio(Date fhRecordatorio) {
		this.fhRecordatorio = fhRecordatorio;
	}
	public String getCdUsuario() {
		return cdUsuario;
	}
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}
	public String getCdFuncionarioOF() {
		return cdFuncionarioOF;
	}
	public void setCdFuncionarioOF(String cdFuncionarioOF) {
		this.cdFuncionarioOF = cdFuncionarioOF;
	}
	public String getTxtComentarioCuest() {
		return txtComentarioCuest;
	}
	public void setTxtComentarioCuest(String txtComentarioCuest) {
		this.txtComentarioCuest = txtComentarioCuest;
	}
	public String getCdOficinaGest() {
		return cdOficinaGest;
	}
	public void setCdOficinaGest(String cdOficinaGest) {
		this.cdOficinaGest = cdOficinaGest;
	}
	public BigDecimal getTpCuestionario() {
		return tpCuestionario;
	}
	public void setTpCuestionario(BigDecimal tpCuestionario) {
		this.tpCuestionario = tpCuestionario;
	}
	
	
}