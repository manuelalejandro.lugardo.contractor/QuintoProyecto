package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.TimestampType;

import com.bbva.msca.back.dto.MecanicaOperacionalOrigenDestinoDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class MecanicaOperacionalOrigenDestinoDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	/*
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalOrigenDestinoDTO> getMecanicaOperacionalOrigen() throws Exception{
		List<MecanicaOperacionalOrigenDestinoDTO> lstMecanicaOperacionalOrigenDTO = new ArrayList<MecanicaOperacionalOrigenDestinoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaOrigenMOCR"));
		 //query.setParameter(1,cdSupervisor);
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("fechaOMOCR", TimestampType.INSTANCE)
	        .addScalar("nbConceptoOMOCR")
	        .addScalar("imImporteOMOCR")
	        .addScalar("nbMonedaOMOCR")

	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalOrigenDestinoDTO.class));
		 lstMecanicaOperacionalOrigenDTO = (List<MecanicaOperacionalOrigenDestinoDTO>)query.getResultList();
		return lstMecanicaOperacionalOrigenDTO;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalOrigenDestinoDTO> getMecanicaOperacionalDestino() throws Exception{
		List<MecanicaOperacionalOrigenDestinoDTO> lstMecanicaOperacionalDestinoDTO = new ArrayList<MecanicaOperacionalOrigenDestinoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaDestinoMOCR"));
		 //query.setParameter(1,cdSupervisor);
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("fechaDMOCR", TimestampType.INSTANCE)
	        .addScalar("nbConceptoDMOCR")
	        .addScalar("imImporteDMOCR")
	        .addScalar("nbMonedaDMOCR")

	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalOrigenDestinoDTO.class));
		 lstMecanicaOperacionalDestinoDTO = (List<MecanicaOperacionalOrigenDestinoDTO>)query.getResultList();
	return lstMecanicaOperacionalDestinoDTO;		
	}
*/

	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalOrigenDestinoDTO> getMecanicaOperacionalCuentasVinculadasOrigen() throws Exception {
		List<MecanicaOperacionalOrigenDestinoDTO> lstMecanicaOperacionalOrigenDTO = new ArrayList<MecanicaOperacionalOrigenDestinoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaOrigenACVO"));
		 //query.setParameter(1,cdSupervisor);
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("fechaACVO", TimestampType.INSTANCE)
	        .addScalar("nbCuentaACVO")
	        .addScalar("imImporteACVO")
	        .addScalar("nbConceptoACVO")

	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalOrigenDestinoDTO.class));
		 lstMecanicaOperacionalOrigenDTO = (List<MecanicaOperacionalOrigenDestinoDTO>)query.getResultList();
		 
		return lstMecanicaOperacionalOrigenDTO;	
	}

	
	
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalOrigenDestinoDTO> getMecanicaOperacionalCuentasVinculadasDestino() throws Exception {
		List<MecanicaOperacionalOrigenDestinoDTO> lstMecanicaOperacionalDestinoDTO = new ArrayList<MecanicaOperacionalOrigenDestinoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaDestinoACVD"));
		 //query.setParameter(1,cdSupervisor);
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("fechaACVD", TimestampType.INSTANCE)
	        .addScalar("nbConceptoACVD")
	        .addScalar("imImporteACVD")
	        .addScalar("nbCuentaACVD")

	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalOrigenDestinoDTO.class));
		 lstMecanicaOperacionalDestinoDTO = (List<MecanicaOperacionalOrigenDestinoDTO>)query.getResultList();
	return lstMecanicaOperacionalDestinoDTO;
	}



	
	
	
}
