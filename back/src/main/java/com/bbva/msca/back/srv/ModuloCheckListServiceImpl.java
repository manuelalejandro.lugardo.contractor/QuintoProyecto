package com.bbva.msca.back.srv;

import java.util.List;

import com.bbva.msca.back.dao.ModuloCheckListDAO;
import com.bbva.msca.back.dto.BusquedaCheckListDTO;
import com.bbva.msca.back.dto.ModuloCheckListDTO;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class ModuloCheckListServiceImpl implements ModuloCheckListService {
	
	
	@Override
	public ResponseGeneric consultarModuloCheckList() {
		
		ResponseGeneric response = new ResponseGeneric();
		ModuloCheckListDAO moduloCheckListDAO = new ModuloCheckListDAO();
		List<ModuloCheckListDTO> lstModuloCheckListDTO = moduloCheckListDAO.getModuloCheckList();
		
		response.setResponse(lstModuloCheckListDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}

	@Override
	public ResponseGeneric busquedaModuloCheckList() {
		ResponseGeneric response = new ResponseGeneric();
		BusquedaCheckListDTO busquedaCheckListDTO = ModuloCheckListDAO.getBusquedaCheckList();
		
		response.setResponse(busquedaCheckListDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	


}
