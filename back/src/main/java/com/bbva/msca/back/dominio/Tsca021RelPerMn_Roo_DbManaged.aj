// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca020Perfil;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

privileged aspect Tsca021RelPerMn_Roo_DbManaged {
    
    @ManyToOne
    @JoinColumn(name = "CD_PERFIL", referencedColumnName = "CD_PERFIL", nullable = false, insertable = false, updatable = false)
    private Tsca020Perfil Tsca021RelPerMn.cdPerfil;
    
    public Tsca020Perfil Tsca021RelPerMn.getCdPerfil() {
        return this.cdPerfil;
    }
    
    public void Tsca021RelPerMn.setCdPerfil(Tsca020Perfil cdPerfil) {
        this.cdPerfil = cdPerfil;
    }
    
}
