package com.bbva.msca.back.srv;

import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.TipologiaDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/TipologiaService")

public interface TipologiaService {
	
	//PARAMETRIA - FN02 TIPOLOGIA
	@GET
	@Path("/consultarTipologias/{cdGerencia}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarTipologia(@PathParam("cdGerencia") int cdGerencia);
	
	@POST
	@Path("/altaTipologia")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric creaTipologia(TipologiaDTO tipologia);
	
	@POST
	@Path("/modificaTipologia")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric modificaTipologia(TipologiaDTO tipologia);

}
