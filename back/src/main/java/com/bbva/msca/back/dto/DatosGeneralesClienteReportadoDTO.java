package com.bbva.msca.back.dto;

import java.util.Date;

public class DatosGeneralesClienteReportadoDTO {

	private String nbClienteCR;
	private String cdClienteCR;
	private String nbCiudadCR;
	private String nbDireccionCR;
	private String cdRfcCR;
	private String nbTelParCR;
	private String nbTelOfiCR;
	private String nbNacCR;
	private Date fhAltaCR;
	private String cdSectorCR;
	private String fhAntNegoCR;
	private Date fechaActualCR;
	private String nbActividadBanxicoCR;
	private String nbCurpCR;
	private String nbPaisNacimientoCR;
	private String nbPaisRecidenciaCR;
	
	private Date fhAperturaCR;
	private Date fhUltimaCR;
	private String tipoFuenteCR;
	private String clasifCteCR; //char
	private String nbOficinaCR;
	private String nbRiesgoCR;
    ////////---analisis del caso
	private String nbClienteCR2;
	private String cdClienteCR2;
	private String nbCiudadCR2;
	private String nbDireccionCR2;
	private String cdRfcCR2;
	private String nbTelParCR2;
	private String nbTelOfiCR2;
	private String nbNacCR2;
	private Date fhAltaCR2;
	private String cdSectorCR2;
	private String fhAntNegoCR2;
	private Date fechaActualCR2;
	private String nbActividadBanxicoCR2;
	private String nbCurpCR2;
	private String nbPaisNacimientoCR2;
	private String nbPaisRecidenciaCR2;
	
	private Date fhAperturaCR2;
	private Date fhUltimaCR2;
	private String tipoFuenteCR2;
	private String clasifCteCR2; //char
	private String nbOficinaCR2;
	private String nbRiesgoCR2;
     
     
	public String getNbClienteCR() {
		return nbClienteCR;
	}
	public void setNbClienteCR(String nbClienteCR) {
		this.nbClienteCR = nbClienteCR;
	}
	public String getCdClienteCR() {
		return cdClienteCR;
	}
	public void setCdClienteCR(String cdClienteCR) {
		this.cdClienteCR = cdClienteCR;
	}
	public String getNbCiudadCR() {
		return nbCiudadCR;
	}
	public void setNbCiudadCR(String nbCiudadCR) {
		this.nbCiudadCR = nbCiudadCR;
	}
	public String getNbDireccionCR() {
		return nbDireccionCR;
	}
	public void setNbDireccionCR(String nbDireccionCR) {
		this.nbDireccionCR = nbDireccionCR;
	}
	public String getCdRfcCR() {
		return cdRfcCR;
	}
	public void setCdRfcCR(String cdRfcCR) {
		this.cdRfcCR = cdRfcCR;
	}
	public String getNbTelParCR() {
		return nbTelParCR;
	}
	public void setNbTelParCR(String nbTelParCR) {
		this.nbTelParCR = nbTelParCR;
	}
	public String getNbTelOfiCR() {
		return nbTelOfiCR;
	}
	public void setNbTelOfiCR(String nbTelOfiCR) {
		this.nbTelOfiCR = nbTelOfiCR;
	}
	public String getNbNacCR() {
		return nbNacCR;
	}
	public void setNbNacCR(String nbNacCR) {
		this.nbNacCR = nbNacCR;
	}
	public Date getFhAltaCR() {
		return fhAltaCR;
	}
	public void setFhAltaCR(Date fhAltaCR) {
		this.fhAltaCR = fhAltaCR;
	}
	public String getCdSectorCR() {
		return cdSectorCR;
	}
	public void setCdSectorCR(String cdSectorCR) {
		this.cdSectorCR = cdSectorCR;
	}
	public String getFhAntNegoCR() {
		return fhAntNegoCR;
	}
	public void setFhAntNegoCR(String fhAntNegoCR) {
		this.fhAntNegoCR = fhAntNegoCR;
	}
	public Date getFechaActualCR() {
		return fechaActualCR;
	}
	public void setFechaActualCR(Date fechaActualCR) {
		this.fechaActualCR = fechaActualCR;
	}
	public String getNbActividadBanxicoCR() {
		return nbActividadBanxicoCR;
	}
	public void setNbActividadBanxicoCR(String nbActividadBanxicoCR) {
		this.nbActividadBanxicoCR = nbActividadBanxicoCR;
	}
	public String getNbCurpCR() {
		return nbCurpCR;
	}
	public void setNbCurpCR(String nbCurpCR) {
		this.nbCurpCR = nbCurpCR;
	}
	public String getNbPaisNacimientoCR() {
		return nbPaisNacimientoCR;
	}
	public void setNbPaisNacimientoCR(String nbPaisNacimientoCR) {
		this.nbPaisNacimientoCR = nbPaisNacimientoCR;
	}
	public String getNbPaisRecidenciaCR() {
		return nbPaisRecidenciaCR;
	}
	public void setNbPaisRecidenciaCR(String nbPaisRecidenciaCR) {
		this.nbPaisRecidenciaCR = nbPaisRecidenciaCR;
	}
	public Date getFhAperturaCR() {
		return fhAperturaCR;
	}
	public void setFhAperturaCR(Date fhAperturaCR) {
		this.fhAperturaCR = fhAperturaCR;
	}
	public Date getFhUltimaCR() {
		return fhUltimaCR;
	}
	public void setFhUltimaCR(Date fhUltimaCR) {
		this.fhUltimaCR = fhUltimaCR;
	}
	public String getTipoFuenteCR() {
		return tipoFuenteCR;
	}
	public void setTipoFuenteCR(String tipoFuenteCR) {
		this.tipoFuenteCR = tipoFuenteCR;
	}
	public String getClasifCteCR() {
		return clasifCteCR;
	}
	public void setClasifCteCR(String clasifCteCR) {
		this.clasifCteCR = clasifCteCR;
	}
	public String getNbOficinaCR() {
		return nbOficinaCR;
	}
	public void setNbOficinaCR(String nbOficinaCR) {
		this.nbOficinaCR = nbOficinaCR;
	}
	public String getNbRiesgoCR() {
		return nbRiesgoCR;
	}
	public void setNbRiesgoCR(String nbRiesgoCR) {
		this.nbRiesgoCR = nbRiesgoCR;
	}
	public String getNbClienteCR2() {
		return nbClienteCR2;
	}
	public void setNbClienteCR2(String nbClienteCR2) {
		this.nbClienteCR2 = nbClienteCR2;
	}
	public String getCdClienteCR2() {
		return cdClienteCR2;
	}
	public void setCdClienteCR2(String cdClienteCR2) {
		this.cdClienteCR2 = cdClienteCR2;
	}
	public String getNbCiudadCR2() {
		return nbCiudadCR2;
	}
	public void setNbCiudadCR2(String nbCiudadCR2) {
		this.nbCiudadCR2 = nbCiudadCR2;
	}
	public String getNbDireccionCR2() {
		return nbDireccionCR2;
	}
	public void setNbDireccionCR2(String nbDireccionCR2) {
		this.nbDireccionCR2 = nbDireccionCR2;
	}
	public String getCdRfcCR2() {
		return cdRfcCR2;
	}
	public void setCdRfcCR2(String cdRfcCR2) {
		this.cdRfcCR2 = cdRfcCR2;
	}
	public String getNbTelParCR2() {
		return nbTelParCR2;
	}
	public void setNbTelParCR2(String nbTelParCR2) {
		this.nbTelParCR2 = nbTelParCR2;
	}
	public String getNbTelOfiCR2() {
		return nbTelOfiCR2;
	}
	public void setNbTelOfiCR2(String nbTelOfiCR2) {
		this.nbTelOfiCR2 = nbTelOfiCR2;
	}
	public String getNbNacCR2() {
		return nbNacCR2;
	}
	public void setNbNacCR2(String nbNacCR2) {
		this.nbNacCR2 = nbNacCR2;
	}
	public Date getFhAltaCR2() {
		return fhAltaCR2;
	}
	public void setFhAltaCR2(Date fhAltaCR2) {
		this.fhAltaCR2 = fhAltaCR2;
	}
	public String getCdSectorCR2() {
		return cdSectorCR2;
	}
	public void setCdSectorCR2(String cdSectorCR2) {
		this.cdSectorCR2 = cdSectorCR2;
	}
	public String getFhAntNegoCR2() {
		return fhAntNegoCR2;
	}
	public void setFhAntNegoCR2(String fhAntNegoCR2) {
		this.fhAntNegoCR2 = fhAntNegoCR2;
	}
	public Date getFechaActualCR2() {
		return fechaActualCR2;
	}
	public void setFechaActualCR2(Date fechaActualCR2) {
		this.fechaActualCR2 = fechaActualCR2;
	}
	public String getNbActividadBanxicoCR2() {
		return nbActividadBanxicoCR2;
	}
	public void setNbActividadBanxicoCR2(String nbActividadBanxicoCR2) {
		this.nbActividadBanxicoCR2 = nbActividadBanxicoCR2;
	}
	public String getNbCurpCR2() {
		return nbCurpCR2;
	}
	public void setNbCurpCR2(String nbCurpCR2) {
		this.nbCurpCR2 = nbCurpCR2;
	}
	public String getNbPaisNacimientoCR2() {
		return nbPaisNacimientoCR2;
	}
	public void setNbPaisNacimientoCR2(String nbPaisNacimientoCR2) {
		this.nbPaisNacimientoCR2 = nbPaisNacimientoCR2;
	}
	public String getNbPaisRecidenciaCR2() {
		return nbPaisRecidenciaCR2;
	}
	public void setNbPaisRecidenciaCR2(String nbPaisRecidenciaCR2) {
		this.nbPaisRecidenciaCR2 = nbPaisRecidenciaCR2;
	}
	public Date getFhAperturaCR2() {
		return fhAperturaCR2;
	}
	public void setFhAperturaCR2(Date fhAperturaCR2) {
		this.fhAperturaCR2 = fhAperturaCR2;
	}
	public Date getFhUltimaCR2() {
		return fhUltimaCR2;
	}
	public void setFhUltimaCR2(Date fhUltimaCR2) {
		this.fhUltimaCR2 = fhUltimaCR2;
	}
	public String getTipoFuenteCR2() {
		return tipoFuenteCR2;
	}
	public void setTipoFuenteCR2(String tipoFuenteCR2) {
		this.tipoFuenteCR2 = tipoFuenteCR2;
	}
	public String getClasifCteCR2() {
		return clasifCteCR2;
	}
	public void setClasifCteCR2(String clasifCteCR2) {
		this.clasifCteCR2 = clasifCteCR2;
	}
	public String getNbOficinaCR2() {
		return nbOficinaCR2;
	}
	public void setNbOficinaCR2(String nbOficinaCR2) {
		this.nbOficinaCR2 = nbOficinaCR2;
	}
	public String getNbRiesgoCR2() {
		return nbRiesgoCR2;
	}
	public void setNbRiesgoCR2(String nbRiesgoCR2) {
		this.nbRiesgoCR2 = nbRiesgoCR2;
	}

	


}
