package com.bbva.msca.back.dto;

import java.util.Date;

public class ParametrosBusquedaDTO {
	private Integer nuCaso;
	private Integer nuFolio;
	private Integer nuCliente;
	private String nbCliente;
	private Integer nuCuenta;
	private Integer cdEscenario;
	private Integer cdSesion;
	private Integer cdSegmento;
	private Integer cdTipologia;
	private Integer cdRespuesta;
	private Integer cdCalificacion;
	private double nuCalificacion;
	private Integer cdPtsMatriz;
	private Integer nuPtsMatriz;
	private Integer cdConsultor;
	private Integer cdDictamenP;
	private Integer cdDictamenF;
	private Integer cdImporte;
	private Integer imImporte;
	private Date    fhAlertaDel;
	private Date    fhAlertaAl;
	private Date    fhEnvioDel;
	private Date    fhEnvioAl;
	private Date    fhVencRevisionDel;
	private Date    fhVencRevisionAl;
	private Date    fhRevisionDel;
	private Date    fhRevisionAl;
	private Date    fhVencLiberacionDel;
	private Date    fhVencLiberacionAl;
	private Date    fhLiberacionDel;
	private Date    fhLiberacionAl;
	private Date    fhVencReporteDel;
	private Date    fhVencReporteAl;
	private Date    fhAsignacionDel;
	private Date    fhAsignacionAl;
	private Integer cdTPersona;
	
	
	
	
	public Integer getCdEscenario() {
		return cdEscenario;
	}
	public void setCdEscenario(Integer cdEscenario) {
		this.cdEscenario = cdEscenario;
	}
	public Integer getCdSesion() {
		return cdSesion;
	}
	public void setCdSesion(Integer cdSesion) {
		this.cdSesion = cdSesion;
	}
	public Integer getCdSegmento() {
		return cdSegmento;
	}
	public void setCdSegmento(Integer cdSegmento) {
		this.cdSegmento = cdSegmento;
	}
	public Integer getCdTipologia() {
		return cdTipologia;
	}
	public void setCdTipologia(Integer cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	public Integer getCdRespuesta() {
		return cdRespuesta;
	}
	public void setCdRespuesta(Integer cdRespuesta) {
		this.cdRespuesta = cdRespuesta;
	}
	public Integer getCdCalificacion() {
		return cdCalificacion;
	}

	public void setCdCalificacion(Integer cdCalificacion) {
		this.cdCalificacion = cdCalificacion;
	}
	public Integer getCdPtsMatriz() {
		return cdPtsMatriz;
	}
	public void setCdPtsMatriz(Integer cdPtsMatriz) {
		this.cdPtsMatriz = cdPtsMatriz;
	}
	public Integer getNuPtsMatriz() {
		return nuPtsMatriz;
	}
	public void setNuPtsMatriz(Integer nuPtsMatriz) {
		this.nuPtsMatriz = nuPtsMatriz;
	}
	public Integer getCdConsultor() {
		return cdConsultor;
	}
	public void setCdConsultor(Integer cdConsultor) {
		this.cdConsultor = cdConsultor;
	}
	public Integer getCdDictamenP() {
		return cdDictamenP;
	}
	public void setCdDictamenP(Integer cdDictamenP) {
		this.cdDictamenP = cdDictamenP;
	}
	public Integer getCdDictamenF() {
		return cdDictamenF;
	}
	public void setCdDictamenF(Integer cdDictamenF) {
		this.cdDictamenF = cdDictamenF;
	}
	public Integer getCdImporte() {
		return cdImporte;
	}
	public void setCdImporte(Integer cdImporte) {
		this.cdImporte = cdImporte;
	}
	public Integer getImImporte() {
		return imImporte;
	}
	public void setImImporte(Integer imImporte) {
		this.imImporte = imImporte;
	}
	public Date getFhAlertaDel() {
		return fhAlertaDel;
	}
	public void setFhAlertaDel(Date fhAlertaDel) {
		this.fhAlertaDel = fhAlertaDel;
	}
	public Date getFhAlertaAl() {
		return fhAlertaAl;
	}
	public void setFhAlertaAl(Date fhAlertaAl) {
		this.fhAlertaAl = fhAlertaAl;
	}
	public Date getFhEnvioDel() {
		return fhEnvioDel;
	}
	public void setFhEnvioDel(Date fhEnvioDel) {
		this.fhEnvioDel = fhEnvioDel;
	}
	public Date getFhEnvioAl() {
		return fhEnvioAl;
	}
	public void setFhEnvioAl(Date fhEnvioAl) {
		this.fhEnvioAl = fhEnvioAl;
	}
	public Date getFhVencRevisionDel() {
		return fhVencRevisionDel;
	}
	public void setFhVencRevisionDel(Date fhVencRevisionDel) {
		this.fhVencRevisionDel = fhVencRevisionDel;
	}
	public Date getFhVencRevisionAl() {
		return fhVencRevisionAl;
	}
	public void setFhVencRevisionAl(Date fhVencRevisionAl) {
		this.fhVencRevisionAl = fhVencRevisionAl;
	}
	public Date getFhRevisionDel() {
		return fhRevisionDel;
	}
	public void setFhRevisionDel(Date fhRevisionDel) {
		this.fhRevisionDel = fhRevisionDel;
	}
	public Date getFhRevisionAl() {
		return fhRevisionAl;
	}
	public void setFhRevisionAl(Date fhRevisionAl) {
		this.fhRevisionAl = fhRevisionAl;
	}
	public Date getFhVencLiberacionDel() {
		return fhVencLiberacionDel;
	}
	public void setFhVencLiberacionDel(Date fhVencLiberacionDel) {
		this.fhVencLiberacionDel = fhVencLiberacionDel;
	}
	public Date getFhVencLiberacionAl() {
		return fhVencLiberacionAl;
	}
	public void setFhVencLiberacionAl(Date fhVencLiberacionAl) {
		this.fhVencLiberacionAl = fhVencLiberacionAl;
	}
	public Date getFhLiberacionDel() {
		return fhLiberacionDel;
	}
	public void setFhLiberacionDel(Date fhLiberacionDel) {
		this.fhLiberacionDel = fhLiberacionDel;
	}
	public Date getFhLiberacionAl() {
		return fhLiberacionAl;
	}
	public void setFhLiberacionAl(Date fhLiberacionAl) {
		this.fhLiberacionAl = fhLiberacionAl;
	}
	public Date getFhVencReporteDel() {
		return fhVencReporteDel;
	}
	public void setFhVencReporteDel(Date fhVencReporteDel) {
		this.fhVencReporteDel = fhVencReporteDel;
	}
	public Date getFhVencReporteAl() {
		return fhVencReporteAl;
	}
	public void setFhVencReporteAl(Date fhVencReporteAl) {
		this.fhVencReporteAl = fhVencReporteAl;
	}
	public Date getFhAsignacionDel() {
		return fhAsignacionDel;
	}
	public void setFhAsignacionDel(Date fhAsignacionDel) {
		this.fhAsignacionDel = fhAsignacionDel;
	}
	public Date getFhAsignacionAl() {
		return fhAsignacionAl;
	}
	public void setFhAsignacionAl(Date fhAsignacionAl) {
		this.fhAsignacionAl = fhAsignacionAl;
	}
	public Integer getCdTPersona() {
		return cdTPersona;
	}
	public void setCdTPersona(Integer cdTPersona) {
		this.cdTPersona = cdTPersona;
	}
	public Integer getNuCaso() {
		return nuCaso;
	}
	public void setNuCaso(Integer nuCaso) {
		this.nuCaso = nuCaso;
	}
	public Integer getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(Integer nuFolio) {
		this.nuFolio = nuFolio;
	}
	public Integer getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(Integer nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public Integer getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(Integer nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public double getNuCalificacion() {
		return nuCalificacion;
	}
	public void setNuCalificacion(double nuCalificacion) {
		this.nuCalificacion = nuCalificacion;
	}
	
	
	
}



