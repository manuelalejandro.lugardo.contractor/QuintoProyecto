package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class CasosPendientesBusquedaDTO {
	
	private BigDecimal nuCaso;
	private BigDecimal nuFolio;
	private BigDecimal nuCliente;
	private String nbCliente;
	private BigDecimal nuCuenta;
	private BigDecimal cdTPersona;
	private BigDecimal cdTipologia;
	private BigDecimal cdSesion;
	private BigDecimal cdSegmento;
	private BigDecimal cdRespuesta;
	private BigDecimal cdEscenario;
	private BigDecimal cdCalificacion;
	private BigDecimal nuCalificacion;
	private BigDecimal cdPtsMatriz;
	private BigDecimal nuPtsMatriz;
	private BigDecimal cdImporte;
	private Date fhAlertaDel;
	private Date fhAlertaAl;
	private Date fhEnvioDel;
	private Date fhEnvioAl;
	private Date fhVencRevisionDel;
	private Date fhVencRevisionAl;
	private Date fhVencLiberacionDel;
	private Date fhVencLiberacionAl;
	private Date fhVencReporteDel;
	private Date fhVencReporteAl;
	private Date fhAsignacionDel;
	private Date fhAsignacionAl;
	
	
	public BigDecimal getNuCaso() {
		return nuCaso;
	}
	public void setNuCaso(BigDecimal nuCaso) {
		this.nuCaso = nuCaso;
	}
	public BigDecimal getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(BigDecimal nuFolio) {
		this.nuFolio = nuFolio;
	}
	public BigDecimal getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(BigDecimal nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public BigDecimal getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(BigDecimal nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public BigDecimal getCdTPersona() {
		return cdTPersona;
	}
	public void setCdTPersona(BigDecimal cdTPersona) {
		this.cdTPersona = cdTPersona;
	}
	public BigDecimal getCdTipologia() {
		return cdTipologia;
	}
	public void setCdTipologia(BigDecimal cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	public BigDecimal getCdSesion() {
		return cdSesion;
	}
	public void setCdSesion(BigDecimal cdSesion) {
		this.cdSesion = cdSesion;
	}
	public BigDecimal getCdSegmento() {
		return cdSegmento;
	}
	public void setCdSegmento(BigDecimal cdSegmento) {
		this.cdSegmento = cdSegmento;
	}
	public BigDecimal getCdRespuesta() {
		return cdRespuesta;
	}
	public void setCdRespuesta(BigDecimal cdRespuesta) {
		this.cdRespuesta = cdRespuesta;
	}
	public BigDecimal getCdEscenario() {
		return cdEscenario;
	}
	public void setCdEscenario(BigDecimal cdEscenario) {
		this.cdEscenario = cdEscenario;
	}
	public BigDecimal getCdCalificacion() {
		return cdCalificacion;
	}
	public void setCdCalificacion(BigDecimal cdCalificacion) {
		this.cdCalificacion = cdCalificacion;
	}
	public BigDecimal getNuCalificacion() {
		return nuCalificacion;
	}
	public void setNuCalificacion(BigDecimal nuCalificacion) {
		this.nuCalificacion = nuCalificacion;
	}
	public BigDecimal getCdPtsMatriz() {
		return cdPtsMatriz;
	}
	public void setCdPtsMatriz(BigDecimal cdPtsMatriz) {
		this.cdPtsMatriz = cdPtsMatriz;
	}
	public BigDecimal getNuPtsMatriz() {
		return nuPtsMatriz;
	}
	public void setNuPtsMatriz(BigDecimal nuPtsMatriz) {
		this.nuPtsMatriz = nuPtsMatriz;
	}
	public BigDecimal getCdImporte() {
		return cdImporte;
	}
	public void setCdImporte(BigDecimal cdImporte) {
		this.cdImporte = cdImporte;
	}
	public Date getFhAlertaDel() {
		return fhAlertaDel;
	}
	public void setFhAlertaDel(Date fhAlertaDel) {
		this.fhAlertaDel = fhAlertaDel;
	}
	public Date getFhAlertaAl() {
		return fhAlertaAl;
	}
	public void setFhAlertaAl(Date fhAlertaAl) {
		this.fhAlertaAl = fhAlertaAl;
	}
	public Date getFhEnvioDel() {
		return fhEnvioDel;
	}
	public void setFhEnvioDel(Date fhEnvioDel) {
		this.fhEnvioDel = fhEnvioDel;
	}
	public Date getFhEnvioAl() {
		return fhEnvioAl;
	}
	public void setFhEnvioAl(Date fhEnvioAl) {
		this.fhEnvioAl = fhEnvioAl;
	}
	public Date getFhVencRevisionDel() {
		return fhVencRevisionDel;
	}
	public void setFhVencRevisionDel(Date fhVencRevisionDel) {
		this.fhVencRevisionDel = fhVencRevisionDel;
	}
	public Date getFhVencRevisionAl() {
		return fhVencRevisionAl;
	}
	public void setFhVencRevisionAl(Date fhVencRevisionAl) {
		this.fhVencRevisionAl = fhVencRevisionAl;
	}
	public Date getFhVencLiberacionDel() {
		return fhVencLiberacionDel;
	}
	public void setFhVencLiberacionDel(Date fhVencLiberacionDel) {
		this.fhVencLiberacionDel = fhVencLiberacionDel;
	}
	public Date getFhVencLiberacionAl() {
		return fhVencLiberacionAl;
	}
	public void setFhVencLiberacionAl(Date fhVencLiberacionAl) {
		this.fhVencLiberacionAl = fhVencLiberacionAl;
	}
	public Date getFhVencReporteDel() {
		return fhVencReporteDel;
	}
	public void setFhVencReporteDel(Date fhVencReporteDel) {
		this.fhVencReporteDel = fhVencReporteDel;
	}
	public Date getFhVencReporteAl() {
		return fhVencReporteAl;
	}
	public void setFhVencReporteAl(Date fhVencReporteAl) {
		this.fhVencReporteAl = fhVencReporteAl;
	}
	public Date getFhAsignacionDel() {
		return fhAsignacionDel;
	}
	public void setFhAsignacionDel(Date fhAsignacionDel) {
		this.fhAsignacionDel = fhAsignacionDel;
	}
	public Date getFhAsignacionAl() {
		return fhAsignacionAl;
	}
	public void setFhAsignacionAl(Date fhAsignacionAl) {
		this.fhAsignacionAl = fhAsignacionAl;
	}
	public BigDecimal getImImporte() {
		return imImporte;
	}
	public void setImImporte(BigDecimal imImporte) {
		this.imImporte = imImporte;
	}
	private BigDecimal imImporte;

}
