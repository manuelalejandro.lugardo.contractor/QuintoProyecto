package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class TipologiaDTO {

	private String cdTipologia;
	private String nbTipologia;
	private BigDecimal nuUmbralMC;
	private BigDecimal cdGerencia;
	private Date fhAlta;
	private String cdUsrAlta;
	private String cdStSistema;
	private String nbUsrAlta;
	private String tpCuestionario;
	private String cdTpTipologia;
	private String nbGerencia;

	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}

	public String getCdTipologia() {
		return cdTipologia;
	}

	public void setNbTipologia(String nbTipologia) {
		this.nbTipologia = nbTipologia;
	}

	public String getNbTipologia() {
		return nbTipologia;
	}

	public void setNuUmbralMC(BigDecimal nuUmbralCM) {
		this.nuUmbralMC = nuUmbralCM;
	}

	public BigDecimal getNuUmbralMC() {
		return nuUmbralMC;
	}

	public void setCdGerencia(BigDecimal cdGerencia) {
		this.cdGerencia = cdGerencia;
	}

	public BigDecimal getCdGerencia() {
		return cdGerencia;
	}

	public void setFhAlta(Date fhAlta) {
		this.fhAlta = fhAlta;
	}

	public Date getFhAlta() {
		return fhAlta;
	}

	public void setCdUsrAlta(String cdUsrAlta) {
		this.cdUsrAlta = cdUsrAlta;
	}

	public String getCdUsrAlta() {
		return cdUsrAlta;
	}

	public void setCdStSistema(String cdStSistema) {
		this.cdStSistema = cdStSistema;
	}

	public String getCdStSistema() {
		return cdStSistema;
	}

	public void setNbUsrAlta(String nbUsrAlta) {
		this.nbUsrAlta = nbUsrAlta;
	}

	public String getNbUsrAlta() {
		return nbUsrAlta;
	}

	public void setNbGerencia(String nbGerencia) {
		this.nbGerencia = nbGerencia;
	}

	public String getNbGerencia() {
		return nbGerencia;
	}

	public void setTpCuestionario(String tpCuestionario) {
		this.tpCuestionario = tpCuestionario;
	}

	public String getTpCuestionario() {
		return tpCuestionario;
	}

	public void setCdTpTipologia(String cdTpTipologia) {
		this.cdTpTipologia = cdTpTipologia;
	}

	public String getCdTpTipologia() {
		return cdTpTipologia;
	}

}
