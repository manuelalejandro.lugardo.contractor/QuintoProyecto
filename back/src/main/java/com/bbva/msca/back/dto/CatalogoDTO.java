package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class CatalogoDTO {
	
	private String cdDetCatalogo;
	private BigDecimal cdCatalogo;
	private String nbValor;
	private String nbDescripcion;
	
	public String getCdDetCatalogo() {
		return cdDetCatalogo;
	}
	public void setCdDetCatalogo(String cdDetCatalogo) {
		this.cdDetCatalogo = cdDetCatalogo;
	}
	public BigDecimal getCdCatalogo() {
		return cdCatalogo;
	}
	public void setCdCatalogo(BigDecimal cdCatalogo) {
		this.cdCatalogo = cdCatalogo;
	}
	public String getNbValor() {
		return nbValor;
	}
	public void setNbValor(String nbValor) {
		this.nbValor = nbValor;
	}
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	
	

}
