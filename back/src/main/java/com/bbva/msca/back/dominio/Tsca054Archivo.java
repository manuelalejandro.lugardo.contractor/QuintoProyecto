package com.bbva.msca.back.dominio;

import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(versionField = "", table = "TSCA054_ARCHIVO", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca054Archivo {
	
	@Id
	@NotNull
	@SequenceGenerator(name = "SQ_TSCA054", sequenceName = "SQ_TSCA054", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TSCA054")
	@Column(name = "CD_FOLIO_ARCHIVO")
	private BigDecimal cdFolioArchivo; 
	
	@Column(name = "CD_USUARIO")
	private String cdUsuario;
	
	@Column(name = "CD_USUARIO_MDF")
	private String cdUsuarioMdf;

}
