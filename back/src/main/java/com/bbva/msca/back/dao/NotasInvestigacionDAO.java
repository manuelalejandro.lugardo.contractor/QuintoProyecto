package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;

import com.bbva.msca.back.dto.NotasInvestigacionDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class NotasInvestigacionDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	/*public List<NotasInvestigacionDTO> getNotasInvestigacion(){
		
		 List<NotasInvestigacionDTO> lstNotasInvestigacion= new ArrayList<NotasInvestigacionDTO>();
		
		 NotasInvestigacionDTO notasInvestigacionDTO = null;
		
			for(int i=0;i<10;i++){
			
				notasInvestigacionDTO = new NotasInvestigacionDTO();
				
				notasInvestigacionDTO.setNbArchivo("");
				notasInvestigacionDTO.setNbUsuCreacion("");
				notasInvestigacionDTO.setFhCreacion(new Date());
				notasInvestigacionDTO.setNbUsuModificacion("");
				notasInvestigacionDTO.setFhModificacion(new Date());
				notasInvestigacionDTO.setTxNotaInvestigacion("");
				
				lstNotasInvestigacion.add(notasInvestigacionDTO);
			
			}
			return lstNotasInvestigacion;
		}
	*/
	@SuppressWarnings("unchecked")
	public List<NotasInvestigacionDTO> getNotasInvestigacion(BigDecimal cdCaso) throws Exception{ 
		
		 List<NotasInvestigacionDTO> lstNotasInvestigacion= new ArrayList<NotasInvestigacionDTO>();
	 
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getNotasInvestigacion")); 
			 queryCR.setParameter(1,cdCaso.toString());
			 	 queryCR.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdFolioArchivo")
				.addScalar("cdExpediente")
				.addScalar("nbTitulo")
				.addScalar("nbTitulo")
				.addScalar("cdUsuario")
				.addScalar("fhCreacion")
				.addScalar("cdUsuarioMdf")
				.addScalar("fhModif")
				.addScalar("txInvestigacion")
				
 			.setResultTransformer(Transformers.aliasToBean(NotasInvestigacionDTO.class));
			
			 	lstNotasInvestigacion = (List<NotasInvestigacionDTO>)queryCR.getResultList();

		return lstNotasInvestigacion;
		
} 
	 
}
