// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import java.lang.String;

privileged aspect Tsca048TpMovto_Roo_ToString {
    
    public String Tsca048TpMovto.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("NbMovto: ").append(getNbMovto()).append(", ");
        sb.append("TpMovto: ").append(getTpMovto());
        return sb.toString();
    }
    
}
