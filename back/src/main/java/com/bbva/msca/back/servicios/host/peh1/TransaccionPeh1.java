package com.bbva.msca.back.servicios.host.peh1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.auditoria.Auditable;
import com.bbva.jee.arq.spring.core.auditoria.InformacionAuditable;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.TriggersRemove;

/**
 * Invocador de la transacci&oacute;n <code>PEH1</code>
 * 
 * @see PeticionTransaccionPeh1
 * @see RespuestaTransaccionPeh1
 *  
 * @author Arquitectura Spring BBVA
 */
@Component
public class TransaccionPeh1 implements InvocadorTransaccion<PeticionTransaccionPeh1,RespuestaTransaccionPeh1> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	@Auditable({InformacionAuditable.ARGUMENTOS, InformacionAuditable.RESULTADO, InformacionAuditable.DURACION})
	public RespuestaTransaccionPeh1 invocar(PeticionTransaccionPeh1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeh1.class, RespuestaTransaccionPeh1.class, transaccion);
	}
	
	@Override
	@Auditable({InformacionAuditable.ARGUMENTOS, InformacionAuditable.RESULTADO, InformacionAuditable.DURACION})
	@Cacheable(cacheName = "cacheTransaccionPeh1")
	public RespuestaTransaccionPeh1 invocarCache(PeticionTransaccionPeh1 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeh1.class, RespuestaTransaccionPeh1.class, transaccion);
	}
	
	@Override
	@Auditable(InformacionAuditable.DURACION)
	@TriggersRemove(cacheName = "cacheTransaccionPeh1", removeAll = true)
	public void vaciarCache() {}
		
}