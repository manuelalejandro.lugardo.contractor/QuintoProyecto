package com.bbva.msca.back.dao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.dominio.Tsca002SegPld;
import com.bbva.msca.back.dominio.Tsca007Tipologia;
import com.bbva.msca.back.dominio.Tsca013Alerta;
import com.bbva.msca.back.dominio.Tsca013AlertaPK;
import com.bbva.msca.back.dominio.Tsca014Caso;
import com.bbva.msca.back.dominio.Tsca016Cliente;
import com.bbva.msca.back.dominio.Tsca017Cuenta;
import com.bbva.msca.back.dominio.Tsca017CuentaPK;
import com.bbva.msca.back.dominio.Tsca023Sesion;
import com.bbva.msca.back.dominio.Tsca043TpPersona;
import com.bbva.msca.back.dominio.Tsca050Clialtrie;
import com.bbva.msca.back.dominio.Tsca052Clialtriedet;
import com.bbva.msca.back.dominio.Tsca052ClialtriedetPK;
import com.bbva.msca.back.dominio.Tsca053Reltipfte;
import com.bbva.msca.back.dominio.Tsca054Archivo;
//import com.bbva.msca.back.dominio.Tsca053ReltipftePri;
//import com.bbva.msca.back.dominio.Tsca054FolioArchivo;
import com.bbva.msca.back.dominio.Tsca066Checklist;
import com.bbva.msca.back.dto.ArchivoDTO;
import com.bbva.msca.back.dto.ChecklistDTO;
import com.bbva.msca.back.dto.CuentasClienteRiesgoDTO;
import com.bbva.msca.back.dto.CuentasClienteRiesgoDetDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class CuentasClienteRiesgoDAO {
	
	private final String PREFIJO_FOLIO = "T050";
	private final String PREFIJO_SEPARADOR = "-";
	private final String FORMATO_FECHA = "dd/MM/yyyy";
	private final String CD_TP_PERSONA_FISICA = " F";
	private final String CD_TP_PERSONA_MORAL = " M";
	private final String CD_TP_PERSONA_GOBIERNO = " G";
	private final String CD_TIENE_ASEGURIAMIENTO_SI = "36";
	private final String CD_TIENE_ASEGURIAMIENTO_NO = "37";
	private final int TAMANIO_MAXIMO_SOL_ESPEC = 5000;
	private final int rangoInicial = 0;
	private final int maxNombre = 20;
	private final int maxNombreAPaterno = 40;
	private final int maxNombreAPatMat = 60;
	private final int rangoXA = 0;
	private final int rangoXB = 19;
	private final int rangoYA = 20;
	private final int rangoYB = 39;
	private final int rangoZA = 40;
	private final int rangoZB = 59;	

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	@Transactional
	public CuentasClienteRiesgoDTO registraClienteAltoRiesgo(
			CuentasClienteRiesgoDTO requerimiento) throws Exception {
		
		Tsca050Clialtrie nuevoRequerimiento = new Tsca050Clialtrie();
		nuevoRequerimiento.setFhRecepcion(requerimiento.getFhRecepcion());
		nuevoRequerimiento.setCdOficio(requerimiento.getNuOficio());
		nuevoRequerimiento.setCdExpediente(requerimiento.getNuExpediente());
		nuevoRequerimiento.setNuFolio(requerimiento.getNuFolio());
		nuevoRequerimiento.setNbRazon( requerimiento.getNbDescripcion() );
		nuevoRequerimiento.setFhPublicacion(requerimiento.getFhPublicacion());
		nuevoRequerimiento.setNuDiasPlazo(requerimiento.getNuDiasPlazo());
		nuevoRequerimiento.setCdNumtipftePri( requerimiento.getNbTipo() );
		nuevoRequerimiento.setNbReferencia(requerimiento.getNbReferencia());		
		nuevoRequerimiento.setCdTieneAseg( requerimiento.getNbAseguramiento() );
		System.out.println( requerimiento.getNbAseguramiento() );
		
		if( requerimiento.getNbSolicitudEspecifica() != null && !requerimiento.getNbSolicitudEspecifica().equals( "" )){
			if( requerimiento.getNbSolicitudEspecifica().length() <= 3000 ){
				System.out.println( "Se guarda en un campo" );
				int tamanio = requerimiento.getNbSolicitudEspecifica().length();
				System.out.println( "Tama�o palabra : " + tamanio );
				String cadena = requerimiento.getNbSolicitudEspecifica().substring( 0 , tamanio - 1 );
				nuevoRequerimiento.setNbSolicitudEspec( cadena );
			}else {
				System.out.println( "Se guarda en dos campos" );
				int tamanio = requerimiento.getNbSolicitudEspecifica().length();
				System.out.println( "Tama�o palabra : " + tamanio );
				String cadena = requerimiento.getNbSolicitudEspecifica().substring( 0 , 2999 );			
				nuevoRequerimiento.setNbSolicitudEspec( cadena );
				String cadena2 = requerimiento.getNbSolicitudEspecifica().substring( 3000 , tamanio - 1 );
				nuevoRequerimiento.setNbSolicitudEspec2( cadena2 );
			}			
		}

		// Generaci�n del Folio de Archivo
		StringBuilder str = new StringBuilder();
		str.append(PREFIJO_FOLIO);
		str.append(PREFIJO_SEPARADOR);
		Date fecha = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		str.append(cal.get(Calendar.YEAR));
		str.append(cal.get(Calendar.MONTH));
		str.append(cal.get(Calendar.DAY_OF_MONTH));
		str.append(cal.get(Calendar.HOUR_OF_DAY));
		str.append(cal.get(Calendar.MINUTE));
		str.append(cal.get(Calendar.SECOND));
		nuevoRequerimiento.setNuFolioArchivo(str.toString());
		nuevoRequerimiento.setNbAutoridad(requerimiento.getNbAutoridad());
		nuevoRequerimiento.persist();
		nuevoRequerimiento.flush();

		requerimiento.setCdClialtrie(nuevoRequerimiento
				.getCdClialtrie());
		requerimiento.setNuFolioArchivo(nuevoRequerimiento.getNuFolioArchivo());

		return requerimiento;
	}
	
	@Transactional
	public CuentasClienteRiesgoDTO editarClienteAltoRiesgo(
			CuentasClienteRiesgoDTO requerimiento) throws Exception {
		
		Tsca050Clialtrie expediente = Tsca050Clialtrie.findTsca050Clialtrie( requerimiento.getCdClialtrie() );		
		expediente.setFhRecepcion(requerimiento.getFhRecepcion());
		expediente.setCdOficio(requerimiento.getNuOficio());
		expediente.setCdExpediente(requerimiento.getNuExpediente());
		expediente.setNuFolio(requerimiento.getNuFolio());
		expediente.setNbRazon( requerimiento.getNbDescripcion() );
		expediente.setFhPublicacion(requerimiento.getFhPublicacion());
		expediente.setNuDiasPlazo(requerimiento.getNuDiasPlazo());
		expediente.setCdNumtipftePri( requerimiento.getNbTipo() );
		expediente.setNbReferencia(requerimiento.getNbReferencia());		
		expediente.setCdTieneAseg( requerimiento.getNbAseguramiento() );
		
		if( requerimiento.getNbSolicitudEspecifica() != null && !requerimiento.getNbSolicitudEspecifica().equals( "" )){
			if( requerimiento.getNbSolicitudEspecifica().length() <= 3000 ){
				System.out.println( "Se guarda en un campo" );
				int tamanio = requerimiento.getNbSolicitudEspecifica().length();
				System.out.println( "Tama�o palabra : " + tamanio );
				String cadena = requerimiento.getNbSolicitudEspecifica().substring( 0 , tamanio - 1 );
				expediente.setNbSolicitudEspec( cadena );
			}else {
				System.out.println( "Se guarda en dos campos" );
				int tamanio = requerimiento.getNbSolicitudEspecifica().length();
				System.out.println( "Tama�o palabra : " + tamanio );
				String cadena = requerimiento.getNbSolicitudEspecifica().substring( 0 , 2999 );			
				expediente.setNbSolicitudEspec( cadena );
				String cadena2 = requerimiento.getNbSolicitudEspecifica().substring( 3000 , tamanio - 1 );
				expediente.setNbSolicitudEspec2( cadena2 );
			}			
		}

		// Generaci�n del Folio de Archivo
		expediente.setNbAutoridad(requerimiento.getNbAutoridad());
		//expediente.setCdClialtrie(requerimiento.getCdClialtrie());
		//expediente.setNuFolioArchivo(requerimiento.getNuFolioArchivo());		
		expediente.merge();
		expediente.flush();

		return requerimiento;
	}	
	
	@SuppressWarnings("unchecked")
	public CuentasClienteRiesgoDTO consultarRequerimiento(BigDecimal cdRequerimiento)
			throws Exception {
		CuentasClienteRiesgoDTO requerimiento = null;
		List<CuentasClienteRiesgoDTO> lstResultado = new ArrayList<CuentasClienteRiesgoDTO>();
		Query query = em.createNativeQuery(Consultas
				.getConsulta("getRequerimientoEdicion"));
		query.setParameter(1, cdRequerimiento );
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdClialtrie")
				.addScalar("fhRecepcion")
				.addScalar("nuOficio")
				.addScalar("nuExpediente")
				.addScalar("nuFolio")
				.addScalar("nbDescripcion")
				.addScalar("fhPublicacion")
				.addScalar("nuDiasPlazo")
				.addScalar("nbTipo")
				.addScalar("nbReferencia")
				.addScalar("nbAseguramiento")
				.addScalar("nbSolicitudEspecifica")
				.addScalar("nbSolicitudEspecifica2")
				.addScalar("nbAutoridad")
				.addScalar("nuFolioArchivo")
				.setResultTransformer(
						Transformers.aliasToBean(CuentasClienteRiesgoDTO.class));
		lstResultado = (List<CuentasClienteRiesgoDTO>) query.getResultList();
		if( lstResultado != null && !lstResultado.isEmpty() ){
			requerimiento = lstResultado.get( 0 );
		}
		return requerimiento;
	}	

	@Transactional
	public CuentasClienteRiesgoDTO registraClienteAltoRiesgoTransac(
			CuentasClienteRiesgoDTO requerimiento, BigDecimal cdTipoAlta, String cdUsuario) throws Exception {
		Tsca050Clialtrie nuevoRequerimiento = new Tsca050Clialtrie();
		nuevoRequerimiento.setFhRecepcion(requerimiento.getFhRecepcion());
		nuevoRequerimiento.setCdOficio(requerimiento.getNuOficio());
		nuevoRequerimiento.setCdExpediente(requerimiento.getNuExpediente());
		nuevoRequerimiento.setNuFolio(requerimiento.getNuFolio());
		nuevoRequerimiento.setNbRazon( requerimiento.getNbDescripcion() );
		nuevoRequerimiento.setFhPublicacion(requerimiento.getFhPublicacion());
		nuevoRequerimiento.setNuDiasPlazo(requerimiento.getNuDiasPlazo());
		nuevoRequerimiento.setCdNumtipftePri( requerimiento.getNbTipo() );
		nuevoRequerimiento.setNbReferencia(requerimiento.getNbReferencia());
		nuevoRequerimiento.setCdTieneAseg(requerimiento.getNbAseguramiento());
		System.out.println( requerimiento.getNbAseguramiento() );
		if( requerimiento.getNbSolicitudEspecifica() != null && !requerimiento.getNbSolicitudEspecifica().equals( "" )){
			if( requerimiento.getNbSolicitudEspecifica().length() <= 3000 ){
				System.out.println( "Se guarda en un campo" );
				int tamanio = requerimiento.getNbSolicitudEspecifica().length();
				System.out.println( "Tama�o palabra : " + tamanio );
				String cadena = requerimiento.getNbSolicitudEspecifica().substring( 0 , tamanio - 1 );
				nuevoRequerimiento.setNbSolicitudEspec( cadena );
			}else {
				System.out.println( "Se guarda en dos campos" );
				int tamanio = requerimiento.getNbSolicitudEspecifica().length();
				System.out.println( "Tama�o palabra : " + tamanio );
				String cadena = requerimiento.getNbSolicitudEspecifica().substring( 0 , 2999 );			
				nuevoRequerimiento.setNbSolicitudEspec( cadena );
				String cadena2 = requerimiento.getNbSolicitudEspecifica().substring( 3000 , tamanio - 1 );
				nuevoRequerimiento.setNbSolicitudEspec2( cadena2 );
			}			
		}
		
		
		//nuevoRequerimiento.setNbSolicitudEspec( requerimiento.getNbSolicitudEspecifica());

		// Generaci�n del Folio de Archivo
		StringBuilder str = new StringBuilder();
		str.append(PREFIJO_FOLIO);
		str.append(PREFIJO_SEPARADOR);
		Date fecha = new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(fecha);
		str.append(cal.get(Calendar.YEAR));
		str.append(cal.get(Calendar.MONTH));
		str.append(cal.get(Calendar.DAY_OF_MONTH));
		str.append(cal.get(Calendar.HOUR_OF_DAY));
		str.append(cal.get(Calendar.MINUTE));
		str.append(cal.get(Calendar.SECOND));
		nuevoRequerimiento.setNuFolioArchivo(str.toString());

		nuevoRequerimiento.setNbAutoridad(requerimiento.getNbAutoridad());

		nuevoRequerimiento.persist();
		nuevoRequerimiento.flush();

		requerimiento.setCdClialtrie(nuevoRequerimiento
				.getCdClialtrie());
		requerimiento.setNuFolioArchivo(nuevoRequerimiento.getNuFolioArchivo());

		List<CuentasClienteRiesgoDetDTO> lstPersonasReportadas = requerimiento
				.getLstClientesRiesgo();
		
		if (lstPersonasReportadas != null && !lstPersonasReportadas.isEmpty()) {
			SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_FECHA);
			// Lista de Personas reportadas
			CuentasClienteRiesgoDetDTO personaReportadaDTO;
			for (int i = 0; i < lstPersonasReportadas.size(); i++) {
				personaReportadaDTO = lstPersonasReportadas.get(i);
				
				Tsca052Clialtriedet personaReportada = new Tsca052Clialtriedet();
				Tsca007Tipologia tipologia = Tsca007Tipologia.findTsca007Tipologia(personaReportadaDTO.getCdTipologia());
				personaReportada.setId(new Tsca052ClialtriedetPK(nuevoRequerimiento.getCdClialtrie(),personaReportadaDTO.getNuId()));
				personaReportada.setCdTpPersona( personaReportadaDTO.getCdTpPersona() );
				personaReportada.setCdTipologia(tipologia);
				
				// Se agregan los dem�s datos
				personaReportada.setCdCaracter(personaReportadaDTO.getCdCaracter());	
				
				String nombreTmp = eliminarCaracteresEspeciales( personaReportadaDTO.getNbNombre() );
				String aPaterno = eliminarCaracteresEspeciales( personaReportadaDTO.getNbApPaterno() );
				String aMaterno = eliminarCaracteresEspeciales( personaReportadaDTO.getNbApMaterno() );
				if( personaReportadaDTO.getCdTpPersona().equals( CD_TP_PERSONA_FISICA.trim() ) )
				{
					if( nombreTmp.length() <= 20  ){
						personaReportada.setNbNombre( nombreTmp.substring( rangoXA, nombreTmp.length() - 1 ) );
					}else if( nombreTmp.length() > 20 ){
						personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );	
					}
					if( aPaterno.length() <= 20 ){
						personaReportada.setNbApePaterno( aPaterno.substring( rangoXA, aPaterno.length() - 1 ) );
					}else if( aPaterno.length() > 20 ){
						personaReportada.setNbApePaterno( aPaterno.substring( rangoXA, rangoXB ) );
					}
					if( aMaterno.length() <= 20 ){
						personaReportada.setNbApeMaterno( aMaterno.substring( rangoXA, aMaterno.length() - 1 ) );
					}else if( aMaterno.length() > 20 ){
						personaReportada.setNbApeMaterno( aMaterno.substring( rangoXA, rangoXB ) );
					}
					
					
				}else if( personaReportadaDTO.getCdTpPersona().equals( CD_TP_PERSONA_MORAL.trim() ) || personaReportadaDTO.getCdTpPersona().equals( CD_TP_PERSONA_GOBIERNO.trim() ))
				{					
					if(personaReportadaDTO.getNbNombre().length() > rangoInicial &&  personaReportadaDTO.getNbNombre().length() <= maxNombre )
					{	
						personaReportada.setNbNombre( nombreTmp );
					}
					else if( nombreTmp.length() > maxNombre &&  nombreTmp.length() <= maxNombreAPaterno ){
						personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );						
						personaReportada.setNbApePaterno( nombreTmp.substring( rangoYA, ( nombreTmp.length() -1 ) ) );
					}
					else if( nombreTmp.length() > maxNombreAPaterno && nombreTmp.length() <= maxNombreAPatMat ){
						personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );						
						personaReportada.setNbApePaterno( nombreTmp.substring( rangoYA, rangoYB ) );						
						personaReportada.setNbApeMaterno( nombreTmp.substring( rangoZA, nombreTmp.length() -1 ) );						

					}					
					else if( nombreTmp.length() > maxNombreAPatMat  ){
						personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );												
						personaReportada.setNbApePaterno( nombreTmp.substring( rangoYA, rangoYB ) );						
						personaReportada.setNbApeMaterno( nombreTmp.substring( rangoZA, rangoZB ) );												
					}					
				}
				
				if( personaReportadaDTO.getCdRFC() != null && personaReportadaDTO.getCdRFC().length() > 13 )
				{
					personaReportada.setCdRfc(personaReportadaDTO.getCdRFC().substring( 0, 12 ));
				}
				else{
					personaReportada.setCdRfc(personaReportadaDTO.getCdRFC() );
				}
				String domicilio = eliminarCaracteresEspeciales( personaReportadaDTO.getNbDomicilio() );
				if( personaReportadaDTO.getNbDomicilio() != null && personaReportadaDTO.getNbDomicilio().length() > 20 )
				{
					
					personaReportada.setNbDomicilio( domicilio.substring( 0, 19 ) );
				}
				else{
					personaReportada.setNbDomicilio( domicilio );
				}
				String complementario = eliminarCaracteresEspeciales( personaReportadaDTO.getNbComplementarios() );
				if( complementario != null && complementario.length() > 20)
				{
					personaReportada.setNbComplementario( complementario.substring( 0, 19 ));
				}
				else{
					personaReportada.setNbComplementario( complementario);
				}
				
				personaReportada.setStCteBancomer( personaReportadaDTO.getCdClienteBancomer() );
				
				if (personaReportadaDTO.getFhNacimiento() != null
						&& !personaReportadaDTO.getFhNacimiento().equals("")) {
					personaReportada.setFhNacimiento( personaReportadaDTO.getFhNacimiento() );
				}
				
				personaReportada.setNuCliente( personaReportadaDTO.getCdCliente() ) ;
				personaReportada.setNuCuenta( personaReportadaDTO.getNuCuenta() );				
				personaReportada.persist();
				personaReportada.flush();
				
				if( personaReportada.getCdTipologia() != null ){
					if( personaReportada.getCdTipologia().getCdTipologia() != null && !personaReportada.getCdTipologia().getCdTipologia().equals( "" ) ){
						if( personaReportada.getCdTipologia().getCdTipologia().equals( "L3" ) || personaReportada.getCdTipologia().getCdTipologia().equals( "LD" ) ){
							Tsca066Checklist chkListPR = new Tsca066Checklist();				
							chkListPR.setTsca052Clialtriedet( personaReportada );
							chkListPR.persist();
							chkListPR.flush();							
						}
					}
				}

				String cdSistema = "SCA";
				String cdSegmento = "000";
				String cdAlerta = getCdAlertaSecuence();
				String cdTipologia = personaReportada.getCdTipologia().getCdTipologia();
				
				if( cdTipoAlta.intValue() == 13 ){
					// Alta Normal
					Tsca013Alerta alerta = new Tsca013Alerta();
					Tsca013AlertaPK idAlerta = new Tsca013AlertaPK( cdAlerta, cdSistema );
					alerta.setId( idAlerta );
					alerta.setCdStAlerta( new BigDecimal( 1 ) );
					alerta.setFhAlerta( new Date() );
					alerta.setFhEnvio( new Date() );
					Tsca002SegPld segmento = Tsca002SegPld.findTsca002SegPld( cdSegmento);					
					if( segmento != null ){
						System.out.println( "CD Segmento" + segmento.getCdSegmento() );
						System.out.println( "CD Seg PLD" + segmento.getCdSegPld() );
						System.out.println( "NB Seg PLD" + segmento.getNbSegPld() );
					}
					else{
						System.out.println( "El Segmento 000 no existe" );
					}
					alerta.setCdSeg( segmento );

					Tsca007Tipologia tip= Tsca007Tipologia.findTsca007Tipologia( cdTipologia );
					System.out.println("Tipologia : " + tip.getCdTipologia() );
					alerta.setCdTipologia( tip );
					
					// Calculo de Sesion
					Tsca023Sesion sesion = Tsca023Sesion.findTsca023Sesion( "12-2016" );
					alerta.setCdSesion( sesion );
					
					alerta.setNbNombreCte( personaReportada.getNbNombre() );
					alerta.setNbApmaternoCte( personaReportada.getNbApePaterno());
					alerta.setNbAppaternoCte( personaReportada.getNbApeMaterno() );
					alerta.setNuCuenta( personaReportada.getNuCuenta() );
					alerta.setCdCliente( personaReportada.getCdCliente() );
					alerta.persist();
					alerta.flush();
				}
				else if( cdTipoAlta.intValue() == 14 ){
					// Alta Urgente
					Tsca013Alerta alerta = new Tsca013Alerta();
					Tsca013AlertaPK idAlerta = new Tsca013AlertaPK( cdAlerta, cdSistema );
					alerta.setId( idAlerta );					
					alerta.setCdStAlerta( new BigDecimal( 2 ) );
					alerta.setFhAlerta( new Date() );
					alerta.setFhEnvio( new Date() );
					Tsca002SegPld segmento = Tsca002SegPld.findTsca002SegPld( cdSegmento);
					alerta.setCdSeg( segmento );
					Tsca007Tipologia tip = Tsca007Tipologia.findTsca007Tipologia( cdTipologia );
					System.out.println("Tipologia : " + tip.getCdTipologia() );
					alerta.setCdTipologia( tip );
					
					// Calculo de Sesion
					Tsca023Sesion sesion = Tsca023Sesion.findTsca023Sesion( "12-2016" );
					alerta.setCdSesion( sesion );
					
					alerta.setNbNombreCte( personaReportada.getNbNombre() );
					alerta.setNbApmaternoCte( personaReportada.getNbApePaterno());
					alerta.setNbAppaternoCte( personaReportada.getNbApeMaterno() );
					alerta.setNuCuenta( personaReportada.getNuCuenta() );
					alerta.setCdCliente( personaReportada.getCdCliente() );
					alerta.persist();
					alerta.flush();
					
					Tsca014Caso caso = new Tsca014Caso();
					caso.setCdCliente( personaReportada.getCdCliente() );
					caso.setCdPrioriAsig( cdTipoAlta.toString() );
					//caso.setCdDictamenPre( "LI" );
					caso.setCdDictamenFin( "LI" );
					caso.setFhAut( new Date() );
					caso.setFhDictamenPre( new Date() );
					caso.setFhDictamenFin( new Date() );
					caso.setFhVencimiento( new Date() );
					caso.setFhVenSia( new Date() );
					caso.setStCaso( new BigDecimal( 3 ) );
					caso.setCdUsuario( cdUsuario );
					caso.persist();
					caso.flush();
					
					alerta.setCdCaso(  caso.getCdCaso() );
					alerta.merge();
					alerta.flush();
				}	
								
				
			}
		}
		
		return requerimiento;
	}
	
	public String getCdAlertaSecuence()throws Exception {
		BigDecimal cdAlerta;		
		Query query = em.createNativeQuery(Consultas.getConsulta("getCdAlertaSecuencia"));
		cdAlerta = (BigDecimal) query.getSingleResult();		
		return cdAlerta.toString();
	}

	@Transactional
	public CuentasClienteRiesgoDetDTO insertaPersonaReportada(
			CuentasClienteRiesgoDetDTO personaReportadaDTO, BigDecimal cdTipoAlta, String cdUsuario) throws Exception {

		Tsca052Clialtriedet personaReportada = new Tsca052Clialtriedet();
		Tsca007Tipologia tipologia = Tsca007Tipologia
				.findTsca007Tipologia(personaReportadaDTO.getCdTipologia());
		SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_FECHA);

		personaReportada.setId(new Tsca052ClialtriedetPK(personaReportadaDTO.getCdClialtrie(), personaReportadaDTO.getNuId()));			
		personaReportada.setCdTpPersona( personaReportadaDTO.getNbTpPersona() );
		personaReportada.setCdTipologia(tipologia);

		// Se agregan los dem�s datos
		personaReportada.setCdCaracter(personaReportadaDTO.getCdCaracter());

		String nombreTmp = eliminarCaracteresEspeciales( personaReportadaDTO.getNbNombre() );
		String aPaterno = eliminarCaracteresEspeciales( personaReportadaDTO.getNbApPaterno() );
		String aMaterno = eliminarCaracteresEspeciales( personaReportadaDTO.getNbApMaterno() );
		if( personaReportadaDTO.getCdTpPersona().equals( CD_TP_PERSONA_FISICA.trim() ) )
		{
			personaReportada.setNbNombre( nombreTmp );
			personaReportada.setNbApePaterno( aPaterno );
			personaReportada.setNbApeMaterno( aMaterno );
		}else if( personaReportadaDTO.getCdTpPersona().equals( CD_TP_PERSONA_MORAL.trim() ) || personaReportadaDTO.getCdTpPersona().equals( CD_TP_PERSONA_GOBIERNO.trim() ))
		{
			if(personaReportadaDTO.getNbNombre().length() > rangoInicial &&  personaReportadaDTO.getNbNombre().length() <= maxNombre )
			{	
				personaReportada.setNbNombre( nombreTmp );
			}
			else if( nombreTmp.length() > maxNombre &&  nombreTmp.length() <= maxNombreAPaterno ){
				personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );						
				personaReportada.setNbApePaterno( nombreTmp.substring( rangoYA, ( nombreTmp.length() -1 ) ) );
			}
			else if( nombreTmp.length() > maxNombreAPaterno && nombreTmp.length() <= maxNombreAPatMat ){
				personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );						
				personaReportada.setNbApePaterno( nombreTmp.substring( rangoYA, rangoYB ) );						
				personaReportada.setNbApeMaterno( nombreTmp.substring( rangoZA, nombreTmp.length() -1 ) );						

			}					
			else if( nombreTmp.length() > maxNombreAPatMat  ){
				personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );												
				personaReportada.setNbApePaterno( nombreTmp.substring( rangoYA, rangoYB ) );						
				personaReportada.setNbApeMaterno( nombreTmp.substring( rangoZA, rangoZB ) );												
			}					
		}		
		if( personaReportadaDTO.getCdRFC() != null && personaReportadaDTO.getCdRFC().length() > 13 )
		{
			personaReportada.setCdRfc(personaReportadaDTO.getCdRFC().substring( 0, 12 ));
		}
		else{
			personaReportada.setCdRfc(personaReportadaDTO.getCdRFC() );
		}
		String domicilio = eliminarCaracteresEspeciales( personaReportadaDTO.getNbDomicilio() );
		if( personaReportadaDTO.getNbDomicilio() != null && personaReportadaDTO.getNbDomicilio().length() > 20 )
		{
			
			personaReportada.setNbDomicilio( domicilio.substring( 0, 19 ) );
		}
		else{
			personaReportada.setNbDomicilio( domicilio );
		}
		String complementario = eliminarCaracteresEspeciales( personaReportadaDTO.getNbComplementarios() );
		if( complementario != null && complementario.length() > 20)
		{
			personaReportada.setNbComplementario( complementario.substring( 0, 19 ));
		}
		else{
			personaReportada.setNbComplementario( complementario);
		}
		
		personaReportada.setStCteBancomer( personaReportadaDTO.getCdClienteBancomer() );
		
		if (personaReportadaDTO.getFhNacimiento() != null
				&& !personaReportadaDTO.getFhNacimiento().equals("")) {
			personaReportada.setFhNacimiento( personaReportadaDTO.getFhNacimiento() );
		}
		
		personaReportada.setNuCliente( personaReportadaDTO.getCdCliente() ) ;
		personaReportada.setNuCuenta( personaReportadaDTO.getNuCuenta() );
		
		personaReportada.persist();
		personaReportada.flush();	
		
		// Inserta Check List por Persona Reportada
		if( personaReportada.getCdTipologia() != null ){
			if( personaReportada.getCdTipologia().getCdTipologia() != null && !personaReportada.getCdTipologia().getCdTipologia().equals( "" ) ){
				if( personaReportada.getCdTipologia().getCdTipologia().equals( "L3" ) || personaReportada.getCdTipologia().getCdTipologia().equals( "LD" ) ){
					Tsca066Checklist chkListPR = new Tsca066Checklist();				
					chkListPR.setTsca052Clialtriedet( personaReportada );
					chkListPR.persist();
					chkListPR.flush();							
				}
			}
		}
		
		System.out.println( "Tipo de Alta : " + cdTipoAlta );
		// Determina la prioridad de alta
		//personaReportadaDTO.get
		String cdSistema = "SCA";
		String cdAlerta = getCdAlertaSecuence();	
		
		if( cdTipoAlta.intValue() == 13 ){
			// Alta Normal
			Tsca013Alerta alerta = new Tsca013Alerta();
			Tsca013AlertaPK idAlerta = new Tsca013AlertaPK( cdAlerta, cdSistema );
			alerta.setCdStAlerta( new BigDecimal( 1 ));
			alerta.setId( idAlerta );
			alerta.setFhAlerta( new Date() );
			alerta.setFhEnvio( new Date() );
			Tsca002SegPld segmento = Tsca002SegPld.findTsca002SegPld( "000" );
			alerta.setCdSeg( segmento );
			Tsca023Sesion sesion = Tsca023Sesion.findTsca023Sesion( "12-2016" );
			alerta.setCdSesion( sesion );			
			alerta.setCdTipologia( personaReportada.getCdTipologia() );			
			alerta.setNbNombreCte( personaReportada.getNbNombre() );
			alerta.setNbApmaternoCte( personaReportada.getNbApePaterno());
			alerta.setNbAppaternoCte( personaReportada.getNbApeMaterno() );
			alerta.setNuCuenta( personaReportada.getNuCuenta() );
			alerta.setCdCliente( personaReportada.getCdCliente() );
			alerta.setCdTipologia( personaReportada.getCdTipologia() );
			alerta.persist();
			alerta.flush();
		}
		else if( cdTipoAlta.intValue() == 14 ){
			// Alta Urgente
			Tsca013Alerta alerta = new Tsca013Alerta();
			Tsca013AlertaPK idAlerta = new Tsca013AlertaPK( cdAlerta, cdSistema );
			alerta.setCdStAlerta( new BigDecimal( 2 ) );
			alerta.setId( idAlerta );		
			alerta.setFhAlerta( new Date() );
			alerta.setFhEnvio( new Date() );
			Tsca002SegPld segmento = Tsca002SegPld.findTsca002SegPld( "000" );
			alerta.setCdSeg( segmento );
			Tsca023Sesion sesion = Tsca023Sesion.findTsca023Sesion( "12-2016" );
			alerta.setCdSesion( sesion );
			alerta.setCdTipologia( personaReportada.getCdTipologia() );
			alerta.setNbNombreCte( personaReportada.getNbNombre() );
			alerta.setNbApmaternoCte( personaReportada.getNbApePaterno());
			alerta.setNbAppaternoCte( personaReportada.getNbApeMaterno() );
			alerta.setNuCuenta( personaReportada.getNuCuenta() );
			alerta.setCdCliente( personaReportada.getCdCliente() );
			alerta.setCdTipologia( personaReportada.getCdTipologia() );
			alerta.persist();
			alerta.flush();
			
			Tsca014Caso caso = new Tsca014Caso();
			caso.setCdCliente( personaReportada.getCdCliente() );
			caso.setCdPrioriAsig( cdTipoAlta.toString() );
			//caso.setCdSesion(cdSesion);
			//caso.setCdUsuario( );
			caso.setCdDictamenPre( "LI" );
			caso.setCdDictamenFin( "LI" );
			caso.setFhAut( new Date() );
			caso.setFhDictamenPre( new Date() );
			caso.setFhDictamenFin( new Date() );
			caso.setFhVencimiento( new Date() );
			caso.setFhVenSia( new Date() );
			caso.setStCaso( new BigDecimal( 3 ) );
			caso.setCdUsuario(cdUsuario);
			//caso.setTpJustif( );
			caso.persist();
			caso.flush();
			
			alerta.setCdCaso(  caso.getCdCaso() );
			alerta.merge();
			alerta.flush();
						
		}	
		
		
		
		// Genera Alerta por persona Reportada
		
		// Determina Si se genera un caso en base a la prioridad

		return personaReportadaDTO;
	}
	
	@Transactional
	public List<CuentasClienteRiesgoDetDTO> insertaPersonaReportadaTransac(
			List<CuentasClienteRiesgoDetDTO> lstPersonasReportadas, BigDecimal cdTipoAlta, String cdUsuario) throws Exception {
		for( CuentasClienteRiesgoDetDTO vo : lstPersonasReportadas ){
			this.insertaPersonaReportada( vo, cdTipoAlta, cdUsuario );
		}
		return lstPersonasReportadas;
	}

	
	@Transactional
	public CuentasClienteRiesgoDetDTO updatePersonaReportada(
			CuentasClienteRiesgoDetDTO personaReportadaDTO) throws Exception {

		Tsca052Clialtriedet personaReportada = Tsca052Clialtriedet.findTsca052Clialtriedet( new Tsca052ClialtriedetPK(personaReportadaDTO
				.getCdClialtrie(), personaReportadaDTO.getNuId()) );
		Tsca007Tipologia tipologia = Tsca007Tipologia
				.findTsca007Tipologia(personaReportadaDTO.getCdTipologia());
		SimpleDateFormat sdf = new SimpleDateFormat(FORMATO_FECHA);
				
		personaReportada.setCdTpPersona(personaReportadaDTO.getNbTpPersona());
		personaReportada.setCdTipologia(tipologia);

		// Se agregan los dem�s datos
		personaReportada.setCdCaracter(personaReportadaDTO.getCdCaracter());


		String nombreTmp = eliminarCaracteresEspeciales( personaReportadaDTO.getNbNombre() );
		String aPaterno = eliminarCaracteresEspeciales( personaReportadaDTO.getNbApPaterno() );
		String aMaterno = eliminarCaracteresEspeciales( personaReportadaDTO.getNbApMaterno() );
		if( personaReportadaDTO.getCdTpPersona().equals( CD_TP_PERSONA_FISICA.trim() ) )
		{
			personaReportada.setNbNombre( nombreTmp );
			personaReportada.setNbApePaterno( aPaterno );
			personaReportada.setNbApeMaterno( aMaterno );
		}else if( personaReportadaDTO.getCdTpPersona().equals( CD_TP_PERSONA_MORAL.trim() ) || personaReportadaDTO.getCdTpPersona().equals( CD_TP_PERSONA_GOBIERNO.trim() ))
		{
			
			if(personaReportadaDTO.getNbNombre().length() > rangoInicial &&  personaReportadaDTO.getNbNombre().length() <= maxNombre )
			{	
				personaReportada.setNbNombre( nombreTmp );
			}
			else if( nombreTmp.length() > maxNombre &&  nombreTmp.length() <= maxNombreAPaterno ){
				personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );						
				personaReportada.setNbApePaterno( nombreTmp.substring( rangoYA, ( nombreTmp.length() -1 ) ) );
			}
			else if( nombreTmp.length() > maxNombreAPaterno && nombreTmp.length() <= maxNombreAPatMat ){
				personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );						
				personaReportada.setNbApePaterno( nombreTmp.substring( rangoYA, rangoYB ) );						
				personaReportada.setNbApeMaterno( nombreTmp.substring( rangoZA, nombreTmp.length() -1 ) );						

			}					
			else if( nombreTmp.length() > maxNombreAPatMat  ){
				personaReportada.setNbNombre( nombreTmp.substring( rangoXA, rangoXB ) );												
				personaReportada.setNbApePaterno( nombreTmp.substring( rangoYA, rangoYB ) );						
				personaReportada.setNbApeMaterno( nombreTmp.substring( rangoZA, rangoZB ) );												
			}					
		}
		
		if( personaReportadaDTO.getCdRFC() != null && personaReportadaDTO.getCdRFC().length() > 13 )
		{
			personaReportada.setCdRfc(personaReportadaDTO.getCdRFC().substring( 0, 12 ));
		}
		else{
			personaReportada.setCdRfc(personaReportadaDTO.getCdRFC() );
		}
		String domicilio = eliminarCaracteresEspeciales( personaReportadaDTO.getNbDomicilio() );
		if( personaReportadaDTO.getNbDomicilio() != null && personaReportadaDTO.getNbDomicilio().length() > 20 )
		{
			
			personaReportada.setNbDomicilio( domicilio.substring( 0, 19 ) );
		}
		else{
			personaReportada.setNbDomicilio( domicilio );
		}
		String complementario = eliminarCaracteresEspeciales( personaReportadaDTO.getNbComplementarios() );
		if( complementario != null && complementario.length() > 20)
		{
			personaReportada.setNbComplementario( complementario.substring( 0, 19 ));
		}
		else{
			personaReportada.setNbComplementario( complementario);
		}
		
		personaReportada.setStCteBancomer( personaReportadaDTO.getCdClienteBancomer() );
		
		if (personaReportadaDTO.getFhNacimiento() != null
				&& !personaReportadaDTO.getFhNacimiento().equals("")) {
			personaReportada.setFhNacimiento( personaReportadaDTO.getFhNacimiento() );
		}
		
		personaReportada.setNuCliente( personaReportadaDTO.getCdCliente() ) ;
		personaReportada.setNuCuenta( personaReportadaDTO.getNuCuenta() );
		
		personaReportada.merge();
		personaReportada.flush();	

		return personaReportadaDTO;
	}	

	@SuppressWarnings("unchecked")
	public List<CuentasClienteRiesgoDetDTO> getClientesLocalizados(
			CuentasClienteRiesgoDetDTO datosCliente) throws Exception {
		List<CuentasClienteRiesgoDetDTO> lstClientes = new ArrayList<CuentasClienteRiesgoDetDTO>();
		Query query = em.createNativeQuery(Consultas
				.getConsulta("getClientesLocalizados"));
		query.setParameter(1, datosCliente.getNbNombre());
		query.setParameter(2, datosCliente.getCdClienteBancomer());
		query.setParameter(3, datosCliente.getNbTpPersona());
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdCliente", StringType.INSTANCE)
				.addScalar("nbNombre", StringType.INSTANCE)
				.addScalar("cdRFC", StringType.INSTANCE)
				.setResultTransformer(
						Transformers
								.aliasToBean(CuentasClienteRiesgoDetDTO.class));
		lstClientes = (List<CuentasClienteRiesgoDetDTO>) query.getResultList();
		return lstClientes;
	}

	public List<CuentasClienteRiesgoDTO> getClienteRiesgo() {

		List<CuentasClienteRiesgoDTO> lstCuentasClienteRiesgoDTO = new ArrayList<CuentasClienteRiesgoDTO>();
		CuentasClienteRiesgoDTO cuentasClienteRiesgoDTO = null;

		for (int i = 0; i < 10; i++) {

			cuentasClienteRiesgoDTO = new CuentasClienteRiesgoDTO();
			cuentasClienteRiesgoDTO.setNbTipo("Beneficiario");
			cuentasClienteRiesgoDTO.setNbDescripcion("Tarjeta de Credito");
			lstCuentasClienteRiesgoDTO.add(cuentasClienteRiesgoDTO);
		}

		return lstCuentasClienteRiesgoDTO;

	}

	@Transactional
	public ArchivoDTO insertarArchivoAdjunto(ArchivoDTO archivo)
			throws Exception {
		// Session session = sessionFactory.getCurrentSession();
		Tsca054Archivo folioArchivo = new Tsca054Archivo();
		//try {

			// Set Folio Expediente
			folioArchivo.setCdExpediente(archivo.getCdExpediente());

			// Obtener Fichero
			/*
			ByteArrayOutputStream buffer = new ByteArrayOutputStream();
			int nRead;
			byte[] data = new byte[16384];
			if (archivo.getNbDocumento() != null) {
				while ((nRead = archivo.getNbDocumento().read(data, 0,
						data.length)) != -1) {
					buffer.write(data, 0, nRead);
				}

				buffer.flush();
				Session session = (Session) em.getDelegate();
				Blob blob = Hibernate.getLobCreator(session).createBlob(
						buffer.toByteArray());
				folioArchivo.setNbDocumento(blob);
			}
			*/

			folioArchivo.setCdExpediente(archivo.getCdExpediente());
			folioArchivo.setNbTitulo(archivo.getNbTitulo());
			folioArchivo.setCdUsuario(archivo.getCdUsuarioCreacion());
			folioArchivo.setFhCreacion(new Date());
			folioArchivo.setCdUsuarioMdf(archivo.getCdUsuarioCreacion());
			folioArchivo.setFhModif(new Date());

			folioArchivo.persist();
			folioArchivo.flush();
		/*
		} catch (IOException e) {
			System.out
					.println("OCurri� un error al insertar el archivo de tipo Blob: "
							+ e.getMessage());
			e.printStackTrace();
		}
		*/

		return archivo;
	}

	@SuppressWarnings("unchecked")
	public List<ArchivoDTO> consultaArchivosByCdExpediente(String cdExpediente)
			throws Exception {
		List<ArchivoDTO> lstResultado = new ArrayList<ArchivoDTO>();
		Query query = em.createNativeQuery(Consultas
				.getConsulta("getConsultaArchivosAdjuntosPorFolioArchivo"));
		query.setParameter(1, cdExpediente);
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdFolioArchivo")
				.addScalar("cdExpediente")
				.addScalar("nbTitulo")
				.setResultTransformer(
						Transformers.aliasToBean(ArchivoDTO.class));
		lstResultado = (List<ArchivoDTO>) query.getResultList();
		return lstResultado;
	}

	public Tsca054Archivo consultaArchivoById(String cdFolioArchivo)
			throws Exception {
		Query query = em
				.createNativeQuery(Consultas.getConsulta("getFileById"));
		query.setParameter(1, cdFolioArchivo);
		Tsca054Archivo folioArchivo = Tsca054Archivo.findTsca054Archivo(new BigDecimal(cdFolioArchivo));
		return folioArchivo;
	}

	@Transactional
	public ArchivoDTO eliminarArchivo(String cdFolioArchivo) throws Exception {
		BigDecimal idFolio = new BigDecimal(cdFolioArchivo);
		ArchivoDTO dtoArchivo = new ArchivoDTO();
		Tsca054Archivo archivo = Tsca054Archivo.findTsca054Archivo(idFolio);
		dtoArchivo.setCdExpediente(archivo.getCdExpediente());
		dtoArchivo.setCdFolioArchivo(archivo.getCdFolioArchivo());
		dtoArchivo.setCdUsuarioCreacion(archivo.getCdUsuario());
		dtoArchivo.setFhCreacion(archivo.getFhCreacion());
		dtoArchivo.setCdUsuarioModificacion(archivo.getCdUsuarioMdf());
		dtoArchivo.setFhModificacion(archivo.getFhModif());
		dtoArchivo.setNbTitulo(archivo.getNbTitulo());
		dtoArchivo.setTxInvestigacion(archivo.getTxInvestigacion());
		archivo.remove();
		return dtoArchivo;
	}

	@SuppressWarnings("unchecked")
	public List<CuentasClienteRiesgoDetDTO> consultaPersonasReportadasByReq(
			String cdRequerimiento) throws Exception {
		List<CuentasClienteRiesgoDetDTO> lstResultado = new ArrayList<CuentasClienteRiesgoDetDTO>();
		Query query = em.createNativeQuery(Consultas
				.getConsulta("getConsultaPersonasReportadasByClienteAR"));
		query.setParameter(1, cdRequerimiento);
		query.unwrap(org.hibernate.SQLQuery.class)	
				.addScalar("cdClialtrie")
				.addScalar("nuId")
				.addScalar("cdCliente")
				.addScalar("cdTipologia")
				.addScalar("cdTpPersona")
				.addScalar("nbTpPersona")
				.addScalar("nuCuenta")
				.addScalar("cdCaracter")
				.addScalar("nbNombre")
				.addScalar("nbApPaterno")
				.addScalar("nbApMaterno")
				.addScalar("fhNacimiento")
				.addScalar("cdRFC")
				.addScalar("nbDomicilio")
				.addScalar("nbComplementarios")
				.addScalar("cdClienteBancomer", StringType.INSTANCE )
				.addScalar("cdDictamen")
				.setResultTransformer(
						Transformers
								.aliasToBean(CuentasClienteRiesgoDetDTO.class));
		lstResultado = (List<CuentasClienteRiesgoDetDTO>) query.getResultList();
		return lstResultado;
	}

	public boolean eliminarPersonaReportada(BigDecimal cdCliAltRie, BigDecimal nuId) throws Exception {
		boolean stDelete = false;
		Tsca052ClialtriedetPK cdPersonaReportada = new Tsca052ClialtriedetPK( cdCliAltRie, nuId);	
		Tsca052Clialtriedet personaReportada = Tsca052Clialtriedet.findTsca052Clialtriedet( cdPersonaReportada );
		ChecklistDAO checkListDAO = new ChecklistDAO();
		Tsca066Checklist checkList = checkListDAO.getCheckListByClialtriedet( cdCliAltRie, nuId );
		checkList.remove();		
		personaReportada.remove();
		stDelete = true;
		return stDelete;
	}
	
	public static String eliminarCaracteresEspeciales(String input) {
		if( input != null ){
		    // Cadena de caracteres original a sustituir.
		    String original = "��������������u�������������������";
		    // Cadena de caracteres ASCII que reemplazar�n los originales.
		    String ascii = "aaaeeeiiiooouuunAAAEEEIIIOOOUUUNcC";
		    String output = input;
		    for (int i=0; i<original.length(); i++) {
		        // Reemplazamos los caracteres especiales.
		        output = output.replace(original.charAt(i), ascii.charAt(i));
		    }//for i
		    return output;			
		}
		else{
			return "";
		}
	}//remove1	
	/*
	@SuppressWarnings("unchecked")
	public static List<Tsca002SegPld> findTsca002SegPldId(BigDecimal cdSegmento) {
		Query query = entityManager().createQuery("SELECT segmento FROM Tsca002SegPld segmento WHERE UPPER(segmento.cdParticipante) = :cdParticipante");
		query.setParameter("cdParticipante", cd_participante);
		return query.getResultList();
	}	
	*/	

}
