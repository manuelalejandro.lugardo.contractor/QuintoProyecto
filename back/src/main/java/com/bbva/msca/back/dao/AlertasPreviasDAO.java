package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import com.bbva.msca.back.dto.AlertasPreviasDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class AlertasPreviasDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	//Consulta Alertas Previas SICA
	@SuppressWarnings("unchecked")
	public List<AlertasPreviasDTO> getAlertasPrevias(String cdCliente) throws Exception{
		
		 List<AlertasPreviasDTO> lstAlertasPreviasDTO = null;
		 AlertasPreviasSiaDAO alertasPreviasSiaDAO = new AlertasPreviasSiaDAO();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getConsultaAlertasPrevias"));
		 query.setParameter(1,cdCliente);
		
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nuCaso")
			.addScalar("nuFolio")
		    .addScalar("nuCliente",StringType.INSTANCE)
            .addScalar("nbCliente")
            .addScalar("nuCuenta",StringType.INSTANCE)
            .addScalar("nbTipologia",StringType.INSTANCE)
            .addScalar("fhAlerta",TimestampType.INSTANCE)
            .addScalar("nbConsultor")
            .addScalar("nbDictamen",StringType.INSTANCE)
            .addScalar("fhDictamen",TimestampType.INSTANCE)
            .addScalar("nbDictamenSIA")
            .addScalar("fhDictamenSIA")
            .addScalar("nbSistema")
            .addScalar("nbCalificacion")
            .addScalar("nbReportada")
            .addScalar("fhReporte",TimestampType.INSTANCE)
            .addScalar("nbCoincidencia")
		 .setResultTransformer(Transformers.aliasToBean(AlertasPreviasDTO.class));
				
		 lstAlertasPreviasDTO = (List<AlertasPreviasDTO>)query.getResultList();
		 
		 if(lstAlertasPreviasDTO != null){
			 lstAlertasPreviasDTO.addAll(alertasPreviasSiaDAO.getAlertasPreviasSIA(cdCliente));
		 }else{
			 lstAlertasPreviasDTO = alertasPreviasSiaDAO.getAlertasPreviasSIA(cdCliente);
		 }	 
		return lstAlertasPreviasDTO;
	}
}
