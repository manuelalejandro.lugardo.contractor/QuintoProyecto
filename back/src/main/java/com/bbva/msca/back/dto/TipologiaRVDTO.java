package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaRVDTO {

	@AnotacionAlerta(tipologia = "rv", idCampo = "imMontoAlerta", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_IMMONTOALERTA", formatter = "formatterMoney")
	private BigDecimal imMontoAlerta;
	@AnotacionAlerta(tipologia = "rv", idCampo = "nbCte", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")
	private String nbCte;
	@AnotacionAlerta(tipologia = "rv", idCampo = "cdSector", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_CDSECTOR", instanceType = "1")
	private String cdSector;
	@AnotacionAlerta(tipologia = "rv", idCampo = "fhApertura", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_FHAPERTURA", instanceType = "2", formatter = "formatterDate")
	private Date fhApertura;
	@AnotacionAlerta(tipologia = "rv", idCampo = "nbSucApertura", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_NBSUCAPERTURA", instanceType = "1")
	private String nbSucApertura;
	@AnotacionAlerta(tipologia = "rv", idCampo = "cdCentroSup", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_CDCENTROSUP", instanceType = "1")
	private String cdCentroSup;
	@AnotacionAlerta(tipologia = "rv", idCampo = "nbDivision", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_NBDIVISION", instanceType = "1")
	private String nbDivision;
	@AnotacionAlerta(tipologia = "rv", idCampo = "nbEstado", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_NBESTADO", instanceType = "1")
	private String nbEstado;
	@AnotacionAlerta(tipologia = "rv", idCampo = "cdSucursal", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_CDSUCURSAL", instanceType = "1")
	private String cdSucursal;
	@AnotacionAlerta(tipologia = "rv", idCampo = "nbCajero", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_NBCAJERO", instanceType = "1")
	private String nbCajero;
	@AnotacionAlerta(tipologia = "rv", idCampo = "nbMunicipio", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_NBMUNICIPIO", instanceType = "1")
	private String nbMunicipio;
	
	
	public BigDecimal getImMontoAlerta() {
		return imMontoAlerta;
	}
	public void setImMontoAlerta(BigDecimal imMontoAlerta) {
		this.imMontoAlerta = imMontoAlerta;
	}
	public String getNbCte() {
		return nbCte;
	}
	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}
	public String getCdSector() {
		return cdSector;
	}
	public void setCdSector(String cdSector) {
		this.cdSector = cdSector;
	}
	public Date getFhApertura() {
		return fhApertura;
	}
	public void setFhApertura(Date fhApertura) {
		this.fhApertura = fhApertura;
	}
	public String getNbSucApertura() {
		return nbSucApertura;
	}
	public void setNbSucApertura(String nbSucApertura) {
		this.nbSucApertura = nbSucApertura;
	}
	public String getCdCentroSup() {
		return cdCentroSup;
	}
	public void setCdCentroSup(String cdCentroSup) {
		this.cdCentroSup = cdCentroSup;
	}
	public String getNbDivision() {
		return nbDivision;
	}
	public void setNbDivision(String nbDivision) {
		this.nbDivision = nbDivision;
	}
	public String getNbEstado() {
		return nbEstado;
	}
	public void setNbEstado(String nbEstado) {
		this.nbEstado = nbEstado;
	}
	public String getCdSucursal() {
		return cdSucursal;
	}
	public void setCdSucursal(String cdSucursal) {
		this.cdSucursal = cdSucursal;
	}
	public String getNbCajero() {
		return nbCajero;
	}
	public void setNbCajero(String nbCajero) {
		this.nbCajero = nbCajero;
	}
	public String getNbMunicipio() {
		return nbMunicipio;
	}
	public void setNbMunicipio(String nbMunicipio) {
		this.nbMunicipio = nbMunicipio;
	}
}
