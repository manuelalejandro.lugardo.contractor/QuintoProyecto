package com.bbva.msca.back.srv;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.AsignacionSesionDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/AsignacionSesionService")

public interface AsignacionSesionService {
	
	// PARAMETRIA - FNO3 ASIGNACION POR SESION
	@GET
	@Path("/consultarAsignacionSesiones")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAsignacionSesiones();
	
	@POST
	@Path("/altaAsigSesion")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric creaAsigSesion(AsignacionSesionDTO asigSesion);
	
	@POST
	@Path("/modificaAsigSesion")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric modificaAsigSesion(AsignacionSesionDTO asigSesion);

}