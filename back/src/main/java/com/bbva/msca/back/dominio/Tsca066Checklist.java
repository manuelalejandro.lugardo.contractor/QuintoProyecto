package com.bbva.msca.back.dominio;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import com.sun.istack.NotNull;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(versionField = "", table = "TSCA066_CHECKLIST", schema = "GORAPR")
public class Tsca066Checklist {

    @Id
    @NotNull
    @SequenceGenerator(name = "SQ_TSCA066", sequenceName = "SQ_TSCA066", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TSCA066")
    @Column(name = "CD_CHECKLIST")
    private BigDecimal idChecklist;
    
}
