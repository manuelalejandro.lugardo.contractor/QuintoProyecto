package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class AlertasPreviasDTO {
	
	private BigDecimal nuCaso;
	private String nuFolio;
	private String nuCliente;
	private String nbCliente;
	private String nuCuenta;
	private String nbTipologia;
	private Date fhAlerta;
	private String nbConsultor;
	private String nbDictamen;
	private Date fhDictamen;	
	private String nbDictamenSIA;//******
	private Date fhDictamenSIA;//******
	private String nbSistema;//******
	private String nbCalificacion;//******
	private String nbReportada;//******
	private Date fhReporte;//******	
	private String nbCoincidencia;//******
	
	
	public String getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(String nuFolio) {
		this.nuFolio = nuFolio;
	}
	public String getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(String nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public BigDecimal getNuCaso() {
		return nuCaso;
	}
	public void setNuCaso(BigDecimal nuCaso) {
		this.nuCaso = nuCaso;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNbDictamen() {
		return nbDictamen;
	}
	public void setNbDictamen(String nbDictamen) {
		this.nbDictamen = nbDictamen;
	}
	public String getNbTipologia() {
		return nbTipologia;
	}
	public void setNbTipologia(String nbTipologia) {
		this.nbTipologia = nbTipologia;
	}
	public String getNbConsultor() {
		return nbConsultor;
	}
	public void setNbConsultor(String nbConsultor) {
		this.nbConsultor = nbConsultor;
	}
	public Date getFhAlerta() {
		return fhAlerta;
	}
	public void setFhAlerta(Date fhAlerta) {
		this.fhAlerta = fhAlerta;
	}
	public String getNbDictamenSIA() {
		return nbDictamenSIA;
	}
	public void setNbDictamenSIA(String nbDictamenSIA) {
		this.nbDictamenSIA = nbDictamenSIA;
	}
	public Date getFhDictamen() {
		return fhDictamen;
	}
	public void setFhDictamen(Date fhDictamen) {
		this.fhDictamen = fhDictamen;
	}
	public Date getFhReporte() {
		return fhReporte;
	}
	public void setFhReporte(Date fhReporte) {
		this.fhReporte = fhReporte;
	}
	public Date getFhDictamenSIA() {
		return fhDictamenSIA;
	}
	public void setFhDictamenSIA(Date fhDictamenSIA) {
		this.fhDictamenSIA = fhDictamenSIA;
	}
	public String getNbSistema() {
		return nbSistema;
	}
	public void setNbSistema(String nbSistema) {
		this.nbSistema = nbSistema;
	}
	public String getNbCalificacion() {
		return nbCalificacion;
	}
	public void setNbCalificacion(String nbCalificacion) {
		this.nbCalificacion = nbCalificacion;
	}
	public String getNbReportada() {
		return nbReportada;
	}
	public void setNbReportada(String nbReportada) {
		this.nbReportada = nbReportada;
	}
	public String getNbCoincidencia() {
		return nbCoincidencia;
	}
	public void setNbCoincidencia(String nbCoincidencia) {
		this.nbCoincidencia = nbCoincidencia;
	}

}
