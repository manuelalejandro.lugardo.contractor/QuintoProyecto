package com.bbva.msca.back.dto;
import java.util.List;

public class TransaccionClienteDTO {
	//tabla tdc PARTE DOS  transacciones
	private String operativaPesosTDC;
	private String montoTotalDepositosTDC;
	private String localidadOperarTDC;

	//tabla tdc TABLA DE TRANSACCIONES
	private List<TransaccionClienteTdcDTO> tablaTDC;

	
	
	public String getOperativaPesosTDC() {
		return operativaPesosTDC;
	}

	public void setOperativaPesosTDC(String operativaPesosTDC) {
		this.operativaPesosTDC = operativaPesosTDC;
	}

	public String getMontoTotalDepositosTDC() {
		return montoTotalDepositosTDC;
	}

	public void setMontoTotalDepositosTDC(String montoTotalDepositosTDC) {
		this.montoTotalDepositosTDC = montoTotalDepositosTDC;
	}

	public String getLocalidadOperarTDC() {
		return localidadOperarTDC;
	}

	public void setLocalidadOperarTDC(String localidadOperarTDC) {
		this.localidadOperarTDC = localidadOperarTDC;
	}

	public List<TransaccionClienteTdcDTO> getTablaTDC() {
		return tablaTDC;
	}

	public void setTablaTDC(List<TransaccionClienteTdcDTO> tablaTDC) {
		this.tablaTDC = tablaTDC;
	}

	



}
