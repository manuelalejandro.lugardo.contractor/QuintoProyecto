package com.bbva.msca.back.servicios.host.pee5;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.auditoria.Auditable;
import com.bbva.jee.arq.spring.core.auditoria.InformacionAuditable;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.TriggersRemove;

/**
 * Invocador de la transacci&oacute;n <code>PEE5</code>
 * 
 * @see PeticionTransaccionPee5
 * @see RespuestaTransaccionPee5
 *  
 * @author Arquitectura Spring BBVA
 */
@Component
public class TransaccionPee5 implements InvocadorTransaccion<PeticionTransaccionPee5,RespuestaTransaccionPee5> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	@Auditable({InformacionAuditable.ARGUMENTOS, InformacionAuditable.RESULTADO, InformacionAuditable.DURACION})
	public RespuestaTransaccionPee5 invocar(PeticionTransaccionPee5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPee5.class, RespuestaTransaccionPee5.class, transaccion);
	}
	
	@Override
	@Auditable({InformacionAuditable.ARGUMENTOS, InformacionAuditable.RESULTADO, InformacionAuditable.DURACION})
	@Cacheable(cacheName = "cacheTransaccionPee5")
	public RespuestaTransaccionPee5 invocarCache(PeticionTransaccionPee5 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPee5.class, RespuestaTransaccionPee5.class, transaccion);
	}
	
	@Override
	@Auditable(InformacionAuditable.DURACION)
	@TriggersRemove(cacheName = "cacheTransaccionPee5", removeAll = true)
	public void vaciarCache() {}
		
}