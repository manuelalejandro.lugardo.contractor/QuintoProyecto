package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bbva.msca.back.dto.LocalizacionClienteRiesgoDTO;

public class LocalizacionClienteRiesgoDAO {
	
	public List<LocalizacionClienteRiesgoDTO> getLocalizacionClienteRiesgo(){
		
		List<LocalizacionClienteRiesgoDTO> lstLocalizacionClienteRiesgoDTO = new ArrayList<LocalizacionClienteRiesgoDTO>();
		LocalizacionClienteRiesgoDTO localizacionClienteRiesgoDTO = null;
		
		for(int i=0;i<10;i++){
			
			localizacionClienteRiesgoDTO = new LocalizacionClienteRiesgoDTO();
			localizacionClienteRiesgoDTO.setStClienteRiesgo(false);
			localizacionClienteRiesgoDTO.setNuCliente(1001 + i);
			localizacionClienteRiesgoDTO.setNbCliente("Ramon Sanchez Romero");
			localizacionClienteRiesgoDTO.setNuRFC("SAPR950823HDFR07");
			localizacionClienteRiesgoDTO.setNbDomicilio("Mexico D.F");
			localizacionClienteRiesgoDTO.setNbSector("F32");
			lstLocalizacionClienteRiesgoDTO.add(localizacionClienteRiesgoDTO);
		}

		return lstLocalizacionClienteRiesgoDTO;
		
	}
	
	public static LocalizacionClienteRiesgoDTO getBusquedaClienteRiesgo(){
		LocalizacionClienteRiesgoDTO bClienteRiesgo = new LocalizacionClienteRiesgoDTO();
		
		bClienteRiesgo.setNbCliente("Ramon");
		bClienteRiesgo.setNbPaterno("Sanchez");
		bClienteRiesgo.setNbMaterno("Romero");
		bClienteRiesgo.setNbRazonSocial("Hombre");
		bClienteRiesgo.setNuRFC("SAPR950823HDFR07");
		bClienteRiesgo.setNuCliente(1001);
		bClienteRiesgo.setFhNacimiento(new Date());
		bClienteRiesgo.setCdTPersona(1);

		return bClienteRiesgo;

}
	

}
