package com.bbva.msca.back.dto;

import java.util.Date;

public class BusquedaRiesgoDTO {
	
	private Integer cdTipo;					
	private Date fhRecepcion;				
	private Integer cdPrioridad;				
	private Integer cdTipologia;				
	private Integer nuCuenta;					
	private Integer nuCliente;					
	private String nbCliente;					
	private Integer nuOficio;			   
	private String nuExpediente;				
	private Integer nuFolio;		
	private Date fhPublicacion;			
	private String nbAutoridad;		
	private String nbReferencia;
	
	public Integer getCdTipo() {
		return cdTipo;
	}
	public void setCdTipo(Integer cdTipo) {
		this.cdTipo = cdTipo;
	}
	public Date getFhRecepcion() {
		return fhRecepcion;
	}
	public void setFhRecepcion(Date fhRecepcion) {
		this.fhRecepcion = fhRecepcion;
	}
	public Integer getCdPrioridad() {
		return cdPrioridad;
	}
	public void setCdPrioridad(Integer cdPrioridad) {
		this.cdPrioridad = cdPrioridad;
	}
	public Integer getCdTipologia() {
		return cdTipologia;
	}
	public void setCdTipologia(Integer cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	public Integer getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(Integer nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public Integer getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(Integer nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public Integer getNuOficio() {
		return nuOficio;
	}
	public void setNuOficio(Integer nuOficio) {
		this.nuOficio = nuOficio;
	}
	public String getNuExpediente() {
		return nuExpediente;
	}
	public void setNuExpediente(String nuExpediente) {
		this.nuExpediente = nuExpediente;
	}
	public Integer getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(Integer nuFolio) {
		this.nuFolio = nuFolio;
	}
	public Date getFhPublicacion() {
		return fhPublicacion;
	}
	public void setFhPublicacion(Date fhPublicacion) {
		this.fhPublicacion = fhPublicacion;
	}
	public String getNbAutoridad() {
		return nbAutoridad;
	}
	public void setNbAutoridad(String nbAutoridad) {
		this.nbAutoridad = nbAutoridad;
	}
	public String getNbReferencia() {
		return nbReferencia;
	}
	public void setNbReferencia(String nbReferencia) {
		this.nbReferencia = nbReferencia;
	}	
	
	


}
