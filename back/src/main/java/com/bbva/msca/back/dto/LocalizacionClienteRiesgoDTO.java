package com.bbva.msca.back.dto;

import java.util.Date;

public class LocalizacionClienteRiesgoDTO {
	
	private boolean stClienteRiesgo;
	private Integer nuCliente;
	private String nbCliente;
	private String nbPaterno;
	private String nbMaterno;
	private String nbRazonSocial;
	private Date fhNacimiento;
	private Integer cdTPersona;
	private String nuRFC;
	private String nbDomicilio;
	private String nbSector;
	
	public boolean getStClienteRiesgo() {
		return stClienteRiesgo;
	}
	public void setStClienteRiesgo(boolean stClienteRiesgo) {
		this.stClienteRiesgo = stClienteRiesgo;
	}
	public Integer getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(Integer nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNuRFC() {
		return nuRFC;
	}
	public void setNuRFC(String nuRFC) {
		this.nuRFC = nuRFC;
	}
	public String getNbDomicilio() {
		return nbDomicilio;
	}
	public void setNbDomicilio(String nbDomicilio) {
		this.nbDomicilio = nbDomicilio;
	}
	public String getNbSector() {
		return nbSector;
	}
	public void setNbSector(String nbSector) {
		this.nbSector = nbSector;
	}
	public String getNbPaterno() {
		return nbPaterno;
	}
	public void setNbPaterno(String nbPaterno) {
		this.nbPaterno = nbPaterno;
	}
	public String getNbMaterno() {
		return nbMaterno;
	}
	public void setNbMaterno(String nbMaterno) {
		this.nbMaterno = nbMaterno;
	}
	public String getNbRazonSocial() {
		return nbRazonSocial;
	}
	public void setNbRazonSocial(String nbRazonSocial) {
		this.nbRazonSocial = nbRazonSocial;
	}
	public Date getFhNacimiento() {
		return fhNacimiento;
	}
	public void setFhNacimiento(Date fhNacimiento) {
		this.fhNacimiento = fhNacimiento;
	}
	public Integer getCdTPersona() {
		return cdTPersona;
	}
	public void setCdTPersona(Integer cdTPersona) {
		this.cdTPersona = cdTPersona;
	}
	
	
	
	

}
