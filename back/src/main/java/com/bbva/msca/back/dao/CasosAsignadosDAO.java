package com.bbva.msca.back.dao;

import java.math.BigDecimal;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.h2.java.lang.System;
import org.hibernate.transform.Transformers;
import org.hibernate.type.TimestampType;
import org.springframework.stereotype.Component;
import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.dto.AlertaDTO;
import com.bbva.msca.back.dto.ParametrosBusquedaCasoDTO;
import com.bbva.msca.back.util.Consultas;

@Component
public class CasosAsignadosDAO {
	
	private static I18nLog logger = I18nLogFactory.getLogI18n((CasosAsignadosDAO.class));
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
		
	@SuppressWarnings("unchecked")
	public List<AlertaDTO> getConsultaCasosAsignadosConsultor(String cdConsultor,Integer limit, Integer offset,String search, String name, String order) throws Exception{
		
		 List<AlertaDTO> lstAlertasDTO = null;
		 String filtro = "";
		 String condicion = " WHERE ";
		 boolean isFiltro = false;
		 SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
		 ParametrosBusquedaCasoDTO parametrosBusquedaCasoDTO = null;
		 String consulta = " SELECT * FROM ( "+Consultas.getConsulta("getCasosAsignados")+" ) ";
		 logger.info(search,search);
		 
		 if(search != null && !search.equals("")){
			
			 String arrFiltros[] = search.split("\\|");
			 
			 
			 if(arrFiltros.length > 0){
				 parametrosBusquedaCasoDTO = new ParametrosBusquedaCasoDTO();
				 parametrosBusquedaCasoDTO.setNuCaso(arrFiltros[0].trim());
				 parametrosBusquedaCasoDTO.setNuFolio(arrFiltros[1].trim());
				 parametrosBusquedaCasoDTO.setNuCliente(arrFiltros[2].trim());
				 parametrosBusquedaCasoDTO.setNbCliente(arrFiltros[3].trim());
				 parametrosBusquedaCasoDTO.setNuCuenta(arrFiltros[4].trim());
				 parametrosBusquedaCasoDTO.setCdTPersona(arrFiltros[5].trim());
				 parametrosBusquedaCasoDTO.setCdTipologia(arrFiltros[6].trim());
				 parametrosBusquedaCasoDTO.setCdSegmento(arrFiltros[7].trim());
				 parametrosBusquedaCasoDTO.setCdSesion(arrFiltros[8].trim());
				 parametrosBusquedaCasoDTO.setCdRespuesta(arrFiltros[9].trim());
				 parametrosBusquedaCasoDTO.setCdEscenario(arrFiltros[10].trim());
				 
				 if(!arrFiltros[11].trim().equals("")){
					 String arrOperador[] = arrFiltros[11].trim().split(",");
					 if(arrOperador.length == 2){
						 parametrosBusquedaCasoDTO.setCdCalificacion(arrOperador[0].trim());
						 parametrosBusquedaCasoDTO.setNuCalificacion(new BigDecimal(arrOperador[1].trim()));
					 }
				 }
				 if(!arrFiltros[12].trim().equals("")){
					 String arrOperadorPts[] = arrFiltros[12].trim().split(",");
					 if(arrOperadorPts.length == 2){
						 parametrosBusquedaCasoDTO.setCdPtsMatriz(arrOperadorPts[0].trim());
						 parametrosBusquedaCasoDTO.setNuPtsMatriz(new BigDecimal(arrOperadorPts[1].trim()));
					 }
				 }
				 if(!arrFiltros[13].trim().equals("")){
					 String arrOperadorIm[] = arrFiltros[13].trim().split(",");
					 if(arrOperadorIm.length == 2){
						 parametrosBusquedaCasoDTO.setCdImporte(arrOperadorIm[0].trim());
						 parametrosBusquedaCasoDTO.setImImporte(new BigDecimal(arrOperadorIm[1].trim()));
					 }
				 }
				 if(!arrFiltros[14].trim().equals("")){
				 parametrosBusquedaCasoDTO.setFhAlertaDel(new Date(arrFiltros[14].trim()));
				 }
				 if(!arrFiltros[15].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhAlertaAl(new Date(arrFiltros[15].trim()));
				 }
				 if(!arrFiltros[16].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhEnvioDel(new Date(arrFiltros[16].trim()));
				 }
				 if(!arrFiltros[17].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhEnvioAl(new Date(arrFiltros[17].trim()));
				 }
				 if(!arrFiltros[18].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencRevisionDel(new Date(arrFiltros[18].trim()));
				 }
				 if(!arrFiltros[19].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencRevisionAl(new Date(arrFiltros[19].trim()));
				 }
				 if(!arrFiltros[20].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencLiberacionDel(new Date(arrFiltros[20].trim()));
				 }
				 if(!arrFiltros[21].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencLiberacionAl(new Date(arrFiltros[21].trim()));
				 }
				 if(!arrFiltros[22].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencReporteDel(new Date(arrFiltros[22].trim()));
				 }
				 if(!arrFiltros[23].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencReporteAl(new Date(arrFiltros[23].trim()));
				 }
				 if(!arrFiltros[24].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhAsignacionDel(new Date(arrFiltros[24].trim()));
				 }
				 if(!arrFiltros[25].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhAsignacionAl(new Date(arrFiltros[25].trim()));
				 }
			 }
			 
		 }
			 
		 if( parametrosBusquedaCasoDTO != null){
			 
			 if(parametrosBusquedaCasoDTO.getNuCaso() != null && !parametrosBusquedaCasoDTO.getNuCaso().equals("")){
				 filtro = filtro + " cdCaso = "+parametrosBusquedaCasoDTO.getNuCaso() ;
				 isFiltro = true;
				
			 } 
			 if(parametrosBusquedaCasoDTO.getNuFolio() != null && !parametrosBusquedaCasoDTO.getNuFolio().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " nuFolioAlerta = '"+parametrosBusquedaCasoDTO.getNuFolio() + "' ";
				 }else{
					 filtro = filtro + " AND nuFolioAlerta = '"+parametrosBusquedaCasoDTO.getNuFolio() + "' ";
				 }
				 
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getNuCliente() != null && !parametrosBusquedaCasoDTO.getNuCliente().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " nuCliente = '"+parametrosBusquedaCasoDTO.getNuCliente().toUpperCase()+"' ";
				 }else{
					 filtro = filtro + " AND nuCliente = '"+parametrosBusquedaCasoDTO.getNuCliente().toUpperCase()+"' ";
				 }
				 isFiltro = true; 
				 
			 }
			 if(parametrosBusquedaCasoDTO.getNbCliente() != null && !parametrosBusquedaCasoDTO.getNbCliente().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + "  nbCliente LIKE '%"+ parametrosBusquedaCasoDTO.getNbCliente().toUpperCase()+"%' ";
				 }else{
					 filtro = filtro + " AND nbCliente LIKE '%"+ parametrosBusquedaCasoDTO.getNbCliente().toUpperCase()+"%' ";
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getNuCuenta() != null && !parametrosBusquedaCasoDTO.getNuCuenta().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " (SUBSTR(nuCuenta,11,20) = '"+ parametrosBusquedaCasoDTO.getNuCuenta()+"' " + " OR nuCuenta = ' " + parametrosBusquedaCasoDTO.getNuCuenta() +"' )";
				 }else{
					 filtro = filtro + " AND (SUBSTR(nuCuenta,11,20) = '"+ parametrosBusquedaCasoDTO.getNuCuenta()+"' " + " OR nuCuenta = '" + parametrosBusquedaCasoDTO.getNuCuenta() +"' )";

				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdTPersona() != null && !parametrosBusquedaCasoDTO.getCdTPersona().equals("") ){
				 if(filtro.equals("")){
					 filtro = filtro + " cdTPersona = '"+ parametrosBusquedaCasoDTO.getCdTPersona().toUpperCase()+"' ";
				 }else{
					 filtro = filtro + " AND cdTPersona = '"+ parametrosBusquedaCasoDTO.getCdTPersona().toUpperCase()+"' ";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdTipologia() != null && !parametrosBusquedaCasoDTO.getCdTipologia().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " cdTipologia = '"+ parametrosBusquedaCasoDTO.getCdTipologia().toUpperCase()+"' ";
				 }else{
					 filtro = filtro + " AND cdTipologia = '"+ parametrosBusquedaCasoDTO.getCdTipologia().toUpperCase()+"' ";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdSegmento() != null && !parametrosBusquedaCasoDTO.getCdSegmento().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " cdSegmento = '"+ parametrosBusquedaCasoDTO.getCdSegmento().toUpperCase()+"' ";
				 }else{
					 filtro = filtro + " AND cdSegmento = '"+ parametrosBusquedaCasoDTO.getCdSegmento().toUpperCase()+"' ";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdSesion() != null && !parametrosBusquedaCasoDTO.getCdSesion().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " cdSesion = '"+ parametrosBusquedaCasoDTO.getCdSesion()+"' ";
				 }else{
					 filtro = filtro + " AND cdSesion = '"+ parametrosBusquedaCasoDTO.getCdSesion()+"' ";
				 }
				 isFiltro = true;
				 
			 }
			
			 if(parametrosBusquedaCasoDTO.getCdRespuesta() != null && !parametrosBusquedaCasoDTO.getCdRespuesta().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " cdRespuesta = '"+ parametrosBusquedaCasoDTO.getCdRespuesta()+"' ";
				 }else{
					 filtro = filtro + " AND cdRespuesta = '"+ parametrosBusquedaCasoDTO.getCdRespuesta()+"' ";
				 }
				 isFiltro = true;
			
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdEscenario() != null && !parametrosBusquedaCasoDTO.getCdEscenario().equals("")){
				 if(filtro.equals("")){
					 consulta+=" ,TSCA025_RELALRESC T25 ";
					 filtro = filtro + " (T25.CD_ALERTA = nuFolioAlerta and T25.CD_SISTEMA = cdSistema) AND T25.CD_ESCENARIO = '"+ parametrosBusquedaCasoDTO.getCdEscenario()+"' ";
				 }else{
					 consulta+=" ,TSCA025_RELALRESC T25 ";
					 filtro = filtro + " AND (T25.CD_ALERTA = nuFolioAlerta and T25.CD_SISTEMA = cdSistema) AND T25.CD_ESCENARIO = '"+ parametrosBusquedaCasoDTO.getCdEscenario()+"' ";
				 }
				 isFiltro = true;
			
			 }
			 if(parametrosBusquedaCasoDTO.getNuCalificacion() != null && !parametrosBusquedaCasoDTO.getNuCalificacion().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " nuScoreMdl  " +  parametrosBusquedaCasoDTO.getCdCalificacion()  +   parametrosBusquedaCasoDTO.getNuCalificacion();
				 }else{
					 filtro = filtro + " AND nuScoreMdl " +  parametrosBusquedaCasoDTO.getCdCalificacion()  +   parametrosBusquedaCasoDTO.getNuCalificacion();
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getNuPtsMatriz() != null && !parametrosBusquedaCasoDTO.getNuPtsMatriz().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " nuScoreMc " + parametrosBusquedaCasoDTO.getCdPtsMatriz() + parametrosBusquedaCasoDTO.getNuPtsMatriz();
				 }else{
					 filtro = filtro + " AND nuScoreMc " + parametrosBusquedaCasoDTO.getCdPtsMatriz() + parametrosBusquedaCasoDTO.getNuPtsMatriz();
				 }
				 isFiltro = true;				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getImImporte() != null  && !parametrosBusquedaCasoDTO.getImImporte().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " imAlerta " + parametrosBusquedaCasoDTO.getCdImporte()  + parametrosBusquedaCasoDTO.getImImporte();
				 }else{
					 filtro = filtro + " AND imAlerta "  + parametrosBusquedaCasoDTO.getCdImporte() +  parametrosBusquedaCasoDTO.getImImporte();
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhAlertaDel() != null && !parametrosBusquedaCasoDTO.getFhAlertaDel().equals("") || parametrosBusquedaCasoDTO.getFhAlertaAl() != null && !parametrosBusquedaCasoDTO.getFhAlertaAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhAlertaDel() != null && !parametrosBusquedaCasoDTO.getFhAlertaDel().equals("") && parametrosBusquedaCasoDTO.getFhAlertaAl() != null && !parametrosBusquedaCasoDTO.getFhAlertaAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhAlerta BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhAlerta BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhAlertaDel() != null && !parametrosBusquedaCasoDTO.getFhAlertaDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhAlertaAl() != null && !parametrosBusquedaCasoDTO.getFhAlertaAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhEnvioDel() != null && !parametrosBusquedaCasoDTO.getFhEnvioDel().equals("") || parametrosBusquedaCasoDTO.getFhEnvioAl() != null && !parametrosBusquedaCasoDTO.getFhEnvioAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhEnvioDel() != null && !parametrosBusquedaCasoDTO.getFhEnvioDel().equals("") && parametrosBusquedaCasoDTO.getFhEnvioAl() != null && !parametrosBusquedaCasoDTO.getFhEnvioAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhEnvio BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhEnvio BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhEnvioDel() != null && !parametrosBusquedaCasoDTO.getFhEnvioDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhEnvioAl() != null && !parametrosBusquedaCasoDTO.getFhEnvioAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhVencRevisionDel() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionDel().equals("") || parametrosBusquedaCasoDTO.getFhVencRevisionAl() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhVencRevisionDel() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionDel().equals("") && parametrosBusquedaCasoDTO.getFhVencRevisionAl() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhVenRev BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhVenRev BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhVencRevisionDel() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhVencRevisionAl() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhVencLiberacionDel() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionDel().equals("") || parametrosBusquedaCasoDTO.getFhVencLiberacionAl() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhVencLiberacionDel() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionDel().equals("") && parametrosBusquedaCasoDTO.getFhVencLiberacionAl() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhVenLibSia BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhVenLibSia BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhVencLiberacionDel() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhVencLiberacionAl() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhVencReporteDel() != null && !parametrosBusquedaCasoDTO.getFhVencReporteDel().equals("") || parametrosBusquedaCasoDTO.getFhVencReporteAl() != null && !parametrosBusquedaCasoDTO.getFhVencReporteAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhVencReporteDel() != null && !parametrosBusquedaCasoDTO.getFhVencReporteDel().equals("") && parametrosBusquedaCasoDTO.getFhVencReporteAl() != null && !parametrosBusquedaCasoDTO.getFhVencReporteAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhRepAut BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhRepAut BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhVencReporteDel() != null && !parametrosBusquedaCasoDTO.getFhVencReporteDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhVencReporteAl() != null && !parametrosBusquedaCasoDTO.getFhVencReporteAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro +  " fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getFhAsignacionDel() != null && !parametrosBusquedaCasoDTO.getFhAsignacionDel().equals("") || parametrosBusquedaCasoDTO.getFhAsignacionAl() != null && !parametrosBusquedaCasoDTO.getFhAsignacionAl().equals("") ){
				 if(parametrosBusquedaCasoDTO.getFhAsignacionDel() != null && !parametrosBusquedaCasoDTO.getFhAsignacionDel().equals("") && parametrosBusquedaCasoDTO.getFhAsignacionAl() != null && !parametrosBusquedaCasoDTO.getFhAsignacionAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhAsignacion BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhAsignacion BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhAsignacionDel() != null && !parametrosBusquedaCasoDTO.getFhAsignacionDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhAsignacionAl() != null && !parametrosBusquedaCasoDTO.getFhAsignacionAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
			 }
		 }
		 
		 if(isFiltro){
				consulta = consulta+condicion+filtro;
			 }
		 
		 consulta = consulta +" "+ " order by "
					             + (name != null && !"".equals(name) ? " " + name + " ": " fhEnvio ")
					             + " "
					             + (order != null && !"".equals(order) ? " " + order + " "
					             : " asc ");
		 
							 
		 
		 Query query = em.createNativeQuery(consulta);
		 query.setParameter(1,cdConsultor);
		 
		 query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdCaso")
		.addScalar("nuFolioAlerta")
		.addScalar("nuCliente")
        .addScalar("nbCliente")
        .addScalar("nuCuenta")
        .addScalar("fhEnvio" ,TimestampType.INSTANCE)
        .addScalar("cdDivisa")
        .addScalar("cdSistema")
		.addScalar("nbSistema")
        .addScalar("cdTipologia")
        .addScalar("nbSegmento")
        .addScalar("fhAsignacion" ,TimestampType.INSTANCE)
        .addScalar("fhAlerta" ,TimestampType.INSTANCE)
        .addScalar("nbPrioriAsig")
        .addScalar("fhVenRev" ,TimestampType.INSTANCE)
        .addScalar("fhVenLibSia" ,TimestampType.INSTANCE)
        .addScalar("fhRepAut",TimestampType.INSTANCE)
        .addScalar("cdSesion")
        .addScalar("cdTPersona")
        .addScalar("cdSegmento")
        .addScalar("imAlerta")
        .addScalar("nuScoreMc")
        .addScalar("nuScoreMdl")
        .addScalar("nbTpPersona")
        .addScalar("cdRespuesta")
        .addScalar("nbRespuesta")
		.setResultTransformer(Transformers.aliasToBean(AlertaDTO.class));
		
		query.setFirstResult(offset);
		query.setMaxResults(limit);

		lstAlertasDTO = (List<AlertaDTO>)query.getResultList();
	 
		return lstAlertasDTO;
	}
	
	public Integer getNuCasosAsignadosConsultor(String cdConsultor,String search) throws Exception{
		 String filtro = "";
		 String condicion = " WHERE ";
		 boolean isFiltro = false;
		 SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
		 ParametrosBusquedaCasoDTO parametrosBusquedaCasoDTO = null;
		 String consulta = " SELECT COUNT(*) FROM ( "+Consultas.getConsulta("getCasosAsignados")+ " ) ";
		 logger.info(search,search);
		 if(search != null && !search.equals("")){
				
			 String arrFiltros[] = search.split("\\|");
			 
			 
			 if(arrFiltros.length > 0){
				 parametrosBusquedaCasoDTO = new ParametrosBusquedaCasoDTO();
				 parametrosBusquedaCasoDTO.setNuCaso(arrFiltros[0].trim());
				 parametrosBusquedaCasoDTO.setNuFolio(arrFiltros[1].trim());
				 parametrosBusquedaCasoDTO.setNuCliente(arrFiltros[2].trim());
				 parametrosBusquedaCasoDTO.setNbCliente(arrFiltros[3].trim());
				 parametrosBusquedaCasoDTO.setNuCuenta(arrFiltros[4].trim());
				 parametrosBusquedaCasoDTO.setCdTPersona(arrFiltros[5].trim());
				 parametrosBusquedaCasoDTO.setCdTipologia(arrFiltros[6].trim());
				 parametrosBusquedaCasoDTO.setCdSegmento(arrFiltros[7].trim());
				 parametrosBusquedaCasoDTO.setCdSesion(arrFiltros[8].trim());
				 parametrosBusquedaCasoDTO.setCdRespuesta(arrFiltros[9].trim());
				 parametrosBusquedaCasoDTO.setCdEscenario(arrFiltros[10].trim());
				 
				 if(!arrFiltros[11].trim().equals("")){
					 String arrOperador[] = arrFiltros[11].trim().split(",");
					 if(arrOperador.length == 2){
						 parametrosBusquedaCasoDTO.setCdCalificacion(arrOperador[0].trim());
						 parametrosBusquedaCasoDTO.setNuCalificacion(new BigDecimal(arrOperador[1].trim()));
					 }
				 }
				 if(!arrFiltros[12].trim().equals("")){
					 String arrOperadorPts[] = arrFiltros[12].trim().split(",");
					 if(arrOperadorPts.length == 2){
						 parametrosBusquedaCasoDTO.setCdPtsMatriz(arrOperadorPts[0].trim());
						 parametrosBusquedaCasoDTO.setNuPtsMatriz(new BigDecimal(arrOperadorPts[1].trim()));
					 }
				 }
				 if(!arrFiltros[13].trim().equals("")){
					 String arrOperadorIm[] = arrFiltros[13].trim().split(",");
					 if(arrOperadorIm.length == 2){
						 parametrosBusquedaCasoDTO.setCdImporte(arrOperadorIm[0].trim());
						 parametrosBusquedaCasoDTO.setImImporte(new BigDecimal(arrOperadorIm[1].trim()));
					 }
				 }
				 if(!arrFiltros[14].trim().equals("")){
				 parametrosBusquedaCasoDTO.setFhAlertaDel(new Date(arrFiltros[14].trim()));
				 }
				 if(!arrFiltros[15].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhAlertaAl(new Date(arrFiltros[15].trim()));
				 }
				 if(!arrFiltros[16].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhEnvioDel(new Date(arrFiltros[16].trim()));
				 }
				 if(!arrFiltros[17].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhEnvioAl(new Date(arrFiltros[17].trim()));
				 }
				 if(!arrFiltros[18].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencRevisionDel(new Date(arrFiltros[18].trim()));
				 }
				 if(!arrFiltros[19].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencRevisionAl(new Date(arrFiltros[19].trim()));
				 }
				 if(!arrFiltros[20].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencLiberacionDel(new Date(arrFiltros[20].trim()));
				 }
				 if(!arrFiltros[21].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencLiberacionAl(new Date(arrFiltros[21].trim()));
				 }
				 if(!arrFiltros[22].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencReporteDel(new Date(arrFiltros[22].trim()));
				 }
				 if(!arrFiltros[23].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhVencReporteAl(new Date(arrFiltros[23].trim()));
				 }
				 if(!arrFiltros[24].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhAsignacionDel(new Date(arrFiltros[24].trim()));
				 }
				 if(!arrFiltros[25].trim().equals("")){
					 parametrosBusquedaCasoDTO.setFhAsignacionAl(new Date(arrFiltros[25].trim()));
				 }
			 }
			 
		 }
			 
		 if( parametrosBusquedaCasoDTO != null){
			 
			 if(parametrosBusquedaCasoDTO.getNuCaso() != null && !parametrosBusquedaCasoDTO.getNuCaso().equals("")){
				 filtro = filtro + " cdCaso = "+parametrosBusquedaCasoDTO.getNuCaso() ;
				 isFiltro = true;
				
			 } 
			 if(parametrosBusquedaCasoDTO.getNuFolio() != null && !parametrosBusquedaCasoDTO.getNuFolio().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " nuFolioAlerta = '"+parametrosBusquedaCasoDTO.getNuFolio() + "' ";
				 }else{
					 filtro = filtro + " AND nuFolioAlerta = '"+parametrosBusquedaCasoDTO.getNuFolio() + "' ";
				 }
				 
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getNuCliente() != null && !parametrosBusquedaCasoDTO.getNuCliente().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " nuCliente = '"+parametrosBusquedaCasoDTO.getNuCliente().toUpperCase()+"' ";
				 }else{
					 filtro = filtro + " AND nuCliente = '"+parametrosBusquedaCasoDTO.getNuCliente().toUpperCase()+"' ";
				 }
				 isFiltro = true; 
				 
			 }
			 if(parametrosBusquedaCasoDTO.getNbCliente() != null && !parametrosBusquedaCasoDTO.getNbCliente().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + "  nbCliente LIKE '%"+ parametrosBusquedaCasoDTO.getNbCliente().toUpperCase()+"%' ";
				 }else{
					 filtro = filtro + " AND nbCliente LIKE '%"+ parametrosBusquedaCasoDTO.getNbCliente().toUpperCase()+"%' ";
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getNuCuenta() != null && !parametrosBusquedaCasoDTO.getNuCuenta().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " (SUBSTR(nuCuenta,11,20) = '"+ parametrosBusquedaCasoDTO.getNuCuenta()+"' " + " OR nuCuenta = ' " + parametrosBusquedaCasoDTO.getNuCuenta() +"' )";
				 }else{
					 filtro = filtro + " AND (SUBSTR(nuCuenta,11,20) = '"+ parametrosBusquedaCasoDTO.getNuCuenta()+"' " + " OR nuCuenta = '" + parametrosBusquedaCasoDTO.getNuCuenta() +"' )";

				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdTPersona() != null && !parametrosBusquedaCasoDTO.getCdTPersona().equals("") ){
				 if(filtro.equals("")){
					 filtro = filtro + " cdTPersona = '"+ parametrosBusquedaCasoDTO.getCdTPersona().toUpperCase()+"' ";
				 }else{
					 filtro = filtro + " AND cdTPersona = '"+ parametrosBusquedaCasoDTO.getCdTPersona().toUpperCase()+"' ";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdTipologia() != null && !parametrosBusquedaCasoDTO.getCdTipologia().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " cdTipologia = '"+ parametrosBusquedaCasoDTO.getCdTipologia().toUpperCase()+"' ";
				 }else{
					 filtro = filtro + " AND cdTipologia = '"+ parametrosBusquedaCasoDTO.getCdTipologia().toUpperCase()+"' ";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdSegmento() != null && !parametrosBusquedaCasoDTO.getCdSegmento().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " cdSegmento = '"+ parametrosBusquedaCasoDTO.getCdSegmento().toUpperCase()+"' ";
				 }else{
					 filtro = filtro + " AND cdSegmento = '"+ parametrosBusquedaCasoDTO.getCdSegmento().toUpperCase()+"' ";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdSesion() != null && !parametrosBusquedaCasoDTO.getCdSesion().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " cdSesion = '"+ parametrosBusquedaCasoDTO.getCdSesion()+"' ";
				 }else{
					 filtro = filtro + " AND cdSesion = '"+ parametrosBusquedaCasoDTO.getCdSesion()+"' ";
				 }
				 isFiltro = true;
				 
			 }
			
			 if(parametrosBusquedaCasoDTO.getCdRespuesta() != null && !parametrosBusquedaCasoDTO.getCdRespuesta().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " cdRespuesta = '"+ parametrosBusquedaCasoDTO.getCdRespuesta()+"' ";
				 }else{
					 filtro = filtro + " AND cdRespuesta = '"+ parametrosBusquedaCasoDTO.getCdRespuesta()+"' ";
				 }
				 isFiltro = true;
			
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdEscenario() != null && !parametrosBusquedaCasoDTO.getCdEscenario().equals("")){
				 if(filtro.equals("")){
					 consulta+=" ,TSCA025_RELALRESC T25 ";
					 filtro = filtro + " (T25.CD_ALERTA = nuFolioAlerta and T25.CD_SISTEMA = cdSistema) AND T25.CD_ESCENARIO = '"+ parametrosBusquedaCasoDTO.getCdEscenario()+"' ";
				 }else{
					 consulta+=" ,TSCA025_RELALRESC T25 ";
					 filtro = filtro + " AND (T25.CD_ALERTA = nuFolioAlerta and T25.CD_SISTEMA = cdSistema) AND T25.CD_ESCENARIO = '"+ parametrosBusquedaCasoDTO.getCdEscenario()+"' ";
				 }
				 isFiltro = true;
			
			 }
			 if(parametrosBusquedaCasoDTO.getNuCalificacion() != null && !parametrosBusquedaCasoDTO.getNuCalificacion().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " nuScoreMdl  " +  parametrosBusquedaCasoDTO.getCdCalificacion()  +   parametrosBusquedaCasoDTO.getNuCalificacion();
				 }else{
					 filtro = filtro + " AND nuScoreMdl " +  parametrosBusquedaCasoDTO.getCdCalificacion()  +   parametrosBusquedaCasoDTO.getNuCalificacion();
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getNuPtsMatriz() != null && !parametrosBusquedaCasoDTO.getNuPtsMatriz().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " nuScoreMc " + parametrosBusquedaCasoDTO.getCdPtsMatriz() + parametrosBusquedaCasoDTO.getNuPtsMatriz();
				 }else{
					 filtro = filtro + " AND nuScoreMc " + parametrosBusquedaCasoDTO.getCdPtsMatriz() + parametrosBusquedaCasoDTO.getNuPtsMatriz();
				 }
				 isFiltro = true;				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getImImporte() != null  && !parametrosBusquedaCasoDTO.getImImporte().equals("")){
				 if(filtro.equals("")){
					 filtro = filtro + " imAlerta " + parametrosBusquedaCasoDTO.getCdImporte()  + parametrosBusquedaCasoDTO.getImImporte();
				 }else{
					 filtro = filtro + " AND imAlerta "  + parametrosBusquedaCasoDTO.getCdImporte() +  parametrosBusquedaCasoDTO.getImImporte();
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhAlertaDel() != null && !parametrosBusquedaCasoDTO.getFhAlertaDel().equals("") || parametrosBusquedaCasoDTO.getFhAlertaAl() != null && !parametrosBusquedaCasoDTO.getFhAlertaAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhAlertaDel() != null && !parametrosBusquedaCasoDTO.getFhAlertaDel().equals("") && parametrosBusquedaCasoDTO.getFhAlertaAl() != null && !parametrosBusquedaCasoDTO.getFhAlertaAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhAlerta BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhAlerta BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhAlertaDel() != null && !parametrosBusquedaCasoDTO.getFhAlertaDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhAlertaAl() != null && !parametrosBusquedaCasoDTO.getFhAlertaAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhEnvioDel() != null && !parametrosBusquedaCasoDTO.getFhEnvioDel().equals("") || parametrosBusquedaCasoDTO.getFhEnvioAl() != null && !parametrosBusquedaCasoDTO.getFhEnvioAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhEnvioDel() != null && !parametrosBusquedaCasoDTO.getFhEnvioDel().equals("") && parametrosBusquedaCasoDTO.getFhEnvioAl() != null && !parametrosBusquedaCasoDTO.getFhEnvioAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhEnvio BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhEnvio BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhEnvioDel() != null && !parametrosBusquedaCasoDTO.getFhEnvioDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhEnvioAl() != null && !parametrosBusquedaCasoDTO.getFhEnvioAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhVencRevisionDel() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionDel().equals("") || parametrosBusquedaCasoDTO.getFhVencRevisionAl() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhVencRevisionDel() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionDel().equals("") && parametrosBusquedaCasoDTO.getFhVencRevisionAl() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhVenRev BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhVenRev BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhVencRevisionDel() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhVencRevisionAl() != null && !parametrosBusquedaCasoDTO.getFhVencRevisionAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhVencLiberacionDel() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionDel().equals("") || parametrosBusquedaCasoDTO.getFhVencLiberacionAl() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhVencLiberacionDel() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionDel().equals("") && parametrosBusquedaCasoDTO.getFhVencLiberacionAl() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhVenLibSia BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhVenLibSia BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhVencLiberacionDel() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhVencLiberacionAl() != null && !parametrosBusquedaCasoDTO.getFhVencLiberacionAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getFhVencReporteDel() != null && !parametrosBusquedaCasoDTO.getFhVencReporteDel().equals("") || parametrosBusquedaCasoDTO.getFhVencReporteAl() != null && !parametrosBusquedaCasoDTO.getFhVencReporteAl().equals("")){
				 if(parametrosBusquedaCasoDTO.getFhVencReporteDel() != null && !parametrosBusquedaCasoDTO.getFhVencReporteDel().equals("") && parametrosBusquedaCasoDTO.getFhVencReporteAl() != null && !parametrosBusquedaCasoDTO.getFhVencReporteAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhRepAut BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhRepAut BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhVencReporteDel() != null && !parametrosBusquedaCasoDTO.getFhVencReporteDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhVencReporteAl() != null && !parametrosBusquedaCasoDTO.getFhVencReporteAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro +  " fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getFhAsignacionDel() != null && !parametrosBusquedaCasoDTO.getFhAsignacionDel().equals("") || parametrosBusquedaCasoDTO.getFhAsignacionAl() != null && !parametrosBusquedaCasoDTO.getFhAsignacionAl().equals("") ){
				 if(parametrosBusquedaCasoDTO.getFhAsignacionDel() != null && !parametrosBusquedaCasoDTO.getFhAsignacionDel().equals("") && parametrosBusquedaCasoDTO.getFhAsignacionAl() != null && !parametrosBusquedaCasoDTO.getFhAsignacionAl().equals("")){
					 if(filtro.equals("")){
						 filtro = filtro + " fhAsignacion BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND fhAsignacion BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhAsignacionDel() != null && !parametrosBusquedaCasoDTO.getFhAsignacionDel().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhAsignacionAl() != null && !parametrosBusquedaCasoDTO.getFhAsignacionAl().equals("")){
						 if(filtro.equals("")){
							 filtro = filtro + " fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAsignacionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
			 }
		 }
		 
		 if(isFiltro){
				consulta = consulta+condicion+filtro;
			 }
		 
		 Query query = em.createNativeQuery(consulta);
		 query.setParameter(1,cdConsultor);
		 
		 Integer conteo = ((Number) query.getSingleResult()).intValue();
	 
		return conteo;
	}
}

