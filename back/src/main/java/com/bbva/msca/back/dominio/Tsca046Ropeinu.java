package com.bbva.msca.back.dominio;

import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(versionField = "", table = "TSCA046_ROPEINU", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca046Ropeinu {
	
	@Id
	@NotNull
	@SequenceGenerator(name = "SQ_TSCA046", sequenceName = "SQ_TSCA046", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TSCA046")
	@Column(name = "CD_REG_OP_INU")
	private BigDecimal cdRegOpInu;
	
	@Column(name = "CD_CASO")
	private BigDecimal cdCaso;
	
	@Column(name = "TP_CTA")
	private BigDecimal cdTpCta;
	
	@Column(name = "TP_OPERAC")
	private String cdTpOperacion;
	
	@Column(name = "CD_CAU_OP_REG")
	private BigDecimal cdCauOpReg;
	
	@Column(name = "NB_RAZ_DES")
	private String nbRazDes;
}
