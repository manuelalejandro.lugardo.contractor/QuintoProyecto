package com.bbva.msca.back.dto;

import java.util.Date;

public class DetalleTipologiaDTO {
	
	private Integer nuPeriodo;
	private Integer nuConsecutivo;
	private String nbAccion;
	private String nbActividad;
	private String nbTipoPersona;
	private String nbOficina;
	private String nbBanca;
	private Integer nuScore;
	private String tMovimiento;
	private String nbIndicador;
	private Integer nuScoreCorto;
	private String nbIndicadorPEP;
	private String nbIndicadorC;
	private Integer nuAlertas;
	private double imEfectivoR;
	private Date fhAlta;
	private String nbActividadCliente;
	private String nbEnvInv;
	private Integer nuCalificacion;
	private String nbIndZonAltRie;
	
	
	public Integer getNuPeriodo() {
		return nuPeriodo;
	}
	public void setNuPeriodo(Integer nuPeriodo) {
		this.nuPeriodo = nuPeriodo;
	}
	public Integer getNuConsecutivo() {
		return nuConsecutivo;
	}
	public void setNuConsecutivo(Integer nuConsecutivo) {
		this.nuConsecutivo = nuConsecutivo;
	}
	public String getNbAccion() {
		return nbAccion;
	}
	public void setNbAccion(String nbAccion) {
		this.nbAccion = nbAccion;
	}
	public String getNbActividad() {
		return nbActividad;
	}
	public void setNbActividad(String nbActividad) {
		this.nbActividad = nbActividad;
	}
	public String getNbTipoPersona() {
		return nbTipoPersona;
	}
	public void setNbTipoPersona(String nbTipoPersona) {
		this.nbTipoPersona = nbTipoPersona;
	}
	public String getNbOficina() {
		return nbOficina;
	}
	public void setNbOficina(String nbOficina) {
		this.nbOficina = nbOficina;
	}
	public String getNbBanca() {
		return nbBanca;
	}
	public void setNbBanca(String nbBanca) {
		this.nbBanca = nbBanca;
	}
	public Integer getNuScore() {
		return nuScore;
	}
	public void setNuScore(Integer nuScore) {
		this.nuScore = nuScore;
	}
	public String gettMovimiento() {
		return tMovimiento;
	}
	public void settMovimiento(String tMovimiento) {
		this.tMovimiento = tMovimiento;
	}
	public String getNbIndicador() {
		return nbIndicador;
	}
	public void setNbIndicador(String nbIndicador) {
		this.nbIndicador = nbIndicador;
	}
	public Integer getNuScoreCorto() {
		return nuScoreCorto;
	}
	public void setNuScoreCorto(Integer nuScoreCorto) {
		this.nuScoreCorto = nuScoreCorto;
	}
	public String getNbIndicadorPEP() {
		return nbIndicadorPEP;
	}
	public void setNbIndicadorPEP(String nbIndicadorPEP) {
		this.nbIndicadorPEP = nbIndicadorPEP;
	}
	public String getNbIndicadorC() {
		return nbIndicadorC;
	}
	public void setNbIndicadorC(String nbIndicadorC) {
		this.nbIndicadorC = nbIndicadorC;
	}
	public Integer getNuAlertas() {
		return nuAlertas;
	}
	public void setNuAlertas(Integer nuAlertas) {
		this.nuAlertas = nuAlertas;
	}
	public double getImEfectivoR() {
		return imEfectivoR;
	}
	public void setImEfectivoR(double imEfectivoR) {
		this.imEfectivoR = imEfectivoR;
	}
	public Date getFhAlta() {
		return fhAlta;
	}
	public void setFhAlta(Date fhAlta) {
		this.fhAlta = fhAlta;
	}
	public String getNbActividadCliente() {
		return nbActividadCliente;
	}
	public void setNbActividadCliente(String nbActividadCliente) {
		this.nbActividadCliente = nbActividadCliente;
	}
	public String getNbEnvInv() {
		return nbEnvInv;
	}
	public void setNbEnvInv(String nbEnvInv) {
		this.nbEnvInv = nbEnvInv;
	}
	public Integer getNuCalificacion() {
		return nuCalificacion;
	}
	public void setNuCalificacion(Integer nuCalificacion) {
		this.nuCalificacion = nuCalificacion;
	}
	public String getNbIndZonAltRie() {
		return nbIndZonAltRie;
	}
	public void setNbIndZonAltRie(String nbIndZonAltRie) {
		this.nbIndZonAltRie = nbIndZonAltRie;
	}

}
