package com.bbva.msca.back.dto;

import java.math.BigDecimal;


public class MatrizDTO {
	
	private BigDecimal cdMatCrit;
	private String nbMatCrit;
	private String cdTipologia;
	private BigDecimal cdCriterio;
	private String cdSector;
	private BigDecimal nuRangoMin;
	private BigDecimal nuRangoMax;
	private String nbValor;
	private String nbDescripcion;
	private BigDecimal nuPuntos;
	private String nbDescTip;
	private String nbCriterio;
	private String nbSector;
	private String cdStSistema;
	private String cdUsuario;
	
	public void setCdMatCrit(BigDecimal cdMatCrit) {
		this.cdMatCrit = cdMatCrit;
	}
	public BigDecimal getCdMatCrit() {
		return cdMatCrit;
	}
	public void setNbMatCrit(String nbMatCrit) {
		this.nbMatCrit = nbMatCrit;
	}
	public String getNbMatCrit() {
		return nbMatCrit;
	}
	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	public String getCdTipologia() {
		return cdTipologia;
	}
	public void setCdCriterio(BigDecimal cdCriterio) {
		this.cdCriterio = cdCriterio;
	}
	public BigDecimal getCdCriterio() {
		return cdCriterio;
	}
	public void setCdSector(String cdSector) {
		this.cdSector = cdSector;
	}
	public String getCdSector() {
		return cdSector;
	}
	public void setNuRangoMin(BigDecimal nuRangoMin) {
		this.nuRangoMin = nuRangoMin;
	}
	public BigDecimal getNuRangoMin() {
		return nuRangoMin;
	}
	public void setNuRangoMax(BigDecimal nuRangoMax) {
		this.nuRangoMax = nuRangoMax;
	}
	public BigDecimal getNuRangoMax() {
		return nuRangoMax;
	}
	public void setNbValor(String nbValor) {
		this.nbValor = nbValor;
	}
	public String getNbValor() {
		return nbValor;
	}
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	public void setNuPuntos(BigDecimal nuPuntos) {
		this.nuPuntos = nuPuntos;
	}
	public BigDecimal getNuPuntos() {
		return nuPuntos;
	}
	public void setNbDescTip(String nbDescTip) {
		this.nbDescTip = nbDescTip;
	}
	public String getNbDescTip() {
		return nbDescTip;
	}
	public void setNbCriterio(String nbCriterio) {
		this.nbCriterio = nbCriterio;
	}
	public String getNbCriterio() {
		return nbCriterio;
	}
	public void setNbSector(String nbSector) {
		this.nbSector = nbSector;
	}
	public String getNbSector() {
		return nbSector;
	}
	public void setCdStSistema(String cdStSistema) {
		this.cdStSistema = cdStSistema;
	}
	public String getCdStSistema() {
		return cdStSistema;
	}
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}
	public String getCdUsuario() {
		return cdUsuario;
	}
		
}
