package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class AsigTipologiaDTO extends TipologiaDTO {

	private BigDecimal cdAsigTipologia;
	private String cdSegmento;
	private String cdTpAsignacion;
	private String cdUsrConsultor;
	private String cdPeriodicidad;
	private BigDecimal nuTopeAlertas;
	private String nbConsultor;
	private String nbPeriodicidad;
	private String nbTpAsignacion;
	private String nbSegmento; 
	
	public void setCdAsigTipologia(BigDecimal cdAsigTipologia) {
		this.cdAsigTipologia = cdAsigTipologia;
	}
	public BigDecimal getCdAsigTipologia() {
		return cdAsigTipologia;
	}
	public void setCdSegmento(String cdSegmento) {
		this.cdSegmento = cdSegmento;
	}
	public String getCdSegmento() {
		return cdSegmento;
	}
	public void setCdTpAsignacion(String cdTpAsignacion) {
		this.cdTpAsignacion = cdTpAsignacion;
	}
	public String getCdTpAsignacion() {
		return cdTpAsignacion;
	}
	public void setCdUsrConsultor(String cdUsrConsultor) {
		this.cdUsrConsultor = cdUsrConsultor;
	}
	public String getCdUsrConsultor() {
		return cdUsrConsultor;
	}
	public void setCdPeriodicidad(String cdPeriodicidad) {
		this.cdPeriodicidad = cdPeriodicidad;
	}
	public String getCdPeriodicidad() {
		return cdPeriodicidad;
	}
	public void setNuTopeAlertas(BigDecimal nuTopeAlertas) {
		this.nuTopeAlertas = nuTopeAlertas;
	}
	public BigDecimal getNuTopeAlertas() {
		return nuTopeAlertas;
	}
	public void setNbPeriodicidad(String nbPeriodicidad) {
		this.nbPeriodicidad = nbPeriodicidad;
	}
	public String getNbPeriodicidad() {
		return nbPeriodicidad;
	}
	public void setNbConsultor(String nbConsultor) {
		this.nbConsultor = nbConsultor;
	}
	public String getNbConsultor() {
		return nbConsultor;
	}
	public void setNbSegmento(String nbSegmento) {
		this.nbSegmento = nbSegmento;
	}
	public String getNbSegmento() {
		return nbSegmento;
	}
	public void setNbTpAsignacion(String nbTpAsignacion) {
		this.nbTpAsignacion = nbTpAsignacion;
	}
	public String getNbTpAsignacion() {
		return nbTpAsignacion;
	}

}
