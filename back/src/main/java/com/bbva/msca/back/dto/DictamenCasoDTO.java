package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class DictamenCasoDTO {
	
	private BigDecimal cdRegOpInu;
	private BigDecimal tCuentas;
	private String tMonetario;
	private String iMonetario;
	private String moneda;
	private BigDecimal cOperacion;
	private String stCuentaCancelada;
	private String txtcomentario;
	private String txtcomentario1;
	private String cdDictamen;
	private BigDecimal causasRegresarConsultorSelect;
	private String razonesRegreso;
	
	private BigDecimal liberadosSIA;
	private BigDecimal topeCasos;
	
	private String otrasGerencias;
	
	
	public BigDecimal getCausasRegresarConsultorSelect() {
		return causasRegresarConsultorSelect;
	}
	public void setCausasRegresarConsultorSelect(
			BigDecimal causasRegresarConsultorSelect) {
		this.causasRegresarConsultorSelect = causasRegresarConsultorSelect;
	}
	public String getRazonesRegreso() {
		return razonesRegreso;
	}
	public void setRazonesRegreso(String razonesRegreso) {
		this.razonesRegreso = razonesRegreso;
	}
	public String getOtrasGerencias() {
		return otrasGerencias;
	}
	public void setOtrasGerencias(String otrasGerencias) {
		this.otrasGerencias = otrasGerencias;
	}
	public BigDecimal getLiberadosSIA() {
		return liberadosSIA;
	}
	public void setLiberadosSIA(BigDecimal liberadosSIA) {
		this.liberadosSIA = liberadosSIA;
	}
	public BigDecimal getTopeCasos() {
		return topeCasos;
	}
	public void setTopeCasos(BigDecimal topeCasos) {
		this.topeCasos = topeCasos;
	}
	public String getStCuentaCancelada() {
		return stCuentaCancelada;
	}
	public void setStCuentaCancelada(String stCuentaCancelada) {
		this.stCuentaCancelada = stCuentaCancelada;
	}
	public String getCdDictamen() {
		return cdDictamen;
	}
	public void setCdDictamen(String cdDictamen) {
		this.cdDictamen = cdDictamen;
	}
	public BigDecimal getCdRegOpInu() {
		return cdRegOpInu;
	}
	public void setCdRegOpInu(BigDecimal cdRegOpInu) {
		this.cdRegOpInu = cdRegOpInu;
	}
	public BigDecimal gettCuentas() {
		return tCuentas;
	}
	public void settCuentas(BigDecimal tCuentas) {
		this.tCuentas = tCuentas;
	}
	public String gettMonetario() {
		return tMonetario;
	}
	public void settMonetario(String tMonetario) {
		this.tMonetario = tMonetario;
	}
	public String getiMonetario() {
		return iMonetario;
	}
	public void setiMonetario(String iMonetario) {
		this.iMonetario = iMonetario;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	public BigDecimal getcOperacion() {
		return cOperacion;
	}
	public void setcOperacion(BigDecimal cOperacion) {
		this.cOperacion = cOperacion;
	}
	public String getTxtcomentario() {
		return txtcomentario;
	}
	public void setTxtcomentario(String txtcomentario) {
		this.txtcomentario = txtcomentario;
	}
	public String getTxtcomentario1() {
		return txtcomentario1;
	}
	public void setTxtcomentario1(String txtcomentario1) {
		this.txtcomentario1 = txtcomentario1;
	}
	
}
