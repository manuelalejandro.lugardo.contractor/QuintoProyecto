package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(identifierType = Tsca012ParametroPK.class, versionField = "", table = "TSCA012_PARAMETRO", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca012Parametro {
}
