package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class TranferenciasInternacionalesRecibidasDTO {
	
	private String nbReferencia;
	private String nuCuentas;
	private Date fhOperacion;
	private String nbTipoTransaccion;
	private String nbCorresponsal;
	private String nbEntidadOrigen;
	private String nbEntidadDestino;
	private String nbDatoDestino;
	private String nbDatoOrdenante;
	private String nbOperacionOrigen;
	private String nbOperacionDestino;
	private BigDecimal nbMoneda;
	private String imInstrumento;
	private String imDolarizado;
	private String nbPaisFIS;
	private String nbRazonSocial;
	private String paisOrigenOIR;
	private String riesgoOIR;
	private String paraisoFiscalOIR;
	private BigDecimal montoTrans13OIR;
	private BigDecimal nuOperaciones13OIR;
	private BigDecimal montoTrans14OIR;
	private BigDecimal nuOperaciones14OIR;
	private BigDecimal montoTrans15OIR;
	private BigDecimal nuOperaciones15OIR;
	private BigDecimal montoTransTotOIR;
	private BigDecimal nuOperacionesTotOIR;
	
		
	public String getNbReferencia() {
		return nbReferencia;
	}
	public void setNbReferencia(String nbReferencia) {
		this.nbReferencia = nbReferencia;
	}
	public String getNuCuentas() {
		return nuCuentas;
	}
	public void setNuCuentas(String nuCuentas) {
		this.nuCuentas = nuCuentas;
	}
	public Date getFhOperacion() {
		return fhOperacion;
	}
	public void setFhOperacion(Date fhOperacion) {
		this.fhOperacion = fhOperacion;
	}
	public String getNbTipoTransaccion() {
		return nbTipoTransaccion;
	}
	public void setNbTipoTransaccion(String nbTipoTransaccion) {
		this.nbTipoTransaccion = nbTipoTransaccion;
	}
	public String getNbCorresponsal() {
		return nbCorresponsal;
	}
	public void setNbCorresponsal(String nbCorresponsal) {
		this.nbCorresponsal = nbCorresponsal;
	}
	public String getNbEntidadOrigen() {
		return nbEntidadOrigen;
	}
	public void setNbEntidadOrigen(String nbEntidadOrigen) {
		this.nbEntidadOrigen = nbEntidadOrigen;
	}
	public String getNbEntidadDestino() {
		return nbEntidadDestino;
	}
	public void setNbEntidadDestino(String nbEntidadDestino) {
		this.nbEntidadDestino = nbEntidadDestino;
	}
	public String getNbDatoDestino() {
		return nbDatoDestino;
	}
	public void setNbDatoDestino(String nbDatoDestino) {
		this.nbDatoDestino = nbDatoDestino;
	}
	public String getNbDatoOrdenante() {
		return nbDatoOrdenante;
	}
	public void setNbDatoOrdenante(String nbDatoOrdenante) {
		this.nbDatoOrdenante = nbDatoOrdenante;
	}
	public String getNbOperacionOrigen() {
		return nbOperacionOrigen;
	}
	public void setNbOperacionOrigen(String nbOperacionOrigen) {
		this.nbOperacionOrigen = nbOperacionOrigen;
	}
	public String getNbOperacionDestino() {
		return nbOperacionDestino;
	}
	public void setNbOperacionDestino(String nbOperacionDestino) {
		this.nbOperacionDestino = nbOperacionDestino;
	}
	public BigDecimal getNbMoneda() {
		return nbMoneda;
	}
	public void setNbMoneda(BigDecimal nbMoneda) {
		this.nbMoneda = nbMoneda;
	}
	public String getImInstrumento() {
		return imInstrumento;
	}
	public void setImInstrumento(String imInstrumento) {
		this.imInstrumento = imInstrumento;
	}
	public String getImDolarizado() {
		return imDolarizado;
	}
	public void setImDolarizado(String imDolarizado) {
		this.imDolarizado = imDolarizado;
	}
	public String getNbPaisFIS() {
		return nbPaisFIS;
	}
	public void setNbPaisFIS(String nbPaisFIS) {
		this.nbPaisFIS = nbPaisFIS;
	}
	public String getNbRazonSocial() {
		return nbRazonSocial;
	}
	public void setNbRazonSocial(String nbRazonSocial) {
		this.nbRazonSocial = nbRazonSocial;
	}
	public String getPaisOrigenOIR() {
		return paisOrigenOIR;
	}
	public void setPaisOrigenOIR(String paisOrigenOIR) {
		this.paisOrigenOIR = paisOrigenOIR;
	}
	 
	public String getRiesgoOIR() {
		return riesgoOIR;
	}
	public void setRiesgoOIR(String riesgoOIR) {
		this.riesgoOIR = riesgoOIR;
	}
	public String getParaisoFiscalOIR() {
		return paraisoFiscalOIR;
	}
	public void setParaisoFiscalOIR(String paraisoFiscalOIR) {
		this.paraisoFiscalOIR = paraisoFiscalOIR;
	}
	public BigDecimal getMontoTrans13OIR() {
		return montoTrans13OIR;
	}
	public void setMontoTrans13OIR(BigDecimal montoTrans13OIR) {
		this.montoTrans13OIR = montoTrans13OIR;
	}
	public BigDecimal getNuOperaciones13OIR() {
		return nuOperaciones13OIR;
	}
	public void setNuOperaciones13OIR(BigDecimal nuOperaciones13OIR) {
		this.nuOperaciones13OIR = nuOperaciones13OIR;
	}
	public BigDecimal getMontoTrans14OIR() {
		return montoTrans14OIR;
	}
	public void setMontoTrans14OIR(BigDecimal montoTrans14OIR) {
		this.montoTrans14OIR = montoTrans14OIR;
	}
	public BigDecimal getNuOperaciones14OIR() {
		return nuOperaciones14OIR;
	}
	public void setNuOperaciones14OIR(BigDecimal nuOperaciones14OIR) {
		this.nuOperaciones14OIR = nuOperaciones14OIR;
	}
	public BigDecimal getMontoTrans15OIR() {
		return montoTrans15OIR;
	}
	public void setMontoTrans15OIR(BigDecimal montoTrans15OIR) {
		this.montoTrans15OIR = montoTrans15OIR;
	}
	public BigDecimal getNuOperaciones15OIR() {
		return nuOperaciones15OIR;
	}
	public void setNuOperaciones15OIR(BigDecimal nuOperaciones15OIR) {
		this.nuOperaciones15OIR = nuOperaciones15OIR;
	}
	public BigDecimal getMontoTransTotOIR() {
		return montoTransTotOIR;
	}
	public void setMontoTransTotOIR(BigDecimal montoTransTotOIR) {
		this.montoTransTotOIR = montoTransTotOIR;
	}
	public BigDecimal getNuOperacionesTotOIR() {
		return nuOperacionesTotOIR;
	}
	public void setNuOperacionesTotOIR(BigDecimal nuOperacionesTotOIR) {
		this.nuOperacionesTotOIR = nuOperacionesTotOIR;
	}

}
