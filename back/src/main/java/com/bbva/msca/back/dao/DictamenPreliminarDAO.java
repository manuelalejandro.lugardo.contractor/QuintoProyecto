package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.dominio.Tsca014Caso;
import com.bbva.msca.back.dominio.Tsca031Dictamen;
import com.bbva.msca.back.dominio.Tsca040InstMon;
import com.bbva.msca.back.dominio.Tsca045CauOpeinu;
import com.bbva.msca.back.dominio.Tsca046Ropeinu;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dto.DictamenCasoDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

@Component
public class DictamenPreliminarDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	//Agregar Dictamen Preliminar 
	
	@Transactional
	public int addDictamenPreliminar(BigDecimal cdCaso,DictamenCasoDTO dictamenCasoDTO) throws Exception{
		int op = Constantes.FIN_ER;
		Tsca046Ropeinu dictamenPreliminar = null;
		Tsca055Log log =  new Tsca055Log();
		
			Tsca014Caso tsca014CasoAlerta = Tsca014Caso.findTsca014Caso(cdCaso);
			
			if(tsca014CasoAlerta != null){
				
				if(tsca014CasoAlerta.getCdDictamenPre() != null){
					
					dictamenPreliminar = getDictamen(tsca014CasoAlerta.getCdCaso());
					
					dictamenPreliminar.setCdTpCta(dictamenCasoDTO.gettCuentas());
					dictamenPreliminar.setCdTpOperacion(dictamenCasoDTO.gettMonetario());
					dictamenPreliminar.setCdInsMon(Tsca040InstMon.findTsca040InstMon(dictamenCasoDTO.getiMonetario()));
					dictamenPreliminar.setCdCauOpeInu(Tsca045CauOpeinu.findTsca045CauOpeinu(dictamenCasoDTO.getcOperacion()));
					dictamenPreliminar.setCdDivisa(dictamenCasoDTO.getMoneda());
					dictamenPreliminar.setStCancelado(dictamenCasoDTO.getStCuentaCancelada());
					dictamenPreliminar.setNbDscOpeInu(dictamenCasoDTO.getTxtcomentario());
					dictamenPreliminar.setNbRazOpeInu(dictamenCasoDTO.getTxtcomentario1());
					
					
					dictamenPreliminar.merge();
					dictamenPreliminar.flush();
					
				}else{
					dictamenPreliminar = new Tsca046Ropeinu();
					
					dictamenPreliminar.setCdCaso(tsca014CasoAlerta.getCdCaso());
					dictamenPreliminar.setCdTpCta(dictamenCasoDTO.gettCuentas());
					dictamenPreliminar.setCdTpOperacion(dictamenCasoDTO.gettMonetario());
					dictamenPreliminar.setCdInsMon(Tsca040InstMon.findTsca040InstMon(dictamenCasoDTO.getiMonetario()));
					dictamenPreliminar.setCdCauOpeInu(Tsca045CauOpeinu.findTsca045CauOpeinu(dictamenCasoDTO.getcOperacion()));
					dictamenPreliminar.setCdDivisa(dictamenCasoDTO.getMoneda());
					dictamenPreliminar.setStCancelado(dictamenCasoDTO.getStCuentaCancelada());
					dictamenPreliminar.setNbDscOpeInu(dictamenCasoDTO.getTxtcomentario());
					dictamenPreliminar.setNbRazOpeInu(dictamenCasoDTO.getTxtcomentario1());
					
					dictamenPreliminar.persist();
					dictamenPreliminar.flush();
					
				}
				
				if (dictamenPreliminar != null){
					
					Tsca031Dictamen tsca031Dictamen = Tsca031Dictamen.findTsca031Dictamen(dictamenCasoDTO.getCdDictamen());
					
					if(tsca031Dictamen != null){
						tsca014CasoAlerta.setCdDictamenPre(tsca031Dictamen.getCdDictamen());//JANROME
						tsca014CasoAlerta.setFhDictamenPre(new Date());
						tsca014CasoAlerta.setStCaso(Constantes.CASO_EN_SUPERVISION);
						
						tsca014CasoAlerta.merge();
						tsca014CasoAlerta.flush();
						
						log.setNbEvento("DICTAMEN PRELIMINAR");
						log.setCdUsuario(tsca014CasoAlerta.getCdUsuario());
						
						//log.setFhFecha(new Timestamp(new Date().getTime()));
						log.setFhFecha(new Timestamp(new Date().getTime()));
						
						if(tsca031Dictamen.getCdDictamen().equals(Constantes.DICTAMEN_DESCARTADO)){
							log.setNbAccionRealizad("DESCARTADO");
						}else if(tsca031Dictamen.getCdDictamen().equals(Constantes.DICTAMEN_LIBERADO_A_SIA)){
							log.setNbAccionRealizad("LIBERADO");
						}
						log.setCdCaso(tsca014CasoAlerta.getCdCaso());
						log.setCdCliente(tsca014CasoAlerta.getCdCliente());
						
						log.persist();
						log.flush();
						
						op = Constantes.FIN_OK;
					}
					op = Constantes.FIN_AV;
				}
			}
		return op;
	}
	
	@SuppressWarnings("unchecked")
	public Tsca046Ropeinu getDictamen(BigDecimal cdCaso){
		
		Tsca046Ropeinu dictamenPreliminar = null;
		List<Tsca046Ropeinu> resultado = new ArrayList<Tsca046Ropeinu>();
		try{
			
			Query query = em.createNativeQuery("SELECT * FROM TSCA046_ROPEINU WHERE CD_CASO = ?",Tsca046Ropeinu.class);
			query.setParameter(1,cdCaso);
			
			resultado = (List<Tsca046Ropeinu>) query.getResultList();
			if( resultado != null && !resultado.isEmpty() )
			{
				dictamenPreliminar = resultado.get(0);	
			}			
			
		}catch(Exception e){
			
			e.printStackTrace();
		}
		return dictamenPreliminar;
	}
	
	//Consulta Dictamenes ya Existentes
	
	@SuppressWarnings("unchecked")
	public DictamenCasoDTO getConsultaDictamenPreliminar(BigDecimal cdCaso) throws Exception{
		
		DictamenCasoDTO dictamenPreliminarConsulta = null;
		List<DictamenCasoDTO> resultado = new ArrayList<DictamenCasoDTO>(); 
		Query query = em.createNativeQuery(Consultas.getConsulta("getConsultaDictamenPreliminar"));
		query.setParameter(1,cdCaso);
		
		query.unwrap(org.hibernate.SQLQuery.class)
		
		.addScalar("cdDictamen",StringType.INSTANCE)
		.addScalar("tCuentas")
		.addScalar("cOperacion")
		.addScalar("moneda",StringType.INSTANCE)
		.addScalar("iMonetario",StringType.INSTANCE)
		.addScalar("tMonetario",StringType.INSTANCE)
		.addScalar("txtcomentario")
		.addScalar("txtcomentario1")
		.addScalar("causasRegresarConsultorSelect")
		.addScalar("razonesRegreso")
		.addScalar("stCuentaCancelada",StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(DictamenCasoDTO.class));
		
		resultado = (List<DictamenCasoDTO>)query.getResultList();
		if( resultado != null && !resultado.isEmpty() )
		{
			dictamenPreliminarConsulta = resultado.get(0);	
		}
		
		
		return dictamenPreliminarConsulta;
	}
	
	//Consultar dictamenes liberados a SIA y Tope de Casos por sesion
	
	@SuppressWarnings("unchecked")
	public DictamenCasoDTO getConsultaDictamenesLiberados(String cdSesion, BigDecimal cdGerencia) throws Exception{
		List<DictamenCasoDTO> lstResultado = new ArrayList<DictamenCasoDTO>();
		DictamenCasoDTO dictamenesLiberados = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getConsultaDictamenesLiberados"));
		query.setParameter(1,cdSesion);
		query.setParameter(2,cdSesion);
		query.setParameter(3,cdGerencia);
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("liberadosSIA")
		.addScalar("topeCasos")
		.setResultTransformer(Transformers.aliasToBean(DictamenCasoDTO.class));
		
		lstResultado = (List<DictamenCasoDTO>)query.getResultList();
		if( lstResultado != null && !lstResultado.isEmpty() ){
			dictamenesLiberados = lstResultado.get( 0 ); 
		}
		return dictamenesLiberados;
	}
	
	
	//Agregar Dictamen Final
	
	@SuppressWarnings("null")
	@Transactional
	public int addDictamenFinal(BigDecimal cdCaso,DictamenCasoDTO dictamenCasoDTO) throws Exception{
		int op = Constantes.FIN_ER;
		Tsca046Ropeinu dictamenFinal = null;
		Tsca055Log log = new Tsca055Log();
		
			Tsca014Caso tsca014CasoAlerta = Tsca014Caso.findTsca014Caso(cdCaso);
			
			if(tsca014CasoAlerta != null){
				
				if(tsca014CasoAlerta.getCdDictamenPre() != null){
					
					dictamenFinal = getDictamen(tsca014CasoAlerta.getCdCaso());
					
					/*dictamenFinal.setCdTpCta(dictamenCasoDTO.gettCuentas());
					dictamenFinal.setCdTpOperacion(dictamenCasoDTO.gettMonetario());
					dictamenFinal.setCdInsMon(Tsca040InstMon.findTsca040InstMon(dictamenCasoDTO.getiMonetario()));
					dictamenFinal.setCdCauOpeInu(Tsca045CauOpeinu.findTsca045CauOpeinu(dictamenCasoDTO.getcOperacion()));
					dictamenFinal.setCdDivisa(dictamenCasoDTO.getMoneda());
					dictamenFinal.setStCancelado(dictamenCasoDTO.getStCuentaCancelada());
					dictamenFinal.setNbDscOpeInu(dictamenCasoDTO.getTxtcomentario());
					dictamenFinal.setNbRazOpeInu(dictamenCasoDTO.getTxtcomentario1());
					
					dictamenFinal.merge();
					dictamenFinal.flush();*/				
				}
				
				if (dictamenFinal != null){
					
					Tsca031Dictamen tsca031Dictamen = Tsca031Dictamen.findTsca031Dictamen(dictamenCasoDTO.getCdDictamen());
					
					if(tsca031Dictamen != null){
						tsca014CasoAlerta.setCdDictamenFin(tsca031Dictamen.getCdDictamen());
						tsca014CasoAlerta.setFhDictamenFin(new Date());
						
						if(tsca031Dictamen.getCdDictamen().equals(Constantes.DICTAMEN_LIBERADO_A_SIA)){
							
							dictamenFinal.setCdTpCta(dictamenCasoDTO.gettCuentas());
							dictamenFinal.setCdTpOperacion(dictamenCasoDTO.gettMonetario());
							dictamenFinal.setCdInsMon(Tsca040InstMon.findTsca040InstMon(dictamenCasoDTO.getiMonetario()));
							dictamenFinal.setCdCauOpeInu(Tsca045CauOpeinu.findTsca045CauOpeinu(dictamenCasoDTO.getcOperacion()));
							dictamenFinal.setCdDivisa(dictamenCasoDTO.getMoneda());
							dictamenFinal.setStCancelado(dictamenCasoDTO.getStCuentaCancelada());
							dictamenFinal.setNbDscOpeInu(dictamenCasoDTO.getTxtcomentario());
							dictamenFinal.setNbRazOpeInu(dictamenCasoDTO.getTxtcomentario1());
							
							dictamenFinal.merge();
							dictamenFinal.flush();
							
							
							tsca014CasoAlerta.setStCaso(Constantes.CASO_LIBERADO_A_SIA);
							
							log.setNbEvento("DICTAMEN FINAL");
							log.setCdUsuario(tsca014CasoAlerta.getCdUsuario());
							log.setFhFecha(new Timestamp(new Date().getTime()));
							log.setNbAccionRealizad("LIBERADO");
							log.setCdCaso(tsca014CasoAlerta.getCdCaso());
							log.setCdCliente(tsca014CasoAlerta.getCdCliente());
							
							log.persist();
							log.flush();
							
						} else if(tsca031Dictamen.getCdDictamen().equals(Constantes.DICTAMEN_DESCARTADO)){
							
							dictamenFinal.setCdTpCta(dictamenCasoDTO.gettCuentas());
							dictamenFinal.setCdTpOperacion(dictamenCasoDTO.gettMonetario());
							dictamenFinal.setCdInsMon(Tsca040InstMon.findTsca040InstMon(dictamenCasoDTO.getiMonetario()));
							dictamenFinal.setCdCauOpeInu(Tsca045CauOpeinu.findTsca045CauOpeinu(dictamenCasoDTO.getcOperacion()));
							dictamenFinal.setCdDivisa(dictamenCasoDTO.getMoneda());
							dictamenFinal.setStCancelado(dictamenCasoDTO.getStCuentaCancelada());
							dictamenFinal.setNbDscOpeInu(dictamenCasoDTO.getTxtcomentario());
							dictamenFinal.setNbRazOpeInu(dictamenCasoDTO.getTxtcomentario1());
							
							dictamenFinal.merge();
							dictamenFinal.flush();
							
							
							tsca014CasoAlerta.setStCaso(Constantes.CASO_DESCARTADO);
							
							log.setNbEvento("DICTAMEN FINAL");
							log.setCdUsuario(tsca014CasoAlerta.getCdUsuario());
							log.setFhFecha(new Timestamp(new Date().getTime()));
							log.setNbAccionRealizad("DESCARTADO" );
							log.setCdCaso(tsca014CasoAlerta.getCdCaso());
							log.setCdCliente(tsca014CasoAlerta.getCdCliente());
							
							log.persist();
							log.flush();
							
						}else if(tsca031Dictamen.getCdDictamen().equals(Constantes.DICTAMEN_REGRESAR_A_CONSULTOR)){
							
							dictamenFinal.setCdCauOpReg(dictamenCasoDTO.getCausasRegresarConsultorSelect());
							dictamenFinal.setNbRazDes(dictamenCasoDTO.getRazonesRegreso());
							
							tsca014CasoAlerta.setStCaso(Constantes.CASO_EN_REANALISIS);
							tsca014CasoAlerta.setCdDictamenPre(tsca031Dictamen.getCdDictamen());
							tsca014CasoAlerta.setCdDictamenFin(null);
							tsca014CasoAlerta.setFhDictamenFin(null);
							
							log.setNbEvento("DICTAMEN FINAL");
							log.setCdUsuario(tsca014CasoAlerta.getCdUsuario());
							log.setFhFecha(new Timestamp(new Date().getTime()));
							log.setNbAccionRealizad("REGRESAR A CONSULTOR" );
							log.setCdCaso(tsca014CasoAlerta.getCdCaso());
							log.setCdCliente(tsca014CasoAlerta.getCdCliente());
							
							log.persist();
							log.flush();
						}
						tsca014CasoAlerta.merge();
						tsca014CasoAlerta.flush();
						op = Constantes.FIN_OK;
					}
					op = Constantes.FIN_AV;
				}
			}
		
		return op;
	}
	
	//Consulta otras gerencias para Txtcomentario
	
	@SuppressWarnings("unchecked")
	public List<DictamenCasoDTO> getConsultaDictamenPreliminarTxtComentario(BigDecimal cdCaso) throws Exception{
		
		List<DictamenCasoDTO> dictamenPreliminarConsulta = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getConsultaDictamenPreliminarTxtComentario"));
		query.setParameter(1,cdCaso);
		query.setParameter(2,cdCaso);
		
		query.unwrap(org.hibernate.SQLQuery.class)
		
		.addScalar("otrasGerencias")
		.setResultTransformer(Transformers.aliasToBean(DictamenCasoDTO.class));
		
		dictamenPreliminarConsulta = (List<DictamenCasoDTO>)query.getResultList();
		
		return dictamenPreliminarConsulta;
	}
}
