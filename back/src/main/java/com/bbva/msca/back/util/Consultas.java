package com.bbva.msca.back.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.FactoryConfigurationError;
import javax.xml.parsers.ParserConfigurationException;

import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import org.dom4j.Attribute;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.DOMReader;
import org.xml.sax.SAXException;

public class Consultas {

	private static I18nLog log = I18nLogFactory.getLogI18n(Consultas.class);

	static HashMap<String,String> mapaConsultas = null;
	

	public static String getConsulta(String noConsulta) throws Exception {
		String consulta = "";
		
		if (noConsulta != null) {
			if (mapaConsultas == null) {
				llenaMapaConsultas();
			}
			consulta = mapaConsultas.get(noConsulta);
		}		

		return consulta;
	}

	public static void llenaMapaConsultas() throws Exception {
		org.w3c.dom.Document domDocument = null;
		DOMReader lector = null;
		Document archivo = null;
		
		DocumentBuilder builder = null;
		DocumentBuilderFactory factory = null;
		
		try {
			InputStream inputStream = Consultas.class.getClassLoader().getResourceAsStream("META-INF/Consultas.xml");
			factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			builder = factory.newDocumentBuilder();
			domDocument = builder.parse(inputStream);
			lector = new DOMReader();
			archivo = lector.read(domDocument);
			leeArchivo(archivo);
		} catch (FactoryConfigurationError ex) {
			throw ex;
		} catch (ParserConfigurationException ex) {
			throw ex;
		} catch (SAXException ex) {
			throw ex;
		} catch (FileNotFoundException ex) {
			throw ex;
		} catch (IOException ex) {
			throw ex;
		}
	}

	public static void leeArchivo(Document archivo) {
		Element rootElement = null;
		Iterator listaConsultas = null;
		Iterator listaAtributos = null;
		Attribute atributo = null;
		Element nodoConsulta = null;
		String consulta = null;
		String numConsulta = null;
		rootElement = archivo.getRootElement();
		listaConsultas = rootElement.elementIterator();

		mapaConsultas = new HashMap();
		while (listaConsultas.hasNext()) {
			nodoConsulta = (Element) listaConsultas.next();
			listaAtributos = nodoConsulta.attributeIterator();
			while (listaAtributos.hasNext()) {
				atributo = (Attribute) listaAtributos.next();
				if(atributo.getQualifiedName().equalsIgnoreCase("nombre")){
					numConsulta = atributo.getValue();
				}
			}
			consulta = nodoConsulta.getText().replaceAll("&lt;", "<").replaceAll("&gt;", ">");
			log.info("Nombre consulta: " + numConsulta);
			mapaConsultas.put(numConsulta, consulta);
		}
	}

}
