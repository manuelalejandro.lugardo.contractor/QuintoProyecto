package com.bbva.msca.back.srv;

import java.math.BigDecimal;
import java.util.List;
import com.bbva.msca.back.dao.TipologiaDAO;
import com.bbva.msca.back.dto.TipologiaDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class TipologiaServiceImpl implements TipologiaService {

	@Override
	public ResponseGeneric consultarTipologia(int cdGerencia) {
		ResponseGeneric response = new ResponseGeneric();
		TipologiaDAO tipologiaDAO = new TipologiaDAO();
		List<TipologiaDTO> lstTipologiaDTO = null;
		try {
			lstTipologiaDTO = tipologiaDAO.getTipologia(cdGerencia);
			response.setResponse(lstTipologiaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public ResponseGeneric creaTipologia(TipologiaDTO tipologia) {
		ResponseGeneric response = new ResponseGeneric();
		TipologiaDAO tipologiaDAO = new TipologiaDAO();
		try {
			switch(tipologiaDAO.creaTipologia(tipologia)){
			case Constantes.FIN_OK:
				response.setResponse(tipologia);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_AV:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_02"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric modificaTipologia(TipologiaDTO tipologia) {
		ResponseGeneric response = new ResponseGeneric();
		TipologiaDAO tipologiaDAO = new TipologiaDAO();
		try {
			response.setResponse(tipologiaDAO.modificaTipologia(tipologia));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

}
