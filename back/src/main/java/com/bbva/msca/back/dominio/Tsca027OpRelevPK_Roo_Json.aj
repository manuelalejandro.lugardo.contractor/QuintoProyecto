// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca027OpRelevPK;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import java.lang.String;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

privileged aspect Tsca027OpRelevPK_Roo_Json {
    
    public String Tsca027OpRelevPK.toJson() {
        return new JSONSerializer().exclude("*.class").serialize(this);
    }
    
    public static Tsca027OpRelevPK Tsca027OpRelevPK.fromJsonToTsca027OpRelevPK(String json) {
        return new JSONDeserializer<Tsca027OpRelevPK>().use(null, Tsca027OpRelevPK.class).deserialize(json);
    }
    
    public static String Tsca027OpRelevPK.toJsonArray(Collection<Tsca027OpRelevPK> collection) {
        return new JSONSerializer().exclude("*.class").serialize(collection);
    }
    
    public static Collection<Tsca027OpRelevPK> Tsca027OpRelevPK.fromJsonArrayToTsca027OpRelevPKs(String json) {
        return new JSONDeserializer<List<Tsca027OpRelevPK>>().use(null, ArrayList.class).use("values", Tsca027OpRelevPK.class).deserialize(json);
    }
    
}
