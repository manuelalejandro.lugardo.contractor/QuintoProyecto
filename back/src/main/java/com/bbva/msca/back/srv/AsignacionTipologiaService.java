package com.bbva.msca.back.srv;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.AsigTipologiaDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/AsignacionTipologiaService")
public interface AsignacionTipologiaService {

	// PARAMETRIA - FN04 ASIGNACION POR TIPOLOGIA
	@GET
	@Path("/consultarAsigTipologias")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAsigTipologias();
	
	@GET
	@Path("/consultarAsigTipologiasGerencia/{cdGerencia}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAsigTipologiasGerencia(@PathParam("cdGerencia") Integer cdGerencia);
	
	@POST
	@Path("/altaAsigTipologia")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric creaAsigTipologia(AsigTipologiaDTO asigAsigTipologia);
	
	@POST
	@Path("/eliminarAsigTipologia")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric eliminarAsigTipologia(AsigTipologiaDTO asigAsigTipologia);	
	
	@POST
	@Path("/modificaAsigTipologia")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric modificaAsigTipologia(AsigTipologiaDTO asigAsigTipologia);
}
