package com.bbva.msca.back.dominio;

import java.math.BigDecimal;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(versionField = "", table = "TSCA014_CASO", schema = "GORAPR")
public class Tsca014Caso {
	
	@Id
	@NotNull
	@SequenceGenerator(name = "SQ_TSCA014", sequenceName = "SQ_TSCA014", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TSCA014")
	@Column(name = "CD_CASO")
	private BigDecimal cdCaso;
	
	@Column(name = "CD_USUARIO")
	private String cdUsuario;
	
	@Column(name = "ST_CASO")
	private BigDecimal stCaso;
}
