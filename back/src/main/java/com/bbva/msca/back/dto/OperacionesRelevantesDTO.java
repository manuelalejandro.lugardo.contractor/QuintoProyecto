package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class OperacionesRelevantesDTO {
	
	private String nuCuentaOPREL;
	private String tpOperacionOPREL; //char
	private BigDecimal nuOperacionesOPREL;
	private BigDecimal importeOPREL;
	private String monedaOPREL;//CHAR
	private Date fhOperacionOPREL;//DATE
	
	
	public String getNuCuentaOPREL() {
		return nuCuentaOPREL;
	}
	public void setNuCuentaOPREL(String nuCuentaOPREL) {
		this.nuCuentaOPREL = nuCuentaOPREL;
	}
	public String getTpOperacionOPREL() {
		return tpOperacionOPREL;
	}
	public void setTpOperacionOPREL(String tpOperacionOPREL) {
		this.tpOperacionOPREL = tpOperacionOPREL;
	}
	public BigDecimal getNuOperacionesOPREL() {
		return nuOperacionesOPREL;
	}
	public void setNuOperacionesOPREL(BigDecimal nuOperacionesOPREL) {
		this.nuOperacionesOPREL = nuOperacionesOPREL;
	}
	public BigDecimal getImporteOPREL() {
		return importeOPREL;
	}
	public void setImporteOPREL(BigDecimal importeOPREL) {
		this.importeOPREL = importeOPREL;
	}
	public String getMonedaOPREL() {
		return monedaOPREL;
	}
	public void setMonedaOPREL(String monedaOPREL) {
		this.monedaOPREL = monedaOPREL;
	}
	public Date getFhOperacionOPREL() {
		return fhOperacionOPREL;
	}
	public void setFhOperacionOPREL(Date fhOperacionOPREL) {
		this.fhOperacionOPREL = fhOperacionOPREL;
	}
	

}
