package com.bbva.msca.back.servicios.host.peh1;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

import com.bbva.jee.arq.spring.core.host.Cabecera;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.NombreCabecera;
import com.bbva.jee.arq.spring.core.host.RespuestaTransaccion;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;

/**
 * Bean de respuesta para la transacci&oacute;n <code>PEH1</code>
 * 
 * @see PeticionTransaccionPeh1
 * 
 * @author Arquitectura Spring BBVA
 */
@RespuestaTransaccion
@Multiformato(formatos = {FormatoPEM0H1S2.class, FormatoPEM0H1S1.class})
@RooJavaBean
@RooToString
@RooSerializable
public class RespuestaTransaccionPeh1 implements MensajeMultiparte {
	
	/**
	 * <p>Cabecera <code>COD-RETORNO</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_RETORNO)
	private String codigoRetorno;
	
		/**
	 * <p>Cabecera <code>CODIGO_CONTROL</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.CODIGO_CONTROL)
	private String codigoControl;
	
	/**
	 * <p>Cabecera <code>SECUENCIA</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.SECUENCIA)
	private String secuencia;
	
	/**
	 * <p>Cabecera <code>LONGITUD_MENSAJE</code></p>
	 */
	@Cabecera(nombre=NombreCabecera.LONGITUD_MENSAJE)
	private Integer longitudMensaje;
	

	
	/**
	 * <p>Cuerpo del mensaje de respuesta multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
