package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class NotasInvestigacionDTO { 
	
	private BigDecimal cdFolioArchivo;   
	private String cdExpediente;
	private String nbTitulo; 
	private String cdUsuario;
	private Date fhCreacion;
	private String cdUsuarioMdf;
	private Date fhModif;
	private String txInvestigacion;
	
	public String getNbTitulo() {
		return nbTitulo;
	}
	public void setNbTitulo(String nbTitulo) {
		this.nbTitulo = nbTitulo;
	}
	public String getCdUsuario() {
		return cdUsuario;
	}
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}
	public Date getFhCreacion() {
		return fhCreacion;
	}
	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}
	public String getCdUsuarioMdf() {
		return cdUsuarioMdf;
	}
	public void setCdUsuarioMdf(String cdUsuarioMdf) {
		this.cdUsuarioMdf = cdUsuarioMdf;
	}
	public Date getFhModif() {
		return fhModif;
	}
	public void setFhModif(Date fhModif) {
		this.fhModif = fhModif;
	}
	public String getTxInvestigacion() {
		return txInvestigacion;
	}
	public void setTxInvestigacion(String txInvestigacion) {
		this.txInvestigacion = txInvestigacion;
	}
	
 
	public String getCdExpediente() {
		return cdExpediente;
	}
	public void setCdExpediente(String cdExpediente) {
		this.cdExpediente = cdExpediente;
	}
	public BigDecimal getCdFolioArchivo() {
		return cdFolioArchivo;
	}
	public void setCdFolioArchivo(BigDecimal cdFolioArchivo) {
		this.cdFolioArchivo = cdFolioArchivo;
	}
}
