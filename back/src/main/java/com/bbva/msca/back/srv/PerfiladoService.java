package com.bbva.msca.back.srv;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.PathParam;

import com.bbva.msca.back.dto.UsuarioDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/PerfiladoService")
public interface PerfiladoService{
	
	@GET
	@Path("/consultarUsuario/{nbCveRed}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarUsuario(@PathParam("nbCveRed") String nbCveRed);
	
	@GET
	@Path("/getInfoEmpleado/{nbCveRed}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric getInfoEmpleado(@PathParam("nbCveRed") String nbCveRed);
	
	@GET
	@Path("/consultarUsuarios")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarUsuarios();
	
	@POST
	@Path("/altaUsuario")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric altaUsuario(UsuarioDTO usuario);
	
	@POST
	@Path("/modificaUsuario")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric modificaUsuario(UsuarioDTO usuario);

}
