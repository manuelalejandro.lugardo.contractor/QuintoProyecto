package com.bbva.msca.back.srv;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.TipoPrioridadDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/TipoPrioridadService")

public interface TipoPrioridadService {
	
	// PARAMETRIA - FN06 RELACION TIPO - PRIORIDAD
	@GET
	@Path("/consultarTipoPrioridad")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarTipoPrioridad();
	
	@POST
	@Path("/altaTipoPrioridad")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric creaTipoPrioridad(TipoPrioridadDTO tipoPrioridad);
	
	@POST
	@Path("/modificaTipoPrioridad")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric modificaTipoPrioridad(TipoPrioridadDTO tipoPrioridad);
	
	@GET
	@Path("/consultarPrioridadPorTipo/{tipoDoc}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarPrioridadPorTipo(@PathParam("tipoDoc") String tipoDoc);

}
