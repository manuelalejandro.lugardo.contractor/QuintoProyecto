package com.bbva.msca.back.servicios.sesion;

import org.apache.commons.logging.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.comunicaciones.Conexion;
import com.bbva.jee.arq.spring.core.comunicaciones.ExcepcionComunicaciones;
import com.bbva.jee.arq.spring.core.comunicaciones.ServicioComunicaciones;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.ExcepcionPS9;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.InicializadorSesion;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.ibm.mq.MQMessage;

@Component
public class InicializadorSesionPS9 implements InicializadorSesion {
	
	private static final Log LOG = I18nLogFactory.getLog(InicializadorSesionPS9.class);
	private static final String NOMBRE_CONFIGURACION_COMUNICACIONES = "configuracion_mq_inicializadorSesionPS9";
	
	@Autowired
	private ServicioComunicaciones servicioComunicaciones;
	
	@Override
	public String getCodigoSesion() throws ExcepcionPS9 {
		
		MQMessage peticion = new MQMessage();
		peticion.applicationOriginData = "LOGN";
		Conexion<MQMessage> conexion = null;
		try {
			conexion = servicioComunicaciones.crearConexion(NOMBRE_CONFIGURACION_COMUNICACIONES, MQMessage.class);
			MQMessage respuesta = conexion.enviarRecibir(peticion);
			String idASTA = respuesta.applicationIdData.trim();
			if ( LOG.isInfoEnabled() ) LOG.info("Obtenido codigo de sesion: " + idASTA);			
			return idASTA;
		} catch ( ExcepcionComunicaciones e ) {
			LOG.error("Error de comunicaciones", e);
			throw new ExcepcionPS9(e);
		} finally {
			if ( conexion != null ) try {
				LOG.debug("Cerrando conexion");
				conexion.cerrar();
			} catch ( ExcepcionComunicaciones e ) {
				LOG.error("No se pudo cerrar la conexion", e);
			}
		}
	}
}
