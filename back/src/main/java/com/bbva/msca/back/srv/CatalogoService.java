package com.bbva.msca.back.srv;

import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/CatalogoService")
public interface CatalogoService {
	
	@GET //OK DOCUMENTACION
	@Path("/consultarDetCatalogoPorCdCatalogo/{cdCatalogo}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDetCatalogoPorCdCatalogo(@PathParam("cdCatalogo") BigDecimal cdCatalogo);
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoSegmento")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoSegmento();

	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoTipoPersona")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoTipoPersona();
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoTipologia")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoTipologias();
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoTipologiaGerencia/{cdGerencia}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoTipologiasGerencia(@PathParam("cdGerencia") Integer cdGerencia);
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoSesion")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoSesion();
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoEscenarios")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoEscenarios();
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoSector")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoSector();
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoCriterio")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoCriterio();
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoPerfil")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoPerfil();
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoRespuesta")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoRespuesta();
	
	@GET//OK DOCUMENTACION
	@Path("/consultarCatalogoGerencia")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoGerencia();

	@GET
	@Path("/consultarCatalogoTipoCuenta")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoTipoCuenta();
	
	@GET
	@Path("/consultarCatalogoTipoOperacionMonetaria")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoTipoOperacionMonetaria();
	
	@GET
	@Path("/consultarCatalogoInstrumentoMonetario")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoInstrumentoMonetario();
	
	@GET
	@Path("/consultarCatalogoMoneda")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoMoneda();
	
	@GET
	@Path("/consultarCatalogoOperacionInusual/{cOperacion}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoOperacionInusual(@PathParam("cOperacion")String cOperacion);

	@GET
	@Path("/consultarCatalogoUsuariosPerfil/{cdPerfil}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarUsuariosPerfil(@PathParam("cdPerfil") Integer cdPerfil);

	@GET
	@Path("/consultarCatalogoUsuariosPerfilSupervisor/{cdSupervisor}/{cdPerfil}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarUsuariosPerfilSupervisor(@PathParam("cdSupervisor") Integer cdSupervisor, @PathParam("cdPerfil") Integer cdPerfil);
	
	@GET
	@Path("/consultarCatalogoDictamenPreliminar/")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoDictamenPreliminar();
	
	@GET
	@Path("/consultarCatalogoDictamenFinal/")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoDictamenFinal();
		
	@GET
	@Path("/consultarCatalogoTipos")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarTipoPrioridad();
	
	@GET
	@Path("/consultarCatalogoTipologiaCAR")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCatalogoTipologiaCAR();
}
