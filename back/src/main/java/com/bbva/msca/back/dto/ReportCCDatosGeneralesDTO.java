package com.bbva.msca.back.dto;

public class ReportCCDatosGeneralesDTO {

	private String nbRegSimplificadoDC;
	private String nbActEconomicaDC;
	private String nbOcupacionDC;

	public String getNbRegSimplificadoDC() {
		return nbRegSimplificadoDC;
	}

	public void setNbRegSimplificadoDC(String nbRegSimplificadoDC) {
		this.nbRegSimplificadoDC = nbRegSimplificadoDC;
	}

	public String getNbActEconomicaDC() {
		return nbActEconomicaDC;
	}

	public void setNbActEconomicaDC(String nbActEconomicaDC) {
		this.nbActEconomicaDC = nbActEconomicaDC;
	}

	public String getNbOcupacionDC() {
		return nbOcupacionDC;
	}

	public void setNbOcupacionDC(String nbOcupacionDC) {
		this.nbOcupacionDC = nbOcupacionDC;
	}

}
