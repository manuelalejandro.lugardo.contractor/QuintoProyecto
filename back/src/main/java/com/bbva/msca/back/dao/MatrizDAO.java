package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;

import com.bbva.msca.back.dominio.Tsca007Tipologia;
import com.bbva.msca.back.dominio.Tsca033MatCrit;
import com.bbva.msca.back.dominio.Tsca037Sector;
import com.bbva.msca.back.dominio.Tsca049Criterio;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dto.MatrizDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

public class MatrizDAO {

	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();

	/**
	 * Bitacora de movimientos Sesion - Tipologia
	 * */
	private Tsca055Log log;

	@SuppressWarnings("unchecked")
	public List<MatrizDTO> getMatrices() throws Exception {
		List<MatrizDTO> lstMatrizDTO = new ArrayList<MatrizDTO>();
		Query query = preparaQuery(0);
		lstMatrizDTO = (List<MatrizDTO>) query.getResultList();
		return lstMatrizDTO;
	}

	@SuppressWarnings("unchecked")
	public List<MatrizDTO> getMatricesGerencia(Integer cdGerencia)
			throws Exception {
		List<MatrizDTO> lstMatrizDTO = new ArrayList<MatrizDTO>();
		Query query = preparaQuery(cdGerencia);
		lstMatrizDTO = (List<MatrizDTO>) query.getResultList();
		return lstMatrizDTO;
	}

	public int creaMatriz(MatrizDTO matriz) throws Exception {
		int op = Constantes.FIN_ER;
		Tsca033MatCrit nuevaMatriz = Tsca033MatCrit.findTsca033MatCrit(matriz.getCdMatCrit());
		if (nuevaMatriz != null) {
			if (nuevaMatriz.getStSistema().equals(Constantes.CD_ST_SIS_I)) {
				copiaABC(matriz, nuevaMatriz);
				nuevaMatriz.merge();
				nuevaMatriz.flush();
				this.log = this.generaLog(matriz, 1);
				this.log.persist();
				this.log.flush();
				op = Constantes.FIN_OK;
			} else {
				op = Constantes.FIN_AV;
			}
		} else {
			nuevaMatriz = new Tsca033MatCrit();
			copiaABC(matriz, nuevaMatriz);
			nuevaMatriz.persist();
			nuevaMatriz.flush();
			matriz.setCdMatCrit(nuevaMatriz.getCdMatCrit());
			this.log = this.generaLog(matriz, 1);
			this.log.persist();
			this.log.flush();
			op = Constantes.FIN_OK;
		}
		return op;
	}

	public MatrizDTO modificaMatriz(MatrizDTO matriz) throws Exception {
		Tsca033MatCrit nuevaMatriz = new Tsca033MatCrit();
		copiaABC(matriz, nuevaMatriz);
		nuevaMatriz.merge();
		nuevaMatriz.flush();
		this.log = this.generaLog(matriz, 2);
		this.log.persist();
		this.log.flush();
		return matriz;
	}

	private void copiaABC(MatrizDTO matriz, Tsca033MatCrit nuevaMatriz) {
		nuevaMatriz.setCdMatCrit(matriz.getCdMatCrit());
		nuevaMatriz.setCdCriterio(Tsca049Criterio.findTsca049Criterio(matriz
				.getCdCriterio()));
		nuevaMatriz.setCdSector(Tsca037Sector.findTsca037Sector(matriz
				.getCdSector()));
		nuevaMatriz.setCdTipologia(Tsca007Tipologia.findTsca007Tipologia(matriz
				.getCdTipologia()));
		nuevaMatriz.setNbRazon(matriz.getNbDescripcion());
		nuevaMatriz.setNbMatCrit(matriz.getNbMatCrit());
		nuevaMatriz.setNbValor(matriz.getNbValor());
		nuevaMatriz.setNuPuntaje(matriz.getNuPuntos());
		nuevaMatriz.setNuRangoMax(matriz.getNuRangoMax());
		nuevaMatriz.setNuRangoMin(matriz.getNuRangoMin());
		nuevaMatriz.setStSistema(matriz.getCdStSistema());
	}

	private Query preparaQuery(Integer cdGerencia) throws Exception {
		Query query = null;
		if (cdGerencia > 0) {
			query = em.createNativeQuery(Consultas
					.getConsulta("getMatrizCriteriosGerenciaParametria"));
			query.setParameter(1, cdGerencia);
			query.setParameter(2, Constantes.CD_ST_SIS_A);
			query.setParameter(3, Constantes.CD_ST_SIS_A);
		} else {
			query = em.createNativeQuery(Consultas
					.getConsulta("getMatrizCriteriosParametria"));
			query.setParameter(1, Constantes.CD_ST_SIS_A);
			query.setParameter(2, Constantes.CD_ST_SIS_A);
		}
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdMatCrit")
				.addScalar("nbMatCrit", StringType.INSTANCE)
				.addScalar("cdTipologia", StringType.INSTANCE)
				.addScalar("cdCriterio")
				.addScalar("cdSector", StringType.INSTANCE)
				.addScalar("nuRangoMin")
				.addScalar("nuRangoMax")
				.addScalar("nbValor", StringType.INSTANCE)
				.addScalar("nbDescripcion", StringType.INSTANCE)
				.addScalar("nuPuntos")
				.addScalar("nbDescTip", StringType.INSTANCE)
				.addScalar("nbCriterio", StringType.INSTANCE)
				.addScalar("nbSector", StringType.INSTANCE)
				.addScalar("cdStSistema", StringType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(MatrizDTO.class));
		return query;
	}

	private Tsca055Log generaLog(MatrizDTO matriz, int accion) {
		Tsca055Log logSesion = new Tsca055Log();
		logSesion.setNbEvento("MSCAVE01012_5");
		logSesion.setCdUsuario(matriz.getCdUsuario());
		logSesion.setFhFecha(new GregorianCalendar().getTime());
		switch (accion) {
		case 1:
			logSesion.setNbAccionRealizad("Alta: " + matriz.getCdMatCrit());
			break;
		case 2:
			logSesion
					.setNbAccionRealizad("Modifica: " + matriz.getCdMatCrit());
			break;
		}
		return logSesion;
	}

}
