package com.bbva.msca.back.srv;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import com.bbva.msca.back.util.ResponseGeneric;

@Path("/ModuloCheckListService")
public interface ModuloCheckListService {
	
	@GET
	@Path("/consultarModuloCheckList")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarModuloCheckList();
	
	@GET
	@Path("/busquedaModuloCheckList")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric busquedaModuloCheckList();
	
}
