package com.bbva.msca.back.dto;

import java.util.Date;

public class OtrosClientesCasoDTO {
	
	 
	private String cdCliente;
	private String nbCliente;
	private String nbDomicilio;
	private String cdRFC;
	private String nbSector;
	
	
	
	
	public String getCdRFC() {
		return cdRFC;
	}
	public void setCdRFC(String cdRFC) {
		this.cdRFC = cdRFC;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNbDomicilio() {
		return nbDomicilio;
	}
	public void setNbDomicilio(String nbDomicilio) {
		this.nbDomicilio = nbDomicilio;
	}
	public String getNbSector() {
		return nbSector;
	}
	public void setNbSector(String nbSector) {
		this.nbSector = nbSector;
	}
	  
	

}
