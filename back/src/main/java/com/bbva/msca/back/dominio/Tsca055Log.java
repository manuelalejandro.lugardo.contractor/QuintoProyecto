package com.bbva.msca.back.dominio;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(versionField = "", table = "TSCA055_LOG", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca055Log {
	
	@Id
	@NotNull
	@SequenceGenerator(name = "SQ_TSCA055", sequenceName = "SQ_TSCA055", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TSCA055")
    @Column(name = "CD_LOG")
	private Integer idLog;
	
	@Column(name = "CD_USUARIO")
	private String cdUsuario;
	
	@Column(name = "CD_CLIENTE")
	private String cdCliente;
	
	@Column(name = "CD_CASO")
	private BigDecimal cdCaso;
	
	@DateTimeFormat(iso = ISO.TIME, pattern = "HH:mm") 
	@Temporal(TemporalType.TIME) 
	@Column(name = "FH_FECHA") 
	private Date fhFecha;
}
