package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.stereotype.Component;

import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import com.bbva.msca.back.dto.MontosTotalDTO;
import com.bbva.msca.back.dto.MovimientosDTO;

@Component
public class MovimientosDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	@SuppressWarnings("unchecked")
	public List<MovimientosDTO> getMovimientos(String nuFolioAlerta, String cdSistema, Integer limit, Integer offset,String search, String name, String order) throws Exception{
		
		 List<MovimientosDTO> lstMovimientosDTO = null;
		 String consulta = " SELECT * FROM ( "+Consultas.getConsulta("getConsultaMovimientos")+" ) ";

		 consulta = consulta +" "+ " order by "
					             + (name != null && !"".equals(name) ? " " + name + " ": " nuNumero ")
					             + " "
					             + (order != null && !"".equals(order) ? " " + order + " "
					             : " asc ");
		 Query query = em.createNativeQuery(consulta);
		 query.setParameter(1,nuFolioAlerta);
		 query.setParameter(2,cdSistema);	
		 query.setParameter(3,nuFolioAlerta);
		 query.setParameter(4,cdSistema);
		 query.setParameter(5,nuFolioAlerta);
		 query.setParameter(6,cdSistema);	
		 query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("nuNumero")
		.addScalar("nuCuenta")
		.addScalar("nbOficina")
        .addScalar("nbConcepto")
        .addScalar("nbTipoMovimiento")
        .addScalar("fhMovimiento",TimestampType.INSTANCE)
        .addScalar("cdDivisa",StringType.INSTANCE)
        .addScalar("imMonto")
		.addScalar("imEfectivo")
        .addScalar("nbDescripcion")
		.setResultTransformer(Transformers.aliasToBean(MovimientosDTO.class));
		
		query.setFirstResult(offset);
		query.setMaxResults(limit);
		 
		 lstMovimientosDTO = (List<MovimientosDTO>)query.getResultList();
	 
		return lstMovimientosDTO;
	}
	
	public Integer getNuMovimientos(String nuFolioAlerta, String cdSistema) throws Exception{
		 
		 String consulta = " SELECT COUNT(*) FROM ( "+Consultas.getConsulta("getConsultaMovimientos")+" ) ";
		 List<Number> resultado = new ArrayList<Number>();
		 Integer conteo = 0;
		 Query query = em.createNativeQuery(consulta);
		 query.setParameter(1,nuFolioAlerta);
		 query.setParameter(2,cdSistema);	
		 query.setParameter(3,nuFolioAlerta);
		 query.setParameter(4,cdSistema);
		 query.setParameter(5,nuFolioAlerta);
		 query.setParameter(6,cdSistema);	
		 resultado = (List<Number>)query.getResultList();
		 if( resultado !=null && !resultado.isEmpty()){
			 conteo = resultado.get( 0 ).intValue();	 
		 }	 
		return conteo;
	}
	
	@SuppressWarnings("unchecked")
	public MontosTotalDTO getTotalAbonos(String nuFolioAlerta, String cdSistema) throws Exception{
		MontosTotalDTO montosTotalDTO = null;  
		
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTotalAbonos"));
		 query.setParameter(1,nuFolioAlerta);
		 query.setParameter(2,cdSistema);	
		 query.setParameter(3,nuFolioAlerta);
		 query.setParameter(4,cdSistema);
		 query.setParameter(5,nuFolioAlerta);
		 query.setParameter(6,cdSistema);
		 
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("numAbonosTotal")
			.addScalar("imAbonosTotal")
			.setResultTransformer(Transformers.aliasToBean(MontosTotalDTO.class));
		 
		 montosTotalDTO=((List<MontosTotalDTO>)query.getResultList()).get(0);
		 
		return montosTotalDTO;
	}
	
	@SuppressWarnings("unchecked")
	public MontosTotalDTO getTotalCargos(String nuFolioAlerta, String cdSistema) throws Exception{
		 MontosTotalDTO montosTotalDTO = null; 
		
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTotalCargos"));
		 query.setParameter(1,nuFolioAlerta);
		 query.setParameter(2,cdSistema);	
		 query.setParameter(3,nuFolioAlerta);
		 query.setParameter(4,cdSistema);
		 query.setParameter(5,nuFolioAlerta);
		 query.setParameter(6,cdSistema);	
		 
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("numCargosTotal")
			.addScalar("imCargosTotal")
			.setResultTransformer(Transformers.aliasToBean(MontosTotalDTO.class));
		 
		 montosTotalDTO=((List<MontosTotalDTO>)query.getResultList()).get(0);
	 
		return montosTotalDTO;
	}
	
	
}
