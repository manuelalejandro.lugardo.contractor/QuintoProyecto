package com.bbva.msca.back.srv;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bbva.msca.back.dao.CedulaCasoDAO;
import com.bbva.msca.back.dao.ConsultaCasosDAO;
import com.bbva.msca.back.dao.CuentasClienteReportadoDAO;
import com.bbva.msca.back.dao.DatosGeneralesClienteReportadoDAO;
import com.bbva.msca.back.dao.DetalleTransferenciasInternacionalesDAO;
import com.bbva.msca.back.dao.LogDAO;
import com.bbva.msca.back.dao.NotasInvestigacionDAO;
import com.bbva.msca.back.dao.OpinionFuncionarioDAO;
import com.bbva.msca.back.dao.OtrosClientesCasoDAO;
import com.bbva.msca.back.dao.OtrosProductosClienteReportadoDAO;
import com.bbva.msca.back.dao.TransferenciasInternacionalesDAO;
import com.bbva.msca.back.dominio.Tsca014Caso;
import com.bbva.msca.back.dominio.Tsca054Archivo;
import com.bbva.msca.back.dto.CasoAnalisisDescripDTO;
import com.bbva.msca.back.dto.CedulaCasoDTO;
import com.bbva.msca.back.dto.ConsultaCasosDTO;
import com.bbva.msca.back.dto.CuentasClienteReportadoDTO;
import com.bbva.msca.back.dto.DatosGeneralesClienteReportadoDTO;
import com.bbva.msca.back.dto.DetalleTransferenciasInternacionalesDTO;
import com.bbva.msca.back.dto.LogDTO;
import com.bbva.msca.back.dto.NotasInvestigacionDTO;
import com.bbva.msca.back.dto.OpinionFuncionarioDTO;
import com.bbva.msca.back.dto.OtrosClientesCasoDTO;
import com.bbva.msca.back.dto.OtrosProductosClienteReportadoDTO;
import com.bbva.msca.back.dto.TranferenciasInternacionalesDTO;
import com.bbva.msca.back.dto.TransferenciasInternacionalesTablaDTO;


import com.bbva.msca.back.dto.RelacionClienteFiguraDTO;
import com.bbva.msca.back.dao.RelacionClienteFiguraDAO;

import com.bbva.msca.back.dao.CuentasClientesFidDAO;
import com.bbva.msca.back.dto.CuentasClienteFidDTO;

import com.bbva.msca.back.dao.FideicomisoDAO;
import com.bbva.msca.back.dto.FideicomisoDTO;

import com.bbva.msca.back.dao.OperacionesMedianasRelevantesDAO;
import com.bbva.msca.back.dto.OperacionesMedianasRelevantesDTO;

import com.bbva.msca.back.dao.PersonasRelacionadasDAO;
import com.bbva.msca.back.dto.PersonasRelacionadasDTO;


//
import com.bbva.msca.back.dao.IdentificacionReporteDAO;
import com.bbva.msca.back.dto.IdentificacionReporteDTO; 

import com.bbva.msca.back.dao.DatosGeneralesClienteDAO;
import com.bbva.msca.back.dto.DatosGeneralesClienteDTO;

import com.bbva.msca.back.dao.ConocimientoClienteDAO;
import com.bbva.msca.back.dto.ConocimientoClienteDTO;
 
   import com.bbva.msca.back.dao.TransaccionClienteDAO;
   import com.bbva.msca.back.dto.TransaccionClienteDTO;
     
   import com.bbva.msca.back.dao.InfoAccionistasyEmpresaDAO;
   import com.bbva.msca.back.dto.InfoAccionistasyEmpresaDTO;

import com.bbva.msca.back.dao.DescripcionTransaccionDAO;
import com.bbva.msca.back.dto.DescripcionTransaccionDTO;

import com.bbva.msca.back.dao.MecanicaOperacionalOrigenDestinoDAO;
import com.bbva.msca.back.dto.MecanicaOperacionalOrigenDestinoDTO;
 
  import com.bbva.msca.back.dao.MecanicaOperacionalConceptoDAO;
  import com.bbva.msca.back.dto.MecanicaOperacionalConceptoDTO;

 
import com.bbva.msca.back.dao.OficiosListasNotasPeriodisticasDAO;
import com.bbva.msca.back.dto.OficiosListasNotasPeriodisticasDTO;

import com.bbva.msca.back.dao.DireccionMapsDAO;
import com.bbva.msca.back.dto.DireccionMapsDTO;

import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;
import com.bbva.msca.back.util.ResponseGenericPagination;

public class CasosServiceImpl implements CasosService {
	//001
	//DETALLE DEL CASO
	@Override
	public ResponseGeneric consultarCedulas(BigDecimal cd_caso, String nuCliente, String nuFolioAlertaDC) {
		ResponseGeneric response = new ResponseGeneric();
		CedulaCasoDAO cedulaCasoDAO = new CedulaCasoDAO();
		CedulaCasoDTO cedulaCasoDTO = null;
		try{
			cedulaCasoDTO = cedulaCasoDAO.getCedulaCaso(cd_caso, nuCliente, nuFolioAlertaDC);
			response.setResponse(cedulaCasoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//002
	//DETALLE DEL CASO CD_SISTEMA-CHAR
	@Override
	public ResponseGeneric consultarIdentificacionReportes(String nuFolioAlerta,String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();
		IdentificacionReporteDTO datIdenRepoDTO = null;
		IdentificacionReporteDAO identificacionReporteDAO = new IdentificacionReporteDAO();
		try{
			datIdenRepoDTO = identificacionReporteDAO.getIdentificacionReporte(nuFolioAlerta, cdSistema);
			response.setResponse(datIdenRepoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//003
	//DETALLE DEL CASO cliente reportado
	@Override
	public ResponseGeneric consultarDatosGeneralesClienteReportado(String cdSistema, String folioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		DatosGeneralesClienteReportadoDTO datosGeneralesDTO = null;
		DatosGeneralesClienteReportadoDAO datosGeneralesDAO = new DatosGeneralesClienteReportadoDAO();
		try{
			datosGeneralesDTO = datosGeneralesDAO.getDatosGeneralesClienteReportado(cdSistema, folioAlerta);
			response.setResponse(datosGeneralesDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
	return response;
	}
	//002
	//analisis DEL CASO 
	@Override
	public ResponseGeneric consultarDatosGeneralesClienteReportado2(String cdSistema, String folioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		DatosGeneralesClienteReportadoDTO datosGeneralesDTO = null;
		DatosGeneralesClienteReportadoDAO datosGeneralesDAO = new DatosGeneralesClienteReportadoDAO();
		try{
			datosGeneralesDTO = datosGeneralesDAO.getDatosGeneralesClienteReportado2(cdSistema, folioAlerta);
			response.setResponse(datosGeneralesDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
	return response;
	}
	
	//004
	//DETALLE DEL CASO datos generales
	public ResponseGeneric consultarDatosGeneralesCliente(String cdSistema, String nuFolioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		DatosGeneralesClienteDTO datosGeneralesDTO = null;
		DatosGeneralesClienteDAO datosGeneralesDAO = new DatosGeneralesClienteDAO();
		try{
			datosGeneralesDTO = datosGeneralesDAO.getDatosGeneralesCliente(cdSistema, nuFolioAlerta);
			response.setResponse(datosGeneralesDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
	return response;
	}
	//005

	
	@Override
	public ResponseGeneric consultarConocimientoCliente(String cdSistema, String nuFolioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		ConocimientoClienteDTO conocimientoClienteDTO = null;
		ConocimientoClienteDAO conocimientoClienteDAO = new ConocimientoClienteDAO();
		try{
			conocimientoClienteDTO = conocimientoClienteDAO.getConocimientoCliente(cdSistema, nuFolioAlerta);
			response.setResponse(conocimientoClienteDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
		response.setCdMensaje("0");
		response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
	}
		return response;
	}


	//DETALLE DEL CASO
	@Override
	public ResponseGeneric consultarTransaccionesCliente(String cdSistema,String nuFolioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		TransaccionClienteDTO transaccionClienteDTO = null;
		TransaccionClienteDAO transaccionClienteDAO = new TransaccionClienteDAO();
		try{
			transaccionClienteDTO = transaccionClienteDAO.getTransaccionesCliente(cdSistema, nuFolioAlerta);
			response.setResponse(transaccionClienteDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
	return response;
	}
	
	//006
	//DETALLE DEL CASO
	@Override
	public ResponseGeneric consultarRelacionClienteFigura(String cdSistema, String nuFolioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		RelacionClienteFiguraDTO relacionClienteFiguraDTO = null;
		RelacionClienteFiguraDAO relacionClienteFiguraDAO = new RelacionClienteFiguraDAO();
		try{
			relacionClienteFiguraDTO = relacionClienteFiguraDAO.getRelacionClienteFigura(cdSistema, nuFolioAlerta);
			response.setResponse(relacionClienteFiguraDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
	return response;
	}

	//008
	//DETALLE DEL CASO
	@Override
	public ResponseGeneric consultarInfoAccionistasyEmpresa(String cdSistema, String folioAlerta) {
		ResponseGeneric response = new ResponseGeneric();	
		InfoAccionistasyEmpresaDAO  infoAccionistasyEmpresaDAO = new InfoAccionistasyEmpresaDAO();
		try{
			List<InfoAccionistasyEmpresaDTO> lstInfoAccionistasyEmpresaDTO = infoAccionistasyEmpresaDAO.getInfoAccionistasyEmpresa(cdSistema, folioAlerta);
			response.setResponse(lstInfoAccionistasyEmpresaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
	return response;
	}
	
	
	
	//OPINION FUNCIONARIO ENCABEZADO
	//
	@Override
	public ResponseGeneric consultarOpinionFuncionarioEnc(String cdSistema, String folioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		OpinionFuncionarioDAO opinionFuncionarioDAO = new OpinionFuncionarioDAO();
		try{
			List<OpinionFuncionarioDTO> opinionFuncionarioDTO = opinionFuncionarioDAO.getOpinionFuncionarioEncabezado(cdSistema, folioAlerta);
			response.setResponse(opinionFuncionarioDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	//OPINION FUNCIONARIO
	//
	@Override
	public ResponseGeneric consultarOpinionFuncionario(String cdSistema, String folioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		OpinionFuncionarioDAO opinionFuncionarioDAO = new OpinionFuncionarioDAO();
		try{
			List<OpinionFuncionarioDTO> lstOpinionFuncionarioDTO = opinionFuncionarioDAO.getOpinionFuncionario(opinionFuncionarioDAO.getOpinionFuncionarioCuestionario(cdSistema, folioAlerta),cdSistema, folioAlerta);
			response.setResponse(lstOpinionFuncionarioDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric agregarOpinionFuncionario(String cdSistema, String nuFolioAlerta, OpinionFuncionarioDTO opinionFuncionarioDTO ) {
		ResponseGeneric response = new ResponseGeneric();
		OpinionFuncionarioDAO opinionFuncionarioDAO = new OpinionFuncionarioDAO();
		
		try{
			response.setResponse(opinionFuncionarioDAO.addOpinionFuncionario(cdSistema, nuFolioAlerta, opinionFuncionarioDTO));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));	
		}
		return response;
	}
	
	
	
	
	

	@Override
	public ResponseGeneric consultarDescripcionTransaccion(String cdSistema, String folioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		DescripcionTransaccionDAO descripcionTransaccionDAO = new DescripcionTransaccionDAO();
		DescripcionTransaccionDTO descripcionTransaccionDTO = null;
		try {
			descripcionTransaccionDTO = descripcionTransaccionDAO.getDescripcionTransaccion(cdSistema, folioAlerta);
			response.setResponse(descripcionTransaccionDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	/*	
	//TABLA CUATRIMESTRAL
	@Override
	public ResponseGeneric consultarMecanicaOperacionalConcepto(BigDecimal cdCaso,String cdAlerta,String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMecanicaOperacionalConcepto = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConceptoDTO = lstMecanicaOperacionalConcepto.consultarMecanicaOperacionalConcepto(cdCaso, cdAlerta, cdSistema);
			response.setResponse(lstMecanicaOperacionalConceptoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	
	//TABLA MES UNO 
	@Override
	public ResponseGeneric consultarConceptosMesUno(BigDecimal cdCaso,String cdAlerta,String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMesUno = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMesUnoDTO = lstMesUno.consultarConceptosMesUno(cdCaso, cdAlerta, cdSistema);
			response.setResponse(lstMesUnoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//TABLA MES DOS
	public ResponseGeneric consultarConceptosMesDos(BigDecimal cdCaso,String cdAlerta,String cdSistema){
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMesDos = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMesDosDTO = lstMesDos.consultarConceptosMesDos(cdCaso, cdAlerta, cdSistema);
			response.setResponse(lstMesDosDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//TABLA MES TRES
	public ResponseGeneric consultarConceptosMesTres(BigDecimal cdCaso,String cdAlerta,String cdSistema){
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMesTres = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMesTresDTO = lstMesTres.consultarConceptosMesTres(cdCaso, cdAlerta, cdSistema);
			response.setResponse(lstMesTresDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//TABLA MES CUATRO
	public ResponseGeneric consultarConceptosMesCuatro(BigDecimal cdCaso,String cdAlerta,String cdSistema){
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMesCuatro = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMesCuatroDTO = lstMesCuatro.consultarConceptosMesCuatro(cdCaso, cdAlerta, cdSistema);
			response.setResponse(lstMesCuatroDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	//TABLA ORIGEN
	@Override
	public ResponseGeneric consultarOrigenMOCR() {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalOrigenDestinoDAO  mecanicaOperacionalOrigen = new MecanicaOperacionalOrigenDestinoDAO();
		
		try {
			List<MecanicaOperacionalOrigenDestinoDTO> lstTablaOrigenMOCR = mecanicaOperacionalOrigen.getMecanicaOperacionalOrigen();
			response.setResponse(lstTablaOrigenMOCR);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//TABLA DESTINO
	@Override
	public ResponseGeneric consultarDestinoMOCR() {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalOrigenDestinoDAO  mecanicaOperacionalDestino = new MecanicaOperacionalOrigenDestinoDAO();

		try {
			List<MecanicaOperacionalOrigenDestinoDTO> lstTablaDestinoMOCR = mecanicaOperacionalDestino.getMecanicaOperacionalDestino();
			response.setResponse(lstTablaDestinoMOCR);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}	
		return response;
	}*/
	
	

	@Override
	public ResponseGeneric consultarCuentasVinculadasOrigen() {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalOrigenDestinoDAO  mecanicaOperacionalOrigen = new MecanicaOperacionalOrigenDestinoDAO();
		
		try {
			List<MecanicaOperacionalOrigenDestinoDTO> lstTablaOrigenMOCR = mecanicaOperacionalOrigen.getMecanicaOperacionalCuentasVinculadasOrigen();
			response.setResponse(lstTablaOrigenMOCR);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCuentasVinculadasDestino() {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalOrigenDestinoDAO  mecanicaOperacionalDestino = new MecanicaOperacionalOrigenDestinoDAO();

		try {
			List<MecanicaOperacionalOrigenDestinoDTO> lstTablaDestinoMOCR = mecanicaOperacionalDestino.getMecanicaOperacionalCuentasVinculadasDestino();
			response.setResponse(lstTablaDestinoMOCR);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}	
		return response;
	}
	
	
	
	
	
	
//OPERACIONES MEDIANAS Y RELEVANTES
	@Override
	public ResponseGeneric consultarOperacionesMedianasRelevantes(BigDecimal cdCaso, String cdSistema, String folioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		OperacionesMedianasRelevantesDTO operacionesMedianasRelevantesDTO = null;
		OperacionesMedianasRelevantesDAO operacionesMedianasRelevantesDAO = new OperacionesMedianasRelevantesDAO();
		
		try {
			operacionesMedianasRelevantesDTO = operacionesMedianasRelevantesDAO.getOperacionesMedianasRelevantes(cdCaso, cdSistema, folioAlerta);
			response.setResponse(operacionesMedianasRelevantesDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}	
		return response;
	}
	
	
	@Override
	public ResponseGeneric consultarAlertasCaso(BigDecimal cdCAso) {
		ResponseGeneric response = new ResponseGeneric();
		ConsultaCasosDAO consultaCasosDAO = new ConsultaCasosDAO();
		try{
			ConsultaCasosDTO lstConsultaCasosDTO = consultaCasosDAO.getNumAlertasCaso(cdCAso);
			response.setResponse(lstConsultaCasosDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	

	@Override
	public ResponseGeneric consultarOficiosListasNotasPeriodisticas() {
		ResponseGeneric response = new ResponseGeneric();
		OficiosListasNotasPeriodisticasDTO oficiosListasNotasPeriodisticasDTO = OficiosListasNotasPeriodisticasDAO.getOficiosListasNotasPeriodisticas();
		response.setResponse(oficiosListasNotasPeriodisticasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}

	@Override
	public ResponseGeneric consultarDireccionesMaps() {
		ResponseGeneric response = new ResponseGeneric();
		DireccionMapsDTO direccionMapsDTO = DireccionMapsDAO.getdireccionMaps();
		response.setResponse(direccionMapsDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	
	/*@Override
	public ResponseGeneric consultarDetalleOperacionesMedRel() {
		ResponseGeneric response = new ResponseGeneric();
		OperacionesMedianasRelevantesDAO detalleOperacionesMedRelDAO = new OperacionesMedianasRelevantesDAO();
		List<DetalleOperacionesMedRelDTO> lstDetalleOperacionesMedRelDAO = detalleOperacionesMedRelDAO.getDetalleOperacionesMedRel();
		
		response.setResponse(lstDetalleOperacionesMedRelDAO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}*/
	
	@Override
	public ResponseGeneric consultarDetalleOperacionesMedianas(BigDecimal cdCaso, String cdSistema, String nuFolioAlerta, Integer limit, Integer offset,String search,String name, String order) {
		ResponseGeneric response = new ResponseGeneric();
		ResponseGenericPagination pagination = new ResponseGenericPagination();
		OperacionesMedianasRelevantesDAO operacionesMedianasRelevantesDAO = new OperacionesMedianasRelevantesDAO();
		try {
			pagination.setRows(operacionesMedianasRelevantesDAO.getConsultaDetalleOperacionesMedianas(cdCaso, cdSistema, nuFolioAlerta, limit, offset, search, name, order));
			pagination.setTotal(operacionesMedianasRelevantesDAO.getNuDetalleOperacionesMedianas(cdCaso, cdSistema, nuFolioAlerta));
			response.setResponse(pagination);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric consultarDetalleOperacionesRelevantes(BigDecimal cdCaso, String cdSistema, String nuFolioAlerta, Integer limit, Integer offset,String search,String name, String order) {
		ResponseGeneric response = new ResponseGeneric();
		ResponseGenericPagination pagination = new ResponseGenericPagination();
		OperacionesMedianasRelevantesDAO operacionesMedianasRelevantesDAO = new OperacionesMedianasRelevantesDAO();
		try {
			pagination.setRows(operacionesMedianasRelevantesDAO.getConsultaDetalleOperacionesRelevantes(cdCaso, cdSistema, nuFolioAlerta, limit, offset, search, name, order));
			pagination.setTotal(operacionesMedianasRelevantesDAO.getNuDetalleOperacionesRelevantes(cdCaso, cdSistema, nuFolioAlerta));
			response.setResponse(pagination);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric consultarPersonasRelacionadas() {
		ResponseGeneric response = new ResponseGeneric();
		PersonasRelacionadasDAO personasRelacionadasDAO = new PersonasRelacionadasDAO();
		List<PersonasRelacionadasDTO> lstPersonasRelacionadasDTO = personasRelacionadasDAO.getPersonasRelacionadas();
		
		response.setResponse(lstPersonasRelacionadasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCuentaClientes() {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClientesFidDAO cuentasClientesFidDAO = new CuentasClientesFidDAO();
		List<CuentasClienteFidDTO> lstCuentasClienteFidDTO = cuentasClientesFidDAO.getCuentasClienteFid();
		
		response.setResponse(lstCuentasClienteFidDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarFideicomiso() {
		ResponseGeneric response = new ResponseGeneric();
		FideicomisoDAO fideicomisoDAO = new FideicomisoDAO();
		List<FideicomisoDTO> lstFideicomisoDTO = fideicomisoDAO.getFideicomiso();
		
		response.setResponse(lstFideicomisoDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarDetalleFideicomiso() {
		ResponseGeneric response = new ResponseGeneric();
		FideicomisoDTO detalleFideicomisoDTO = FideicomisoDAO.getDetalleFideicomiso();
		
		response.setResponse(detalleFideicomisoDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarNotasInvestigacion(BigDecimal cdCaso) {
		ResponseGeneric response = new ResponseGeneric();
		NotasInvestigacionDAO notasInvestigacionDAO = new NotasInvestigacionDAO();
		List<NotasInvestigacionDTO> lstNotasInvestigacionDTO;
		try{
			lstNotasInvestigacionDTO = notasInvestigacionDAO.getNotasInvestigacion(cdCaso); 
		response.setResponse(lstNotasInvestigacionDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
		response.setCdMensaje("0");
		response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
	}
		return response;
	}
	
	
	@Override
	public ResponseGeneric consultarTranferenciaInternacionalRecEnv(String cdSistema, String nuFolioAlerta) {
		ResponseGeneric response = new ResponseGeneric();
		TransferenciasInternacionalesDAO transferenciasInternacionalesDAO = new TransferenciasInternacionalesDAO();
		TranferenciasInternacionalesDTO transferenciasInternacionalesDTO = null;
		
		try {
			transferenciasInternacionalesDTO = transferenciasInternacionalesDAO.getTransferenciasInternacionales(cdSistema, nuFolioAlerta);
			response.setResponse(transferenciasInternacionalesDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}	
		return response;
	}
	
	
	
	
	
	
	@Override
	public ResponseGeneric consultarDetalleTranferenciaInternacionalRecEnv() {
		ResponseGeneric response = new ResponseGeneric();
		DetalleTransferenciasInternacionalesDAO detalleTransferenciasInternacionalesDAO = new DetalleTransferenciasInternacionalesDAO();
		List<DetalleTransferenciasInternacionalesDTO> lstDetalleTransferenciasInternacionalesDTO = detalleTransferenciasInternacionalesDAO.getDetalleTransferenciasInternacionales();
		
		response.setResponse(lstDetalleTransferenciasInternacionalesDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarLog() {
		ResponseGeneric response = new ResponseGeneric();
		LogDAO logDAO = new LogDAO();
		List<LogDTO> lstLogDTO = logDAO.getLog();
		
		response.setResponse(lstLogDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}


	@Override
	public ResponseGeneric crearDescripCaso(Tsca014Caso descripcion, BigDecimal cdCaso)throws Exception { 
		ResponseGeneric response = new ResponseGeneric();
		Tsca014Caso tsca014Caso =  Tsca014Caso.findTsca014Caso(cdCaso);
		tsca014Caso.setTxDesOpeReal(descripcion.getTxDesOpeReal());
		tsca014Caso.setTxDesRazInv(descripcion.getTxDesRazInv()); 
		tsca014Caso.merge();
		tsca014Caso.flush();  
		response.setResponse(cdCaso);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		return response;
	}
	 
	@Override
	public ResponseGeneric consultarLogAnalisis(BigDecimal cdCaso, String nuCliente) {
		ResponseGeneric response = new ResponseGeneric();
		LogDAO logDAO = new LogDAO();
		List<LogDTO> lstLogDTO; 
		try{
		 lstLogDTO = logDAO.getLogAnalisis(cdCaso, nuCliente); 
		response.setResponse(lstLogDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
		response.setCdMensaje("0");
		response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
	}
		return response;
	}

	
	@Override
	 public ResponseGeneric addNota(Tsca054Archivo nota, String nbCveRed) throws Exception {
		ResponseGeneric response = new ResponseGeneric(); 
	 	Tsca054Archivo tsca054FolioArchivo = Tsca054Archivo.findTsca054Archivo(nota.getCdFolioArchivo());
		tsca054FolioArchivo.setTxInvestigacion(nota.getTxInvestigacion()); 
		tsca054FolioArchivo.setCdUsuarioMdf(nbCveRed);
		tsca054FolioArchivo.setFhModif(new Date()); 
		tsca054FolioArchivo.merge();
		tsca054FolioArchivo.flush(); 
		response.setResponse(tsca054FolioArchivo);
		response.setCdMensaje("1");
 		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));	
		return response;
	}
	
	public ResponseGeneric addArchivo(Tsca054Archivo archivo, String nbCveRed)
	throws Exception {
	ResponseGeneric response = new ResponseGeneric();
    	archivo.setCdUsuario(nbCveRed);
 		archivo.setCdUsuarioMdf(nbCveRed); 
 		archivo.setFhCreacion(new Date());
 		archivo.setFhModif(new Date());
 		archivo.persist();
 		archivo.flush();
 		response.setResponse(archivo);
 		response.setCdMensaje("1");
 		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));			 
 			return response;
	}
	
	@Override
	public ResponseGeneric consultarKYC(BigDecimal cdCaso,String cdCliente) { 
		ResponseGeneric response = new ResponseGeneric();
		ConocimientoClienteDAO conocimientoClienteDAO = new ConocimientoClienteDAO(); 
		  ConocimientoClienteDTO lstKycDTO;
		try{
		 lstKycDTO = conocimientoClienteDAO.getKYC(cdCaso, cdCliente);
		response.setResponse(lstKycDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
		response.setCdMensaje("0");
		response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
	}
		return response;
		}
	

	@Override
	public ResponseGeneric consultarDescrip(BigDecimal cdCaso) { 
		ResponseGeneric response = new ResponseGeneric();
		ConsultaCasosDAO consultaDescrip = new ConsultaCasosDAO(); 
		 CasoAnalisisDescripDTO  lstDesDTO;
		try{
			lstDesDTO = consultaDescrip.getDesc(cdCaso);
		response.setResponse(lstDesDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
		response.setCdMensaje("0");
		response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
	    }
		return response;
	}
		
	@Override
	public ResponseGeneric consultaAccionistasDetCaso(BigDecimal cdCaso) { 
		ResponseGeneric response = new ResponseGeneric();
		InfoAccionistasyEmpresaDAO accionista = new InfoAccionistasyEmpresaDAO();
		List<InfoAccionistasyEmpresaDTO> lstAccionistaDTO; 
		try{
			lstAccionistaDTO = accionista.getAccionista(cdCaso); 
		response.setResponse(lstAccionistaDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
		response.setCdMensaje("0");
		response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
	}
		return response;
	}
	
	
	@Override
	public ResponseGeneric consultaOtrosClientesCaso(BigDecimal cdCaso) { 
		ResponseGeneric response = new ResponseGeneric();
		OtrosClientesCasoDAO otrosClientes = new OtrosClientesCasoDAO();
		List<OtrosClientesCasoDTO> lstOtrosClientes; 
		try{
			lstOtrosClientes = otrosClientes.getOtrosClientes(cdCaso); 
		response.setResponse(lstOtrosClientes);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
		response.setCdMensaje("0");
		response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
	}
		return response;
	}	


	//TABLA CUATRIMESTRAL
	@Override
	public ResponseGeneric consultarMecanicaOperacionalConceptoAnalisis(BigDecimal cdCaso,String cdAlerta,String cdSesion) {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMecanicaOperacionalConcepto = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConceptoDTO = lstMecanicaOperacionalConcepto.consultarMecanicaOperacionalConceptoAnalisis(cdCaso, cdAlerta, cdSesion);
			response.setResponse(lstMecanicaOperacionalConceptoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	
	//TABLA MES UNO 
	@Override
	public ResponseGeneric consultarConceptosMesUnoAnalisis(BigDecimal cdCaso,String cdAlerta,String cdSesion) {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMesUno = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMesUnoDTO = lstMesUno.consultarConceptosMesUnoAnalisis(cdCaso, cdAlerta, cdSesion);
			response.setResponse(lstMesUnoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//TABLA MES DOS
	public ResponseGeneric consultarConceptosMesDosAnalisis(BigDecimal cdCaso,String cdAlerta,String cdSesion) {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMesDos = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMesDosDTO = lstMesDos.consultarConceptosMesDosAnalisis(cdCaso, cdAlerta, cdSesion);
			response.setResponse(lstMesDosDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//TABLA MES TRES
	public ResponseGeneric consultarConceptosMesTresAnalisis(BigDecimal cdCaso,String cdAlerta,String cdSesion) {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMesTres = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMesTresDTO = lstMesTres.consultarConceptosMesTresAnalisis(cdCaso, cdAlerta, cdSesion);
			response.setResponse(lstMesTresDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//TABLA MES CUATRO
	public ResponseGeneric consultarConceptosMesCuatroAnalisis(BigDecimal cdCaso,String cdAlerta,String cdSesion) {
		ResponseGeneric response = new ResponseGeneric();	
		MecanicaOperacionalConceptoDAO  lstMesCuatro = new MecanicaOperacionalConceptoDAO();
		try {
			List<MecanicaOperacionalConceptoDTO> lstMesCuatroDTO = lstMesCuatro.consultarConceptosMesCuatroAnalisis(cdCaso, cdAlerta, cdSesion);
			response.setResponse(lstMesCuatroDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	 
	@Override
	public ResponseGeneric consultarTranferenciaInternacionalRecEnvDet(
			String cdSistema, String nuFolioAlerta, String numeroCuenta, String tipoOperacion, Integer anioActual ) {
		ResponseGeneric response = new ResponseGeneric();
		TransferenciasInternacionalesDAO transferenciasInternacionalesDAO = new TransferenciasInternacionalesDAO();
		//TranferenciasInternacionalesDTO transferenciasInternacionalesDTO = null;
		List<TransferenciasInternacionalesTablaDTO> lstResultado = null;
		try {
			lstResultado = transferenciasInternacionalesDAO.getTransferenciasInternacionalesDet(
					cdSistema, nuFolioAlerta, numeroCuenta, tipoOperacion, anioActual );
			response.setResponse( lstResultado );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}	
		return response;
	}
	
	@Override
	public ResponseGeneric consultarDetalleTranferenciaInternacionalRecEnvDet(String cdSistema, String folioAlerta, String numeroCuenta, String tipoOperacion, Integer anioActual) {
		ResponseGeneric response = new ResponseGeneric();
		TransferenciasInternacionalesDAO transferenciasInternacionalesDAO = new TransferenciasInternacionalesDAO();
		//TranferenciasInternacionalesDTO transferenciasInternacionalesDTO = null;
		List<TransferenciasInternacionalesTablaDTO> lstResultado = null; 
		try {
			lstResultado = transferenciasInternacionalesDAO.getDetalleTransferenciasInternacionalesDet(cdSistema, folioAlerta, numeroCuenta, tipoOperacion, anioActual);
			response.setResponse(lstResultado);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}	
		return response;
	}
	
	
	// FN003 - PESTA�A CUENTAS DEL CLIENTE
	// Tabla cuentas del Cliente
	@Override
	public ResponseGeneric consultarCuentasClienteReportado(String cdSistema, String folioAlerta, BigDecimal cdCaso) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteReportadoDAO lstCuentasClienteReportado = new CuentasClienteReportadoDAO();
		try {
			List<CuentasClienteReportadoDTO> lstCuentasClienteReportadoDTO = lstCuentasClienteReportado
					.consultarCuentasClienteReportado(cdSistema, folioAlerta, cdCaso);
			response.setResponse(lstCuentasClienteReportadoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	// Tabla otros productos
	@Override
	public ResponseGeneric consultarOtrosProductosCR(String cdSistema,
			String folioAlerta, BigDecimal cdCaso) {
		ResponseGeneric response = new ResponseGeneric();
		OtrosProductosClienteReportadoDAO otrosProductosClienteReportadoDAO = new OtrosProductosClienteReportadoDAO();
		OtrosProductosClienteReportadoDTO otrosProductosClienteReportadoDTO = null;
		try {
			otrosProductosClienteReportadoDTO = otrosProductosClienteReportadoDAO
					.consultarOtrosProductosCR(cdSistema, folioAlerta, cdCaso);
			response.setResponse(otrosProductosClienteReportadoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	// Tabla fideicomiso
	@Override
	public ResponseGeneric consultaFideicomisoCaso(BigDecimal cdCaso) {
		ResponseGeneric response = new ResponseGeneric();
		FideicomisoDAO fideicomisoDAO = new FideicomisoDAO();
		List<FideicomisoDTO> lstFideicomisoDTO;
		try {
			lstFideicomisoDTO = fideicomisoDAO.getfideicomiso(cdCaso);
			response.setResponse(lstFideicomisoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	// Personas Relacionadas al fideicomiso
	@Override
	public ResponseGeneric consultaDetalleFideicomisoCaso(BigDecimal cdCaso) {
		ResponseGeneric response = new ResponseGeneric();
		FideicomisoDAO fideicomisoDAO = new FideicomisoDAO();
		List<FideicomisoDTO> lstFideicomisoDTO;
		try {
			lstFideicomisoDTO = fideicomisoDAO.getDetalleFideicomiso(cdCaso);
			response.setResponse(lstFideicomisoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	
	//Cabecera cuatrimestral
	@Override
	public ResponseGeneric cabeceraCuatrimestral(String cdAlerta,String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();
		MecanicaOperacionalConceptoDAO mecanicaOperacionalConceptoDAO = new MecanicaOperacionalConceptoDAO();
		List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConcepto;
		try {
			lstMecanicaOperacionalConcepto = mecanicaOperacionalConceptoDAO.getCabeceraCuatrimestral(cdAlerta,cdSistema);
			response.setResponse(lstMecanicaOperacionalConcepto);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric cabeceraMesUno(String cdAlerta,String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();
		MecanicaOperacionalConceptoDAO mecanicaOperacionalConceptoDAO = new MecanicaOperacionalConceptoDAO();
		List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConcepto;
		try {
			lstMecanicaOperacionalConcepto = mecanicaOperacionalConceptoDAO.getCabeceraMesUno(cdAlerta,cdSistema);
			response.setResponse(lstMecanicaOperacionalConcepto);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	@Override
	public ResponseGeneric cabeceraMesDos(String cdAlerta,String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();
		MecanicaOperacionalConceptoDAO mecanicaOperacionalConceptoDAO = new MecanicaOperacionalConceptoDAO();
		List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConcepto;
		try {
			lstMecanicaOperacionalConcepto = mecanicaOperacionalConceptoDAO.getCabeceraMesDos(cdAlerta,cdSistema);
			response.setResponse(lstMecanicaOperacionalConcepto);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	@Override
	public ResponseGeneric cabeceraMesTres(String cdAlerta,String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();
		MecanicaOperacionalConceptoDAO mecanicaOperacionalConceptoDAO = new MecanicaOperacionalConceptoDAO();
		List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConcepto;
		try {
			lstMecanicaOperacionalConcepto = mecanicaOperacionalConceptoDAO.getCabeceraMesTres(cdAlerta,cdSistema);
			response.setResponse(lstMecanicaOperacionalConcepto);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	@Override
	public ResponseGeneric cabeceraMesCuatro(String cdAlerta,String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();
		MecanicaOperacionalConceptoDAO mecanicaOperacionalConceptoDAO = new MecanicaOperacionalConceptoDAO();
		List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConcepto;
		try {
			lstMecanicaOperacionalConcepto = mecanicaOperacionalConceptoDAO.getCabeceraMesCuatro(cdAlerta,cdSistema);
			response.setResponse(lstMecanicaOperacionalConcepto);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	//DETALLE DEL CASO CD_SISTEMA-CHAR
	@Override
	public ResponseGeneric consultarIdentificacionReportes2(String nuFolioAlerta,String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();
		IdentificacionReporteDTO datIdenRepoDTO = null;
		IdentificacionReporteDAO identificacionReporteDAO = new IdentificacionReporteDAO();
		try{
			datIdenRepoDTO = identificacionReporteDAO.getIdentificacionReporte2(nuFolioAlerta, cdSistema);
			response.setResponse(datIdenRepoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	
	
	
	
	
}