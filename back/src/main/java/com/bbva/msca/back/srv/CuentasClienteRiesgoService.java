package com.bbva.msca.back.srv;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.CuentasClienteRiesgoDTO;
import com.bbva.msca.back.dto.CuentasClienteRiesgoDetDTO;
import com.bbva.msca.back.dto.MovimientosClientesRiesgoDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/CuentasClienteRiesgoService")
public interface CuentasClienteRiesgoService {
	

	
	@PUT
	@Path("/altaClienteRiesgo")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric altaClienteRiesgo(CuentasClienteRiesgoDTO clienteRiesgo);
	
	@PUT
	@Path("/editarClienteRiesgo")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric editarClienteRiesgo(CuentasClienteRiesgoDTO clienteRiesgo);
	
	@GET
	@Path("/consultarRequerimiento/{cdRequerimiento}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarRequerimiento(@PathParam("cdRequerimiento") BigDecimal cdRequerimiento);		
	
	@PUT
	@Path("/altaClienteRiesgoAutomatica/{cdTipoAlta}/{cdUsuario}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric altaClienteRiesgoAutomatica(CuentasClienteRiesgoDTO clienteRiesgo, @PathParam("cdTipoAlta") BigDecimal cdTipoAlta, @PathParam("cdUsuario") String cdUsuario);	
	
	@PUT
	@Path("/actualizarPersonaReportada")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric actualizarPersonaReportada(CuentasClienteRiesgoDetDTO personaReprotada);
	
	@PUT
	@Path("/altaPersonaReportada/{cdTipoAlta}/{cdUsuario}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric altaPersonaReportada(CuentasClienteRiesgoDetDTO personaReportada, @PathParam("cdTipoAlta") BigDecimal cdTipoAlta, @PathParam("cdUsuario") String cdUsuario);
	
	@PUT
	@Path("/altaPersonaReportadaTransac/{cdTipoAlta}/{cdUsuario}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric altaPersonaReportadaTransac(List<CuentasClienteRiesgoDetDTO> lstPersonasReportadas,  @PathParam("cdTipoAlta") BigDecimal cdTipoAlta, @PathParam("cdUsuario") String cdUsuario);	
	
	@GET
	@Path("/consultarPersonasReportadas/{cdRequerimiento}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarPersonasReportadas( @PathParam("cdRequerimiento") String cdRequerimiento );	
	
	@GET
	@Path("/localizacionClienteRiesgo/{datosCliente}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric LocalizacionClienteRiesgo(@PathParam("datosCliente") CuentasClienteRiesgoDetDTO datosCliente);
	
	@GET
	@Path("/BusquedaClienteRiesgo")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric BusquedaClienteRiesgo();
	
	@GET
	@Path("/ClienteRiesgo")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric ClienteRiesgo();
	
	@POST
	@Path("/MovimientosClienteRiesgoFiltro/{nuCuenta}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric MovimientosClienteRiesgoFiltro(@PathParam("nuCuenta") String nuCuenta, MovimientosClientesRiesgoDTO movimientosClientesRiesgoDTO);

	@GET
	@Path("/MovimientosClienteRiesgo/{nuCuenta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric MovimientosClienteRiesgo(@PathParam("nuCuenta") String nuCuenta);
	
	@POST
	@Path("/adjuntarArchivo/{cdExpediente}/{nombreArchivo}/{cdUsuarioCreacion}")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric adjuntarArchivo(@PathParam("cdExpediente") String cdExpediente,@PathParam("nombreArchivo") String nombreArchivo, @PathParam("cdUsuarioCreacion") String cdUsuarioCreacion, InputStream inputStream);
	/*
	@PUT
	@Path("/adjuntarArchivo")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })	
	public ResponseGeneric adjuntarArchivo(ArchivoDTO archivo);	
	 */
	@GET
	@Path("/consultarArchivos/{cdExpediente}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarArchivosByCdExpediente( @PathParam("cdExpediente") String cdFolioArchivo);
	
	@GET
	@Path("/downloadFile/{cdFolioArchivo}")
	  @Produces(MediaType.APPLICATION_OCTET_STREAM)
	  public ResponseGeneric downloadFileById(@PathParam("cdFolioArchivo") String cdFolioarchivo);
	
	@DELETE
	@Path("/eliminarArchivo/{cdFolioArchivo}")
	public ResponseGeneric eliminarArchivo( @PathParam( "cdFolioArchivo" ) String cdFolioArchivo );
	
	@DELETE
	@Path("/eliminarPersonaReportada/{cdCliAltRie}/{nuId}")
	public ResponseGeneric eliminarPersonaReportada( @PathParam( "cdCliAltRie" ) BigDecimal cdCliAltRie,@PathParam( "nuId" ) BigDecimal nuId );		
	
}
