package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaC8DTO {

	@AnotacionAlerta(tipologia = "c8", idCampo = "cdAccion", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_CDACCION", instanceType = "1")
	private String cdAccion;
	@AnotacionAlerta(tipologia = "c8", idCampo = "nbCte", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")
	private String nbCte;
	@AnotacionAlerta(tipologia = "c8", idCampo = "nbActividad", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_NBACTIVIDAD", instanceType = "1")
	private String nbActividad;
	@AnotacionAlerta(tipologia = "c8", idCampo = "tpPerJuri", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_TPPERJURI", instanceType = "1")
	private String tpPerJuri;
	@AnotacionAlerta(tipologia = "c8", idCampo = "cdOficinaGest", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_CDOFICINAGEST", instanceType = "1")
	private String cdOficinaGest;
	@AnotacionAlerta(tipologia = "c8", idCampo = "imScore", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_IMSCORE")
	private BigDecimal imScore;
	@AnotacionAlerta(tipologia = "c8", idCampo = "ctMovimiento", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_CTMOVIMIENTO", instanceType = "1")
	private String ctMovimiento;
	@AnotacionAlerta(tipologia = "c8", idCampo = "nbSucursal", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_NBSUCURSAL", instanceType = "1")
	private String nbSucursal;
	@AnotacionAlerta(tipologia = "c8", idCampo = "nbEstado", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_NBESTADO", instanceType = "1")
	private String nbEstado;
	@AnotacionAlerta(tipologia = "c8", idCampo = "imImporteRet", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_IMIMPORTERET")
	private BigDecimal imImporteRet;
	@AnotacionAlerta(tipologia = "c8", idCampo = "tpInstMonetario", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_TPINTSMONETARIO", instanceType = "1")
	private String tpInstMonetario;
	@AnotacionAlerta(tipologia = "c8", idCampo = "nbFiller2", posicion = "12", nbEtiqueta = "MSCK_LBL_01002_NBFILLER2", instanceType = "1")
	private String nbFiller2;
	@AnotacionAlerta(tipologia = "c8", idCampo = "cdFoco", posicion = "13", nbEtiqueta = "MSCK_LBL_01002_CDFOCO", instanceType = "1")
	private String cdFoco;
	@AnotacionAlerta(tipologia = "c8", idCampo = "cdPriorSc", posicion = "14", nbEtiqueta = "MSCK_LBL_01002_CDPRIORSC", instanceType = "1")
	private String cdPriorSc;
	
	public String getCdAccion() {
		return cdAccion;
	}
	public void setCdAccion(String cdAccion) {
		this.cdAccion = cdAccion;
	}
	public String getNbCte() {
		return nbCte;
	}
	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}
	public String getNbActividad() {
		return nbActividad;
	}
	public void setNbActividad(String nbActividad) {
		this.nbActividad = nbActividad;
	}
	public String getTpPerJuri() {
		return tpPerJuri;
	}
	public void setTpPerJuri(String tpPerJuri) {
		this.tpPerJuri = tpPerJuri;
	}
	public String getCdOficinaGest() {
		return cdOficinaGest;
	}
	public void setCdOficinaGest(String cdOficinaGest) {
		this.cdOficinaGest = cdOficinaGest;
	}
	public BigDecimal getImScore() {
		return imScore;
	}
	public void setImScore(BigDecimal imScore) {
		this.imScore = imScore;
	}
	public String getCtMovimiento() {
		return ctMovimiento;
	}
	public void setCtMovimiento(String ctMovimiento) {
		this.ctMovimiento = ctMovimiento;
	}
	public String getNbSucursal() {
		return nbSucursal;
	}
	public void setNbSucursal(String nbSucursal) {
		this.nbSucursal = nbSucursal;
	}
	public String getNbEstado() {
		return nbEstado;
	}
	public void setNbEstado(String nbEstado) {
		this.nbEstado = nbEstado;
	}
	public BigDecimal getImImporteRet() {
		return imImporteRet;
	}
	public void setImImporteRet(BigDecimal imImporteRet) {
		this.imImporteRet = imImporteRet;
	}
	public String getTpInstMonetario() {
		return tpInstMonetario;
	}
	public void setTpInstMonetario(String tpInstMonetario) {
		this.tpInstMonetario = tpInstMonetario;
	}
	public String getNbFiller2() {
		return nbFiller2;
	}
	public void setNbFiller2(String nbFiller2) {
		this.nbFiller2 = nbFiller2;
	}
	public String getCdFoco() {
		return cdFoco;
	}
	public void setCdFoco(String cdFoco) {
		this.cdFoco = cdFoco;
	}
	public String getCdPriorSc() {
		return cdPriorSc;
	}
	public void setCdPriorSc(String cdPriorSc) {
		this.cdPriorSc = cdPriorSc;
	}
}
