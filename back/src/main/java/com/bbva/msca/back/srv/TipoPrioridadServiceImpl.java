package com.bbva.msca.back.srv;

import java.util.List;
import com.bbva.msca.back.dao.TipoPrioridadDAO;
import com.bbva.msca.back.dto.TipoPrioridadDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class TipoPrioridadServiceImpl implements TipoPrioridadService{
	
	@Override
	public ResponseGeneric consultarTipoPrioridad() {
		ResponseGeneric response = new ResponseGeneric();
		TipoPrioridadDAO tipoPrioridadDAO = new TipoPrioridadDAO();
		List<TipoPrioridadDTO> lstTipoPrioridadDTO = null;
		try {
			lstTipoPrioridadDTO = tipoPrioridadDAO.getTipoPrioridades();
			response.setResponse(lstTipoPrioridadDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric creaTipoPrioridad(TipoPrioridadDTO tipoPrioridad) {
		ResponseGeneric response = new ResponseGeneric();
		TipoPrioridadDAO tipoPrioridadDAO = new TipoPrioridadDAO();
		try {
			switch(tipoPrioridadDAO.creaTipoPrioridades(tipoPrioridad)){
			case Constantes.FIN_OK:
				response.setResponse(tipoPrioridad);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_AV:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_07"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric modificaTipoPrioridad(TipoPrioridadDTO tipoPrioridad) {
		ResponseGeneric response = new ResponseGeneric();
		TipoPrioridadDAO tipoPrioridadDAO = new TipoPrioridadDAO();
		TipoPrioridadDTO tipoPrioridadDTO = null;
		try {
			tipoPrioridadDTO = tipoPrioridadDAO.modificaTipoPrioridades(tipoPrioridad);
			response.setResponse(tipoPrioridadDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric consultarPrioridadPorTipo(String tipoDoc) {
		ResponseGeneric response = new ResponseGeneric();
		TipoPrioridadDAO tipoPrioridadDAO = new TipoPrioridadDAO();
		TipoPrioridadDTO tipoPrioridadDTO = null;
		try {
			tipoPrioridadDTO = tipoPrioridadDAO.getPrioridadPorTipo(tipoDoc);
			response.setResponse(tipoPrioridadDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
}
