package com.bbva.msca.back.srv;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


import com.bbva.msca.back.util.ResponseGeneric;

@Path("/MovimientosService")
public interface MovimientosService {
	
	@GET
	@Path("/consultarMovimientos/{nuFolioAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarMovimientos(@PathParam("nuFolioAlerta") String nuFolioAlerta, @PathParam("cdSistema") String cdSistema,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset,
			@QueryParam("search") String search,
			@QueryParam("name") String name,
			@QueryParam("order") String order);
	
	@GET
	@Path("/consultarMontosTotales/{nuFolioAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarMontosTotales(@PathParam("nuFolioAlerta") String nuFolioAlerta, @PathParam("cdSistema") String cdSistema);

}
