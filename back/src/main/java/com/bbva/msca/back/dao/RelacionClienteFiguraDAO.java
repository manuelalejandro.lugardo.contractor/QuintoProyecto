package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import com.bbva.msca.back.dto.RelacionClienteFiguraDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

@SuppressWarnings("unused")
public class RelacionClienteFiguraDAO {
	
	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();

		@SuppressWarnings("unchecked")
		public  RelacionClienteFiguraDTO getRelacionClienteFigura(String cdSistema, String nuFolioAlerta) throws Exception{
			RelacionClienteFiguraDTO relacionClienteFiguraDTO = new RelacionClienteFiguraDTO();
			List<RelacionClienteFiguraDTO> lstRelacionClienteFigura = null;
			
				 Query query = em.createNativeQuery(Consultas.getConsulta("getRelacionClienteFigura"));
				 query.setParameter(1,cdSistema);
				 query.setParameter(2,nuFolioAlerta);
				 query.unwrap(org.hibernate.SQLQuery.class)
					.addScalar("puestoPEP")
					.addScalar("nombrePEP")
					.addScalar("parentPEP")
					.addScalar("nombrePersRel")

				.setResultTransformer(Transformers.aliasToBean(RelacionClienteFiguraDTO.class));
					 
				 lstRelacionClienteFigura = (List<RelacionClienteFiguraDTO>)query.getResultList();
				 if ( !lstRelacionClienteFigura.isEmpty() ) 
				{
					 relacionClienteFiguraDTO = lstRelacionClienteFigura.get( 0 );
				}else{
					relacionClienteFiguraDTO.setNombrePEP(" ");
					relacionClienteFiguraDTO.setPuestoPEP(" ");
					relacionClienteFiguraDTO.setParentPEP(" ");
					relacionClienteFiguraDTO.setNombrePersRel(" ");
				}
				 
		return relacionClienteFiguraDTO;
		
	}

}
