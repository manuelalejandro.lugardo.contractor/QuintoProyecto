package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class RiesgoDTO {
	
	private String cdTipo;
	private BigDecimal nuID;
	private BigDecimal nuNumero;
	private String nbTipo;
	private Date fhRecepcion;
	private String cdPrioridad;
	private String nuOficio;
	private String nuExpediente;
	private BigDecimal nuFolio;
	private String nbDescripcion;
	private Date fhPublicacion;
	private BigDecimal nuDiasPlazo;
	private String nbAutoridad;
	private String nbReferencia;
	private String nbAseguramiento;
	private  String nbSolicitud;
	private String txtSolicitud;
	private String nuCuenta;
	private String cdTipologia;
	private String cdCliente;
	private String nbCliente;
	private String nbApPaterno;
	private String nbApMaterno;
	private String cdTPersona;
	private Date fhNacimiento;
	private String cdRFC;
	private String cdClienteB;
	private String cdDictamen;
	private String nbTipoPersona;
	private String cdCaracter;
	private String nbDomicilio;
	private String nbComplementarios;
	private BigDecimal cdClialtrie;
	private String nbPrioridad;
	
	//DATOS GENERALES CLIENTE REPORTADO
	private String nbPoblacion;
	private String nbTel1;
	private  String nbTel2;
	private String cdPais;
	private String cdNacionalidad; 
	private String cdSector;
	private Date nuAntiguedad;
	private String nbActa;
	private String nbNombre;
	
	
	//OFICINA GESTORA
	
	private String cdOficina;
	private String nbOficina;
	private String nbMercado;
	private String nbRiesgo;
	private String nbDivision;
	private String nbFuncionario;
	
	
	public String getCdOficina() {
		return cdOficina;
	}
	public void setCdOficina(String cdOficina) {
		this.cdOficina = cdOficina;
	}
	public String getNbOficina() {
		return nbOficina;
	}
	public void setNbOficina(String nbOficina) {
		this.nbOficina = nbOficina;
	}
	public String getNbMercado() {
		return nbMercado;
	}
	public void setNbMercado(String nbMercado) {
		this.nbMercado = nbMercado;
	}
	public String getNbRiesgo() {
		return nbRiesgo;
	}
	public void setNbRiesgo(String nbRiesgo) {
		this.nbRiesgo = nbRiesgo;
	}
	public String getNbDivision() {
		return nbDivision;
	}
	public void setNbDivision(String nbDivision) {
		this.nbDivision = nbDivision;
	}
	public String getNbFuncionario() {
		return nbFuncionario;
	}
	public void setNbFuncionario(String nbFuncionario) {
		this.nbFuncionario = nbFuncionario;
	}
	public String getNbNombre() {
		return nbNombre;
	}
	public void setNbNombre(String nbNombre) {
		this.nbNombre = nbNombre;
	}
	public String getCdPais() {
		return cdPais;
	}
	public void setCdPais(String cdPais) {
		this.cdPais = cdPais;
	}
	public String getNbPoblacion() {
		return nbPoblacion;
	}
	public void setNbPoblacion(String nbPoblacion) {
		this.nbPoblacion = nbPoblacion;
	}
	public String getNbTel1() {
		return nbTel1;
	}
	public void setNbTel1(String nbTel1) {
		this.nbTel1 = nbTel1;
	}
	public String getNbTel2() {
		return nbTel2;
	}
	public void setNbTel2(String nbTel2) {
		this.nbTel2 = nbTel2;
	}
	public String getCdNacionalidad() {
		return cdNacionalidad;
	}
	public void setCdNacionalidad(String cdNacionalidad) {
		this.cdNacionalidad = cdNacionalidad;
	}
	public String getCdSector() {
		return cdSector;
	}
	public void setCdSector(String cdSector) {
		this.cdSector = cdSector;
	}
	public Date getNuAntiguedad() {
		return nuAntiguedad;
	}
	public void setNuAntiguedad(Date nuAntiguedad) {
		this.nuAntiguedad = nuAntiguedad;
	}
	public String getNbActa() {
		return nbActa;
	}
	public void setNbActa(String nbActa) {
		this.nbActa = nbActa;
	}
	public String getNbComplementarios() {
		return nbComplementarios;
	}
	public void setNbComplementarios(String nbComplementarios) {
		this.nbComplementarios = nbComplementarios;
	}
	public String getNbDomicilio() {
		return nbDomicilio;
	}
	public void setNbDomicilio(String nbDomicilio) {
		this.nbDomicilio = nbDomicilio;
	}
	public String getCdCaracter() {
		return cdCaracter;
	}
	public void setCdCaracter(String cdCaracter) {
		this.cdCaracter = cdCaracter;
	}
	public String getNbTipoPersona() {
		return nbTipoPersona;
	}
	public void setNbTipoPersona(String nbTipoPersona) {
		this.nbTipoPersona = nbTipoPersona;
	}
	public String getCdDictamen() {
		return cdDictamen;
	}
	public void setCdDictamen(String cdDictamen) {
		this.cdDictamen = cdDictamen;
	}
	public String getCdClienteB() {
		return cdClienteB;
	}
	public void setCdClienteB(String cdClienteB) {
		this.cdClienteB = cdClienteB;
	}
	public String getCdRFC() {
		return cdRFC;
	}
	public void setCdRFC(String cdRFC) {
		this.cdRFC = cdRFC;
	}
	public Date getFhNacimiento() {
		return fhNacimiento;
	}
	public void setFhNacimiento(Date fhNacimiento) {
		this.fhNacimiento = fhNacimiento;
	}
	public BigDecimal getNuNumero() {
		return nuNumero;
	}
	public void setNuNumero(BigDecimal nuNumero) {
		this.nuNumero = nuNumero;
	}
	public String getCdTipo() {
		return cdTipo;
	}
	public void setCdTipo(String cdTipo) {
		this.cdTipo = cdTipo;
	}
	public String getCdPrioridad() {
		return cdPrioridad;
	}
	public void setCdPrioridad(String cdPrioridad) {
		this.cdPrioridad = cdPrioridad;
	}
	public String getCdTipologia() {
		return cdTipologia;
	}
	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public Date getFhRecepcion() {
		return fhRecepcion;
	}
	public void setFhRecepcion(Date fhRecepcion) {
		this.fhRecepcion = fhRecepcion;
	}
	
	public String getNuOficio() {
		return nuOficio;
	}
	public void setNuOficio(String nuOficio) {
		this.nuOficio = nuOficio;
	}
	public String getNuExpediente() {
		return nuExpediente;
	}
	public void setNuExpediente(String nuExpediente) {
		this.nuExpediente = nuExpediente;
	}
	public BigDecimal getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(BigDecimal nuFolio) {
		this.nuFolio = nuFolio;
	}
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	public Date getFhPublicacion() {
		return fhPublicacion;
	}
	public void setFhPublicacion(Date fhPublicacion) {
		this.fhPublicacion = fhPublicacion;
	}
	public BigDecimal getNuDiasPlazo() {
		return nuDiasPlazo;
	}
	public void setNuDiasPlazo(BigDecimal nuDiasPlazo) {
		this.nuDiasPlazo = nuDiasPlazo;
	}
	public String getNbAutoridad() {
		return nbAutoridad;
	}
	public void setNbAutoridad(String nbAutoridad) {
		this.nbAutoridad = nbAutoridad;
	}
	public String getNbReferencia() {
		return nbReferencia;
	}
	public void setNbReferencia(String nbReferencia) {
		this.nbReferencia = nbReferencia;
	}
	public String getNbAseguramiento() {
		return nbAseguramiento;
	}
	public void setNbAseguramiento(String nbAseguramiento) {
		this.nbAseguramiento = nbAseguramiento;
	}
	public String getNbSolicitud() {
		return nbSolicitud;
	}
	public void setNbSolicitud(String nbSolicitud) {
		this.nbSolicitud = nbSolicitud;
	}
	public String getNbTipo() {
		return nbTipo;
	}
	public void setNbTipo(String nbTipo) {
		this.nbTipo = nbTipo;
	}
	public BigDecimal getNuID() {
		return nuID;
	}
	public void setNuID(BigDecimal nuID) {
		this.nuID = nuID;
	}
	public String getCdTPersona() {
		return cdTPersona;
	}
	public void setCdTPersona(String cdTPersona) {
		this.cdTPersona = cdTPersona;
	}
	public String getNbApPaterno() {
		return nbApPaterno;
	}
	public void setNbApPaterno(String nbApPaterno) {
		this.nbApPaterno = nbApPaterno;
	}
	public String getNbApMaterno() {
		return nbApMaterno;
	}
	public void setNbApMaterno(String nbApMaterno) {
		this.nbApMaterno = nbApMaterno;
	}
	public BigDecimal getCdClialtrie() {
		return cdClialtrie;
	}
	public void setCdClialtrie(BigDecimal cdClialtrie) {
		this.cdClialtrie = cdClialtrie;
	}
	public String getNbPrioridad() {
		return nbPrioridad;
	}
	public void setNbPrioridad(String nbPrioridad) {
		this.nbPrioridad = nbPrioridad;
	}
	public String getTxtSolicitud() {
		return txtSolicitud;
	}
	public void setTxtSolicitud(String txtSolicitud) {
		this.txtSolicitud = txtSolicitud;
	}
	
	
	
}
