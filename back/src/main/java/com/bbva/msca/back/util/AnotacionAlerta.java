package com.bbva.msca.back.util;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface AnotacionAlerta {
	public String idCampo() default "";
	public String posicion() default "0";
	public String nbEtiqueta() default "";
	public String instanceType() default "";
	public String formatter() default "";
	public String tipologia() default "";
}
