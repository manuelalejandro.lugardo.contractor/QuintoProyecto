package com.bbva.msca.back.servicios.host.pee5;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;


/**
 * Formato de datos <code>(nombre = "PEM0E5S")</code> de la transacci&oacute;n <code>PEE5</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "PEM0E5S")
@RooJavaBean
@RooToString
@RooSerializable
public class FormatoPEM0E5S {
	
	/**
	 * <p>Campo <code>CONTENI</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "CONTENI", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 80, longitudMaxima = 80)
	private String conteni;
	
}