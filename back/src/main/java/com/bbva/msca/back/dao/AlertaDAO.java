package com.bbva.msca.back.dao;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.bbva.msca.back.dto.AlertaDTO;
import com.bbva.msca.back.dto.CampoDetalleAlertaDTO;
import com.bbva.msca.back.dto.EscenariosDTO;
import com.bbva.msca.back.dto.MatrizCriterioAlertaDTO;
import com.bbva.msca.back.dto.TipologiaBXDTO;
import com.bbva.msca.back.dto.TipologiaC8DTO;
import com.bbva.msca.back.dto.TipologiaDMDTO;
import com.bbva.msca.back.dto.TipologiaDSDTO;
import com.bbva.msca.back.dto.TipologiaE8DTO;
import com.bbva.msca.back.dto.TipologiaF1DTO;
import com.bbva.msca.back.dto.TipologiaI1DTO;
import com.bbva.msca.back.dto.TipologiaM2DTO;
import com.bbva.msca.back.dto.TipologiaM4DTO;
import com.bbva.msca.back.dto.TipologiaMDDTO;
import com.bbva.msca.back.dto.TipologiaMFDTO;
import com.bbva.msca.back.dto.TipologiaMSDTO;
import com.bbva.msca.back.dto.TipologiaMTDTO;
import com.bbva.msca.back.dto.TipologiaR1DTO;
import com.bbva.msca.back.dto.TipologiaR5DTO;
import com.bbva.msca.back.dto.TipologiaR7DTO;
import com.bbva.msca.back.dto.TipologiaR8DTO;
import com.bbva.msca.back.dto.TipologiaR9DTO;
import com.bbva.msca.back.dto.TipologiaRPDTO;
import com.bbva.msca.back.dto.TipologiaRVDTO;
import com.bbva.msca.back.dto.TipologiaTCDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.AnotacionAlerta;
import com.bbva.msca.back.util.Consultas;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.EnumTipologia;

public class AlertaDAO {

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	@SuppressWarnings("unchecked")
	public AlertaDTO getAlerta(String cdAlerta, String cdSistema)
			throws Exception {
		AlertaDTO alertaDTO = new AlertaDTO();
		List<AlertaDTO> lstResultado = new ArrayList<AlertaDTO>();
		Query query = em.createNativeQuery(Consultas
				.getConsulta("getAlertaSistema"));
		query.setParameter(1, cdSistema);
		query.setParameter(2, cdAlerta);
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("nuFolioAlerta", StringType.INSTANCE)
				.addScalar("cdSistema", StringType.INSTANCE)
				.addScalar("nbSistema")
				.addScalar("cdTipologia", StringType.INSTANCE)
				.addScalar("imAlerta")
				.addScalar("cdSesion")
				.addScalar("cdSegmento", StringType.INSTANCE)
				.addScalar("nbSegmento")
				.addScalar("fhAlerta", TimestampType.INSTANCE)
				.addScalar("fhEnvio", TimestampType.INSTANCE)
				.addScalar("cdDivisa", StringType.INSTANCE)
				.addScalar("cdStAlerta")
				.addScalar("nbStAlerta")
				.addScalar("txMotivoDescarte")
				.addScalar("stRepAut", StringType.INSTANCE)
				.addScalar("nuScoreMc")
				.addScalar("nuScoreMdl")
				.addScalar("fhAsignacion", TimestampType.INSTANCE)
				.addScalar("fhVenRev", TimestampType.INSTANCE)
				.addScalar("fhVenLibSia", TimestampType.INSTANCE)
				.addScalar("fhRepAut", TimestampType.INSTANCE)
				.addScalar("cdPrioriAsig")
				.addScalar("nuCliente", StringType.INSTANCE)
				.addScalar("nuCuenta", StringType.INSTANCE)
				.addScalar("cdCaso")
				.setResultTransformer(Transformers.aliasToBean(AlertaDTO.class));
		
		lstResultado = (List<AlertaDTO>) query.getResultList();
		if(lstResultado != null && !lstResultado.isEmpty()){
			alertaDTO = lstResultado.get(0);
		}
		return alertaDTO;
	}

	public AlertaDTO getAlertaTipologia(String cdAlerta, String cdSistema) throws Exception {
		AlertaDTO alertaDTO = this.getAlerta(cdAlerta, cdSistema);
		alertaDTO.setCamposDetalleAlerta(getDetalleTipologia(alertaDTO.getCdTipologia(), cdAlerta, cdSistema));
		return alertaDTO;
	}

	public AlertaDTO getAlertaEscenarioTipologia(String cdAlerta,String cdSistema) throws Exception {
		AlertaDTO alertaDTO = this.getAlertaTipologia(cdAlerta, cdSistema);
		alertaDTO.setEscenariosAlerta(this.getEscenariosAlerta(cdAlerta,cdSistema));
		return alertaDTO;
	}

	@SuppressWarnings("unchecked")
	public List<CampoDetalleAlertaDTO> getDetalleTipologia(String cdTipologia,
			String cdAlerta, String cdSistema) throws Exception {
		List<CampoDetalleAlertaDTO> camposDetalleAlerta = null;
		Map<Integer, Object> parametros = new HashMap<Integer, Object>();
		parametros = new HashMap<Integer, Object>();
		parametros.put(1, cdAlerta);
		parametros.put(2, cdSistema);
		parametros.put(3, cdTipologia);
		Query query = null;
		if( cdTipologia != null && !cdTipologia.equals( "" )){
			switch (EnumTipologia.valueOf(cdTipologia)) {
			case B1:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaBXDTO> lstResultado = new ArrayList<TipologiaBXDTO>();
				TipologiaBXDTO tipologiaB1 = new TipologiaBXDTO();
				lstResultado = (List<TipologiaBXDTO>) query.getResultList();
				if(lstResultado != null && !lstResultado.isEmpty()){
					tipologiaB1 = lstResultado.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaB1);
				break;
			case B2:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				
				List<TipologiaBXDTO> lstResultadoB2 = new ArrayList<TipologiaBXDTO>();
				TipologiaBXDTO tipologiaB2 = new TipologiaBXDTO();
				lstResultadoB2 = (List<TipologiaBXDTO>) query.getResultList();
				if(lstResultadoB2 != null && !lstResultadoB2.isEmpty()){
					tipologiaB2 = lstResultadoB2.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaB2);
				break;
			case B3:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaBXDTO> lstResultadoB3 = new ArrayList<TipologiaBXDTO>();
				TipologiaBXDTO tipologiaB3 = new TipologiaBXDTO();
				lstResultadoB3 = (List<TipologiaBXDTO>) query.getResultList();
				if(lstResultadoB3 != null && !lstResultadoB3.isEmpty()){
					tipologiaB3 = lstResultadoB3.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaB3);
				break;
			case B4:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaBXDTO> lstResultadoB4 = new ArrayList<TipologiaBXDTO>();
				TipologiaBXDTO tipologiaB4 = new TipologiaBXDTO();
				lstResultadoB4 = (List<TipologiaBXDTO>) query.getResultList();
				if(lstResultadoB4 != null && !lstResultadoB4.isEmpty()){
					tipologiaB4 = lstResultadoB4.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaB4);
				break;
			case B5:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaBXDTO> lstResultadoB5 = new ArrayList<TipologiaBXDTO>();
				TipologiaBXDTO tipologiaB5 = new TipologiaBXDTO();
				lstResultadoB5 = (List<TipologiaBXDTO>) query.getResultList();
				if(lstResultadoB5 != null && !lstResultadoB5.isEmpty()){
					tipologiaB5 = lstResultadoB5.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaB5);
				break;
			case B6:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaBXDTO> lstResultadoB6 = new ArrayList<TipologiaBXDTO>();
				TipologiaBXDTO tipologiaB6 = new TipologiaBXDTO();
				lstResultadoB6 = (List<TipologiaBXDTO>) query.getResultList();
				if(lstResultadoB6 != null && !lstResultadoB6.isEmpty()){
					tipologiaB6 = lstResultadoB6.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaB6);
				break;
			case B7:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				TipologiaBXDTO tipologiaB7 = (TipologiaBXDTO) query
						.getSingleResult();
				camposDetalleAlerta = getListaCamposAlerta(tipologiaB7);
				break;
			case B8:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaBXDTO> lstResultadoB8 = new ArrayList<TipologiaBXDTO>();
				TipologiaBXDTO tipologiaB8 = new TipologiaBXDTO();
				lstResultadoB8 = (List<TipologiaBXDTO>) query.getResultList();
				if(lstResultadoB8 != null && !lstResultadoB8.isEmpty()){
					tipologiaB8 = lstResultadoB8.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaB8);
				break;
			case B9:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaBXDTO> lstResultadoB9 = new ArrayList<TipologiaBXDTO>();
				TipologiaBXDTO tipologiaB9 = new TipologiaBXDTO();
				lstResultadoB9 = (List<TipologiaBXDTO>) query.getResultList();
				if(lstResultadoB9 != null && !lstResultadoB9.isEmpty()){
					tipologiaB9 = lstResultadoB9.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaB9);
				break;
			case BX:// OK
				query = this.getQueryTipologia(TipologiaBXDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaBXDTO> lstResultadoBX = new ArrayList<TipologiaBXDTO>();
				TipologiaBXDTO tipologiaBX = new TipologiaBXDTO();
				lstResultadoBX = (List<TipologiaBXDTO>) query.getResultList();
				if(lstResultadoBX != null && !lstResultadoBX.isEmpty()){
					tipologiaBX = lstResultadoBX.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaBX);
				break;
			case C1:// RQ01.FN03
				break;
			case C6:// RQ01.FN03
				break;
			case C7:// RQ01.FN03
				break;
			case C8:// OK
				query = this.getQueryTipologia(TipologiaC8DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaC8DTO> lstResultadoC8 = new ArrayList<TipologiaC8DTO>();
				TipologiaC8DTO tipologiaC8 = new TipologiaC8DTO();
				lstResultadoC8 = (List<TipologiaC8DTO>) query.getResultList();
				if(lstResultadoC8 != null && !lstResultadoC8.isEmpty()){
					tipologiaC8 = lstResultadoC8.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaC8);
				break;
			case DM:// OK
				query = this.getQueryTipologia(TipologiaDMDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaDMDTO> lstResultadoDM = new ArrayList<TipologiaDMDTO>();
				TipologiaDMDTO tipologiaDM = new TipologiaDMDTO();
				lstResultadoDM = (List<TipologiaDMDTO>) query.getResultList();
				if(lstResultadoDM != null && !lstResultadoDM.isEmpty()){
					tipologiaDM = lstResultadoDM.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaDM);
				break;
			case DS:// OK
				query = this.getQueryTipologia(TipologiaDSDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaDSDTO> lstResultadoDS = new ArrayList<TipologiaDSDTO>();
				TipologiaDSDTO tipologiaDS = new TipologiaDSDTO();
				lstResultadoDS = (List<TipologiaDSDTO>) query.getResultList();
				if(lstResultadoDS != null && !lstResultadoDS.isEmpty()){
					tipologiaDS = lstResultadoDS.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaDS);
				break;
			case AI:// OK
				query = this.getQueryTipologia(TipologiaE8DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaE8DTO> lstResultadoAI = new ArrayList<TipologiaE8DTO>();
				TipologiaE8DTO tipologiaAI = new TipologiaE8DTO();
				lstResultadoAI = (List<TipologiaE8DTO>) query.getResultList();
				if(lstResultadoAI != null && !lstResultadoAI.isEmpty()){
					tipologiaAI = lstResultadoAI.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaAI);
				break;
			case E1:// OK
				query = this.getQueryTipologia(TipologiaE8DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaE8DTO> lstResultadoE1 = new ArrayList<TipologiaE8DTO>();
				TipologiaE8DTO tipologiaE1 = new TipologiaE8DTO();
				lstResultadoE1 = (List<TipologiaE8DTO>) query.getResultList();
				if(lstResultadoE1 != null && !lstResultadoE1.isEmpty()){
					tipologiaE1 = lstResultadoE1.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaE1);
				break;
			case E3:// / OK
				query = this.getQueryTipologia(TipologiaE8DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaE8DTO> lstResultadoE3 = new ArrayList<TipologiaE8DTO>();
				TipologiaE8DTO tipologiaE3 = new TipologiaE8DTO();
				lstResultadoE3 = (List<TipologiaE8DTO>) query.getResultList();
				if(lstResultadoE3 != null && !lstResultadoE3.isEmpty()){
					tipologiaE3 = lstResultadoE3.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaE3);
				break;
			case IP:// OK
				query = this.getQueryTipologia(TipologiaE8DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaE8DTO> lstResultadoIP = new ArrayList<TipologiaE8DTO>();
				TipologiaE8DTO tipologiaIP = new TipologiaE8DTO();
				lstResultadoIP = (List<TipologiaE8DTO>) query.getResultList();
				if(lstResultadoIP != null && !lstResultadoIP.isEmpty()){
					tipologiaIP = lstResultadoIP.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaIP);
				break;
			case E8:// OK
				query = this.getQueryTipologia(TipologiaE8DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaE8DTO> lstResultadoE8 = new ArrayList<TipologiaE8DTO>();
				TipologiaE8DTO tipologiaE8 = new TipologiaE8DTO();
				lstResultadoE8 = (List<TipologiaE8DTO>) query.getResultList();
				if(lstResultadoE8 != null && !lstResultadoE8.isEmpty()){
					tipologiaE8 = lstResultadoE8.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaE8);
				break;
			case M2:// OK
				query = this.getQueryTipologia(TipologiaM2DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaM2DTO> lstResultadoM2 = new ArrayList<TipologiaM2DTO>();
				TipologiaM2DTO tipologiaM2 = new TipologiaM2DTO();
				lstResultadoM2 = (List<TipologiaM2DTO>) query.getResultList();
				if(lstResultadoM2 != null && !lstResultadoM2.isEmpty()){
					tipologiaM2 = lstResultadoM2.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaM2);
				break;
			case F1:// OK
				query = this.getQueryTipologia(TipologiaF1DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaF1DTO> lstResultadoF1 = new ArrayList<TipologiaF1DTO>();
				TipologiaF1DTO tipologiaF1 = new TipologiaF1DTO();
				lstResultadoF1 = (List<TipologiaF1DTO>) query.getResultList();
				if(lstResultadoF1 != null && !lstResultadoF1.isEmpty()){
					tipologiaF1 = lstResultadoF1.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaF1);
				break;
			case I1:// OK
				query = this.getQueryTipologia(TipologiaI1DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaI1DTO> lstResultadoI1 = new ArrayList<TipologiaI1DTO>();
				TipologiaI1DTO tipologiaI1 = new TipologiaI1DTO();
				lstResultadoI1 = (List<TipologiaI1DTO>) query.getResultList();
				if(lstResultadoI1 != null && !lstResultadoI1.isEmpty()){
					tipologiaI1 = lstResultadoI1.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaI1);
				break;
			case E4:// RQ01.FN03
				break;
			case E5:// RQ01.FN03
				break;
			case E9:// RQ01.FN03
				break;
			case F2:// RQ01.FN03
				break;
			case FF:// RQ01.FN03
				break;
			case L2:// RQ01.FN03
				break;
			case L3:// RQ01.FN03
				break;
			case L4:// RQ01.FN03
				break;
			case L5:// RQ01.FN03
				break;
			case L6:// RQ01.FN03
				break;
			case L7:// RQ01.FN03
				break;
			case L8:// RQ01.FN03
				break;
			case L9:// RQ01.FN03
				break;
			case LB:// RQ01.FN03
				break;
			case LC:// RQ01.FN03
				break;
			case LD:// RQ01.FN03
				break;
			case LR:// RQ01.FN03
				break;
			case LS:// RQ01.FN03
				break;
			case O3:// RQ01.FN03
				break;
			case O4:// RQ01.FN03
				break;
			case M4:// OK
				query = this.getQueryTipologia(TipologiaM4DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaM4DTO> lstResultadoM4 = new ArrayList<TipologiaM4DTO>();
				TipologiaM4DTO tipologiaM4 = new TipologiaM4DTO();
				lstResultadoM4 = (List<TipologiaM4DTO>) query.getResultList();
				if(lstResultadoM4 != null && !lstResultadoM4.isEmpty()){
					tipologiaM4 = lstResultadoM4.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaM4);
				break;
			case MB:// NO HAY LAYOUT
				// query = this.getQueryTipologia(TipologiaMBDTO.class, parametros,
				// "getDetalleTipologiaGenericaAlerta");
				// TipologiaMBDTO tipologiaMB = (TipologiaMBDTO) query
				// .getSingleResult();
				// camposDetalleAlerta = getListaCamposAlerta(tipologiaMB);
				break;
			case MD:// OK
				query = this.getQueryTipologia(TipologiaMDDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaMDDTO> lstResultadoMD = new ArrayList<TipologiaMDDTO>();
				TipologiaMDDTO tipologiaMD = new TipologiaMDDTO();
				lstResultadoMD = (List<TipologiaMDDTO>) query.getResultList();
				if(lstResultadoMD != null && !lstResultadoMD.isEmpty()){
					tipologiaMD = lstResultadoMD.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaMD);
				break;
			case ME:// NO ESTA VALIDAR
				// query = this.getQueryTipologia(TipologiaMEDTO.class, parametros,
				// "getDetalleTipologiaGenericaAlerta");
				// TipologiaMEDTO tipologiaME = (TipologiaMEDTO) query
				// .getSingleResult();
				// camposDetalleAlerta = getListaCamposAlerta(tipologiaME);
				break;
			case MF:// OK
				query = this.getQueryTipologia(TipologiaMFDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaMFDTO> lstResultadoMF = new ArrayList<TipologiaMFDTO>();
				TipologiaMFDTO tipologiaMF = new TipologiaMFDTO();
				lstResultadoMF = (List<TipologiaMFDTO>) query.getResultList();
				if(lstResultadoMF != null && !lstResultadoMF.isEmpty()){
					tipologiaMF = lstResultadoMF.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaMF);
				break;
			case MP:// RQ01.FN03
				break;
			case MR:// RQ01.FN03
				break;
			case MS://OK
				query = this.getQueryTipologia(TipologiaMSDTO.class, parametros,
				"getDetalleTipologiaGenericaAlerta");
				List<TipologiaMSDTO> lstResultadoMS = new ArrayList<TipologiaMSDTO>();
				TipologiaMSDTO tipologiaMS = new TipologiaMSDTO();
				lstResultadoMS = (List<TipologiaMSDTO>) query.getResultList();
				if(lstResultadoMS != null && !lstResultadoMS.isEmpty()){
					tipologiaMS = lstResultadoMS.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaMS);
				break;
			case MT:// OK
				query = this.getQueryTipologia(TipologiaMTDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaMTDTO> lstResultadoMT = new ArrayList<TipologiaMTDTO>();
				TipologiaMTDTO tipologiaMT = new TipologiaMTDTO();
				lstResultadoMT = (List<TipologiaMTDTO>) query.getResultList();
				if(lstResultadoMT != null && !lstResultadoMT.isEmpty()){
					tipologiaMT = lstResultadoMT.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaMT);
				break;
			case TC:// OK
				query = this.getQueryTipologia(TipologiaTCDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaTCDTO> lstResultadoTC = new ArrayList<TipologiaTCDTO>();
				TipologiaTCDTO tipologiaTC = new TipologiaTCDTO();
				lstResultadoTC = (List<TipologiaTCDTO>) query.getResultList();
				if(lstResultadoTC != null && !lstResultadoTC.isEmpty()){
					tipologiaTC = lstResultadoTC.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaTC);
				break;
			case R5:// OK
				query = this.getQueryTipologia(TipologiaR5DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaR5DTO> lstResultadoR5 = new ArrayList<TipologiaR5DTO>();
				TipologiaR5DTO tipologiaR5 = new TipologiaR5DTO();
				lstResultadoR5 = (List<TipologiaR5DTO>) query.getResultList();
				if(lstResultadoR5 != null && !lstResultadoR5.isEmpty()){
					tipologiaR5 = lstResultadoR5.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaR5);
				break;
			case R7:// OK
				query = this.getQueryTipologia(TipologiaR7DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaR7DTO> lstResultadoR7 = new ArrayList<TipologiaR7DTO>();
				TipologiaR7DTO tipologiaR7 = new TipologiaR7DTO();
				lstResultadoR7 = (List<TipologiaR7DTO>) query.getResultList();
				if(lstResultadoR7 != null && !lstResultadoR7.isEmpty()){
					tipologiaR7 = lstResultadoR7.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaR7);
				break;
			case R8:// OK
				query = this.getQueryTipologia(TipologiaR8DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaR8DTO> lstResultadoR8 = new ArrayList<TipologiaR8DTO>();
				TipologiaR8DTO tipologiaR8 = new TipologiaR8DTO();
				lstResultadoR8 = (List<TipologiaR8DTO>) query.getResultList();
				if(lstResultadoR8 != null && !lstResultadoR8.isEmpty()){
					tipologiaR8 = lstResultadoR8.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaR8);
				break;
			case R9:// OK
				query = this.getQueryTipologia(TipologiaR9DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaR9DTO> lstResultadoR9 = new ArrayList<TipologiaR9DTO>();
				TipologiaR9DTO tipologiaR9 = new TipologiaR9DTO();
				lstResultadoR9 = (List<TipologiaR9DTO>) query.getResultList();
				if(lstResultadoR9 != null && !lstResultadoR9.isEmpty()){
					tipologiaR9 = lstResultadoR9.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaR9);
				break;
			case RD:// RQ01.FN03
			case RP:// OK
				query = this.getQueryTipologia(TipologiaRPDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaRPDTO> lstResultadoRP = new ArrayList<TipologiaRPDTO>();
				TipologiaRPDTO tipologiaRP = new TipologiaRPDTO();
				lstResultadoRP = (List<TipologiaRPDTO>) query.getResultList();
				if(lstResultadoRP != null && !lstResultadoRP.isEmpty()){
					tipologiaRP = lstResultadoRP.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaRP);
				break;
			case RV:// OK
				query = this.getQueryTipologia(TipologiaRVDTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaRVDTO> lstResultadoRV = new ArrayList<TipologiaRVDTO>();
				TipologiaRVDTO tipologiaRV = new TipologiaRVDTO();
				lstResultadoRV = (List<TipologiaRVDTO>) query.getResultList();
				if(lstResultadoRV != null && !lstResultadoRV.isEmpty()){
					tipologiaRV = lstResultadoRV.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaRV);
				break;
			case TD:// RQ01.FN03
				break;
			case M3:// RQ01.FN03
				break;
			case NM:// RQ01.FN03
				break;
			case R1:// OK
				query = this.getQueryTipologia(TipologiaR1DTO.class, parametros,
						"getDetalleTipologiaGenericaAlerta");
				List<TipologiaR1DTO> lstResultadoR1 = new ArrayList<TipologiaR1DTO>();
				TipologiaR1DTO tipologiaR1 = new TipologiaR1DTO();
				lstResultadoR1 = (List<TipologiaR1DTO>) query.getResultList();
				if(lstResultadoR1 != null && !lstResultadoR1.isEmpty()){
					tipologiaR1 = lstResultadoR1.get(0);
				}
				camposDetalleAlerta = getListaCamposAlerta(tipologiaR1);
				break;
			}

		}
				return camposDetalleAlerta;
	}

	@SuppressWarnings("unchecked")
	public List<EscenariosDTO> getEscenariosAlerta(String cdAlerta,
			String cdSistema) throws Exception {
		List<EscenariosDTO> escenariosAlerta = new ArrayList<EscenariosDTO>();
		Query query = em.createNativeQuery(Consultas
				.getConsulta("getEscenariosAlertaSistema"));
		query.setParameter(1, cdAlerta);
		query.setParameter(2, cdSistema);
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdEscenario", StringType.INSTANCE)
				.addScalar("cdSistema", StringType.INSTANCE)
				.addScalar("nbEscenario")
				.addScalar("cdFoco", StringType.INSTANCE)
				.addScalar("txDescEscenario")
				.addScalar("nbHighlight")
				.setResultTransformer(
						Transformers.aliasToBean(EscenariosDTO.class));
		escenariosAlerta = (List<EscenariosDTO>) query.getResultList();
		return escenariosAlerta;
	}

	@SuppressWarnings("rawtypes")
	private Query getQueryTipologia(Class clase,
			Map<Integer, Object> parametros, String queryTipologia)
			throws Exception {
		String idCampoQuery = "";
		Query query = em.createNativeQuery(Consultas
				.getConsulta(queryTipologia));
		for (Integer pos : parametros.keySet()) {
			query.setParameter(pos, parametros.get(pos));
		}
		for (Field campo : clase.getDeclaredFields()) {
			AnotacionAlerta anota = campo.getAnnotation(AnotacionAlerta.class);
			idCampoQuery = anota.idCampo();
			switch (anota.instanceType().isEmpty() ? 0 : Integer.parseInt(anota
					.instanceType())) {
			case 0:
				query.unwrap(org.hibernate.SQLQuery.class).addScalar(
						idCampoQuery);
				break;
			case 1:
				query.unwrap(org.hibernate.SQLQuery.class).addScalar(
						idCampoQuery, StringType.INSTANCE);
				break;
			case 2:
				query.unwrap(org.hibernate.SQLQuery.class).addScalar(
						idCampoQuery, TimestampType.INSTANCE);
				break;
			}
		}
		query.unwrap(org.hibernate.SQLQuery.class).setResultTransformer(
				Transformers.aliasToBean(clase));
		return query;
	}

	@SuppressWarnings("rawtypes")
	private List<CampoDetalleAlertaDTO> getListaCamposAlerta(Object tipologiaXX)
			throws Exception {
		Class clase = tipologiaXX.getClass();
		List<CampoDetalleAlertaDTO> campos = new ArrayList<CampoDetalleAlertaDTO>();
		Field[] camposDTO = clase.getDeclaredFields();
		for (Field campo : camposDTO) {
			CampoDetalleAlertaDTO campoHtml = new CampoDetalleAlertaDTO();
			AnotacionAlerta anota = campo.getAnnotation(AnotacionAlerta.class);
			BeanUtils.setProperty(campoHtml, "idCampo", anota.tipologia() + "_"
					+ anota.idCampo());
			BeanUtils.setProperty(campoHtml, "nbCampo",
					PropertyUtils.getProperty(tipologiaXX, anota.idCampo()));
			BeanUtils.setProperty(campoHtml, "posicion", anota.posicion());
			BeanUtils.setProperty(campoHtml, "formatter", anota.formatter());
			if (!anota.nbEtiqueta().isEmpty()) {
				BeanUtils.setProperty(campoHtml, "lblCampo",
						MensajesI18n.getText(anota.nbEtiqueta()));
			} else {
				BeanUtils.setProperty(campoHtml, "lblCampo", "");
			}
			campos.add(campoHtml);
		}
		return campos;
	}
	
	@SuppressWarnings("unchecked")
	public List<MatrizCriterioAlertaDTO> getMatrizCriterioAlerta(String cdAlerta, String cdSistema) throws Exception {
		List<MatrizCriterioAlertaDTO> matrizCriterioAlertaDTO = new ArrayList<MatrizCriterioAlertaDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("getMatrizCriterioAlerta"));
		query.setParameter(1, cdAlerta);
		query.setParameter(2, cdSistema);
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdCriterio")
				.addScalar("nbCriterio")
				.addScalar("nuPuntaje")
				.setResultTransformer(Transformers.aliasToBean(MatrizCriterioAlertaDTO.class));
		matrizCriterioAlertaDTO = (List<MatrizCriterioAlertaDTO>) query.getResultList();
		return matrizCriterioAlertaDTO;
	}

}
