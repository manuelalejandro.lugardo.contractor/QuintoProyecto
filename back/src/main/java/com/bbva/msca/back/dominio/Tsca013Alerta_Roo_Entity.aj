// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca013Alerta;
import com.bbva.msca.back.dominio.Tsca013AlertaPK;
import java.util.List;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca013Alerta_Roo_Entity {
    
    declare @type: Tsca013Alerta: @Entity;
    
    declare @type: Tsca013Alerta: @Table(name = "TSCA013_ALERTA", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca013Alerta.entityManager;
    
    @EmbeddedId
    private Tsca013AlertaPK Tsca013Alerta.id;
    
    public Tsca013AlertaPK Tsca013Alerta.getId() {
        return this.id;
    }
    
    public void Tsca013Alerta.setId(Tsca013AlertaPK id) {
        this.id = id;
    }
    
    @Transactional
    public void Tsca013Alerta.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca013Alerta.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca013Alerta attached = Tsca013Alerta.findTsca013Alerta(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca013Alerta.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca013Alerta.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca013Alerta Tsca013Alerta.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca013Alerta merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca013Alerta.entityManager() {
        EntityManager em = new Tsca013Alerta().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca013Alerta.countTsca013Alertas() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca013Alerta o", Long.class).getSingleResult();
    }
    
    public static List<Tsca013Alerta> Tsca013Alerta.findAllTsca013Alertas() {
        return entityManager().createQuery("SELECT o FROM Tsca013Alerta o", Tsca013Alerta.class).getResultList();
    }
    
    public static Tsca013Alerta Tsca013Alerta.findTsca013Alerta(Tsca013AlertaPK id) {
        if (id == null) return null;
        return entityManager().find(Tsca013Alerta.class, id);
    }
    
    public static List<Tsca013Alerta> Tsca013Alerta.findTsca013AlertaEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca013Alerta o", Tsca013Alerta.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
