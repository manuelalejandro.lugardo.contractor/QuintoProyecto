package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class AsignacionSesionDTO extends SesionDTO {
	
	private String nbSesion;
	private BigDecimal cdGerencia;
	private String nbGerencia;
	private BigDecimal nuAlertas;
	private String cdDetCatalogo;
	
	
	public String getNbSesion() {
		return nbSesion;
	}
	public void setNbSesion(String nbSesion) {
		this.nbSesion = nbSesion;
	}
	public BigDecimal getCdGerencia() {
		return cdGerencia;
	}
	public void setCdGerencia(BigDecimal cdGerencia) {
		this.cdGerencia = cdGerencia;
	}
	public String getNbGerencia() {
		return nbGerencia;
	}
	public void setNbGerencia(String nbGerencia) {
		this.nbGerencia = nbGerencia;
	}
	public BigDecimal getNuAlertas() {
		return nuAlertas;
	}
	public void setNuAlertas(BigDecimal nuAletas) {
		this.nuAlertas = nuAletas;
	}
	public void setCdDetCatalogo(String cdDetCatalogo) {
		this.cdDetCatalogo = cdDetCatalogo;
	}
	public String getCdDetCatalogo() {
		return cdDetCatalogo;
	}

}
