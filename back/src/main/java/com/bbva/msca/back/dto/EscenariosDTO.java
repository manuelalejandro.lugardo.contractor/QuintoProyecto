package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class EscenariosDTO {

	private String cdEscenario;
	private String cdSistema;
	private String nbEscenario;
	private String cdFoco;
	private String txDescEscenario;
	private String nbHighlight;

	public void setCdEscenario(String cdEscenario) {
		this.cdEscenario = cdEscenario;
	}

	public String getCdEscenario() {
		return cdEscenario;
	}

	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	public String getCdSistema() {
		return cdSistema;
	}

	public void setNbEscenario(String nbEscenario) {
		this.nbEscenario = nbEscenario;
	}

	public String getNbEscenario() {
		return nbEscenario;
	}

	public void setCdFoco(String cdFoco) {
		this.cdFoco = cdFoco;
	}

	public String getCdFoco() {
		return cdFoco;
	}

	public void setTxDescEscenario(String txDescEscenario) {
		this.txDescEscenario = txDescEscenario;
	}

	public String getTxDescEscenario() {
		return txDescEscenario;
	}

	public void setNbHighlight(String nbHighlight) {
		this.nbHighlight = nbHighlight;
	}

	public String getNbHighlight() {
		return nbHighlight;
	}

}
