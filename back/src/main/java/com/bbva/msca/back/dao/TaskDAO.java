package com.bbva.msca.back.dao;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.util.BdSac;

public class TaskDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	@Transactional
	public void ejecutaStoreProcedure(String SP)throws Exception{
		Query query = em.createNativeQuery(SP);
		query.executeUpdate();
	}
	
	
	public void ejecutarBat(String comando)throws Exception{ 
	    try { 
	        String linea; 
	        Process p = Runtime.getRuntime().exec(comando); 
	        BufferedReader input = new BufferedReader (new InputStreamReader (p.getInputStream())); 
	        while ((linea = input.readLine()) != null) { 
	             System.out.println(linea); 
	         } 
	           input.close(); 
	    }catch (Exception err) { 
	           err.printStackTrace(); 
	    } 
	  }  
}
