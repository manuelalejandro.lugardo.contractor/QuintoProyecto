package com.bbva.msca.back.srv;

import java.math.BigDecimal;
import java.util.List;

import com.bbva.msca.back.dao.RiesgoDAO;
import com.bbva.msca.back.dto.RiesgoDTO;

import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class ConsultaClienteRiesgoServiceImpl implements ConsultaClienteRiesgoService {
	//Consultar Reportes de Riesgo
	@Override
	public ResponseGeneric consultarRiesgos() {
		ResponseGeneric response = new ResponseGeneric();
		RiesgoDAO riesgoDAO = new RiesgoDAO();
		
		try{
			List<RiesgoDTO> lstRiesgoDTO = riesgoDAO.getRiesgos();
			response.setResponse(lstRiesgoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	


    //Parametros de Busqueda de Clientes de Riesgo
	@Override
	public ResponseGeneric consultarRiesgosFiltro(RiesgoDTO riesgoDTO) {
		ResponseGeneric response = new ResponseGeneric();
		RiesgoDAO riesgoDAO = new RiesgoDAO();
		
		try{
			List<RiesgoDTO> lstRiesgoDTO = riesgoDAO.getRiesgos(riesgoDTO);
			response.setResponse(lstRiesgoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	//Personas Reportadas
	
	@Override
	public ResponseGeneric consultarPersonasReportadas(BigDecimal cdClialtrie) {
		
		ResponseGeneric response = new ResponseGeneric();
		RiesgoDAO riesgoDAO = new RiesgoDAO();
		
		try{
			List<RiesgoDTO> lstRiesgoDTO = riesgoDAO.getPersonasReportados(cdClialtrie);
			response.setResponse(lstRiesgoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}



	@Override
	public ResponseGeneric consultarDatosPersonaReportada(String nuCuenta) {
			ResponseGeneric response = new ResponseGeneric();
			RiesgoDTO riesgoDTO = null;
			RiesgoDAO riesgoDAO = new RiesgoDAO();
			try{
				riesgoDTO = riesgoDAO.getConsultarDatosPersonaReportada(nuCuenta);
				response.setResponse(riesgoDTO);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			}catch(Exception e){e.printStackTrace();
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			}
		return response;
		}



	@Override
	public ResponseGeneric consultarDatosOficinaPersonaReportada(String cdCliente) {
		ResponseGeneric response = new ResponseGeneric();
		RiesgoDTO riesgoDTO = null;
		RiesgoDAO riesgoDAO = new RiesgoDAO();
		try{
			riesgoDTO = riesgoDAO.getConsultarDatosOficinaPersonaReportada(cdCliente);
			response.setResponse(riesgoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
	return response;
	}



	@Override
	public ResponseGeneric consultarOtrosClientesPersonaReportada(String cdCliente) {
		ResponseGeneric response = new ResponseGeneric();
		RiesgoDAO riesgoDAO = new RiesgoDAO();
		try{
			List<RiesgoDTO> lstRiesgoDTO = riesgoDAO.getConsultarOtrosClientesPersonaReportada(cdCliente);			
			response.setResponse(lstRiesgoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
	return response;
	}
}



	


