package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.bbva.msca.back.dto.CuentasClienteFidDTO;

public class CuentasClientesFidDAO {
	
	public List<CuentasClienteFidDTO> getCuentasClienteFid(){
		
		 List<CuentasClienteFidDTO> lstCuentasClienteFidDTO = new ArrayList<CuentasClienteFidDTO>();
		
		 CuentasClienteFidDTO cuentasClienteFidDTO = null;
		
			for(int i=0;i<10;i++){
			
				cuentasClienteFidDTO = new CuentasClienteFidDTO();
				
				cuentasClienteFidDTO.setFhApertura(new Date());
				cuentasClienteFidDTO.setNuCuenta(123);
				cuentasClienteFidDTO.setNbProducto("");
				cuentasClienteFidDTO.setNbSubproducto("");
				cuentasClienteFidDTO.setNbEstatus("");
				cuentasClienteFidDTO.setFhCancelacion(new Date());
				cuentasClienteFidDTO.setNbTitular("");
				cuentasClienteFidDTO.setNuCtePart(11);
				cuentasClienteFidDTO.setNbParticipes("");
				cuentasClienteFidDTO.setImProducto(12);
				cuentasClienteFidDTO.setImProProm(12);
				
				lstCuentasClienteFidDTO.add(cuentasClienteFidDTO);
			
			}
			return lstCuentasClienteFidDTO;
		}

}
