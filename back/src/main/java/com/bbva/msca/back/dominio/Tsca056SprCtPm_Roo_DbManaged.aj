// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca016Cliente;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

privileged aspect Tsca056SprCtPm_Roo_DbManaged {
    
    @ManyToOne
    @JoinColumns({ @JoinColumn(name = "CD_SUPER_CTE", referencedColumnName = "CD_CLIENTE", nullable = false, insertable = false, updatable = false), @JoinColumn(name = "CD_CASO", referencedColumnName = "CD_CASO", nullable = false, insertable = false, updatable = false) })
    private Tsca016Cliente Tsca056SprCtPm.tsca016Cliente;
    
    @ManyToOne
    @JoinColumns({ @JoinColumn(name = "CD_CLIENTE", referencedColumnName = "CD_CLIENTE", nullable = false, insertable = false, updatable = false), @JoinColumn(name = "CD_CASO", referencedColumnName = "CD_CASO", nullable = false, insertable = false, updatable = false) })
    private Tsca016Cliente Tsca056SprCtPm.tsca016Cliente1;
    
    @Column(name = "FH_INFORMACION")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date Tsca056SprCtPm.fhInformacion;
    
    public Tsca016Cliente Tsca056SprCtPm.getTsca016Cliente() {
        return this.tsca016Cliente;
    }
    
    public void Tsca056SprCtPm.setTsca016Cliente(Tsca016Cliente tsca016Cliente) {
        this.tsca016Cliente = tsca016Cliente;
    }
    
    public Tsca016Cliente Tsca056SprCtPm.getTsca016Cliente1() {
        return this.tsca016Cliente1;
    }
    
    public void Tsca056SprCtPm.setTsca016Cliente1(Tsca016Cliente tsca016Cliente1) {
        this.tsca016Cliente1 = tsca016Cliente1;
    }
    
    public Date Tsca056SprCtPm.getFhInformacion() {
        return this.fhInformacion;
    }
    
    public void Tsca056SprCtPm.setFhInformacion(Date fhInformacion) {
        this.fhInformacion = fhInformacion;
    }
    
}
