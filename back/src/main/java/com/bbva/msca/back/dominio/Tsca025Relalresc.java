package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(identifierType = Tsca025RelalrescPK.class, versionField = "", table = "TSCA025_RELALRESC", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca025Relalresc {
}
