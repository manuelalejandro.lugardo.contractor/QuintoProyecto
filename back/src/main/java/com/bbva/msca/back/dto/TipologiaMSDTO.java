package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaMSDTO {

	@AnotacionAlerta(tipologia = "ms", idCampo = "cdPeriodo", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_CDPERIODO")
	private BigDecimal cdPeriodo;
	@AnotacionAlerta(tipologia = "ms", idCampo = "cdCte", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_CDCTE")
	private BigDecimal cdCte;
	@AnotacionAlerta(tipologia = "ms", idCampo = "cdAccion", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_CDACCION", instanceType = "1")
	private String cdAccion;
	@AnotacionAlerta(tipologia = "ms", idCampo = "nbCte", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")
	private String nbCte;
	@AnotacionAlerta(tipologia = "ms", idCampo = "tpPerJuri", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_TPPERJURI", instanceType = "1")
	private String tpPerJuri;
	@AnotacionAlerta(tipologia = "ms", idCampo = "cdOficinaGest", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_CDOFICINAGEST", instanceType = "1")
	private String cdOficinaGest;
	@AnotacionAlerta(tipologia = "ms", idCampo = "nbBanca", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_NBBANCA", instanceType = "1")
	private String nbBanca;
	@AnotacionAlerta(tipologia = "ms", idCampo = "cdMatch", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_CDMATCH", instanceType = "1")
	private String cdMatch;
	@AnotacionAlerta(tipologia = "ms", idCampo = "imScore", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_IMSCORE")
	private BigDecimal imScore;
	@AnotacionAlerta(tipologia = "ms", idCampo = "ctMovimiento", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_CTMOVIMIENTO", instanceType = "1")
	private String ctMovimiento;
	@AnotacionAlerta(tipologia = "ms", idCampo = "cdDirecta", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_CDDIRECTA", instanceType = "1")
	private String cdDirecta;
	@AnotacionAlerta(tipologia = "ms", idCampo = "nuScoreCorte", posicion = "12", nbEtiqueta = "MSCK_LBL_01002_NUSCORECORTE")
	private BigDecimal nuScoreCorte;
	@AnotacionAlerta(tipologia = "ms", idCampo = "nbDescripcion", posicion = "13", nbEtiqueta = "MSCK_LBL_01002_NBDESCRIPCION", instanceType = "1")
	private String nbDescripcion;
	@AnotacionAlerta(tipologia = "ms", idCampo = "fhGen", posicion = "14", nbEtiqueta = "MSCK_LBL_01002_FHGEN", instanceType = "2")
	private Date fhGen;
	@AnotacionAlerta(tipologia = "ms", idCampo = "imImporteDep", posicion = "15", nbEtiqueta = "MSCK_LBL_01002_IMIMPORTEDEP")
	private BigDecimal imImporteDep;
	@AnotacionAlerta(tipologia = "ms", idCampo = "tpInstMonetario", posicion = "16", nbEtiqueta = "MSCK_LBL_01002_TPINTSMONETARIO", instanceType = "1")
	private String tpInstMonetario;
	
	public BigDecimal getCdPeriodo() {
		return cdPeriodo;
	}
	public void setCdPeriodo(BigDecimal cdPeriodo) {
		this.cdPeriodo = cdPeriodo;
	}
	public BigDecimal getCdCte() {
		return cdCte;
	}
	public void setCdCte(BigDecimal cdCte) {
		this.cdCte = cdCte;
	}
	public String getCdAccion() {
		return cdAccion;
	}
	public void setCdAccion(String cdAccion) {
		this.cdAccion = cdAccion;
	}
	public String getNbCte() {
		return nbCte;
	}
	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}
	public String getTpPerJuri() {
		return tpPerJuri;
	}
	public void setTpPerJuri(String tpPerJuri) {
		this.tpPerJuri = tpPerJuri;
	}
	public String getCdOficinaGest() {
		return cdOficinaGest;
	}
	public void setCdOficinaGest(String cdOficinaGest) {
		this.cdOficinaGest = cdOficinaGest;
	}
	public String getNbBanca() {
		return nbBanca;
	}
	public void setNbBanca(String nbBanca) {
		this.nbBanca = nbBanca;
	}
	public String getCdMatch() {
		return cdMatch;
	}
	public void setCdMatch(String cdMatch) {
		this.cdMatch = cdMatch;
	}
	public BigDecimal getImScore() {
		return imScore;
	}
	public void setImScore(BigDecimal imScore) {
		this.imScore = imScore;
	}
	public String getCtMovimiento() {
		return ctMovimiento;
	}
	public void setCtMovimiento(String ctMovimiento) {
		this.ctMovimiento = ctMovimiento;
	}
	public String getCdDirecta() {
		return cdDirecta;
	}
	public void setCdDirecta(String cdDirecta) {
		this.cdDirecta = cdDirecta;
	}
	public BigDecimal getNuScoreCorte() {
		return nuScoreCorte;
	}
	public void setNuScoreCorte(BigDecimal nuScoreCorte) {
		this.nuScoreCorte = nuScoreCorte;
	}
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	public Date getFhGen() {
		return fhGen;
	}
	public void setFhGen(Date fhGen) {
		this.fhGen = fhGen;
	}
	public BigDecimal getImImporteDep() {
		return imImporteDep;
	}
	public void setImImporteDep(BigDecimal imImporteDep) {
		this.imImporteDep = imImporteDep;
	}
	public String getTpInstMonetario() {
		return tpInstMonetario;
	}
	public void setTpInstMonetario(String tpInstMonetario) {
		this.tpInstMonetario = tpInstMonetario;
	}
}
