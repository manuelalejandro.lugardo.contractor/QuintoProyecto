package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class BusquedaBloqueoCuentasDTO {
	
	private BigDecimal cdCaso;
	private String nuFolioAlerta;
	private String cdCliente;//Char
	private String nbCliente;
	private String nuCuenta;
	private String cdTipologia; //Char
	private String cdSector; //Char
	private String cdImporte; //Char
	private BigDecimal imImporte;
	private Date fhAlertaDel;
	private Date fhAlertaAl;
	
	
	public BigDecimal getCdCaso() {
		return cdCaso;
	}
	public void setCdCaso(BigDecimal cdCaso) {
		this.cdCaso = cdCaso;
	}
	public String getNuFolioAlerta() {
		return nuFolioAlerta;
	}
	public void setNuFolioAlerta(String nuFolioAlerta) {
		this.nuFolioAlerta = nuFolioAlerta;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public String getCdTipologia() {
		return cdTipologia;
	}
	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	public String getCdSector() {
		return cdSector;
	}
	public void setCdSector(String cdSector) {
		this.cdSector = cdSector;
	}
	public String getCdImporte() {
		return cdImporte;
	}
	public void setCdImporte(String cdImporte) {
		this.cdImporte = cdImporte;
	}
	public BigDecimal getImImporte() {
		return imImporte;
	}
	public void setImImporte(BigDecimal imImporte) {
		this.imImporte = imImporte;
	}
	public Date getFhAlertaDel() {
		return fhAlertaDel;
	}
	public void setFhAlertaDel(Date fhAlertaDel) {
		this.fhAlertaDel = fhAlertaDel;
	}
	public Date getFhAlertaAl() {
		return fhAlertaAl;
	}
	public void setFhAlertaAl(Date fhAlertaAl) {
		this.fhAlertaAl = fhAlertaAl;
	}
}
