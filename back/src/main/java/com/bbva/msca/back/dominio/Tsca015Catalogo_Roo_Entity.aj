// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca015Catalogo;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca015Catalogo_Roo_Entity {
    
    declare @type: Tsca015Catalogo: @Entity;
    
    declare @type: Tsca015Catalogo: @Table(name = "TSCA015_CATALOGO", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca015Catalogo.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CD_CATALOGO")
    private BigDecimal Tsca015Catalogo.cdCatalogo;
    
    public BigDecimal Tsca015Catalogo.getCdCatalogo() {
        return this.cdCatalogo;
    }
    
    public void Tsca015Catalogo.setCdCatalogo(BigDecimal id) {
        this.cdCatalogo = id;
    }
    
    @Transactional
    public void Tsca015Catalogo.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca015Catalogo.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca015Catalogo attached = Tsca015Catalogo.findTsca015Catalogo(this.cdCatalogo);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca015Catalogo.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca015Catalogo.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca015Catalogo Tsca015Catalogo.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca015Catalogo merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca015Catalogo.entityManager() {
        EntityManager em = new Tsca015Catalogo().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca015Catalogo.countTsca015Catalogoes() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca015Catalogo o", Long.class).getSingleResult();
    }
    
    public static List<Tsca015Catalogo> Tsca015Catalogo.findAllTsca015Catalogoes() {
        return entityManager().createQuery("SELECT o FROM Tsca015Catalogo o", Tsca015Catalogo.class).getResultList();
    }
    
    public static Tsca015Catalogo Tsca015Catalogo.findTsca015Catalogo(BigDecimal cdCatalogo) {
        if (cdCatalogo == null) return null;
        return entityManager().find(Tsca015Catalogo.class, cdCatalogo);
    }
    
    public static List<Tsca015Catalogo> Tsca015Catalogo.findTsca015CatalogoEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca015Catalogo o", Tsca015Catalogo.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
