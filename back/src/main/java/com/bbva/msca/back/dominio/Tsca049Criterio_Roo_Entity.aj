// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca049Criterio;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca049Criterio_Roo_Entity {
    
    declare @type: Tsca049Criterio: @Entity;
    
    declare @type: Tsca049Criterio: @Table(name = "TSCA049_CRITERIO", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca049Criterio.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CD_CRITERIO")
    private BigDecimal Tsca049Criterio.cdCriterio;
    
    public BigDecimal Tsca049Criterio.getCdCriterio() {
        return this.cdCriterio;
    }
    
    public void Tsca049Criterio.setCdCriterio(BigDecimal id) {
        this.cdCriterio = id;
    }
    
    @Transactional
    public void Tsca049Criterio.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca049Criterio.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca049Criterio attached = Tsca049Criterio.findTsca049Criterio(this.cdCriterio);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca049Criterio.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca049Criterio.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca049Criterio Tsca049Criterio.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca049Criterio merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca049Criterio.entityManager() {
        EntityManager em = new Tsca049Criterio().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca049Criterio.countTsca049Criterios() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca049Criterio o", Long.class).getSingleResult();
    }
    
    public static List<Tsca049Criterio> Tsca049Criterio.findAllTsca049Criterios() {
        return entityManager().createQuery("SELECT o FROM Tsca049Criterio o", Tsca049Criterio.class).getResultList();
    }
    
    public static Tsca049Criterio Tsca049Criterio.findTsca049Criterio(BigDecimal cdCriterio) {
        if (cdCriterio == null) return null;
        return entityManager().find(Tsca049Criterio.class, cdCriterio);
    }
    
    public static List<Tsca049Criterio> Tsca049Criterio.findTsca049CriterioEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca049Criterio o", Tsca049Criterio.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
