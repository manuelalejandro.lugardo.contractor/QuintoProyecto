package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.bbva.msca.back.dto.DetalleTransferenciasInternacionalesDTO;

public class DetalleTransferenciasInternacionalesDAO {
	
	public List<DetalleTransferenciasInternacionalesDTO> getDetalleTransferenciasInternacionales(){
		List<DetalleTransferenciasInternacionalesDTO> lstDetalleTransferenciasInternacionalesDTO = new ArrayList<DetalleTransferenciasInternacionalesDTO>();
		DetalleTransferenciasInternacionalesDTO detalleTransferenciasInternacionalesDTO = null;
		
		for(int i=0;i<10;i++){
			detalleTransferenciasInternacionalesDTO = new DetalleTransferenciasInternacionalesDTO();
			
			detalleTransferenciasInternacionalesDTO.setNbReferencia("");
			detalleTransferenciasInternacionalesDTO.setNuCuentas(new BigDecimal(85));
			detalleTransferenciasInternacionalesDTO.setFhOperacion(new Date());
			detalleTransferenciasInternacionalesDTO.setNbTipoTransaccion("");
			detalleTransferenciasInternacionalesDTO.setNbCorresponsal("");
			detalleTransferenciasInternacionalesDTO.setNbEntidadOrigen("");
			detalleTransferenciasInternacionalesDTO.setNbEntidadDestino("");
			detalleTransferenciasInternacionalesDTO.setNbDatoDestino("");
			detalleTransferenciasInternacionalesDTO.setNbDatoOrdenante("");
			detalleTransferenciasInternacionalesDTO.setNbOperacionOrigen("");
			detalleTransferenciasInternacionalesDTO.setNbOperacionDestino("");
			detalleTransferenciasInternacionalesDTO.setNbMoneda("");
			detalleTransferenciasInternacionalesDTO.setImInstrumento(new BigDecimal(85));
			detalleTransferenciasInternacionalesDTO.setImDolarizado(new BigDecimal(85));
			detalleTransferenciasInternacionalesDTO.setNbPaisFIS("");
			detalleTransferenciasInternacionalesDTO.setNbRazonSocial("");
			
			lstDetalleTransferenciasInternacionalesDTO.add(detalleTransferenciasInternacionalesDTO);
			
		}

		return lstDetalleTransferenciasInternacionalesDTO;
	}

}
