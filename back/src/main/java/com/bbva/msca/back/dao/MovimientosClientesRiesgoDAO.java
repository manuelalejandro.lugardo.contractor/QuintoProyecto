package com.bbva.msca.back.dao;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import com.bbva.msca.back.dto.MovimientosClientesRiesgoDTO;



import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.TimestampType;
import org.springframework.stereotype.Component;

import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;


@Component
public class MovimientosClientesRiesgoDAO {

    BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
@SuppressWarnings("unchecked")
public List<MovimientosClientesRiesgoDTO> getMovimientosClientesRiesgo(MovimientosClientesRiesgoDTO movimientosClientesRiesgoDTO,String nuCuenta) throws Exception{
		
		List<MovimientosClientesRiesgoDTO> lstMovimientosClientesRiesgoDTO = null;
		 String consulta = Consultas.getConsulta("getConsultaMovimientosClienteRiesgo");
		 String filtro = "";
		 String condicion = " WHERE ";
		 boolean isFiltro = false;
		 SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
		 if( movimientosClientesRiesgoDTO != null){
			 
			 if(movimientosClientesRiesgoDTO.getFhMovimientoDel() != null || movimientosClientesRiesgoDTO.getFhMovimientoAl() != null ){
				 if(movimientosClientesRiesgoDTO.getFhMovimientoDel() != null && movimientosClientesRiesgoDTO.getFhMovimientoAl() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhMovimiento BETWEEN " + "TO_DATE('"  + formato.format(movimientosClientesRiesgoDTO.getFhMovimientoDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(movimientosClientesRiesgoDTO.getFhMovimientoAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND CP.fhMovimiento BETWEEN " + "TO_DATE('"  + formato.format(movimientosClientesRiesgoDTO.getFhMovimientoDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(movimientosClientesRiesgoDTO.getFhMovimientoAl()) +"','DD/MM/YYYY') ";
					 }
					
				 }else{
					 if(movimientosClientesRiesgoDTO.getFhMovimientoDel() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhMovimiento = TO_DATE ('"  + formato.format(movimientosClientesRiesgoDTO.getFhMovimientoDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(movimientosClientesRiesgoDTO.getFhMovimientoAl() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhMovimiento = TO_DATE ('"  + formato.format(movimientosClientesRiesgoDTO.getFhMovimientoAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND CP.fhMovimiento = TO_DATE ('"  + formato.format(movimientosClientesRiesgoDTO.getFhMovimientoAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(movimientosClientesRiesgoDTO.getImMovimientoDesde() != null || movimientosClientesRiesgoDTO.getImMovimientoHasta() != null ){
				 if(movimientosClientesRiesgoDTO.getImMovimientoDesde() != null && movimientosClientesRiesgoDTO.getImMovimientoHasta() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.imImporte BETWEEN " + movimientosClientesRiesgoDTO.getImMovimientoDesde() + "AND " + movimientosClientesRiesgoDTO.getImMovimientoHasta();
					 }else{
						 filtro = filtro + " AND CP.imImporte BETWEEN "  + movimientosClientesRiesgoDTO.getImMovimientoDesde() + "AND "  + movimientosClientesRiesgoDTO.getImMovimientoHasta();
					 }
					
				 }else{
					 if(movimientosClientesRiesgoDTO.getImMovimientoDesde() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.imImporte = '"  + movimientosClientesRiesgoDTO.getImMovimientoDesde()+ "'";
						 }else{
							 filtro = filtro +  " AND CP.imImporte =  '"  + movimientosClientesRiesgoDTO.getImMovimientoDesde()+"'";
					 
					     }
					 }
					 if(movimientosClientesRiesgoDTO.getImMovimientoHasta() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.imImporte = "  + movimientosClientesRiesgoDTO.getImMovimientoHasta();
						 }else{
							 filtro = filtro +  " AND CP.imImporte = "  + movimientosClientesRiesgoDTO.getImMovimientoHasta();
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(isFiltro){
				consulta = consulta+condicion+filtro;
			 }
			
		 }
		 Query query = em.createNativeQuery(consulta);
		 query.setParameter(1,nuCuenta);
		 
		
		 query.unwrap(org.hibernate.SQLQuery.class)
		     .addScalar("fhMovimiento",TimestampType.INSTANCE)
			 .addScalar("fhValor",TimestampType.INSTANCE)
			 .addScalar("nbDescripcion")
		     .addScalar("nbReferencia")
		     .addScalar("imImporte")
		     .addScalar("nuMovimiento")
		     .addScalar("nuCuenta")
		     .setResultTransformer(Transformers.aliasToBean(MovimientosClientesRiesgoDTO.class));
				
		 lstMovimientosClientesRiesgoDTO = (List<MovimientosClientesRiesgoDTO>)query.getResultList();
			
		
			return lstMovimientosClientesRiesgoDTO;
		}
	
@SuppressWarnings("unchecked")	
public List<MovimientosClientesRiesgoDTO> getMovimientosClientesRiesgo(String nuCuenta) throws Exception{
		
	 List<MovimientosClientesRiesgoDTO> lstMovimientosClientesRiesgoDTO = null;
	 Query query = em.createNativeQuery(Consultas.getConsulta("getConsultaMovimientosClienteRiesgo"));
	 query.setParameter(1,nuCuenta);
	 
	 query.unwrap(org.hibernate.SQLQuery.class)
		 .addScalar("fhMovimiento",TimestampType.INSTANCE)
		 .addScalar("fhValor",TimestampType.INSTANCE)
		 .addScalar("nbDescripcion")
	     .addScalar("nbReferencia")
	     .addScalar("imImporte")
	     .addScalar("nuMovimiento")
	     .addScalar("nuCuenta")
         .setResultTransformer(Transformers.aliasToBean(MovimientosClientesRiesgoDTO.class));
	 
	    lstMovimientosClientesRiesgoDTO = (List<MovimientosClientesRiesgoDTO>)query.getResultList();
		
	    return lstMovimientosClientesRiesgoDTO;
		
	}


}
