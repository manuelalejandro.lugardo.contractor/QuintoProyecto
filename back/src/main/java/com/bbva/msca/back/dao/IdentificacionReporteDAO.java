package com.bbva.msca.back.dao;

import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;
import java.util.ArrayList;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.bbva.msca.back.dto.IdentificacionReporteDTO;

@SuppressWarnings("unused")
public class IdentificacionReporteDAO {
	

	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();
	
	@SuppressWarnings({ "unchecked"})
	public  IdentificacionReporteDTO getIdentificacionReporte(String nuFolioAlerta, String cdSistema) throws Exception{
		IdentificacionReporteDTO identificacionReporteDTO = new IdentificacionReporteDTO();
			 
			 Query query = em.createNativeQuery(Consultas.getConsulta("getIdentificacionReporte"));
			 query.setParameter(1,nuFolioAlerta);
			 query.setParameter(2,cdSistema);

			 query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("nuFolioAlertaIR")
				.addScalar("nbSistemaIR")
				.addScalar("nuCuentaIR")
				.addScalar("montoIR")
				.addScalar("divisaIR", StringType.INSTANCE)
				.addScalar("fhAlertaIR", TimestampType.INSTANCE)
				.addScalar("fhEnvioIR", TimestampType.INSTANCE)
				.addScalar("cdSesionIR")
					//oficina que gestiona
					.addScalar("nbOficinaGestoraIR")
					.addScalar("nbRiesgoIR")
					.addScalar("nbMercadoIR", StringType.INSTANCE)
					.addScalar("nbDivisionIR", StringType.INSTANCE)
					.addScalar("nbFuncionarioIR", StringType.INSTANCE)
				
				
			.setResultTransformer(Transformers.aliasToBean(IdentificacionReporteDTO.class));
			 
			 DecimalFormat formateador = new DecimalFormat("$"+"###,###,###,###.##");
			 
			 List<IdentificacionReporteDTO> lstIdentificacionReporte = (List<IdentificacionReporteDTO>)query.getResultList();
			 
			 if(!lstIdentificacionReporte.isEmpty()){
				 identificacionReporteDTO = lstIdentificacionReporte.get( 0 );
				 
				 if(identificacionReporteDTO.getDivisaIR() != null){
				
					 if(identificacionReporteDTO.getMontoIR() != null){
						 StringBuilder montoDivisaIR = new StringBuilder();
						 StringBuilder montoIR = new StringBuilder();
						 StringBuilder divisaIR = new StringBuilder();
						 
						 montoIR.append(formateador.format(identificacionReporteDTO.getMontoIR()).toString());
						 divisaIR.append(identificacionReporteDTO.getDivisaIR().toString());
						 
						montoDivisaIR.append( montoIR.toString() ).append(" ").append(divisaIR.toString());
						identificacionReporteDTO.setMontoDivisaIR(montoDivisaIR.toString());
					 }else{
						 identificacionReporteDTO.setMontoDivisaIR(null);
					 }
				 }else{
					 identificacionReporteDTO.setDivisaIR("");
				 }
				
				
			 }
			 
		return identificacionReporteDTO;
	}

	
	
	@SuppressWarnings({ "unchecked"})
	public  IdentificacionReporteDTO getIdentificacionReporte2(String nuFolioAlerta, String cdSistema) throws Exception{
		IdentificacionReporteDTO identificacionReporteDTO = new IdentificacionReporteDTO();
			 
			 Query query = em.createNativeQuery(Consultas.getConsulta("getIdentificacionReporte2"));
			 query.setParameter(1,nuFolioAlerta);
			 query.setParameter(2,cdSistema);

			 query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("nuFolioAlertaIR2")
				.addScalar("nbSistemaIR2")
				.addScalar("nuCuentaIR2")
				.addScalar("montoIR2")
				.addScalar("divisaIR2", StringType.INSTANCE)
				.addScalar("fhAlertaIR2", TimestampType.INSTANCE)
				.addScalar("fhEnvioIR2", TimestampType.INSTANCE)
				.addScalar("cdSesionIR2")
					//oficina que gestiona
					.addScalar("nbOficinaGestoraIR2")
					.addScalar("nbRiesgoIR2")
					.addScalar("nbMercadoIR2", StringType.INSTANCE)
					.addScalar("nbDivisionIR2", StringType.INSTANCE)
					.addScalar("nbFuncionarioIR2", StringType.INSTANCE)
				
				
			.setResultTransformer(Transformers.aliasToBean(IdentificacionReporteDTO.class));
			 
			
			 
			 List<IdentificacionReporteDTO> lstIdentificacionReporte = (List<IdentificacionReporteDTO>)query.getResultList();
			 
			 if(!lstIdentificacionReporte.isEmpty()){
				 identificacionReporteDTO = lstIdentificacionReporte.get( 0 );
				 
				 StringBuilder montoDivisaIR = new StringBuilder();
				 StringBuilder montoIR = new StringBuilder();
				 StringBuilder divisaIR = new StringBuilder();
				 
				 if(identificacionReporteDTO.getMontoIR2() != null){
				 montoIR.append(identificacionReporteDTO.getMontoIR2().toString());
				 divisaIR.append(identificacionReporteDTO.getDivisaIR2().toString());
				 
				montoDivisaIR.append( montoIR.toString() ).append(" ").append(divisaIR.toString());
				identificacionReporteDTO.setMontoDivisaIR2(montoDivisaIR.toString());
				 }else{
					 identificacionReporteDTO.setMontoDivisaIR2("");
					 identificacionReporteDTO.setMontoDivisaIR2("");
					 identificacionReporteDTO.setMontoDivisaIR2("");
				 }
				 
			 }
			 
		return identificacionReporteDTO;
	}
	
	
	
	
	
	
	
	
	
	
	
}