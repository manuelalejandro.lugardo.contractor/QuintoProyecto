package com.bbva.msca.back.dto;

import java.math.BigDecimal;


public class ConocimientoClienteDTO {

	private	String	nbRegSimplificado;
	private	String	nbActEconomica;
	private	String	nbOcupacion;
	private	String	nbPuestoPEP;
	private	String	nbPEP;
	private	String	nbParentescoPEP;
	private	String	nbPersRel;
	private	String	nbPaisResidencia;
	private	String	nbNacionalidad; 
	private	String	nbJusAperCta;
	private	String	nbProRecursos;
	private	String	nbPriFueIngresos;
	private	String	nbIndRiesgo;
	private	String	nbRieActividad;
	private	BigDecimal	nuTipCliente;
	private	BigDecimal	nuActEconomica;
	private	BigDecimal	nuOcupacion;
	private	BigDecimal	nuRegPais;
	private	BigDecimal	nuProServicios;
	private	BigDecimal	nuOpeHabitual;
	private	String nbJusOpePesos;
	private	String nbJusOpeDolares; 
	private	String imMonTotDepositos;
	private	String nbLocOperar;
	private	String fhCamAltBajRiesgo;
	private	String fhAltAltRiesgo;
	private	String fhUltMovAltBaj; 
	private	String nbOrigenBaja;
	private	String nbOrigenAlta;
	private	BigDecimal	nuIntervinientes;
	private	BigDecimal	nuSucursal;
	private	BigDecimal	nuCanCaptacion;
	private	BigDecimal	nuAntiguedadPuntaje;
	private	BigDecimal	nuEdad;
	private	BigDecimal	nuTotal; 
	private	String	nbMotRiesgo;
	private	String txObjetoSocial;
	private	String imInstrumento;
	private	String cdEntFedOrigen;
	private	String cdEntFedDestino;
	private	String imTransInt;
	private	String cdEntFedIntOri;
	private	String cdEntFedIntDes;
	private	String imTransNacional;
	private	String cdEntFedNalOri;
	private	String cdEntFedNalDes;
	private	String imEfectivo;

	private	String paisRecidenciaCAR;
	private	String nacionalidadCAR;
	private	String justCuentaCAR;
	private	String procedenciaRecCAR;
	private	String fuenteIngresoCAR;
	private	String indRiesgoCAR;
	private	String indRiesgoActCAR;
	private	String fhCamAltBajRiesgoCAR;
	private	String fhAltAltRiesgoCAR;
	private	String fhUltMovAltBajCAR;
	private	String nbOrigenBajaCAR;
	private	String nbOrigenAltaCAR;
	
	private	String nuTipClienteRBA;
	private	String nuActEconomicaRBA;
	private	String nuOcupacionRBA;
	private	String nuRegPaisRBA;
	private	String nuProServiciosRBA;
	private	String nuOpeHabitualRBA;
	private	String nuIntervinientesRBA;
	private	String nuSucursalRBA;
	private	String nuCanCaptacionRBA;
	private	String nuAntiguedadPuntajeRBA;
	private	String nuEdadRBA;
	private	String nuTotalRBA;
	private	String nbMotRiesgoRBA;
	
	
	
	
	public String getCdEntFedDestino() {
		return cdEntFedDestino;
	}
	public void setCdEntFedDestino(String cdEntFedDestino) {
		this.cdEntFedDestino = cdEntFedDestino;
	}
	public String getCdEntFedOrigen() {
		return cdEntFedOrigen;
	}
	public void setCdEntFedOrigen(String cdEntFedOrigen) {
		this.cdEntFedOrigen = cdEntFedOrigen;
	}
	public String getImTransInt() {
		return imTransInt;
	}
	public void setImTransInt(String imTransInt) {
		this.imTransInt = imTransInt;
	}
	public String getCdEntFedIntOri() {
		return cdEntFedIntOri;
	}
	public void setCdEntFedIntOri(String cdEntFedIntOri) {
		this.cdEntFedIntOri = cdEntFedIntOri;
	}
	public String getCdEntFedIntDes() {
		return cdEntFedIntDes;
	}
	public void setCdEntFedIntDes(String cdEntFedIntDes) {
		this.cdEntFedIntDes = cdEntFedIntDes;
	}
	public String getImTransNacional() {
		return imTransNacional;
	}
	public void setImTransNacional(String imTransNacional) {
		this.imTransNacional = imTransNacional;
	}
	public String getCdEntFedNalOri() {
		return cdEntFedNalOri;
	}
	public void setCdEntFedNalOri(String cdEntFedNalOri) {
		this.cdEntFedNalOri = cdEntFedNalOri;
	}
	public String getCdEntFedNalDes() {
		return cdEntFedNalDes;
	}
	public void setCdEntFedNalDes(String cdEntFedNalDes) {
		this.cdEntFedNalDes = cdEntFedNalDes;
	}
	public String getImEfectivo() {
		return imEfectivo;
	}
	public void setImEfectivo(String imEfectivo) {
		this.imEfectivo = imEfectivo;
	}
	public String getImInstrumento() {
		return imInstrumento;
	}
	public void setImInstrumento(String imInstrumento) {
		this.imInstrumento = imInstrumento;
	}
	public String getTxObjetoSocial() {
		return txObjetoSocial;
	}
	public void setTxObjetoSocial(String txObjetoSocial) {
		this.txObjetoSocial = txObjetoSocial;
	}
	public String getNbRegSimplificado() {
		return nbRegSimplificado;
	}
	public void setNbRegSimplificado(String nbRegSimplificado) {
		this.nbRegSimplificado = nbRegSimplificado;
	}
	public String getNbActEconomica() {
		return nbActEconomica;
	}
	public void setNbActEconomica(String nbActEconomica) {
		this.nbActEconomica = nbActEconomica;
	}
	public String getNbOcupacion() {
		return nbOcupacion;
	}
	public void setNbOcupacion(String nbOcupacion) {
		this.nbOcupacion = nbOcupacion;
	}
	public String getNbPuestoPEP() {
		return nbPuestoPEP;
	}
	public void setNbPuestoPEP(String nbPuestoPEP) {
		this.nbPuestoPEP = nbPuestoPEP;
	}
	public String getNbPEP() {
		return nbPEP;
	}
	public void setNbPEP(String nbPEP) {
		this.nbPEP = nbPEP;
	}
	public String getNbParentescoPEP() {
		return nbParentescoPEP;
	}
	public void setNbParentescoPEP(String nbParentescoPEP) {
		this.nbParentescoPEP = nbParentescoPEP;
	}
	public String getNbPersRel() {
		return nbPersRel;
	}
	public void setNbPersRel(String nbPersRel) {
		this.nbPersRel = nbPersRel;
	}
	public String getNbPaisResidencia() {
		return nbPaisResidencia;
	}
	public void setNbPaisResidencia(String nbPaisResidencia) {
		this.nbPaisResidencia = nbPaisResidencia;
	}
	public String getNbNacionalidad() {
		return nbNacionalidad;
	}
	public void setNbNacionalidad(String nbNacionalidad) {
		this.nbNacionalidad = nbNacionalidad;
	}
	public String getNbJusAperCta() {
		return nbJusAperCta;
	}
	public void setNbJusAperCta(String nbJusAperCta) {
		this.nbJusAperCta = nbJusAperCta;
	}
	public String getNbProRecursos() {
		return nbProRecursos;
	}
	public void setNbProRecursos(String nbProRecursos) {
		this.nbProRecursos = nbProRecursos;




	}
	public String getNbPriFueIngresos() {
		return nbPriFueIngresos;

	}
	public void setNbPriFueIngresos(String nbPriFueIngresos) {
		this.nbPriFueIngresos = nbPriFueIngresos;


	}
	public String getNbIndRiesgo() {
		return nbIndRiesgo;

	}
	public void setNbIndRiesgo(String nbIndRiesgo) {
		this.nbIndRiesgo = nbIndRiesgo;


	}
	public String getNbRieActividad() {
		return nbRieActividad;

	}
	public void setNbRieActividad(String nbRieActividad) {
		this.nbRieActividad = nbRieActividad;


	}
	public BigDecimal getNuTipCliente() {
		return nuTipCliente;


	}
	public void setNuTipCliente(BigDecimal nuTipCliente) {
		this.nuTipCliente = nuTipCliente;


	}
	public BigDecimal getNuActEconomica() {
		return nuActEconomica;


	}
	public void setNuActEconomica(BigDecimal nuActEconomica) {
		this.nuActEconomica = nuActEconomica;


	}
	public BigDecimal getNuOcupacion() {
		return nuOcupacion;


	}
	public void setNuOcupacion(BigDecimal nuOcupacion) {
		this.nuOcupacion = nuOcupacion;


	}
	public BigDecimal getNuRegPais() {
		return nuRegPais;


	}
	public void setNuRegPais(BigDecimal nuRegPais) {
		this.nuRegPais = nuRegPais;


	}
	public BigDecimal getNuProServicios() {
		return nuProServicios;


	}
	public void setNuProServicios(BigDecimal nuProServicios) {
		this.nuProServicios = nuProServicios;


	}
	public BigDecimal getNuOpeHabitual() {
		return nuOpeHabitual;


	}
	public void setNuOpeHabitual(BigDecimal nuOpeHabitual) {
		this.nuOpeHabitual = nuOpeHabitual;


	}
	public BigDecimal getNuIntervinientes() {
		return nuIntervinientes;


	}
	public void setNuIntervinientes(BigDecimal nuIntervinientes) {
		this.nuIntervinientes = nuIntervinientes;


	}
	public BigDecimal getNuSucursal() {
		return nuSucursal;


	}
	public void setNuSucursal(BigDecimal nuSucursal) {
		this.nuSucursal = nuSucursal;


	}
	public BigDecimal getNuCanCaptacion() {
		return nuCanCaptacion;


	}
	public void setNuCanCaptacion(BigDecimal nuCanCaptacion) {
		this.nuCanCaptacion = nuCanCaptacion;


	}
	public BigDecimal getNuAntiguedadPuntaje() {
		return nuAntiguedadPuntaje;


	}
	public void setNuAntiguedadPuntaje(BigDecimal nuAntiguedadPuntaje) {
		this.nuAntiguedadPuntaje = nuAntiguedadPuntaje;


	}
	public BigDecimal getNuEdad() {
		return nuEdad;


	}
	public void setNuEdad(BigDecimal nuEdad) {
		this.nuEdad = nuEdad;


	}
	public BigDecimal getNuTotal() {
		return nuTotal;


	}
	public void setNuTotal(BigDecimal nuTotal) {
		this.nuTotal = nuTotal;


	}
	public String getNbMotRiesgo() {
		return nbMotRiesgo;

	}
	public void setNbMotRiesgo(String nbMotRiesgo) {
		this.nbMotRiesgo = nbMotRiesgo;


	}
 
	public String getNbJusOpePesos() {
		return nbJusOpePesos;

	}
	public void setNbJusOpePesos(String nbJusOpePesos) {
		this.nbJusOpePesos = nbJusOpePesos;


	}
	public String getNbJusOpeDolares() {
		return nbJusOpeDolares;

	}
	public void setNbJusOpeDolares(String nbJusOpeDolares) {
		this.nbJusOpeDolares = nbJusOpeDolares;


	}
	public String getImMonTotDepositos() {
		return imMonTotDepositos;


	}
	public void setImMonTotDepositos(String imMonTotDepositos) {
		this.imMonTotDepositos = imMonTotDepositos;


	}
	public String getNbLocOperar() {
		return nbLocOperar;

	}
	public void setNbLocOperar(String nbLocOperar) {
		this.nbLocOperar = nbLocOperar;


	}
	public String getFhCamAltBajRiesgo() {
		return fhCamAltBajRiesgo;


	}
	public void setFhCamAltBajRiesgo(String fhCamAltBajRiesgo) {
		this.fhCamAltBajRiesgo = fhCamAltBajRiesgo;


	}
	public String getFhAltAltRiesgo() {
		return fhAltAltRiesgo;

	}
	public void setFhAltAltRiesgo(String fhAltAltRiesgo) {
		this.fhAltAltRiesgo = fhAltAltRiesgo;


	}
	public String getFhUltMovAltBaj() {
		return fhUltMovAltBaj;

	}
	public void setFhUltMovAltBaj(String fhUltMovAltBaj) {
		this.fhUltMovAltBaj = fhUltMovAltBaj;


	}
	public String getNbOrigenBaja() {
		return nbOrigenBaja;

	}
	public void setNbOrigenBaja(String nbOrigenBaja) {
		this.nbOrigenBaja = nbOrigenBaja;


	}
	public String getNbOrigenAlta() {
		return nbOrigenAlta;

	}
	public void setNbOrigenAlta(String nbOrigenAlta) {
		this.nbOrigenAlta = nbOrigenAlta;
	}
	public String getPaisRecidenciaCAR() {
		return paisRecidenciaCAR;
	}
	public void setPaisRecidenciaCAR(String paisRecidenciaCAR) {
		this.paisRecidenciaCAR = paisRecidenciaCAR;
	}
	public String getNacionalidadCAR() {
		return nacionalidadCAR;
	}
	public void setNacionalidadCAR(String nacionalidadCAR) {
		this.nacionalidadCAR = nacionalidadCAR;
	}
	public String getJustCuentaCAR() {
		return justCuentaCAR;
	}
	public void setJustCuentaCAR(String justCuentaCAR) {
		this.justCuentaCAR = justCuentaCAR;
	}
	public String getProcedenciaRecCAR() {
		return procedenciaRecCAR;
	}
	public void setProcedenciaRecCAR(String procedenciaRecCAR) {
		this.procedenciaRecCAR = procedenciaRecCAR;
	}
	public String getFuenteIngresoCAR() {
		return fuenteIngresoCAR;
	}
	public void setFuenteIngresoCAR(String fuenteIngresoCAR) {
		this.fuenteIngresoCAR = fuenteIngresoCAR;
	}
	public String getIndRiesgoCAR() {
		return indRiesgoCAR;
	}
	public void setIndRiesgoCAR(String indRiesgoCAR) {
		this.indRiesgoCAR = indRiesgoCAR;
	}
	public String getIndRiesgoActCAR() {
		return indRiesgoActCAR;
	}
	public void setIndRiesgoActCAR(String indRiesgoActCAR) {
		this.indRiesgoActCAR = indRiesgoActCAR;
	}
	public String getNuTipClienteRBA() {
		return nuTipClienteRBA;
	}
	public void setNuTipClienteRBA(String nuTipClienteRBA) {
		this.nuTipClienteRBA = nuTipClienteRBA;
	}
	public String getNuActEconomicaRBA() {
		return nuActEconomicaRBA;
	}
	public void setNuActEconomicaRBA(String nuActEconomicaRBA) {
		this.nuActEconomicaRBA = nuActEconomicaRBA;
	}
	public String getNuOcupacionRBA() {
		return nuOcupacionRBA;
	}
	public void setNuOcupacionRBA(String nuOcupacionRBA) {
		this.nuOcupacionRBA = nuOcupacionRBA;
	}
	public String getNuRegPaisRBA() {
		return nuRegPaisRBA;
	}
	public void setNuRegPaisRBA(String nuRegPaisRBA) {
		this.nuRegPaisRBA = nuRegPaisRBA;
	}
	public String getNuProServiciosRBA() {
		return nuProServiciosRBA;
	}
	public void setNuProServiciosRBA(String nuProServiciosRBA) {
		this.nuProServiciosRBA = nuProServiciosRBA;
	}
	public String getNuOpeHabitualRBA() {
		return nuOpeHabitualRBA;
	}
	public void setNuOpeHabitualRBA(String nuOpeHabitualRBA) {
		this.nuOpeHabitualRBA = nuOpeHabitualRBA;
	}
	public String getNuIntervinientesRBA() {
		return nuIntervinientesRBA;
	}
	public void setNuIntervinientesRBA(String nuIntervinientesRBA) {
		this.nuIntervinientesRBA = nuIntervinientesRBA;
	}
	public String getNuSucursalRBA() {
		return nuSucursalRBA;
	}
	public void setNuSucursalRBA(String nuSucursalRBA) {
		this.nuSucursalRBA = nuSucursalRBA;
	}
	public String getNuCanCaptacionRBA() {
		return nuCanCaptacionRBA;
	}
	public void setNuCanCaptacionRBA(String nuCanCaptacionRBA) {
		this.nuCanCaptacionRBA = nuCanCaptacionRBA;
	}
	public String getNuAntiguedadPuntajeRBA() {
		return nuAntiguedadPuntajeRBA;
	}
	public void setNuAntiguedadPuntajeRBA(String nuAntiguedadPuntajeRBA) {
		this.nuAntiguedadPuntajeRBA = nuAntiguedadPuntajeRBA;
	}
	public String getNuEdadRBA() {
		return nuEdadRBA;
	}
	public void setNuEdadRBA(String nuEdadRBA) {
		this.nuEdadRBA = nuEdadRBA;
	}
	public String getNuTotalRBA() {
		return nuTotalRBA;
	}
	public void setNuTotalRBA(String nuTotalRBA) {
		this.nuTotalRBA = nuTotalRBA;
	}
	public String getNbMotRiesgoRBA() {
		return nbMotRiesgoRBA;
	}
	public void setNbMotRiesgoRBA(String nbMotRiesgoRBA) {
		this.nbMotRiesgoRBA = nbMotRiesgoRBA;
	}
	public String getFhCamAltBajRiesgoCAR() {
		return fhCamAltBajRiesgoCAR;
	}
	public void setFhCamAltBajRiesgoCAR(String fhCamAltBajRiesgoCAR) {
		this.fhCamAltBajRiesgoCAR = fhCamAltBajRiesgoCAR;
	}
	public String getFhAltAltRiesgoCAR() {
		return fhAltAltRiesgoCAR;
	}
	public void setFhAltAltRiesgoCAR(String fhAltAltRiesgoCAR) {
		this.fhAltAltRiesgoCAR = fhAltAltRiesgoCAR;
	}
	public String getFhUltMovAltBajCAR() {
		return fhUltMovAltBajCAR;
	}
	public void setFhUltMovAltBajCAR(String fhUltMovAltBajCAR) {
		this.fhUltMovAltBajCAR = fhUltMovAltBajCAR;
	}
	public String getNbOrigenBajaCAR() {
		return nbOrigenBajaCAR;
	}
	public void setNbOrigenBajaCAR(String nbOrigenBajaCAR) {
		this.nbOrigenBajaCAR = nbOrigenBajaCAR;
	}
	public String getNbOrigenAltaCAR() {
		return nbOrigenAltaCAR;
	}
	public void setNbOrigenAltaCAR(String nbOrigenAltaCAR) {
		this.nbOrigenAltaCAR = nbOrigenAltaCAR;
	}	





}
