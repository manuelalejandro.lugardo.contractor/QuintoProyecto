package com.bbva.msca.back.dominio;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(table = "TSCA024_USUARIO", schema = "GORAPR", versionField = "")
public class Tsca024Usuario {
	
	@Id
    @Column(name = "CD_USUARIO")
    private String cdUsuario;

}
