// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca014Caso;
import com.bbva.msca.back.dominio.Tsca072TprelFid;
import java.lang.String;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

privileged aspect Tsca042Fideicomiso_Roo_DbManaged {
    
    @ManyToOne
    @JoinColumn(name = "CD_CASO", referencedColumnName = "CD_CASO", nullable = false, insertable = false, updatable = false)
    private Tsca014Caso Tsca042Fideicomiso.cdCaso;
    
    @ManyToOne
    @JoinColumn(name = "TP_FIDEICOMISO", referencedColumnName = "TP_FIDEICOMISO")
    private Tsca072TprelFid Tsca042Fideicomiso.tpFideicomiso;
    
    @Column(name = "CD_REFERENCIA", length = 20)
    private String Tsca042Fideicomiso.cdReferencia;
    
    @Column(name = "NB_FINALIDAD", length = 70)
    private String Tsca042Fideicomiso.nbFinalidad;
    
    @Column(name = "FH_CONSTITUCION")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date Tsca042Fideicomiso.fhConstitucion;
    
    @Column(name = "CD_FIDUCIARIO", length = 4)
    private String Tsca042Fideicomiso.cdFiduciario;
    
    @Column(name = "NB_PATRIMONIO", length = 80)
    private String Tsca042Fideicomiso.nbPatrimonio;
    
    public Tsca014Caso Tsca042Fideicomiso.getCdCaso() {
        return this.cdCaso;
    }
    
    public void Tsca042Fideicomiso.setCdCaso(Tsca014Caso cdCaso) {
        this.cdCaso = cdCaso;
    }
    
    public Tsca072TprelFid Tsca042Fideicomiso.getTpFideicomiso() {
        return this.tpFideicomiso;
    }
    
    public void Tsca042Fideicomiso.setTpFideicomiso(Tsca072TprelFid tpFideicomiso) {
        this.tpFideicomiso = tpFideicomiso;
    }
    
    public String Tsca042Fideicomiso.getCdReferencia() {
        return this.cdReferencia;
    }
    
    public void Tsca042Fideicomiso.setCdReferencia(String cdReferencia) {
        this.cdReferencia = cdReferencia;
    }
    
    public String Tsca042Fideicomiso.getNbFinalidad() {
        return this.nbFinalidad;
    }
    
    public void Tsca042Fideicomiso.setNbFinalidad(String nbFinalidad) {
        this.nbFinalidad = nbFinalidad;
    }
    
    public Date Tsca042Fideicomiso.getFhConstitucion() {
        return this.fhConstitucion;
    }
    
    public void Tsca042Fideicomiso.setFhConstitucion(Date fhConstitucion) {
        this.fhConstitucion = fhConstitucion;
    }
    
    public String Tsca042Fideicomiso.getCdFiduciario() {
        return this.cdFiduciario;
    }
    
    public void Tsca042Fideicomiso.setCdFiduciario(String cdFiduciario) {
        this.cdFiduciario = cdFiduciario;
    }
    
    public String Tsca042Fideicomiso.getNbPatrimonio() {
        return this.nbPatrimonio;
    }
    
    public void Tsca042Fideicomiso.setNbPatrimonio(String nbPatrimonio) {
        this.nbPatrimonio = nbPatrimonio;
    }
    
}
