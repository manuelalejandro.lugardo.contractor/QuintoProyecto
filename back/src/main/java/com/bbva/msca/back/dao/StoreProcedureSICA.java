package com.bbva.msca.back.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import oracle.jdbc.OracleTypes;

import org.hibernate.impl.SessionImpl;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.dto.MecanicaOperacionalConceptoDTO;

@Component
@Configurable
public class StoreProcedureSICA {
	
	@PersistenceContext
    transient EntityManager entityManager;

	@Transactional
	public void consultaProcedimientoOut() {
		SessionImpl sesionimpl = (SessionImpl)entityManager.getDelegate();
		Connection con = sesionimpl.connection();
		CallableStatement  cstmt = null;
		
		//List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConceptoDTO = null;
		//List lstResultSet = null;
		
		try {
			cstmt = con.prepareCall("{ CALL SP_MEC_OPE_ALL(?,?,?,?) }");
			cstmt.setQueryTimeout(1800);
			cstmt.setString(1,"");
			cstmt.setString(2,"");
			cstmt.setString(3,"");
			cstmt.registerOutParameter(4,OracleTypes.CURSOR);
			cstmt.execute();
			ResultSet rset = (ResultSet)cstmt.getObject(4);
			/*
			lstResultSet = rset.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTC")
	        .addScalar("nuMovimientosTC")
	        .addScalar("imMontoTC")
	        .addScalar("nuPorcentajeTC")
	        .addScalar("nuPromedioTC")
	        .addScalar("nuMovimientosATC")
	        .addScalar("imMontoATC")
	        .addScalar("nuPorcentajeATC")
	        .addScalar("nuPromedioATC")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class)).list();
			lstMecanicaOperacionalConceptoDTO = (List<MecanicaOperacionalConceptoDTO>)lstResultSet;
			*/
			
			//System.out.println(lstMecanicaOperacionalConceptoDTO.get(0).getNbConceptoTC());
			
			while (rset.next()){
				System.out.println(rset.getString("nbConceptoTC"));
			}
				
			rset.close();
			cstmt.close();
			con.close();
				
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
