package com.bbva.msca.back.util;

import java.math.BigDecimal;

public class Constantes {
	
	public static final int FIN_OK = 1;
	public static final int FIN_ER = -1;
	public static final int FIN_AV = 0;
	
	public static final String CD_ST_SIS_A = "11";
	public static final String CD_ST_SIS_I = "12";
	
	public static final int SUPERVISOR = 2;
	
	public static BigDecimal CASO_EN_ANALISIS = new BigDecimal(1);
	public static BigDecimal CASO_EN_SUPERVISION = new BigDecimal(2);
	public static BigDecimal CASO_LIBERADO_A_SIA = new BigDecimal(3);
	public static BigDecimal CASO_DESCARTADO = new BigDecimal(4);
	public static BigDecimal CASO_EN_REANALISIS = new BigDecimal(5);
	
	public static BigDecimal ALERTA_PENDIENTE_DE_ASIGNACION = new BigDecimal(1);
	public static BigDecimal ALERTA_ASIGNADA = new BigDecimal(2);
	public static BigDecimal ALERTA_DESCARTADA = new BigDecimal(3);
	
	public static String DICTAMEN_LIBERADO_A_SIA = "LI";
	public static String DICTAMEN_DESCARTADO = "DE";
	public static String DICTAMEN_REGRESAR_A_CONSULTOR = "RE";
	
	public static String ASIGNACION_MANUAL = "27";
	public static String ASIGNACION_AUTOMATICA = "26";
	
	public static final String NOMBRE_PLANTILLA_CEDULA_CASO = "VAR.ARQSPRING.REPORTES.NOMBRE.CEDULA.CASO";
	public static final String PATH_SALIDA_REPORTES = "VAR.ARQSPRING.REPORTES.PATH.OUT.CEDULA.CASO";
	public static final String LOGO_BANCOMER = "VAR.ARQSPRING.REPORTE.LOGO.BANCOMER";
	public static final String FORMATO_FECHA_REPORTE_CEDULA_CASO = "EEEE, d 'de' MMMM 'de' yyyy";
	public static final String LENGUAJE_FORMATO_FECHA_MIN = "es";
	public static final String LENGUAJE_FORMATO_FECHA_MAY = "ES";
	public static final String NOMBRE_REPORTE_SALIDA_CEDULA_CASO = "VAR.ARQSPRING.REPORTES.NOMBRE.OUT.CEDULA.CASO";
	public static final String EXTENSION_PDF = ".pdf";
	public static final String NAME_SEPARATOR = "_";
}
