package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaMTDTO {
	
	@AnotacionAlerta(tipologia = "mt", idCampo = "imMontoAlerta", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_IMMONTOALERTA", formatter = "formatterMoney")
	private BigDecimal imMontoAlerta;
		
	@AnotacionAlerta(tipologia = "mt", idCampo = "cdPeriodo", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_CDPERIODO")
	private BigDecimal cdPeriodo;
		
	@AnotacionAlerta(tipologia = "mt", idCampo = "cdAccion", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_CDACCION", instanceType = "1")
	private String cdAccion;
	
	@AnotacionAlerta(tipologia = "mt", idCampo = "nbCte", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")
	private String nbCte;
	
	@AnotacionAlerta(tipologia = "mt", idCampo = "tpPerJuri", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_TPPERJURI", instanceType = "1")
	private String tpPerJuri;
	
	@AnotacionAlerta(tipologia = "mt", idCampo = "cdOficinaGest", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_CDOFICINAGEST", instanceType = "1")
	private String cdOficinaGest;
	
	@AnotacionAlerta(tipologia = "mt", idCampo = "nbBanca", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_NBBANCA", instanceType = "1")
	private String nbBanca;
	
	@AnotacionAlerta(tipologia = "mt", idCampo = "imScore", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_IMSCORE")
	private BigDecimal imScore;
	
	@AnotacionAlerta(tipologia = "mt", idCampo = "ctMovimiento", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_CTMOVIMIENTO", instanceType = "1")
	private String ctMovimiento;
	
	@AnotacionAlerta(tipologia = "mt", idCampo = "cdDirecta", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_CDDIRECTA", instanceType = "1")
	private String cdDirecta;
	
	@AnotacionAlerta(tipologia = "mt", idCampo = "nuScoreCorte", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_NUSCORECORTE")
	private BigDecimal nuScoreCorte;

	public void setImMontoAlerta(BigDecimal imMontoAlerta) {
		this.imMontoAlerta = imMontoAlerta;
	}

	public BigDecimal getImMontoAlerta() {
		return this.imMontoAlerta;
	}

	public void setCdPeriodo(BigDecimal cdPeriodo) {
		this.cdPeriodo = cdPeriodo;
	}

	public BigDecimal getCdPeriodo() {
		return this.cdPeriodo;
	}

	public void setCdAccion(String cdAccion) {
		this.cdAccion = cdAccion;
	}

	public String getCdAccion() {
		return this.cdAccion;
	}

	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}

	public String getNbCte() {
		return this.nbCte;
	}

	public void setTpPerJuri(String tpPerJuri) {
		this.tpPerJuri = tpPerJuri;
	}

	public String getTpPerJuri() {
		return this.tpPerJuri;
	}

	public void setCdOficinaGest(String cdOficinaGest) {
		this.cdOficinaGest = cdOficinaGest;
	}

	public String getCdOficinaGest() {
		return this.cdOficinaGest;
	}

	public void setNbBanca(String nbBanca) {
		this.nbBanca = nbBanca;
	}

	public String getNbBanca() {
		return this.nbBanca;
	}

	public void setImScore(BigDecimal imScore) {
		this.imScore = imScore;
	}

	public BigDecimal getImScore() {
		return this.imScore;
	}

	public void setCtMovimiento(String ctMovimiento) {
		this.ctMovimiento = ctMovimiento;
	}

	public String getCtMovimiento() {
		return this.ctMovimiento;
	}

	public void setCdDirecta(String cdDirecta) {
		this.cdDirecta = cdDirecta;
	}

	public String getCdDirecta() {
		return this.cdDirecta;
	}

	public void setNuScoreCorte(BigDecimal nuScoreCorte) {
		this.nuScoreCorte = nuScoreCorte;
	}

	public BigDecimal getNuScoreCorte() {
		return this.nuScoreCorte;
	}
}
