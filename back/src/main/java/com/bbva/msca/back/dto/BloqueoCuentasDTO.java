package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class BloqueoCuentasDTO {
	
	private BigDecimal cdCaso;
	private String nuFolioAlerta;
	private String cdCliente;
	private String nuCuenta;
	private String nbCliente;
	private String cdSistema;// ???
	private String cdTipologia;
	private String stBloqueo;
	private Date fhBloqueo;
	private String nbReconsiderado;
	private String nbObservaciones;
	private String nbOperacion;
	private String nbDesbloqueo;
	private String cdSector;
	private BigDecimal imImporte;
	private Date fhAlerta;
	
	
	
	public BigDecimal getImImporte() {
		return imImporte;
	}
	public void setImImporte(BigDecimal imImporte) {
		this.imImporte = imImporte;
	}
	public Date getFhAlerta() {
		return fhAlerta;
	}
	public void setFhAlerta(Date fhAlerta) {
		this.fhAlerta = fhAlerta;
	}
	public String getCdSector() {
		return cdSector;
	}
	public void setCdSector(String cdSector) {
		this.cdSector = cdSector;
	}
	public String getNuFolioAlerta() {
		return nuFolioAlerta;
	}
	public void setNuFolioAlerta(String nuFolioAlerta) {
		this.nuFolioAlerta = nuFolioAlerta;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public BigDecimal getCdCaso() {
		return cdCaso;
	}
	public void setCdCaso(BigDecimal cdCaso) {
		this.cdCaso = cdCaso;
	}
	public String getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public String getCdSistema() {
		return cdSistema;
	}
	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}
	public String getCdTipologia() {
		return cdTipologia;
	}
	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	public String getStBloqueo() {
		return stBloqueo;
	}
	public void setStBloqueo(String stBloqueo) {
		this.stBloqueo = stBloqueo;
	}
	public Date getFhBloqueo() {
		return fhBloqueo;
	}
	public void setFhBloqueo(Date fhBloqueo) {
		this.fhBloqueo = fhBloqueo;
	}
	public String getNbReconsiderado() {
		return nbReconsiderado;
	}
	public void setNbReconsiderado(String nbReconsiderado) {
		this.nbReconsiderado = nbReconsiderado;
	}
	public String getNbObservaciones() {
		return nbObservaciones;
	}
	public void setNbObservaciones(String nbObservaciones) {
		this.nbObservaciones = nbObservaciones;
	}
	public String getNbOperacion() {
		return nbOperacion;
	}
	public void setNbOperacion(String nbOperacion) {
		this.nbOperacion = nbOperacion;
	}
	public String getNbDesbloqueo() {
		return nbDesbloqueo;
	}
	public void setNbDesbloqueo(String nbDesbloqueo) {
		this.nbDesbloqueo = nbDesbloqueo;
	}

}
