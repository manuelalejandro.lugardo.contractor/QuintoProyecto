package com.bbva.msca.back.dto;

import java.util.Date;

public class PersonasRelacionadasDTO {
	
	private Integer nuCliente;
	private String nbCliente;
	private String nbTipRelacion;
	private String nbRFC;
	private Integer nuNIF;
	private String nbPaisNIF;
	private Integer nuFEA;
	private String nbSexo;
	private Date fhNacConstitucion;
	private String nbEntNacimiento;
	private String nbEntFedNacimiento;
	private String nbNacionalidad;
	private String nbOcupacion;
	private String nbCURP;
	private String nbCorreo;
	private Integer nuTelefono;
	private String nbCalle;
	private Integer nuExterior;
	private Integer nuInterior;
	private Integer nuCP;
	private String nbColonia;
	private String nbDelegacion;
	private String nbEntidad;
	private String nbPais;
	
	
	public Integer getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(Integer nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNbTipRelacion() {
		return nbTipRelacion;
	}
	public void setNbTipRelacion(String nbTipRelacion) {
		this.nbTipRelacion = nbTipRelacion;
	}
	public String getNbRFC() {
		return nbRFC;
	}
	public void setNbRFC(String nbRFC) {
		this.nbRFC = nbRFC;
	}
	public Integer getNuNIF() {
		return nuNIF;
	}
	public void setNuNIF(Integer nuNIF) {
		this.nuNIF = nuNIF;
	}
	public String getNbPaisNIF() {
		return nbPaisNIF;
	}
	public void setNbPaisNIF(String nbPaisNIF) {
		this.nbPaisNIF = nbPaisNIF;
	}
	public Integer getNuFEA() {
		return nuFEA;
	}
	public void setNuFEA(Integer nuFEA) {
		this.nuFEA = nuFEA;
	}
	public String getNbSexo() {
		return nbSexo;
	}
	public void setNbSexo(String nbSexo) {
		this.nbSexo = nbSexo;
	}
	public Date getFhNacConstitucion() {
		return fhNacConstitucion;
	}
	public void setFhNacConstitucion(Date fhNacConstitucion) {
		this.fhNacConstitucion = fhNacConstitucion;
	}
	public String getNbEntNacimiento() {
		return nbEntNacimiento;
	}
	public void setNbEntNacimiento(String nbEntNacimiento) {
		this.nbEntNacimiento = nbEntNacimiento;
	}
	public String getNbEntFedNacimiento() {
		return nbEntFedNacimiento;
	}
	public void setNbEntFedNacimiento(String nbEntFedNacimiento) {
		this.nbEntFedNacimiento = nbEntFedNacimiento;
	}
	public String getNbNacionalidad() {
		return nbNacionalidad;
	}
	public void setNbNacionalidad(String nbNacionalidad) {
		this.nbNacionalidad = nbNacionalidad;
	}
	public String getNbOcupacion() {
		return nbOcupacion;
	}
	public void setNbOcupacion(String nbOcupacion) {
		this.nbOcupacion = nbOcupacion;
	}
	public String getNbCURP() {
		return nbCURP;
	}
	public void setNbCURP(String nbCURP) {
		this.nbCURP = nbCURP;
	}
	public String getNbCorreo() {
		return nbCorreo;
	}
	public void setNbCorreo(String nbCorreo) {
		this.nbCorreo = nbCorreo;
	}
	public Integer getNuTelefono() {
		return nuTelefono;
	}
	public void setNuTelefono(Integer nuTelefono) {
		this.nuTelefono = nuTelefono;
	}
	public String getNbCalle() {
		return nbCalle;
	}
	public void setNbCalle(String nbCalle) {
		this.nbCalle = nbCalle;
	}
	public Integer getNuExterior() {
		return nuExterior;
	}
	public void setNuExterior(Integer nuExterior) {
		this.nuExterior = nuExterior;
	}
	public Integer getNuInterior() {
		return nuInterior;
	}
	public void setNuInterior(Integer nuInterior) {
		this.nuInterior = nuInterior;
	}
	public Integer getNuCP() {
		return nuCP;
	}
	public void setNuCP(Integer nuCP) {
		this.nuCP = nuCP;
	}
	public String getNbColonia() {
		return nbColonia;
	}
	public void setNbColonia(String nbColonia) {
		this.nbColonia = nbColonia;
	}
	public String getNbDelegacion() {
		return nbDelegacion;
	}
	public void setNbDelegacion(String nbDelegacion) {
		this.nbDelegacion = nbDelegacion;
	}
	public String getNbEntidad() {
		return nbEntidad;
	}
	public void setNbEntidad(String nbEntidad) {
		this.nbEntidad = nbEntidad;
	}
	public String getNbPais() {
		return nbPais;
	}
	public void setNbPais(String nbPais) {
		this.nbPais = nbPais;
	}

}
