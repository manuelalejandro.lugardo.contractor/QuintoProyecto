package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaBXDTO {

	@AnotacionAlerta(tipologia = "bx", idCampo = "imMontoAlerta", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_IMMONTOALERTA", formatter = "formatterMoney")
	private BigDecimal imMontoAlerta;
	@AnotacionAlerta(tipologia = "bx", idCampo = "nbBeneficiario", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_NBBENEFICIARIO", instanceType = "1")
	private String nbBeneficiario;
	@AnotacionAlerta(tipologia = "bx", idCampo = "nuTransac", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_NUTRANSAC")
	private BigDecimal nuTransac;
	@AnotacionAlerta(tipologia = "bx", idCampo = "nbEmisor", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_NBEMISOR", instanceType = "1")
	private String nbEmisor;
	@AnotacionAlerta(tipologia = "bx", idCampo = "stTransBanco", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_STTRANSBANCO", instanceType = "1")
	private String stTransBanco;
	@AnotacionAlerta(tipologia = "bx", idCampo = "nuOrdenante", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_NUORDENANTES")
	private BigDecimal nuOrdenante;
	

	public BigDecimal getImMontoAlerta() {
		return imMontoAlerta;
	}
	public void setImMontoAlerta(BigDecimal imMontoAlerta) {
		this.imMontoAlerta = imMontoAlerta;
	}
	public String getNbBeneficiario() {
		return nbBeneficiario;
	}
	public void setNbBeneficiario(String nbBeneficiario) {
		this.nbBeneficiario = nbBeneficiario;
	}
	public BigDecimal getNuTransac() {
		return nuTransac;
	}
	public void setNuTransac(BigDecimal nuTransac) {
		this.nuTransac = nuTransac;
	}
	public String getNbEmisor() {
		return nbEmisor;
	}
	public void setNbEmisor(String nbEmisor) {
		this.nbEmisor = nbEmisor;
	}
	public String getStTransBanco() {
		return stTransBanco;
	}
	public void setStTransBanco(String stTransBanco) {
		this.stTransBanco = stTransBanco;
	}
	public BigDecimal getNuOrdenante() {
		return nuOrdenante;
	}
	public void setNuOrdenante(BigDecimal nuOrdenante) {
		this.nuOrdenante = nuOrdenante;
	}
}
