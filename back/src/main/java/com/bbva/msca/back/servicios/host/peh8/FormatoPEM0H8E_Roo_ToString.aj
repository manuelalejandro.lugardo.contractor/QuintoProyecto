// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.servicios.host.peh8;

import java.lang.String;

privileged aspect FormatoPEM0H8E_Roo_ToString {
    
    public String FormatoPEM0H8E.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Numcta: ").append(getNumcta()).append(", ");
        sb.append("Ultcla: ").append(getUltcla()).append(", ");
        sb.append("Ultsec: ").append(getUltsec());
        return sb.toString();
    }
    
}
