package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(identifierType = Tsca036SprCtPfPK.class, versionField = "", table = "TSCA036_SPR_CT_PF", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca036SprCtPf {
}
