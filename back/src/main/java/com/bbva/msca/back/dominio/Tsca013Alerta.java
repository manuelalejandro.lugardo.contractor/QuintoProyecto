package com.bbva.msca.back.dominio;

import java.math.BigDecimal;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.Column;

@RooJavaBean
@RooToString
@RooEntity(identifierType = Tsca013AlertaPK.class, versionField = "", table = "TSCA013_ALERTA", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca013Alerta {
	
	@Column(name = "CD_CASO")
	private BigDecimal cdCaso;
	
	@Column(name = "ST_ALERTA")
	private BigDecimal cdStAlerta;
}
