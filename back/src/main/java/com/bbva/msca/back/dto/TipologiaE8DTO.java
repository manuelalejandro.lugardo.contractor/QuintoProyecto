package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaE8DTO {

	@AnotacionAlerta(tipologia = "rp", idCampo = "cdSesion", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_CDSESION", instanceType = "1")
	private String cdSesion;
	@AnotacionAlerta(tipologia = "rp", idCampo = "imMontoAlerta", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_IMMONTOALERTA", formatter = "formatterMoney")
	private BigDecimal imMontoAlerta;
	@AnotacionAlerta(tipologia = "rp", idCampo = "cdCaso", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_CDCASO")
	private BigDecimal cdCaso;
	@AnotacionAlerta(tipologia = "rp", idCampo = "nbNombreCte", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_NBNOMBRECTE", instanceType = "1")
	private String nbNombreCte;
	@AnotacionAlerta(tipologia = "rp", idCampo = "nbAppaternoCte", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_NBAPPATERNOCTE", instanceType = "1")
	private String nbAppaternoCte;
	@AnotacionAlerta(tipologia = "rp", idCampo = "nbApmaternoCte", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_NBAPMATERNOCTE", instanceType = "1")
	private String nbApmaternoCte;
	@AnotacionAlerta(tipologia = "rp", idCampo = "stCasoPriAlta", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_STCASOPRIALTA", instanceType = "1")
	private String stCasoPriAlta;
	@AnotacionAlerta(tipologia = "rp", idCampo = "stAlcancePriAlta", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_STALCANCEPRIALTA", instanceType = "1")
	private String stAlcancePriAlta;
	@AnotacionAlerta(tipologia = "rp", idCampo = "cdDictamen", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_CDDICTAMEN", instanceType = "1")
	private String cdDictamen;
	
	
	public String getCdSesion() {
		return cdSesion;
	}
	public void setCdSesion(String cdSesion) {
		this.cdSesion = cdSesion;
	}
	public BigDecimal getImMontoAlerta() {
		return imMontoAlerta;
	}
	public void setImMontoAlerta(BigDecimal imMontoAlerta) {
		this.imMontoAlerta = imMontoAlerta;
	}
	public BigDecimal getCdCaso() {
		return cdCaso;
	}
	public void setCdCaso(BigDecimal cdCaso) {
		this.cdCaso = cdCaso;
	}
	public String getNbNombreCte() {
		return nbNombreCte;
	}
	public void setNbNombreCte(String nbNombreCte) {
		this.nbNombreCte = nbNombreCte;
	}
	public String getNbAppaternoCte() {
		return nbAppaternoCte;
	}
	public void setNbAppaternoCte(String nbAppaternoCte) {
		this.nbAppaternoCte = nbAppaternoCte;
	}
	public String getNbApmaternoCte() {
		return nbApmaternoCte;
	}
	public void setNbApmaternoCte(String nbApmaternoCte) {
		this.nbApmaternoCte = nbApmaternoCte;
	}
	public String getStCasoPriAlta() {
		return stCasoPriAlta;
	}
	public void setStCasoPriAlta(String stCasoPriAlta) {
		this.stCasoPriAlta = stCasoPriAlta;
	}
	public String getStAlcancePriAlta() {
		return stAlcancePriAlta;
	}
	public void setStAlcancePriAlta(String stAlcancePriAlta) {
		this.stAlcancePriAlta = stAlcancePriAlta;
	}
	public String getCdDictamen() {
		return cdDictamen;
	}
	public void setCdDictamen(String cdDictamen) {
		this.cdDictamen = cdDictamen;
	}
}
