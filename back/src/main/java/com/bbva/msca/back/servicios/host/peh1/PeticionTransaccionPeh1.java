package com.bbva.msca.back.servicios.host.peh1;

import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>PEH1</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPeh1</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPeh1</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> config_ps9_pe</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEH1CCT.txt
 * PEH1CONSULTA RELACIONES CON LA ENTIDAD PE        PE2C00H1MBDPEPO PEM0H1E             PEH1  NN3000NNNNNN    SSTN A      NNNNNNNN  NN                2008-06-04IDCFHP  2013-11-0819.26.01QG4CUCCT2008-06-04-11.19.44.587256IDCFHP  0001-01-010001-01-01
 * FICHERO: PEH1FDF.txt
 * PEM0H1E �COMUNICACION CON NACAR        �F�09�00120�01�00001�OPCION �OPCION              �A�001�0�O�        �
 * PEM0H1E �COMUNICACION CON NACAR        �F�09�00120�02�00002�NUMCLIE�NUMERO DE CLIENTE   �A�008�0�O�        �
 * PEM0H1E �COMUNICACION CON NACAR        �F�09�00120�03�00010�CLVIDEN�SECUENCIA           �A�014�0�O�        �
 * PEM0H1E �COMUNICACION CON NACAR        �F�09�00120�04�00024�NOMBRE �NOMBRE              �A�020�0�O�        �
 * PEM0H1E �COMUNICACION CON NACAR        �F�09�00120�05�00044�PRIAPE �PRIMER APELLIDO     �A�020�0�O�        �
 * PEM0H1E �COMUNICACION CON NACAR        �F�09�00120�06�00064�SEGAPE �SEGUNDO APELLIDO    �A�020�0�O�        �
 * PEM0H1E �COMUNICACION CON NACAR        �F�09�00120�07�00084�ULTCTA �ULTIMA CUENTA       �A�018�0�O�        �
 * PEM0H1E �COMUNICACION CON NACAR        �F�09�00120�08�00102�ULTPART�ULTIMO PARTICIPE    �A�003�0�O�        �
 * PEM0H1E �COMUNICACION CON NACAR        �F�09�00120�09�00105�ULTTARJ�ULTIMA TARJETA      �A�016�0�O�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�01�00001�NOMBRE �NOMBRE CLIENTE      �A�020�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�02�00021�PRIAPE �PRIMER APELLIDO     �A�020�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�03�00041�SEGAPE �SEGUNDO APELLIDO    �A�020�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�04�00061�CLVIDEN�CLAVE DE IDENTIFICAC�A�014�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�05�00075�MASDAT �INDICADOR MAS DATOS �A�001�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�06�00076�CODMSG �CODIGO DE MENSAJE   �A�007�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�07�00083�DESMSG �DESCRIPCION MENSAJE �A�030�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�08�00113�NUMREG �NUMERO DE REGISTROS �N�003�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�09�00116�ULTCTA �ULTIMA CUENTA       �A�018�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�10�00134�ULTPART�ULTIMO PARTICIPE    �A�003�0�S�        �
 * PEM0H1S1�DATOS GRABADOS EN TS1         �X�11�00152�11�00137�ULTTARJ�ULTIMA TARJETA      �A�016�0�S�        �
 * PEM0H1S2�DATOS GRABADOS EN TS2         �X�08�00110�01�00001�CLAVCTA�CLAVE DE LA CUENTA  �A�020�0�S�        �
 * PEM0H1S2�DATOS GRABADOS EN TS2         �X�08�00110�02�00021�CLAINTE�CLASE DE INTERVENC. �A�001�0�S�        �
 * PEM0H1S2�DATOS GRABADOS EN TS2         �X�08�00110�03�00022�DESINTE�DESCRIP. DE INTERVEN�A�040�0�S�        �
 * PEM0H1S2�DATOS GRABADOS EN TS2         �X�08�00110�04�00062�SECINTE�SECUENC. DE INTERV. �A�002�0�S�        �
 * PEM0H1S2�DATOS GRABADOS EN TS2         �X�08�00110�05�00064�ESTADO �ESTADO DEL CONTRATO �A�001�0�S�        �
 * PEM0H1S2�DATOS GRABADOS EN TS2         �X�08�00110�06�00065�PELSERV�DESCRIPCION DEL SERV�A�020�0�S�        �
 * PEM0H1S2�DATOS GRABADOS EN TS2         �X�08�00110�07�00085�BBVMOVI�SERVICIO BBVA MOVIL �A�025�0�S�        �
 * PEM0H1S2�DATOS GRABADOS EN TS2         �X�08�00110�08�00110�INDCOEX�INDIC. DE EXTRACTO  �A�001�0�S�        �
 * FICHERO: PEH1FDX.txt
 * PEH1PEM0H1S1PEECH1S1PE2C00H11S                             IDCFHP  2008-06-04-11.48.21.251560IDCFHP  2008-06-04-11.48.21.251591
 * PEH1PEM0H1S2PEECH1S2PE2C00H11S                             IDCFHP  2008-06-04-11.48.37.234138IDCFHP  2008-06-04-11.48.37.234196
</pre></code>
 * 
 * @see RespuestaTransaccionPeh1
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PEH1",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "config_ps9",
	respuesta = RespuestaTransaccionPeh1.class
)
@Multiformato(formatos = {FormatoPEM0H1E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionPeh1 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}