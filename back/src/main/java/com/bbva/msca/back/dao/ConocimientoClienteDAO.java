package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import com.bbva.msca.back.dto.ConocimientoClienteDTO;

public class ConocimientoClienteDAO {

	BdSac bdSac = new BdSac();
	
	private EntityManager em = bdSac.getEntityManager();
 
	@SuppressWarnings("unchecked")
	public ConocimientoClienteDTO getKYC(BigDecimal cdCaso,String cdCliente) throws Exception{ 
	 	  ConocimientoClienteDTO datosKyc = new ConocimientoClienteDTO();
	 	 List<ConocimientoClienteDTO> lstDatosKyc = null;
	 	  
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getDatosKycAnalisis"));
			 queryCR.setParameter(1,cdCaso); 
			 queryCR.setParameter(2,cdCliente); 
			 	 queryCR.unwrap(org.hibernate.SQLQuery.class)
		 		.addScalar("nbRegSimplificado")
				.addScalar("nbActEconomica")
				.addScalar("nbOcupacion")
				.addScalar("nbPuestoPEP")
				.addScalar("nbPEP")
				.addScalar("nbParentescoPEP")
				.addScalar("nbPersRel")
				.addScalar("nbPaisResidencia")
				.addScalar("nbNacionalidad")
				.addScalar("nbJusAperCta")
				.addScalar("nbProRecursos")
				.addScalar("nbPriFueIngresos")
				.addScalar("nbIndRiesgo",StringType.INSTANCE)
				.addScalar("nbRieActividad")
				.addScalar("nuTipCliente")
				.addScalar("nuActEconomica")
				.addScalar("nuOcupacion")
				.addScalar("nuRegPais")
				.addScalar("nuProServicios")
				.addScalar("nuOpeHabitual")
				.addScalar("nuIntervinientes")
				.addScalar("nuSucursal")
				.addScalar("nuCanCaptacion")
				.addScalar("nuAntiguedadPuntaje")
				.addScalar("nuEdad")
				.addScalar("nuTotal")
				.addScalar("nbMotRiesgo")
				.addScalar("nbJusOpePesos")
				.addScalar("nbJusOpeDolares",StringType.INSTANCE)
				.addScalar("imMonTotDepositos")
				.addScalar("nbLocOperar")
				.addScalar("fhCamAltBajRiesgo",StringType.INSTANCE)
				.addScalar("fhAltAltRiesgo",StringType.INSTANCE)
				.addScalar("fhUltMovAltBaj",StringType.INSTANCE)
				.addScalar("nbOrigenBaja",StringType.INSTANCE)
				.addScalar("nbOrigenAlta",StringType.INSTANCE)
				.addScalar("txObjetoSocial")
				.addScalar("imInstrumento")
				.addScalar("cdEntFedOrigen")
				.addScalar("cdEntFedDestino")
				.addScalar("imTransInt")
				.addScalar("cdEntFedIntOri")
				.addScalar("cdEntFedIntDes")
				.addScalar("imTransNacional")
				.addScalar("cdEntFedNalOri")
				.addScalar("cdEntFedNalDes")
				.addScalar("imEfectivo")
				
				.setResultTransformer(Transformers.aliasToBean(ConocimientoClienteDTO.class)); 
		  
			 	lstDatosKyc = ( List<ConocimientoClienteDTO> )queryCR.getResultList();
			 	if(!lstDatosKyc.isEmpty()){
			 		datosKyc = lstDatosKyc.get(0);
			 	}
		return datosKyc;
		 
	}		


	@SuppressWarnings("unchecked")
	public ConocimientoClienteDTO getConocimientoCliente(String cdSistema, String nuFolioAlerta) throws Exception{
		ConocimientoClienteDTO transaccionClienteDTO = new ConocimientoClienteDTO();
		List<ConocimientoClienteDTO> lstResultado = null;
		
		Query query = em.createNativeQuery(Consultas.getConsulta("getConocimientoDelCliente"));
		 query.setParameter(1,cdSistema);
		 query.setParameter(2,nuFolioAlerta);
		 query.unwrap(org.hibernate.SQLQuery.class)
			
			.addScalar("paisRecidenciaCAR", StringType.INSTANCE)
			.addScalar("nacionalidadCAR", StringType.INSTANCE)
			.addScalar("justCuentaCAR")
			.addScalar("procedenciaRecCAR")
			.addScalar("fuenteIngresoCAR")
				.addScalar("fhCamAltBajRiesgoCAR", StringType.INSTANCE) //NO SE PINTAN
				.addScalar("fhAltAltRiesgoCAR", StringType.INSTANCE)    //NO SE PINTAN
				.addScalar("fhUltMovAltBajCAR", StringType.INSTANCE)    //NO SE PINTAN
				.addScalar("nbOrigenBajaCAR", StringType.INSTANCE)      //NO SE PINTAN
				.addScalar("nbOrigenAltaCAR", StringType.INSTANCE)      //NO SE PINTAN
			.addScalar("indRiesgoCAR", StringType.INSTANCE)
			.addScalar("indRiesgoActCAR", StringType.INSTANCE)
			
		
			.addScalar("nuTipClienteRBA", StringType.INSTANCE) 
			.addScalar("nuActEconomicaRBA", StringType.INSTANCE) 
			.addScalar("nuOcupacionRBA", StringType.INSTANCE) 
			.addScalar("nuRegPaisRBA", StringType.INSTANCE) 
			.addScalar("nuProServiciosRBA", StringType.INSTANCE) 
			.addScalar("nuOpeHabitualRBA", StringType.INSTANCE) 
			.addScalar("nuIntervinientesRBA", StringType.INSTANCE) 
			.addScalar("nuSucursalRBA", StringType.INSTANCE) 
			.addScalar("nuCanCaptacionRBA", StringType.INSTANCE) 
			.addScalar("nuAntiguedadPuntajeRBA", StringType.INSTANCE) 
			.addScalar("nuEdadRBA", StringType.INSTANCE) 
			.addScalar("nuTotalRBA", StringType.INSTANCE) 
			.addScalar("nbMotRiesgoRBA", StringType.INSTANCE) 

					
		.setResultTransformer(Transformers.aliasToBean(ConocimientoClienteDTO.class));
		 
		 lstResultado = (List<ConocimientoClienteDTO>)query.getResultList();
		 if(!lstResultado.isEmpty()){
		 transaccionClienteDTO = lstResultado.get(0);
		 }		 
					
		return transaccionClienteDTO;

	}
}
