package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaR1DTO {

	@AnotacionAlerta(tipologia = "r1", idCampo = "cdPeriodo", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_CDPERIODO")
	private BigDecimal cdPeriodo;
	@AnotacionAlerta(tipologia = "r1", idCampo = "cdAccion", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_CDACCION", instanceType = "1")
	private String cdAccion;
	@AnotacionAlerta(tipologia = "r1", idCampo = "nbActividad", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_NBACTIVIDAD", instanceType = "1")
	private String nbActividad;
	@AnotacionAlerta(tipologia = "r1", idCampo = "tpPerJuri", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_TPPERJURI", instanceType = "1")
	private String tpPerJuri;
	@AnotacionAlerta(tipologia = "r1", idCampo = "cdOficinaGest", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_CDOFICINAGEST", instanceType = "1")
	private String cdOficinaGest;
	@AnotacionAlerta(tipologia = "r1", idCampo = "nbBanca", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_NBBANCA", instanceType = "1")
	private String nbBanca;
	@AnotacionAlerta(tipologia = "r1", idCampo = "imScore", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_IMSCORE")
	private BigDecimal imScore;
	@AnotacionAlerta(tipologia = "r1", idCampo = "ctMovimiento", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_CTMOVIMIENTO", instanceType = "1")
	private String ctMovimiento;
	@AnotacionAlerta(tipologia = "r1", idCampo = "cdDirecta", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_CDDIRECTA", instanceType = "1")
	private String cdDirecta;
	@AnotacionAlerta(tipologia = "r1", idCampo = "nuScoreCorte", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_NUSCORECORTE")
	private BigDecimal nuScoreCorte;
	@AnotacionAlerta(tipologia = "r1", idCampo = "nbCte", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")	
	private String nbCte;
	
	public String getNbCte() {
		return nbCte;
	}
	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}
	public BigDecimal getCdPeriodo() {
		return cdPeriodo;
	}
	public void setCdPeriodo(BigDecimal cdPeriodo) {
		this.cdPeriodo = cdPeriodo;
	}
	public String getCdAccion() {
		return cdAccion;
	}
	public void setCdAccion(String cdAccion) {
		this.cdAccion = cdAccion;
	}
	public String getNbActividad() {
		return nbActividad;
	}
	public void setNbActividad(String nbActividad) {
		this.nbActividad = nbActividad;
	}
	public String getTpPerJuri() {
		return tpPerJuri;
	}
	public void setTpPerJuri(String tpPerJuri) {
		this.tpPerJuri = tpPerJuri;
	}
	public String getCdOficinaGest() {
		return cdOficinaGest;
	}
	public void setCdOficinaGest(String cdOficinaGest) {
		this.cdOficinaGest = cdOficinaGest;
	}
	public String getNbBanca() {
		return nbBanca;
	}
	public void setNbBanca(String nbBanca) {
		this.nbBanca = nbBanca;
	}
	public BigDecimal getImScore() {
		return imScore;
	}
	public void setImScore(BigDecimal imScore) {
		this.imScore = imScore;
	}
	public String getCtMovimiento() {
		return ctMovimiento;
	}
	public void setCtMovimiento(String ctMovimiento) {
		this.ctMovimiento = ctMovimiento;
	}
	public String getCdDirecta() {
		return cdDirecta;
	}
	public void setCdDirecta(String cdDirecta) {
		this.cdDirecta = cdDirecta;
	}
	public BigDecimal getNuScoreCorte() {
		return nuScoreCorte;
	}
	public void setNuScoreCorte(BigDecimal nuScoreCorte) {
		this.nuScoreCorte = nuScoreCorte;
	}
}
