package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class MontosTotalDTO {
	
	private BigDecimal numAbonosTotal;
	private BigDecimal imAbonosTotal;
	
	private BigDecimal numCargosTotal;
	private BigDecimal imCargosTotal;
	
	public BigDecimal getNumAbonosTotal() {
		return numAbonosTotal;
	}
	public void setNumAbonosTotal(BigDecimal numAbonosTotal) {
		this.numAbonosTotal = numAbonosTotal;
	}
	public BigDecimal getImAbonosTotal() {
		return imAbonosTotal;
	}
	public void setImAbonosTotal(BigDecimal imAbonosTotal) {
		this.imAbonosTotal = imAbonosTotal;
	}
	public BigDecimal getNumCargosTotal() {
		return numCargosTotal;
	}
	public void setNumCargosTotal(BigDecimal numCargosTotal) {
		this.numCargosTotal = numCargosTotal;
	}
	public BigDecimal getImCargosTotal() {
		return imCargosTotal;
	}
	public void setImCargosTotal(BigDecimal imCargosTotal) {
		this.imCargosTotal = imCargosTotal;
	}
	
	

}
