package com.bbva.msca.back.srv;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.host.ExcepcionConfiguracion;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionProtocolo;
import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionRespuestaHost;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.Autorizacion;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.CopySalida;
import com.bbva.jee.arq.spring.core.host.protocolo.ps9.aplicacion.Aviso;
import com.bbva.jee.arq.spring.core.host.servicios.ErrorConfiguracion;
import com.bbva.jee.arq.spring.core.host.servicios.ErrorPoolConexionesCompleto;
import com.bbva.jee.arq.spring.core.host.servicios.ErrorProtocolo;
import com.bbva.jee.arq.spring.core.host.servicios.ErrorRespuestaHost;
import com.bbva.jee.arq.spring.core.host.servicios.ErrorTimeout;
import com.bbva.jee.arq.spring.core.host.servicios.ErrorTransaccion;
import com.bbva.jee.arq.spring.core.host.servicios.ErrorTransporte;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionPoolConexionesCompleto;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionTimeout;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionTransporte;
import com.bbva.msca.back.dto.LocalizacionClienteDTO;
import com.bbva.msca.back.servicios.host.pee5.FormatoPEM0E5E;
import com.bbva.msca.back.servicios.host.pee5.FormatoPEM0E5S;
import com.bbva.msca.back.servicios.host.pee5.PeticionTransaccionPee5;
import com.bbva.msca.back.servicios.host.pee5.RespuestaTransaccionPee5;
import com.bbva.msca.back.servicios.host.pee5.TransaccionPee5;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class TransaccionPEE5ServiceImpl implements TransaccionPEE5Service{
	
	@Autowired
	private TransaccionPee5 transaccion;	

	@Override
	public ResponseGeneric invocar( LocalizacionClienteDTO clienteDTO ) {
		System.out.println( "== INVOCA SERVICIO LOCALIZACION CLIENTES ==" );
		ResponseGeneric response = new ResponseGeneric();
		PeticionTransaccionPee5 peticion = new PeticionTransaccionPee5();
		List<LocalizacionClienteDTO> lstClientesLocalizados = new ArrayList<LocalizacionClienteDTO>();
		Autorizacion autorizacion = peticion.getCuerpo().getParte(Autorizacion.class);		
		System.out.println( "Usuario : " + ArqSpringContext.getUsuario() );
		System.out.println( "Identificacion cliente : " + ArqSpringContext.getContextoEscritorio().getDatosTecnicos().getIdentificacionCliente()  );
		System.out.println( "Tsec : " + ArqSpringContext.getContextoEscritorio().getDatosTecnicos().getTsec()  );
		System.out.println( "Identificacion cliente : " + ArqSpringContext.getContextoEscritorio().getDatosTecnicos().getUsuarioLogico() );
		
		
		if( autorizacion != null ){
			autorizacion.setUsuario( "CPBAS096" );
			autorizacion.setPassword( "ener1701" );
		}
		else{
			System.out.println( "La autorizacion es nula" );
			autorizacion = new Autorizacion();
			autorizacion.setUsuario( "CPBAS096" );
			autorizacion.setPassword( "ener1701" );			
			peticion.getCuerpo().getPartes().add( autorizacion );
			
		}
		FormatoPEM0E5E formatoEntrada = new FormatoPEM0E5E();		
		System.out.println( "== Datos Recibidos ==" );
		System.out.println( clienteDTO.toString() );
		
		// Seteamos los datos al formato de entrada
		formatoEntrada.setClaiden( clienteDTO.getNumIdentificacion() );
		formatoEntrada.setCodiden( clienteDTO.getTpIdentificacion() );
		if( clienteDTO.getNumeroConsecutivo() != null ){
			formatoEntrada.setConsec( new Integer( clienteDTO.getNumeroConsecutivo() ) );	
		}		
		formatoEntrada.setHomonim( clienteDTO.getHomonimia() );
		formatoEntrada.setIdtipb( "S" );
		formatoEntrada.setMarcpdv( clienteDTO.getMarcaPVD() );
		formatoEntrada.setNombre( clienteDTO.getNbNombre() );
		formatoEntrada.setNumclie( clienteDTO.getNuCliente() );
		formatoEntrada.setNumcta( clienteDTO.getNumeroCuenta() );
		formatoEntrada.setNumtarj( clienteDTO.getNumeroTarjeta() );
		formatoEntrada.setPriape( clienteDTO.getNbPaterno() );
		formatoEntrada.setSegape( clienteDTO.getNbMaterno() );
		peticion.getCuerpo().getPartes().add( formatoEntrada );
		
		try {
			
			System.out.println("\n\n%%%%%%% Invocando transaccion %%%%%%%%\n\n");
			System.out.println( peticion );
			System.out.println("\n\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n");
			RespuestaTransaccionPee5 respuesta = transaccion.invocar(peticion);
			System.out.println("\n\n***** Respuesta Recibida *****\n\n");
			System.out.println( respuesta );
			System.out.println( "\n\n**********************************\n\n" );
			Error error = respuesta.getCuerpo().getParte(Error.class);
			if ( error != null ) throw new ExcepcionRespuestaHost(error.getMessage(), error.getLocalizedMessage());
						
			List<Aviso> avisos = respuesta.getCuerpo().getPartes(Aviso.class);
			for ( Aviso aviso : avisos ) {
				System.out.println("\n\n>>>>> Aviso <<<<<\n\n");
				System.out.println( aviso.getDescripcionAviso() );
				System.out.println("\n\n>>>>>>>>><<<<<<<<<<\n\n");
			}
			CopySalida copySalida = respuesta.getCuerpo().getParte(CopySalida.class);
			if ( copySalida != null ) {
				FormatoPEM0E5S formatoSalida = copySalida.getCopy(FormatoPEM0E5S.class);
				System.out.println("\n\n====== Formato de Salida ======\n\n");
				System.out.println( formatoSalida );
				System.out.println("\n\n=============================\n\n");
			}
			else{
				System.out.println("\n\n====== El formato de salida es nulo ======\n\n");
			}			
			response.setResponse( lstClientesLocalizados );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));			
		} catch ( ExcepcionConfiguracion e ) {
			System.out.println( "\n==========================" );
			System.out.println( "\n Error Configuracion : " );
			e.printStackTrace();
			ErrorConfiguracion info = new ErrorConfiguracion();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));			
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionRespuestaHost e ) {
			System.out.println( "\n==========================" );
			System.out.println( "\n Error Respuesta Host : " );
			e.printStackTrace();			
			ErrorRespuestaHost info = new ErrorRespuestaHost();
			info.setDescripcion(e.getMessage());
			info.setCodigoError(e.getCodigoError());
			info.setDescripcionError(e.getDescripcionError());
			Response respuesta = Response.serverError().entity(info).build();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));			
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionProtocolo e ) {
			System.out.println( "\n==========================" );
			System.out.println( "\n Error Protocolo : " );
			e.printStackTrace();						
			ErrorProtocolo info = new ErrorProtocolo();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));			
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionPoolConexionesCompleto e ) {
			System.out.println( "\n==========================" );
			System.out.println( "\n Error Pool Conexiones Completo : " );
			e.printStackTrace();						
			ErrorPoolConexionesCompleto info = new ErrorPoolConexionesCompleto();
			info.setDescripcion(e.getMessage());
			info.setReferencia(e.getReferencia() == null ? null : e.getReferencia().toString());
			Response respuesta = Response.serverError().entity(info).build();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));			
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionTimeout e ) {
			System.out.println( "\n==========================" );
			System.out.println( "\n Error Timeout : " );
			e.printStackTrace();						
			ErrorTimeout info = new ErrorTimeout();
			info.setDescripcion(e.getMessage());
			info.setReferencia(e.getReferencia() == null ? null : e.getReferencia().toString());
			Response respuesta = Response.serverError().entity(info).build();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));			
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionTransporte e ) {
			System.out.println( "\n==========================" );
			System.out.println( "\n Error Transporte : " );
			e.printStackTrace();						
			ErrorTransporte info = new ErrorTransporte();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));			
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionTransaccion e ) {
			System.out.println( "\n==========================" );
			System.out.println( "\n Error Transaccion : " );
			e.printStackTrace();						
			ErrorTransaccion info = new ErrorTransaccion();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));			
			throw new WebApplicationException(respuesta);
		}  catch (Exception e) {
			System.out.println( "\n==========================" );
			System.out.println( "\n Error General Exception : " );
			e.printStackTrace();						
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		System.out.println( "== FINALIZA INVOCA SERVICIO LOCALIZACION CLIENTES ==" );		
		return response;		
		/*
		try {
			return transaccion.invocar(peticion);
		} catch ( ExcepcionConfiguracion e ) {
			ErrorConfiguracion info = new ErrorConfiguracion();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionRespuestaHost e ) {
			ErrorRespuestaHost info = new ErrorRespuestaHost();
			info.setDescripcion(e.getMessage());
			info.setCodigoError(e.getCodigoError());
			info.setDescripcionError(e.getDescripcionError());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionProtocolo e ) {
			ErrorProtocolo info = new ErrorProtocolo();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionPoolConexionesCompleto e ) {
			ErrorPoolConexionesCompleto info = new ErrorPoolConexionesCompleto();
			info.setDescripcion(e.getMessage());
			info.setReferencia(e.getReferencia() == null ? null : e.getReferencia().toString());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionTimeout e ) {
			ErrorTimeout info = new ErrorTimeout();
			info.setDescripcion(e.getMessage());
			info.setReferencia(e.getReferencia() == null ? null : e.getReferencia().toString());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionTransporte e ) {
			ErrorTransporte info = new ErrorTransporte();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionTransaccion e ) {
			ErrorTransaccion info = new ErrorTransaccion();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		}
		*/	
	}

	@Override
	public ResponseGeneric invocarCache(LocalizacionClienteDTO clienteDTO){
	//public RespuestaTransaccionPee5 invocarCache(PeticionTransaccionPee5 peticion) {
		PeticionTransaccionPee5 peticion = new PeticionTransaccionPee5();
		
		System.out.println( "== Ejecuta la transaccion PEE5 ==" );
		System.out.println( clienteDTO.toString() );
		ResponseGeneric response = new ResponseGeneric();
		try {
			
			response.setResponse( null );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;		
		/*
		PeticionTransaccionPee5 peticion = new PeticionTransaccionPee5();
		try {
			return transaccion.invocarCache(peticion);
		} catch ( ExcepcionConfiguracion e ) {
			ErrorConfiguracion info = new ErrorConfiguracion();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionRespuestaHost e ) {
			ErrorRespuestaHost info = new ErrorRespuestaHost();
			info.setDescripcion(e.getMessage());
			info.setCodigoError(e.getCodigoError());
			info.setDescripcionError(e.getDescripcionError());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionProtocolo e ) {
			ErrorProtocolo info = new ErrorProtocolo();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionPoolConexionesCompleto e ) {
			ErrorPoolConexionesCompleto info = new ErrorPoolConexionesCompleto();
			info.setDescripcion(e.getMessage());
			info.setReferencia(e.getReferencia() == null ? null : e.getReferencia().toString());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionTimeout e ) {
			ErrorTimeout info = new ErrorTimeout();
			info.setDescripcion(e.getMessage());
			info.setReferencia(e.getReferencia() == null ? null : e.getReferencia().toString());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionTransporte e ) {
			ErrorTransporte info = new ErrorTransporte();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		} catch ( ExcepcionTransaccion e ) {
			ErrorTransaccion info = new ErrorTransaccion();
			info.setDescripcion(e.getMessage());
			Response respuesta = Response.serverError().entity(info).build();
			throw new WebApplicationException(respuesta);
		}
		*/
	}

	@Override
	public void vaciarCache() {
		transaccion.vaciarCache();
	}
}
