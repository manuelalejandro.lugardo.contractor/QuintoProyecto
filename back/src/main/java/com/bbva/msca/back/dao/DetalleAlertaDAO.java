package com.bbva.msca.back.dao;

import java.util.Date;
import com.bbva.msca.back.dto.DetalleAlertaDTO;

public class DetalleAlertaDAO {
	
	public static DetalleAlertaDTO getDetalleAlerta(){
		
		DetalleAlertaDTO detalleAlertaDTO = new DetalleAlertaDTO();
		
		detalleAlertaDTO.setNuFolio(123);
		detalleAlertaDTO.setNuCliente(123);
		detalleAlertaDTO.setNbCliente("");
		detalleAlertaDTO.setNuCuenta(123);
		detalleAlertaDTO.setFhAlerta(new Date());
		detalleAlertaDTO.setFhEnvio(new Date());
		detalleAlertaDTO.setFhVencimientoLiberacion(new Date());
		detalleAlertaDTO.setNbOrigen("");
		detalleAlertaDTO.setNbTipologia("");
		detalleAlertaDTO.setNbSegmento("");
		detalleAlertaDTO.setImImporte(123);
		detalleAlertaDTO.setNbDivisa("");
		
		return detalleAlertaDTO;
	}

}
