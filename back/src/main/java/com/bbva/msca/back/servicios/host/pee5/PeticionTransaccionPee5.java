package com.bbva.msca.back.servicios.host.pee5;

import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>PEE5</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPee5</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPee5</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> config_ps9</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEE5.QGDTCCT.txt
 * PEE5LOCALIZACION DE CLIENTES           PE        PE2C00E5MBDPEPO PEM0E5E             PEE5  NN3000CNNNNN    SSTN    C   NNNNNNNN  NN                2009-05-20IDBEX61 2015-07-3011.14.09XMCB099 2009-05-20-12.20.01.639671IDBEX61 0001-01-010001-01-01
 * FICHERO: PEE5.QGDTFDF.txt
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�01�00016�IDTIPB �TIPO BUSQUEDA S/N/A �A�001�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�02�00020�NUMCLIE�NUMERO CLIENTE      �A�008�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�03�00031�CODIDEN�TIPO IDENTIFICACION �A�001�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�04�00035�CLAIDEN�NUM. IDENTIFICACION �A�010�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�05�00048�RFC    �REG. FED. CONT.     �A�010�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�06�00061�HOMONIM�HOMONIMIA           �A�004�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�07�00068�NOMBRE �NOMBRE FISICAS      �A�020�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�08�00091�PRIAPE �PRIMER APELLIDO     �A�020�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�09�00114�SEGAPE �SEGUNDO APELLIDO    �A�020�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�10�00137�NOMBREM�NOMBRE MORALES      �A�060�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�11�00200�NUMCTA �NUMERO CUENTA       �A�010�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�12�00213�NUMTARJ�NUMERO TARJETA      �A�016�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�13�00232�CONSEC �NUM. CONSECUTIVO    �N�004�0�O�        �
 * PEM0E5E �ENT. LOCALIZACION DE CLIENTES �E�14�00239�14�00239�MARCPDV�MARCA PARA PDV      �A�001�0�O�        �
 * PEM0E5S �SAL. LOCALIZACION DE CLIENTES �S�01�00095�01�00016�CONTENI�CONTENIDO SALIDA    �A�080�0�S�        �
 * FICHERO: PEE5.QGDTFDX.txt
 * PEE5PEM0E5S PENC0E5SPE2C00E51S                             IDBEX61 2009-05-20-12.28.17.738205IDBEX61 2009-05-20-12.28.17.738220
</pre></code>
 * 
 * @see RespuestaTransaccionPee5
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PEE5",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "config_ps9",
	respuesta = RespuestaTransaccionPee5.class
)
@Multiformato(formatos = {FormatoPEM0E5E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionPee5 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}