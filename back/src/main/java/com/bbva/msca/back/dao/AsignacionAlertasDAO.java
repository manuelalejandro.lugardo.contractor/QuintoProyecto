package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.List;
import com.bbva.msca.back.dto.AsignacionAlertasDTO;

public class AsignacionAlertasDAO {
	
	public List<AsignacionAlertasDTO> getAsignacionCasos(){
		
		 List<AsignacionAlertasDTO> lstAsignacionAlertasDTO = new ArrayList<AsignacionAlertasDTO>();
		
		 AsignacionAlertasDTO asignacionAlertasDTO = null;
		
			for(int i=0;i<10;i++){
			
				asignacionAlertasDTO = new AsignacionAlertasDTO();
				
				asignacionAlertasDTO.setStSelected(true);
				asignacionAlertasDTO.setCdConsultor(5);
				asignacionAlertasDTO.setNbConsultor("");
				asignacionAlertasDTO.setNuAlertasNormal(5);
				asignacionAlertasDTO.setNuAlertasUrgente(5);
				asignacionAlertasDTO.setNuAlertasAnalisis(5);
				asignacionAlertasDTO.setNuAlertasSupervision(5);
				
				lstAsignacionAlertasDTO.add(asignacionAlertasDTO);
			
			}
			return lstAsignacionAlertasDTO;
		}

}
