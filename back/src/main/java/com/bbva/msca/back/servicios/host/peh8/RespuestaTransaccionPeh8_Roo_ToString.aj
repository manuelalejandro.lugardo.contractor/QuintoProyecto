// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.servicios.host.peh8;

import java.lang.String;

privileged aspect RespuestaTransaccionPeh8_Roo_ToString {
    
    public String RespuestaTransaccionPeh8.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CodigoControl: ").append(getCodigoControl()).append(", ");
        sb.append("CodigoRetorno: ").append(getCodigoRetorno()).append(", ");
        sb.append("Cuerpo: ").append(getCuerpo()).append(", ");
        sb.append("LongitudMensaje: ").append(getLongitudMensaje()).append(", ");
        sb.append("Secuencia: ").append(getSecuencia());
        return sb.toString();
    }
    
}
