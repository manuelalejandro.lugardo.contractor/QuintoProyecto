package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class ConsultaCasosDTO extends CasosAsignadosDTO{
	
	private Date fhRevision;
	private String nbEstatus;

	private BigDecimal numCasoAC;
	private BigDecimal numAlertasAC;
	
	
	public Date getFhRevision() {
		return fhRevision;
	}
	public void setFhRevision(Date fhRevision) {
		this.fhRevision = fhRevision;
	}
	public String getNbEstatus() {
		return nbEstatus;
	}
	public void setNbEstatus(String nbEstatus) {
		this.nbEstatus = nbEstatus;
	}
	public BigDecimal getNumCasoAC() {
		return numCasoAC;
	}
	public void setNumCasoAC(BigDecimal numCasoAC) {
		this.numCasoAC = numCasoAC;
	}
	public BigDecimal getNumAlertasAC() {
		return numAlertasAC;
	}
	public void setNumAlertasAC(BigDecimal numAlertasAC) {
		this.numAlertasAC = numAlertasAC;
	}
	
}
