package com.bbva.msca.back.srv;

import com.bbva.msca.back.util.ResponseGeneric;

import java.math.BigDecimal;
import java.util.List;
import com.bbva.msca.back.dao.ConsultaCasosDAO;
import com.bbva.msca.back.dto.AlertaDTO;
import com.bbva.msca.back.dto.ParametrosBusquedaCasoDTO;
import com.bbva.msca.back.util.MensajesI18n;

public class ConsultaCasosServiceImpl implements ConsultaCasosService{
	
	@Override
	public ResponseGeneric consultarCasosSupervisorFiltro(BigDecimal cdSupervisor, ParametrosBusquedaCasoDTO parametrosBusquedaCasoDTO ) {
		ResponseGeneric response = new ResponseGeneric();
		ConsultaCasosDAO consultaCasosDAO = new ConsultaCasosDAO();
		
		try{
		List<AlertaDTO> lstAlertasDTO = consultaCasosDAO.getConsultaCasosSupervisor(parametrosBusquedaCasoDTO, cdSupervisor);
		response.setResponse(lstAlertasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));	
		}
		return response;
		
	}
	
	@Override
	public ResponseGeneric consultarCasosSupervisor(BigDecimal cdSupervisor) {
		ResponseGeneric response = new ResponseGeneric();
		ConsultaCasosDAO consultaCasosDAO = new ConsultaCasosDAO();
		
		try{
		List<AlertaDTO> lstAlertasDTO = consultaCasosDAO.getConsultaCasosSupervisor(cdSupervisor);
		response.setResponse(lstAlertasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			
		}
		return response;
		
	}
	
	@Override
	public ResponseGeneric consultarCasosConsultorFiltro(BigDecimal cdConsultor, ParametrosBusquedaCasoDTO parametrosBusquedaCasoDTO ) {
		ResponseGeneric response = new ResponseGeneric();
		ConsultaCasosDAO consultaCasosDAO = new ConsultaCasosDAO();
		
		try{
		List<AlertaDTO> lstAlertasDTO = consultaCasosDAO.getConsultaCasosConsultor(parametrosBusquedaCasoDTO, cdConsultor);
		response.setResponse(lstAlertasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));	
		}
		return response;
		
	}
	
	@Override
	public ResponseGeneric consultarCasosConsultor(BigDecimal cdConsultor) {
		ResponseGeneric response = new ResponseGeneric();
		ConsultaCasosDAO consultaCasosDAO = new ConsultaCasosDAO();
		
		try{
		List<AlertaDTO> lstAlertasDTO = consultaCasosDAO.getConsultaCasosConsultor(cdConsultor);
		response.setResponse(lstAlertasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			
		}
		return response;
		
	}

}
