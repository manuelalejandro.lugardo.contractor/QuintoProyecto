package com.bbva.msca.back.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import com.bbva.msca.back.dto.TransaccionClienteDTO;
import com.bbva.msca.back.dto.TransaccionClienteTdcDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class TransaccionClienteDAO {
	
	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();

		
		@SuppressWarnings({ "unchecked"})
		public  TransaccionClienteDTO getTransaccionesCliente(String cdSistema, String nuFolioAlerta) throws Exception{
			TransaccionClienteDTO transaccionClienteDTO = new TransaccionClienteDTO();
			List<TransaccionClienteDTO> lstResultado = null;
			//DecimalFormat formateador = new DecimalFormat("$"+"###,###,###,###.##");
			
				 Query query = em.createNativeQuery(Consultas.getConsulta("getTransaccionalidadParteDos"));
				 query.setParameter(1,cdSistema);
				 query.setParameter(2,nuFolioAlerta);
				 query.unwrap(org.hibernate.SQLQuery.class)
				 					
					.addScalar("operativaPesosTDC")
					.addScalar("montoTotalDepositosTDC")
					.addScalar("localidadOperarTDC")
				.setResultTransformer(Transformers.aliasToBean(TransaccionClienteDTO.class));
				 lstResultado = (List<TransaccionClienteDTO>)query.getResultList();
				
				 if( !lstResultado.isEmpty() ){
				     transaccionClienteDTO = lstResultado.get( 0 );
				}
				
					 
		
				
				
				//get   tdc
				Query queryNSC = em.createNativeQuery(Consultas.getConsulta("getTransaccionalidadParteUno"));
				
				queryNSC.setParameter(1,cdSistema);
				queryNSC.setParameter(2,nuFolioAlerta);
				queryNSC.setParameter(3,cdSistema);
				queryNSC.setParameter(4,nuFolioAlerta);
				queryNSC.setParameter(5,cdSistema);
				queryNSC.setParameter(6,nuFolioAlerta);
				queryNSC.setParameter(7,cdSistema);
				queryNSC.setParameter(8,nuFolioAlerta);
				
				queryNSC.unwrap(org.hibernate.SQLQuery.class)
	
				.addScalar("instrumentosMonetariosTDC")
				.addScalar("importeTDC")
				.addScalar("noOperacionesTDC", StringType.INSTANCE)
				.addScalar("origenTransTDC")
				.addScalar("destinoTransTDC")
				
				.setResultTransformer(Transformers.aliasToBean(TransaccionClienteTdcDTO.class));
				
				transaccionClienteDTO.setTablaTDC((List<TransaccionClienteTdcDTO>)queryNSC.getResultList());

							 
		return transaccionClienteDTO;		
	}

}
