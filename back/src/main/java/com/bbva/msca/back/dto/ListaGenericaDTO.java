package com.bbva.msca.back.dto;

public class ListaGenericaDTO {
	private String cdValor;
	private String nbValor;
	public String getCdValor() {
		return cdValor;
	}
	public void setCdValor(String cdValor) {
		this.cdValor = cdValor;
	}
	public String getNbValor() {
		return nbValor;
	}
	public void setNbValor(String nbValor) {
		this.nbValor = nbValor;
	}
	
}
