package com.bbva.msca.back.dto;

public class DescripcionTransaccionDTO {
	
	private String nbDesTransaccion;

	public String getNbDesTransaccion() {
		return nbDesTransaccion;
	}

	public void setNbDesTransaccion(String nbDesTransaccion) {
		this.nbDesTransaccion = nbDesTransaccion;
	}
	
	

}
