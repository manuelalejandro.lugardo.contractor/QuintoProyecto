package com.bbva.msca.back.dto;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ReportConocimientoCliente {

	// Seccion Datos Generales del Cliente
	private String nbRegSimplificadoDC;
	private String nbActEconomicaDC;
	private String nbOcupacionDC;
	// Seccion Transaccionalidad Declarada por el cliente
	private JRBeanCollectionDataSource lstTransaccionesCliente;
	private String operativaPesosTDC;
	private String montoTotalDepositosTDC;
	private String localidadOperarTDC;
	private String operativaDolaresTDC;
	// Seccion Cliente-PEP
	private String puestoPEP;
	private String nombrePEP;
	private String parentPEP;
	private String nombrePersRel;
	// Seccion Factores de Riesgo Cliente
	private String paisRecidenciaCAR;
	private String nacionalidadCAR;
	private String justCuentaCAR;
	private String procedenciaRecCAR;
	private String fuenteIngresoCAR;
	private String fhCamAltBajRiesgoCAR;
	private String fhAltAltRiesgoCAR;
	private String fhUltMovAltBajCAR;
	private String nbOrigenBajaCAR;
	private String nbOrigenAltaCAR;
	private String indRiesgoCAR;
	private String indRiesgoActCAR;
	// Seccion Factores de Riesgo RBA
	private String nuTipClienteRBA;
	private String nuActEconomicaRBA;
	private String nuOcupacionRBA;
	private String nuRegPaisRBA;
	private String nuProServiciosRBA;
	private String nuOpeHabitualRBA;
	private String nuIntervinientesRBA;
	private String nuSucursalRBA;
	private String nuCanCaptacionRBA;
	private String nuAntiguedadPuntajeRBA;
	private String nuEdadRBA;
	private String nuTotalRBA;
	private String nbMotRiesgoRBA;
	
	// Seccion - Informacion de Accionistas y Estructura de la empresa
	private JRBeanCollectionDataSource lstAccionistas;
	
	// Seccion - Otros Productos del cliente reportado
	private JRBeanCollectionDataSource lstOPTarjetaCredito;
	private JRBeanCollectionDataSource lstOPPatrimonial;
	// Seccion - Cuentas del cliente Reportado
	private JRBeanCollectionDataSource lstCuentasClienteRep;
	// Seccion - Descripcion de la transaccion
	private String descripcionTransaccion;
	// Seccion - Opinion del funcionario
	private JRBeanCollectionDataSource lstOpinionFuncionario;
	// Seccion - Mecanica Operacional de la Cuenta
	private JRBeanCollectionDataSource lstMecOpResumen;
	private JRBeanCollectionDataSource lstMecOpMesUno;
	private JRBeanCollectionDataSource lstMecOpMesDos;
	private JRBeanCollectionDataSource lstMecOpMesTres;
	private JRBeanCollectionDataSource lstMecOpMesCuatro;
	// Seccion - Operaciones Medianas y Relevantes
	private JRBeanCollectionDataSource lstOperacionesMedianas;
	private JRBeanCollectionDataSource lstOperacionesRelevantes;
	// Seccion - Transferencias Internacionales
	private JRBeanCollectionDataSource lstTransEnviadas;
	private JRBeanCollectionDataSource lstTransRecibidas;
	// Seccion - Oficios Listas y Notas Periodisticas
	private String oficiosListasNotas;
	
	

	public String getNbRegSimplificadoDC() {
		return nbRegSimplificadoDC;
	}

	public void setNbRegSimplificadoDC(String nbRegSimplificadoDC) {
		this.nbRegSimplificadoDC = nbRegSimplificadoDC;
	}

	public String getNbActEconomicaDC() {
		return nbActEconomicaDC;
	}

	public void setNbActEconomicaDC(String nbActEconomicaDC) {
		this.nbActEconomicaDC = nbActEconomicaDC;
	}

	public String getNbOcupacionDC() {
		return nbOcupacionDC;
	}

	public void setNbOcupacionDC(String nbOcupacionDC) {
		this.nbOcupacionDC = nbOcupacionDC;
	}

	public JRBeanCollectionDataSource getLstTransaccionesCliente() {
		return lstTransaccionesCliente;
	}

	public void setLstTransaccionesCliente(
			JRBeanCollectionDataSource lstTransaccionesCliente) {
		this.lstTransaccionesCliente = lstTransaccionesCliente;
	}

	public String getOperativaPesosTDC() {
		return operativaPesosTDC;
	}

	public void setOperativaPesosTDC(String operativaPesosTDC) {
		this.operativaPesosTDC = operativaPesosTDC;
	}

	public String getMontoTotalDepositosTDC() {
		return montoTotalDepositosTDC;
	}

	public void setMontoTotalDepositosTDC(String montoTotalDepositosTDC) {
		this.montoTotalDepositosTDC = montoTotalDepositosTDC;
	}

	public String getLocalidadOperarTDC() {
		return localidadOperarTDC;
	}

	public void setLocalidadOperarTDC(String localidadOperarTDC) {
		this.localidadOperarTDC = localidadOperarTDC;
	}

	public String getOperativaDolaresTDC() {
		return operativaDolaresTDC;
	}

	public void setOperativaDolaresTDC(String operativaDolaresTDC) {
		this.operativaDolaresTDC = operativaDolaresTDC;
	}

	public String getPuestoPEP() {
		return puestoPEP;
	}

	public void setPuestoPEP(String puestoPEP) {
		this.puestoPEP = puestoPEP;
	}

	public String getNombrePEP() {
		return nombrePEP;
	}

	public void setNombrePEP(String nombrePEP) {
		this.nombrePEP = nombrePEP;
	}

	public String getParentPEP() {
		return parentPEP;
	}

	public void setParentPEP(String parentPEP) {
		this.parentPEP = parentPEP;
	}

	public String getNombrePersRel() {
		return nombrePersRel;
	}

	public void setNombrePersRel(String nombrePersRel) {
		this.nombrePersRel = nombrePersRel;
	}

	public String getPaisRecidenciaCAR() {
		return paisRecidenciaCAR;
	}

	public void setPaisRecidenciaCAR(String paisRecidenciaCAR) {
		this.paisRecidenciaCAR = paisRecidenciaCAR;
	}

	public String getNacionalidadCAR() {
		return nacionalidadCAR;
	}

	public void setNacionalidadCAR(String nacionalidadCAR) {
		this.nacionalidadCAR = nacionalidadCAR;
	}

	public String getJustCuentaCAR() {
		return justCuentaCAR;
	}

	public void setJustCuentaCAR(String justCuentaCAR) {
		this.justCuentaCAR = justCuentaCAR;
	}

	public String getProcedenciaRecCAR() {
		return procedenciaRecCAR;
	}

	public void setProcedenciaRecCAR(String procedenciaRecCAR) {
		this.procedenciaRecCAR = procedenciaRecCAR;
	}

	public String getFuenteIngresoCAR() {
		return fuenteIngresoCAR;
	}

	public void setFuenteIngresoCAR(String fuenteIngresoCAR) {
		this.fuenteIngresoCAR = fuenteIngresoCAR;
	}

	public String getFhCamAltBajRiesgoCAR() {
		return fhCamAltBajRiesgoCAR;
	}

	public void setFhCamAltBajRiesgoCAR(String fhCamAltBajRiesgoCAR) {
		this.fhCamAltBajRiesgoCAR = fhCamAltBajRiesgoCAR;
	}

	public String getFhAltAltRiesgoCAR() {
		return fhAltAltRiesgoCAR;
	}

	public void setFhAltAltRiesgoCAR(String fhAltAltRiesgoCAR) {
		this.fhAltAltRiesgoCAR = fhAltAltRiesgoCAR;
	}

	public String getFhUltMovAltBajCAR() {
		return fhUltMovAltBajCAR;
	}

	public void setFhUltMovAltBajCAR(String fhUltMovAltBajCAR) {
		this.fhUltMovAltBajCAR = fhUltMovAltBajCAR;
	}

	public String getNbOrigenBajaCAR() {
		return nbOrigenBajaCAR;
	}

	public void setNbOrigenBajaCAR(String nbOrigenBajaCAR) {
		this.nbOrigenBajaCAR = nbOrigenBajaCAR;
	}

	public String getNbOrigenAltaCAR() {
		return nbOrigenAltaCAR;
	}

	public void setNbOrigenAltaCAR(String nbOrigenAltaCAR) {
		this.nbOrigenAltaCAR = nbOrigenAltaCAR;
	}

	public String getIndRiesgoCAR() {
		return indRiesgoCAR;
	}

	public void setIndRiesgoCAR(String indRiesgoCAR) {
		this.indRiesgoCAR = indRiesgoCAR;
	}

	public String getIndRiesgoActCAR() {
		return indRiesgoActCAR;
	}

	public void setIndRiesgoActCAR(String indRiesgoActCAR) {
		this.indRiesgoActCAR = indRiesgoActCAR;
	}

	public String getNuTipClienteRBA() {
		return nuTipClienteRBA;
	}

	public void setNuTipClienteRBA(String nuTipClienteRBA) {
		this.nuTipClienteRBA = nuTipClienteRBA;
	}

	public String getNuActEconomicaRBA() {
		return nuActEconomicaRBA;
	}

	public void setNuActEconomicaRBA(String nuActEconomicaRBA) {
		this.nuActEconomicaRBA = nuActEconomicaRBA;
	}

	public String getNuOcupacionRBA() {
		return nuOcupacionRBA;
	}

	public void setNuOcupacionRBA(String nuOcupacionRBA) {
		this.nuOcupacionRBA = nuOcupacionRBA;
	}

	public String getNuRegPaisRBA() {
		return nuRegPaisRBA;
	}

	public void setNuRegPaisRBA(String nuRegPaisRBA) {
		this.nuRegPaisRBA = nuRegPaisRBA;
	}

	public String getNuProServiciosRBA() {
		return nuProServiciosRBA;
	}

	public void setNuProServiciosRBA(String nuProServiciosRBA) {
		this.nuProServiciosRBA = nuProServiciosRBA;
	}

	public String getNuOpeHabitualRBA() {
		return nuOpeHabitualRBA;
	}

	public void setNuOpeHabitualRBA(String nuOpeHabitualRBA) {
		this.nuOpeHabitualRBA = nuOpeHabitualRBA;
	}

	public String getNuIntervinientesRBA() {
		return nuIntervinientesRBA;
	}

	public void setNuIntervinientesRBA(String nuIntervinientesRBA) {
		this.nuIntervinientesRBA = nuIntervinientesRBA;
	}

	public String getNuSucursalRBA() {
		return nuSucursalRBA;
	}

	public void setNuSucursalRBA(String nuSucursalRBA) {
		this.nuSucursalRBA = nuSucursalRBA;
	}

	public String getNuCanCaptacionRBA() {
		return nuCanCaptacionRBA;
	}

	public void setNuCanCaptacionRBA(String nuCanCaptacionRBA) {
		this.nuCanCaptacionRBA = nuCanCaptacionRBA;
	}

	public String getNuAntiguedadPuntajeRBA() {
		return nuAntiguedadPuntajeRBA;
	}

	public void setNuAntiguedadPuntajeRBA(String nuAntiguedadPuntajeRBA) {
		this.nuAntiguedadPuntajeRBA = nuAntiguedadPuntajeRBA;
	}

	public String getNuEdadRBA() {
		return nuEdadRBA;
	}

	public void setNuEdadRBA(String nuEdadRBA) {
		this.nuEdadRBA = nuEdadRBA;
	}

	public String getNuTotalRBA() {
		return nuTotalRBA;
	}

	public void setNuTotalRBA(String nuTotalRBA) {
		this.nuTotalRBA = nuTotalRBA;
	}

	public String getNbMotRiesgoRBA() {
		return nbMotRiesgoRBA;
	}

	public void setNbMotRiesgoRBA(String nbMotRiesgoRBA) {
		this.nbMotRiesgoRBA = nbMotRiesgoRBA;
	}

	public JRBeanCollectionDataSource getLstAccionistas() {
		return lstAccionistas;
	}

	public void setLstAccionistas(JRBeanCollectionDataSource lstAccionistas) {
		this.lstAccionistas = lstAccionistas;
	}

	public JRBeanCollectionDataSource getLstOPTarjetaCredito() {
		return lstOPTarjetaCredito;
	}

	public void setLstOPTarjetaCredito(
			JRBeanCollectionDataSource lstOPTarjetaCredito) {
		this.lstOPTarjetaCredito = lstOPTarjetaCredito;
	}

	public JRBeanCollectionDataSource getLstOPPatrimonial() {
		return lstOPPatrimonial;
	}

	public void setLstOPPatrimonial(JRBeanCollectionDataSource lstOPPatrimonial) {
		this.lstOPPatrimonial = lstOPPatrimonial;
	}

	public JRBeanCollectionDataSource getLstCuentasClienteRep() {
		return lstCuentasClienteRep;
	}

	public void setLstCuentasClienteRep(
			JRBeanCollectionDataSource lstCuentasClienteRep) {
		this.lstCuentasClienteRep = lstCuentasClienteRep;
	}

	public String getDescripcionTransaccion() {
		return descripcionTransaccion;
	}

	public void setDescripcionTransaccion(String descripcionTransaccion) {
		this.descripcionTransaccion = descripcionTransaccion;
	}

	public JRBeanCollectionDataSource getLstOpinionFuncionario() {
		return lstOpinionFuncionario;
	}

	public void setLstOpinionFuncionario(
			JRBeanCollectionDataSource lstOpinionFuncionario) {
		this.lstOpinionFuncionario = lstOpinionFuncionario;
	}

	public String getOficiosListasNotas() {
		return oficiosListasNotas;
	}

	public void setOficiosListasNotas(String oficiosListasNotas) {
		this.oficiosListasNotas = oficiosListasNotas;
	}

	public JRBeanCollectionDataSource getLstMecOpResumen() {
		return lstMecOpResumen;
	}

	public void setLstMecOpResumen(JRBeanCollectionDataSource lstMecOpResumen) {
		this.lstMecOpResumen = lstMecOpResumen;
	}

	public JRBeanCollectionDataSource getLstMecOpMesUno() {
		return lstMecOpMesUno;
	}

	public void setLstMecOpMesUno(JRBeanCollectionDataSource lstMecOpMesUno) {
		this.lstMecOpMesUno = lstMecOpMesUno;
	}

	public JRBeanCollectionDataSource getLstMecOpMesDos() {
		return lstMecOpMesDos;
	}

	public void setLstMecOpMesDos(JRBeanCollectionDataSource lstMecOpMesDos) {
		this.lstMecOpMesDos = lstMecOpMesDos;
	}

	public JRBeanCollectionDataSource getLstMecOpMesTres() {
		return lstMecOpMesTres;
	}

	public void setLstMecOpMesTres(JRBeanCollectionDataSource lstMecOpMesTres) {
		this.lstMecOpMesTres = lstMecOpMesTres;
	}

	public JRBeanCollectionDataSource getLstMecOpMesCuatro() {
		return lstMecOpMesCuatro;
	}

	public void setLstMecOpMesCuatro(JRBeanCollectionDataSource lstMecOpMesCuatro) {
		this.lstMecOpMesCuatro = lstMecOpMesCuatro;
	}

	public JRBeanCollectionDataSource getLstOperacionesMedianas() {
		return lstOperacionesMedianas;
	}

	public void setLstOperacionesMedianas(
			JRBeanCollectionDataSource lstOperacionesMedianas) {
		this.lstOperacionesMedianas = lstOperacionesMedianas;
	}

	public JRBeanCollectionDataSource getLstOperacionesRelevantes() {
		return lstOperacionesRelevantes;
	}

	public void setLstOperacionesRelevantes(
			JRBeanCollectionDataSource lstOperacionesRelevantes) {
		this.lstOperacionesRelevantes = lstOperacionesRelevantes;
	}

	public JRBeanCollectionDataSource getLstTransEnviadas() {
		return lstTransEnviadas;
	}

	public void setLstTransEnviadas(JRBeanCollectionDataSource lstTransEnviadas) {
		this.lstTransEnviadas = lstTransEnviadas;
	}

	public JRBeanCollectionDataSource getLstTransRecibidas() {
		return lstTransRecibidas;
	}

	public void setLstTransRecibidas(JRBeanCollectionDataSource lstTransRecibidas) {
		this.lstTransRecibidas = lstTransRecibidas;
	}
	

}
