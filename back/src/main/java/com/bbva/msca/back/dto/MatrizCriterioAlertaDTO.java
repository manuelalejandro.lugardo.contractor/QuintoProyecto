package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class MatrizCriterioAlertaDTO {
	
	private BigDecimal cdCriterio;
	private String nbCriterio;
	private BigDecimal nuPuntaje;
	
	public BigDecimal getCdCriterio() {
		return cdCriterio;
	}
	public void setCdCriterio(BigDecimal cdCriterio) {
		this.cdCriterio = cdCriterio;
	}
	public String getNbCriterio() {
		return nbCriterio;
	}
	public void setNbCriterio(String nbCriterio) {
		this.nbCriterio = nbCriterio;
	}
	public BigDecimal getNuPuntaje() {
		return nuPuntaje;
	}
	public void setNuPuntaje(BigDecimal nuPuntaje) {
		this.nuPuntaje = nuPuntaje;
	}
}
