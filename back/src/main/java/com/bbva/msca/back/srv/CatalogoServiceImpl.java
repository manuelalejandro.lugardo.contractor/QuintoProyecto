package com.bbva.msca.back.srv;

import java.math.BigDecimal;
import java.util.List;

import com.bbva.msca.back.dao.CatalogoDAO;
import com.bbva.msca.back.dto.CatalogoDTO;
import com.bbva.msca.back.dto.ListaGenericaDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class CatalogoServiceImpl implements CatalogoService {

	@Override
	public ResponseGeneric consultarDetCatalogoPorCdCatalogo(BigDecimal cdCatalogo) {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<CatalogoDTO> lstCatalogoDTO = catalogoDAO.getDetCatalogoPorCdCatalogo(cdCatalogo);
			response.setResponse(lstCatalogoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric consultarCatalogoSegmento() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoSegmento();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoTipoCuenta() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoTipoCuenta();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoTipoOperacionMonetaria() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoTipoOperacionMonetaria();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoInstrumentoMonetario() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoInstrumentoMonetario();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoMoneda() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoMoneda();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoOperacionInusual(String cOperacion) {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoOperacionInusual(cOperacion);
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric consultarCatalogoTipoPersona() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoTipoPersona();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric consultarCatalogoTipologias() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoTipologia();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoTipologiasGerencia(Integer cdGerencia) {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoTipologiaGerencia(cdGerencia, Constantes.CD_ST_SIS_A) ;
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric consultarCatalogoSesion() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoSesion();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric consultarCatalogoEscenarios() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoEscenarios();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoSector() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoSectores();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoCriterio() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoCriterios();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoPerfil() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoPerfiles();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCatalogoGerencia() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoGerencias();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarUsuariosPerfil(Integer cdPerfil) {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		List<ListaGenericaDTO> lstUsuarioDTO = null;
		try{
			lstUsuarioDTO = catalogoDAO.getCatalogoUsuariosPerfil(cdPerfil);
			response.setResponse(lstUsuarioDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric consultarUsuariosPerfilSupervisor(Integer cdSupervisor, Integer cdPerfil) {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		List<ListaGenericaDTO> lstUsuarioDTO = null;
		try{
			lstUsuarioDTO = catalogoDAO.getCatalogoUsuariosPerfilSupervisor(cdSupervisor, cdPerfil);
			response.setResponse(lstUsuarioDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric consultarTipoPrioridad() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		List<ListaGenericaDTO> lstUsuarioDTO = null;
		try{
			lstUsuarioDTO = catalogoDAO.getCatalogoTipoPrioridad();
			response.setResponse(lstUsuarioDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric consultarCatalogoDictamenPreliminar() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoDictamenPreliminar();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	public ResponseGeneric consultarCatalogoDictamenFinal() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoDictamenFinal();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric consultarCatalogoTipologiaCAR() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoTipologiaCAR();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric consultarCatalogoRespuesta() {
		ResponseGeneric response = new ResponseGeneric();
		CatalogoDAO catalogoDAO = new CatalogoDAO();
		
		try{
			List<ListaGenericaDTO> lstListaGenericaDTO = catalogoDAO.getCatalogoRespuesta();
			response.setResponse(lstListaGenericaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}


}
