package com.bbva.msca.back.dominio;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity( table = "TSCA047_ASIG_TPLG", schema = "GORAPR", versionField = "")

public class Tsca047AsigTplg {
	
	@Id
	@NotNull
	@SequenceGenerator(name = "SQ_TSCA047", sequenceName = "SQ_TSCA047", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TSCA047")
    @Column(name = "CD_ASIG_TIPOLOGIA")
    private BigDecimal cdAsigTipologia;
	
	@Column(name="CD_CONSULTOR")
	String cdUsrConsultor;
	
	@Column(name="CD_ADMINISTRADOR")
	String cdUsrAlta;
	
	
}
