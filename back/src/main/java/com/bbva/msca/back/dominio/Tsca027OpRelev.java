package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import com.bbva.msca.back.dominio.Tsca027OpRelevPK;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(versionField = "", table = "TSCA027_OP_RELEV", schema = "GORAPR", identifierType = Tsca027OpRelevPK.class)
public class Tsca027OpRelev {
}
