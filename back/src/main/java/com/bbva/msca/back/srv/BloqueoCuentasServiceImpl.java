package com.bbva.msca.back.srv;
import java.util.List;

import com.bbva.msca.back.dao.BloqueoCuentasDAO;
import com.bbva.msca.back.dto.BloqueoCuentasDTO;
import com.bbva.msca.back.dto.BusquedaBloqueoCuentasDTO;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class BloqueoCuentasServiceImpl implements BloqueoCuentasService{
	
	@Override
	public ResponseGeneric consultarBloqueoCuentasFiltro(BusquedaBloqueoCuentasDTO busquedaBloqueoCuentasDTO ) {
		ResponseGeneric response = new ResponseGeneric();
		BloqueoCuentasDAO bloqueoCuentasDAO = new BloqueoCuentasDAO();
		
		try{
		List<BloqueoCuentasDTO> lstBloqueoCuentasDTO = bloqueoCuentasDAO.getConsultaBloqueoCuentasFiltro(busquedaBloqueoCuentasDTO);
		response.setResponse(lstBloqueoCuentasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));	
		}
		return response;
		
	}
	
	@Override
	public ResponseGeneric consultarBloqueoCuentas() {
		ResponseGeneric response = new ResponseGeneric();
		BloqueoCuentasDAO bloqueoCuentasDAO = new BloqueoCuentasDAO();
		
		try{
		List<BloqueoCuentasDTO> lstBloqueoCuentasDTO = bloqueoCuentasDAO.getConsultaBloqueoCuentas();
		response.setResponse(lstBloqueoCuentasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			
		}
		return response;
		
	}
}
