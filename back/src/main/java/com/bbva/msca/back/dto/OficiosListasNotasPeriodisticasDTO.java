package com.bbva.msca.back.dto;

public class OficiosListasNotasPeriodisticasDTO {
	
	private String txtOficiosNotasPer;

	public String getTxtOficiosNotasPer() {
		return txtOficiosNotasPer;
	}

	public void setTxtOficiosNotasPer(String txtOficiosNotasPer) {
		this.txtOficiosNotasPer = txtOficiosNotasPer;
	}

}
