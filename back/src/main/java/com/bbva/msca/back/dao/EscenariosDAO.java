package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import com.bbva.msca.back.dto.EscenariosDTO;
import com.bbva.msca.back.util.BdSac;

public class EscenariosDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	public List<EscenariosDTO> getEscenarios() throws Exception{
		
		 List<EscenariosDTO> lstEscenariosDTO = new ArrayList<EscenariosDTO>();
		
		 EscenariosDTO escenariosDTO = null;
		
			for(int i=0;i<10;i++){
			
				escenariosDTO = new EscenariosDTO();
				
				escenariosDTO.setNbHighlight("");
				
				lstEscenariosDTO.add(escenariosDTO);
			
			}
			return lstEscenariosDTO;
		}

}
