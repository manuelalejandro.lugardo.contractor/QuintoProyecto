package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;
import com.bbva.msca.back.dominio.Tsca052Clialtriedet;
import com.bbva.msca.back.dominio.Tsca052ClialtriedetPK;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dominio.Tsca066Checklist;
import com.bbva.msca.back.dto.ChecklistDTO;

public class ChecklistDAO {

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	/**
	 * Bitacora de movimientos Sesion - Tipologia
	 * */
	private Tsca055Log log;

	@SuppressWarnings("unchecked")
	public List<ChecklistDTO> getChecklist(ChecklistDTO checklistDTO)throws Exception {

		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
		
		List<ChecklistDTO> listaCheckCliAltRie = new ArrayList<ChecklistDTO>();
		
		Query query = em.createNativeQuery(Consultas.getConsulta("busquedaChecklist"));
		query.setParameter(1, checklistDTO.getCdTipologia());
		query.setParameter(2, checklistDTO.getCdOficio());
		//formato.format(checklistDTO.getFhRecepcion()) +"','DD/MM/YYYY')
		query.setParameter(3, formato.format(checklistDTO.getFhRecepcion()));
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdCliAltRie")
				.addScalar("cdChecklist")
				.addScalar("nuId")
				//.addScalar("cdClienteBancomer", StringType.INSTANCE)
				.addScalar("nbLiberadoASia")
				.addScalar("nbCorreoPreAnalisis")
				.addScalar("nbCorreoFolioPld")
				.addScalar("nbCalificacion")
				.addScalar("nbSolInfoFiduciario")
				.addScalar("nbRelevantes")
				.addScalar("nbOpis")
				.addScalar("nbListaBbva")
				.addScalar("nbValidarBloqCtas")
				//.addScalar("cdCliente", StringType.INSTANCE)
				//.addScalar("nbCliRazSoc")
				//.addScalar("fhRecepcion", TimestampType.INSTANCE)
				//.addScalar("cdOficio", StringType.INSTANCE)
				//.addScalar("cdTipologia", StringType.INSTANCE)
				.setResultTransformer(
						Transformers.aliasToBean(ChecklistDTO.class));

		listaCheckCliAltRie = (List<ChecklistDTO>) query.getResultList();

		return listaCheckCliAltRie;
	}

	@Transactional
	public int registraChecklist(ChecklistDTO checklistDTO) {
		int op = Constantes.FIN_ER;
		Tsca066Checklist chkLst = this.copiaABC(checklistDTO);
		chkLst.persist();
		chkLst.flush();
		this.log = this.generaLog(checklistDTO, 1);
		this.log.persist();
		this.log.flush();
		op = Constantes.FIN_OK;
		return op;
	}

	@Transactional
	public int actualizaChecklist(ChecklistDTO checklistDTO) {
		int op = Constantes.FIN_ER;
		Tsca066Checklist chkLst = this.copiaABC(checklistDTO);
		chkLst.merge();
		chkLst.flush();
		this.log = this.generaLog(checklistDTO, 2);
		this.log.merge();
		this.log.flush();
		op = Constantes.FIN_OK;
		return op;
	}

	@Transactional
	public int actualizaChecklists(List<ChecklistDTO> lstChkCliAltRie)
			throws Exception {
		int op = Constantes.FIN_ER;
		for (ChecklistDTO checklistDTO : lstChkCliAltRie) {
			Tsca066Checklist chkLst = this.copiaABC(checklistDTO);
			chkLst.merge();
			chkLst.flush();
			this.log = this.generaLog(checklistDTO, 2);
			this.log.persist();
			this.log.flush();
		}
		op = Constantes.FIN_OK;
		return op;
	}

	private Tsca066Checklist copiaABC(ChecklistDTO chkLst) {
		Tsca066Checklist chkLstBd = new Tsca066Checklist();
		Tsca052Clialtriedet cliDet = Tsca052Clialtriedet.findTsca052Clialtriedet(
				new Tsca052ClialtriedetPK(chkLst.getCdCliAltRie(), chkLst.getNuId()));
		chkLstBd.setIdChecklist(chkLst.getCdChecklist());
		chkLstBd.setNbLiberadoASia(chkLst.getNbLiberadoASia());
		chkLstBd.setNbCorreoPreAnal(chkLst.getNbCorreoPreAnalisis());
		chkLstBd.setNbCorreoFolPld(chkLst.getNbCorreoFolioPld());
		chkLstBd.setNbCalificacion(chkLst.getNbCalificacion());
		chkLstBd.setNbSolInfoFiduci(chkLst.getNbSolInfoFiduciario());
		chkLstBd.setNbRelevantes(chkLst.getNbRelevantes());
		chkLstBd.setNbOpis(chkLst.getNbOpis());
		chkLstBd.setNbListaBbva(chkLst.getNbListaBbva());
		chkLstBd.setNbValBloqCtas(chkLst.getNbValidarBloqCtas());
		chkLstBd.setTsca052Clialtriedet(cliDet);
		return chkLstBd;
	}

	@SuppressWarnings("unused")
	private Query getQuery(ChecklistDTO busqueda) throws Exception {
		StringBuilder strQuery = new StringBuilder();
		int fhPar = 0, cdOf = 0, cdTip = 0;
		Query query;
		strQuery.append(Consultas.getConsulta("getChecklistCliAltRie"));
		strQuery.append(" WHERE ");
		if (busqueda.getFhRecepcion() != null) {
			strQuery.append(" CL.FH_RECEPCION = ?");
			fhPar = 1;
		}
		if (busqueda.getCdOficio() != null && !busqueda.getCdOficio().isEmpty()) {
			if (fhPar > 0) {
				strQuery.append(" AND ");
				cdOf = fhPar + 1;
			} else {
				cdOf = 1;
			}
			strQuery.append(" CL.CD_OFICIO = ?");
		}
		if (busqueda.getCdTipologia() != null
				&& !busqueda.getCdTipologia().isEmpty()) {
			if (fhPar > 0 || cdOf > 0) {
				strQuery.append(" AND ");
				if(cdOf>0){
					cdTip = cdOf + 1;
				}else{
				    cdTip = fhPar + 1;
				}
			} else {
				cdTip = 1;
			}
			strQuery.append("CLDET.CD_TIPOLOGIA = ?");
		}
		query = em.createNativeQuery(strQuery.toString());
		if (fhPar > 0) {
			query.setParameter(fhPar, busqueda.getFhRecepcion());
		}
		if (cdOf > 0) {
			query.setParameter(cdOf, busqueda.getCdOficio());
		}
		if (cdTip > 0) {
			query.setParameter(cdTip, busqueda.getCdTipologia());
		}
		return query;
	}

	private Tsca055Log generaLog(ChecklistDTO check, int accion) {
		Tsca055Log logSesion = new Tsca055Log();
		logSesion.setNbEvento("MSCAVE01015_7");
		logSesion.setCdUsuario(check.getCdUsrAlta());
		logSesion.setFhFecha(new GregorianCalendar().getTime());
		switch (accion) {
		case 1:
			logSesion.setNbAccionRealizad("Alta: " + check.getCdCliAltRie());
			break;
		case 2:
			logSesion.setNbAccionRealizad("Modifica: "
					+ check.getCdCliAltRie());
			break;
		}
		return logSesion;
	}
	
	@SuppressWarnings("unchecked")
	public Tsca066Checklist getCheckListByClialtriedet( BigDecimal cdCliAltRie, BigDecimal nuId) throws Exception {
		Tsca066Checklist checkList = new Tsca066Checklist();
		List<ChecklistDTO> lstResultado = new ArrayList<ChecklistDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("getCheckListByCliealrie"));
		query.setParameter(1, cdCliAltRie);
		query.setParameter(2, nuId);
		query.unwrap(org.hibernate.SQLQuery.class)	
				.addScalar("cdCliAltRie")
				.addScalar("nuId")
				.addScalar("cdChecklist")
				.addScalar("nbLiberadoASia",StringType.INSTANCE)
				.addScalar("nbCorreoPreAnalisis",StringType.INSTANCE)
				.addScalar("nbCorreoFolioPld",StringType.INSTANCE)
				.addScalar("nbCalificacion",StringType.INSTANCE)
				.addScalar("nbSolInfoFiduciario",StringType.INSTANCE)
				.addScalar("nbRelevantes",StringType.INSTANCE)
				.addScalar("nbOpis",StringType.INSTANCE)
				.addScalar("nbListaBbva", StringType.INSTANCE )
				.addScalar("nbValidarBloqCtas")
				.setResultTransformer(Transformers.aliasToBean(ChecklistDTO.class));	
		lstResultado = (List<ChecklistDTO>) query.getResultList();
		if( lstResultado != null )
		{
			ChecklistDTO checkListDTO = lstResultado.get( 0 );
			checkList = Tsca066Checklist.findTsca066Checklist( checkListDTO.getCdChecklist() );
		}
		return checkList;		
	}

}
