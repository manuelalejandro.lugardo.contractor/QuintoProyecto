package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.msca.back.dominio.Tsca013Alerta;
import com.bbva.msca.back.dominio.Tsca014Caso;
import com.bbva.msca.back.dominio.Tsca023Sesion;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dto.AlertaDTO;
import com.bbva.msca.back.dto.UsuarioCasoDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
public class AsignacionCasosDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	@SuppressWarnings("unchecked")
	public List<AlertaDTO> getConsultaAlertasPendientesAsignacionGerencia(BigDecimal cdGerencia, Integer limit, Integer offset,String search, String name, String order) throws Exception{
		
		List<AlertaDTO> lstAlertasDTO = null;
		
		String consulta = " SELECT * FROM ( "+Consultas.getConsulta("getConsultaAlertasPendientesAsignacionSupervisor")+" ) ";

		if(search != null && !search.equals("")){
			consulta = consulta +" WHERE " +
									" nuFolioAlerta LIKE '%"+search+"%' " +
									" OR nuCliente LIKE '%"+search+"%' " +
									" OR nbCliente LIKE '%"+search+"%' " +
									" OR nuCuenta LIKE '%"+search+"%' " +
									" OR cdTipologia LIKE '%"+search+"%' ";
		}
		
		consulta = consulta +" "+ " order by "
	             + (name != null && !"".equals(name) ? " " + name + " ": " fhEnvio ")
	             + " "
	             + (order != null && !"".equals(order) ? " " + order + " "
	             : " asc ");
		
		Query query = em.createNativeQuery(consulta);
		query.setParameter(1,Constantes.ALERTA_PENDIENTE_DE_ASIGNACION);
		query.setParameter(2,cdGerencia);
		query.setParameter(3,Constantes.ASIGNACION_MANUAL);
		query.setParameter(4,Constantes.CASO_EN_ANALISIS);
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("nuFolioAlerta")
		.addScalar("nuCliente",StringType.INSTANCE)
        .addScalar("nbCliente")
        .addScalar("nuCuenta",StringType.INSTANCE)
        .addScalar("fhAlerta",TimestampType.INSTANCE)
        .addScalar("fhEnvio",TimestampType.INSTANCE)
        .addScalar("fhVenLibSia",TimestampType.INSTANCE)
		.addScalar("nbSistema")
		.addScalar("cdSistema",StringType.INSTANCE)
        .addScalar("cdTipologia",StringType.INSTANCE)
        .addScalar("nbSegmento")
        .addScalar("imAlerta")
        .addScalar("cdDivisa",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(AlertaDTO.class));
		
	 	query.setFirstResult(offset);
		query.setMaxResults(limit);
	 	
		lstAlertasDTO = (List<AlertaDTO>)query.getResultList();
	 
		return lstAlertasDTO;
	}
	
	public Integer getNuAlertasPendientesAsignacionGerencia(BigDecimal cdGerencia, String search) throws Exception{
		 
		 String consulta = " SELECT COUNT(*) FROM ( "+Consultas.getConsulta("getConsultaAlertasPendientesAsignacionSupervisor")+" ) ";
		 
		 if(search != null && !search.equals("")){
			 consulta = consulta +" WHERE " +
									" nuFolioAlerta LIKE '%"+search+"%' " +
									" OR nuCliente LIKE '%"+search+"%' " +
									" OR nbCliente LIKE '%"+search+"%' " +
									" OR nuCuenta LIKE '%"+search+"%' " +
									" OR cdTipologia LIKE '%"+search+"%' ";
		 }
	
		 Query query = em.createNativeQuery(consulta);
		 query.setParameter(1,Constantes.ALERTA_PENDIENTE_DE_ASIGNACION);
		 query.setParameter(2,cdGerencia);
		 query.setParameter(3,Constantes.ASIGNACION_MANUAL);
		 query.setParameter(4,Constantes.CASO_EN_ANALISIS);	
		 
		 Integer conteo = ((Number) query.getSingleResult()).intValue();
	 
		return conteo;
	}
	
	@SuppressWarnings("unchecked")
	public List<AlertaDTO> getConsultaCasosAsignadosConsultor(BigDecimal cdConsultor, Integer limit, Integer offset,String search, String name, String order) throws Exception{
		
		 List<AlertaDTO> lstAlertasDTO = null;
		 
		 String consulta = " SELECT * FROM ( "+Consultas.getConsulta("getConsultaCasosAsignadosConsultor")+" ) ";

		 consulta = consulta +" "+ " order by "
					             + (name != null && !"".equals(name) ? " " + name + " ": " nbPrioriAsig ")
					             + " "
					             + (order != null && !"".equals(order) ? " " + order + " "
					             : " asc ");
		 
		 Query query = em.createNativeQuery(consulta);
		 query.setParameter(1,Constantes.CASO_EN_ANALISIS);
		 query.setParameter(2,cdConsultor);
			
		 query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdCaso")
		.addScalar("nuFolioAlerta")
		.addScalar("nuCliente",StringType.INSTANCE)
        .addScalar("nbCliente")
        .addScalar("nuCuenta",StringType.INSTANCE)
        .addScalar("fhEnvio",TimestampType.INSTANCE)
		.addScalar("nbSistema")
		.addScalar("cdSistema",StringType.INSTANCE)
        .addScalar("cdTipologia",StringType.INSTANCE)
        .addScalar("nbSegmento")
        .addScalar("fhAsignacion",TimestampType.INSTANCE)
        .addScalar("nbPrioriAsig")
        .addScalar("fhVenRev",TimestampType.INSTANCE)
        .addScalar("fhVenLibSia",TimestampType.INSTANCE)
        .addScalar("fhRepAut",TimestampType.INSTANCE)
        .addScalar("fhLimRepAutA",TimestampType.INSTANCE)
        .addScalar("fhLimRepAutB",TimestampType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(AlertaDTO.class));
		
		query.setFirstResult(offset);
		query.setMaxResults(limit);
		 
		lstAlertasDTO = (List<AlertaDTO>)query.getResultList();
	 
		return lstAlertasDTO;
	}
	
	public Integer getNuCasosAsignadosConsultor(BigDecimal cdConsultor) throws Exception{
		 
		 String consulta = " SELECT COUNT(*) FROM ( "+Consultas.getConsulta("getConsultaCasosAsignadosConsultor")+" ) ";
	
		 Query query = em.createNativeQuery(consulta);
		 query.setParameter(1,Constantes.CASO_EN_ANALISIS);
		 query.setParameter(2,cdConsultor);
		 
		 Integer conteo = ((Number) query.getSingleResult()).intValue();
	 
		return conteo;
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioCasoDTO> getConsultoresSupervisor(BigDecimal cdSupervisor) throws Exception{
		
		 List<UsuarioCasoDTO> lstUsuarioDTO = null;
		 Query query = em.createNativeQuery(Consultas.getConsulta("getConsultoresSupervisor"));
		 query.setParameter(1,cdSupervisor);
		 query.setParameter(2,cdSupervisor);
			
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("cdUsuario",StringType.INSTANCE)
	        .addScalar("nbPersona")
	        .addScalar("nbPaterno")
	        .addScalar("nbMaterno")
	        .addScalar("nuPrioriAsigNormal")
	        .addScalar("nuPrioriAsigUrgente")
	        .addScalar("nuStAlertaAnalisis")
	        .addScalar("nuStAlertaSupervision")
	        .setResultTransformer(Transformers.aliasToBean(UsuarioCasoDTO.class));
		
		 lstUsuarioDTO = (List<UsuarioCasoDTO>)query.getResultList();
	 
		return lstUsuarioDTO;
	}
	
	@Transactional
	public int addAlertasCaso(List<AlertaDTO> lstAlertaDTO,String cdUsuario,String cdPrioriAsig) throws Exception{
		int op = Constantes.FIN_ER;
		Tsca014Caso tsca014CasoAlerta = null;
		Tsca013Alerta tsca013Alerta = null;
		Tsca055Log tsca055Log = null;
		List<Tsca013Alerta> lstTsca013Alerta = null;
		String nuCliente = "";
		String nbAccion = "MODIFICACIÓN";
		PerfiladoDAO perfiladoDAO = null;
		
		for(int i=0;i<lstAlertaDTO.size();i++){
			
			if(!nuCliente.equals(lstAlertaDTO.get(i).getNuCliente())){
				
				nuCliente = lstAlertaDTO.get(i).getNuCliente();
				
				if(lstAlertaDTO.get(i).getNuCliente() != null){
					tsca014CasoAlerta = getCasoCliente(lstAlertaDTO.get(i).getNuCliente());
				
					if(tsca014CasoAlerta == null){
						tsca014CasoAlerta = new Tsca014Caso();
						tsca014CasoAlerta.setCdUsuario(cdUsuario);
						tsca014CasoAlerta.setCdPrioriAsig(cdPrioriAsig);
						tsca014CasoAlerta.setCdCliente(lstAlertaDTO.get(i).getNuCliente());
						tsca014CasoAlerta.setStCaso(Constantes.CASO_EN_ANALISIS);
						tsca014CasoAlerta.persist();
						tsca014CasoAlerta.flush();
						nbAccion = "ALTA";
					}
				}
				
				if(tsca014CasoAlerta != null){
					lstTsca013Alerta = getAlertasByCliente(lstAlertaDTO.get(i).getNuCliente());
					
					for(int x=0;x<lstTsca013Alerta.size();x++){
						tsca013Alerta = lstTsca013Alerta.get(x);
						tsca013Alerta.setCdCaso(tsca014CasoAlerta.getCdCaso());
						tsca013Alerta.setCdStAlerta(Constantes.ALERTA_ASIGNADA);
						tsca013Alerta.setFhAsignacion(new Date());
						tsca013Alerta.merge();
						tsca013Alerta.flush();
					}
					if(lstTsca013Alerta.size()>0){
						if(tsca014CasoAlerta.getCdSesion() == null){
							tsca014CasoAlerta.setCdSesion(Tsca023Sesion.findTsca023Sesion(lstTsca013Alerta.get(0).getCdSesion().getCdSesion()));//JANROME
							tsca014CasoAlerta.merge();
							tsca014CasoAlerta.flush();
							
							tsca055Log = new Tsca055Log();
							perfiladoDAO = new PerfiladoDAO();
							tsca055Log.setCdCaso(tsca014CasoAlerta.getCdCaso());
							tsca055Log.setCdCliente(tsca014CasoAlerta.getCdCliente());
							tsca055Log.setCdUsuario(perfiladoDAO.getInfoEmpleado(ArqSpringContext.getUsuario().trim().toUpperCase().replaceAll("^MX.", "")).getCdUsuario());
							tsca055Log.setFhFecha(new Timestamp(new Date().getTime()));
							tsca055Log.setNbAccionRealizad(nbAccion);
							tsca055Log.setNbEvento("ASIGNACI\u00d3N DE CASO");
							tsca055Log.persist();
							tsca055Log.flush();
						}
					}
					op = Constantes.FIN_OK;
				}
			}
		}
		return op;
	}
	
	@SuppressWarnings("unchecked")
	public List<Tsca013Alerta> getAlertasByCliente(String cdCliente){
		List<Tsca013Alerta> lstTsca013Alerta = null;
	
		try {
			Query query = em.createNativeQuery(Consultas.getConsulta("getAlertasByCliente"),Tsca013Alerta.class);
			query.setParameter(1,cdCliente);
			query.setParameter(2,Constantes.ALERTA_PENDIENTE_DE_ASIGNACION);
			lstTsca013Alerta = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lstTsca013Alerta;
	}
	
	@SuppressWarnings("unchecked")
	public Tsca014Caso getCasoCliente(String nuCliente){
		
		Tsca014Caso tsca014CasoAlerta = null;
		List<Tsca014Caso> resultado = new ArrayList<Tsca014Caso>();
		try{
			Query query = em.createNativeQuery(Consultas.getConsulta("getCasoCliente"),Tsca014Caso.class);
			query.setParameter(1,nuCliente);
			query.setParameter(2,Constantes.CASO_EN_ANALISIS);
			resultado = (List<Tsca014Caso>) query.getResultList();
			if( resultado != null && !resultado.isEmpty())
			{
				tsca014CasoAlerta = resultado.get( 0 );	
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return tsca014CasoAlerta;
	}
	
	@SuppressWarnings("unchecked")
	public Tsca013Alerta getAlerta(String nuFolioAlerta,String cdSistema){
		
		Tsca013Alerta tsca013Alerta = null;
		List<Tsca013Alerta> resultado = new ArrayList<Tsca013Alerta>();
		
		try{
			Query query = em.createNativeQuery(Consultas.getConsulta("getAlerta"),Tsca013Alerta.class);
			query.setParameter(1,nuFolioAlerta);
			query.setParameter(2,cdSistema);
			resultado = (List<Tsca013Alerta>)query.getResultList();
			if( resultado != null && !resultado.isEmpty())
			{
				tsca013Alerta = resultado.get(0);	
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return tsca013Alerta;
	}
	
	@Transactional
	public int setDescartarAlertas(List<AlertaDTO> lstAlertaDTO) throws Exception{
		
		int op = Constantes.FIN_ER;
		Tsca013Alerta tsca013Alerta = null;
		
		for(int i=0;i<lstAlertaDTO.size();i++){
			
			tsca013Alerta = getAlerta(lstAlertaDTO.get(i).getNuFolioAlerta(),lstAlertaDTO.get(i).getCdSistema());
			if(tsca013Alerta != null){
				tsca013Alerta.setCdStAlerta(Constantes.ALERTA_DESCARTADA);
				tsca013Alerta.setTxMotivoDescarte(lstAlertaDTO.get(i).getTxMotivoDescarte());
				tsca013Alerta.merge();
				tsca013Alerta.flush();
			}
		}
		op = Constantes.FIN_OK;
		
		return op;
	}
	
	@Transactional
	public int addCasosConsultor(List<AlertaDTO> lstAlertaDTO,String cdUsuario) throws Exception{
		int op = Constantes.FIN_ER;
		Tsca014Caso tsca014CasoAlerta = null;
		String nuCliente = "";
		
		for(int i=0;i<lstAlertaDTO.size();i++){
			
			if(!nuCliente.equals(lstAlertaDTO.get(i).getNuCliente())){
				
				nuCliente = lstAlertaDTO.get(i).getNuCliente();
				tsca014CasoAlerta = getCasoCliente(lstAlertaDTO.get(i).getNuCliente());
				
				if(tsca014CasoAlerta != null){
					tsca014CasoAlerta.setCdUsuario(cdUsuario);
					tsca014CasoAlerta.merge();
					tsca014CasoAlerta.flush();
					op = Constantes.FIN_OK;
				}
			}
		}
		return op;
	}

}
