package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.bbva.msca.back.dto.CatalogoDTO;
import com.bbva.msca.back.dto.ListaGenericaDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Component;

@Component
public class CatalogoDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	@SuppressWarnings("unchecked")
	public List<CatalogoDTO> getDetCatalogoPorCdCatalogo(BigDecimal cdCatalogo) throws Exception{
		
		List<CatalogoDTO> lstCatalogoDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getDetCatalogoPorCdCatalogo"));
		query.setParameter(1,cdCatalogo);
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdDetCatalogo",StringType.INSTANCE)
		.addScalar("nbValor")
        .addScalar("cdCatalogo")
        .addScalar("nbDescripcion")
        .setResultTransformer(Transformers.aliasToBean(CatalogoDTO.class));
		
	 	lstCatalogoDTO = (List<CatalogoDTO>)query.getResultList();
	 
		return lstCatalogoDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoSegmento() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoSegmento"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}

	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoTipoPersona() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoTipoPersona"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoTipologia() throws Exception{
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoTipologia"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoTipologiaGerencia(Integer cdGerencia, String estatus ) throws Exception{
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoTipologiaGerencia"));
		query.setParameter(1, cdGerencia);
		query.setParameter(2, estatus );
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoSesion() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoSesion"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoEscenarios() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoEscenarios"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}

	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoSectores() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoSectores"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoCriterios() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoCriterios"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoPerfiles() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoPerfiles"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoUsuariosPerfil(Integer cdPerfil) throws Exception{
		List<ListaGenericaDTO> lstUsuarioDTO = new ArrayList<ListaGenericaDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoUsuariosPerfil"));
		query.setParameter(1, cdPerfil);
		query.setParameter(2, Constantes.CD_ST_SIS_A);
		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
		lstUsuarioDTO = (List<ListaGenericaDTO>)query.getResultList();

		return lstUsuarioDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoUsuariosPerfilSupervisor(Integer cdSupervisor, Integer cdPerfil) throws Exception{
		List<ListaGenericaDTO> lstUsuarioDTO = new ArrayList<ListaGenericaDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoUsuariosPerfilSupervisor"));
		query.setParameter(1, cdSupervisor);
		query.setParameter(2, cdPerfil);
		query.setParameter(3, Constantes.CD_ST_SIS_A);
		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
		lstUsuarioDTO = (List<ListaGenericaDTO>)query.getResultList();

		return lstUsuarioDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoGerencias() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoGerencias"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}

	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoTipoCuenta() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoTipoCuenta"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoTipoOperacionMonetaria() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoTipoOperacionMonetaria"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoInstrumentoMonetario() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoInstrumentoMonetario"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoMoneda() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoMoneda"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoOperacionInusual(String cOperacion) throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoOperacionInusual"));
		query.setParameter(1,cOperacion);
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}

	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoDictamenPreliminar() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoDictamenPreliminar"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoDictamenFinal() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoDictamenFinal"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoTipoPrioridad() throws Exception{
		String stActivo = Constantes.CD_ST_SIS_A;
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoTipos"));
		query.setParameter(1, stActivo );
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoTipologiaCAR() throws Exception{
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoTipologiaCAR"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ListaGenericaDTO> getCatalogoRespuesta() throws Exception{
		
		List<ListaGenericaDTO> lstListaGenericaDTO = null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getCatalogoRespuesta"));
		
	 	query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdValor",StringType.INSTANCE)
		.addScalar("nbValor",StringType.INSTANCE)
        .setResultTransformer(Transformers.aliasToBean(ListaGenericaDTO.class));
		
	 	lstListaGenericaDTO = (List<ListaGenericaDTO>)query.getResultList();
	 
		return lstListaGenericaDTO;
	}

}
