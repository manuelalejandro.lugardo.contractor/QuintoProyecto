// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca063Accionista;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca063Accionista_Roo_Entity {
    
    declare @type: Tsca063Accionista: @Entity;
    
    declare @type: Tsca063Accionista: @Table(name = "TSCA063_ACCIONISTA", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca063Accionista.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CD_ACCIONISTA")
    private BigDecimal Tsca063Accionista.cdAccionista;
    
    public BigDecimal Tsca063Accionista.getCdAccionista() {
        return this.cdAccionista;
    }
    
    public void Tsca063Accionista.setCdAccionista(BigDecimal id) {
        this.cdAccionista = id;
    }
    
    @Transactional
    public void Tsca063Accionista.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca063Accionista.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca063Accionista attached = Tsca063Accionista.findTsca063Accionista(this.cdAccionista);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca063Accionista.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca063Accionista.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca063Accionista Tsca063Accionista.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca063Accionista merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca063Accionista.entityManager() {
        EntityManager em = new Tsca063Accionista().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca063Accionista.countTsca063Accionistas() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca063Accionista o", Long.class).getSingleResult();
    }
    
    public static List<Tsca063Accionista> Tsca063Accionista.findAllTsca063Accionistas() {
        return entityManager().createQuery("SELECT o FROM Tsca063Accionista o", Tsca063Accionista.class).getResultList();
    }
    
    public static Tsca063Accionista Tsca063Accionista.findTsca063Accionista(BigDecimal cdAccionista) {
        if (cdAccionista == null) return null;
        return entityManager().find(Tsca063Accionista.class, cdAccionista);
    }
    
    public static List<Tsca063Accionista> Tsca063Accionista.findTsca063AccionistaEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca063Accionista o", Tsca063Accionista.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
