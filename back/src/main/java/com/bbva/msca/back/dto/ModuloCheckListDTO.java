package com.bbva.msca.back.dto;

public class ModuloCheckListDTO {
	private Integer nuCliente;
	private String nbCliente;
	private String nbTipologia;
	private String nbPregunta;
	private boolean stLiberadaSIA;
	private boolean stCorreoPrevio;
	private boolean stCorreoFolio;
	private boolean stCalificacion;
	private boolean stInfoFiduciario;
	private boolean stRelevantes;
	private boolean stOpis;
	private boolean stListaBBVA;
	private boolean stBloqueoCtas;
	
	public Integer getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(Integer nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNbTipologia() {
		return nbTipologia;
	}
	public void setNbTipologia(String nbTipologia) {
		this.nbTipologia = nbTipologia;
	}
	public String getNbPregunta() {
		return nbPregunta;
	}
	public void setNbPregunta(String nbPregunta) {
		this.nbPregunta = nbPregunta;
	}
	public boolean getStLiberadaSIA() {
		return stLiberadaSIA;
	}
	public void setStLiberadaSIA(boolean stLiberadaSIA) {
		this.stLiberadaSIA = stLiberadaSIA;
	}
	public boolean getStCorreoPrevio() {
		return stCorreoPrevio;
	}
	public void setStCorreoPrevio(boolean stCorreoPrevio) {
		this.stCorreoPrevio = stCorreoPrevio;
	}
	public boolean getStCorreoFolio() {
		return stCorreoFolio;
	}
	public void setStCorreoFolio(boolean stCorreoFolio) {
		this.stCorreoFolio = stCorreoFolio;
	}
	public boolean getStCalificacion() {
		return stCalificacion;
	}
	public void setStCalificacion(boolean stCalificacion) {
		this.stCalificacion = stCalificacion;
	}
	public boolean getStInfoFiduciario() {
		return stInfoFiduciario;
	}
	public void setStInfoFiduciario(boolean stInfoFiduciario) {
		this.stInfoFiduciario = stInfoFiduciario;
	}
	public boolean getStRelevantes() {
		return stRelevantes;
	}
	public void setStRelevantes(boolean stRelevantes) {
		this.stRelevantes = stRelevantes;
	}
	public boolean getStOpis() {
		return stOpis;
	}
	public void setStOpis(boolean stOpis) {
		this.stOpis = stOpis;
	}
	public boolean getStListaBBVA() {
		return stListaBBVA;
	}
	public void setStListaBBVA(boolean stListaBBVA) {
		this.stListaBBVA = stListaBBVA;
	}
	public boolean getStBloqueoCtas() {
		return stBloqueoCtas;
	}
	public void setStBloqueoCtas(boolean stBloqueoCtas) {
		this.stBloqueoCtas = stBloqueoCtas;
	}
	
	

}
