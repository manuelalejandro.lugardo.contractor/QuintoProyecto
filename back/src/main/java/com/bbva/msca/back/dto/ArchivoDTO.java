package com.bbva.msca.back.dto;

import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Date;

public class ArchivoDTO {

	private BigDecimal cdFolioArchivo;
	private String cdExpediente;
	private String nbTitulo;
	private String cdUsuarioCreacion;
	private Date fhCreacion;
	private String cdUsuarioModificacion;
	private Date fhModificacion;
	private InputStream nbDocumento;
	private String txInvestigacion;

	public BigDecimal getCdFolioArchivo() {
		return cdFolioArchivo;
	}

	public void setCdFolioArchivo(BigDecimal cdFolioArchivo) {
		this.cdFolioArchivo = cdFolioArchivo;
	}

	public String getCdExpediente() {
		return cdExpediente;
	}

	public void setCdExpediente(String cdExpediente) {
		this.cdExpediente = cdExpediente;
	}

	public String getNbTitulo() {
		return nbTitulo;
	}

	public void setNbTitulo(String nbTitulo) {
		this.nbTitulo = nbTitulo;
	}

	public String getCdUsuarioCreacion() {
		return cdUsuarioCreacion;
	}

	public void setCdUsuarioCreacion(String cdUsuarioCreacion) {
		this.cdUsuarioCreacion = cdUsuarioCreacion;
	}

	public Date getFhCreacion() {
		return fhCreacion;
	}

	public void setFhCreacion(Date fhCreacion) {
		this.fhCreacion = fhCreacion;
	}

	public String getCdUsuarioModificacion() {
		return cdUsuarioModificacion;
	}

	public void setCdUsuarioModificacion(String cdUsuarioModificacion) {
		this.cdUsuarioModificacion = cdUsuarioModificacion;
	}

	public Date getFhModificacion() {
		return fhModificacion;
	}

	public void setFhModificacion(Date fhModificacion) {
		this.fhModificacion = fhModificacion;
	}

	public InputStream getNbDocumento() {
		return nbDocumento;
	}

	public void setNbDocumento(InputStream nbDocumento) {
		this.nbDocumento = nbDocumento;
	}

	public String getTxInvestigacion() {
		return txInvestigacion;
	}

	public void setTxInvestigacion(String txInvestigacion) {
		this.txInvestigacion = txInvestigacion;
	}

}
