package com.bbva.msca.back.srv;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/DetalleAlertaService")

public interface DetalleAlertaService {
	
	@GET
	@Path("/consultarDetalleAlertas")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDetalleAlertas();
	
	@GET
	@Path("/consultarDetalleTipologia")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDetalleTipologia();
	
	@GET
	@Path("/consultarEscenarios")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarEscenarios();

}
