// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import java.lang.Object;
import java.lang.String;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

privileged aspect Tsca039FircosoftPK_Roo_Identifier {
    
    declare @type: Tsca039FircosoftPK: @Embeddable;
    
    @Column(name = "CD_CASO", nullable = false)
    private BigDecimal Tsca039FircosoftPK.cdCaso;
    
    @Column(name = "CD_CLIENTE", nullable = false, length = 8)
    private String Tsca039FircosoftPK.cdCliente;
    
    public Tsca039FircosoftPK.new(BigDecimal cdCaso, String cdCliente) {
        super();
        this.cdCaso = cdCaso;
        this.cdCliente = cdCliente;
    }

    private Tsca039FircosoftPK.new() {
        super();
    }

    public BigDecimal Tsca039FircosoftPK.getCdCaso() {
        return this.cdCaso;
    }
    
    public String Tsca039FircosoftPK.getCdCliente() {
        return this.cdCliente;
    }
    
    public boolean Tsca039FircosoftPK.equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Tsca039FircosoftPK)) return false;
        Tsca039FircosoftPK other = (Tsca039FircosoftPK) obj;
        if (cdCaso == null) {
            if (other.cdCaso != null) return false;
        } else if (!cdCaso.equals(other.cdCaso)) return false;
        if (cdCliente == null) {
            if (other.cdCliente != null) return false;
        } else if (!cdCliente.equals(other.cdCliente)) return false;
        return true;
    }
    
    public int Tsca039FircosoftPK.hashCode() {
        final int prime = 31;
        int result = 17;
        result = prime * result + (cdCaso == null ? 0 : cdCaso.hashCode());
        result = prime * result + (cdCliente == null ? 0 : cdCliente.hashCode());
        return result;
    }
    
}
