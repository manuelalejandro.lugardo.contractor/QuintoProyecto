package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.bbva.msca.back.dto.AlertasPreviasDTO;
import com.bbva.msca.back.util.Conexion;
import com.bbva.msca.back.util.Consultas;

public class AlertasPreviasSiaDAO {
	
	public List<AlertasPreviasDTO> getAlertasPreviasSIA(String cdCliente) throws Exception{
		Conexion conexion = new Conexion();
		Connection connection = conexion.getConexion();
		
	    String query =  Consultas.getConsulta("getConsultaAlertasPreviasSIA");
	    
	      PreparedStatement statement = null;
	      ResultSet resultSet = null;
	      List<AlertasPreviasDTO> lstAlertasPreviasDTO = new ArrayList<AlertasPreviasDTO>();
	      statement = connection.prepareStatement(query);
	      statement.setString(1,cdCliente);
	      
	      resultSet = statement.executeQuery();
	      
	      while(resultSet.next()){
	    	  
	    	 AlertasPreviasDTO alertasPreviasDTO = new AlertasPreviasDTO();
	    	 
	    	 alertasPreviasDTO.setNuCaso(resultSet.getBigDecimal("nuCaso"));
	    	 alertasPreviasDTO.setNuFolio(resultSet.getString("nuFolio"));
	    	 alertasPreviasDTO.setNuCliente(resultSet.getString("nuCliente"));
	    	 alertasPreviasDTO.setNbCliente(resultSet.getString("nbCliente"));
	    	 alertasPreviasDTO.setNuCuenta(resultSet.getString("nuCuenta"));
	    	 alertasPreviasDTO.setNbTipologia(resultSet.getString("nbTipologia"));
	    	 alertasPreviasDTO.setFhAlerta(resultSet.getDate("fhAlerta"));
	    	 alertasPreviasDTO.setNbConsultor(resultSet.getString("nbConsultor"));
	    	 alertasPreviasDTO.setNbDictamen(resultSet.getString("nbDictamen"));
	    	 alertasPreviasDTO.setFhDictamen(resultSet.getDate("fhDictamen"));
	    	 alertasPreviasDTO.setNbDictamenSIA(resultSet.getString("nbDictamenSIA"));
	    	 alertasPreviasDTO.setFhDictamenSIA(resultSet.getDate("fhDictamenSIA"));
	    	 alertasPreviasDTO.setNbSistema(resultSet.getString("nbSistema"));
	    	 alertasPreviasDTO.setNbCalificacion(resultSet.getString("nbCalificacion"));
	    	 alertasPreviasDTO.setNbReportada(resultSet.getString("nbReportada"));
	    	 alertasPreviasDTO.setFhReporte(resultSet.getDate("fhReporte"));
	    	 alertasPreviasDTO.setNbCoincidencia(resultSet.getString("nbCoincidencia"));
	    	 	    	 
	    	 lstAlertasPreviasDTO.add(alertasPreviasDTO);
	      }
	      
	      connection = conexion.close();
	     return lstAlertasPreviasDTO;
	}

}
