package com.bbva.msca.back.dominio;

import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.PersistenceContext;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(table = "TSCA023_SESION", schema = "GORAPR", versionField = "")
public class Tsca023Sesion {

    @Id
    @Column(name = "CD_SESION")
    private String cdSesion;
    
    @Column(name = "CD_USUARIO")
    private String cdUsrAlta;
}
