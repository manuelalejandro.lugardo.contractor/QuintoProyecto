package com.bbva.msca.back.dto;

import java.util.Date;

public class PersonaReportadaDTO {
	
	private Integer nuID;
	private String nbCaracter;
	private  String nbTipoPersona;
	private String nbCliente;
	private String nbApellidoP;
	private String nbApellidoM;
	private Date fhNacimiento;
	private String nuRFC;
	private String nbDomicilio;
	private String nbComplementarios;
	private String nbCBancomer;
	private Integer nuCliente;
	private Integer nuCuenta;
	private String nbTipologia;
	private String nbDictamen;
	
	public Integer getNuID() {
		return nuID;
	}
	public void setNuID(Integer nuID) {
		this.nuID = nuID;
	}
	public String getNbCaracter() {
		return nbCaracter;
	}
	public void setNbCaracter(String nbCaracter) {
		this.nbCaracter = nbCaracter;
	}
	public String getNbTipoPersona() {
		return nbTipoPersona;
	}
	public void setNbTipoPersona(String nbTipoPersona) {
		this.nbTipoPersona = nbTipoPersona;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNbApellidoP() {
		return nbApellidoP;
	}
	public void setNbApellidoP(String nbApellidoP) {
		this.nbApellidoP = nbApellidoP;
	}
	public String getNbApellidoM() {
		return nbApellidoM;
	}
	public void setNbApellidoM(String nbApellidoM) {
		this.nbApellidoM = nbApellidoM;
	}
	public Date getFhNacimiento() {
		return fhNacimiento;
	}
	public void setFhNacimiento(Date fhNacimiento) {
		this.fhNacimiento = fhNacimiento;
	}
	public String getNuRFC() {
		return nuRFC;
	}
	public void setNuRFC(String nuRFC) {
		this.nuRFC = nuRFC;
	}
	public String getNbDomicilio() {
		return nbDomicilio;
	}
	public void setNbDomicilio(String nbDomicilio) {
		this.nbDomicilio = nbDomicilio;
	}
	public String getNbComplementarios() {
		return nbComplementarios;
	}
	public void setNbComplementarios(String nbComplementarios) {
		this.nbComplementarios = nbComplementarios;
	}
	public String getNbCBancomer() {
		return nbCBancomer;
	}
	public void setNbCBancomer(String nbCBancomer) {
		this.nbCBancomer = nbCBancomer;
	}
	public Integer getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(Integer nuCliente) {
		this.nuCliente = nuCliente;
	}
	public Integer getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(Integer nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public String getNbTipologia() {
		return nbTipologia;
	}
	public void setNbTipologia(String nbTipologia) {
		this.nbTipologia = nbTipologia;
	}
	public String getNbDictamen() {
		return nbDictamen;
	}
	public void setNbDictamen(String nbDictamen) {
		this.nbDictamen = nbDictamen;
	}
	
	

}
