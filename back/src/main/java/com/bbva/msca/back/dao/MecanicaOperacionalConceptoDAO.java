package com.bbva.msca.back.dao;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;

import com.bbva.msca.back.dto.MecanicaOperacionalConceptoDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import com.bbva.msca.back.util.MensajesI18n;

import org.hibernate.impl.SessionImpl;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Connection;
import java.sql.ResultSet;
import oracle.jdbc.OracleTypes;

@Component
@Configurable
public class MecanicaOperacionalConceptoDAO {
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	@SuppressWarnings("unchecked")
	@Transactional
	public List<MecanicaOperacionalConceptoDTO> consultarMecanicaOperacionalConceptoAnalisis(BigDecimal cdCaso, String cdAlerta, String cdSistema) throws Exception{
		
		SessionImpl sesionimpl = (SessionImpl)em.getDelegate();
		Connection con = sesionimpl.connection();
		CallableStatement  cstmt = null;
		
		List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConceptoDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		MecanicaOperacionalConceptoDTO mecanica = null;

		cstmt = con.prepareCall(MensajesI18n.getText("SP_MEC_OPE_ALL"));
		
		cstmt.setQueryTimeout(1800);
		cstmt.setBigDecimal(1,cdCaso);
		cstmt.setString(2,cdAlerta);
		cstmt.setString(3,cdSistema);
		cstmt.registerOutParameter(4,OracleTypes.CURSOR);
		//System.out.print("");
		cstmt.execute();
		ResultSet rset = (ResultSet)cstmt.getObject(4);
		
		while (rset.next()){
			mecanica = new MecanicaOperacionalConceptoDTO();
			
			mecanica.setNbConceptoTC(rset.getString("nbConceptoTC"));
			mecanica.setNuMovimientosTC(rset.getBigDecimal("nuMovimientosTC"));
			mecanica.setImMontoTC(rset.getBigDecimal("imMontoTC"));
			mecanica.setNuPorcentajeTC(rset.getBigDecimal("nuPorcentajeTC"));
			mecanica.setNuPromedioTC(rset.getBigDecimal("nuPromedioTC"));
			mecanica.setNuMovimientosATC(rset.getBigDecimal("nuMovimientosATC"));
			mecanica.setImMontoATC(rset.getBigDecimal("imMontoATC"));
			mecanica.setNuPorcentajeATC(rset.getBigDecimal("nuPorcentajeATC"));
			mecanica.setNuPromedioATC(rset.getBigDecimal("nuPromedioATC"));
			
			lstMecanicaOperacionalConceptoDTO.add(mecanica);
		}
			rset.close();
			cstmt.close();
			con.close();
			
		return lstMecanicaOperacionalConceptoDTO;
	}
	/*
	//TABLA DE MES UNO
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> consultarConceptosMesUno(BigDecimal cdCaso, String cdAlerta, String cdSistema) throws Exception{
		List<MecanicaOperacionalConceptoDTO> lstTablaMesUnoDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaMesUnoAnalisis"));
		    query.setParameter(1,cdCaso);
		 	query.setParameter(2,cdAlerta); 
		 	query.setParameter(3,cdSistema);
		 	query.setParameter(4,cdAlerta);
		 	query.setParameter(5,cdSistema);
		 	query.setParameter(6,cdAlerta);
		 	query.setParameter(7,cdSistema);
		 	query.setParameter(8,cdCaso);
		 	query.setParameter(9,cdAlerta); 
		 	query.setParameter(10,cdSistema);
		 	query.setParameter(11,cdAlerta);
		 	query.setParameter(12,cdSistema);
		 	query.setParameter(13,cdAlerta);
		 	query.setParameter(14,cdSistema);
		 	query.setParameter(15,cdCaso);
		 	query.setParameter(16,cdAlerta);
		 	query.setParameter(17,cdSistema);
		 	query.setParameter(18,cdAlerta);
		 	query.setParameter(19,cdSistema);
		 	query.setParameter(20,cdAlerta);
		 	query.setParameter(21,cdSistema);
		 	query.setParameter(22,cdCaso); 
		 	query.setParameter(23,cdAlerta);
		 	query.setParameter(24,cdSistema);
		 	query.setParameter(25,cdAlerta);
		 	query.setParameter(26,cdSistema);
		 	query.setParameter(27,cdAlerta);
		 	query.setParameter(28,cdSistema);
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTM1", StringType.INSTANCE)
	        .addScalar("nuMovimientosCTM1")
	        .addScalar("imMontoCTM1")
	        .addScalar("nuPorcentajeCTM1")
	        .addScalar("nuPromedioCTM1")
	        .addScalar("nuMovimientosATM1")
	        .addScalar("imMontoATM1")
	        .addScalar("nuPorcentajeATM1")
	        .addScalar("nuPromedioATM1")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		 lstTablaMesUnoDTO = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();
		return lstTablaMesUnoDTO;
	}

	
	//TABLA DE MES DOS
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> consultarConceptosMesDos(BigDecimal cdCaso, String cdAlerta, String cdSistema) throws Exception{
		List<MecanicaOperacionalConceptoDTO> lstTablaMesDosDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaMesDosAnalisis"));
		 query.setParameter(1,cdCaso);
		 	query.setParameter(2,cdAlerta); 
		 	query.setParameter(3,cdSistema);
		 	query.setParameter(4,cdAlerta);
		 	query.setParameter(5,cdSistema);
		 	query.setParameter(6,cdAlerta);
		 	query.setParameter(7,cdSistema);
		 	query.setParameter(8,cdCaso);
		 	query.setParameter(9,cdAlerta); 
		 	query.setParameter(10,cdSistema);
		 	query.setParameter(11,cdAlerta);
		 	query.setParameter(12,cdSistema);
		 	query.setParameter(13,cdAlerta);
		 	query.setParameter(14,cdSistema);
		 	query.setParameter(15,cdCaso);
		 	query.setParameter(16,cdAlerta);
		 	query.setParameter(17,cdSistema);
		 	query.setParameter(18,cdAlerta);
		 	query.setParameter(19,cdSistema);
		 	query.setParameter(20,cdAlerta);
		 	query.setParameter(21,cdSistema);
		 	query.setParameter(22,cdCaso); 
		 	query.setParameter(23,cdAlerta);
		 	query.setParameter(24,cdSistema);
		 	query.setParameter(25,cdAlerta);
		 	query.setParameter(26,cdSistema);
		 	query.setParameter(27,cdAlerta);
		 	query.setParameter(28,cdSistema);
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTM2", StringType.INSTANCE)
	        .addScalar("nuMovimientosCTM2")
	        .addScalar("imMontoCTM2")
	        .addScalar("nuPorcentajeCTM2")
	        .addScalar("nuPromedioCTM2")
	        .addScalar("nuMovimientosATM2")
	        .addScalar("imMontoATM2")
	        .addScalar("nuPorcentajeATM2")
	        .addScalar("nuPromedioATM2")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		 lstTablaMesDosDTO = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();
		return lstTablaMesDosDTO;
	}

	
	//TABLA DE MES TRES
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> consultarConceptosMesTres(BigDecimal cdCaso, String cdAlerta, String cdSistema) throws Exception{
		List<MecanicaOperacionalConceptoDTO> lstTablaMesTresDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaMesTresAnalisis"));
		 query.setParameter(1,cdCaso);
		 	query.setParameter(2,cdAlerta); 
		 	query.setParameter(3,cdSistema);
		 	query.setParameter(4,cdAlerta);
		 	query.setParameter(5,cdSistema);
		 	query.setParameter(6,cdAlerta);
		 	query.setParameter(7,cdSistema);
		 	query.setParameter(8,cdCaso);
		 	query.setParameter(9,cdAlerta); 
		 	query.setParameter(10,cdSistema);
		 	query.setParameter(11,cdAlerta);
		 	query.setParameter(12,cdSistema);
		 	query.setParameter(13,cdAlerta);
		 	query.setParameter(14,cdSistema);
		 	query.setParameter(15,cdCaso);
		 	query.setParameter(16,cdAlerta);
		 	query.setParameter(17,cdSistema);
		 	query.setParameter(18,cdAlerta);
		 	query.setParameter(19,cdSistema);
		 	query.setParameter(20,cdAlerta);
		 	query.setParameter(21,cdSistema);
		 	query.setParameter(22,cdCaso); 
		 	query.setParameter(23,cdAlerta);
		 	query.setParameter(24,cdSistema);
		 	query.setParameter(25,cdAlerta);
		 	query.setParameter(26,cdSistema);
		 	query.setParameter(27,cdAlerta);
		 	query.setParameter(28,cdSistema);
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTM3", StringType.INSTANCE)
	        .addScalar("nuMovimientosCTM3")
	        .addScalar("imMontoCTM3")
	        .addScalar("nuPorcentajeCTM3")
	        .addScalar("nuPromedioCTM3")
	        .addScalar("nuMovimientosATM3")
	        .addScalar("imMontoATM3")
	        .addScalar("nuPorcentajeATM3")
	        .addScalar("nuPromedioATM3")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		 lstTablaMesTresDTO = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();
		return lstTablaMesTresDTO;
	}

	
	//TABLA DE MES CUATRO
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> consultarConceptosMesCuatro(BigDecimal cdCaso, String cdAlerta, String cdSistema) throws Exception{
		List<MecanicaOperacionalConceptoDTO> lstTablaMesCuatroDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaMesCuatroAnalisis"));
		 query.setParameter(1,cdCaso);
		 	query.setParameter(2,cdAlerta); 
		 	query.setParameter(3,cdSistema);
		 	query.setParameter(4,cdAlerta);
		 	query.setParameter(5,cdSistema);
		 	query.setParameter(6,cdAlerta);
		 	query.setParameter(7,cdSistema);
		 	query.setParameter(8,cdCaso);
		 	query.setParameter(9,cdAlerta); 
		 	query.setParameter(10,cdSistema);
		 	query.setParameter(11,cdAlerta);
		 	query.setParameter(12,cdSistema);
		 	query.setParameter(13,cdAlerta);
		 	query.setParameter(14,cdSistema);
		 	query.setParameter(15,cdCaso);
		 	query.setParameter(16,cdAlerta);
		 	query.setParameter(17,cdSistema);
		 	query.setParameter(18,cdAlerta);
		 	query.setParameter(19,cdSistema);
		 	query.setParameter(20,cdAlerta);
		 	query.setParameter(21,cdSistema);
		 	query.setParameter(22,cdCaso); 
		 	query.setParameter(23,cdAlerta);
		 	query.setParameter(24,cdSistema);
		 	query.setParameter(25,cdAlerta);
		 	query.setParameter(26,cdSistema);
		 	query.setParameter(27,cdAlerta);
		 	query.setParameter(28,cdSistema);
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTM4", StringType.INSTANCE)
	        .addScalar("nuMovimientosCTM4")
	        .addScalar("imMontoCTM4")
	        .addScalar("nuPorcentajeCTM4")
	        .addScalar("nuPromedioCTM4")
	        .addScalar("nuMovimientosATM4")
	        .addScalar("imMontoATM4")
	        .addScalar("nuPorcentajeATM4")
	        .addScalar("nuPromedioATM4")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		 lstTablaMesCuatroDTO = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();
		return lstTablaMesCuatroDTO;
	}*/
		
	/*@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> consultarMecanicaOperacionalConceptoAnalisis(BigDecimal cdCaso, String cdAlerta,String cdSistema) throws Exception{
		List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConceptoDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaCuatrimestralAnalisis"));
		 	query.setParameter(1,cdCaso);
		 	query.setParameter(2,cdAlerta); 
		 	query.setParameter(3,cdSistema);
		 	query.setParameter(4,cdAlerta);
		 	query.setParameter(5,cdSistema);
		 	query.setParameter(6,cdAlerta);
		 	query.setParameter(7,cdSistema);
		 	query.setParameter(8,cdCaso);
		 	query.setParameter(9,cdAlerta); 
		 	query.setParameter(10,cdSistema);
		 	query.setParameter(11,cdAlerta);
		 	query.setParameter(12,cdSistema);
		 	query.setParameter(13,cdAlerta);
		 	query.setParameter(14,cdSistema);
		 	query.setParameter(15,cdCaso);
		 	query.setParameter(16,cdAlerta);
		 	query.setParameter(17,cdSistema);
		 	query.setParameter(18,cdAlerta);
		 	query.setParameter(19,cdSistema);
		 	query.setParameter(20,cdAlerta);
		 	query.setParameter(21,cdSistema);
		 	query.setParameter(22,cdCaso); 
		 	query.setParameter(23,cdAlerta);
		 	query.setParameter(24,cdSistema);
		 	query.setParameter(25,cdAlerta);
		 	query.setParameter(26,cdSistema);
		 	query.setParameter(27,cdAlerta);
		 	query.setParameter(28,cdSistema);
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTC")
	        .addScalar("nuMovimientosTC")
	        .addScalar("imMontoTC")
	        .addScalar("nuPorcentajeTC")
	        .addScalar("nuPromedioTC")
	        .addScalar("nuMovimientosATC")
	        .addScalar("imMontoATC")
	        .addScalar("nuPorcentajeATC")
	        .addScalar("nuPromedioATC")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		 lstMecanicaOperacionalConceptoDTO = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();
		return lstMecanicaOperacionalConceptoDTO;
	}*/
	
	//TABLA DE MES UNO
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> consultarConceptosMesUnoAnalisis(BigDecimal cdCaso,String cdAlerta,String cdSistema) throws Exception{
		List<MecanicaOperacionalConceptoDTO> lstTablaMesUnoDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaMesUnoAnalisis"));
		 
		 query.setParameter(1,cdCaso);
		 	query.setParameter(2,cdAlerta); 
		 	query.setParameter(3,cdSistema);
		 	query.setParameter(4,cdAlerta);
		 	query.setParameter(5,cdSistema);
		 	
		 	query.setParameter(6,cdCaso);
		 	query.setParameter(7,cdAlerta); 
		 	query.setParameter(8,cdSistema);
		 	query.setParameter(9,cdAlerta);
		 	query.setParameter(10,cdSistema);
		 	
		 	query.setParameter(11,cdCaso);
		 	query.setParameter(12,cdAlerta);
		 	query.setParameter(13,cdSistema);
		 	query.setParameter(14,cdAlerta);
		 	query.setParameter(15,cdSistema);
		 	
		 	query.setParameter(16,cdCaso); 
		 	query.setParameter(17,cdAlerta);
		 	query.setParameter(18,cdSistema);
		 	query.setParameter(19,cdAlerta);
		 	query.setParameter(20,cdSistema);
		 	
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTM1")
	        .addScalar("nuMovimientosCTM1")
	        .addScalar("imMontoCTM1")
	        .addScalar("nuPorcentajeCTM1")
	        .addScalar("nuPromedioCTM1")
	        .addScalar("nuMovimientosATM1")
	        .addScalar("imMontoATM1")
	        .addScalar("nuPorcentajeATM1")
	        .addScalar("nuPromedioATM1")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		 lstTablaMesUnoDTO = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();
		return lstTablaMesUnoDTO;
	}

	
	//TABLA DE MES DOS
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> consultarConceptosMesDosAnalisis(BigDecimal cdCaso,String cdAlerta,String cdSistema) throws Exception{
		List<MecanicaOperacionalConceptoDTO> lstTablaMesDosDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaMesDosAnalisis"));
		 
		 query.setParameter(1,cdCaso);
		 	query.setParameter(2,cdAlerta); 
		 	query.setParameter(3,cdSistema);
		 	query.setParameter(4,cdAlerta);
		 	query.setParameter(5,cdSistema);
		 	
		 	query.setParameter(6,cdCaso);
		 	query.setParameter(7,cdAlerta); 
		 	query.setParameter(8,cdSistema);
		 	query.setParameter(9,cdAlerta);
		 	query.setParameter(10,cdSistema);
		 	
		 	query.setParameter(11,cdCaso);
		 	query.setParameter(12,cdAlerta);
		 	query.setParameter(13,cdSistema);
		 	query.setParameter(14,cdAlerta);
		 	query.setParameter(15,cdSistema);
		 	
		 	query.setParameter(16,cdCaso); 
		 	query.setParameter(17,cdAlerta);
		 	query.setParameter(18,cdSistema);
		 	query.setParameter(19,cdAlerta);
		 	query.setParameter(20,cdSistema);
		 	
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTM2")
	        .addScalar("nuMovimientosCTM2")
	        .addScalar("imMontoCTM2")
	        .addScalar("nuPorcentajeCTM2")
	        .addScalar("nuPromedioCTM2")
	        .addScalar("nuMovimientosATM2")
	        .addScalar("imMontoATM2")
	        .addScalar("nuPorcentajeATM2")
	        .addScalar("nuPromedioATM2")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		 lstTablaMesDosDTO = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();
		return lstTablaMesDosDTO;
	}

	
	//TABLA DE MES TRES
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> consultarConceptosMesTresAnalisis(BigDecimal cdCaso,String cdAlerta,String cdSistema) throws Exception{
		List<MecanicaOperacionalConceptoDTO> lstTablaMesTresDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaMesTresAnalisis"));
		 
		 query.setParameter(1,cdCaso);
		 	query.setParameter(2,cdAlerta); 
		 	query.setParameter(3,cdSistema);
		 	query.setParameter(4,cdAlerta);
		 	query.setParameter(5,cdSistema);
		 	
		 	query.setParameter(6,cdCaso);
		 	query.setParameter(7,cdAlerta); 
		 	query.setParameter(8,cdSistema);
		 	query.setParameter(9,cdAlerta);
		 	query.setParameter(10,cdSistema);
		 	
		 	query.setParameter(11,cdCaso);
		 	query.setParameter(12,cdAlerta);
		 	query.setParameter(13,cdSistema);
		 	query.setParameter(14,cdAlerta);
		 	query.setParameter(15,cdSistema);
		 	
		 	query.setParameter(16,cdCaso); 
		 	query.setParameter(17,cdAlerta);
		 	query.setParameter(18,cdSistema);
		 	query.setParameter(19,cdAlerta);
		 	query.setParameter(20,cdSistema);
		 	
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTM3")
	        .addScalar("nuMovimientosCTM3")
	        .addScalar("imMontoCTM3")
	        .addScalar("nuPorcentajeCTM3")
	        .addScalar("nuPromedioCTM3")
	        .addScalar("nuMovimientosATM3")
	        .addScalar("imMontoATM3")
	        .addScalar("nuPorcentajeATM3")
	        .addScalar("nuPromedioATM3")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		 lstTablaMesTresDTO = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();
		return lstTablaMesTresDTO;
	}

	
	//TABLA DE MES CUATRO
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> consultarConceptosMesCuatroAnalisis(BigDecimal cdCaso,String cdAlerta,String cdSistema) throws Exception{
		List<MecanicaOperacionalConceptoDTO> lstTablaMesCuatroDTO = new ArrayList<MecanicaOperacionalConceptoDTO>();
		 Query query = em.createNativeQuery(Consultas.getConsulta("getTablaMesCuatroAnalisis"));
		 
		 query.setParameter(1,cdCaso);
		 	query.setParameter(2,cdAlerta); 
		 	query.setParameter(3,cdSistema);
		 	query.setParameter(4,cdAlerta);
		 	query.setParameter(5,cdSistema);
		 	
		 	query.setParameter(6,cdCaso);
		 	query.setParameter(7,cdAlerta); 
		 	query.setParameter(8,cdSistema);
		 	query.setParameter(9,cdAlerta);
		 	query.setParameter(10,cdSistema);
		 	
		 	query.setParameter(11,cdCaso);
		 	query.setParameter(12,cdAlerta);
		 	query.setParameter(13,cdSistema);
		 	query.setParameter(14,cdAlerta);
		 	query.setParameter(15,cdSistema);
		 	
		 	query.setParameter(16,cdCaso); 
		 	query.setParameter(17,cdAlerta);
		 	query.setParameter(18,cdSistema);
		 	query.setParameter(19,cdAlerta);
		 	query.setParameter(20,cdSistema);
		 	
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nbConceptoTM4")
	        .addScalar("nuMovimientosCTM4")
	        .addScalar("imMontoCTM4")
	        .addScalar("nuPorcentajeCTM4")
	        .addScalar("nuPromedioCTM4")
	        .addScalar("nuMovimientosATM4")
	        .addScalar("imMontoATM4")
	        .addScalar("nuPorcentajeATM4")
	        .addScalar("nuPromedioATM4")	
	        .setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		 lstTablaMesCuatroDTO = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();
		return lstTablaMesCuatroDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> getCabeceraCuatrimestral(String cdAlerta,String cdSistema) throws Exception{

		List<MecanicaOperacionalConceptoDTO>  cabeceraCuatrimestral = new ArrayList<MecanicaOperacionalConceptoDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("cabeceraCuatrimestral"));
		query.setParameter(1,cdAlerta);
		query.setParameter(2,cdSistema);

		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("inicio")
		.addScalar("fin")
		.setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		
		cabeceraCuatrimestral = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();		
		
		return cabeceraCuatrimestral;
	}
	
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> getCabeceraMesUno(String cdAlerta,String cdSistema) throws Exception{

		List<MecanicaOperacionalConceptoDTO>  cabeceraCuatrimestral = new ArrayList<MecanicaOperacionalConceptoDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("cabeceraMesUno"));
		query.setParameter(1,cdAlerta);
		query.setParameter(2,cdSistema);

		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("mes")
		.setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		
		cabeceraCuatrimestral = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();		
		
		return cabeceraCuatrimestral;
	}
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> getCabeceraMesDos(String cdAlerta,String cdSistema) throws Exception{

		List<MecanicaOperacionalConceptoDTO>  cabeceraCuatrimestral = new ArrayList<MecanicaOperacionalConceptoDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("cabeceraMesDos"));
		query.setParameter(1,cdAlerta);
		query.setParameter(2,cdSistema);

		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("mes2")
		.setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		
		cabeceraCuatrimestral = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();		
		
		return cabeceraCuatrimestral;
	}
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> getCabeceraMesTres(String cdAlerta,String cdSistema) throws Exception{

		List<MecanicaOperacionalConceptoDTO>  cabeceraCuatrimestral = new ArrayList<MecanicaOperacionalConceptoDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("cabeceraMesTres"));
		query.setParameter(1,cdAlerta);
		query.setParameter(2,cdSistema);

		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("mes3")
		.setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		
		cabeceraCuatrimestral = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();		
		
		return cabeceraCuatrimestral;
	}
	@SuppressWarnings("unchecked")
	public List<MecanicaOperacionalConceptoDTO> getCabeceraMesCuatro(String cdAlerta,String cdSistema) throws Exception{

		List<MecanicaOperacionalConceptoDTO>  cabeceraCuatrimestral = new ArrayList<MecanicaOperacionalConceptoDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("cabeceraMesCuatro"));
		query.setParameter(1,cdAlerta);
		query.setParameter(2,cdSistema);

		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("mes4")
		.setResultTransformer(Transformers.aliasToBean(MecanicaOperacionalConceptoDTO.class));
		
		cabeceraCuatrimestral = (List<MecanicaOperacionalConceptoDTO>)query.getResultList();		
		
		return cabeceraCuatrimestral;
	}
}