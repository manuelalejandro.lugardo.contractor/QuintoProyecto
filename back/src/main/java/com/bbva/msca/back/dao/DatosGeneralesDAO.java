package com.bbva.msca.back.dao;

import com.bbva.msca.back.dto.DatosGeneralesDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;

public class DatosGeneralesDAO {

	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();

	@SuppressWarnings("unchecked")
	public DatosGeneralesDTO getDatosGenerales(BigDecimal cdCaso,
			String nuCliente) throws Exception {

		DatosGeneralesDTO datosGeneralesDTO = null;
		List<DatosGeneralesDTO> resultado = new ArrayList<DatosGeneralesDTO>();
		Query query = em.createNativeQuery(Consultas
				.getConsulta("getConsultarDatosGenerales"));
		query.setParameter(1, cdCaso);
		query.setParameter(2, nuCliente);

		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("nbCliente")
				.addScalar("nuCliente", StringType.INSTANCE)
				.addScalar("nbCiudad")
				.addScalar("nbDomicilio")
				.addScalar("nbRFC")
				.addScalar("nbTelParticular")
				.addScalar("nbTelOficina")
				.addScalar("nbNacionalidad")
				.addScalar("fhNacimiento")
				.addScalar("nbSector", StringType.INSTANCE)
				.addScalar("nuAntiguedad")
				.addScalar("nbActividad")
				.setResultTransformer(
						Transformers.aliasToBean(DatosGeneralesDTO.class));
		resultado = (List<DatosGeneralesDTO>) query.getResultList();
		if (resultado != null && !resultado.isEmpty()) {
			datosGeneralesDTO = resultado.get(0);
		}
		return datosGeneralesDTO;
	}

}
