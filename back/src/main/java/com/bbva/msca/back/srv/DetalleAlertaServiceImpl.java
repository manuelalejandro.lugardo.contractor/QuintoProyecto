package com.bbva.msca.back.srv;

import com.bbva.msca.back.dao.DetalleAlertaDAO;
import com.bbva.msca.back.dao.DetalleTipologiaDAO;
import com.bbva.msca.back.dao.EscenariosDAO;
import com.bbva.msca.back.dto.DetalleAlertaDTO;
import com.bbva.msca.back.dto.DetalleTipologiaDTO;
import com.bbva.msca.back.util.ResponseGeneric;
import com.bbva.msca.back.util.MensajesI18n;

public class DetalleAlertaServiceImpl implements DetalleAlertaService{
	
	@Override
	public ResponseGeneric consultarDetalleAlertas() {
		ResponseGeneric response = new ResponseGeneric();
		DetalleAlertaDTO detalleAlertaDTO = DetalleAlertaDAO.getDetalleAlerta();
		
		response.setResponse(detalleAlertaDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarDetalleTipologia() {
		ResponseGeneric response = new ResponseGeneric();
		DetalleTipologiaDTO detalleTipologiaDTO = DetalleTipologiaDAO.getDetalleTipologia();
		
		response.setResponse(detalleTipologiaDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarEscenarios() {
		ResponseGeneric response = new ResponseGeneric();
		EscenariosDAO escenariosDAO = new EscenariosDAO();
		try{	
			response.setResponse(escenariosDAO.getEscenarios());
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

}
