package com.bbva.msca.back.dto;

import java.util.Date;

public class CuentasClienteFidDTO {
	
	private Date fhApertura;
	private Integer nuCuenta;
	private String nbProducto;
	private String nbSubproducto;
	private String nbEstatus;
	private Date fhCancelacion;
	private String nbTitular;
	private Integer nuCtePart;
	private String nbParticipes;
	private double imProducto;
	private double imProProm;
	
	
	public Date getFhApertura() {
		return fhApertura;
	}
	public void setFhApertura(Date fhApertura) {
		this.fhApertura = fhApertura;
	}
	public Integer getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(Integer nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public String getNbProducto() {
		return nbProducto;
	}
	public void setNbProducto(String nbProducto) {
		this.nbProducto = nbProducto;
	}
	public String getNbSubproducto() {
		return nbSubproducto;
	}
	public void setNbSubproducto(String nbSubproducto) {
		this.nbSubproducto = nbSubproducto;
	}
	public String getNbEstatus() {
		return nbEstatus;
	}
	public void setNbEstatus(String nbEstatus) {
		this.nbEstatus = nbEstatus;
	}
	public Date getFhCancelacion() {
		return fhCancelacion;
	}
	public void setFhCancelacion(Date fhCancelacion) {
		this.fhCancelacion = fhCancelacion;
	}
	public String getNbTitular() {
		return nbTitular;
	}
	public void setNbTitular(String nbTitular) {
		this.nbTitular = nbTitular;
	}
	public Integer getNuCtePart() {
		return nuCtePart;
	}
	public void setNuCtePart(Integer nuCtePart) {
		this.nuCtePart = nuCtePart;
	}
	public String getNbParticipes() {
		return nbParticipes;
	}
	public void setNbParticipes(String nbParticipes) {
		this.nbParticipes = nbParticipes;
	}
	public double getImProducto() {
		return imProducto;
	}
	public void setImProducto(double imProducto) {
		this.imProducto = imProducto;
	}
	public double getImProProm() {
		return imProProm;
	}
	public void setImProProm(double imProProm) {
		this.imProProm = imProProm;
	}

}
