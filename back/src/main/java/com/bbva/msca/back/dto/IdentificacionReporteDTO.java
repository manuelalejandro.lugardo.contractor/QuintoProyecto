package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class IdentificacionReporteDTO {

	private String nuFolioAlertaIR;
	private String nbSistemaIR;
	private String nuCuentaIR;
	private BigDecimal montoIR;
	private String divisaIR;
	private String montoDivisaIR;
	private Date fhAlertaIR;
	private Date fhEnvioIR;
	private String cdSesionIR;
	
	private String nbOficinaGestoraIR;
	private String nbRiesgoIR;
	private String nbMercadoIR;
	private String nbDivisionIR;
	private String nbFuncionarioIR;
	

	private String nuFolioAlertaIR2;
	private String nbSistemaIR2;
	private String nuCuentaIR2;
	private BigDecimal montoIR2;
	private String divisaIR2;
	private String montoDivisaIR2;
	private Date fhAlertaIR2;
	private Date fhEnvioIR2;
	private String cdSesionIR2;
	
	private String nbOficinaGestoraIR2;
	private String nbRiesgoIR2;
	private String nbMercadoIR2;
	private String nbDivisionIR2;
	private String nbFuncionarioIR2;
	
	public void setNuFolioAlertaIR(String nuFolioAlertaIR) {
		this.nuFolioAlertaIR = nuFolioAlertaIR;
	}
	public String getNuFolioAlertaIR() {
		return nuFolioAlertaIR;
	}
	public void setNbSistemaIR(String nbSistemaIR) {
		this.nbSistemaIR = nbSistemaIR;
	}
	public String getNbSistemaIR() {
		return nbSistemaIR;
	}
	public void setNuCuentaIR(String nuCuentaIR) {
		this.nuCuentaIR = nuCuentaIR;
	}
	public String getNuCuentaIR() {
		return nuCuentaIR;
	}
	public void setFhEnvioIR(Date fhEnvioIR) {
		this.fhEnvioIR = fhEnvioIR;
	}
	public Date getFhEnvioIR() {
		return fhEnvioIR;
	}
	public void setFhAlertaIR(Date fhAlertaIR) {
		this.fhAlertaIR = fhAlertaIR;
	}
	public Date getFhAlertaIR() {
		return fhAlertaIR;
	}
	public void setCdSesionIR(String cdSesionIR) {
		this.cdSesionIR = cdSesionIR;
	}
	public String getCdSesionIR() {
		return cdSesionIR;
	}
	public void setMontoIR(BigDecimal montoIR) {
		this.montoIR = montoIR;
	}
	public BigDecimal getMontoIR() {
		return montoIR;
	}
	public void setDivisaIR(String divisaIR) {
		this.divisaIR = divisaIR;
	}
	public String getDivisaIR() {
		return divisaIR;
	}
	public void setMontoDivisaIR(String montoDivisaIR) {
		this.montoDivisaIR = montoDivisaIR;
	}
	public String getMontoDivisaIR() {
		return montoDivisaIR;
	}
	public void setNbOficinaGestoraIR(String nbOficinaGestoraIR) {
		this.nbOficinaGestoraIR = nbOficinaGestoraIR;
	}
	public String getNbOficinaGestoraIR() {
		return nbOficinaGestoraIR;
	}
	public void setNbRiesgoIR(String nbRiesgoIR) {
		this.nbRiesgoIR = nbRiesgoIR;
	}
	public String getNbRiesgoIR() {
		return nbRiesgoIR;
	}
	public void setNbMercadoIR(String nbMercadoIR) {
		this.nbMercadoIR = nbMercadoIR;
	}
	public String getNbMercadoIR() {
		return nbMercadoIR;
	}
	public void setNbDivisionIR(String nbDivisionIR) {
		this.nbDivisionIR = nbDivisionIR;
	}
	public String getNbDivisionIR() {
		return nbDivisionIR;
	}
	public void setNbFuncionarioIR(String nbFuncionarioIR) {
		this.nbFuncionarioIR = nbFuncionarioIR;
	}
	public String getNbFuncionarioIR() {
		return nbFuncionarioIR;
	}
	public String getNuFolioAlertaIR2() {
		return nuFolioAlertaIR2;
	}
	public void setNuFolioAlertaIR2(String nuFolioAlertaIR2) {
		this.nuFolioAlertaIR2 = nuFolioAlertaIR2;
	}
	public String getNbSistemaIR2() {
		return nbSistemaIR2;
	}
	public void setNbSistemaIR2(String nbSistemaIR2) {
		this.nbSistemaIR2 = nbSistemaIR2;
	}
	public String getNuCuentaIR2() {
		return nuCuentaIR2;
	}
	public void setNuCuentaIR2(String nuCuentaIR2) {
		this.nuCuentaIR2 = nuCuentaIR2;
	}
	public BigDecimal getMontoIR2() {
		return montoIR2;
	}
	public void setMontoIR2(BigDecimal montoIR2) {
		this.montoIR2 = montoIR2;
	}
	public String getDivisaIR2() {
		return divisaIR2;
	}
	public void setDivisaIR2(String divisaIR2) {
		this.divisaIR2 = divisaIR2;
	}
	public String getMontoDivisaIR2() {
		return montoDivisaIR2;
	}
	public void setMontoDivisaIR2(String montoDivisaIR2) {
		this.montoDivisaIR2 = montoDivisaIR2;
	}
	public Date getFhAlertaIR2() {
		return fhAlertaIR2;
	}
	public void setFhAlertaIR2(Date fhAlertaIR2) {
		this.fhAlertaIR2 = fhAlertaIR2;
	}
	public Date getFhEnvioIR2() {
		return fhEnvioIR2;
	}
	public void setFhEnvioIR2(Date fhEnvioIR2) {
		this.fhEnvioIR2 = fhEnvioIR2;
	}
	public String getCdSesionIR2() {
		return cdSesionIR2;
	}
	public void setCdSesionIR2(String cdSesionIR2) {
		this.cdSesionIR2 = cdSesionIR2;
	}
	public String getNbOficinaGestoraIR2() {
		return nbOficinaGestoraIR2;
	}
	public void setNbOficinaGestoraIR2(String nbOficinaGestoraIR2) {
		this.nbOficinaGestoraIR2 = nbOficinaGestoraIR2;
	}
	public String getNbRiesgoIR2() {
		return nbRiesgoIR2;
	}
	public void setNbRiesgoIR2(String nbRiesgoIR2) {
		this.nbRiesgoIR2 = nbRiesgoIR2;
	}
	public String getNbMercadoIR2() {
		return nbMercadoIR2;
	}
	public void setNbMercadoIR2(String nbMercadoIR2) {
		this.nbMercadoIR2 = nbMercadoIR2;
	}
	public String getNbDivisionIR2() {
		return nbDivisionIR2;
	}
	public void setNbDivisionIR2(String nbDivisionIR2) {
		this.nbDivisionIR2 = nbDivisionIR2;
	}
	public String getNbFuncionarioIR2() {
		return nbFuncionarioIR2;
	}
	public void setNbFuncionarioIR2(String nbFuncionarioIR2) {
		this.nbFuncionarioIR2 = nbFuncionarioIR2;
	}


	
	
}
