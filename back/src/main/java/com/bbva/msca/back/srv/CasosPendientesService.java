package com.bbva.msca.back.srv;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;

import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import com.bbva.msca.back.dto.ParametrosBusquedaCasoDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/CasosPendientesService")

public interface CasosPendientesService {
	
	@GET
	@Path("/consultaCasosAsignadosConsultor/{cdConsultor}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultaCasosAsignadosConsultor(@PathParam("cdConsultor") String cdConsultor,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset,
			@QueryParam("search") String search,
			@QueryParam("name") String name,
			@QueryParam("order") String order );

}