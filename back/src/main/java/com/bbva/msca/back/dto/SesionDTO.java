package com.bbva.msca.back.dto;

import java.util.Date;

public class SesionDTO {

	private String cdSesion;
	private Date fhPeIn;
	private Date fhPeFn;
	private String cdUsrAlta;
	private Date fhAlta;
	private String cdStSistema;
	private Date fhLimRepAut;
	private String nbUsrRed;

	public void setCdSesion(String cdSesion) {
		this.cdSesion = cdSesion;
	}

	public String getCdSesion() {
		return cdSesion;
	}

	public void setFhPeIn(Date fhPeIn) {
		this.fhPeIn = fhPeIn;
	}

	public Date getFhPeIn() {
		return fhPeIn;
	}

	public void setFhPeFn(Date fhPeFn) {
		this.fhPeFn = fhPeFn;
	}

	public Date getFhPeFn() {
		return fhPeFn;
	}

	public void setCdUsrAlta(String cdUsrAlta) {
		this.cdUsrAlta = cdUsrAlta;
	}

	public String getCdUsrAlta() {
		return cdUsrAlta;
	}

	public void setFhAlta(Date fhAlta) {
		this.fhAlta = fhAlta;
	}

	public Date getFhAlta() {
		return fhAlta;
	}

	public void setCdStSistema(String cdStSistema) {
		this.cdStSistema = cdStSistema;
	}

	public String getCdStSistema() {
		return cdStSistema;
	}

	public void setFhLimRepAut(Date fhLimRepAut) {
		this.fhLimRepAut = fhLimRepAut;
	}

	public Date getFhLimRepAut() {
		return fhLimRepAut;
	}

	public void setNbUsrRed(String nbUsrRed) {
		this.nbUsrRed = nbUsrRed;
	}

	public String getNbUsrRed() {
		return nbUsrRed;
	}

}
