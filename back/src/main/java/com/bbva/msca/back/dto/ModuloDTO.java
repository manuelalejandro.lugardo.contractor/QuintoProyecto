package com.bbva.msca.back.dto;


public class ModuloDTO {
	private String cdModulo;
	private String nbModulo;
	private String txModulo;
	
	public String getCdModulo() {
		return cdModulo;
	}
	public void setCdModulo(String cdModulo) {
		this.cdModulo = cdModulo;
	}
	public String getNbModulo() {
		return nbModulo;
	}
	public void setNbModulo(String nbModulo) {
		this.nbModulo = nbModulo;
	}
	public String getTxModulo() {
		return txModulo;
	}
	public void setTxModulo(String txModulo) {
		this.txModulo = txModulo;
	}
	
}
