package com.bbva.msca.back.srv;

import java.math.BigDecimal;
import java.util.List;

import com.bbva.msca.back.dto.AlertaDTO;
import com.bbva.msca.back.dto.UsuarioCasoDTO;

import com.bbva.msca.back.dao.AsignacionCasosDAO;

import com.bbva.msca.back.dao.AsignacionAlertasDAO;
import com.bbva.msca.back.dto.AsignacionAlertasDTO;

import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;
import com.bbva.msca.back.util.ResponseGenericPagination;

public class AsignacionCasosServiceImpl implements AsignacionCasosService{
	
	@Override
	public ResponseGeneric consultarAlertasPendientesAsignacionGerencia(BigDecimal cdGerencia, Integer limit, Integer offset,String search, String name, String order) {
		ResponseGeneric response = new ResponseGeneric();
		ResponseGenericPagination pagination = new ResponseGenericPagination();
		AsignacionCasosDAO asignacionCasosDAO = new AsignacionCasosDAO();
		
		try{
			pagination.setRows(asignacionCasosDAO.getConsultaAlertasPendientesAsignacionGerencia(cdGerencia,limit,offset,search,name,order));
			pagination.setTotal(asignacionCasosDAO.getNuAlertasPendientesAsignacionGerencia(cdGerencia,search));
			response.setResponse(pagination);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarCasosAsignadosConsultor(BigDecimal cdConsultor, Integer limit, Integer offset,String search, String name, String order) {
		ResponseGeneric response = new ResponseGeneric();
		ResponseGenericPagination pagination = new ResponseGenericPagination();
		AsignacionCasosDAO asignacionCasosDAO = new AsignacionCasosDAO();
		
		try{
			pagination.setRows(asignacionCasosDAO.getConsultaCasosAsignadosConsultor(cdConsultor,limit,offset,search,name,order));
			pagination.setTotal(asignacionCasosDAO.getNuCasosAsignadosConsultor(cdConsultor));
			response.setResponse(pagination);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarAlertasAsignadas() {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionAlertasDAO asignacionAlertasDAO = new AsignacionAlertasDAO();
		List<AsignacionAlertasDTO> lstAsignacionAlertasDTO = asignacionAlertasDAO.getAsignacionCasos();
		
		response.setResponse(lstAsignacionAlertasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}

	@Override
	public ResponseGeneric consultarConsultoresSupervisor(BigDecimal cdSupervisor) {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionCasosDAO asignacionCasosDAO = new AsignacionCasosDAO();
		List<UsuarioCasoDTO> lstUsuarioDTO;
		try {
			lstUsuarioDTO = asignacionCasosDAO.getConsultoresSupervisor(cdSupervisor);
			response.setResponse(lstUsuarioDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	@Override
	public ResponseGeneric asignarAlertasCasos(List<AlertaDTO> lstAlertaDTO, String cdUsuario,String cdPrioriAsig) {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionCasosDAO asignacionCasosDAO = new AsignacionCasosDAO();
		
		try {
			int res = asignacionCasosDAO.addAlertasCaso(lstAlertaDTO, cdUsuario, cdPrioriAsig);
			response.setResponse(res);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric descartarAlertas(List<AlertaDTO> lstAlertaDTO) {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionCasosDAO asignacionCasosDAO = new AsignacionCasosDAO();
		
		try {
			int res = asignacionCasosDAO.setDescartarAlertas(lstAlertaDTO);
			response.setResponse(res);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric reasignarCasos(List<AlertaDTO> lstAlertaDTO, String cdUsuario) {
		
		ResponseGeneric response = new ResponseGeneric();
		AsignacionCasosDAO asignacionCasosDAO = new AsignacionCasosDAO();
		
		try {
			int res = asignacionCasosDAO.addCasosConsultor(lstAlertaDTO, cdUsuario);
			response.setResponse(res);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
}
