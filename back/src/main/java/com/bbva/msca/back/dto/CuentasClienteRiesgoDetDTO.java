package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class CuentasClienteRiesgoDetDTO {

	private BigDecimal cdClialtrie;
	private BigDecimal nuId;
	private String cdCliente;
	private String cdTipologia;
	private String cdTpPersona;
	private String nbTpPersona;

	private String nuCuenta;
	private String cdCaracter;
	private String nbNombre;
	private String nbApPaterno;
	private String nbApMaterno;
	private Date fhNacimiento;
	private String cdRFC;
	private String nbDomicilio;
	private String nbComplementarios;
	private String cdClienteBancomer;
	private String nuCliente;
	private String cdDictamen;
	private BigDecimal cdCaso;

	/**
	 * @return the cdClialtrie
	 */
	public BigDecimal getCdClialtrie() {
		return cdClialtrie;
	}

	/**
	 * @param cdClialtrie
	 *            the cdClialtrie to set
	 */
	public void setCdClialtrie(BigDecimal cdClialtrie) {
		this.cdClialtrie = cdClialtrie;
	}

	/**
	 * @return the nuId
	 */
	public BigDecimal getNuId() {
		return nuId;
	}

	/**
	 * @param nuId
	 *            the nuId to set
	 */
	public void setNuId(BigDecimal nuId) {
		this.nuId = nuId;
	}

	/**
	 * @return the cdCliente
	 */
	public String getCdCliente() {
		return cdCliente;
	}

	/**
	 * @param cdCliente
	 *            the cdCliente to set
	 */
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}

	/**
	 * @return the cdTipologia
	 */
	public String getCdTipologia() {
		return cdTipologia;
	}

	/**
	 * @param cdTipologia
	 *            the cdTipologia to set
	 */
	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}

	/**
	 * @return the cdTpPersona
	 */
	public String getCdTpPersona() {
		return cdTpPersona;
	}

	/**
	 * @param cdTpPersona
	 *            the cdTpPersona to set
	 */
	public void setCdTpPersona(String cdTpPersona) {
		this.cdTpPersona = cdTpPersona;
	}

	/**
	 * @return the nuCuenta
	 */
	public String getNuCuenta() {
		return nuCuenta;
	}

	/**
	 * @param nuCuenta
	 *            the nuCuenta to set
	 */
	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}

	/**
	 * @return the cdCaracter
	 */
	public String getCdCaracter() {
		return cdCaracter;
	}

	/**
	 * @param cdCaracter
	 *            the cdCaracter to set
	 */
	public void setCdCaracter(String cdCaracter) {
		this.cdCaracter = cdCaracter;
	}

	/**
	 * @return the nbNombre
	 */
	public String getNbNombre() {
		return nbNombre;
	}

	/**
	 * @param nbNombre
	 *            the nbNombre to set
	 */
	public void setNbNombre(String nbNombre) {
		this.nbNombre = nbNombre;
	}

	/**
	 * @return the nbApPaterno
	 */
	public String getNbApPaterno() {
		return nbApPaterno;
	}

	/**
	 * @param nbApPaterno
	 *            the nbApPaterno to set
	 */
	public void setNbApPaterno(String nbApPaterno) {
		this.nbApPaterno = nbApPaterno;
	}

	/**
	 * @return the nbApMaterno
	 */
	public String getNbApMaterno() {
		return nbApMaterno;
	}

	/**
	 * @param nbApMaterno
	 *            the nbApMaterno to set
	 */
	public void setNbApMaterno(String nbApMaterno) {
		this.nbApMaterno = nbApMaterno;
	}

	/**
	 * @return the fhNacimiento
	 */
	public Date getFhNacimiento() {
		return fhNacimiento;
	}

	/**
	 * @param fhNacimiento
	 *            the fhNacimiento to set
	 */
	public void setFhNacimiento(Date fhNacimiento) {
		this.fhNacimiento = fhNacimiento;
	}

	/**
	 * @return the cdRFC
	 */
	public String getCdRFC() {
		return cdRFC;
	}

	/**
	 * @param cdRFC
	 *            the cdRFC to set
	 */
	public void setCdRFC(String cdRFC) {
		this.cdRFC = cdRFC;
	}

	/**
	 * @return the nbDomicilio
	 */
	public String getNbDomicilio() {
		return nbDomicilio;
	}

	/**
	 * @param nbDomicilio
	 *            the nbDomicilio to set
	 */
	public void setNbDomicilio(String nbDomicilio) {
		this.nbDomicilio = nbDomicilio;
	}

	/**
	 * @return the nbComplementarios
	 */
	public String getNbComplementarios() {
		return nbComplementarios;
	}

	/**
	 * @param nbComplementarios
	 *            the nbComplementarios to set
	 */
	public void setNbComplementarios(String nbComplementarios) {
		this.nbComplementarios = nbComplementarios;
	}

	/**
	 * @return the cdClienteBancomer
	 */
	public String getCdClienteBancomer() {
		return cdClienteBancomer;
	}

	/**
	 * @param cdClienteBancomer
	 *            the cdClienteBancomer to set
	 */
	public void setCdClienteBancomer(String cdClienteBancomer) {
		this.cdClienteBancomer = cdClienteBancomer;
	}

	/**
	 * @return the nuCliente
	 */
	public String getNuCliente() {
		return nuCliente;
	}

	/**
	 * @param nuCliente
	 *            the nuCliente to set
	 */
	public void setNuCliente(String nuCliente) {
		this.nuCliente = nuCliente;
	}

	/**
	 * @return the cdDictamen
	 */
	public String getCdDictamen() {
		return cdDictamen;
	}

	/**
	 * @return the nbTpPersona
	 */
	public String getNbTpPersona() {
		return nbTpPersona;
	}

	/**
	 * @param nbTpPersona the nbTpPersona to set
	 */
	public void setNbTpPersona(String nbTpPersona) {
		this.nbTpPersona = nbTpPersona;
	}

	/**
	 * @param cdDictamen
	 *            the cdDictamen to set
	 */
	public void setCdDictamen(String cdDictamen) {
		this.cdDictamen = cdDictamen;
	}
	
	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append( "\ncdClialtrie : " + cdClialtrie );
		str.append( "\nnuId: " + nuId );
		str.append( "\ncdCliente: " + cdCliente );
		str.append( "\ncdTipologia: " + cdTipologia );
		str.append( "\ncdTpPersona: " + cdTpPersona);
		str.append( "\nnbTpPersona: " + nbTpPersona );
		str.append( "\nnuCuenta: " + nuCuenta );
		str.append( "\ncdCaracter: " + cdCaracter );
		str.append( "\nnbNombre: " + nbNombre );
		str.append( "\nnbApPaterno: " + nbApPaterno );
		str.append( "\nnbApMaterno: " + nbApMaterno );
		str.append( "\nfhNacimiento: " + fhNacimiento );
		str.append( "\ncdRFC: " + cdRFC );
		str.append( "\nnbDomicilio: " + nbDomicilio );
		str.append( "\nnbComplementarios: " + nbComplementarios);
		str.append( "\ncdClienteBancomer: " + cdClienteBancomer);
		str.append( "\nnuCliente: " + nuCliente);
		str.append( "\ncdDictamen: " + cdDictamen);
		str.append( "\ncdCaso: " + cdCaso);
		return str.toString();
	}

}
