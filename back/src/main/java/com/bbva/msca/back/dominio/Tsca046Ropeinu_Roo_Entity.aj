// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca046Ropeinu;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca046Ropeinu_Roo_Entity {
    
    declare @type: Tsca046Ropeinu: @Entity;
    
    declare @type: Tsca046Ropeinu: @Table(name = "TSCA046_ROPEINU", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca046Ropeinu.entityManager;
    
    @Transactional
    public void Tsca046Ropeinu.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca046Ropeinu.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca046Ropeinu attached = Tsca046Ropeinu.findTsca046Ropeinu(this.cdRegOpInu);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca046Ropeinu.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca046Ropeinu.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca046Ropeinu Tsca046Ropeinu.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca046Ropeinu merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca046Ropeinu.entityManager() {
        EntityManager em = new Tsca046Ropeinu().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca046Ropeinu.countTsca046Ropeinus() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca046Ropeinu o", Long.class).getSingleResult();
    }
    
    public static List<Tsca046Ropeinu> Tsca046Ropeinu.findAllTsca046Ropeinus() {
        return entityManager().createQuery("SELECT o FROM Tsca046Ropeinu o", Tsca046Ropeinu.class).getResultList();
    }
    
    public static Tsca046Ropeinu Tsca046Ropeinu.findTsca046Ropeinu(BigDecimal cdRegOpInu) {
        if (cdRegOpInu == null) return null;
        return entityManager().find(Tsca046Ropeinu.class, cdRegOpInu);
    }
    
    public static List<Tsca046Ropeinu> Tsca046Ropeinu.findTsca046RopeinuEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca046Ropeinu o", Tsca046Ropeinu.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
