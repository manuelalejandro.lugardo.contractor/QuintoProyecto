package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class UsuarioCasoDTO extends UsuarioDTO{

	private BigDecimal nuPrioriAsigNormal;
	private BigDecimal nuPrioriAsigUrgente;
	private BigDecimal nuStAlertaAnalisis;
	private BigDecimal nuStAlertaSupervision;
	
	public BigDecimal getNuPrioriAsigNormal() {
		return nuPrioriAsigNormal;
	}
	public void setNuPrioriAsigNormal(BigDecimal nuPrioriAsigNormal) {
		this.nuPrioriAsigNormal = nuPrioriAsigNormal;
	}
	public BigDecimal getNuPrioriAsigUrgente() {
		return nuPrioriAsigUrgente;
	}
	public void setNuPrioriAsigUrgente(BigDecimal nuPrioriAsigUrgente) {
		this.nuPrioriAsigUrgente = nuPrioriAsigUrgente;
	}
	public BigDecimal getNuStAlertaAnalisis() {
		return nuStAlertaAnalisis;
	}
	public void setNuStAlertaAnalisis(BigDecimal nuStAlertaAnalisis) {
		this.nuStAlertaAnalisis = nuStAlertaAnalisis;
	}
	public BigDecimal getNuStAlertaSupervision() {
		return nuStAlertaSupervision;
	}
	public void setNuStAlertaSupervision(BigDecimal nuStAlertaSupervision) {
		this.nuStAlertaSupervision = nuStAlertaSupervision;
	}
	
}
