package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.bbva.msca.back.dto.CuentasClienteReportadoDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class CuentasClienteReportadoDAO {
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	@SuppressWarnings("unchecked")
	public List<CuentasClienteReportadoDTO> consultarCuentasClienteReportado(String cdSistema, String nuFolioAlerta, BigDecimal cdCaso) throws Exception{
		   List<CuentasClienteReportadoDTO> lstCuentasClienteReportado = new ArrayList<CuentasClienteReportadoDTO>();
		
		 Query query = em.createNativeQuery(Consultas.getConsulta("getCuentasClienteReportado"));
		 query.setParameter(1,cdCaso);
		 query.setParameter(2,cdCaso);
		 query.setParameter(3,cdCaso);
		 query.unwrap(org.hibernate.SQLQuery.class)

			.addScalar("fhAperturaCCR", TimestampType.INSTANCE)
	        .addScalar("nuCuentaCCR")
	        .addScalar("nbProductoCCR", StringType.INSTANCE)
	        .addScalar("nbSubproductoCCR", StringType.INSTANCE)
	        .addScalar("nbEstatusCCR", StringType.INSTANCE)
	        .addScalar("fhCancelacionCCR", StringType.INSTANCE)
	        .addScalar("nbTitularCCR")
	        .addScalar("nuCtePartCCR", StringType.INSTANCE)
	        .addScalar("nbParticipesCCR")
	        .addScalar("imSaldoCCR", StringType.INSTANCE)
	        .addScalar("imSaldoPromCCR", StringType.INSTANCE)

	        .setResultTransformer(Transformers.aliasToBean(CuentasClienteReportadoDTO.class));
		
		 lstCuentasClienteReportado = (List<CuentasClienteReportadoDTO>)query.getResultList();


		 
		return lstCuentasClienteReportado;
		
	}
}
