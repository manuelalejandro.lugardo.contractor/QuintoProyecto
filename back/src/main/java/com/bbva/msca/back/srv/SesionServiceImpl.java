package com.bbva.msca.back.srv;

import java.util.List;

import com.bbva.msca.back.dao.SesionDAO;
import com.bbva.msca.back.dto.SesionDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class SesionServiceImpl implements SesionService {
	
	@Override
	public ResponseGeneric consultarSesiones() {
		ResponseGeneric response = new ResponseGeneric();
		SesionDAO sesionDAO = new SesionDAO();
		List<SesionDTO> lstSesionDTO = null;
		try {
			lstSesionDTO = sesionDAO.getSesiones();
			response.setResponse(lstSesionDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric creaSesion(SesionDTO sesion) {
		ResponseGeneric response = new ResponseGeneric();
		SesionDAO sesionDAO = new SesionDAO();
		try {
			switch(sesionDAO.creaSesion(sesion)){
			case Constantes.FIN_OK:
				response.setResponse(sesion);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_AV:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_01"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric modificaSesion(SesionDTO sesion) {
		ResponseGeneric response = new ResponseGeneric();
		SesionDAO sesionDAO = new SesionDAO();
		try {
			response.setResponse(sesionDAO.modificaSesion(sesion));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
}
