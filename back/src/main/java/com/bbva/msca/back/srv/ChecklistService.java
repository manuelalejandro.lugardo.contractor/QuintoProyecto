package com.bbva.msca.back.srv;

import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.ChecklistDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/ChecklistService")
public interface ChecklistService {

	@POST
	@Path("/consultarChecklist")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarChecklists(ChecklistDTO checklistDTO);

	@POST
	@Path("/altaChecklist")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric creaChecklist(ChecklistDTO checklistDTO);

	@POST
	@Path("/modificaChecklist")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric modificaChecklist(ChecklistDTO checklistDTO);

	@POST
	@Path("/modificaChecklists")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric modificaChecklists(List<ChecklistDTO> lstChecklistDTO);

	

}
