// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca067RelFid;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca067RelFid_Roo_Entity {
    
    declare @type: Tsca067RelFid: @Entity;
    
    declare @type: Tsca067RelFid: @Table(name = "TSCA067_REL_FID", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca067RelFid.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CD_REL_FID")
    private BigDecimal Tsca067RelFid.cdRelFid;
    
    public BigDecimal Tsca067RelFid.getCdRelFid() {
        return this.cdRelFid;
    }
    
    public void Tsca067RelFid.setCdRelFid(BigDecimal id) {
        this.cdRelFid = id;
    }
    
    @Transactional
    public void Tsca067RelFid.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca067RelFid.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca067RelFid attached = Tsca067RelFid.findTsca067RelFid(this.cdRelFid);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca067RelFid.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca067RelFid.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca067RelFid Tsca067RelFid.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca067RelFid merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca067RelFid.entityManager() {
        EntityManager em = new Tsca067RelFid().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca067RelFid.countTsca067RelFids() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca067RelFid o", Long.class).getSingleResult();
    }
    
    public static List<Tsca067RelFid> Tsca067RelFid.findAllTsca067RelFids() {
        return entityManager().createQuery("SELECT o FROM Tsca067RelFid o", Tsca067RelFid.class).getResultList();
    }
    
    public static Tsca067RelFid Tsca067RelFid.findTsca067RelFid(BigDecimal cdRelFid) {
        if (cdRelFid == null) return null;
        return entityManager().find(Tsca067RelFid.class, cdRelFid);
    }
    
    public static List<Tsca067RelFid> Tsca067RelFid.findTsca067RelFidEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca067RelFid o", Tsca067RelFid.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
