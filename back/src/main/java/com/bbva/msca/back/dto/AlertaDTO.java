package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.bbva.msca.back.util.AnotacionAlerta;

public class AlertaDTO {

	private String nuFolioAlerta;
	private String cdSistema;
	private String nbSistema;
	private String cdTipologia;
	private BigDecimal imAlerta;
	private String cdSesion;
	private String cdSegmento;
	private String nbSegmento;
	private Date fhAlerta;
	private Date fhEnvio;
	private String cdDivisa;
	private BigDecimal cdStAlerta;
	private String nbStAlerta;
	private String txMotivoDescarte;
	private String stRepAut;
	private BigDecimal nuScoreMc;
	private BigDecimal nuScoreMdl;
	private Date fhAsignacion;
	private Date fhVenRev;
	private Date fhVenLibSia;
	private Date fhRepAut;
	private String cdPrioriAsig;
	private String nuCliente;
	private String nbCliente;
	private String nuCuenta;
	private BigDecimal cdCaso;
	private String cdEscenario;
	private String cdTPersona;
	private BigDecimal cdStCaso;
	private String nbStCaso;
	private String nbHighLight;
	private Date fhLibSia;
	private String nbPrioriAsig;
	private String nbPeriodo;
	private String cdConsultor;
	private Date fhLimRepAutA;
	private Date fhLimRepAutB;
	private String cdDictamenP;
	private String nbDictamenP;
	private String cdDictamenF;
	private String nbDictamenF;
	private String nbConsultor;
	private List<CampoDetalleAlertaDTO> camposDetalleAlerta;
	private List<EscenariosDTO> escenariosAlerta;
	private String nbEscenario;
	private String nbTpPersona;
	private String cdRespuesta;
	private String nbRespuesta;
	private String cdOficinaGest;

	public Date getFhLimRepAutA() {
		return fhLimRepAutA;
	}

	public void setFhLimRepAutA(Date fhLimRepAutA) {
		this.fhLimRepAutA = fhLimRepAutA;
	}

	public Date getFhLimRepAutB() {
		return fhLimRepAutB;
	}

	public void setFhLimRepAutB(Date fhLimRepAutB) {
		this.fhLimRepAutB = fhLimRepAutB;
	}

	public String getNbPrioriAsig() {
		return nbPrioriAsig;
	}

	public void setNbPrioriAsig(String nbPrioriAsig) {
		this.nbPrioriAsig = nbPrioriAsig;
	}

	public String getNuFolioAlerta() {
		return nuFolioAlerta;
	}

	public void setNuFolioAlerta(String nuFolioAlerta) {
		this.nuFolioAlerta = nuFolioAlerta;
	}

	public String getCdSistema() {
		return cdSistema;
	}

	public void setCdSistema(String cdSistema) {
		this.cdSistema = cdSistema;
	}

	public String getNbSistema() {
		return nbSistema;
	}

	public void setNbSistema(String nbSistema) {
		this.nbSistema = nbSistema;
	}

	public String getCdTipologia() {
		return cdTipologia;
	}

	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}

	public BigDecimal getCdCaso() {
		return cdCaso;
	}

	public void setCdCaso(BigDecimal cdCaso) {
		this.cdCaso = cdCaso;
	}

	public String getCdSesion() {
		return cdSesion;
	}

	public void setCdSesion(String cdSesion) {
		this.cdSesion = cdSesion;
	}

	public String getCdDivisa() {
		return cdDivisa;
	}

	public void setCdDivisa(String cdDivisa) {
		this.cdDivisa = cdDivisa;
	}

	public String getNuCuenta() {
		return nuCuenta;
	}

	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}

	public BigDecimal getCdStAlerta() {
		return cdStAlerta;
	}

	public void setCdStAlerta(BigDecimal cdStAlerta) {
		this.cdStAlerta = cdStAlerta;
	}

	public String getNbStAlerta() {
		return nbStAlerta;
	}

	public void setNbStAlerta(String nbStAlerta) {
		this.nbStAlerta = nbStAlerta;
	}

	public Date getFhAlerta() {
		return fhAlerta;
	}

	public void setFhAlerta(Date fhAlerta) {
		this.fhAlerta = fhAlerta;
	}

	public Date getFhEnvio() {
		return fhEnvio;
	}

	public void setFhEnvio(Date fhEnvio) {
		this.fhEnvio = fhEnvio;
	}

	public BigDecimal getImAlerta() {
		return imAlerta;
	}

	public void setImAlerta(BigDecimal imAlerta) {
		this.imAlerta = imAlerta;
	}

	public String getTxMotivoDescarte() {
		return txMotivoDescarte;
	}

	public void setTxMotivoDescarte(String txMotivoDescarte) {
		this.txMotivoDescarte = txMotivoDescarte;
	}

	public String getStRepAut() {
		return stRepAut;
	}

	public void setStRepAut(String stRepAut) {
		this.stRepAut = stRepAut;
	}

	public String getNbHighLight() {
		return nbHighLight;
	}

	public void setNbHighLight(String nbHighLight) {
		this.nbHighLight = nbHighLight;
	}

	public String getNbSegmento() {
		return nbSegmento;
	}

	public void setNbSegmento(String nbSegmento) {
		this.nbSegmento = nbSegmento;
	}

	public BigDecimal getNuScoreMc() {
		return nuScoreMc;
	}

	public void setNuScoreMc(BigDecimal nuScoreMc) {
		this.nuScoreMc = nuScoreMc;
	}

	public BigDecimal getNuScoreMdl() {
		return nuScoreMdl;
	}

	public void setNuScoreMdl(BigDecimal nuScoreMdl) {
		this.nuScoreMdl = nuScoreMdl;
	}

	public Date getFhAsignacion() {
		return fhAsignacion;
	}

	public void setFhAsignacion(Date fhAsignacion) {
		this.fhAsignacion = fhAsignacion;
	}

	public Date getFhVenRev() {
		return fhVenRev;
	}

	public void setFhVenRev(Date fhVenRev) {
		this.fhVenRev = fhVenRev;
	}

	public Date getFhVenLibSia() {
		return fhVenLibSia;
	}

	public void setFhVenLibSia(Date fhVenLibSia) {
		this.fhVenLibSia = fhVenLibSia;
	}

	public Date getFhRepAut() {
		return fhRepAut;
	}

	public void setFhRepAut(Date fhRepAut) {
		this.fhRepAut = fhRepAut;
	}

	public String getNbCliente() {
		return nbCliente;
	}

	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}

	public String getNbPeriodo() {
		return nbPeriodo;
	}

	public void setNbPeriodo(String nbPeriodo) {
		this.nbPeriodo = nbPeriodo;
	}

	public String getCdPrioriAsig() {
		return cdPrioriAsig;
	}

	public void setCdPrioriAsig(String cdPrioriAsig) {
		this.cdPrioriAsig = cdPrioriAsig;
	}

	public String getCdTPersona() {
		return cdTPersona;
	}

	public void setCdTPersona(String cdTPersona) {
		this.cdTPersona = cdTPersona;
	}

	public String getCdSegmento() {
		return cdSegmento;
	}

	public void setCdSegmento(String cdSegmento) {
		this.cdSegmento = cdSegmento;
	}

	public String getCdEscenario() {
		return cdEscenario;
	}

	public void setCdEscenario(String cdEscenario) {
		this.cdEscenario = cdEscenario;
	}

	public String getCdConsultor() {
		return cdConsultor;
	}

	public void setCdConsultor(String cdConsultor) {
		this.cdConsultor = cdConsultor;
	}

	public String getCdDictamenP() {
		return cdDictamenP;
	}

	public void setCdDictamenP(String cdDictamenP) {
		this.cdDictamenP = cdDictamenP;
	}

	public String getCdDictamenF() {
		return cdDictamenF;
	}

	public void setCdDictamenF(String cdDictamenF) {
		this.cdDictamenF = cdDictamenF;
	}

	public Date getFhLibSia() {
		return fhLibSia;
	}

	public void setFhLibSia(Date fhLibSia) {
		this.fhLibSia = fhLibSia;
	}

	public BigDecimal getCdStCaso() {
		return cdStCaso;
	}

	public void setCdStCaso(BigDecimal cdStCaso) {
		this.cdStCaso = cdStCaso;
	}

	public String getNbStCaso() {
		return nbStCaso;
	}

	public void setNbStCaso(String nbStCaso) {
		this.nbStCaso = nbStCaso;
	}

	public String getNbConsultor() {
		return nbConsultor;
	}

	public void setNbConsultor(String nbConsultor) {
		this.nbConsultor = nbConsultor;
	}

	public void setNuCliente(String nuCliente) {
		this.nuCliente = nuCliente;
	}

	public String getNuCliente() {
		return nuCliente;
	}

	public void setCamposDetalleAlerta(
			List<CampoDetalleAlertaDTO> camposDetalleAlerta) {
		this.camposDetalleAlerta = camposDetalleAlerta;
	}

	public List<CampoDetalleAlertaDTO> getCamposDetalleAlerta() {
		return camposDetalleAlerta;
	}

	public String getNbDictamenP() {
		return nbDictamenP;
	}

	public void setNbDictamenP(String nbDictamenP) {
		this.nbDictamenP = nbDictamenP;
	}

	public void setEscenariosAlerta(List<EscenariosDTO> escenariosAlerta) {
		this.escenariosAlerta = escenariosAlerta;
	}

	public List<EscenariosDTO> getEscenariosAlerta() {
		return escenariosAlerta;
	}

	public String getNbEscenario() {
		return nbEscenario;
	}

	public void setNbEscenario(String nbEscenario) {
		this.nbEscenario = nbEscenario;
	}

	public String getNbTpPersona() {
		return nbTpPersona;
	}

	public void setNbTpPersona(String nbTpPersona) {
		this.nbTpPersona = nbTpPersona;
	}

	public String getNbDictamenF() {
		return nbDictamenF;
	}

	public void setNbDictamenF(String nbDictamenF) {
		this.nbDictamenF = nbDictamenF;
	}

	public String getCdRespuesta() {
		return cdRespuesta;
	}

	public void setCdRespuesta(String cdRespuesta) {
		this.cdRespuesta = cdRespuesta;
	}

	public String getNbRespuesta() {
		return nbRespuesta;
	}

	public void setNbRespuesta(String nbRespuesta) {
		this.nbRespuesta = nbRespuesta;
	}

	public String getCdOficinaGest() {
		return cdOficinaGest;
	}

	public void setCdOficinaGest(String cdOficinaGest) {
		this.cdOficinaGest = cdOficinaGest;
	}

}
