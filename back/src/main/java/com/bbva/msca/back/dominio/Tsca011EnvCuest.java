package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import com.bbva.msca.back.dominio.Tsca011EnvCuestPK;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(versionField = "", table = "TSCA011_ENV_CUEST", schema = "GORAPR", identifierType = Tsca011EnvCuestPK.class)
public class Tsca011EnvCuest {
}
