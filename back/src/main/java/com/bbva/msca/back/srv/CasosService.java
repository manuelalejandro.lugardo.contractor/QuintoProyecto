package com.bbva.msca.back.srv;

import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Consumes;
import com.bbva.msca.back.dominio.Tsca014Caso;
import com.bbva.msca.back.dominio.Tsca054Archivo;
import com.bbva.msca.back.dto.OpinionFuncionarioDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/CasosService")
public interface CasosService {
	//001
	//Cedula de Caso
	@GET
	@Path("/consultarCedulas/{cdCaso}/{nuCliente}/{nuFolioAlertaDC}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCedulas(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("nuCliente") String nuCliente, @PathParam("nuFolioAlertaDC") String nuFolioAlertaDC);

	//002
	//Datos de Identificacion del Reporte
	@GET
	@Path("/consultarIdentificacionReportes/{nuFolioAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarIdentificacionReportes(@PathParam("nuFolioAlerta") String nuFolioAlerta,@PathParam("cdSistema") String cdSistema);

	//Datos de Identificacion del Reporte2
	@GET
	@Path("/consultarIdentificacionReportes2/{nuFolioAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarIdentificacionReportes2(@PathParam("nuFolioAlerta") String nuFolioAlerta,@PathParam("cdSistema") String cdSistema);


	//Datos Generales del Cliente Reportado
	@GET
	@Path("/consultarDatosGeneralesClienteReportado/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDatosGeneralesClienteReportado(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);

	//analisis del casoDatos Generales del Cliente Reportado
	@GET
	@Path("/consultarDatosGeneralesClienteReportado2/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDatosGeneralesClienteReportado2(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);
	
	//004
	//Datos Generales del Cliente
	@GET
	@Path("/consultarDatosGeneralesCliente/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDatosGeneralesCliente(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);
	
	//005

	//Consulta Sobre Conocimiento del Cliente
	@GET
	@Path("/consultarConocimientoCliente/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConocimientoCliente(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);

	
	//Transacciones del Cliente
	@GET
	@Path("/consultarTransaccionesCliente/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarTransaccionesCliente(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);
	
	//006
	//getRelacionClienteFigura
	@GET
	@Path("/consultarRelacionClienteFigura/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarRelacionClienteFigura(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);
	
	//008
	//INFO ACCIONISTAS EMPRESAS
	@GET
	@Path("/consultarInfoAccionistasyEmpresa/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarInfoAccionistasyEmpresa(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);

	//Opinion del Funcionario
	@GET
	@Path("/consultarOpinionFuncionarioEnc/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarOpinionFuncionarioEnc(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);
	
	@GET
	@Path("/consultarOpinionFuncionario/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarOpinionFuncionario(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);
	
	
	@PUT
	@Path("/agregarOpinionFuncionario/{cdSistema}/{nuFolioAlerta}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric agregarOpinionFuncionario(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta, OpinionFuncionarioDTO opinionFuncionarioDTO);
	
	
	
	//Descripcion de la Transaccion del Cliente
	@GET
	@Path("/consultarDescripcionTransaccion/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDescripcionTransaccion(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta);
	
	
	/*
	//Mecanica Operacional
	@GET
	@Path("/consultarTablaCuatrimestral/{cdCaso}/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarMecanicaOperacionalConcepto(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	//TABLA MES 1
	@GET
	@Path("/consultarConceptosMesUno/{cdCaso}/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConceptosMesUno(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	//TABLA MES 2
	@GET
	@Path("/consultarConceptosMesDos/{cdCaso}/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConceptosMesDos(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	//TABLA MES 3
	@GET
	@Path("/consultarConceptosMesTres/{cdCaso}/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConceptosMesTres(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	//TABLA MES 4
	@GET
	@Path("/consultarConceptosMesCuatro/{cdCaso}/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConceptosMesCuatro(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	//TABLA ORIGEN
	@GET
	@Path("/consultarOrigenMOCR")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarOrigenMOCR();
	//TABLA DESTINO
	@GET
	@Path("/consultarDestinoMOCR")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDestinoMOCR();
	*/
	//Operaciones Medianas y Relevantes
	@GET
	@Path("/consultarOperacionesMedianasRelevantes/{cdCaso}/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarOperacionesMedianasRelevantes(@PathParam("cdCaso") BigDecimal cdCaso, @PathParam("cdSistema") String cdSistema, @PathParam("nuFolioAlerta") String nuFolioAlerta);
	

	//Transferencias Internacionales
	@GET
	@Path("/consultarTranferenciaInternacionalRecEnv/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarTranferenciaInternacionalRecEnv(@PathParam("cdSistema") String cdSistema, @PathParam("nuFolioAlerta") String nuFolioAlerta);
	

	@GET
	@Path("/consultarAlertas/{cdCaso}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAlertasCaso(@PathParam("cdCaso") BigDecimal cdCaso);
	
	
	
	
	//Analisis de Cuentas Vinculadas
	@GET
	@Path("/consultarCuentasVinculadasOrigen")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCuentasVinculadasOrigen();
	
	@GET
	@Path("/consultarCuentasVinculadasDestino")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCuentasVinculadasDestino();
	
	//Oficios Listas y Notas Periodisticas
	@GET
	@Path("/consultarOficiosListasNotasPeriodisticas")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarOficiosListasNotasPeriodisticas();
	
	//Domicilio en Google Maps
	@GET
	@Path("/consultarDireccionesMaps")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDireccionesMaps();
	
	
	
	
	       //Detalle Operaciones Medianas y Relevantes
	/*@GET
	@Path("/consultarDetalleOperacionesMedRel")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDetalleOperacionesMedRel();*/
	
	@GET
	@Path("/consultarDetalleOperacionesMedianas/{cdCaso}/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDetalleOperacionesMedianas(@PathParam("cdCaso") BigDecimal cdCaso, @PathParam("cdSistema") String cdSistema, @PathParam("nuFolioAlerta") String nuFolioAlerta,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset,
			@QueryParam("search") String search,
			@QueryParam("name") String name,
			@QueryParam("order") String order);
	
	@GET
	@Path("/consultarDetalleOperacionesRelevantes/{cdCaso}/{cdSistema}/{nuFolioAlerta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDetalleOperacionesRelevantes(@PathParam("cdCaso") BigDecimal cdCaso, @PathParam("cdSistema") String cdSistema, @PathParam("nuFolioAlerta") String nuFolioAlerta,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset,
			@QueryParam("search") String search,
			@QueryParam("name") String name,
			@QueryParam("order") String order);
	
	
	// Fideicomiso 
	@GET
	@Path("/consultarFideicomiso")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarFideicomiso();
	     // Fideicomiso - Personas Relacionadas
	@GET
	@Path("/consultarPersonasRelacionadas")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarPersonasRelacionadas();
	       //Formulario de FIdeicomiso
	@GET
	@Path("/consultarDetalleFideicomiso")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDetalleFideicomiso();

	
	//Cuentas del Cliente
	@GET
	@Path("/consultarCuentaClientes")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCuentaClientes();
	
	//Notas de Investigacion
	@GET
	@Path("/consultarNotasInvestigacion/{cdCaso}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarNotasInvestigacion(@PathParam("cdCaso") BigDecimal cdCaso);
	
	
	
			//Detalle de Transferencias Internacionales
	@GET
	@Path("/consultarDetalleTranferenciaInternacionalRecEnv")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDetalleTranferenciaInternacionalRecEnv();
	
	//Log
	@GET
	@Path("/consultarLog")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarLog();


	//SOO
	
	@POST
	@Path("/CrearDescripCaso/{cdCaso}") 
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces((MediaType.APPLICATION_JSON))
	public ResponseGeneric crearDescripCaso(Tsca014Caso descripcion,@PathParam("cdCaso") BigDecimal cdCaso) throws Exception;

	@GET
	@Path("/consultarLogAnalisis/{cdCaso}/{nuCliente}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarLogAnalisis(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("nuCliente") String nuCliente);


	@POST
	@Path("/addNota/{nbCveRed}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces((MediaType.APPLICATION_JSON))
	public ResponseGeneric addNota(Tsca054Archivo nota,@PathParam("nbCveRed") String nbCveRed) throws Exception;
	
	@PUT
	@Path("/addArchivo/{nbCveRed}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric addArchivo(Tsca054Archivo archivo,@PathParam("nbCveRed") String nbCveRed)
			throws Exception;
	


	@Path("/OrdenDelDiaService")
	public interface OrdenDelDiaService {

		@GET
		@Path("/GenerarOrdenDelDia")
		@Consumes({ MediaType.APPLICATION_JSON })
		public ResponseGeneric generarOrdenDelDia(@QueryParam("comite") List<String> comite) throws Exception;
		
	}

	

    
	
	     
	@GET
	@Path("/consultaKYC/{cdCaso}/{cdCliente}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarKYC(@PathParam("cdCaso") BigDecimal cdCaso, @PathParam("cdCliente") String cdCliente);
	
	@GET
	@Path("/consultaDescripcion/{cdCaso}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDescrip(@PathParam("cdCaso") BigDecimal cdCaso);
	
	@GET
	@Path("/consultaAccionistasDetCaso/{cdCaso}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultaAccionistasDetCaso(@PathParam("cdCaso") BigDecimal cdCaso);
	
	
	@GET
	@Path("/consultaOtrosClientesCaso/{cdCaso}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultaOtrosClientesCaso(@PathParam("cdCaso") BigDecimal cdCaso);
	
	@GET
	@Path("/consultarTablaCuatrimestralAnalisis/{cdCaso}/{cdAlerta}/{cdSesion}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarMecanicaOperacionalConceptoAnalisis(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSesion") String cdSesion);
	//TABLA MES 1
	@GET
	@Path("/consultarConceptosMesUnoAnalisis/{cdCaso}/{cdAlerta}/{cdSesion}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConceptosMesUnoAnalisis(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSesion") String cdSesion);
	//TABLA MES 2
	@GET
	@Path("/consultarConceptosMesDosAnalisis/{cdCaso}/{cdAlerta}/{cdSesion}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConceptosMesDosAnalisis(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSesion") String cdSesion);
	//TABLA MES 3
	@GET
	@Path("/consultarConceptosMesTresAnalisis/{cdCaso}/{cdAlerta}/{cdSesion}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConceptosMesTresAnalisis(@PathParam("cdCaso")BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSesion") String cdSesion);
	//TABLA MES 4
	@GET
	@Path("/consultarConceptosMesCuatroAnalisis/{cdCaso}/{cdAlerta}/{cdSesion}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConceptosMesCuatroAnalisis(@PathParam("cdCaso") BigDecimal cdCaso,@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSesion") String cdSesion);
	
	@GET
	@Path("/consultarTranferenciaInternacionalRecEnvDet/{cdSistema}/{nuFolioAlerta}/{numCuenta}/{tipoOperacion}/{anioActual}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarTranferenciaInternacionalRecEnvDet(
			@PathParam("cdSistema") String cdSistema, @PathParam("nuFolioAlerta") String nuFolioAlerta,
			@PathParam("numCuenta") String numCuenta, @PathParam("tipoOperacion") String tipoOperacion,
			@PathParam("anioActual") Integer anioActual);

	@GET
	@Path("/consultarDetalleTranferenciaInternacionalRecEnvDet/{cdSistema}/{nuFolioAlerta}/{numCuenta}/{tipoOperacion}/{anioActual}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDetalleTranferenciaInternacionalRecEnvDet(
			@PathParam("cdSistema") String cdSistema, @PathParam("nuFolioAlerta") String nuFolioAlerta,
			@PathParam("numCuenta") String numCuenta, @PathParam("tipoOperacion") String tipoOperacion,
			@PathParam("anioActual") Integer anioActual);
	
	
	//FN003 - PESTA�A CUENTAS DEL CLIENTE
	//Tabla cuentas del Cliente
	@GET
	@Path("/consultarCuentasClienteReportado/{cdSistema}/{nuFolioAlerta}/{cdCaso}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCuentasClienteReportado(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta,@PathParam("cdCaso") BigDecimal cdCaso);
	
	//Tabla otros productos
	@GET
	@Path("/consultarOtrosProductosCR/{cdSistema}/{nuFolioAlerta}/{cdCaso}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarOtrosProductosCR(@PathParam("cdSistema") String cdSistema,@PathParam("nuFolioAlerta") String nuFolioAlerta,@PathParam("cdCaso") BigDecimal cdCaso);
	
	//Tabla fideicomiso
	@GET
	@Path("/consultaFideicomisoCaso/{cdCaso}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultaFideicomisoCaso(@PathParam("cdCaso") BigDecimal cdCaso);
	
	//Personas Relacionadas al fideicomiso
	@GET
	@Path("/consultaDetalleFideicomisoCaso/{cdCaso}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultaDetalleFideicomisoCaso(@PathParam("cdCaso") BigDecimal cdCaso);
	
	
	//Cabecera Cuatrimestral
	@GET
	@Path("/cabeceraCuatrimestral/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric cabeceraCuatrimestral(@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	
	@GET
	@Path("/cabeceraMesUno/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric cabeceraMesUno(@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	
	@GET
	@Path("/cabeceraMesDos/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric cabeceraMesDos(@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	
	@GET
	@Path("/cabeceraMesTres/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric cabeceraMesTres(@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	
	@GET
	@Path("/cabeceraMesCuatro/{cdAlerta}/{cdSistema}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric cabeceraMesCuatro(@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
}
