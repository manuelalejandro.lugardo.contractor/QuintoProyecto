package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class LogDTO {
	
	private BigDecimal nuId;
	private String nbEvento;
	private String nbAccion;
	private String nbClvUsu;
	private String nbUsuario;
	private String fhRegistro;
	
	
	public BigDecimal getNuId() {
		return nuId;
	}
	public void setNuId(BigDecimal nuId) {
		this.nuId = nuId;
	}
	public String getNbEvento() {
		return nbEvento;
	}
	public void setNbEvento(String nbEvento) {
		this.nbEvento = nbEvento;
	}
	public String getNbAccion() {
		return nbAccion;
	}
	public void setNbAccion(String nbAccion) {
		this.nbAccion = nbAccion;
	}
	public String getNbClvUsu() {
		return nbClvUsu;
	}
	public void setNbClvUsu(String nbClvUsu) {
		this.nbClvUsu = nbClvUsu;
	}
	public String getNbUsuario() {
		return nbUsuario;
	}
	public void setNbUsuario(String nbUsuario) {
		this.nbUsuario = nbUsuario;
	}
	public String getFhRegistro() {
		return fhRegistro;
	}
	public void setFhRegistro(String fhRegistro) {
		this.fhRegistro = fhRegistro;
	}

}
