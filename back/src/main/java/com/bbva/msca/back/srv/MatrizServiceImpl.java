package com.bbva.msca.back.srv;

import java.util.List;

import com.bbva.msca.back.dao.MatrizDAO;
import com.bbva.msca.back.dto.MatrizDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;



public class MatrizServiceImpl implements MatrizService {
	@Override
	public ResponseGeneric consultarMatrices() {
		ResponseGeneric response = new ResponseGeneric();
		MatrizDAO matrizDAO = new MatrizDAO();
		List<MatrizDTO> lstMatrizDTO = null;
		
		try {
			lstMatrizDTO = matrizDAO.getMatrices();
			response.setResponse(lstMatrizDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric consultarMatricesGerencia(Integer cdGerencia) {
		ResponseGeneric response = new ResponseGeneric();
		MatrizDAO matrizDAO = new MatrizDAO();
		List<MatrizDTO> lstMatrizDTO = null;
		
		try {
			lstMatrizDTO = matrizDAO.getMatricesGerencia(cdGerencia);
			response.setResponse(lstMatrizDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		
		return response;
	}

	@Override
	public ResponseGeneric creaMatriz(MatrizDTO matriz) {
		ResponseGeneric response = new ResponseGeneric();
		MatrizDAO matrizDAO = new MatrizDAO();
		try {
			switch(matrizDAO.creaMatriz(matriz)){
			case Constantes.FIN_OK:
				response.setResponse(matriz);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_AV:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_05"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric modificaMatriz(MatrizDTO matriz) {
		ResponseGeneric response = new ResponseGeneric();
		MatrizDAO matrizDAO = new MatrizDAO();
		try {
			response.setResponse(matrizDAO.modificaMatriz(matriz));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
}
