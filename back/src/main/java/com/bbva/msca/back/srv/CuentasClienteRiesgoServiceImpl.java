package com.bbva.msca.back.srv;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.bbva.msca.back.dao.CuentasClienteRiesgoDAO;
import com.bbva.msca.back.dao.LocalizacionClienteRiesgoDAO;
import com.bbva.msca.back.dao.MovimientosClientesRiesgoDAO;
import com.bbva.msca.back.dominio.Tsca054Archivo;
import com.bbva.msca.back.dto.ArchivoDTO;
import com.bbva.msca.back.dto.CuentasClienteRiesgoDTO;
import com.bbva.msca.back.dto.CuentasClienteRiesgoDetDTO;
import com.bbva.msca.back.dto.LocalizacionClienteRiesgoDTO;
import com.bbva.msca.back.dto.MovimientosClientesRiesgoDTO;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class CuentasClienteRiesgoServiceImpl implements CuentasClienteRiesgoService{
	
	private final String CD_TP_PERSONA_MORAL = "M";
	private final String CD_TP_PERSONA_GOBIERNO = "G";
	
	@Override
	public ResponseGeneric altaClienteRiesgo(CuentasClienteRiesgoDTO clienteRiesgo) {
		
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {
			
			response.setResponse(cuentaClienteDAO.registraClienteAltoRiesgo(clienteRiesgo )  );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	
	@Override
	public ResponseGeneric editarClienteRiesgo(CuentasClienteRiesgoDTO clienteRiesgo) {
		
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {
			
			response.setResponse(cuentaClienteDAO.editarClienteAltoRiesgo(clienteRiesgo )  );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}	
	
	@Override
	public ResponseGeneric consultarRequerimiento(BigDecimal cdRequerimiento ) {		
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {			
			response.setResponse(cuentaClienteDAO.consultarRequerimiento( cdRequerimiento )  );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}		
	
	@Override
	public ResponseGeneric altaClienteRiesgoAutomatica(CuentasClienteRiesgoDTO clienteRiesgo, BigDecimal cdTipoAlta, String cdUsuario) {
		
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {
			
			response.setResponse(cuentaClienteDAO.registraClienteAltoRiesgoTransac(clienteRiesgo, cdTipoAlta, cdUsuario ));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}	
	
	
	@Override
	public ResponseGeneric LocalizacionClienteRiesgo(CuentasClienteRiesgoDetDTO datosCliente) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentasClienteRiesgoDAO = new CuentasClienteRiesgoDAO();
		List<CuentasClienteRiesgoDetDTO> lstClientesAltoRiesgo = null;
		try {
			lstClientesAltoRiesgo = cuentasClienteRiesgoDAO.getClientesLocalizados(datosCliente);
			response.setResponse(lstClientesAltoRiesgo);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric BusquedaClienteRiesgo() {
		ResponseGeneric response = new ResponseGeneric();
		LocalizacionClienteRiesgoDTO localizacionClienteRiesgoDTO = LocalizacionClienteRiesgoDAO.getBusquedaClienteRiesgo();
		
		response.setResponse(localizacionClienteRiesgoDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}

	@Override
	public ResponseGeneric ClienteRiesgo() {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO clienteRiesgoDAO = new CuentasClienteRiesgoDAO();
		List<CuentasClienteRiesgoDTO> lstCuentasClienteRiesgoDTO = clienteRiesgoDAO.getClienteRiesgo();
		
		response.setResponse(lstCuentasClienteRiesgoDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		return response;
	}
	
	@Override
public ResponseGeneric MovimientosClienteRiesgoFiltro(String nuCuenta, MovimientosClientesRiesgoDTO movimientosClientesRiesgoDTO){
		ResponseGeneric response = new ResponseGeneric();
		MovimientosClientesRiesgoDAO movimientosClientesRiesgoDAO = new MovimientosClientesRiesgoDAO();
		try{
			List<MovimientosClientesRiesgoDTO> lstMovimientosClientesRiesgoDTO = movimientosClientesRiesgoDAO.getMovimientosClientesRiesgo(movimientosClientesRiesgoDTO, nuCuenta);
			response.setResponse(lstMovimientosClientesRiesgoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		    }catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));	
		    }
		return response;
  }
	
	@Override
	public ResponseGeneric MovimientosClienteRiesgo(String nuCuenta) {
		ResponseGeneric response = new ResponseGeneric();
		MovimientosClientesRiesgoDAO movimientosClientesRiesgoDAO = new MovimientosClientesRiesgoDAO();
		
		try{
			List<MovimientosClientesRiesgoDTO> lstMovimientosClientesRiesgoDTO = movimientosClientesRiesgoDAO.getMovimientosClientesRiesgo(nuCuenta);
			response.setResponse(lstMovimientosClientesRiesgoDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}
	
	@Override
	//public ResponseGeneric adjuntarArchivo(ArchivoDTO archivo) {
	public ResponseGeneric adjuntarArchivo(String cdExpediente, String nombreArchivo, String cdUsuarioCreacion, InputStream inputStream) {
		
		ResponseGeneric response = new ResponseGeneric();
		ArchivoDTO archivo = new ArchivoDTO();
		archivo.setCdExpediente( cdExpediente );
		archivo.setNbTitulo( nombreArchivo );
		archivo.setCdUsuarioCreacion(cdUsuarioCreacion);
		archivo.setCdUsuarioModificacion( cdUsuarioCreacion );
		archivo.setNbDocumento( inputStream );
		try {
			CuentasClienteRiesgoDAO ctsCteRsg = new CuentasClienteRiesgoDAO();
			ctsCteRsg.insertarArchivoAdjunto( archivo );
			response.setResponse( true );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponse( false );
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));			
		}
		return response;
	}


	@Override
	public ResponseGeneric consultarArchivosByCdExpediente(String cdExpediente) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {			
			response.setResponse(cuentaClienteDAO.consultaArchivosByCdExpediente( cdExpediente ) );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	@Override
	public ResponseGeneric downloadFileById(String cdFolioArchivo) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();	
		try {
			Tsca054Archivo folioArchivo = cuentaClienteDAO.consultaArchivoById( cdFolioArchivo );
			byte[] blobBytes = null;
			FileOutputStream outputStream = new FileOutputStream( "C:\\TmpFiles\\" + folioArchivo.getNbTitulo() );
			outputStream.write(blobBytes);
			outputStream.close();			
			response.setResponse(
				Response.ok(outputStream, MediaType.APPLICATION_OCTET_STREAM).header("Content-Disposition", "attachment; filename=\"" + folioArchivo.getNbTitulo() + "\"" ).build()					
			);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}	

	@Override
	public ResponseGeneric eliminarArchivo(String cdFolioArchivo) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {
			cuentaClienteDAO.eliminarArchivo( cdFolioArchivo );
			response.setResponse( true );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponse( false );
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}


	@Override
	public ResponseGeneric altaPersonaReportada(
			CuentasClienteRiesgoDetDTO personaReportada, BigDecimal cdTipoAlta, String cdUsuario) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {			
			response.setResponse(cuentaClienteDAO.insertaPersonaReportada( personaReportada, cdTipoAlta, cdUsuario ));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}
	
	@Override
	public ResponseGeneric altaPersonaReportadaTransac(
			List<CuentasClienteRiesgoDetDTO> lstPersonasReportadas,BigDecimal cdTipoAlta, String cdUsuario) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {			
			response.setResponse(cuentaClienteDAO.insertaPersonaReportadaTransac( lstPersonasReportadas, cdTipoAlta, cdUsuario ) );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}	


	@Override
	public ResponseGeneric consultarPersonasReportadas(String cdRequerimiento) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {			
			List<CuentasClienteRiesgoDetDTO> lstResultado = cuentaClienteDAO.consultaPersonasReportadasByReq( cdRequerimiento );
			if( lstResultado != null ){
				StringBuilder razonSocial = new StringBuilder();
				for( CuentasClienteRiesgoDetDTO req : lstResultado )
				{
					razonSocial = new StringBuilder();
					if( req.getCdTpPersona().equals ( CD_TP_PERSONA_MORAL ) || req.getCdTpPersona().equals( CD_TP_PERSONA_GOBIERNO ) )
					{
						if( req.getNbNombre() == null ){
							req.setNbNombre( "" );
						}
						if( req.getNbApPaterno() == null ){
							req.setNbApPaterno( "" );
						}						
						if( req.getNbApMaterno() == null ){
							req.setNbApMaterno( "" );
						}							
						razonSocial.append( req.getNbNombre() );
						razonSocial.append( req.getNbApPaterno() );
						razonSocial.append( req.getNbApMaterno() );
						req.setNbNombre( razonSocial.toString() );
						req.setNbApPaterno( "" );
						req.setNbApMaterno( "" );
					}
				}
			}
			response.setResponse( lstResultado );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	// EV006003
	@Override
	public ResponseGeneric actualizarPersonaReportada(CuentasClienteRiesgoDetDTO personaReportada) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {			
			response.setResponse(cuentaClienteDAO.updatePersonaReportada( personaReportada ) );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	@Override
	public ResponseGeneric eliminarPersonaReportada(BigDecimal cdCliAltRie,
			BigDecimal nuId) {
		ResponseGeneric response = new ResponseGeneric();
		CuentasClienteRiesgoDAO cuentaClienteDAO = new CuentasClienteRiesgoDAO();
		try {
			cuentaClienteDAO.eliminarPersonaReportada( cdCliAltRie, nuId  );
			response.setResponse( true );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			
		} catch (Exception e) {
			e.printStackTrace();
			response.setResponse(false);
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}		
}