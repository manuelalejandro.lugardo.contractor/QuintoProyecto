package com.bbva.msca.back.srv;

import java.util.List;
import com.bbva.msca.back.dao.AsignacionTipologiaDAO;
import com.bbva.msca.back.dto.AsigTipologiaDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class AsignacionTipologiaServiceImpl implements
		AsignacionTipologiaService {

	@Override
	public ResponseGeneric consultarAsigTipologias() {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionTipologiaDAO AsigtipologiaDAO = new AsignacionTipologiaDAO();
		List<AsigTipologiaDTO> lstAsigTipologiaDTO = null;
		try {
			lstAsigTipologiaDTO = AsigtipologiaDAO.getAsigTipologias();
			response.setResponse(lstAsigTipologiaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}

		return response;
	}
	
	@Override
	public ResponseGeneric consultarAsigTipologiasGerencia(Integer cdGerencia) {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionTipologiaDAO AsigtipologiaDAO = new AsignacionTipologiaDAO();
		List<AsigTipologiaDTO> lstAsigTipologiaDTO = null;
		try {
			lstAsigTipologiaDTO = AsigtipologiaDAO.getAsigTipologiasGerencia(cdGerencia);
			response.setResponse(lstAsigTipologiaDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}

		return response;
	}

	@Override
	public ResponseGeneric creaAsigTipologia(AsigTipologiaDTO asigTipologia) {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionTipologiaDAO asigSesionDAO = new AsignacionTipologiaDAO();
		try {
			switch(asigSesionDAO.creaAsigTipologia(asigTipologia)){
			case Constantes.FIN_OK:
				response.setResponse(asigTipologia);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_AV:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_04"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric eliminarAsigTipologia(AsigTipologiaDTO asigTipologia) {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionTipologiaDAO asigSesionDAO = new AsignacionTipologiaDAO();
		try {
			response.setResponse( asigSesionDAO.eliminarAsigTipologia(asigTipologia) );
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;

	}	
	
	@Override
	public ResponseGeneric modificaAsigTipologia(AsigTipologiaDTO asigTipologia) {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionTipologiaDAO asigSesionDAO = new AsignacionTipologiaDAO();
		try {
			response.setResponse(asigSesionDAO.modificaAsigTipologia(asigTipologia));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;

	}

}
