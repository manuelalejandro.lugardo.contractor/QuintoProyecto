package com.bbva.msca.back.srv;

import java.util.List;

import javax.persistence.NoResultException;

import org.springframework.dao.EmptyResultDataAccessException;

import com.bbva.msca.back.dao.PerfiladoDAO;
import com.bbva.msca.back.dto.UsuarioDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class PerfiladoServiceImpl implements PerfiladoService {

	@Override
	public ResponseGeneric consultarUsuario(String nbCveRed) {
		ResponseGeneric response = new ResponseGeneric();
		PerfiladoDAO perfiladoDAO = new PerfiladoDAO();
		UsuarioDTO usuarioDTO = null;
		try{
			usuarioDTO = perfiladoDAO.getUsuario(nbCveRed);
			response.setResponse(usuarioDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			if(usuarioDTO.getNbCveRed() == null){
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			}
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric getInfoEmpleado(String nbCveRed) {
		ResponseGeneric response = new ResponseGeneric();
		PerfiladoDAO perfiladoDAO = new PerfiladoDAO();
		UsuarioDTO usuarioDTO = null;
		try{
			usuarioDTO = perfiladoDAO.getInfoEmpleado(nbCveRed);
			if(usuarioDTO != null && !usuarioDTO.getCdUsuario().isEmpty()){
				response.setResponse(usuarioDTO);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
			} else {
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_06"));
			}
		}catch(EmptyResultDataAccessException ex){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_06"));
		}catch(NoResultException e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_06"));
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric consultarUsuarios() {
		ResponseGeneric response = new ResponseGeneric();
		PerfiladoDAO perfiladoDAO = new PerfiladoDAO();
		List<UsuarioDTO> lstUsuarioDTO = null;
		
		try{
			lstUsuarioDTO = perfiladoDAO.getUsuarios();
			response.setResponse(lstUsuarioDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		
		return response;
	}
	
	@Override
	public ResponseGeneric altaUsuario(UsuarioDTO usuario){
		ResponseGeneric response = new ResponseGeneric();
		PerfiladoDAO perfiladoDAO = new PerfiladoDAO();
		try {
			switch(perfiladoDAO.creaUsuario(usuario)){
			case Constantes.FIN_OK:
				response.setResponse(usuario);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_AV:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_06_2"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric modificaUsuario(UsuarioDTO usuario){
		ResponseGeneric response = new ResponseGeneric();
		PerfiladoDAO perfiladoDAO = new PerfiladoDAO();
		try {
			response.setResponse(perfiladoDAO.modificaUsuario(usuario));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));	
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
}
