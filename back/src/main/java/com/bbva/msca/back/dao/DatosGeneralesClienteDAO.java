package com.bbva.msca.back.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import com.bbva.msca.back.dto.DatosGeneralesClienteDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class DatosGeneralesClienteDAO {

	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();
	
	@SuppressWarnings("unchecked")
	public  DatosGeneralesClienteDTO getDatosGeneralesCliente(String cdSistema, String nuFolioAlerta) throws Exception{
		DatosGeneralesClienteDTO datosGeneralesClienteDTO = new DatosGeneralesClienteDTO();
		List<DatosGeneralesClienteDTO> lstDatosGeneralesClienteDTO = null;
		
			 Query query = em.createNativeQuery(Consultas.getConsulta("getDatosGeneralesCliente"));
			 query.setParameter(1,cdSistema);
			 query.setParameter(2,nuFolioAlerta);

			 query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("nbRegSimplificadoDC")
				.addScalar("nbActEconomicaDC")
				.addScalar("nbOcupacionDC")
			.setResultTransformer(Transformers.aliasToBean(DatosGeneralesClienteDTO.class));
			
			 lstDatosGeneralesClienteDTO = (List<DatosGeneralesClienteDTO>)query.getResultList();
			 if ( !lstDatosGeneralesClienteDTO.isEmpty()){
				 datosGeneralesClienteDTO = lstDatosGeneralesClienteDTO.get(0);
				}
			 
		return datosGeneralesClienteDTO;
	}

}