package com.bbva.msca.back.dto;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class ReportOtrosProductosDTO {

	private JRBeanCollectionDataSource lstOtrProdTarjetaCredito;
	private JRBeanCollectionDataSource lstOtrProdPatrimonial;

	public JRBeanCollectionDataSource getLstOtrProdTarjetaCredito() {
		return lstOtrProdTarjetaCredito;
	}

	public void setLstOtrProdTarjetaCredito(
			JRBeanCollectionDataSource lstOtrProdTarjetaCredito) {
		this.lstOtrProdTarjetaCredito = lstOtrProdTarjetaCredito;
	}

	public JRBeanCollectionDataSource getLstOtrProdPatrimonial() {
		return lstOtrProdPatrimonial;
	}

	public void setLstOtrProdPatrimonial(
			JRBeanCollectionDataSource lstOtrProdPatrimonial) {
		this.lstOtrProdPatrimonial = lstOtrProdPatrimonial;
	}

}
