package com.bbva.msca.back.srv;



import com.bbva.msca.back.dao.CasosAsignadosDAO;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGenericPagination;
import com.bbva.msca.back.util.ResponseGeneric;

public class CasosPendientesServiceImpl implements CasosPendientesService{
	
	
	@Override
	public ResponseGeneric consultaCasosAsignadosConsultor(String cdConsultor,Integer limit, Integer offset,String search, String name, String order) {
		ResponseGeneric response = new ResponseGeneric();
		ResponseGenericPagination pagination = new ResponseGenericPagination();
		CasosAsignadosDAO casosAsignadosDAO = new CasosAsignadosDAO();
		
		try{
			pagination.setRows(casosAsignadosDAO.getConsultaCasosAsignadosConsultor(cdConsultor,limit,offset,search,name,order));
			pagination.setTotal(casosAsignadosDAO.getNuCasosAsignadosConsultor(cdConsultor,search));
			response.setResponse(pagination);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			
		}
		
		return response;
	}
}
