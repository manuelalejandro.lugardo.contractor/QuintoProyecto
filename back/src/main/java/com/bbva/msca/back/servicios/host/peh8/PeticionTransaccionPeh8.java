package com.bbva.msca.back.servicios.host.peh8;

import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.serializable.RooSerializable;
import org.springframework.roo.addon.tostring.RooToString;

/**
 * <p>Transacci&oacute;n <code>PEH8</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionPeh8</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionPeh8</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> config_ps9_pe</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEH8CCT.txt
 * PEH8CONSULTA DE INTERVINIENTES         PE        PE2C00H8MBDPEPO PEM0H8E             PEH8  NN0000NNNNNN    SSTN    C   NNNNNNNN  NN                2010-06-14CICSDM112013-11-0810.22.04QG4CUCCT2010-06-14-11.37.05.333504CICSDM110001-01-010001-01-01
 * FICHERO: PEH8FDF.txt
 * PEM0H8E �CONSULTA DE INTERVINIENTES    �F�03�00021�01�00001�NUMCTA �NUMERO DE CUENTA    �A�018�0�R�        �
 * PEM0H8E �CONSULTA DE INTERVINIENTES    �F�03�00021�02�00019�ULTCLA �ULTIMA CLAVE        �A�001�0�O�        �
 * PEM0H8E �CONSULTA DE INTERVINIENTES    �F�03�00021�03�00020�ULTSEC �ULTIMA SECUENCIA    �A�002�0�O�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�01�00001�CAMPO01�CVE.INTERVENCION    �A�001�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�02�00002�CAMPO02�DESC.CVE.INTERVENC. �A�015�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�03�00017�CAMPO03�SEC.INTERVENCION    �A�002�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�04�00019�CAMPO04�NUMERO DE CLIENTE   �A�008�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�05�00027�CAMPO05�CODIGO DE IDENTIFIC.�A�001�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�06�00028�CAMPO06�CVE.IDENTIFICACION  �A�010�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�07�00038�CAMPO07�DIGITO IDENTIFICAC. �A�001�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�08�00039�CAMPO08�SEC.IDENTIFICAC.    �A�002�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�09�00041�CAMPO09�SUBJGRUP            �A�001�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�10�00042�CAMPO10�SUJSUJGRUP          �A�002�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�11�00044�CAMPO11�PECNARES            �A�004�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�12�00048�CAMPO12�FECHA NACIMIENTO    �A�010�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�13�00058�CAMPO13�FECHAPE             �A�010�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�14�00068�CAMPO14�NOMBRE              �A�060�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�15�00128�CAMPO15�PARENTCLI           �A�040�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�16�00168�CAMPO16�PARTICIP            �N�003�0�S�        �
 * PEM0H8S �CONSULTA DE INTERVINIENTES    �X�17�00172�17�00171�CAMPO17�ESTAEXP             �A�002�0�S�        �
 * FICHERO: PEH8FDX.txt
 * PEH8PEM0H8S PEECH8S1PE2C00H81S                             CICSDM112010-07-06-10.21.34.988002CICSDM112010-07-06-10.21.34.988066
</pre></code>
 * 
 * @see RespuestaTransaccionPeh8
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "PEH8",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "config_ps9",
	respuesta = RespuestaTransaccionPeh8.class
)
@Multiformato(formatos = {FormatoPEM0H8E.class})
@RooJavaBean
@RooToString
@RooSerializable
public class PeticionTransaccionPeh8 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}