package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.sql.Date;

@SuppressWarnings("unused")
public class CedulaCasoDTO {
	
	private BigDecimal cdCaso;
	private String nbConsultor;
	private String otrasAlertas;
	private String cdOtroCaso;
	private String nuCliente;//StringType.INSTANCE)
	private String otrosClientes;//StringType.INSTANCE)
	private String tipoAsig;//StringType.INSTANCE)
	private String nuSuperCliente;
	private String nuFolioAlertaDC;
	private String tpPersona;//char
	private String cdCliente;//char
	
	
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getNuFolioAlertaDC() {
		return nuFolioAlertaDC;
	}
	public void setNuFolioAlertaDC(String nuFolioAlertaDC) {
		this.nuFolioAlertaDC = nuFolioAlertaDC;
	}
	public String getTpPersona() {
		return tpPersona;
	}
	public void setTpPersona(String tpPersona) {
		this.tpPersona = tpPersona;
	}
	public String getNuSuperCliente() {
		return nuSuperCliente;
	}
	public void setNuSuperCliente(String nuSuperCliente) {
		this.nuSuperCliente = nuSuperCliente;
	}
	public BigDecimal getCdCaso() {
		return cdCaso;
	}
	public void setCdCaso(BigDecimal cdCaso) {
		this.cdCaso = cdCaso;
	}
	//
	public String getNbConsultor() {
		return nbConsultor;
	}
	public void setNbConsultor(String nbConsultor) {
		this.nbConsultor = nbConsultor;
	}
	//
	public String getOtrasAlertas() {
		return otrasAlertas;
	}
	public void setOtrasAlertas(String otrasAlertas) {
		this.otrasAlertas = otrasAlertas;
	}
	//
	public String getCdOtroCaso() {
		return cdOtroCaso;
	}
	public void setCdOtroCaso(String cdOtroCaso) {
		this.cdOtroCaso = cdOtroCaso;
	}
	//
	public String getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(String nuCliente) {
		this.nuCliente = nuCliente;
	}
	//
	public String getOtrosClientes() {
		return otrosClientes;
	}
	public void setOtrosClientes(String otrosClientes) {
		this.otrosClientes = otrosClientes;
	}
	
	//
	public String getTipoAsig() {
		return tipoAsig;
	}
	public void setTipoAsig(String tipoAsig) {
		this.tipoAsig = tipoAsig;
	}
	public String getNuFolioAlerta() {
		return nuFolioAlertaDC;
	}
	public void setNuFolioAlerta(String nuFolioAlertaDC) {
		this.nuFolioAlertaDC = nuFolioAlertaDC;
	}	
	
}
