package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import com.bbva.msca.back.dominio.Tsca016ClientePK;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(versionField = "", table = "TSCA016_CLIENTE", schema = "GORAPR", identifierType = Tsca016ClientePK.class)
public class Tsca016Cliente {
}
