package com.bbva.msca.back.dominio;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(versionField = "", table = "TSCA028_TP_CTA", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca028TpCta {
	
	@Id
	@NotNull
	@Column(name = "TP_CTA")
	private BigDecimal cdTpCta;
}
