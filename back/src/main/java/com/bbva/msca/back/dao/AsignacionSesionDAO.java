package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.bbva.msca.back.dominio.Tsca018DetCatalog;
import com.bbva.msca.back.dominio.Tsca023Sesion;
import com.bbva.msca.back.dominio.Tsca030Gerencia;
import com.bbva.msca.back.dominio.Tsca035AsigSes;
import com.bbva.msca.back.dominio.Tsca035AsigSesPK;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dto.AsignacionSesionDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

public class AsignacionSesionDAO {

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	/**
	 * Bitacora de movimientos Sesion - Tipologia
	 * */
	private Tsca055Log log;

	@SuppressWarnings("unchecked")
	public List<AsignacionSesionDTO> getAsignacionSesiones() throws Exception {

		List<AsignacionSesionDTO> lstAsignacionSesionDTO = new ArrayList<AsignacionSesionDTO>();

		Query query = em.createNativeQuery(Consultas.getConsulta("getAsignacionSesionesParametria"));
		query.setParameter(1, Constantes.CD_ST_SIS_A);

		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdSesion", StringType.INSTANCE)
				.addScalar("fhPeIn", TimestampType.INSTANCE)
				.addScalar("fhPeFn", TimestampType.INSTANCE)
				.addScalar("cdGerencia")
				.addScalar("nbGerencia", StringType.INSTANCE)
				.addScalar("nuAlertas")
				.addScalar("fhAlta", TimestampType.INSTANCE)
				.addScalar("nbUsrRed", StringType.INSTANCE)
				.addScalar("cdDetCatalogo", StringType.INSTANCE)
				.setResultTransformer(
				Transformers.aliasToBean(AsignacionSesionDTO.class));

		lstAsignacionSesionDTO = (List<AsignacionSesionDTO>) query
				.getResultList();

		return lstAsignacionSesionDTO;
	}

	public int creaAsigSesion(AsignacionSesionDTO asigSesion)
			throws Exception {
		int op = Constantes.FIN_ER;
		Tsca035AsigSes nuevaAsigSesion = Tsca035AsigSes
				.findTsca035AsigSes(new Tsca035AsigSesPK(asigSesion
						.getCdSesion(), asigSesion.getCdGerencia()));
		Tsca018DetCatalog st_baja = Tsca018DetCatalog.findTsca018DetCatalog(Constantes.CD_ST_SIS_I);
		if(nuevaAsigSesion != null){
			if(nuevaAsigSesion.getCdDetCatalogo().equals(st_baja.getCdDetCatalogo())){
				this.copiaABC(nuevaAsigSesion, asigSesion);
				nuevaAsigSesion.merge();
				nuevaAsigSesion.flush();
				this.log = this.generaLog(asigSesion, 1);
				this.log.persist();
				this.log.flush();
				op = Constantes.FIN_OK;
			}else{
				op = Constantes.FIN_AV;
			}
		}else{
			nuevaAsigSesion = new Tsca035AsigSes();
			copiaABC(nuevaAsigSesion, asigSesion);
			nuevaAsigSesion.persist();
			nuevaAsigSesion.flush();
			this.log = this.generaLog(asigSesion, 1);
			this.log.persist();
			this.log.flush();
			op = Constantes.FIN_OK;
		}
		return op;
	}

	public AsignacionSesionDTO modificaAsigSesion(AsignacionSesionDTO asigSesion)
			throws Exception {
		Tsca035AsigSes nuevaAsigSesion = new Tsca035AsigSes();
		this.copiaABC(nuevaAsigSesion, asigSesion);
		nuevaAsigSesion.merge();
		nuevaAsigSesion.flush();
		this.log = this.generaLog(asigSesion, 2);
		this.log.persist();
		this.log.flush();
		return asigSesion;
	}
	
	private void copiaABC(Tsca035AsigSes nuevaAsigSesion, AsignacionSesionDTO asigSesion){
		nuevaAsigSesion.setId(new Tsca035AsigSesPK(asigSesion.getCdSesion(),asigSesion.getCdGerencia()));
		nuevaAsigSesion.setCdGerencia(Tsca030Gerencia.findTsca030Gerencia(asigSesion.getCdGerencia()));
		nuevaAsigSesion.setCdSesion(Tsca023Sesion.findTsca023Sesion(asigSesion.getCdSesion()));
		nuevaAsigSesion.setNuMaxAler(asigSesion.getNuAlertas());
		nuevaAsigSesion.setCdDetCatalogo(asigSesion.getCdDetCatalogo());
	}
	
	private Tsca055Log generaLog(AsignacionSesionDTO asigSesion, int accion){
		Tsca055Log logSesion = new Tsca055Log();
		logSesion.setNbEvento("MSCAVE01012_3");
		logSesion.setCdUsuario(asigSesion.getCdUsrAlta());
		logSesion.setFhFecha(new GregorianCalendar().getTime());
		switch(accion){
		case 1:
			logSesion.setNbAccionRealizad("Alta: " + asigSesion.getCdSesion().trim());
			break;
		case 2:		
			logSesion.setNbAccionRealizad("Modifica: " + asigSesion.getCdSesion().trim());
			break;
		}
		return logSesion;
	}

}
