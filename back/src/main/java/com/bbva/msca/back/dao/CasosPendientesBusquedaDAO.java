package com.bbva.msca.back.dao;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;

import com.bbva.msca.back.dto.CasosPendientesBusquedaDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class CasosPendientesBusquedaDAO {
		
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	    
		@SuppressWarnings("unchecked")
		public CasosPendientesBusquedaDTO getCasosPendientesBusqueda() throws Exception{
			List<CasosPendientesBusquedaDTO> resultado = new ArrayList<CasosPendientesBusquedaDTO>(); 
			CasosPendientesBusquedaDTO casosPendientesBusquedaDTO = new CasosPendientesBusquedaDTO();
			Query query = em.createNativeQuery(Consultas.getConsulta("getCasosPendientesBusqueda"));
			
			query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nuCaso")
			.addScalar("nuFolio")
			.setResultTransformer(Transformers.aliasToBean(CasosPendientesBusquedaDTO.class));
			resultado = (List<CasosPendientesBusquedaDTO>)query.getResultList();
			if( resultado != null && !resultado.isEmpty()){
				casosPendientesBusquedaDTO = resultado.get(0);	
			}			
			return casosPendientesBusquedaDTO;
	}
			
}
