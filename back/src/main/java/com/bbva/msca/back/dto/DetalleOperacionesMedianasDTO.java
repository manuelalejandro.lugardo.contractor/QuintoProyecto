package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class DetalleOperacionesMedianasDTO {

	private String nbReferencia;
	private String nuCuentas;
	private Date fhOperacion;
	private String nbTipoTransaccion;
	private String nbCorresponsal;
	private String nbEntidadOrigen;
	private String nbEntidadDestino;
	private String nbDatoDestino;
	private String nbDatoOrdenante;
	private String nbOperacionOrigen;
	private String nbOperacionDestino;
	private String nbMoneda;
	private String nbInstrumento;
	private String imDolarizado;
	private String nbPaisFIS;
	private String nbRazonSocial;
	private BigDecimal imOperacion;
	private String nbOficina;
	
	public BigDecimal getImOperacion() {
		return imOperacion;
	}
	public void setImOperacion(BigDecimal imOperacion) {
		this.imOperacion = imOperacion;
	}
	public String getNbOficina() {
		return nbOficina;
	}
	public void setNbOficina(String nbOficina) {
		this.nbOficina = nbOficina;
	}
	public String getNbReferencia() {
		return nbReferencia;
	}
	public void setNbReferencia(String nbReferencia) {
		this.nbReferencia = nbReferencia;
	}
	public String getNuCuentas() {
		return nuCuentas;
	}
	public void setNuCuentas(String nuCuentas) {
		this.nuCuentas = nuCuentas;
	}
	public Date getFhOperacion() {
		return fhOperacion;
	}
	public void setFhOperacion(Date fhOperacion) {
		this.fhOperacion = fhOperacion;
	}
	public String getNbTipoTransaccion() {
		return nbTipoTransaccion;
	}
	public void setNbTipoTransaccion(String nbTipoTransaccion) {
		this.nbTipoTransaccion = nbTipoTransaccion;
	}
	public String getNbCorresponsal() {
		return nbCorresponsal;
	}
	public void setNbCorresponsal(String nbCorresponsal) {
		this.nbCorresponsal = nbCorresponsal;
	}
	public String getNbEntidadOrigen() {
		return nbEntidadOrigen;
	}
	public void setNbEntidadOrigen(String nbEntidadOrigen) {
		this.nbEntidadOrigen = nbEntidadOrigen;
	}
	public String getNbEntidadDestino() {
		return nbEntidadDestino;
	}
	public void setNbEntidadDestino(String nbEntidadDestino) {
		this.nbEntidadDestino = nbEntidadDestino;
	}
	public String getNbDatoDestino() {
		return nbDatoDestino;
	}
	public void setNbDatoDestino(String nbDatoDestino) {
		this.nbDatoDestino = nbDatoDestino;
	}
	public String getNbDatoOrdenante() {
		return nbDatoOrdenante;
	}
	public void setNbDatoOrdenante(String nbDatoOrdenante) {
		this.nbDatoOrdenante = nbDatoOrdenante;
	}
	public String getNbOperacionOrigen() {
		return nbOperacionOrigen;
	}
	public void setNbOperacionOrigen(String nbOperacionOrigen) {
		this.nbOperacionOrigen = nbOperacionOrigen;
	}
	public String getNbOperacionDestino() {
		return nbOperacionDestino;
	}
	public void setNbOperacionDestino(String nbOperacionDestino) {
		this.nbOperacionDestino = nbOperacionDestino;
	}
	public String getNbMoneda() {
		return nbMoneda;
	}
	public void setNbMoneda(String nbMoneda) {
		this.nbMoneda = nbMoneda;
	}
	public String getNbInstrumento() {
		return nbInstrumento;
	}
	public void setNbInstrumento(String nbInstrumento) {
		this.nbInstrumento = nbInstrumento;
	}
	public String getImDolarizado() {
		return imDolarizado;
	}
	public void setImDolarizado(String imDolarizado) {
		this.imDolarizado = imDolarizado;
	}
	public String getNbPaisFIS() {
		return nbPaisFIS;
	}
	public void setNbPaisFIS(String nbPaisFIS) {
		this.nbPaisFIS = nbPaisFIS;
	}
	public String getNbRazonSocial() {
		return nbRazonSocial;
	}
	public void setNbRazonSocial(String nbRazonSocial) {
		this.nbRazonSocial = nbRazonSocial;
	}
}
