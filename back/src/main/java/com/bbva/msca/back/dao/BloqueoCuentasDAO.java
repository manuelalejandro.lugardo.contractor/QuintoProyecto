package com.bbva.msca.back.dao;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.bbva.msca.back.dto.BloqueoCuentasDTO;
import com.bbva.msca.back.dto.BusquedaBloqueoCuentasDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class BloqueoCuentasDAO{
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	@SuppressWarnings("unchecked")
	public List<BloqueoCuentasDTO> getConsultaBloqueoCuentasFiltro(BusquedaBloqueoCuentasDTO busquedaBloqueoCuentasDTO) throws Exception{
		
		 List<BloqueoCuentasDTO> lstBusquedaBloqueoCuentasDTO = null;
		 String consulta = Consultas.getConsulta("getConsultaBloqueoCuentas");
		 String filtro = "";
		 String condicion = " WHERE ";
		 boolean isFiltro = false;
		 SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
		 if( busquedaBloqueoCuentasDTO != null){
			 
			 if(busquedaBloqueoCuentasDTO.getCdCaso() != null){
				 filtro = filtro + " CP.cdCaso = "+busquedaBloqueoCuentasDTO.getCdCaso();
				 isFiltro = true;
				
			 } 
			 if(busquedaBloqueoCuentasDTO.getNuFolioAlerta() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.nuFolioAlerta = '"+busquedaBloqueoCuentasDTO.getNuFolioAlerta()+"'";
				 }else{
					 filtro = filtro + " AND CP.nuFolioAlerta = '"+busquedaBloqueoCuentasDTO.getNuFolioAlerta()+"'";
				 }
				 
				 isFiltro = true;
				 
			 }
			 if(busquedaBloqueoCuentasDTO.getCdCliente() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdCliente = '"+busquedaBloqueoCuentasDTO.getCdCliente()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdCliente = '"+busquedaBloqueoCuentasDTO.getCdCliente()+"'";
				 }
				 isFiltro = true;
				 
			 }
			 if(busquedaBloqueoCuentasDTO.getNbCliente() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + "  CP.nbCliente = '"+ busquedaBloqueoCuentasDTO.getNbCliente()+"'";
				 }else{
					 filtro = filtro + " AND CP.nbCliente = '"+ busquedaBloqueoCuentasDTO.getNbCliente()+"'";
				 }
				 isFiltro = true;
				 
			 }
			 if(busquedaBloqueoCuentasDTO.getNuCuenta() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.nuCuenta = '"+ busquedaBloqueoCuentasDTO.getNuCuenta()+"'";
				 }else{
					 filtro = filtro + " AND CP.nuCuenta = '"+ busquedaBloqueoCuentasDTO.getNuCuenta()+"'";
				 }
				 isFiltro = true;
				 
			 }
			 if(busquedaBloqueoCuentasDTO.getCdTipologia() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdTipologia = '"+ busquedaBloqueoCuentasDTO.getCdTipologia()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdTipologia = '"+ busquedaBloqueoCuentasDTO.getCdTipologia()+"'";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(busquedaBloqueoCuentasDTO.getCdSector() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdSector = '"+ busquedaBloqueoCuentasDTO.getCdSector()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdSector = '"+ busquedaBloqueoCuentasDTO.getCdSector()+"'";
				 }
				 isFiltro = true;
				 
			 }			 
			 
			 if(busquedaBloqueoCuentasDTO.getImImporte() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.imImporte " + busquedaBloqueoCuentasDTO.getCdImporte()  + busquedaBloqueoCuentasDTO.getImImporte();
				 }else{
					 filtro = filtro + " AND CP.imImporte"  + busquedaBloqueoCuentasDTO.getCdImporte() +  busquedaBloqueoCuentasDTO.getImImporte();
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(busquedaBloqueoCuentasDTO.getFhAlertaDel() != null || busquedaBloqueoCuentasDTO.getFhAlertaAl() != null ){
				 if(busquedaBloqueoCuentasDTO.getFhAlertaDel() != null && busquedaBloqueoCuentasDTO.getFhAlertaAl() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhAlerta BETWEEN " + "TO_DATE('"  + formato.format(busquedaBloqueoCuentasDTO.getFhAlertaDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(busquedaBloqueoCuentasDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND CP.fhAlerta BETWEEN " + "TO_DATE('"  + formato.format(busquedaBloqueoCuentasDTO.getFhAlertaDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(busquedaBloqueoCuentasDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(busquedaBloqueoCuentasDTO.getFhAlertaDel() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhAlerta = TO_DATE ('"  + formato.format(busquedaBloqueoCuentasDTO.getFhAlertaDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND CP.fhAlerta = TO_DATE ('"  + formato.format(busquedaBloqueoCuentasDTO.getFhAlertaDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(busquedaBloqueoCuentasDTO.getFhAlertaAl() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhAlerta = TO_DATE ('"  + formato.format(busquedaBloqueoCuentasDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND CP.fhAlerta = TO_DATE ('"  + formato.format(busquedaBloqueoCuentasDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 if(isFiltro){
				consulta = consulta+condicion+filtro;
			 }
			
		 }
		Query query = em.createNativeQuery(consulta);
		
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("cdCaso")
			.addScalar("nuFolioAlerta")
		    .addScalar("cdCliente",StringType.INSTANCE)
            .addScalar("nbCliente")
            .addScalar("nuCuenta",StringType.INSTANCE)
            .addScalar("cdSistema" ,StringType.INSTANCE)
            .addScalar("cdTipologia",StringType.INSTANCE)
            .addScalar("stBloqueo",StringType.INSTANCE)
            .addScalar("fhBloqueo",TimestampType.INSTANCE)
            .addScalar("nbReconsiderado")
            .addScalar("nbObservaciones")
            .addScalar("cdSector",StringType.INSTANCE)
            .addScalar("imImporte")
		 .setResultTransformer(Transformers.aliasToBean(BloqueoCuentasDTO.class));
				
		 lstBusquedaBloqueoCuentasDTO = (List<BloqueoCuentasDTO>)query.getResultList();
			
		
			return lstBusquedaBloqueoCuentasDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<BloqueoCuentasDTO> getConsultaBloqueoCuentas() throws Exception{
		
		 List<BloqueoCuentasDTO> lstBloqueoCuentasDTO = null;
		 Query query = em.createNativeQuery(Consultas.getConsulta("getConsultaBloqueoCuentas"));
		
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("cdCaso")
			.addScalar("nuFolioAlerta")
		    .addScalar("cdCliente",StringType.INSTANCE)
            .addScalar("nbCliente")
            .addScalar("nuCuenta",StringType.INSTANCE)
            .addScalar("cdSistema" ,StringType.INSTANCE)
            .addScalar("cdTipologia",StringType.INSTANCE)
            .addScalar("stBloqueo",StringType.INSTANCE)
            .addScalar("fhBloqueo",TimestampType.INSTANCE)
            .addScalar("nbReconsiderado")
            .addScalar("nbObservaciones")
            .addScalar("cdSector",StringType.INSTANCE)
            .addScalar("imImporte")
		 .setResultTransformer(Transformers.aliasToBean(BloqueoCuentasDTO.class));
				
		 lstBloqueoCuentasDTO = (List<BloqueoCuentasDTO>)query.getResultList();
			
		
			return lstBloqueoCuentasDTO;
		}
}
