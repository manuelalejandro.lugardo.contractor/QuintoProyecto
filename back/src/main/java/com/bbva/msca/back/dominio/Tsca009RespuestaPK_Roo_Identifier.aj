// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import java.lang.Object;
import java.lang.String;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Embeddable;

privileged aspect Tsca009RespuestaPK_Roo_Identifier {
    
    declare @type: Tsca009RespuestaPK: @Embeddable;
    
    @Column(name = "CD_ALERTA", nullable = false, length = 10)
    private String Tsca009RespuestaPK.cdAlerta;
    
    @Column(name = "CD_SISTEMA", nullable = false, length = 3)
    private String Tsca009RespuestaPK.cdSistema;
    
    @Column(name = "NU_PREGUNTA", nullable = false)
    private BigDecimal Tsca009RespuestaPK.nuPregunta;
    
    public Tsca009RespuestaPK.new(String cdAlerta, String cdSistema, BigDecimal nuPregunta) {
        super();
        this.cdAlerta = cdAlerta;
        this.cdSistema = cdSistema;
        this.nuPregunta = nuPregunta;
    }

    private Tsca009RespuestaPK.new() {
        super();
    }

    public String Tsca009RespuestaPK.getCdAlerta() {
        return this.cdAlerta;
    }
    
    public String Tsca009RespuestaPK.getCdSistema() {
        return this.cdSistema;
    }
    
    public BigDecimal Tsca009RespuestaPK.getNuPregunta() {
        return this.nuPregunta;
    }
    
    public boolean Tsca009RespuestaPK.equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (!(obj instanceof Tsca009RespuestaPK)) return false;
        Tsca009RespuestaPK other = (Tsca009RespuestaPK) obj;
        if (cdAlerta == null) {
            if (other.cdAlerta != null) return false;
        } else if (!cdAlerta.equals(other.cdAlerta)) return false;
        if (cdSistema == null) {
            if (other.cdSistema != null) return false;
        } else if (!cdSistema.equals(other.cdSistema)) return false;
        if (nuPregunta == null) {
            if (other.nuPregunta != null) return false;
        } else if (!nuPregunta.equals(other.nuPregunta)) return false;
        return true;
    }
    
    public int Tsca009RespuestaPK.hashCode() {
        final int prime = 31;
        int result = 17;
        result = prime * result + (cdAlerta == null ? 0 : cdAlerta.hashCode());
        result = prime * result + (cdSistema == null ? 0 : cdSistema.hashCode());
        result = prime * result + (nuPregunta == null ? 0 : nuPregunta.hashCode());
        return result;
    }
    
}
