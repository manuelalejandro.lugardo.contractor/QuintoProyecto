package com.bbva.msca.back.dto;

import java.math.BigDecimal;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaRPDTO {

	@AnotacionAlerta(tipologia = "rp", idCampo = "cdPeriodo", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_CDPERIODO")
	private BigDecimal cdPeriodo;
	@AnotacionAlerta(tipologia = "rp", idCampo = "cdAccion", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_CDACCION", instanceType = "1")
	private String cdAccion;
	@AnotacionAlerta(tipologia = "rp", idCampo = "nbCte", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")
	private String nbCte;
	@AnotacionAlerta(tipologia = "rp", idCampo = "nbActividad", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_NBACTIVIDAD", instanceType = "1")
	private String nbActividad;
	@AnotacionAlerta(tipologia = "rp", idCampo = "tpPerJuri", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_TPPERJURI", instanceType = "1")
	private String tpPerJuri;
	@AnotacionAlerta(tipologia = "rp", idCampo = "cdOficinaGest", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_CDOFICINAGEST", instanceType = "1")
	private String cdOficinaGest;
	@AnotacionAlerta(tipologia = "rp", idCampo = "imScore", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_IMSCORE")
	private BigDecimal imScore;
	@AnotacionAlerta(tipologia = "rp", idCampo = "ctMovimiento", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_CTMOVIMIENTO", instanceType = "1")
	private String ctMovimiento;
	@AnotacionAlerta(tipologia = "rp", idCampo = "cdDirecta", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_CDDIRECTA", instanceType = "1")
	private String cdDirecta;
	@AnotacionAlerta(tipologia = "rp", idCampo = "nuScoreCorte", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_NUSCORECORTE")
	private BigDecimal nuScoreCorte;
	@AnotacionAlerta(tipologia = "rp", idCampo = "nbBanca", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_NBBANCA", instanceType = "1")
	private String nbBanca;
	@AnotacionAlerta(tipologia = "rp", idCampo = "cdCte", posicion = "12", nbEtiqueta = "MSCK_LBL_01002_CDCTE")	
	private String cdCte;

	
	
	public String getCdCte() {
		return cdCte;
	}
	public void setCdCte(String cdCte) {
		this.cdCte = cdCte;
	}
	public String getNbBanca() {
		return nbBanca;
	}
	public void setNbBanca(String nbBanca) {
		this.nbBanca = nbBanca;
	}
	public BigDecimal getCdPeriodo() {
		return cdPeriodo;
	}
	public void setCdPeriodo(BigDecimal cdPeriodo) {
		this.cdPeriodo = cdPeriodo;
	}
	public String getCdAccion() {
		return cdAccion;
	}
	public void setCdAccion(String cdAccion) {
		this.cdAccion = cdAccion;
	}
	public String getNbCte() {
		return nbCte;
	}
	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}
	public String getNbActividad() {
		return nbActividad;
	}
	public void setNbActividad(String nbActividad) {
		this.nbActividad = nbActividad;
	}
	public String getTpPerJuri() {
		return tpPerJuri;
	}
	public void setTpPerJuri(String tpPerJuri) {
		this.tpPerJuri = tpPerJuri;
	}
	public String getCdOficinaGest() {
		return cdOficinaGest;
	}
	public void setCdOficinaGest(String cdOficinaGest) {
		this.cdOficinaGest = cdOficinaGest;
	}
	public BigDecimal getImScore() {
		return imScore;
	}
	public void setImScore(BigDecimal imScore) {
		this.imScore = imScore;
	}
	public String getCtMovimiento() {
		return ctMovimiento;
	}
	public void setCtMovimiento(String ctMovimiento) {
		this.ctMovimiento = ctMovimiento;
	}
	public String getCdDirecta() {
		return cdDirecta;
	}
	public void setCdDirecta(String cdDirecta) {
		this.cdDirecta = cdDirecta;
	}
	public BigDecimal getNuScoreCorte() {
		return nuScoreCorte;
	}
	public void setNuScoreCorte(BigDecimal nuScoreCorte) {
		this.nuScoreCorte = nuScoreCorte;
	}
}
