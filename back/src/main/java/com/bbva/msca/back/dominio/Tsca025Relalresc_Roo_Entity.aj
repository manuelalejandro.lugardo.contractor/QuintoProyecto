// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca025Relalresc;
import com.bbva.msca.back.dominio.Tsca025RelalrescPK;
import java.util.List;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca025Relalresc_Roo_Entity {
    
    declare @type: Tsca025Relalresc: @Entity;
    
    declare @type: Tsca025Relalresc: @Table(name = "TSCA025_RELALRESC", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca025Relalresc.entityManager;
    
    @EmbeddedId
    private Tsca025RelalrescPK Tsca025Relalresc.id;
    
    public Tsca025RelalrescPK Tsca025Relalresc.getId() {
        return this.id;
    }
    
    public void Tsca025Relalresc.setId(Tsca025RelalrescPK id) {
        this.id = id;
    }
    
    @Transactional
    public void Tsca025Relalresc.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca025Relalresc.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca025Relalresc attached = Tsca025Relalresc.findTsca025Relalresc(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca025Relalresc.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca025Relalresc.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca025Relalresc Tsca025Relalresc.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca025Relalresc merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca025Relalresc.entityManager() {
        EntityManager em = new Tsca025Relalresc().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca025Relalresc.countTsca025Relalrescs() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca025Relalresc o", Long.class).getSingleResult();
    }
    
    public static List<Tsca025Relalresc> Tsca025Relalresc.findAllTsca025Relalrescs() {
        return entityManager().createQuery("SELECT o FROM Tsca025Relalresc o", Tsca025Relalresc.class).getResultList();
    }
    
    public static Tsca025Relalresc Tsca025Relalresc.findTsca025Relalresc(Tsca025RelalrescPK id) {
        if (id == null) return null;
        return entityManager().find(Tsca025Relalresc.class, id);
    }
    
    public static List<Tsca025Relalresc> Tsca025Relalresc.findTsca025RelalrescEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca025Relalresc o", Tsca025Relalresc.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
