// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca059MovTc;
import com.bbva.msca.back.dominio.Tsca059MovTcPK;
import java.util.List;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca059MovTc_Roo_Entity {
    
    declare @type: Tsca059MovTc: @Entity;
    
    declare @type: Tsca059MovTc: @Table(name = "TSCA059_MOV_TC", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca059MovTc.entityManager;
    
    @EmbeddedId
    private Tsca059MovTcPK Tsca059MovTc.id;
    
    public Tsca059MovTcPK Tsca059MovTc.getId() {
        return this.id;
    }
    
    public void Tsca059MovTc.setId(Tsca059MovTcPK id) {
        this.id = id;
    }
    
    @Transactional
    public void Tsca059MovTc.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca059MovTc.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca059MovTc attached = Tsca059MovTc.findTsca059MovTc(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca059MovTc.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca059MovTc.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca059MovTc Tsca059MovTc.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca059MovTc merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca059MovTc.entityManager() {
        EntityManager em = new Tsca059MovTc().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca059MovTc.countTsca059MovTcs() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca059MovTc o", Long.class).getSingleResult();
    }
    
    public static List<Tsca059MovTc> Tsca059MovTc.findAllTsca059MovTcs() {
        return entityManager().createQuery("SELECT o FROM Tsca059MovTc o", Tsca059MovTc.class).getResultList();
    }
    
    public static Tsca059MovTc Tsca059MovTc.findTsca059MovTc(Tsca059MovTcPK id) {
        if (id == null) return null;
        return entityManager().find(Tsca059MovTc.class, id);
    }
    
    public static List<Tsca059MovTc> Tsca059MovTc.findTsca059MovTcEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca059MovTc o", Tsca059MovTc.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
