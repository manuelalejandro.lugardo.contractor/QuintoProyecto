package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class CuentasClienteRiesgoDTO {

	private BigDecimal cdClialtrie;
	private BigDecimal nuID;
	private String nbTipo;
	private String nbPrioridad;
	private String cdPrioridad;
	private Date fhRecepcion;
	private String nuOficio;
	private String nuExpediente;
	private BigDecimal nuFolio;
	private String nbDescripcion;
	private Date fhPublicacion;
	private BigDecimal nuDiasPlazo;
	private String nbAutoridad;
	private String nbReferencia;
	private String nbAseguramiento;
	private String nbSolicitudEspecifica;
	private String nbSolicitudEspecifica2;
	private String nuFolioArchivo;
	private List<CuentasClienteRiesgoDetDTO> lstClientesRiesgo;
	
	
	//Cuentas de Cliente de  Riesgo
	
	private String nuCuenta;
	private String cdCliente;
	private String nbTparti;
	private String nuSecParticipacion;
	private String nbTitular;
	private String stEstatus;
	private BigDecimal imIngresos;
	private BigDecimal imEgresos;
	private BigDecimal imEfecRealIngresos;
	private BigDecimal imEfecRealEgresos;
	private BigDecimal imSaldoDisp;
	private String nbBloqueo;
	private Date fhCancelacion;
	private String cdTipologia; 
	
	
	
	public String getCdTipologia() {
		return cdTipologia;
	}
	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	public BigDecimal getNuID() {
		return nuID;
	}
	public void setNuID(BigDecimal nuID) {
		this.nuID = nuID;
	}
	public String getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getNbTparti() {
		return nbTparti;
	}
	public void setNbTparti(String nbTparti) {
		this.nbTparti = nbTparti;
	}
	public String getNuSecParticipacion() {
		return nuSecParticipacion;
	}
	public void setNuSecParticipacion(String nuSecParticipacion) {
		this.nuSecParticipacion = nuSecParticipacion;
	}
	public String getNbTitular() {
		return nbTitular;
	}
	public void setNbTitular(String nbTitular) {
		this.nbTitular = nbTitular;
	}
	public String getStEstatus() {
		return stEstatus;
	}
	public void setStEstatus(String stEstatus) {
		this.stEstatus = stEstatus;
	}
	public BigDecimal getImIngresos() {
		return imIngresos;
	}
	public void setImIngresos(BigDecimal imIngresos) {
		this.imIngresos = imIngresos;
	}
	public BigDecimal getImEgresos() {
		return imEgresos;
	}
	public void setImEgresos(BigDecimal imEgresos) {
		this.imEgresos = imEgresos;
	}
	public BigDecimal getImEfecRealIngresos() {
		return imEfecRealIngresos;
	}
	public void setImEfecRealIngresos(BigDecimal imEfecRealIngresos) {
		this.imEfecRealIngresos = imEfecRealIngresos;
	}
	public BigDecimal getImEfecRealEgresos() {
		return imEfecRealEgresos;
	}
	public void setImEfecRealEgresos(BigDecimal imEfecRealEgresos) {
		this.imEfecRealEgresos = imEfecRealEgresos;
	}
	public BigDecimal getImSaldoDisp() {
		return imSaldoDisp;
	}
	public void setImSaldoDisp(BigDecimal imSaldoDisp) {
		this.imSaldoDisp = imSaldoDisp;
	}
	public String getNbBloqueo() {
		return nbBloqueo;
	}
	public void setNbBloqueo(String nbBloqueo) {
		this.nbBloqueo = nbBloqueo;
	}
	public Date getFhCancelacion() {
		return fhCancelacion;
	}
	public void setFhCancelacion(Date fhCancelacion) {
		this.fhCancelacion = fhCancelacion;
	}
	/**
	 * @return the cdClialtrie
	 */
	public BigDecimal getCdClialtrie() {
		return cdClialtrie;
	}
	/**
	 * @param cdClialtrie the cdClialtrie to set
	 */
	public void setCdClialtrie(BigDecimal cdClialtrie) {
		this.cdClialtrie = cdClialtrie;
	}
	/**
	 * @return the nbTipo
	 */
	public String getNbTipo() {
		return nbTipo;
	}
	/**
	 * @param nbTipo the nbTipo to set
	 */
	public void setNbTipo(String nbTipo) {
		this.nbTipo = nbTipo;
	}
	/**
	 * @return the nbPrioridad
	 */
	public String getNbPrioridad() {
		return nbPrioridad;
	}
	/**
	 * @param nbPrioridad the nbPrioridad to set
	 */
	public void setNbPrioridad(String nbPrioridad) {
		this.nbPrioridad = nbPrioridad;
	}
	
		
	/**
	 * @return the cdPrioridad
	 */
	public String getCdPrioridad() {
		return cdPrioridad;
	}
	/**
	 * @param cdPrioridad the cdPrioridad to set
	 */
	public void setCdPrioridad(String cdPrioridad) {
		this.cdPrioridad = cdPrioridad;
	}
	/**
	 * @return the fhRecepcion
	 */
	public Date getFhRecepcion() {
		return fhRecepcion;
	}
	/**
	 * @param fhRecepcion the fhRecepcion to set
	 */
	public void setFhRecepcion(Date fhRecepcion) {
		this.fhRecepcion = fhRecepcion;
	}
	/**
	 * @return the nuOficio
	 */
	public String getNuOficio() {
		return nuOficio;
	}
	/**
	 * @param nuOficio the nuOficio to set
	 */
	public void setNuOficio(String nuOficio) {
		this.nuOficio = nuOficio;
	}
	/**
	 * @return the nuExpediente
	 */
	public String getNuExpediente() {
		return nuExpediente;
	}
	/**
	 * @param nuExpediente the nuExpediente to set
	 */
	public void setNuExpediente(String nuExpediente) {
		this.nuExpediente = nuExpediente;
	}
	/**
	 * @return the nuFolio
	 */
	public BigDecimal getNuFolio() {
		return nuFolio;
	}
	/**
	 * @param nuFolio the nuFolio to set
	 */
	public void setNuFolio(BigDecimal nuFolio) {
		this.nuFolio = nuFolio;
	}
	/**
	 * @return the nbDescripcion
	 */
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	/**
	 * @param nbDescripcion the nbDescripcion to set
	 */
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	/**
	 * @return the fhPublicacion
	 */
	public Date getFhPublicacion() {
		return fhPublicacion;
	}
	/**
	 * @param fhPublicacion the fhPublicacion to set
	 */
	public void setFhPublicacion(Date fhPublicacion) {
		this.fhPublicacion = fhPublicacion;
	}
	/**
	 * @return the nuDias
	 */
	public BigDecimal getNuDiasPlazo() {
		return nuDiasPlazo;
	}
	/**
	 * @param nuDias the nuDias to set
	 */
	public void setNuDiasPlazo(BigDecimal nuDiasPlazo) {
		this.nuDiasPlazo = nuDiasPlazo;
	}
	/**
	 * @return the nbAutoridad
	 */
	public String getNbAutoridad() {
		return nbAutoridad;
	}
	/**
	 * @param nbAutoridad the nbAutoridad to set
	 */
	public void setNbAutoridad(String nbAutoridad) {
		this.nbAutoridad = nbAutoridad;
	}
	/**
	 * @return the nbReferencia
	 */
	public String getNbReferencia() {
		return nbReferencia;
	}
	/**
	 * @param nbReferencia the nbReferencia to set
	 */
	public void setNbReferencia(String nbReferencia) {
		this.nbReferencia = nbReferencia;
	}
	/**
	 * @return the nbAseguramiento
	 */
	public String getNbAseguramiento() {
		return nbAseguramiento;
	}
	/**
	 * @param nbAseguramiento the nbAseguramiento to set
	 */
	public void setNbAseguramiento(String nbAseguramiento) {
		this.nbAseguramiento = nbAseguramiento;
	}
	/**
	 * @return the nbSolicitudEspecifica
	 */
	public String getNbSolicitudEspecifica() {
		return nbSolicitudEspecifica;
	}
	/**
	 * @param nbSolicitudEspecifica the nbSolicitudEspecifica to set
	 */
	public void setNbSolicitudEspecifica(String nbSolicitudEspecifica) {
		this.nbSolicitudEspecifica = nbSolicitudEspecifica;
	}
	/**
	 * @return the lstClientesRiesgo
	 */
	public List<CuentasClienteRiesgoDetDTO> getLstClientesRiesgo() {
		return lstClientesRiesgo;
	}
	/**
	 * @param lstClientesRiesgo the lstClientesRiesgo to set
	 */
	public void setLstClientesRiesgo(
			List<CuentasClienteRiesgoDetDTO> lstClientesRiesgo) {
		this.lstClientesRiesgo = lstClientesRiesgo;
	}
	
	
	
	public String getNbSolicitudEspecifica2() {
		return nbSolicitudEspecifica2;
	}
	public void setNbSolicitudEspecifica2(String nbSolicitudEspecifica2) {
		this.nbSolicitudEspecifica2 = nbSolicitudEspecifica2;
	}
	public String getNuFolioArchivo() {
		return nuFolioArchivo;
	}
	public void setNuFolioArchivo(String nuFolioArchivo) {
		this.nuFolioArchivo = nuFolioArchivo;
	}
	public String toString(){
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append( "\n == CLIENTE DE ALTO RIESGO ==" );
		strBuilder.append( "\n cdClialtrie : " + cdClialtrie );
		strBuilder.append( "\n nbTipo : " + nbTipo );
		strBuilder.append( "\n nbPrioridad : " + nbPrioridad );
		strBuilder.append( "\n cdPrioridad : " + cdPrioridad );
		strBuilder.append( "\n fhRecepcion : " + fhRecepcion );
		strBuilder.append( "\n nuOficio : " + nuOficio );
		strBuilder.append( "\n nuExpediente : " + nuExpediente );
		strBuilder.append( "\n nuFolio : " + nuFolio );
		strBuilder.append( "\n nbDescripcion : " + nbDescripcion );
		strBuilder.append( "\n fhPublicacion : " + fhPublicacion );
		strBuilder.append( "\n nuDiasPlazo : " + nuDiasPlazo );
		strBuilder.append( "\n nbAutoridad : " + nbAutoridad );
		strBuilder.append( "\n nbReferencia : " + nbReferencia );;
		strBuilder.append( "\n nbAseguramiento : " + nbAseguramiento );
		strBuilder.append( "\n nbSolicitudEspecifica : " + nbSolicitudEspecifica );
		strBuilder.append( "\n nuFolioArchivo : " + nuFolioArchivo );		
		return strBuilder.toString();
	}
	
}
