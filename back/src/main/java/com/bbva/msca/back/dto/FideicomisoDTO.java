package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class FideicomisoDTO {
	
	private String nuReferencia;
	private String nbFinalidad;
	private String nbLugConstitucion;
	private Date fhConstitucion;
	private String nbInsFiduciaria;
	private String nbPatFideicomitido;
	private String imAportaciones;
	
	
	BigDecimal nuFideicomiso;	
	private String cdCliente;
	private String nbCliente; 
	private String tpRelacion;  
	private String cdRFC;
	private String nbClienteDet; 
	private String nuFiscal;
	private String cdPais; 
	private String nuFea;  
	private String cdSexo;
	private Date fhNacimiento; 
	private String cdEntidad; 
	private String cdNacionalidad; 
	private String cdOcupacion;
	private String cdCurp; 
	private String cdRFCDet; 
	private String cdCorreo;
	private String cdTelefono; 
	private String nbCalle; 
	private String  nuExterior; 
	private String  nuInterior;
	private String  cdPostal; 
	private String nbColonia;  
	private String nbDelegacion;   
	private String  nbEntidad;  
	private String  cdPaisDet;  

	
	
	
	public BigDecimal getNuFideicomiso() {
		return nuFideicomiso;
	}
	public void setNuFideicomiso(BigDecimal nuFideicomiso) {
		this.nuFideicomiso = nuFideicomiso;
	}
	public String getCdCliente() {
		return cdCliente;
	}
	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getTpRelacion() {
		return tpRelacion;
	}
	public void setTpRelacion(String tpRelacion) {
		this.tpRelacion = tpRelacion;
	}
	public String getCdRFC() {
		return cdRFC;
	}
	public void setCdRFC(String cdRFC) {
		this.cdRFC = cdRFC;
	}
	public String getNbClienteDet() {
		return nbClienteDet;
	}
	public void setNbClienteDet(String nbClienteDet) {
		this.nbClienteDet = nbClienteDet;
	}
	public String getNuFiscal() {
		return nuFiscal;
	}
	public void setNuFiscal(String nuFiscal) {
		this.nuFiscal = nuFiscal;
	}
	public String getCdPais() {
		return cdPais;
	}
	public void setCdPais(String cdPais) {
		this.cdPais = cdPais;
	}
	public String getNuFea() {
		return nuFea;
	}
	public void setNuFea(String nuFea) {
		this.nuFea = nuFea;
	}
	public String getCdSexo() {
		return cdSexo;
	}
	public void setCdSexo(String cdSexo) {
		this.cdSexo = cdSexo;
	}
	public Date getFhNacimiento() {
		return fhNacimiento;
	}
	public void setFhNacimiento(Date fhNacimiento) {
		this.fhNacimiento = fhNacimiento;
	}
	public String getCdEntidad() {
		return cdEntidad;
	}
	public void setCdEntidad(String cdEntidad) {
		this.cdEntidad = cdEntidad;
	}
	public String getCdNacionalidad() {
		return cdNacionalidad;
	}
	public void setCdNacionalidad(String cdNacionalidad) {
		this.cdNacionalidad = cdNacionalidad;
	}
	public String getCdOcupacion() {
		return cdOcupacion;
	}
	public void setCdOcupacion(String cdOcupacion) {
		this.cdOcupacion = cdOcupacion;
	}
	public String getCdCurp() {
		return cdCurp;
	}
	public void setCdCurp(String cdCurp) {
		this.cdCurp = cdCurp;
	}
	public String getCdRFCDet() {
		return cdRFCDet;
	}
	public void setCdRFCDet(String cdRFCDet) {
		this.cdRFCDet = cdRFCDet;
	}
	public String getCdCorreo() {
		return cdCorreo;
	}
	public void setCdCorreo(String cdCorreo) {
		this.cdCorreo = cdCorreo;
	}
	public String getCdTelefono() {
		return cdTelefono;
	}
	public void setCdTelefono(String cdTelefono) {
		this.cdTelefono = cdTelefono;
	}
	public String getNbCalle() {
		return nbCalle;
	}
	public void setNbCalle(String nbCalle) {
		this.nbCalle = nbCalle;
	}
	public String getNuExterior() {
		return nuExterior;
	}
	public void setNuExterior(String nuExterior) {
		this.nuExterior = nuExterior;
	}
	public String getNuInterior() {
		return nuInterior;
	}
	public void setNuInterior(String nuInterior) {
		this.nuInterior = nuInterior;
	}
	public String getCdPostal() {
		return cdPostal;
	}
	public void setCdPostal(String cdPostal) {
		this.cdPostal = cdPostal;
	}
	public String getNbColonia() {
		return nbColonia;
	}
	public void setNbColonia(String nbColonia) {
		this.nbColonia = nbColonia;
	}
	public String getNbDelegacion() {
		return nbDelegacion;
	}
	public void setNbDelegacion(String nbDelegacion) {
		this.nbDelegacion = nbDelegacion;
	}
	public String getNbEntidad() {
		return nbEntidad;
	}
	public void setNbEntidad(String nbEntidad) {
		this.nbEntidad = nbEntidad;
	}
	public String getCdPaisDet() {
		return cdPaisDet;
	}
	public void setCdPaisDet(String cdPaisDet) {
		this.cdPaisDet = cdPaisDet;
	}
	public String getNuReferencia() {
		return nuReferencia;
	}
	public void setNuReferencia(String nuReferencia) {
		this.nuReferencia = nuReferencia;
	}
	public String getNbFinalidad() {
		return nbFinalidad;
	}
	public void setNbFinalidad(String nbFinalidad) {
		this.nbFinalidad = nbFinalidad;
	}
	public String getNbLugConstitucion() {
		return nbLugConstitucion;
	}
	public void setNbLugConstitucion(String nbLugConstitucion) {
		this.nbLugConstitucion = nbLugConstitucion;
	}
	public Date getFhConstitucion() {
		return fhConstitucion;
	}
	public void setFhConstitucion(Date fhConstitucion) {
		this.fhConstitucion = fhConstitucion;
	}
	public String getNbInsFiduciaria() {
		return nbInsFiduciaria;
	}
	public void setNbInsFiduciaria(String nbInsFiduciaria) {
		this.nbInsFiduciaria = nbInsFiduciaria;
	}
	public String getNbPatFideicomitido() {
		return nbPatFideicomitido;
	}
	public void setNbPatFideicomitido(String nbPatFideicomitido) {
		this.nbPatFideicomitido = nbPatFideicomitido;
	}
	public String getImAportaciones() {
		return imAportaciones;
	}
	public void setImAportaciones(String imAportaciones) {
		this.imAportaciones = imAportaciones;
	}

}
