package com.bbva.msca.back.util;

import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;

import com.bbva.jee.arq.spring.core.log.I18nLog;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;

public class MensajesI18n {

	private static final I18nLog LOGGER = I18nLogFactory.getLogI18n(MensajesI18n.class);

	private MensajesI18n() {
	}

	public static String getText(String llaveMensaje) {
		String msg = "No existe el id " + llaveMensaje;
		try {
			msg = ArqSpringContext.getPropiedad(llaveMensaje);
		} catch (PropiedadNoEncontradaExcepcion e1) {
			LOGGER.error(e1.getMessage(), e1);
		}
		
		return msg;
	}

}
