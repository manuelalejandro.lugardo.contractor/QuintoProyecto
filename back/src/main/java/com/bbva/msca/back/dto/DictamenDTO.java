package com.bbva.msca.back.dto;

import java.util.Date;

public class DictamenDTO {
	
	private boolean stCasoAsignado;
	private Integer nuCaso;
	private Integer nuFolio;
	private Integer nuCliente;
	private String nbCliente;
	private Integer nuCuenta;
	private String nbTipologia;
	private String nbSegmento;
	private String nbSesion;
	private String nbConsultor;
	private String nbDictamen;
	private double nuCalificacion;
	private Integer nuScore;
	private String nbRespuesta;
	private Date fhAsignacion;
	private Date fhEnvio;
	private Date fhLimiteReporteA;
	private Date fhLimiteReporteB;
	private Date fhVencimientoRevision;
	private Date fhRevision;
	private Date fhVencimientoLiberacion;
	private Date fhVencimientoReporte;
	
	

	public boolean getStCasoAsignado() {
		return stCasoAsignado;
	}
	public void setStCasoAsignado(boolean stCasoAsignado) {
		this.stCasoAsignado = stCasoAsignado;
	}
	public Integer getNuCaso() {
		return nuCaso;
	}
	public void setNuCaso(Integer nuCaso) {
		this.nuCaso = nuCaso;
	}
	public Integer getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(Integer nuFolio) {
		this.nuFolio = nuFolio;
	}
	public Integer getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(Integer nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public Integer getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(Integer nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public String getNbTipologia() {
		return nbTipologia;
	}
	public void setNbTipologia(String nbTipologia) {
		this.nbTipologia = nbTipologia;
	}
	public String getNbSegmento() {
		return nbSegmento;
	}
	public void setNbSegmento(String nbSegmento) {
		this.nbSegmento = nbSegmento;
	}
	public String getNbSesion() {
		return nbSesion;
	}
	public void setNbSesion(String nbSesion) {
		this.nbSesion = nbSesion;
	}
	public String getNbConsultor() {
		return nbConsultor;
	}
	public void setNbConsultor(String nbConsultor) {
		this.nbConsultor = nbConsultor;
	}
	public String getNbDictamen() {
		return nbDictamen;
	}
	public void setNbDictamen(String nbDictamen) {
		this.nbDictamen = nbDictamen;
	}
	public double getNuCalificacion() {
		return nuCalificacion;
	}
	public void setNuCalificacion(double nuCalificacion) {
		this.nuCalificacion = nuCalificacion;
	}
	public Integer getNuScore() {
		return nuScore;
	}
	public void setNuScore(Integer nuScore) {
		this.nuScore = nuScore;
	}
	public String getNbRespuesta() {
		return nbRespuesta;
	}
	public void setNbRespuesta(String nbRespuesta) {
		this.nbRespuesta = nbRespuesta;
	}
	public Date getFhAsignacion() {
		return fhAsignacion;
	}
	public void setFhAsignacion(Date fhAsignacion) {
		this.fhAsignacion = fhAsignacion;
	}
	public Date getFhEnvio() {
		return fhEnvio;
	}
	public void setFhEnvio(Date fhEnvio) {
		this.fhEnvio = fhEnvio;
	}
	public Date getFhLimiteReporteA() {
		return fhLimiteReporteA;
	}
	public void setFhLimiteReporteA(Date fhLimiteReporteA) {
		this.fhLimiteReporteA = fhLimiteReporteA;
	}
	public Date getFhLimiteReporteB() {
		return fhLimiteReporteB;
	}
	public void setFhLimiteReporteB(Date fhLimiteReporteB) {
		this.fhLimiteReporteB = fhLimiteReporteB;
	}
	public Date getFhVencimientoRevision() {
		return fhVencimientoRevision;
	}
	public void setFhVencimientoRevision(Date fhVencimientoRevision) {
		this.fhVencimientoRevision = fhVencimientoRevision;
	}
	public Date getFhRevision() {
		return fhRevision;
	}
	public void setFhRevision(Date fhRevision) {
		this.fhRevision = fhRevision;
	}
	public Date getFhVencimientoLiberacion() {
		return fhVencimientoLiberacion;
	}
	public void setFhVencimientoLiberacion(Date fhVencimientoLiberacion) {
		this.fhVencimientoLiberacion = fhVencimientoLiberacion;
	}
	public Date getFhVencimientoReporte() {
		return fhVencimientoReporte;
	}
	public void setFhVencimientoReporte(Date fhVencimientoReporte) {
		this.fhVencimientoReporte = fhVencimientoReporte;
	}
	
}
	
	