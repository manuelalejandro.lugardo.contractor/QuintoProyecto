package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class MovimientosClientesRiesgoDTO {
	
	private Date fhMovimiento;
	private Date fhValor;
	private String nbDescripcion;
	private String nbReferencia;
	private BigDecimal imImporte;
	private BigDecimal nuMovimiento;
	private Date fhMovimientoDel;
	private Date fhMovimientoAl;
	private BigDecimal imMovimientoDesde;
	private BigDecimal imMovimientoHasta;
	private String nuCuenta;
	
	
	
	public String getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public Date getFhMovimiento() {
		return fhMovimiento;
	}
	public void setFhMovimiento(Date fhMovimiento) {
		this.fhMovimiento = fhMovimiento;
	}
	public Date getFhValor() {
		return fhValor;
	}
	public void setFhValor(Date fhValor) {
		this.fhValor = fhValor;
	}
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	public String getNbReferencia() {
		return nbReferencia;
	}
	public void setNbReferencia(String nbReferencia) {
		this.nbReferencia = nbReferencia;
	}
	public BigDecimal getImImporte() {
		return imImporte;
	}
	public void setImImporte(BigDecimal imImporte) {
		this.imImporte = imImporte;
	}
	public BigDecimal getNuMovimiento() {
		return nuMovimiento;
	}
	public void setNuMovimiento(BigDecimal nuMovimiento) {
		this.nuMovimiento = nuMovimiento;
	}
	public Date getFhMovimientoDel() {
		return fhMovimientoDel;
	}
	public void setFhMovimientoDel(Date fhMovimientoDel) {
		this.fhMovimientoDel = fhMovimientoDel;
	}
	public Date getFhMovimientoAl() {
		return fhMovimientoAl;
	}
	public void setFhMovimientoAl(Date fhMovimientoAl) {
		this.fhMovimientoAl = fhMovimientoAl;
	}
	public BigDecimal getImMovimientoDesde() {
		return imMovimientoDesde;
	}
	public void setImMovimientoDesde(BigDecimal imMovimientoDesde) {
		this.imMovimientoDesde = imMovimientoDesde;
	}
	public BigDecimal getImMovimientoHasta() {
		return imMovimientoHasta;
	}
	public void setImMovimientoHasta(BigDecimal imMovimientoHasta) {
		this.imMovimientoHasta = imMovimientoHasta;
	}
	
	
	
}
