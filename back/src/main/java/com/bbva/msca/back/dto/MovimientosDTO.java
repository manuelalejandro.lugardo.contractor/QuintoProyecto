package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class MovimientosDTO {
	
	private BigDecimal nuNumero;
	private String nuCuenta;
	private String nbOficina;
	private String nbConcepto;
	private String nbTipoMovimiento;
	private Date fhMovimiento;
	private BigDecimal imMonto;
	private String cdDivisa;
	private BigDecimal imEfectivo;
	private String nbDescripcion;
	
	public BigDecimal getNuNumero() {
		return nuNumero;
	}
	public void setNuNumero(BigDecimal nuNumero) {
		this.nuNumero = nuNumero;
	}
	public String getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public String getNbOficina() {
		return nbOficina;
	}
	public void setNbOficina(String nbOficina) {
		this.nbOficina = nbOficina;
	}
	public String getNbConcepto() {
		return nbConcepto;
	}
	public void setNbConcepto(String nbConcepto) {
		this.nbConcepto = nbConcepto;
	}
	public String getNbTipoMovimiento() {
		return nbTipoMovimiento;
	}
	public void setNbTipoMovimiento(String nbTipoMovimiento) {
		this.nbTipoMovimiento = nbTipoMovimiento;
	}
	public Date getFhMovimiento() {
		return fhMovimiento;
	}
	public void setFhMovimiento(Date fhMovimiento) {
		this.fhMovimiento = fhMovimiento;
	}
	public BigDecimal getImMonto() {
		return imMonto;
	}
	public void setImMonto(BigDecimal imMonto) {
		this.imMonto = imMonto;
	}
	public String getCdDivisa() {
		return cdDivisa;
	}
	public void setCdDivisa(String cdDivisa) {
		this.cdDivisa = cdDivisa;
	}
	public BigDecimal getImEfectivo() {
		return imEfectivo;
	}
	public void setImEfectivo(BigDecimal imEfectivo) {
		this.imEfectivo = imEfectivo;
	}
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	
	

}
