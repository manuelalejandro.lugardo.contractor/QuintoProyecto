package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class InfoAccionistasyEmpresaDTO {
	
	private String nbCliente;
	private String nbParticipacion;
	private String nbEmpresa;
	private BigDecimal porParticipacion;
	private String cdAccionista;
	private String nuFolio;
	private String nbDecision;
	
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNbParticipacion() {
		return nbParticipacion;
	}
	public void setNbParticipacion(String nbParticipacion) {
		this.nbParticipacion = nbParticipacion;
	}
	public String getNbEmpresa() {
		return nbEmpresa;
	}
	public void setNbEmpresa(String nbEmpresa) {
		this.nbEmpresa = nbEmpresa;
	}
	public BigDecimal getPorParticipacion() {
		return porParticipacion;
	}
	public void setPorParticipacion(BigDecimal porParticipacion) {
		this.porParticipacion = porParticipacion;
	}
	public String getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(String nuFolio) {
		this.nuFolio = nuFolio;
	}
	public String getNbDecision() {
		return nbDecision;
	}
	public void setNbDecision(String nbDecision) {
		this.nbDecision = nbDecision;
	}
	public String getCdAccionista() {
		return cdAccionista;
	}
	public void setCdAccionista(String cdAccionista) {
		this.cdAccionista = cdAccionista;
	}
	
	

}
