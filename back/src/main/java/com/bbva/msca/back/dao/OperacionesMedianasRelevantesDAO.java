package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import com.bbva.msca.back.dto.DetalleOperacionesMedianasDTO;
import com.bbva.msca.back.dto.OperacionesMedianasRelevantesDTO;
import com.bbva.msca.back.dto.OperacionesRelevantesDTO;
import com.bbva.msca.back.dto.OperacionesMedianasDTO;

public class OperacionesMedianasRelevantesDAO {
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	@SuppressWarnings("unchecked")
	public OperacionesMedianasRelevantesDTO getOperacionesMedianasRelevantes(BigDecimal cdCaso, String cdSistema, String folioAlerta)throws Exception {

		OperacionesMedianasRelevantesDTO operacionesMedianasRelevantesDTO = new OperacionesMedianasRelevantesDTO();
		List<OperacionesRelevantesDTO> lstOperacionesRelevantes = new ArrayList<OperacionesRelevantesDTO>();
		List<OperacionesMedianasDTO> lstOperacionesMedianas = new ArrayList<OperacionesMedianasDTO>();

		Query query = em.createNativeQuery(Consultas.getConsulta("getOperacionesMedianas"));

		query.setParameter(1, cdCaso);
		query.setParameter(2, folioAlerta);
		query.setParameter(3, cdSistema);

		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("nbOperacionesOMED")
		.addScalar("nuCuentaOMED")
		.addScalar("nuOperacionesOMED")
		.addScalar("imImporteOMED")
		.addScalar("nbMonedaOMED", StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(OperacionesMedianasDTO.class));

		lstOperacionesMedianas = (List<OperacionesMedianasDTO>) query.getResultList();
		operacionesMedianasRelevantesDTO.setTablaMedianas(lstOperacionesMedianas);

		Query queryREL = em.createNativeQuery(Consultas.getConsulta("getOperacionesRelevantes"));

		queryREL.setParameter(1, cdCaso);
		queryREL.setParameter(2, folioAlerta);
		queryREL.setParameter(3, cdSistema);

		queryREL.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("nuCuentaOPREL")
		.addScalar("tpOperacionOPREL")
		.addScalar("nuOperacionesOPREL")
		.addScalar("importeOPREL")
		.addScalar("monedaOPREL", StringType.INSTANCE)
		.setResultTransformer(Transformers.aliasToBean(OperacionesRelevantesDTO.class));

		lstOperacionesRelevantes = (List<OperacionesRelevantesDTO>) queryREL.getResultList();
		operacionesMedianasRelevantesDTO.setTablaRelevantes(lstOperacionesRelevantes);
		
		return operacionesMedianasRelevantesDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<DetalleOperacionesMedianasDTO> getConsultaDetalleOperacionesMedianas(BigDecimal cdCaso, String cdSistema, String nuFolioAlerta, Integer limit, Integer offset,String search, String name, String order) throws Exception{
		
		List<DetalleOperacionesMedianasDTO> detalleOperacionesMedianas = null;
		
		String consulta = " SELECT * FROM ( "+Consultas.getConsulta("getConsultaDetalleOperacionesMedianas")+" ) ";

		consulta = consulta +" "+ " order by "
					             + (name != null && !"".equals(name) ? " " + name + " ": " fhOperacion ")
					             + " "
					             + (order != null && !"".equals(order) ? " " + order + " "
					             : " asc ");
		
		Query query = em.createNativeQuery(consulta);
		query.setParameter(1, cdCaso);
		query.setParameter(2, nuFolioAlerta);
		query.setParameter(3, cdSistema);
		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("fhOperacion",TimestampType.INSTANCE)
		.addScalar("nbMoneda")
		.addScalar("nbInstrumento")
		.addScalar("nbTipoTransaccion")
		.addScalar("nuCuentas")
		.addScalar("nbReferencia")
		.addScalar("imOperacion")
		.addScalar("nbOficina")
		.setResultTransformer(Transformers.aliasToBean(DetalleOperacionesMedianasDTO.class));
		
		query.setFirstResult(offset);
		query.setMaxResults(limit);
		
		detalleOperacionesMedianas = (List<DetalleOperacionesMedianasDTO>)query.getResultList();
		
		return detalleOperacionesMedianas;
	}
	
	public Integer getNuDetalleOperacionesMedianas(BigDecimal cdCaso, String cdSistema, String nuFolioAlerta) throws Exception{
		 
		 String consulta = " SELECT COUNT(*) FROM ( "+Consultas.getConsulta("getConsultaDetalleOperacionesMedianas")+" ) ";
	
		 Query query = em.createNativeQuery(consulta);
		 	query.setParameter(1, cdCaso);
			query.setParameter(2, nuFolioAlerta);
			query.setParameter(3, cdSistema);
		 
		 Integer conteo = ((Number) query.getSingleResult()).intValue();
	 
		return conteo;
	}
	
	@SuppressWarnings("unchecked")
	public List<DetalleOperacionesMedianasDTO> getConsultaDetalleOperacionesRelevantes(BigDecimal cdCaso, String cdSistema, String nuFolioAlerta, Integer limit, Integer offset,String search, String name, String order) throws Exception{
		
		List<DetalleOperacionesMedianasDTO> detalleOperacionesRelevantes = null;
		
		String consulta = " SELECT * FROM ( "+Consultas.getConsulta("getConsultaDetalleOperacionesRelevantes")+" ) ";

		consulta = consulta +" "+ " order by "
					             + (name != null && !"".equals(name) ? " " + name + " ": " fhOperacion ")
					             + " "
					             + (order != null && !"".equals(order) ? " " + order + " "
					             : " asc ");
		
		Query query = em.createNativeQuery(consulta);
		query.setParameter(1, cdCaso);
		query.setParameter(2, nuFolioAlerta);
		query.setParameter(3, cdSistema);
		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("fhOperacion",TimestampType.INSTANCE)
		.addScalar("nbMoneda")
		.addScalar("nbInstrumento")
		.addScalar("nbTipoTransaccion")
		.addScalar("nuCuentas")
		.addScalar("nbReferencia")
		.addScalar("imOperacion")
		.addScalar("nbOficina")
		.setResultTransformer(Transformers.aliasToBean(DetalleOperacionesMedianasDTO.class));
		
		query.setFirstResult(offset);
		query.setMaxResults(limit);
		
		detalleOperacionesRelevantes = (List<DetalleOperacionesMedianasDTO>)query.getResultList();
		
		return detalleOperacionesRelevantes;
	}
	
	public Integer getNuDetalleOperacionesRelevantes(BigDecimal cdCaso, String cdSistema, String nuFolioAlerta) throws Exception{
		 
		 String consulta = " SELECT COUNT(*) FROM ( "+Consultas.getConsulta("getConsultaDetalleOperacionesRelevantes")+" ) ";
	
		 Query query = em.createNativeQuery(consulta);
		 	query.setParameter(1, cdCaso);
			query.setParameter(2, nuFolioAlerta);
			query.setParameter(3, cdSistema);
			
		 Integer conteo = ((Number) query.getSingleResult()).intValue();
	 
		return conteo;
	}
}
