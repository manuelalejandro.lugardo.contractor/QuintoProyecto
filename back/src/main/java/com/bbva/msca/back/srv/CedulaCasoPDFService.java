package com.bbva.msca.back.srv;

import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/CedulaCasoPDFService")
public interface CedulaCasoPDFService {
	
	@GET
	@Path("/generaReporteCedulaCaso/{cdCaso}/{nuFolioAlerta}/{cdSistema}/{cdCliente}/{nuCuenta}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces( "application/pdf" )
	public Response cedulaCasoPDF(
			
			
			@PathParam("cdCaso") BigDecimal cdCaso,
			@PathParam("nuFolioAlerta") String nuFolioAlerta,
			@PathParam("cdSistema") String cdSistema,
			@PathParam("cdCliente") String cdCliente,
			@PathParam("nuCuenta") String nuCuenta
	) throws Exception;
	

}
