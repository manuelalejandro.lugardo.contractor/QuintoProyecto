package com.bbva.msca.back.srv;

import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.ParametrosBusquedaCasoDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/ConsultaCasosService")

public interface ConsultaCasosService {
	
	@POST
	@Path("/consultarCasosSupervisorFiltro/{cdSupervisor}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCasosSupervisorFiltro(@PathParam("cdSupervisor") BigDecimal cdSupervisor, ParametrosBusquedaCasoDTO parametrosBusquedaCasoDTO);
	
	@GET
	@Path("/consultarCasosSupervisor/{cdSupervisor}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCasosSupervisor(@PathParam("cdSupervisor") BigDecimal cdSupervisor);
	
	@GET
	@Path("/consultarCasosConsultor/{cdConsultor}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCasosConsultor(@PathParam("cdConsultor") BigDecimal cdConsultor);
	
	@POST
	@Path("/consultarCasosConsultorFiltro/{cdConsultor}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCasosConsultorFiltro(@PathParam("cdConsultor") BigDecimal cdConsultor, ParametrosBusquedaCasoDTO parametrosBusquedaCasoDTO);

}
