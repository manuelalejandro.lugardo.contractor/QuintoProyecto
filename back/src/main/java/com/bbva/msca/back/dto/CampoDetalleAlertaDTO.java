package com.bbva.msca.back.dto;

public class CampoDetalleAlertaDTO {

	private String idCampo;
	
	private Object nbCampo;
	
	private String lblCampo;
	
	private Integer posicion;
	
	private String formatter;

	public void setIdCampo(String idCampo) {
		this.idCampo = idCampo;
	}

	public String getIdCampo() {
		return idCampo;
	}

	public void setNbCampo(Object nbCampo) {
		this.nbCampo = nbCampo;
	}

	public Object getNbCampo() {
		return nbCampo;
	}

	public void setPosicion(Integer posicion) {
		this.posicion = posicion;
	}

	public Integer getPosicion() {
		return posicion;
	}

	public void setLblCampo(String lblCampo) {
		this.lblCampo = lblCampo;
	}

	public String getLblCampo() {
		return lblCampo;
	}

	public void setFormatter(String formatter) {
		this.formatter = formatter;
	}

	public String getFormatter() {
		return formatter;
	}

}
