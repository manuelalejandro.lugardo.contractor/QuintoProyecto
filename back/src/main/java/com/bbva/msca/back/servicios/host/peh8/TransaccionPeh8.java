package com.bbva.msca.back.servicios.host.peh8;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.auditoria.Auditable;
import com.bbva.jee.arq.spring.core.auditoria.InformacionAuditable;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.googlecode.ehcache.annotations.Cacheable;
import com.googlecode.ehcache.annotations.TriggersRemove;

/**
 * Invocador de la transacci&oacute;n <code>PEH8</code>
 * 
 * @see PeticionTransaccionPeh8
 * @see RespuestaTransaccionPeh8
 *  
 * @author Arquitectura Spring BBVA
 */
@Component
public class TransaccionPeh8 implements InvocadorTransaccion<PeticionTransaccionPeh8,RespuestaTransaccionPeh8> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	@Auditable({InformacionAuditable.ARGUMENTOS, InformacionAuditable.RESULTADO, InformacionAuditable.DURACION})
	public RespuestaTransaccionPeh8 invocar(PeticionTransaccionPeh8 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeh8.class, RespuestaTransaccionPeh8.class, transaccion);
	}
	
	@Override
	@Auditable({InformacionAuditable.ARGUMENTOS, InformacionAuditable.RESULTADO, InformacionAuditable.DURACION})
	@Cacheable(cacheName = "cacheTransaccionPeh8")
	public RespuestaTransaccionPeh8 invocarCache(PeticionTransaccionPeh8 transaccion) throws ExcepcionTransaccion {
		return servicioTransacciones.invocar(PeticionTransaccionPeh8.class, RespuestaTransaccionPeh8.class, transaccion);
	}
	
	@Override
	@Auditable(InformacionAuditable.DURACION)
	@TriggersRemove(cacheName = "cacheTransaccionPeh8", removeAll = true)
	public void vaciarCache() {}
		
}