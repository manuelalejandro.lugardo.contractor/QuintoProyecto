package com.bbva.msca.back.dto;

import java.util.Date;

public class LocalizacionClienteDTO {
	
	private String cdTipoBusqueda;
	private String nuCliente; // OK
	private String tpIdentificacion;
	private String numIdentificacion;
	private String cdRFC;
	private String homonimia;
	private String nbNombre; // OK
	private String nbPaterno; // OK
	private String nbMaterno; // OK
	private String nbRazonSocial; // OK
	private String numeroCuenta;
	private String numeroTarjeta;
	private String numeroConsecutivo;
	private String marcaPVD;
	private int stClienteLoc;	
		
	private Date fhNacimiento; // OK
	private String cdTpPersona; // OK
	private String domicilio;
	private String sector;
	private String cdClienteBancomer;
	

	public String getCdTipoBusqueda() {
		return cdTipoBusqueda;
	}


	public void setCdTipoBusqueda(String cdTipoBusqueda) {
		this.cdTipoBusqueda = cdTipoBusqueda;
	}


	public String getNuCliente() {
		return nuCliente;
	}


	public void setNuCliente(String nuCliente) {
		this.nuCliente = nuCliente;
	}


	public String getTpIdentificacion() {
		return tpIdentificacion;
	}


	public void setTpIdentificacion(String tpIdentificacion) {
		this.tpIdentificacion = tpIdentificacion;
	}


	public String getNumIdentificacion() {
		return numIdentificacion;
	}


	public void setNumIdentificacion(String numIdentificacion) {
		this.numIdentificacion = numIdentificacion;
	}


	public String getCdRFC() {
		return cdRFC;
	}


	public void setCdRFC(String cdRFC) {
		this.cdRFC = cdRFC;
	}


	public String getHomonimia() {
		return homonimia;
	}


	public void setHomonimia(String homonimia) {
		this.homonimia = homonimia;
	}


	public String getNbNombre() {
		return nbNombre;
	}


	public void setNbNombre(String nbNombre) {
		this.nbNombre = nbNombre;
	}


	public String getNbPaterno() {
		return nbPaterno;
	}


	public void setNbPaterno(String nbPaterno) {
		this.nbPaterno = nbPaterno;
	}


	public String getNbMaterno() {
		return nbMaterno;
	}


	public void setNbMaterno(String nbMaterno) {
		this.nbMaterno = nbMaterno;
	}


	public String getNbRazonSocial() {
		return nbRazonSocial;
	}


	public void setNbRazonSocial(String nbRazonSocial) {
		this.nbRazonSocial = nbRazonSocial;
	}


	public String getNumeroCuenta() {
		return numeroCuenta;
	}


	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}


	public String getNumeroTarjeta() {
		return numeroTarjeta;
	}


	public void setNumeroTarjeta(String numeroTarjeta) {
		this.numeroTarjeta = numeroTarjeta;
	}


	public String getNumeroConsecutivo() {
		return numeroConsecutivo;
	}


	public void setNumeroConsecutivo(String numeroConsecutivo) {
		this.numeroConsecutivo = numeroConsecutivo;
	}


	public String getMarcaPVD() {
		return marcaPVD;
	}


	public void setMarcaPVD(String marcaPVD) {
		this.marcaPVD = marcaPVD;
	}


	public int getStClienteLoc() {
		return stClienteLoc;
	}


	public void setStClienteLoc(int stClienteLoc) {
		this.stClienteLoc = stClienteLoc;
	}


	public Date getFhNacimiento() {
		return fhNacimiento;
	}


	public void setFhNacimiento(Date fhNacimiento) {
		this.fhNacimiento = fhNacimiento;
	}


	public String getCdTpPersona() {
		return cdTpPersona;
	}


	public void setCdTpPersona(String cdTpPersona) {
		this.cdTpPersona = cdTpPersona;
	}


	public String getDomicilio() {
		return domicilio;
	}


	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}


	public String getSector() {
		return sector;
	}


	public void setSector(String sector) {
		this.sector = sector;
	}


	public String getCdClienteBancomer() {
		return cdClienteBancomer;
	}


	public void setCdClienteBancomer(String cdClienteBancomer) {
		this.cdClienteBancomer = cdClienteBancomer;
	}


	public String toString()
	{
		StringBuilder str = new StringBuilder();
		str.append( "\n cdTipoBusqueda : "+ cdTipoBusqueda );
		str.append( "\n nuCliente : " + nuCliente );
		str.append( "\n tpIdentificacion : " + tpIdentificacion );
		str.append( "\n numIdentificacion : " + numIdentificacion );
		str.append( "\n cdRFC : " + cdRFC );
		str.append( "\n homonimia : " + homonimia );
		str.append( "\n nbNombre : " + nbNombre );
		str.append( "\n nbPaterno : " + nbPaterno );
		str.append( "\n nbMaterno : " + nbMaterno );
		str.append( "\n nbRazonSocial : " + nbRazonSocial );
		str.append( "\n numeroCuenta : " + numeroCuenta );
		str.append( "\n numeroTarjeta : " + numeroTarjeta );
		str.append( "\n numeroConsecutivo : " + numeroConsecutivo );
		str.append( "\n marcaPVD : " + marcaPVD );
		str.append( "\n stClienteLoc : " + stClienteLoc );
		str.append( "\n fhNacimiento : " + fhNacimiento );
		str.append( "\n cdTpPersona : " + cdTpPersona );
		str.append( "\n domicilio : " + domicilio );
		str.append( "\n sector : " + sector );
		str.append( "\n cdClienteBancomer : " + cdClienteBancomer );
		return str.toString();
	}

}
