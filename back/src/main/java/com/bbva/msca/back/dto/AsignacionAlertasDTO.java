package com.bbva.msca.back.dto;

public class AsignacionAlertasDTO {
	
	private boolean stSelected;
	private Integer cdConsultor;
	private String nbConsultor;
	private Integer nuAlertasNormal;
	private Integer nuAlertasUrgente;
	private Integer nuAlertasAnalisis;
	private Integer nuAlertasSupervision;
	
	
	public boolean isStSelected() {
		return stSelected;
	}
	public void setStSelected(boolean stSelected) {
		this.stSelected = stSelected;
	}
	public Integer getCdConsultor() {
		return cdConsultor;
	}
	public void setCdConsultor(Integer cdConsultor) {
		this.cdConsultor = cdConsultor;
	}
	public String getNbConsultor() {
		return nbConsultor;
	}
	public void setNbConsultor(String nbConsultor) {
		this.nbConsultor = nbConsultor;
	}
	public Integer getNuAlertasNormal() {
		return nuAlertasNormal;
	}
	public void setNuAlertasNormal(Integer nuAlertasNormal) {
		this.nuAlertasNormal = nuAlertasNormal;
	}
	public Integer getNuAlertasUrgente() {
		return nuAlertasUrgente;
	}
	public void setNuAlertasUrgente(Integer nuAlertasUrgente) {
		this.nuAlertasUrgente = nuAlertasUrgente;
	}
	public Integer getNuAlertasAnalisis() {
		return nuAlertasAnalisis;
	}
	public void setNuAlertasAnalisis(Integer nuAlertasAnalisis) {
		this.nuAlertasAnalisis = nuAlertasAnalisis;
	}
	public Integer getNuAlertasSupervision() {
		return nuAlertasSupervision;
	}
	public void setNuAlertasSupervision(Integer nuAlertasSupervision) {
		this.nuAlertasSupervision = nuAlertasSupervision;
	}

}
