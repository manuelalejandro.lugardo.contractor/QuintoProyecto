package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bbva.msca.back.dto.PersonaReportadaDTO;

public class PersonaReportadaDAO {
	
	public List<PersonaReportadaDTO> getPersonasReportadas(){
		List<PersonaReportadaDTO> lstPersonaReportadaDTO = new ArrayList<PersonaReportadaDTO>();
		PersonaReportadaDTO personaReportadaDTO = null;
		
		for(int i=0;i<10;i++){
			personaReportadaDTO = new PersonaReportadaDTO();
			
			personaReportadaDTO.setNuID(121 + i);
			personaReportadaDTO.setNbCaracter("Investigado");
			personaReportadaDTO.setNbTipoPersona("Fiscal");
			personaReportadaDTO.setNbCliente("Ramon");
			personaReportadaDTO.setNbApellidoP("Sanchez");
			personaReportadaDTO.setNbApellidoM("Romero");
			personaReportadaDTO.setFhNacimiento(new Date());
			personaReportadaDTO.setNuRFC("SAPR950823HDFR07");
			personaReportadaDTO.setNbDomicilio("Mexico DF");
			personaReportadaDTO.setNbComplementarios(" Fecha de Construccion: 23 de Mayo");
			personaReportadaDTO.setNbCBancomer("NO");
			personaReportadaDTO.setNuCliente(19887);
			personaReportadaDTO.setNuCuenta(2337);
			personaReportadaDTO.setNbTipologia("R8");
			personaReportadaDTO.setNbDictamen("LI");

			lstPersonaReportadaDTO.add(personaReportadaDTO);
			
		}

		return lstPersonaReportadaDTO;
		
	}

}
