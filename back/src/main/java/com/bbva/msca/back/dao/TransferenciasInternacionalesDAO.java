package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import com.bbva.msca.back.dto.TranferenciasInternacionalesDTO;
import com.bbva.msca.back.dto.TranferenciasInternacionalesRecibidasDTO;
import com.bbva.msca.back.dto.TransferenciasInternacionalesTablaDTO;
import com.bbva.msca.back.dto.TransferenciasInternacionalesTablaDetalleDTO;

public class TransferenciasInternacionalesDAO {

	private final String TP_OP_ENTRADA = "E";
	private final int TP_OP_SALIDA = 'S';
	private final String NO_RANGO_MESES = "-31";

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	@SuppressWarnings("unchecked")
	public TranferenciasInternacionalesDTO getTransferenciasInternacionales(
			String cdSistema, String folioAlerta) throws Exception {
		TranferenciasInternacionalesDTO tranferenciasInternacionalesDTO = new TranferenciasInternacionalesDTO();
		List<TranferenciasInternacionalesDTO> lstTranferenciasInternacionalesDTO = null;

		Query query = em.createNativeQuery(Consultas
				.getConsulta("getOperacionesInternacionalesEnviadas"));

		query.setParameter(1, cdSistema);
		query.setParameter(2, folioAlerta);

		query.unwrap(org.hibernate.SQLQuery.class)
				// transacciones enviadas
				.addScalar("paisDestinoOIE")
				.addScalar("riesgoOIE", StringType.INSTANCE)
				.addScalar("paraisoFiscalOIE", StringType.INSTANCE)
				.addScalar("montoTrans13OIE")
				.addScalar("nuOperaciones13OIE")
				.addScalar("montoTrans14OIE")
				.addScalar("nuOperaciones14OIE")
				.addScalar("montoTrans15OIE")
				.addScalar("nuOperaciones15OIE")
				.addScalar("montoTransTotOIE")
				.addScalar("nuOperacionesTotOIE")

				.setResultTransformer(
						Transformers
								.aliasToBean(TranferenciasInternacionalesDTO.class));

		lstTranferenciasInternacionalesDTO = (List<TranferenciasInternacionalesDTO>) query
				.getResultList();
		// tranferenciasInternacionalesDTO.setTablaEnviadas(lstTranferenciasInternacionalesDTO);

		Query queryOIR = em.createNativeQuery(Consultas
				.getConsulta("getOperacionesInternacionalesRecibidas"));

		queryOIR.setParameter(1, cdSistema);
		queryOIR.setParameter(2, folioAlerta);

		queryOIR.unwrap(org.hibernate.SQLQuery.class)

				.addScalar("paisOrigenOIR")
				.addScalar("riesgoOIR", StringType.INSTANCE)
				.addScalar("paraisoFiscalOIR", StringType.INSTANCE)
				.addScalar("montoTrans13OIR")
				.addScalar("nuOperaciones13OIR")
				.addScalar("montoTrans14OIR")
				.addScalar("nuOperaciones14OIR")
				.addScalar("montoTrans15OIR")
				.addScalar("nuOperaciones15OIR")
				.addScalar("montoTransTotOIR")
				.addScalar("nuOperacionesTotOIR")

				.setResultTransformer(
						Transformers
								.aliasToBean(TranferenciasInternacionalesRecibidasDTO.class));

		// tranferenciasInternacionalesDTO.setTablaRecibidas((List<TranferenciasInternacionalesRecibidasDTO>)queryOIR.getResultList());

		return tranferenciasInternacionalesDTO;
	}

	@SuppressWarnings("unchecked")
	public List<TransferenciasInternacionalesTablaDTO> getTransferenciasInternacionalesDet(
			String cdSistema, String folioAlerta, String numeroCuenta,
			String tipoOperacion, Integer anioActual) throws Exception {

		List<TransferenciasInternacionalesTablaDTO> lstResultado = new ArrayList<TransferenciasInternacionalesTablaDTO>();
		List<TranferenciasInternacionalesDTO> lstTransferencias = new ArrayList<TranferenciasInternacionalesDTO>();

		Query query = em.createNativeQuery(Consultas
				.getConsulta("getOperacionesInternacionalesEnviadasDet"));
		query.setParameter(1, tipoOperacion);
		query.setParameter(2, cdSistema);
		query.setParameter(3, folioAlerta);

		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("numCuenta")
				.addScalar("tpTransaccion", StringType.INSTANCE)
				.addScalar("cdPaisDestino")
				.addScalar("cdParaisoFiscal")
				.addScalar("nivelRiesgo")
				.addScalar("numOperaciones")
				.addScalar("montoTransacciones")
				.addScalar("anioProceso")
				.setResultTransformer(
						Transformers
								.aliasToBean(TranferenciasInternacionalesDTO.class));

		lstTransferencias = (List<TranferenciasInternacionalesDTO>) query
				.getResultList();
		List<String> lstPaises = new ArrayList<String>();
		// Parsear Resultado
		// Agrupar por paises
		for (TranferenciasInternacionalesDTO transfer : lstTransferencias) {
			lstPaises.add(transfer.getCdPaisDestino());
		}

		// Elimina elementos duplicados
		HashSet<String> hsPaises = new HashSet<String>();
		hsPaises.addAll(lstPaises);

		lstPaises.clear();
		lstPaises.addAll(hsPaises);

		for (String nombrePais : lstPaises) {
			System.out.println("Nombre Pais : " + nombrePais);
		}
		TransferenciasInternacionalesTablaDTO resumenPais = null;
		// Establece valores principales
		for (int indiceA = 0; indiceA < lstPaises.size(); indiceA++) {
			resumenPais = new TransferenciasInternacionalesTablaDTO();
			for (int indiceB = 0; indiceB < lstTransferencias.size(); indiceB++) {
				if (lstTransferencias.get(indiceB).getCdPaisDestino()
						.equals(lstPaises.get(indiceA))) {
					resumenPais.setRiesgo(lstTransferencias.get(indiceB)
							.getNivelRiesgo());
					resumenPais.setParaisoFiscal(lstTransferencias.get(indiceB)
							.getCdParaisoFiscal());
					resumenPais.setPaisDestino(lstTransferencias.get(indiceB)
							.getCdPaisDestino());
					lstResultado.add(resumenPais);
					break;
				}
			}
		}

		// Establece Montos y Total de Operaciones
		for (int indiceA = 0; indiceA < lstPaises.size(); indiceA++) {
			resumenPais = lstResultado.get(indiceA);
			for (int indiceB = 0; indiceB < lstTransferencias.size(); indiceB++) {
				if (lstTransferencias.get(indiceB).getCdPaisDestino()
						.equals(lstPaises.get(indiceA))
						&& lstTransferencias.get(indiceB).getAnioProceso()
								.equals(anioActual + "")) {
					resumenPais.setMontoAnioTres(lstTransferencias.get(indiceB)
							.getMontoTransacciones());
					resumenPais.setNumOperAnioTres(lstTransferencias.get(
							indiceB).getNumOperaciones());
				}
				if (lstTransferencias.get(indiceB).getCdPaisDestino()
						.equals(lstPaises.get(indiceA))
						&& lstTransferencias.get(indiceB).getAnioProceso()
								.equals((anioActual - 1) + "")) {
					resumenPais.setMontoAnioDos(lstTransferencias.get(indiceB)
							.getMontoTransacciones());
					resumenPais.setNumOperAnioDos(lstTransferencias
							.get(indiceB).getNumOperaciones());
				}
				if (lstTransferencias.get(indiceB).getCdPaisDestino()
						.equals(lstPaises.get(indiceA))
						&& lstTransferencias.get(indiceB).getAnioProceso()
								.equals((anioActual - 2) + "")) {
					resumenPais.setMontoAnioUno(lstTransferencias.get(indiceB)
							.getMontoTransacciones());
					resumenPais.setNumOperAnioUno(lstTransferencias
							.get(indiceB).getNumOperaciones());
				}
			}
		}

		for (TransferenciasInternacionalesTablaDTO tblVal : lstResultado) {
			System.out.println(" == Valor Registro Tabla == ");
			if (tblVal.getMontoAnioUno() == null) {
				tblVal.setMontoAnioUno(new BigDecimal(0));
			}
			if (tblVal.getNumOperAnioUno() == null) {
				tblVal.setNumOperAnioUno(new BigDecimal(0));
			}
			if (tblVal.getMontoAnioDos() == null) {
				tblVal.setMontoAnioDos(new BigDecimal(0));
			}
			if (tblVal.getNumOperAnioDos() == null) {
				tblVal.setNumOperAnioDos(new BigDecimal(0));
			}
			if (tblVal.getMontoAnioTres() == null) {
				tblVal.setMontoAnioTres(new BigDecimal(0));
			}
			if (tblVal.getNumOperAnioTres() == null) {
				tblVal.setNumOperAnioTres(new BigDecimal(0));
			}

			tblVal.setMontoAnios(tblVal.getMontoAnioUno()
					.add(tblVal.getMontoAnioDos())
					.add(tblVal.getMontoAnioTres()));
			tblVal.setTotalAnios(tblVal.getNumOperAnioUno()
					.add(tblVal.getNumOperAnioDos())
					.add(tblVal.getNumOperAnioTres()));
			System.out.println(tblVal.toString());
		}

		return lstResultado;
	}

	@SuppressWarnings("unchecked")
	public List<TransferenciasInternacionalesTablaDTO> getDetalleTransferenciasInternacionalesDet(
			String cdSistema, String folioAlerta, String numeroCuenta,
			String tipoOperacion, Integer anioActual) throws Exception {

		List<TransferenciasInternacionalesTablaDTO> lstResultados = new ArrayList<TransferenciasInternacionalesTablaDTO>();
		List<TranferenciasInternacionalesDTO> lstTransferencia = new ArrayList<TranferenciasInternacionalesDTO>();

		Query query = em.createNativeQuery(Consultas
				.getConsulta("getDetalleOperacionesInternacionales"));
		query.setParameter(1, tipoOperacion);
		query.setParameter(2, cdSistema);
		query.setParameter(3, folioAlerta);

		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("numCuentaD")
				.addScalar("tpTransaccionD", StringType.INSTANCE)
				.addScalar("cdPaisDestinoD")
				.addScalar("cdParaisoFiscalD")
				.addScalar("nivelRiesgoD")
				.addScalar("numOperacionesD")
				.addScalar("montoTransaccionesD")
				.addScalar("anioProcesoD")
				.setResultTransformer(
						Transformers
								.aliasToBean(TranferenciasInternacionalesDTO.class));

		lstTransferencia = (List<TranferenciasInternacionalesDTO>) query
				.getResultList();
		List<String> lstPaises = new ArrayList<String>();
		// Parsear Resultado
		// Agrupar por paises
		for (TranferenciasInternacionalesDTO transfer : lstTransferencia) {
			lstPaises.add(transfer.getCdPaisDestinoD());
		}

		// Elimina elementos duplicados
		HashSet<String> hsPaises = new HashSet<String>();
		hsPaises.addAll(lstPaises);

		lstPaises.clear();
		lstPaises.addAll(hsPaises);

		for (String nombrePais : lstPaises) {
			System.out.println("Nombre Pais : " + nombrePais);
		}
		TransferenciasInternacionalesTablaDTO resumenPais = null;
		// Establece valores principales
		for (int indiceA = 0; indiceA < lstPaises.size(); indiceA++) {
			resumenPais = new TransferenciasInternacionalesTablaDTO();
			for (int indiceB = 0; indiceB < lstTransferencia.size(); indiceB++) {
				if (lstTransferencia.get(indiceB).getCdPaisDestinoD()
						.equals(lstPaises.get(indiceA))) {
					resumenPais.setRiesgoD(lstTransferencia.get(indiceB)
							.getNivelRiesgoD());
					resumenPais.setParaisoFiscalD(lstTransferencia.get(indiceB)
							.getCdParaisoFiscalD());
					resumenPais.setPaisDestinoD(lstTransferencia.get(indiceB)
							.getCdPaisDestinoD());
					lstResultados.add(resumenPais);
					break;
				}
			}
		}

		// Establece Montos y Total de Operaciones
		for (int indiceA = 0; indiceA < lstPaises.size(); indiceA++) {
			resumenPais = lstResultados.get(indiceA);
			for (int indiceB = 0; indiceB < lstTransferencia.size(); indiceB++) {
				if (lstTransferencia.get(indiceB).getCdPaisDestinoD()
						.equals(lstPaises.get(indiceA))
						&& lstTransferencia.get(indiceB).getAnioProcesoD()
								.equals(anioActual + "")) {
					resumenPais.setMontoAnioTresD(lstTransferencia.get(indiceB)
							.getMontoTransaccionesD());
					resumenPais.setNumOperAnioTresD(lstTransferencia.get(
							indiceB).getNumOperacionesD());
				}
				if (lstTransferencia.get(indiceB).getCdPaisDestinoD()
						.equals(lstPaises.get(indiceA))
						&& lstTransferencia.get(indiceB).getAnioProcesoD()
								.equals((anioActual - 1) + "")) {
					resumenPais.setMontoAnioDosD(lstTransferencia.get(indiceB)
							.getMontoTransaccionesD());
					resumenPais.setNumOperAnioDosD(lstTransferencia
							.get(indiceB).getNumOperacionesD());
				}
				if (lstTransferencia.get(indiceB).getCdPaisDestinoD()
						.equals(lstPaises.get(indiceA))
						&& lstTransferencia.get(indiceB).getAnioProcesoD()
								.equals((anioActual - 2) + "")) {
					resumenPais.setMontoAnioUnoD(lstTransferencia.get(indiceB)
							.getMontoTransaccionesD());
					resumenPais.setNumOperAnioUnoD(lstTransferencia
							.get(indiceB).getNumOperacionesD());
				}
			}
		}

		for (TransferenciasInternacionalesTablaDTO tblVal : lstResultados) {
			System.out.println(" == Valor Registro Tabla == ");
			if (tblVal.getMontoAnioUnoD() == null) {
				tblVal.setMontoAnioUnoD(new BigDecimal(0));
			}
			if (tblVal.getNumOperAnioUnoD() == null) {
				tblVal.setNumOperAnioUnoD(new BigDecimal(0));
			}
			if (tblVal.getMontoAnioDosD() == null) {
				tblVal.setMontoAnioDosD(new BigDecimal(0));
			}
			if (tblVal.getNumOperAnioDosD() == null) {
				tblVal.setNumOperAnioDosD(new BigDecimal(0));
			}
			if (tblVal.getMontoAnioTresD() == null) {
				tblVal.setMontoAnioTresD(new BigDecimal(0));
			}
			if (tblVal.getNumOperAnioTresD() == null) {
				tblVal.setNumOperAnioTresD(new BigDecimal(0));
			}

			tblVal.setMontoAniosD(tblVal.getMontoAnioUnoD()
					.add(tblVal.getMontoAnioDosD())
					.add(tblVal.getMontoAnioTresD()));
			tblVal.setTotalAniosD(tblVal.getNumOperAnioUnoD()
					.add(tblVal.getNumOperAnioDosD())
					.add(tblVal.getNumOperAnioTresD()));
			System.out.println(tblVal.toString());
		}

		return lstResultados;
}
}
