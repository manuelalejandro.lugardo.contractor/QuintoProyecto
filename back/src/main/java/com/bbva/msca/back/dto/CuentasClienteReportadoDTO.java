package com.bbva.msca.back.dto;

import java.util.Date;

public class CuentasClienteReportadoDTO {
	
	private Date fhAperturaCCR;
    private String nuCuentaCCR;
    private String nbProductoCCR;
    private String nbSubproductoCCR;
    private String nbEstatusCCR;
    private String fhCancelacionCCR;
    private String nbTitularCCR;
    private String nuCtePartCCR;
    private String nbParticipesCCR;
    private String imSaldoCCR;
    private String imSaldoPromCCR;
    
	public Date getFhAperturaCCR() {
		return fhAperturaCCR;
	}
	public void setFhAperturaCCR(Date fhAperturaCCR) {
		this.fhAperturaCCR = fhAperturaCCR;
	}
	public String getNuCuentaCCR() {
		return nuCuentaCCR;
	}
	public void setNuCuentaCCR(String nuCuentaCCR) {
		this.nuCuentaCCR = nuCuentaCCR;
	}
	public String getNbProductoCCR() {
		return nbProductoCCR;
	}
	public void setNbProductoCCR(String nbProductoCCR) {
		this.nbProductoCCR = nbProductoCCR;
	}
	public String getNbSubproductoCCR() {
		return nbSubproductoCCR;
	}
	public void setNbSubproductoCCR(String nbSubproductoCCR) {
		this.nbSubproductoCCR = nbSubproductoCCR;
	}
	public String getNbEstatusCCR() {
		return nbEstatusCCR;
	}
	public void setNbEstatusCCR(String nbEstatusCCR) {
		this.nbEstatusCCR = nbEstatusCCR;
	}
	public String getFhCancelacionCCR() {
		return fhCancelacionCCR;
	}
	public void setFhCancelacionCCR(String fhCancelacionCCR) {
		this.fhCancelacionCCR = fhCancelacionCCR;
	}
	public String getNbTitularCCR() {
		return nbTitularCCR;
	}
	public void setNbTitularCCR(String nbTitularCCR) {
		this.nbTitularCCR = nbTitularCCR;
	}
	public String getNuCtePartCCR() {
		return nuCtePartCCR;
	}
	public void setNuCtePartCCR(String nuCtePartCCR) {
		this.nuCtePartCCR = nuCtePartCCR;
	}
	public String getNbParticipesCCR() {
		return nbParticipesCCR;
	}
	public void setNbParticipesCCR(String nbParticipesCCR) {
		this.nbParticipesCCR = nbParticipesCCR;
	}
	public String getImSaldoCCR() {
		return imSaldoCCR;
	}
	public void setImSaldoCCR(String imSaldoCCR) {
		this.imSaldoCCR = imSaldoCCR;
	}
	public String getImSaldoPromCCR() {
		return imSaldoPromCCR;
	}
	public void setImSaldoPromCCR(String imSaldoPromCCR) {
		this.imSaldoPromCCR = imSaldoPromCCR;
	}
    


	
	

}
