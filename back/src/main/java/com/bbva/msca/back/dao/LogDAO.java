package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;

import com.bbva.msca.back.dto.LogDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class LogDAO {
	

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	public List<LogDTO> getLog(){
		
		 List<LogDTO> lstLogDTO = new ArrayList<LogDTO>();
		
		 LogDTO logDTO = null;
		
			for(int i=0;i<10;i++){
			
				logDTO = new LogDTO();
				
				logDTO.setNuId(new BigDecimal(85));
				logDTO.setNbEvento("");
				logDTO.setNbAccion("");
				logDTO.setNbClvUsu("");
				logDTO.setNbUsuario("");
				logDTO.setFhRegistro("");
				
				lstLogDTO.add(logDTO);
			
			}
			return lstLogDTO;
		}
	
	@SuppressWarnings("unchecked")
	public List<LogDTO> getLogAnalisis(BigDecimal cdCaso , String nuCliente) throws Exception{ 
	 	 List<LogDTO> datosLog = null;
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getDatosLogAnalisis"));
			 queryCR.setParameter(1,cdCaso);
			 queryCR.setParameter(2,nuCliente); 
			 	 queryCR.unwrap(org.hibernate.SQLQuery.class)
		 		.addScalar("nuId")
				.addScalar("nbEvento")
				.addScalar("nbAccion")
				.addScalar("nbClvUsu")
				.addScalar("nbUsuario") 
				.addScalar("fhRegistro") 
				
 			.setResultTransformer(Transformers.aliasToBean(LogDTO.class));
			
			 	datosLog = (List<LogDTO>)queryCR.getResultList();

		return datosLog;
		
} 
	  
}
