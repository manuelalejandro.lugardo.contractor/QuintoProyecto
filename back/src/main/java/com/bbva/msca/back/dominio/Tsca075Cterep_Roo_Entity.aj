// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca075Cterep;
import java.lang.String;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca075Cterep_Roo_Entity {
    
    declare @type: Tsca075Cterep: @Entity;
    
    declare @type: Tsca075Cterep: @Table(name = "TSCA075_CTEREP", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca075Cterep.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CD_CTEREP", length = 8)
    private String Tsca075Cterep.cdCterep;
    
    public String Tsca075Cterep.getCdCterep() {
        return this.cdCterep;
    }
    
    public void Tsca075Cterep.setCdCterep(String id) {
        this.cdCterep = id;
    }
    
    @Transactional
    public void Tsca075Cterep.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca075Cterep.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca075Cterep attached = Tsca075Cterep.findTsca075Cterep(this.cdCterep);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca075Cterep.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca075Cterep.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca075Cterep Tsca075Cterep.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca075Cterep merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca075Cterep.entityManager() {
        EntityManager em = new Tsca075Cterep().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca075Cterep.countTsca075Ctereps() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca075Cterep o", Long.class).getSingleResult();
    }
    
    public static List<Tsca075Cterep> Tsca075Cterep.findAllTsca075Ctereps() {
        return entityManager().createQuery("SELECT o FROM Tsca075Cterep o", Tsca075Cterep.class).getResultList();
    }
    
    public static Tsca075Cterep Tsca075Cterep.findTsca075Cterep(String cdCterep) {
        if (cdCterep == null || cdCterep.length() == 0) return null;
        return entityManager().find(Tsca075Cterep.class, cdCterep);
    }
    
    public static List<Tsca075Cterep> Tsca075Cterep.findTsca075CterepEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca075Cterep o", Tsca075Cterep.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
