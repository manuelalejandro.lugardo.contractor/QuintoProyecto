package com.bbva.msca.back.srv;

import java.math.BigDecimal;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import com.bbva.msca.back.dto.DictamenCasoDTO;
import com.bbva.msca.back.dto.ParametrosBusquedaCasoDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/DictamenService")
public interface DictamenService {
	
	
	
	//Agregar Dictamen Preliminar OK Documentación
	@PUT
	@Path("/asignarDictamenPreliminar/{cdCaso}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric asignarDictamenPreliminar(@PathParam("cdCaso")BigDecimal cdCaso, DictamenCasoDTO dictamenCasoDTO);
	
	//Consultar Dictamen Preliminar OK Documentación
	@GET
	@Path("/consultarDictamenPreliminar/{cdCaso}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDictamenPreliminar(@PathParam("cdCaso")BigDecimal cdCaso);
	
	//Consulta de Dictamenes Liberados a SIA OK Documentación
	@GET
	@Path("/getConsultaDictamenesLiberados/{cdSesion}/{cdGerencia}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric getConsultaDictamenesLiberados(@PathParam("cdSesion")String cdSesion, @PathParam("cdGerencia")BigDecimal cdGerencia);

	//Consulta Casos Dictaminado por Consultor OK Documentación
	@GET
	@Path("/consultarCasosDictamen/{cdSupervisor}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCasosDictamen(@PathParam("cdSupervisor")BigDecimal cdSupervisor);
	
	//Consulta Casos Dictaminado por Consultor OK Documentación
	@POST
	@Path("/consultarCasosDictamenFiltro/{cdSupervisor}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCasosDictamenFiltro(@PathParam("cdSupervisor")BigDecimal cdSupervisor, ParametrosBusquedaCasoDTO parametrosBusquedaCasoDTO);

	
	//Agregar Dictamen Final OK Documentación
	@PUT
	@Path("/asignarDictamenFinal/{cdCaso}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric asignarDictamenFinal(@PathParam("cdCaso")BigDecimal cdCaso, DictamenCasoDTO dictamenCasoDTO);
	
	//Consultar Otras Gerencias para txtComentario OK Documentación
	@GET
	@Path("/consultarDictamenPreliminarTxtComentario/{cdCaso}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDictamenPreliminarTxtComentario(@PathParam("cdCaso")BigDecimal cdCaso);

}
