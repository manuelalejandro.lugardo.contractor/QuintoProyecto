package com.bbva.msca.back.srv;

import com.bbva.msca.back.util.ResponseGenericPagination;
import com.bbva.msca.back.dao.MovimientosDAO;
import com.bbva.msca.back.dto.MontosTotalDTO;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class MovimientosServiceImpl implements MovimientosService{
	
	@Override
	public ResponseGeneric consultarMovimientos(String nuFolioAlerta, String cdSistema, Integer limit, Integer offset,String search,String name, String order) {
		ResponseGeneric response = new ResponseGeneric();
		ResponseGenericPagination pagination = new ResponseGenericPagination();
		MovimientosDAO movimientosDAO = new MovimientosDAO();
		
		try{
			pagination.setRows(movimientosDAO.getMovimientos(nuFolioAlerta, cdSistema, limit, offset, search, name, order));
			pagination.setTotal(movimientosDAO.getNuMovimientos(nuFolioAlerta, cdSistema));
			response.setResponse(pagination);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		
		return response;
	}

	@Override
	public ResponseGeneric consultarMontosTotales(String nuFolioAlerta, String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();
		MovimientosDAO movimientosDAO = new MovimientosDAO();
		MontosTotalDTO montosTotalDTO = new MontosTotalDTO();
		MontosTotalDTO montosTotalCargosDTO = null;
		MontosTotalDTO montosTotalAbonosDTO = null;
		
		try{
			montosTotalAbonosDTO = movimientosDAO.getTotalAbonos(nuFolioAlerta, cdSistema);
			montosTotalCargosDTO = movimientosDAO.getTotalCargos(nuFolioAlerta, cdSistema);
			
			montosTotalDTO.setImAbonosTotal(montosTotalAbonosDTO.getImAbonosTotal());
			montosTotalDTO.setImCargosTotal(montosTotalCargosDTO.getImCargosTotal());
			montosTotalDTO.setNumAbonosTotal(montosTotalAbonosDTO.getNumAbonosTotal());
			montosTotalDTO.setNumCargosTotal(montosTotalCargosDTO.getNumCargosTotal());
			
			
			response.setResponse(montosTotalDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

}
