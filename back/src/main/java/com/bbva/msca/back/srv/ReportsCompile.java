package com.bbva.msca.back.srv;

import net.sf.jasperreports.engine.JasperCompileManager;

public class ReportsCompile {
	
	public static void main(String[] args) {
		try {
			System.out.println( "Comipla Plantillas"  );
			
			// Cedula Caso
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/RP_CedulaCaso.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/RP_CedulaCaso.jasper"
					);
			
			// Datos principales de la cedula
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_DatosCaso.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_DatosCaso.jasper"				
					);
			
			// Identificacion Reporte
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_identificacionReporte.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_identificacionReporte.jasper"				
					);
			
			// Datos Generales del cliente Reportado
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_Datos_Generales.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_Datos_Generales.jasper"				
					);

			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_Cont_ConocimientoCliente.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_Cont_ConocimientoCliente.jasper"				
					);

			
			/*
			// Conocimiento del Cliente
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_ConocimientoDelCliente.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_ConocimientoDelCliente.jasper"				
					);			
			
			// Lista Transaccionalidad Decarada
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_TransaccionalidadCliente.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_TransaccionalidadCliente.jasper"				
					);
			
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_InfAccEstructEmp.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_InfAccEstructEmp.jasper"				
					);
			*/
			/*
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_OtrosProductosClienteReportado.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_OtrosProductosClienteReportado.jasper"				
					);
			 */			
			/*
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_OP_TarjetaCredito.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_OP_TarjetaCredito.jasper"				
					);											
			*/
			/*
			// Descripcion de la transaccion
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_DescripcionTransaccion.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_DescripcionTransaccion.jasper"				
					);
			

			
			// Opinion del Funcionario
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_OpinionDelFuncionario.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_OpinionDelFuncionario.jasper"				
					);			
			
			// Oficios Listas y Notas Periodisticas
			JasperCompileManager.compileReportToFile(
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_OficiosListasNotasPeriodisticas.jrxml",
					"D:/BBMHCP/MSCA/APP/Plantillas/SR_OficiosListasNotasPeriodisticas.jasper"				
					);						
			*/
			System.out.println( "Compilacion Finalizada"  );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}		

}
