// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca003Oficina;
import java.lang.String;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.OneToMany;

privileged aspect Tsca073Funcionario_Roo_DbManaged {
    
    @OneToMany(mappedBy = "cdFuncionario")
    private Set<Tsca003Oficina> Tsca073Funcionario.tsca003Oficinas;
    
    @Column(name = "NB_FUNCIONARIO", length = 40)
    private String Tsca073Funcionario.nbFuncionario;
    
    public Set<Tsca003Oficina> Tsca073Funcionario.getTsca003Oficinas() {
        return this.tsca003Oficinas;
    }
    
    public void Tsca073Funcionario.setTsca003Oficinas(Set<Tsca003Oficina> tsca003Oficinas) {
        this.tsca003Oficinas = tsca003Oficinas;
    }
    
    public String Tsca073Funcionario.getNbFuncionario() {
        return this.nbFuncionario;
    }
    
    public void Tsca073Funcionario.setNbFuncionario(String nbFuncionario) {
        this.nbFuncionario = nbFuncionario;
    }
    
}
