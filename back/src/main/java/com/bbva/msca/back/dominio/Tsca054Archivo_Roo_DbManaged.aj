// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import java.lang.String;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.springframework.format.annotation.DateTimeFormat;

privileged aspect Tsca054Archivo_Roo_DbManaged {
    
    @Column(name = "CD_EXPEDIENTE", length = 20)
    private String Tsca054Archivo.cdExpediente;
    
    @Column(name = "NB_TITULO", length = 20)
    private String Tsca054Archivo.nbTitulo;
    
    @Column(name = "FH_CREACION")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date Tsca054Archivo.fhCreacion;
    
    @Column(name = "FH_MODIF")
    @Temporal(TemporalType.DATE)
    @DateTimeFormat(style = "M-")
    private Date Tsca054Archivo.fhModif;
    
    @Column(name = "TX_INVESTIGACION", length = 2000)
    private String Tsca054Archivo.txInvestigacion;
    
    public String Tsca054Archivo.getCdExpediente() {
        return this.cdExpediente;
    }
    
    public void Tsca054Archivo.setCdExpediente(String cdExpediente) {
        this.cdExpediente = cdExpediente;
    }
    
    public String Tsca054Archivo.getNbTitulo() {
        return this.nbTitulo;
    }
    
    public void Tsca054Archivo.setNbTitulo(String nbTitulo) {
        this.nbTitulo = nbTitulo;
    }
    
    public Date Tsca054Archivo.getFhCreacion() {
        return this.fhCreacion;
    }
    
    public void Tsca054Archivo.setFhCreacion(Date fhCreacion) {
        this.fhCreacion = fhCreacion;
    }
    
    public Date Tsca054Archivo.getFhModif() {
        return this.fhModif;
    }
    
    public void Tsca054Archivo.setFhModif(Date fhModif) {
        this.fhModif = fhModif;
    }
    
    public String Tsca054Archivo.getTxInvestigacion() {
        return this.txInvestigacion;
    }
    
    public void Tsca054Archivo.setTxInvestigacion(String txInvestigacion) {
        this.txInvestigacion = txInvestigacion;
    }
    
}
