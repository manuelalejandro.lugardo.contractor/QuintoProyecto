package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaR8DTO {

	@AnotacionAlerta(tipologia = "r8", idCampo = "nbCte", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")
	private String nbCte;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nbAppaternoCte", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_NBAPPATERNOCTE", instanceType = "1")
	private String nbAppaternoCte;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nbApmaternoCte", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_NBAPMATERNOCTE", instanceType = "1")
	private String nbApmaternoCte;
	@AnotacionAlerta(tipologia = "r8", idCampo = "fhNac", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_FHNAC", instanceType = "2" , formatter = "formatterDate")
	private Date fhNac;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nuEdad", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_NUEDAD")
	private BigDecimal nuEdad;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nbActividad", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_NBACTIVIDAD", instanceType = "1")
	private String nbActividad;
	@AnotacionAlerta(tipologia = "r8", idCampo = "ctMovimiento", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_CTMOVIMIENTO", instanceType = "1")
	private String ctMovimiento;
	@AnotacionAlerta(tipologia = "r8", idCampo = "cdSector", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_CDSECTOR", instanceType = "1")
	private String cdSector;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nbSucApertura", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_NBSUCURSAL", instanceType = "1")
	private String nbSucApertura;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nbEstado", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_NBESTADO", instanceType = "1")
	private String nbEstado;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nuDep", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_NUDEP")
	private BigDecimal nuDep;
	@AnotacionAlerta(tipologia = "r8", idCampo = "imImporteDep", posicion = "12", nbEtiqueta = "MSCK_LBL_01002_IMIMPORTEDEP", formatter = "formatterMoney")
	private BigDecimal imImporteDep;
	@AnotacionAlerta(tipologia = "r8", idCampo = "imImporteRet", posicion = "13", nbEtiqueta = "MSCK_LBL_01002_IMIMPORTERET", formatter = "formatterMoney")
	private BigDecimal imImporteRet;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nuRet", posicion = "14", nbEtiqueta = "MSCK_LBL_01002_NURET")
	private BigDecimal nuRet;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nuCv", posicion = "15", nbEtiqueta = "MSCK_LBL_01002_NUCV")
	private BigDecimal nuCv;
	@AnotacionAlerta(tipologia = "r8", idCampo = "imImporteCv", posicion = "16", nbEtiqueta = "MSCK_LBL_01002_IMIMPORTECV", formatter = "formatterMoney")
	private BigDecimal imImporteCv;
	@AnotacionAlerta(tipologia = "r8", idCampo = "cdMovOtro", posicion = "17", nbEtiqueta = "MSCK_LBL_01002_CDMOVOTRO")
	private BigDecimal cdMovOtro;
	@AnotacionAlerta(tipologia = "r8", idCampo = "imImpOtro", posicion = "18", nbEtiqueta = "MSCK_LBL_01002_IMIMPOTRO", formatter = "formatterMoney")
	private BigDecimal imImpOtro;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nuPorcRelev", posicion = "19", nbEtiqueta = "MSCK_LBL_01002_NUPORCRELEV")
	private BigDecimal nuPorcRelev;
	@AnotacionAlerta(tipologia = "r8", idCampo = "nuPorcMedian", posicion = "20", nbEtiqueta = "MSCK_LBL_01002_NUPORCMEDIAN")
	private BigDecimal nuPorcMedian;
	@AnotacionAlerta(tipologia = "r8", idCampo = "fhMasReciente", posicion = "21", nbEtiqueta = "MSCK_LBL_01002_FHMASRECIENTE", instanceType = "2", formatter = "formatterDate")
	private Date fhMasReciente;
	@AnotacionAlerta(tipologia = "r8", idCampo = "cdNuevocr", posicion = "22", nbEtiqueta = "MSCK_LBL_01002_CDNUEVOCR", instanceType = "1")
	private String cdNuevocr;
	
	public String getNbCte() {
		return nbCte;
	}
	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}
	public String getNbActividad() {
		return nbActividad;
	}
	public void setNbActividad(String nbActividad) {
		this.nbActividad = nbActividad;
	}
	public String getCtMovimiento() {
		return ctMovimiento;
	}
	public void setCtMovimiento(String ctMovimiento) {
		this.ctMovimiento = ctMovimiento;
	}
	public String getNbAppaternoCte() {
		return nbAppaternoCte;
	}
	public void setNbAppaternoCte(String nbAppaternoCte) {
		this.nbAppaternoCte = nbAppaternoCte;
	}
	public String getNbApmaternoCte() {
		return nbApmaternoCte;
	}
	public void setNbApmaternoCte(String nbApmaternoCte) {
		this.nbApmaternoCte = nbApmaternoCte;
	}
	public Date getFhNac() {
		return fhNac;
	}
	public void setFhNac(Date fhNac) {
		this.fhNac = fhNac;
	}
	public BigDecimal getNuEdad() {
		return nuEdad;
	}
	public void setNuEdad(BigDecimal nuEdad) {
		this.nuEdad = nuEdad;
	}
	public String getCdSector() {
		return cdSector;
	}
	public void setCdSector(String cdSector) {
		this.cdSector = cdSector;
	}
	public String getNbSucApertura() {
		return nbSucApertura;
	}
	public void setNbSucApertura(String nbSucApertura) {
		this.nbSucApertura = nbSucApertura;
	}
	public String getNbEstado() {
		return nbEstado;
	}
	public void setNbEstado(String nbEstado) {
		this.nbEstado = nbEstado;
	}
	public BigDecimal getNuDep() {
		return nuDep;
	}
	public void setNuDep(BigDecimal nuDep) {
		this.nuDep = nuDep;
	}
	public BigDecimal getImImporteDep() {
		return imImporteDep;
	}
	public void setImImporteDep(BigDecimal imImporteDep) {
		this.imImporteDep = imImporteDep;
	}
	public BigDecimal getImImporteRet() {
		return imImporteRet;
	}
	public void setImImporteRet(BigDecimal imImporteRet) {
		this.imImporteRet = imImporteRet;
	}
	public BigDecimal getNuRet() {
		return nuRet;
	}
	public void setNuRet(BigDecimal nuRet) {
		this.nuRet = nuRet;
	}
	public BigDecimal getNuCv() {
		return nuCv;
	}
	public void setNuCv(BigDecimal nuCv) {
		this.nuCv = nuCv;
	}
	public BigDecimal getImImporteCv() {
		return imImporteCv;
	}
	public void setImImporteCv(BigDecimal imImporteCv) {
		this.imImporteCv = imImporteCv;
	}
	public BigDecimal getCdMovOtro() {
		return cdMovOtro;
	}
	public void setCdMovOtro(BigDecimal cdMovOtro) {
		this.cdMovOtro = cdMovOtro;
	}
	public BigDecimal getImImpOtro() {
		return imImpOtro;
	}
	public void setImImpOtro(BigDecimal imImpOtro) {
		this.imImpOtro = imImpOtro;
	}
	public BigDecimal getNuPorcRelev() {
		return nuPorcRelev;
	}
	public void setNuPorcRelev(BigDecimal nuPorcRelev) {
		this.nuPorcRelev = nuPorcRelev;
	}
	public BigDecimal getNuPorcMedian() {
		return nuPorcMedian;
	}
	public void setNuPorcMedian(BigDecimal nuPorcMedian) {
		this.nuPorcMedian = nuPorcMedian;
	}
	public Date getFhMasReciente() {
		return fhMasReciente;
	}
	public void setFhMasReciente(Date fhMasReciente) {
		this.fhMasReciente = fhMasReciente;
	}
	public String getCdNuevocr() {
		return cdNuevocr;
	}
	public void setCdNuevocr(String cdNuevocr) {
		this.cdNuevocr = cdNuevocr;
	}
}
