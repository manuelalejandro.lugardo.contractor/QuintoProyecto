package com.bbva.msca.back.srv;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.util.ResponseGeneric;

@Path("/AlertasService")
public interface AlertasService {

	@PUT
	@Path("/crearAlerta")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric crearAlerta(Object obj) throws Exception;

	@POST
	@Path("/modificarAlerta")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric modificarAlerta(Object obj) throws Exception;

	@DELETE
	@Path("/eliminarAlerta")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric eliminarAlerta(Object obj) throws Exception;

	@GET
	@Path("/consultarAlerta/{cdAlerta}/{cdSistema}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAlerta(@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
	
	@GET
	@Path("/consultarAlertaDetalle/{cdAlerta}/{cdSistema}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAlertaDetalle(@PathParam("cdAlerta") String idAlerta, @PathParam("cdSistema") String idSistema);

	@GET
	@Path("/consultarAlertas")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAlertas(Object obj) throws Exception;
	
	
	//Consulta de Alertas Previas de SIA y SICA 
	@GET
	@Path("/consultarAlertasPrevias/{cdCliente}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAlertasPrevias(@PathParam("cdCliente") String cdCliente);
	
	@GET
	@Path("/consultarMatrizCriterioAlerta/{cdAlerta}/{cdSistema}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarMatrizCriterioAlerta(@PathParam("cdAlerta") String cdAlerta,@PathParam("cdSistema") String cdSistema);
}
