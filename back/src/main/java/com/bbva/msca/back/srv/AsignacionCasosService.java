package com.bbva.msca.back.srv;

import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.AlertaDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/AsignacionCasosService")

public interface AsignacionCasosService {
	
	@GET
	@Path("/consultarAlertasPendientesAsignacionGerencia/{cdGerencia}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAlertasPendientesAsignacionGerencia(@PathParam("cdGerencia") BigDecimal cdGerencia,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset,
			@QueryParam("search") String search,
			@QueryParam("name") String name,
			@QueryParam("order") String order);
	
	@GET
	@Path("/consultarCasosAsignadosConsultor/{cdConsultor}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarCasosAsignadosConsultor(@PathParam("cdConsultor") BigDecimal cdConsultor,
			@QueryParam("limit") Integer limit,
			@QueryParam("offset") Integer offset,
			@QueryParam("search") String search,
			@QueryParam("name") String name,
			@QueryParam("order") String order);
	
	@GET
	@Path("/consultarConsultoresSupervisor/{cdSupervisor}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarConsultoresSupervisor(@PathParam("cdSupervisor") BigDecimal cdSupervisor);
	
	@GET
	@Path("/consultarAlertasAsignadas")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarAlertasAsignadas();
	
	@PUT
	@Path("/asignarAlertasCasos/{cdUsuario}/{cdPrioriAsig}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric asignarAlertasCasos(List<AlertaDTO> lstAlertaDTO,@PathParam("cdUsuario") String cdUsuario,@PathParam("cdPrioriAsig") String cdPrioriAsig);
	
	
	@POST
	@Path("/descartarAlertas")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric descartarAlertas(List<AlertaDTO> lstAlertaDTO);
	
	@POST
	@Path("/reasignarCasos/{cdUsuario}")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric reasignarCasos(List<AlertaDTO> lstAlertaDTO,@PathParam("cdUsuario") String cdUsuario);
	

}
