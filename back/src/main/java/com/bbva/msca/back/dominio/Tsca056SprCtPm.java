package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(identifierType = Tsca056SprCtPmPK.class, versionField = "", table = "TSCA056_SPR_CT_PM", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca056SprCtPm {
}
