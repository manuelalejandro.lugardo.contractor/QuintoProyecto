package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class ChecklistDTO {

	private BigDecimal cdCliAltRie;
	private BigDecimal nuId;
	private BigDecimal cdChecklist;
	private String cdClienteBancomer;
	private String nbClienteBancomer;
	private Character nbLiberadoASia;
	private Character nbCorreoPreAnalisis;
	private Character nbCorreoFolioPld;
	private Character nbCalificacion;
	private Character nbSolInfoFiduciario;
	private Character nbRelevantes;
	private Character nbOpis;
	private Character nbListaBbva;
	private Character nbValidarBloqCtas;
	private String cdCliente;
	private String nbCliRazSoc;
	private Date fhRecepcion;
	private String cdOficio;
	private String cdTipologia;
	private String cdUsrAlta;

	public void setCdCliAltRie(BigDecimal cdCliAltRie) {
		this.cdCliAltRie = cdCliAltRie;
	}

	public BigDecimal getCdCliAltRie() {
		return cdCliAltRie;
	}

	public void setNuId(BigDecimal nuId) {
		this.nuId = nuId;
	}

	public BigDecimal getNuId() {
		return nuId;
	}

	public void setCdChecklist(BigDecimal cdChecklist) {
		this.cdChecklist = cdChecklist;
	}

	public BigDecimal getCdChecklist() {
		return cdChecklist;
	}

	public void setCdClienteBancomer(String cdClienteBancomer) {
		this.cdClienteBancomer = cdClienteBancomer;
		if(cdClienteBancomer!=null && !cdClienteBancomer.isEmpty() 
				&& cdClienteBancomer.toUpperCase().equals("SI")){
			this.setNbClienteBancomer("S");
		}else{
			this.setNbClienteBancomer("Y");
		}
	}

	public String getCdClienteBancomer() {
		return cdClienteBancomer;
	}

	public void setNbLiberadoASia(Character nbLiberadoASia) {
		this.nbLiberadoASia = nbLiberadoASia;
	}

	public Character getNbLiberadoASia() {
		return nbLiberadoASia;
	}

	public void setNbCorreoPreAnalisis(Character nbCorreoPreAnalisis) {
		this.nbCorreoPreAnalisis = nbCorreoPreAnalisis;
	}

	public Character getNbCorreoPreAnalisis() {
		return nbCorreoPreAnalisis;
	}

	public void setNbCorreoFolioPld(Character nbCorreoFolioPld) {
		this.nbCorreoFolioPld = nbCorreoFolioPld;
	}

	public Character getNbCorreoFolioPld() {
		return nbCorreoFolioPld;
	}

	public void setNbCalificacion(Character nbCalificacion) {
		this.nbCalificacion = nbCalificacion;
	}

	public Character getNbCalificacion() {
		return nbCalificacion;
	}

	public void setNbSolInfoFiduciario(Character nbSolInfoFiduciario) {
		this.nbSolInfoFiduciario = nbSolInfoFiduciario;
	}

	public Character getNbSolInfoFiduciario() {
		return nbSolInfoFiduciario;
	}

	public void setNbRelevantes(Character nbRelevantes) {
		this.nbRelevantes = nbRelevantes;
	}

	public Character getNbRelevantes() {
		return nbRelevantes;
	}

	public void setNbOpis(Character nbOpis) {
		this.nbOpis = nbOpis;
	}

	public Character getNbOpis() {
		return nbOpis;
	}

	public void setNbListaBbva(Character nbListaBbva) {
		this.nbListaBbva = nbListaBbva;
	}

	public Character getNbListaBbva() {
		return nbListaBbva;
	}

	public void setNbValidarBloqCtas(Character nbValidarBloqCtas) {
		this.nbValidarBloqCtas = nbValidarBloqCtas;
	}

	public Character getNbValidarBloqCtas() {
		return nbValidarBloqCtas;
	}

	public void setCdCliente(String cdCliente) {
		this.cdCliente = cdCliente;
	}

	public String getCdCliente() {
		return cdCliente;
	}

	public void setNbCliRazSoc(String nbCliRazSoc) {
		this.nbCliRazSoc = nbCliRazSoc;
	}

	public String getNbCliRazSoc() {
		return nbCliRazSoc;
	}

	public void setFhRecepcion(Date fhRecepcion) {
		this.fhRecepcion = fhRecepcion;
	}

	public Date getFhRecepcion() {
		return fhRecepcion;
	}

	public void setCdOficio(String cdOficio) {
		this.cdOficio = cdOficio;
	}

	public String getCdOficio() {
		return cdOficio;
	}

	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}

	public String getCdTipologia() {
		return cdTipologia;
	}

	public void setCdUsrAlta(String cdUsrAlta) {
		this.cdUsrAlta = cdUsrAlta;
	}

	public String getCdUsrAlta() {
		return cdUsrAlta;
	}

	public void setNbClienteBancomer(String nbClienteBancomer) {
		this.nbClienteBancomer = nbClienteBancomer;
	}

	public String getNbClienteBancomer() {
		return nbClienteBancomer;
	}

}
