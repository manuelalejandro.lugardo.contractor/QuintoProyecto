package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import com.bbva.msca.back.dominio.Tsca017CuentaPK;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(versionField = "", table = "TSCA017_CUENTA", schema = "GORAPR", identifierType = Tsca017CuentaPK.class)
public class Tsca017Cuenta {
}
