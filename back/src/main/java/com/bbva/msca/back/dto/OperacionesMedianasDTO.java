package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;


public class OperacionesMedianasDTO {

	private String nuCuentaOMED;
	private String nbOperacionesOMED; 
	private BigDecimal nuOperacionesOMED;
	private BigDecimal imImporteOMED;
	private String nbMonedaOMED;
	private Date fhOperacionOMED;
	
	public String getNuCuentaOMED() {
		return nuCuentaOMED;
	}
	public void setNuCuentaOMED(String nuCuentaOMED) {
		this.nuCuentaOMED = nuCuentaOMED;
	}
	public String getNbOperacionesOMED() {
		return nbOperacionesOMED;
	}
	public void setNbOperacionesOMED(String nbOperacionesOMED) {
		this.nbOperacionesOMED = nbOperacionesOMED;
	}
	public BigDecimal getNuOperacionesOMED() {
		return nuOperacionesOMED;
	}
	public void setNuOperacionesOMED(BigDecimal nuOperacionesOMED) {
		this.nuOperacionesOMED = nuOperacionesOMED;
	}
	public BigDecimal getImImporteOMED() {
		return imImporteOMED;
	}
	public void setImImporteOMED(BigDecimal imImporteOMED) {
		this.imImporteOMED = imImporteOMED;
	}
	public String getNbMonedaOMED() {
		return nbMonedaOMED;
	}
	public void setNbMonedaOMED(String nbMonedaOMED) {
		this.nbMonedaOMED = nbMonedaOMED;
	}
	public Date getFhOperacionOMED() {
		return fhOperacionOMED;
	}
	public void setFhOperacionOMED(Date fhOperacionOMED) {
		this.fhOperacionOMED = fhOperacionOMED;
	}
	
	
}