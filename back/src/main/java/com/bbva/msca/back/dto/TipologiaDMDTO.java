package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaDMDTO {

	@AnotacionAlerta(tipologia = "dm", idCampo = "imMontoAlerta", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_IMMONTOALERTA", formatter = "formatterMoney")
	private BigDecimal imMontoAlerta;
	@AnotacionAlerta(tipologia = "dm", idCampo = "nbCte", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")
	private String nbCte;
	@AnotacionAlerta(tipologia = "dm", idCampo = "nuOper", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_NUOPER")
	private BigDecimal nuOper;
	@AnotacionAlerta(tipologia = "dm", idCampo = "nbSucApertura", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_NBSUCAPERTURA", instanceType = "1")
	private String nbSucApertura;
	@AnotacionAlerta(tipologia = "dm", idCampo = "nbDivision", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_NBDIVISION", instanceType = "1")
	private String nbDivision;
	@AnotacionAlerta(tipologia = "dm", idCampo = "nbEstado", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_NBESTADO", instanceType = "1")
	private String nbEstado;
	@AnotacionAlerta(tipologia = "dm", idCampo = "tpSector", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_TPSECTOR", instanceType = "1")
	private String tpSector;
	@AnotacionAlerta(tipologia = "dm", idCampo = "nuAntiguedad", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_NUANTIGUEDAD")
	private String nuAntiguedad;
	@AnotacionAlerta(tipologia = "dm", idCampo = "cdSucursal", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_CDSUCURSAL", instanceType = "1")
	private String cdSucursal;
	@AnotacionAlerta(tipologia = "dm", idCampo = "cdDirZona", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_CDDIRZONA", instanceType = "1")
	private String cdDirZona;
	

	public BigDecimal getImMontoAlerta() {
		return imMontoAlerta;
	}
	public void setImMontoAlerta(BigDecimal imMontoAlerta) {
		this.imMontoAlerta = imMontoAlerta;
	}
	public String getNbCte() {
		return nbCte;
	}
	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}
	public BigDecimal getNuOper() {
		return nuOper;
	}
	public void setNuOper(BigDecimal nuOper) {
		this.nuOper = nuOper;
	}
	public String getNbSucApertura() {
		return nbSucApertura;
	}
	public void setNbSucApertura(String nbSucApertura) {
		this.nbSucApertura = nbSucApertura;
	}
	public String getNbDivision() {
		return nbDivision;
	}
	public void setNbDivision(String nbDivision) {
		this.nbDivision = nbDivision;
	}
	public String getNbEstado() {
		return nbEstado;
	}
	public void setNbEstado(String nbEstado) {
		this.nbEstado = nbEstado;
	}
	public String getTpSector() {
		return tpSector;
	}
	public void setTpSector(String tpSector) {
		this.tpSector = tpSector;
	}
	public String getNuAntiguedad() {
		return nuAntiguedad;
	}
	public void setNuAntiguedad(String nuAntiguedad) {
		this.nuAntiguedad = nuAntiguedad;
	}
	public String getCdSucursal() {
		return cdSucursal;
	}
	public void setCdSucursal(String cdSucursal) {
		this.cdSucursal = cdSucursal;
	}
	public String getCdDirZona() {
		return cdDirZona;
	}
	public void setCdDirZona(String cdDirZona) {
		this.cdDirZona = cdDirZona;
	}
}
