package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.List;


public class UsuarioDTO  {
	
	private String cdUsuario;
	private BigDecimal cdPerfil1;
	private String nbPerfil1;
	private BigDecimal cdPerfil2;
	private String nbPerfil2;
	private String nbMaterno;
	private String nbPaterno;
	private String nbCveRed;
	private String nbPersona;
	private String nbEmail;
	private BigDecimal cdEstatus;
	private String nbEstatus;
	private String nbSupervisor;
	private List<ModuloDTO> lstModulos1;
	private List<ModuloDTO> lstModulos2;
	private String cdStSistema;
	private String cdSupervisor;
	private String nbCveRedSupervisor;
	private BigDecimal cdGerencia;
	private BigDecimal cdGerenciaSupervisor;
	private String nbCliente;
	private String nbStSistema;
	private String cdUsrAlta;
	
	public BigDecimal getCdGerencia() {
		return cdGerencia;
	}
	public void setCdGerencia(BigDecimal cdGerencia) {
		this.cdGerencia = cdGerencia;
	}
	public String getCdUsuario() {
		return cdUsuario;
	}
	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}
	public String getCdStSistema() {
		return cdStSistema;
	}
	public void setCdStSistema(String cdStSistema) {
		this.cdStSistema = cdStSistema;
	}
	public String getCdSupervisor() {
		return cdSupervisor;
	}
	public void setCdSupervisor(String cdSupervisor) {
		this.cdSupervisor = cdSupervisor;
	}
	public BigDecimal getCdPerfil1() {
		return cdPerfil1;
	}
	public void setCdPerfil1(BigDecimal cdPerfil1) {
		this.cdPerfil1 = cdPerfil1;
	}
	public BigDecimal getCdPerfil2() {
		return cdPerfil2;
	}
	public void setCdPerfil2(BigDecimal cdPerfil2) {
		this.cdPerfil2 = cdPerfil2;
	}
	public String getNbMaterno() {
		return nbMaterno;
	}
	public void setNbMaterno(String nbMaterno) {
		this.nbMaterno = nbMaterno;
	}
	public String getNbPaterno() {
		return nbPaterno;
	}
	public void setNbPaterno(String nbPaterno) {
		this.nbPaterno = nbPaterno;
	}
	public String getNbCveRed() {
		return nbCveRed;
	}
	public void setNbCveRed(String nbCveRed) {
		this.nbCveRed = nbCveRed;
	}
	public String getNbPersona() {
		return nbPersona;
	}
	public void setNbPersona(String nbPersona) {
		this.nbPersona = nbPersona;
	}
	public List<ModuloDTO> getLstModulos1() {
		return lstModulos1;
	}
	public void setLstModulos1(List<ModuloDTO> lstModulos1) {
		this.lstModulos1 = lstModulos1;
	}
	public String getNbPerfil1() {
		return nbPerfil1;
	}
	public void setNbPerfil1(String nbPerfil1) {
		this.nbPerfil1 = nbPerfil1;
	}
	public String getNbPerfil2() {
		return nbPerfil2;
	}
	public void setNbPerfil2(String nbPerfil2) {
		this.nbPerfil2 = nbPerfil2;
	}
	public List<ModuloDTO> getLstModulos2() {
		return lstModulos2;
	}
	public void setLstModulos2(List<ModuloDTO> lstModulos2) {
		this.lstModulos2 = lstModulos2;
	}
	
	public String getNbEmail() {
		return nbEmail;
	}
	public void setNbEmail(String nbEmail) {
		this.nbEmail = nbEmail;
	}
	public BigDecimal getCdEstatus() {
		return cdEstatus;
	}
	public void setCdEstatus(BigDecimal cdEstatus) {
		this.cdEstatus = cdEstatus;
	}
	public String getNbEstatus() {
		return nbEstatus;
	}
	public void setNbEstatus(String nbEstatus) {
		this.nbEstatus = nbEstatus;
	}
	public String getNbSupervisor() {
		return nbSupervisor;
	}
	public void setNbSupervisor(String nbSupervisor) {
		this.nbSupervisor = nbSupervisor;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbStSistema(String nbStSistema) {
		this.nbStSistema = nbStSistema;
	}
	public String getNbStSistema() {
		return nbStSistema;
	}
	public void setNbCveRedSupervisor(String nbCveRedSupervisor) {
		this.nbCveRedSupervisor = nbCveRedSupervisor;
	}
	public String getNbCveRedSupervisor() {
		return nbCveRedSupervisor;
	}
	public void setCdGerenciaSupervisor(BigDecimal cdGerenciaSupervisor) {
		this.cdGerenciaSupervisor = cdGerenciaSupervisor;
	}
	public BigDecimal getCdGerenciaSupervisor() {
		return cdGerenciaSupervisor;
	}
	public void setCdUsrAlta(String cdUsrAlta) {
		this.cdUsrAlta = cdUsrAlta;
	}
	public String getCdUsrAlta() {
		return cdUsrAlta;
	}
	
}
