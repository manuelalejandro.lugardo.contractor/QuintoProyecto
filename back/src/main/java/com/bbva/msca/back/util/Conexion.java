package com.bbva.msca.back.util;

import java.sql.Connection;
import java.sql.SQLException;

public class Conexion {
	
	ConexionSIABD conexionSIABD = null;
	Connection  connection = null;
	
	
	
	public Conexion(){
	}
	
	public Connection getConexion(){
		
		conexionSIABD = new ConexionSIABD();
		
		if (conexionSIABD != null){
			connection = conexionSIABD.getConnection();
		}
		return connection;
	} 
	
	public Connection close() throws SQLException{
		if (connection!= null){
			connection.close();
		}
		return connection;
	} 
}
