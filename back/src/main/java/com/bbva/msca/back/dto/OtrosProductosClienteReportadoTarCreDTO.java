package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class OtrosProductosClienteReportadoTarCreDTO {
	

	private Date fhAperturaOPCR;
    private String nuCuentaOPCR;
    private String nbProductoOPCR;
    private String nbSubproductoOPCR;
    private String nbEstatusOPCR;
    private String fhCancelacionOPCR;
    private String nbTitularOPCR;
    private String nuCtePartOPCR;
    private String nbParticipesOPCR;
    private String imProductoOPCR;
    private String imProPromOPCR;
    
    
	public Date getFhAperturaOPCR() {
		return fhAperturaOPCR;
	}
	public void setFhAperturaOPCR(Date fhAperturaOPCR) {
		this.fhAperturaOPCR = fhAperturaOPCR;
	}
	public String getNuCuentaOPCR() {
		return nuCuentaOPCR;
	}
	public void setNuCuentaOPCR(String nuCuentaOPCR) {
		this.nuCuentaOPCR = nuCuentaOPCR;
	}
	public String getNbProductoOPCR() {
		return nbProductoOPCR;
	}
	public void setNbProductoOPCR(String nbProductoOPCR) {
		this.nbProductoOPCR = nbProductoOPCR;
	}
	public String getNbSubproductoOPCR() {
		return nbSubproductoOPCR;
	}
	public void setNbSubproductoOPCR(String nbSubproductoOPCR) {
		this.nbSubproductoOPCR = nbSubproductoOPCR;
	}
	public String getNbEstatusOPCR() {
		return nbEstatusOPCR;
	}
	public void setNbEstatusOPCR(String nbEstatusOPCR) {
		this.nbEstatusOPCR = nbEstatusOPCR;
	}
	public String getFhCancelacionOPCR() {
		return fhCancelacionOPCR;
	}
	public void setFhCancelacionOPCR(String fhCancelacionOPCR) {
		this.fhCancelacionOPCR = fhCancelacionOPCR;
	}
	public String getNbTitularOPCR() {
		return nbTitularOPCR;
	}
	public void setNbTitularOPCR(String nbTitularOPCR) {
		this.nbTitularOPCR = nbTitularOPCR;
	}
	public String getNuCtePartOPCR() {
		return nuCtePartOPCR;
	}
	public void setNuCtePartOPCR(String nuCtePartOPCR) {
		this.nuCtePartOPCR = nuCtePartOPCR;
	}
	public String getNbParticipesOPCR() {
		return nbParticipesOPCR;
	}
	public void setNbParticipesOPCR(String nbParticipesOPCR) {
		this.nbParticipesOPCR = nbParticipesOPCR;
	}
	public String getImProductoOPCR() {
		return imProductoOPCR;
	}
	public void setImProductoOPCR(String imProductoOPCR) {
		this.imProductoOPCR = imProductoOPCR;
	}
	public String getImProPromOPCR() {
		return imProPromOPCR;
	}
	public void setImProPromOPCR(String imProPromOPCR) {
		this.imProPromOPCR = imProPromOPCR;
	}
    
    
    
}