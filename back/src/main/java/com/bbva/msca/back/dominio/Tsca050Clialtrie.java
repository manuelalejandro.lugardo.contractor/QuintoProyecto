package com.bbva.msca.back.dominio;

import java.math.BigDecimal;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;

@RooJavaBean
@RooToString
@RooEntity(versionField = "", table = "TSCA050_CLIALTRIE", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca050Clialtrie {

	@Id
	@NotNull
	@SequenceGenerator(name = "SQ_TSCA050", sequenceName = "SQ_TSCA050", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TSCA050")
	@Column(name = "CD_CLIALTRIE")
	private BigDecimal cdClialtrie;

	@Column(name = "TP_FTE_PRI")
	private String cdNumtipftePri;
	
	@Column(name = "TX_SOLICITUD_ESPED")
	private String nbSolicitudEspec2; 
	
	public String getNbSolicitudEspec2() {
		return nbSolicitudEspec2;
	}

	public void setNbSolicitudEspec2(String nbSolicitudEspec2) {
		this.nbSolicitudEspec2 = nbSolicitudEspec2;
	}	

}
