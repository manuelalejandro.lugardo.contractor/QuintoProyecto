package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import com.bbva.msca.back.dto.InfoAccionistasyEmpresaDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import com.bbva.msca.back.util.ConexionSIABD;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class InfoAccionistasyEmpresaDAO {
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	@SuppressWarnings("unchecked")
	public List<InfoAccionistasyEmpresaDTO> getInfoAccionistasyEmpresa(String cdSistema, String nuFolioAlerta) throws Exception{

		PreparedStatement stmt = new ConexionSIABD().getConnection().prepareStatement(Consultas.getConsulta("getInfoAccionistaEmpFolio"));
		
		List<InfoAccionistasyEmpresaDTO> lstInfoAccionistasyEmpresaDTO = new ArrayList<InfoAccionistasyEmpresaDTO>();

		 Query query = em.createNativeQuery(Consultas.getConsulta("getInfoAccionistaEmp"));
		 query.setParameter(1,cdSistema);
		 query.setParameter(2,nuFolioAlerta);
		 query.unwrap(org.hibernate.SQLQuery.class)

			.addScalar("nbCliente", StringType.INSTANCE)
	        .addScalar("nbParticipacion", StringType.INSTANCE)
	        .addScalar("nbEmpresa")
	        .addScalar("porParticipacion")
	      //  .addScalar("nuFolio", StringType.INSTANCE)
	      //  .addScalar("nbDecision", StringType.INSTANCE)
	        .addScalar("cdAccionista")

	        .setResultTransformer(Transformers.aliasToBean(InfoAccionistasyEmpresaDTO.class));
		
		 lstInfoAccionistasyEmpresaDTO = (List<InfoAccionistasyEmpresaDTO>)query.getResultList();


		 
		// Query queryFolio = em.createNativeQuery(Consultas.getConsulta("getInfoAccionistaEmpFolio"));
		/* queryFolio.setParameter(1,lstInfoAccionistasyEmpresaDTO.get(0).getCdAccionista().toString());
           queryFolio.unwrap(org.hibernate.SQLQuery.class)
	       .addScalar("nuFolio", StringType.INSTANCE)
	       .addScalar("nbDecision", StringType.INSTANCE)
	       .setResultTransformer(Transformers.aliasToBean(InfoAccionistasyEmpresaDTO.class));*/
		 ResponseGeneric response = new ResponseGeneric();	
		 try {
			 for(int index = 0; index < lstInfoAccionistasyEmpresaDTO.size(); index++){
			 stmt.setObject(1, lstInfoAccionistasyEmpresaDTO.get(index).getCdAccionista());
			 ResultSet rs = stmt.executeQuery();
			    while (rs.next()) {
				 lstInfoAccionistasyEmpresaDTO.get(index).setNuFolio(rs.getString("nuFolio"));
				 lstInfoAccionistasyEmpresaDTO.get(index).setNbDecision(rs.getString("nbDecision"));
		        }
			 }
		 }catch(Exception e){e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			
			for(int index = 0; index < lstInfoAccionistasyEmpresaDTO.size(); index++){
			 lstInfoAccionistasyEmpresaDTO.get(index).setNuFolio(" ");
			 lstInfoAccionistasyEmpresaDTO.get(index).setNbDecision(" ");
			}
		}finally {
		    if (stmt != null){ 
		    	stmt.close(); 
		    }
		 }
		 
		 //lstInfoAccionistasyEmpresaDTO = (List<InfoAccionistasyEmpresaDTO>)queryFolio.getResultList();
		// lstInfoAccionistasyEmpresaDTO.get(0).setNuFolio((String)queryFolio.getSingleResult());
		// lstInfoAccionistasyEmpresaDTO.get(0).setNbDecision((String)queryFolio.getSingleResult());
		 
		 

		 
	 
		return lstInfoAccionistasyEmpresaDTO;	
	}
	
	
	@SuppressWarnings("unchecked")
	public List<InfoAccionistasyEmpresaDTO> getAccionista(BigDecimal cdCaso) throws Exception{ 
	 	 List<InfoAccionistasyEmpresaDTO> datosAccionista = null;
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getAccionistasDetCaso"));
			 queryCR.setParameter(1,cdCaso); 
			 	 queryCR.unwrap(org.hibernate.SQLQuery.class)
		 		.addScalar("nbCliente")
		        .addScalar("nbParticipacion",StringType.INSTANCE)
		        .addScalar("nbEmpresa")
		        .addScalar("porParticipacion")
		        .addScalar("nuFolio", StringType.INSTANCE)
		        .addScalar("nbDecision", StringType.INSTANCE)
				
 			.setResultTransformer(Transformers.aliasToBean(InfoAccionistasyEmpresaDTO.class));
			
			 	datosAccionista = (List<InfoAccionistasyEmpresaDTO>)queryCR.getResultList();

		return datosAccionista;
		
} 
}
