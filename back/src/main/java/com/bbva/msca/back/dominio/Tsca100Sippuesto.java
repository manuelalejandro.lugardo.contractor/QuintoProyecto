package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(identifierType = Tsca100SippuestoPK.class, versionField = "", table = "TSCA100_SIPPUESTO", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca100Sippuesto {
}
