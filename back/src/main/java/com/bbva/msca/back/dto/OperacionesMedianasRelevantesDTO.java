package com.bbva.msca.back.dto;

import java.util.List;

public class OperacionesMedianasRelevantesDTO {

	private List<OperacionesRelevantesDTO> tablaRelevantes;
	private List<OperacionesMedianasDTO> tablaMedianas;
	
	
	public List<OperacionesRelevantesDTO> getTablaRelevantes() {
		return tablaRelevantes;
	}
	public void setTablaRelevantes(List<OperacionesRelevantesDTO> tablaRelevantes) {
		this.tablaRelevantes = tablaRelevantes;
	}
	
	
	public List<OperacionesMedianasDTO> getTablaMedianas() {
		return tablaMedianas;
	}
	public void setTablaMedianas(
			List<OperacionesMedianasDTO> list) {
		this.tablaMedianas = list;
	}



	

}
