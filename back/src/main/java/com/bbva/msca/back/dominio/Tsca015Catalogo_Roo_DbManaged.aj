// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca018DetCatalog;
import java.lang.String;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

privileged aspect Tsca015Catalogo_Roo_DbManaged {
    
    @OneToMany(mappedBy = "cdCatalogo")
    private Set<Tsca018DetCatalog> Tsca015Catalogo.tsca018DetCatalogs;
    
    @Column(name = "NB_CATALOGO", length = 60)
    @NotNull
    private String Tsca015Catalogo.nbCatalogo;
    
    public Set<Tsca018DetCatalog> Tsca015Catalogo.getTsca018DetCatalogs() {
        return this.tsca018DetCatalogs;
    }
    
    public void Tsca015Catalogo.setTsca018DetCatalogs(Set<Tsca018DetCatalog> tsca018DetCatalogs) {
        this.tsca018DetCatalogs = tsca018DetCatalogs;
    }
    
    public String Tsca015Catalogo.getNbCatalogo() {
        return this.nbCatalogo;
    }
    
    public void Tsca015Catalogo.setNbCatalogo(String nbCatalogo) {
        this.nbCatalogo = nbCatalogo;
    }
    
}
