package com.bbva.msca.back.dto;

import java.math.BigDecimal;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaI1DTO {

	@AnotacionAlerta(tipologia = "i1", idCampo = "imMontoAlerta", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_IMMONTOALERTA", formatter = "formatterMoney")
	private BigDecimal imMontoAlerta;
	@AnotacionAlerta(tipologia = "i1", idCampo = "nbCte", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")
	private String nbCte;
	@AnotacionAlerta(tipologia = "i1", idCampo = "nuOper", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_NUOPER")
	private BigDecimal nuOper;
	@AnotacionAlerta(tipologia = "i1", idCampo = "tpRiesgo", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_TPRIESGO", instanceType = "1")
	private String tpRiesgo;
	@AnotacionAlerta(tipologia = "i1", idCampo = "nbEntReveladora", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_NBENTREVELADORA", instanceType = "1")
	private String nbEntReveladora;
	@AnotacionAlerta(tipologia = "i1", idCampo = "nbEntReceptora", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_NBENTRECEPTORA", instanceType = "1")
	private String nbEntReceptora;
	@AnotacionAlerta(tipologia = "i1", idCampo = "tpOperacion", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_TPOPERACION", instanceType = "1")
	private String tpOperacion;
	@AnotacionAlerta(tipologia = "i1", idCampo = "tpInstMonetario", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_TPINTSMONETARIO", instanceType = "1")
	private String tpInstMonetario;
	@AnotacionAlerta(tipologia = "i1", idCampo = "txPeriodo", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_TXPERIODO", instanceType = "1")
	private String txPeriodo;
	@AnotacionAlerta(tipologia = "i1", idCampo = "txMotivoReporte", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_TXMOTIVOREPORTE", instanceType = "1")
	private String txMotivoReporte;
	
	public BigDecimal getImMontoAlerta() {
		return imMontoAlerta;
	}
	public void setImMontoAlerta(BigDecimal imMontoAlerta) {
		this.imMontoAlerta = imMontoAlerta;
	}
	public String getNbCte() {
		return nbCte;
	}
	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}
	public BigDecimal getNuOper() {
		return nuOper;
	}
	public void setNuOper(BigDecimal nuOper) {
		this.nuOper = nuOper;
	}
	public String getTpRiesgo() {
		return tpRiesgo;
	}
	public void setTpRiesgo(String tpRiesgo) {
		this.tpRiesgo = tpRiesgo;
	}
	public String getNbEntReveladora() {
		return nbEntReveladora;
	}
	public void setNbEntReveladora(String nbEntReveladora) {
		this.nbEntReveladora = nbEntReveladora;
	}
	public String getNbEntReceptora() {
		return nbEntReceptora;
	}
	public void setNbEntReceptora(String nbEntReceptora) {
		this.nbEntReceptora = nbEntReceptora;
	}
	public String getTpOperacion() {
		return tpOperacion;
	}
	public void setTpOperacion(String tpOperacion) {
		this.tpOperacion = tpOperacion;
	}
	public String getTpInstMonetario() {
		return tpInstMonetario;
	}
	public void setTpInstMonetario(String tpInstMonetario) {
		this.tpInstMonetario = tpInstMonetario;
	}
	public String getTxPeriodo() {
		return txPeriodo;
	}
	public void setTxPeriodo(String txPeriodo) {
		this.txPeriodo = txPeriodo;
	}
	public String getTxMotivoReporte() {
		return txMotivoReporte;
	}
	public void setTxMotivoReporte(String txMotivoReporte) {
		this.txMotivoReporte = txMotivoReporte;
	}
}
