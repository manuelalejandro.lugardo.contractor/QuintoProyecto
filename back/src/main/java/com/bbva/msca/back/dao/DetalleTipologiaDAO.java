package com.bbva.msca.back.dao;

import java.util.Date;
import com.bbva.msca.back.dto.DetalleTipologiaDTO;

public class DetalleTipologiaDAO {
	
	public static DetalleTipologiaDTO getDetalleTipologia(){
		
		DetalleTipologiaDTO detalleTipologiaDTO = new DetalleTipologiaDTO();
		
		detalleTipologiaDTO.setNuPeriodo(113);
		detalleTipologiaDTO.setNuConsecutivo(113);
		detalleTipologiaDTO.setNbAccion("");
		detalleTipologiaDTO.setNbActividad("");
		detalleTipologiaDTO.setNbTipoPersona("");
		detalleTipologiaDTO.setNbOficina("");
		detalleTipologiaDTO.setNbBanca("");
		detalleTipologiaDTO.setNuScore(113);
		detalleTipologiaDTO.settMovimiento("");
		detalleTipologiaDTO.setNbIndicador("");
		detalleTipologiaDTO.setNuScoreCorto(113);
		detalleTipologiaDTO.setNbIndicadorPEP("");
		detalleTipologiaDTO.setNbIndicadorC("");
		detalleTipologiaDTO.setNuAlertas(113);
		detalleTipologiaDTO.setImEfectivoR(123);
		detalleTipologiaDTO.setFhAlta(new Date());
		detalleTipologiaDTO.setNbActividadCliente("");
		detalleTipologiaDTO.setNbEnvInv("");
		detalleTipologiaDTO.setNuCalificacion(113);
		detalleTipologiaDTO.setNbIndZonAltRie("");
		
		return detalleTipologiaDTO;
	}

}
