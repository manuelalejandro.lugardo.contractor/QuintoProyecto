package com.bbva.msca.back.dto;

import java.util.Date;

public class DatosGeneralesDTO {
	
	private String nbCliente;
	private String nuCliente;
	private String nbCiudad;
	private String nbDomicilio;
	private String nbRFC;
	private String nbTelParticular;
	private String nbTelOficina;
	private String nbNacionalidad;
	private Date fhNacimiento;
	private String nbSector;
	private String nuAntiguedad;
	private String nbActividad;
	
	
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(String nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCiudad() {
		return nbCiudad;
	}
	public void setNbCiudad(String nbCiudad) {
		this.nbCiudad = nbCiudad;
	}
	public String getNbDomicilio() {
		return nbDomicilio;
	}
	public void setNbDomicilio(String nbDomicilio) {
		this.nbDomicilio = nbDomicilio;
	}
	public String getNbRFC() {
		return nbRFC;
	}
	public void setNbRFC(String nbRFC) {
		this.nbRFC = nbRFC;
	}
	public String getNbTelParticular() {
		return nbTelParticular;
	}
	public void setNbTelParticular(String nbTelParticular) {
		this.nbTelParticular = nbTelParticular;
	}
	public String getNbTelOficina() {
		return nbTelOficina;
	}
	public void setNbTelOficina(String nbTelOficina) {
		this.nbTelOficina = nbTelOficina;
	}
	public String getNbNacionalidad() {
		return nbNacionalidad;
	}
	public void setNbNacionalidad(String nbNacionalidad) {
		this.nbNacionalidad = nbNacionalidad;
	}
	public Date getFhNacimiento() {
		return fhNacimiento;
	}
	public void setFhNacimiento(Date fhNacimiento) {
		this.fhNacimiento = fhNacimiento;
	}
	public String getNbSector() {
		return nbSector;
	}
	public void setNbSector(String nbSector) {
		this.nbSector = nbSector;
	}
	public String getNuAntiguedad() {
		return nuAntiguedad;
	}
	public void setNuAntiguedad(String nuAntiguedad) {
		this.nuAntiguedad = nuAntiguedad;
	}
	public String getNbActividad() {
		return nbActividad;
	}
	public void setNbActividad(String nbActividad) {
		this.nbActividad = nbActividad;
	}


}
