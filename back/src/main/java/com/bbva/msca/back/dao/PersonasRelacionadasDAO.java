package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.bbva.msca.back.dto.PersonasRelacionadasDTO;

public class PersonasRelacionadasDAO {
	
	public List<PersonasRelacionadasDTO> getPersonasRelacionadas(){
		
		 List<PersonasRelacionadasDTO> lstPersonasRelacionadasDTO = new ArrayList<PersonasRelacionadasDTO>();
		
		 PersonasRelacionadasDTO personasRelacionadasDTO = null;
		
			for(int i=0;i<10;i++){
			
				personasRelacionadasDTO = new PersonasRelacionadasDTO();
				
				personasRelacionadasDTO.setNuCliente(1234);
				personasRelacionadasDTO.setNbCliente("");
				personasRelacionadasDTO.setNbTipRelacion("");
				personasRelacionadasDTO.setNbRFC("");
				personasRelacionadasDTO.setNuNIF(1234);
				personasRelacionadasDTO.setNbPaisNIF("");
				personasRelacionadasDTO.setNuFEA(1234);
				personasRelacionadasDTO.setNbSexo("");
				personasRelacionadasDTO.setFhNacConstitucion(new Date());
				personasRelacionadasDTO.setNbEntNacimiento("");
				personasRelacionadasDTO.setNbEntFedNacimiento("");
				personasRelacionadasDTO.setNbNacionalidad("");
				personasRelacionadasDTO.setNbOcupacion("");
				personasRelacionadasDTO.setNbCURP("");
				personasRelacionadasDTO.setNbCorreo("");
				personasRelacionadasDTO.setNuTelefono(1234);
				personasRelacionadasDTO.setNbCalle("");
				personasRelacionadasDTO.setNuExterior(1234);
				personasRelacionadasDTO.setNuInterior(1234);
				personasRelacionadasDTO.setNuCP(1234);
				personasRelacionadasDTO.setNbColonia("");
				personasRelacionadasDTO.setNbDelegacion("");
				personasRelacionadasDTO.setNbEntidad("");
				personasRelacionadasDTO.setNbPais("");
				
				lstPersonasRelacionadasDTO.add(personasRelacionadasDTO);
			
			}
			return lstPersonasRelacionadasDTO;
		}

}
