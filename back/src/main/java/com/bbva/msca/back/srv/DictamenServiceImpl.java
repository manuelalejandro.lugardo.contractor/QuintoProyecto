package com.bbva.msca.back.srv;

import java.math.BigDecimal;
import java.util.List;

import com.bbva.msca.back.dao.DictamenDAO;
import com.bbva.msca.back.dao.DictamenPreliminarDAO;
import com.bbva.msca.back.dto.AlertaDTO;
import com.bbva.msca.back.dto.DictamenCasoDTO;
import com.bbva.msca.back.dto.ParametrosBusquedaCasoDTO;

import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class DictamenServiceImpl implements DictamenService {
	
	
	
	//Dictamen Preeliminar insertar
	
	@Override
	public ResponseGeneric asignarDictamenPreliminar(BigDecimal cdCaso,DictamenCasoDTO dictamenCasoDTO ) {
		ResponseGeneric response = new ResponseGeneric();
		DictamenPreliminarDAO dictamenPreliminarDAO = new DictamenPreliminarDAO();
		
		
		try{
			response.setResponse(dictamenPreliminarDAO.addDictamenPreliminar(cdCaso, dictamenCasoDTO));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			
		}
		
		return response;
	}
	
	//Dictamen Preeliminar Consultar
	
	@Override
	public ResponseGeneric consultarDictamenPreliminar(BigDecimal cdCaso) {
		ResponseGeneric response = new ResponseGeneric();
		DictamenPreliminarDAO dictamenPreliminarDAO = new DictamenPreliminarDAO();
		DictamenCasoDTO lstDictamenPreliminarDTO = null;
		try {
			lstDictamenPreliminarDTO = dictamenPreliminarDAO.getConsultaDictamenPreliminar(cdCaso);
			response.setResponse(lstDictamenPreliminarDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric getConsultaDictamenesLiberados(String cdSesion, BigDecimal cdGerencia) {
		ResponseGeneric response = new ResponseGeneric();
		DictamenPreliminarDAO dictamenPreliminarDAO = new DictamenPreliminarDAO();
		DictamenCasoDTO dictamen = null;
		try {
			dictamen = dictamenPreliminarDAO.getConsultaDictamenesLiberados(cdSesion, cdGerencia);			
			response.setResponse(dictamen);
			if( dictamen != null ){
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));	
			}else
			{
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01008"));				
			}					
		} catch (Exception e) {
			response.setCdMensaje("0");			
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));			
		}
		return response;
	}
	
	//Dictamen Final Insertar
	
	@Override
	public ResponseGeneric asignarDictamenFinal(BigDecimal cdCaso,DictamenCasoDTO dictamenCasoDTO ) {
		ResponseGeneric response = new ResponseGeneric();
		DictamenPreliminarDAO dictamenPreliminarDAO = new DictamenPreliminarDAO();
		
		
		try{
			response.setResponse(dictamenPreliminarDAO.addDictamenFinal(cdCaso, dictamenCasoDTO));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			
		}
		
		return response;
	}

	@Override
	public ResponseGeneric consultarCasosDictamen(BigDecimal cdSupervisor) {
		ResponseGeneric response = new ResponseGeneric();
		DictamenDAO dictamenDAO = new DictamenDAO();
		
		try{
		List<AlertaDTO> lstAlertasDTO = dictamenDAO.getConsultaCasosDictamenSupervisor(cdSupervisor);
		response.setResponse(lstAlertasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			
		}
		return response;
	}

	@Override
	public ResponseGeneric consultarCasosDictamenFiltro(BigDecimal cdSupervisor, ParametrosBusquedaCasoDTO parametrosBusquedaCasoDTO) {
		ResponseGeneric response = new ResponseGeneric();
		DictamenDAO dictamenDAO = new DictamenDAO();
		
		try{
		List<AlertaDTO> lstAlertasDTO = dictamenDAO.getConsultaCasosDictamenSupervisor(parametrosBusquedaCasoDTO, cdSupervisor);
		response.setResponse(lstAlertasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));	
		}
		return response;
	}
	
	//Consultar Otras Gerencias para txtComentario
	@Override
	public ResponseGeneric consultarDictamenPreliminarTxtComentario(BigDecimal cdCaso) {
		ResponseGeneric response = new ResponseGeneric();
		DictamenPreliminarDAO dictamenPreliminarDAO = new DictamenPreliminarDAO();
		try {
			List<DictamenCasoDTO> lstDictamenPreliminarDTO = dictamenPreliminarDAO.getConsultaDictamenPreliminarTxtComentario(cdCaso);
			response.setResponse(lstDictamenPreliminarDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
}

