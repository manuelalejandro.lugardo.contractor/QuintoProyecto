package com.bbva.msca.back.dto;

public class RelacionClienteFiguraDTO {
	
	private String puestoPEP;
	private String nombrePEP;
	private String parentPEP;
	private String nombrePersRel;
	
	public String getPuestoPEP() {
		return puestoPEP;
	}
	public String getNombrePEP() {
		return nombrePEP;
	}
	public String getParentPEP() {
		return parentPEP;
	}
	public String getNombrePersRel() {
		return nombrePersRel;
	}
	public void setPuestoPEP(String puestoPEP) {
		this.puestoPEP = puestoPEP;
	}
	public void setNombrePEP(String nombrePEP) {
		this.nombrePEP = nombrePEP;
	}
	public void setParentPEP(String parentPEP) {
		this.parentPEP = parentPEP;
	}
	public void setNombrePersRel(String nombrePersRel) {
		this.nombrePersRel = nombrePersRel;
	}


	


		
}
