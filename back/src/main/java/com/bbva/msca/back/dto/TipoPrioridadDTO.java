package com.bbva.msca.back.dto;

public class TipoPrioridadDTO {

	private String cdNumtipftePri;
	private String cdStatus;
	private String cdPrioridad;
	private String nbTipoFuente;
	private String nbValor;
	private String cdUsuario;

	public void setCdNumtipftePri(String cdNumtipftePri) {
		this.cdNumtipftePri = cdNumtipftePri;
	}

	public String getCdNumtipftePri() {
		return cdNumtipftePri;
	}

	public void setCdStatus(String cdStatus) {
		this.cdStatus = cdStatus;
	}

	public String getCdStatus() {
		return cdStatus;
	}

	public void setCdPrioridad(String cdPrioridad) {
		this.cdPrioridad = cdPrioridad;
	}

	public String getCdPrioridad() {
		return cdPrioridad;
	}

	public void setNbTipoFuente(String nbTipoFuente) {
		this.nbTipoFuente = nbTipoFuente;
	}

	public String getNbTipoFuente() {
		return nbTipoFuente;
	}

	public void setNbValor(String nbValor) {
		this.nbValor = nbValor;
	}

	public String getNbValor() {
		return nbValor;
	}

	public void setCdUsuario(String cdUsuario) {
		this.cdUsuario = cdUsuario;
	}

	public String getCdUsuario() {
		return cdUsuario;
	}

}
