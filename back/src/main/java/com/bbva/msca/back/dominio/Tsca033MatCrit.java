package com.bbva.msca.back.dominio;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.SequenceGenerator;
import javax.validation.constraints.NotNull;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(table = "TSCA033_MAT_CRIT", schema = "GORAPR", versionField = "")
public class Tsca033MatCrit {
	
	@Id
	@NotNull
	@SequenceGenerator(name = "SQ_TSCA033", sequenceName = "SQ_TSCA033", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQ_TSCA033")
    @Column(name = "CD_MAT_CRIT")
    private BigDecimal cdMatCrit;

}
