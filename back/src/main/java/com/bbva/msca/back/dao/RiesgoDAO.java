package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import com.bbva.msca.back.dto.RiesgoDTO;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.stereotype.Component;

import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

@Component
public class RiesgoDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> getRiesgos() throws Exception{
		
		List<RiesgoDTO> lstRiesgoDTO =null;
		 Query query = em.createNativeQuery(Consultas.getConsulta("getRiesgos"));
		 query.unwrap(org.hibernate.SQLQuery.class)
		 	.addScalar("cdClialtrie")	
		 	.addScalar("cdTipo",StringType.INSTANCE)
			.addScalar("cdPrioridad")
			.addScalar("nbPrioridad")
			.addScalar("nbTipo")
			.addScalar("fhRecepcion",TimestampType.INSTANCE)
	        .addScalar("nuOficio")
	        .addScalar("nuExpediente")
	        .addScalar("nuFolio")
	        .addScalar("nbAutoridad")
	        .addScalar("nbDescripcion")
	        .addScalar("fhPublicacion",TimestampType.INSTANCE)
			.addScalar("nuDiasPlazo")
	        .addScalar("nbReferencia")
	        .addScalar("nbAseguramiento",StringType.INSTANCE)
	        .addScalar("nbSolicitud")
	        .addScalar("txtSolicitud")
			.setResultTransformer(Transformers.aliasToBean(RiesgoDTO.class));
		    lstRiesgoDTO = (List<RiesgoDTO>)query.getResultList();
			
			 return lstRiesgoDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> getRiesgos(RiesgoDTO riesgoDTO) throws Exception{
			
			List<RiesgoDTO> lstRiesgoDTO = null;
			String condicion = " WHERE ";
			 String consulta = Consultas.getConsulta("getRiesgos");
			 String filtro = "";
			 
			 boolean isFiltro = false;
			 SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
			 
			 if( riesgoDTO != null){
				 
				 if(riesgoDTO.getCdTipo() != null){
					 filtro = filtro + " CP.cdTipo = '"+riesgoDTO.getCdTipo() + "' ";
					 isFiltro = true;
					
				 } 
				 
				 if(riesgoDTO.getCdPrioridad() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.cdPrioridad = '"+riesgoDTO.getCdPrioridad() + "' ";
					 }else{
						 filtro = filtro + " AND CP.cdPrioridad = '"+riesgoDTO.getCdPrioridad() + "' ";
					 }
					 
					 isFiltro = true;
					 
				 }
				 if(riesgoDTO.getFhRecepcion() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhRecepcion = TO_DATE (' "  + formato.format(riesgoDTO.getFhRecepcion()) + "' ,'DD/MM/YYYY') ";
					 }else{
						 
						 filtro = filtro + " AND CP.fhRecepcion = TO_DATE (' "  + formato.format(riesgoDTO.getFhRecepcion()) + "' ,'DD/MM/YYYY') ";
					 }
					 
					 isFiltro = true;
				 }
				 
				 if(riesgoDTO.getFhPublicacion() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhPublicacion = TO_DATE (' "  + formato.format(riesgoDTO.getFhPublicacion()) + "' ,'DD/MM/YYYY') ";
					 }else{
						 
						 filtro = filtro + " AND CP.fhPublicacion = TO_DATE (' "  + formato.format(riesgoDTO.getFhPublicacion()) + "' ,'DD/MM/YYYY')";
					 }
					 
					 isFiltro = true;
				 }
				 
				 if(riesgoDTO.getNuOficio() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.nuOficio = '"  + riesgoDTO.getNuOficio().toUpperCase() + "' ";
					 }else{
						 
						 filtro = filtro + " AND CP.nuOficio = '"  + riesgoDTO.getNuOficio().toUpperCase() + "' ";
					 }
					 
					 isFiltro = true;
				 }
				 
				 if(riesgoDTO.getNuExpediente() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.nuExpediente = '"  + riesgoDTO.getNuExpediente().toUpperCase() + "' ";
					 }else{
						 
						 filtro = filtro + " AND CP.nuExpediente = '"  + riesgoDTO.getNuExpediente().toUpperCase() + "' ";
					 }
					 
					 isFiltro = true;
				 }
				 
				 if(riesgoDTO.getNuFolio() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.nuFolio = "  + riesgoDTO.getNuFolio();
					 }else{
						 
						 filtro = filtro + " AND CP.nuFolio = "  + riesgoDTO.getNuFolio();
					 }
					 
					 isFiltro = true;
				 }
				 
				 if(riesgoDTO.getNbReferencia() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.nbReferencia = '"  + riesgoDTO.getNbReferencia().toUpperCase() + "' ";
					 }else{
						 
						 filtro = filtro + " AND CP.nbReferencia = '"  + riesgoDTO.getNbReferencia().toUpperCase() + "' ";
					 }
					 
					 isFiltro = true;
				 }
				 if(riesgoDTO.getNbAutoridad() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.nbAutoridad = '"  + riesgoDTO.getNbAutoridad().toUpperCase() + "' ";
					 }else{
						 
						 filtro = filtro + " AND CP.nbAutoridad = '"  + riesgoDTO.getNbAutoridad().toUpperCase() + "' ";
					 }
					 
					 isFiltro = true;
				 }
				 
	if(riesgoDTO.getNuCuenta() != null || riesgoDTO.getCdCliente() != null || riesgoDTO.getNbCliente() != null || riesgoDTO.getCdTipologia() != null ){
		if(riesgoDTO.getNuCuenta() != null && riesgoDTO.getCdCliente() != null && riesgoDTO.getNbCliente() != null && riesgoDTO.getCdTipologia() != null ){	
			 if(filtro.equals("")){
				 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA = '" + riesgoDTO.getCdTipologia() + "' AND (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" +riesgoDTO.getNuCuenta() + "' ) AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
			 }else{
				 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA =  '" + riesgoDTO.getCdTipologia() + "' AND (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" +riesgoDTO.getNuCuenta() + "' ) AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
			  } 
			 }else{
				 if(riesgoDTO.getNuCuenta() != null && riesgoDTO.getCdCliente() != null && riesgoDTO.getCdTipologia() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA = '" + riesgoDTO.getCdTipologia() + "' AND (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '"+ riesgoDTO.getNuCuenta() + "') AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA =  '" + riesgoDTO.getCdTipologia() + "' AND (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '"+ riesgoDTO.getNuCuenta() + "') AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' )";
					  }
					 isFiltro = true;
				   } else
				 
				 if(riesgoDTO.getNuCuenta() != null && riesgoDTO.getNbCliente() != null && riesgoDTO.getCdTipologia() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA = '" + riesgoDTO.getCdTipologia() + "' AND (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" +riesgoDTO.getNuCuenta() + "') AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA =  '" + riesgoDTO.getCdTipologia() + "' AND (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" +riesgoDTO.getNuCuenta() + "') AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getCdCliente() != null && riesgoDTO.getNbCliente() != null && riesgoDTO.getCdTipologia() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA = '" + riesgoDTO.getCdTipologia() + "' AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA =  '" + riesgoDTO.getCdTipologia() + "' AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getCdCliente() != null && riesgoDTO.getNbCliente() != null && riesgoDTO.getNuCuenta() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" + riesgoDTO.getNuCuenta() +  "' ) AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE (SUBSTR(NU_CUENTA,11,20) =  '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" + riesgoDTO.getNuCuenta() + "' ) AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getCdTipologia() != null && riesgoDTO.getNuCuenta() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA = '" + riesgoDTO.getCdTipologia() + "' AND (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() +"' OR NU_CUENTA = '" + riesgoDTO.getNuCuenta() + "'))";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA =  '" + riesgoDTO.getCdTipologia() + "' AND (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() +"' OR NU_CUENTA = '" + riesgoDTO.getNuCuenta() + "'))";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getCdTipologia() != null && riesgoDTO.getCdCliente() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA = '" + riesgoDTO.getCdTipologia() + "' AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() +"' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA =  '" + riesgoDTO.getCdTipologia() + "' AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() +"' )";
					  }
					 isFiltro = true;
				   }else
				 
				 if(riesgoDTO.getCdTipologia() != null && riesgoDTO.getNbCliente() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA = '" + riesgoDTO.getCdTipologia() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA =  '" + riesgoDTO.getCdTipologia() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getNuCuenta() != null && riesgoDTO.getCdCliente() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" + riesgoDTO.getNuCuenta() + "') AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() +"' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" + riesgoDTO.getNuCuenta() + "') AND NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() +"' )";
					  }
					 isFiltro = true;
				   }else
				 
				 if(riesgoDTO.getNuCuenta() != null && riesgoDTO.getNbCliente() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '"  + riesgoDTO.getNuCuenta() + "' ) AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '"  + riesgoDTO.getNuCuenta() + "' ) AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getCdCliente() != null && riesgoDTO.getNbCliente() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE NU_CLIENTE =  '" + riesgoDTO.getCdCliente().toUpperCase() + "' AND NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getCdTipologia() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA = '" + riesgoDTO.getCdTipologia() + "' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE CD_TIPOLOGIA =  '" + riesgoDTO.getCdTipologia() + "' )";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getNuCuenta() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" + riesgoDTO.getNuCuenta() + "' ))";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE (SUBSTR(NU_CUENTA,11,20) = '" + riesgoDTO.getNuCuenta() + "' OR NU_CUENTA = '" + riesgoDTO.getNuCuenta() + "' ))";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getCdCliente() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE NU_CLIENTE = '" + riesgoDTO.getCdCliente().toUpperCase() + "' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE NU_CLIENTE =  '" + riesgoDTO.getCdCliente().toUpperCase() + "' )";
					  }
					 isFiltro = true;
				   }else
				 if(riesgoDTO.getNbCliente() != null ){
					 if(filtro.equals("")){
						 filtro = filtro + "CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					 }else{
						 filtro = filtro + " AND CP.cdClialtrie IN(SELECT T052SC.CD_CLIALTRIE FROM TSCA052_CLIALTRIEDET T052SC WHERE NB_NOMBRE || ' ' || NB_APE_PATERNO || ' ' ||  NB_APE_MATERNO LIKE '%" + riesgoDTO.getNbCliente().toUpperCase() + "%' )";
					  }
					 isFiltro = true;
				   }
			 
			 }
				isFiltro = true;
			 }			 
				 if(isFiltro){ 	
					consulta = consulta+condicion+filtro;
				 }
				
			 }
			 Query query = em.createNativeQuery(consulta);
	
			 query.unwrap(org.hibernate.SQLQuery.class)
			 		.addScalar("cdClialtrie")
					.addScalar("cdTipo",StringType.INSTANCE)
					 .addScalar("cdPrioridad")
					.addScalar("nbPrioridad")
					.addScalar("nbTipo")
					.addScalar("fhRecepcion",TimestampType.INSTANCE)
			        .addScalar("nuOficio")
			        .addScalar("nuExpediente")
			        .addScalar("nuFolio")
			        .addScalar("nbAutoridad")
			        .addScalar("nbDescripcion")
			        .addScalar("fhPublicacion",TimestampType.INSTANCE)
					.addScalar("nuDiasPlazo")
			        .addScalar("nbReferencia")
			        .addScalar("nbAseguramiento",StringType.INSTANCE)
			        .addScalar("nbSolicitud")
			        .addScalar("txtSolicitud")
			        .setResultTransformer(Transformers.aliasToBean(RiesgoDTO.class));
					
			 lstRiesgoDTO = (List<RiesgoDTO>)query.getResultList();
				
			
				return lstRiesgoDTO;
			}
	
	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> getPersonasReportados(BigDecimal cdClialtrie) throws Exception{
		
		List<RiesgoDTO> lstRiesgoDTO =null;
		 Query query = em.createNativeQuery(Consultas.getConsulta("getPersonaReportada"));
		 query.setParameter(1,cdClialtrie);

		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("nuID")
			.addScalar("cdClialtrie")
			.addScalar("cdCaracter")
			.addScalar("cdTPersona")
			.addScalar("nbTipoPersona")
			.addScalar("nbCliente")
	        .addScalar("nbApPaterno")
	        .addScalar("nbApMaterno")
	        .addScalar("fhNacimiento")
	        .addScalar("cdRFC")
	        .addScalar("cdTipologia",StringType.INSTANCE)
			.addScalar("nbDomicilio")
	        .addScalar("nbComplementarios")
	        .addScalar("cdClienteB",StringType.INSTANCE)
	        .addScalar("cdCliente",StringType.INSTANCE)
	        .addScalar("nuCuenta")
	        .addScalar("cdDictamen",StringType.INSTANCE)
			.setResultTransformer(Transformers.aliasToBean(RiesgoDTO.class));
		    lstRiesgoDTO = (List<RiesgoDTO>)query.getResultList();
			
			 return lstRiesgoDTO;
	}
	
	
	@SuppressWarnings({ "unchecked" })
	public RiesgoDTO getConsultarDatosPersonaReportada(String nuCuenta) throws Exception{

		RiesgoDTO riesgoDTO = null;
		
			 
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getConsultarDatosPersonaReportada"));
			 
			 queryCR.setParameter(1,nuCuenta);
			 queryCR.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("nbNombre")
				.addScalar("cdCliente",StringType.INSTANCE)
				.addScalar("nbDomicilio")
				.addScalar("cdRFC")
				.addScalar("nbPoblacion")
				.addScalar("nbTel1")
				.addScalar("nbTel2")
				.addScalar("cdPais")
				.addScalar("fhNacimiento", TimestampType.INSTANCE)
				.addScalar("cdNacionalidad")
				.addScalar("cdSector",StringType.INSTANCE)
				//.addScalar("fhAntNegoCR",StringType.INSTANCE)//mes dias a�os
				.addScalar("nbActa")
				//.addScalar("nuAntiguedad",TimestampType.INSTANCE)
	
				
			.setResultTransformer(Transformers.aliasToBean(RiesgoDTO.class));
			 
			 List<RiesgoDTO> lstRiesgoDTO = (List<RiesgoDTO>)queryCR.getResultList();
			
			 
			 riesgoDTO = lstRiesgoDTO.get( 0 );
			 return riesgoDTO;

	}
	
	@SuppressWarnings({ "unchecked" })
	public RiesgoDTO getConsultarDatosOficinaPersonaReportada(String cdCliente) throws Exception{

		RiesgoDTO riesgoDTO = null;
		
			 
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getConsultarDatosOficinaPersonaReportada"));
			 
			 queryCR.setParameter(1,cdCliente);
			 queryCR.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdOficina",StringType.INSTANCE)
				.addScalar("nbOficina")
				.addScalar("nbMercado")
				.addScalar("nbRiesgo")
				.addScalar("nbDivision")
				.addScalar("nbFuncionario")
			.setResultTransformer(Transformers.aliasToBean(RiesgoDTO.class));
			 
			 List<RiesgoDTO> lstRiesgoDTO = (List<RiesgoDTO>)queryCR.getResultList();
			
			 
			 riesgoDTO = lstRiesgoDTO.get( 0 );
			 return riesgoDTO;

	}
	
	@SuppressWarnings("unchecked")
	public List<RiesgoDTO> getConsultarOtrosClientesPersonaReportada(String cdCliente) throws Exception{
		
		List<RiesgoDTO> lstRiesgoDTO =null;
		Query query = em.createNativeQuery(Consultas.getConsulta("getConsultarOtrosClientesPersonaReportada"));
		 query.setParameter(1,cdCliente);

		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("cdCliente",StringType.INSTANCE)
			.addScalar("nbNombre")
			.addScalar("nbDomicilio")
			.addScalar("cdRFC")
			.addScalar("cdSector",StringType.INSTANCE)
			.setResultTransformer(Transformers.aliasToBean(RiesgoDTO.class));
		    lstRiesgoDTO = (List<RiesgoDTO>)query.getResultList();
			
			 return lstRiesgoDTO;
	}

}
