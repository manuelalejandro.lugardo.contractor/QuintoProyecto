package com.bbva.msca.back.dao;

import com.bbva.msca.back.dto.OtrosProductosClienteReportadoDTO;
import com.bbva.msca.back.dto.OtrosProductosClienteReportadoPatDTO;
import com.bbva.msca.back.dto.OtrosProductosClienteReportadoTarCreDTO;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class OtrosProductosClienteReportadoDAO {
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	@SuppressWarnings("unchecked")
	public OtrosProductosClienteReportadoDTO consultarOtrosProductosCR(String cdSistema, String nuFolioAlerta, BigDecimal cdCaso) throws Exception{
		
		OtrosProductosClienteReportadoDTO otrosProductosClienteReportadoDTO = new OtrosProductosClienteReportadoDTO();
		List<OtrosProductosClienteReportadoTarCreDTO> lstOtrosProductosClienteReportado = null;
		
		 Query query = em.createNativeQuery(Consultas.getConsulta("getOtrosProductosCR"));
		 query.setParameter(1,cdCaso);
		 query.setParameter(2,cdCaso);
		 query.setParameter(3,cdCaso);
		 //query.setParameter(4,cdCaso);
		 query.unwrap(org.hibernate.SQLQuery.class)

			.addScalar("fhAperturaOPCR", TimestampType.INSTANCE)
	        .addScalar("nuCuentaOPCR")
	        .addScalar("nbProductoOPCR")
	        .addScalar("nbSubproductoOPCR", StringType.INSTANCE)
	        .addScalar("nbEstatusOPCR", StringType.INSTANCE)
	        //.addScalar("fhCancelacionOPCR", StringType.INSTANCE)
	        .addScalar("nbTitularOPCR", StringType.INSTANCE)
	        .addScalar("nuCtePartOPCR", StringType.INSTANCE)
	        .addScalar("nbParticipesOPCR", StringType.INSTANCE)
	        .addScalar("imProductoOPCR", StringType.INSTANCE)
	        .addScalar("imProPromOPCR", StringType.INSTANCE)

	        .setResultTransformer(Transformers.aliasToBean(OtrosProductosClienteReportadoTarCreDTO.class));
		
		 lstOtrosProductosClienteReportado = (List<OtrosProductosClienteReportadoTarCreDTO>)query.getResultList();
		
		 /*
		 //11, 12, 14, 26, 27, 28, 29, 71 y 75 Integer.parseInt("1234");
		 String nuCtePartCredito = " "; 
		 int nuCtePartCreditoSub = 0;
		 for(int index=0; index < lstOtrosProductosClienteReportado.size(); index++){
			if(lstOtrosProductosClienteReportado.get(index).getNuCtePartOPCR() != null ){
			  nuCtePartCredito = lstOtrosProductosClienteReportado.get(index).getNuCtePartOPCR().toString();
			  nuCtePartCreditoSub = Integer.parseInt(nuCtePartCredito.substring(11, 13));
			 
				if(nuCtePartCreditoSub == 11){
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(nuCtePartCredito);
				}else if (nuCtePartCreditoSub == 12){
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(nuCtePartCredito);
				}else if (nuCtePartCreditoSub == 14){
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(nuCtePartCredito);
				}else if (nuCtePartCreditoSub == 26){
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(nuCtePartCredito);
				}else if (nuCtePartCreditoSub == 27){
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(nuCtePartCredito);
				}else if (nuCtePartCreditoSub == 28){
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(nuCtePartCredito);
				}else if (nuCtePartCreditoSub == 29){
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(nuCtePartCredito);
				}else if (nuCtePartCreditoSub == 71){
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(nuCtePartCredito);
				}else if (nuCtePartCreditoSub == 75){
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(nuCtePartCredito);
				}else{
					lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(" ");
				}
			}else{
				lstOtrosProductosClienteReportado.get(index).setNuCtePartOPCR(" ");
			}
		 }*/

		 otrosProductosClienteReportadoDTO.setTablaCredito(lstOtrosProductosClienteReportado);

		 
		 
		 
		List<OtrosProductosClienteReportadoPatDTO> lstOtrosProductosClienteReportadoP = null;
		
		 Query queryP = em.createNativeQuery(Consultas.getConsulta("getOtrosProductosCRPATRI"));
		 queryP.setParameter(1,cdCaso);
		 //queryP.setParameter(2,nuFolioAlerta);
		 queryP.unwrap(org.hibernate.SQLQuery.class)
		 	.addScalar("fhAperturaOPCRP", TimestampType.INSTANCE)
	        .addScalar("nuCuentaOPCRP")
	        .addScalar("nbProductoOPCRP")
	        .addScalar("nbEstatusOPCRP", StringType.INSTANCE)
	        .addScalar("nbTitularOPCRP", StringType.INSTANCE)
	        .addScalar("nuCtePartOPCRP", StringType.INSTANCE)
	        .addScalar("nbParticipesOPCRP", StringType.INSTANCE)
	        .addScalar("imProductoOPCRP", StringType.INSTANCE)
	        //.addScalar("nuCtaPartOPCRP")

		.setResultTransformer(Transformers.aliasToBean(OtrosProductosClienteReportadoPatDTO.class));
		
		 lstOtrosProductosClienteReportadoP = (List<OtrosProductosClienteReportadoPatDTO>)queryP.getResultList();
		 
		 // 21    Integer.parseInt("1234");
		 int nuCtePartPatriSub= 0;
		 String nuCtaPartPatrimonial = " ";
		 for(int index=0; index < lstOtrosProductosClienteReportadoP.size(); index++){
			if(lstOtrosProductosClienteReportadoP.get(index).getNuCtePartOPCRP() != null ){
				nuCtaPartPatrimonial = lstOtrosProductosClienteReportadoP.get(index).getNuCtaPartOPCRP().toString();
				nuCtePartPatriSub = Integer.parseInt(nuCtaPartPatrimonial.substring(11, 13));
			 if (nuCtePartPatriSub == 21){
				 lstOtrosProductosClienteReportadoP.get(index).getNuCtePartOPCRP();
				}else{
					lstOtrosProductosClienteReportadoP.get(index).setNuCtePartOPCRP("");
				}
			}else{
				lstOtrosProductosClienteReportadoP.get(index).setNuCtePartOPCRP("");
			}
		 }		
		otrosProductosClienteReportadoDTO.setTablaPatrimonial(lstOtrosProductosClienteReportadoP);
		 
		return otrosProductosClienteReportadoDTO;
	}
}