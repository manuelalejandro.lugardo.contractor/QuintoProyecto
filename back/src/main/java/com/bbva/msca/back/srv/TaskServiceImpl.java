package com.bbva.msca.back.srv;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.bbva.msca.back.dao.TaskDAO;

@Service
@EnableScheduling
public class TaskServiceImpl {

	@Autowired
	private TaskDAO taskDAO;
	
	@Value("${VAR.SICA.CRON}")
    private boolean CRON;
	
	@Value("${VAR.SICA.JOB.CTL}")
    private String JOB_CTL;
	
	@Value("${VAR.SICA.JOB.SP}")
    private String JOB_SP;
	
	@Scheduled(cron="${VAR.SICA.CRON.CTL}")
	public void cargaArchivosAlertas() {
		try {
			if(CRON){
				taskDAO.ejecutarBat(JOB_CTL);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Scheduled(cron="${VAR.SICA.CRON.SP}")
	public void alertaF1(){
		try{
			if(CRON){
				taskDAO.ejecutarBat(JOB_SP);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
}
