package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class TranferenciasInternacionalesDTO {

	private String numCuenta;
	private String tpTransaccion;
	private String cdPaisDestino;
	private String cdParaisoFiscal;
	private String nivelRiesgo;
	private BigDecimal numOperaciones;
	private BigDecimal montoTransacciones;
	private String anioProceso;
	
	private String numCuentaD;
	private String tpTransaccionD;
	private String cdPaisDestinoD;
	private String cdParaisoFiscalD;
	private String nivelRiesgoD;
	private BigDecimal numOperacionesD;
	private BigDecimal montoTransaccionesD;
	private String anioProcesoD;
	
	
	
	public String getNumCuentaD() {
		return numCuentaD;
	}

	public void setNumCuentaD(String numCuentaD) {
		this.numCuentaD = numCuentaD;
	}

	public String getTpTransaccionD() {
		return tpTransaccionD;
	}

	public void setTpTransaccionD(String tpTransaccionD) {
		this.tpTransaccionD = tpTransaccionD;
	}

	public String getCdPaisDestinoD() {
		return cdPaisDestinoD;
	}

	public void setCdPaisDestinoD(String cdPaisDestinoD) {
		this.cdPaisDestinoD = cdPaisDestinoD;
	}

	public String getCdParaisoFiscalD() {
		return cdParaisoFiscalD;
	}

	public void setCdParaisoFiscalD(String cdParaisoFiscalD) {
		this.cdParaisoFiscalD = cdParaisoFiscalD;
	}

	public String getNivelRiesgoD() {
		return nivelRiesgoD;
	}

	public void setNivelRiesgoD(String nivelRiesgoD) {
		this.nivelRiesgoD = nivelRiesgoD;
	}

	public BigDecimal getNumOperacionesD() {
		return numOperacionesD;
	}

	public void setNumOperacionesD(BigDecimal numOperacionesD) {
		this.numOperacionesD = numOperacionesD;
	}

	public BigDecimal getMontoTransaccionesD() {
		return montoTransaccionesD;
	}

	public void setMontoTransaccionesD(BigDecimal montoTransaccionesD) {
		this.montoTransaccionesD = montoTransaccionesD;
	}

	public String getAnioProcesoD() {
		return anioProcesoD;
	}

	public void setAnioProcesoD(String anioProcesoD) {
		this.anioProcesoD = anioProcesoD;
	}

	public String getNumCuenta() {
		return numCuenta;
	}

	public void setNumCuenta(String numCuenta) {
		this.numCuenta = numCuenta;
	}

	public String getTpTransaccion() {
		return tpTransaccion;
	}

	public void setTpTransaccion(String tpTransaccion) {
		this.tpTransaccion = tpTransaccion;
	}

	public String getCdPaisDestino() {
		return cdPaisDestino;
	}

	public void setCdPaisDestino(String cdPaisDestino) {
		this.cdPaisDestino = cdPaisDestino;
	}

	public String getCdParaisoFiscal() {
		return cdParaisoFiscal;
	}

	public void setCdParaisoFiscal(String cdParaisoFiscal) {
		this.cdParaisoFiscal = cdParaisoFiscal;
	}

	public String getNivelRiesgo() {
		return nivelRiesgo;
	}

	public void setNivelRiesgo(String nivelRiesgo) {
		this.nivelRiesgo = nivelRiesgo;
	}

	public BigDecimal getNumOperaciones() {
		return numOperaciones;
	}

	public void setNumOperaciones(BigDecimal numOperaciones) {
		this.numOperaciones = numOperaciones;
	}

	public BigDecimal getMontoTransacciones() {
		return montoTransacciones;
	}

	public void setMontoTransacciones(BigDecimal montoTransacciones) {
		this.montoTransacciones = montoTransacciones;
	}

	public String getAnioProceso() {
		return anioProceso;
	}

	public void setAnioProceso(String anioProceso) {
		this.anioProceso = anioProceso;
	}

}
