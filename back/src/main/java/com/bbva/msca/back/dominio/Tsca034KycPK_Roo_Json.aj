// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca034KycPK;
import flexjson.JSONDeserializer;
import flexjson.JSONSerializer;
import java.lang.String;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

privileged aspect Tsca034KycPK_Roo_Json {
    
    public String Tsca034KycPK.toJson() {
        return new JSONSerializer().exclude("*.class").serialize(this);
    }
    
    public static Tsca034KycPK Tsca034KycPK.fromJsonToTsca034KycPK(String json) {
        return new JSONDeserializer<Tsca034KycPK>().use(null, Tsca034KycPK.class).deserialize(json);
    }
    
    public static String Tsca034KycPK.toJsonArray(Collection<Tsca034KycPK> collection) {
        return new JSONSerializer().exclude("*.class").serialize(collection);
    }
    
    public static Collection<Tsca034KycPK> Tsca034KycPK.fromJsonArrayToTsca034KycPKs(String json) {
        return new JSONDeserializer<List<Tsca034KycPK>>().use(null, ArrayList.class).use("values", Tsca034KycPK.class).deserialize(json);
    }
    
}
