package com.bbva.msca.back.dao;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.dominio.Tsca023Sesion;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dto.SesionDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

public class SesionDAO {

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	/**
	 * Bitacora de movimientos Sesion - Tipologia
	 * */
	private Tsca055Log log;

	@SuppressWarnings("unchecked")
	public List<SesionDTO> getSesiones() throws Exception {
		List<SesionDTO> lstSesionDTO = new ArrayList<SesionDTO>();
		Query query = em.createNativeQuery(Consultas
				.getConsulta("getSesionesParametria"));
		query.setParameter(1, Constantes.CD_ST_SIS_A);
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdSesion",StringType.INSTANCE)
				.addScalar("fhPeIn", TimestampType.INSTANCE)
				.addScalar("fhPeFn", TimestampType.INSTANCE)
				.addScalar("cdUsrAlta",StringType.INSTANCE)
				.addScalar("fhAlta", TimestampType.INSTANCE)
				.addScalar("cdStSistema",StringType.INSTANCE)
				.addScalar("fhLimRepAut", TimestampType.INSTANCE)
				.addScalar("nbUsrRed",StringType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(SesionDTO.class));
		lstSesionDTO = (List<SesionDTO>) query.getResultList();
		return lstSesionDTO;
	}
	
	@Transactional
	public int creaSesion(SesionDTO sesion) throws Exception{
		int op = Constantes.FIN_ER;
		Tsca023Sesion nuevaSesion =  Tsca023Sesion.findTsca023Sesion(sesion.getCdSesion());
		if(nuevaSesion != null){
			if(nuevaSesion.getStSistema().equals(Constantes.CD_ST_SIS_I)){
				this.copiaABC(nuevaSesion, sesion);
				nuevaSesion.merge();
				nuevaSesion.flush();
				this.log = this.generaLog(nuevaSesion, 1);
				this.log.persist();
				this.log.flush();
				op = Constantes.FIN_OK;
			}else{
				op = Constantes.FIN_AV;
			}
		}else{
			nuevaSesion = new Tsca023Sesion();
			this.copiaABC(nuevaSesion, sesion);
			nuevaSesion.persist();
			nuevaSesion.flush();
			this.log = this.generaLog(nuevaSesion, 1);
			this.log.persist();
			this.log.flush();
			op = Constantes.FIN_OK;
		}
		return op;
	}
	
	@Transactional
	public SesionDTO modificaSesion(SesionDTO sesion) throws Exception{
		Tsca023Sesion nuevaSesion = new Tsca023Sesion();
		this.copiaABC(nuevaSesion, sesion);
		nuevaSesion.merge();
		nuevaSesion.flush();
		this.log = this.generaLog(nuevaSesion, 2);
		this.log.persist();
		this.log.flush();
		return sesion;
	}
	
	private void copiaABC(Tsca023Sesion nuevaSesion, SesionDTO sesion){
		nuevaSesion.setCdSesion(sesion.getCdSesion());
		nuevaSesion.setFhPeIn(sesion.getFhPeIn());
		nuevaSesion.setFhPeFn(sesion.getFhPeFn());
		nuevaSesion.setFhAlta(sesion.getFhAlta());
		nuevaSesion.setCdUsrAlta(sesion.getCdUsrAlta());
		nuevaSesion.setStSistema(sesion.getCdStSistema());
		nuevaSesion.setFhLimRepAut(sesion.getFhLimRepAut());
	}
	
	private Tsca055Log generaLog(Tsca023Sesion sesion, int accion){
		Tsca055Log logSesion = new Tsca055Log();
		logSesion.setNbEvento("MSCAVE01012_1");
		logSesion.setCdUsuario(sesion.getCdUsrAlta());
		logSesion.setFhFecha(new GregorianCalendar().getTime());
		switch(accion){
		case 1:
			logSesion.setNbAccionRealizad("Alta: " + sesion.getCdSesion().trim());
			break;
		case 2:		
			logSesion.setNbAccionRealizad("Modifica: " + sesion.getCdSesion().trim());
			break;
		}
		return logSesion;
	}

}
