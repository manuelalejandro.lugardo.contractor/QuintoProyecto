package com.bbva.msca.back.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.dominio.Tsca053Reltipfte;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dto.TipoPrioridadDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

public class TipoPrioridadDAO {

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	/**
	 * Bitacora de movimientos Sesion - Tipologia
	 * */
	private Tsca055Log log;

	@SuppressWarnings("unchecked")
	public List<TipoPrioridadDTO> getTipoPrioridades() throws Exception {
		List<TipoPrioridadDTO> lstTipoPrioridadDTO = new ArrayList<TipoPrioridadDTO>();
		Query query = em.createNativeQuery(Consultas
				.getConsulta("getRelacionesTipoPrioridadParametria"));
		query.setParameter(1, Constantes.CD_ST_SIS_A);
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdNumtipftePri", StringType.INSTANCE)
				.addScalar("cdPrioridad", StringType.INSTANCE)
				.addScalar("cdStatus", StringType.INSTANCE)
				.addScalar("nbTipoFuente", StringType.INSTANCE)
				.addScalar("nbValor", StringType.INSTANCE)
				.setResultTransformer(
						Transformers.aliasToBean(TipoPrioridadDTO.class));
		lstTipoPrioridadDTO = (List<TipoPrioridadDTO>) query.getResultList();
		return lstTipoPrioridadDTO;
	}

	@Transactional
	public int creaTipoPrioridades(TipoPrioridadDTO relTpPrio) throws Exception {
		int op = Constantes.FIN_ER;
		Tsca053Reltipfte nuevaRelTpPrio = Tsca053Reltipfte.findTsca053Reltipfte(relTpPrio.getCdNumtipftePri());
		if (nuevaRelTpPrio != null) {
			if (nuevaRelTpPrio.getStFuente().equals(Constantes.CD_ST_SIS_I)) {
				this.copiaCaracDTO(nuevaRelTpPrio, relTpPrio);
				nuevaRelTpPrio.merge();
				nuevaRelTpPrio.flush();
				this.log = this.generaLog(relTpPrio, 1);
				this.log.persist();
				this.log.flush();
				op = Constantes.FIN_OK;
			} else {
				op = Constantes.FIN_AV;
			}
		} else {
			nuevaRelTpPrio = new Tsca053Reltipfte();
			relTpPrio.setCdNumtipftePri(new SimpleDateFormat("ddHHmmss")
					.format(new GregorianCalendar().getTime()));
			this.copiaCaracDTO(nuevaRelTpPrio, relTpPrio);
			nuevaRelTpPrio.persist();
			nuevaRelTpPrio.flush();
			this.log = this.generaLog(relTpPrio, 1);
			this.log.persist();
			this.log.flush();
			op = Constantes.FIN_OK;
		}
		return op;
	}

	@Transactional
	public TipoPrioridadDTO modificaTipoPrioridades(TipoPrioridadDTO relTpPrio)
			throws Exception {
		Tsca053Reltipfte nuevaRelTpPrio = new Tsca053Reltipfte();
		this.copiaCaracDTO(nuevaRelTpPrio, relTpPrio);
		nuevaRelTpPrio.merge();
		nuevaRelTpPrio.flush();
		this.log = this.generaLog(relTpPrio, 2);
		this.log.persist();
		this.log.flush();
		return relTpPrio;
	}

	private void copiaCaracDTO(Tsca053Reltipfte nuevaRelTpPrio,
			TipoPrioridadDTO relTpPrio) {
		nuevaRelTpPrio.setCdNumtipftePri(relTpPrio.getCdNumtipftePri());
		nuevaRelTpPrio.setCdPrioridad(relTpPrio.getCdPrioridad());
		nuevaRelTpPrio.setStFuente(relTpPrio.getCdStatus());
		nuevaRelTpPrio.setTpFuente(relTpPrio.getNbTipoFuente());
		
	}

	@SuppressWarnings("unchecked")
	public TipoPrioridadDTO getPrioridadPorTipo(String tipoDoc)
			throws Exception {

		List<TipoPrioridadDTO> resultado = new ArrayList<TipoPrioridadDTO>();
		TipoPrioridadDTO tipoPrioridadDTO = new TipoPrioridadDTO();

		Query query = em.createNativeQuery(Consultas
				.getConsulta("getPrioridadPorTipo"));
		query.setParameter(1, Constantes.CD_ST_SIS_A);
		query.setParameter(2, tipoDoc);

		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdPrioridad", StringType.INSTANCE)
				.addScalar("nbValor", StringType.INSTANCE)
				.setResultTransformer(
						Transformers.aliasToBean(TipoPrioridadDTO.class));

		resultado = (List<TipoPrioridadDTO>) query.getResultList();
		if( resultado != null && !resultado.isEmpty()){
			tipoPrioridadDTO = resultado.get( 0 );	
		}
		

		return tipoPrioridadDTO;
	}

	private Tsca055Log generaLog(TipoPrioridadDTO tipoPrioridad, int accion) {
		Tsca055Log logSesion = new Tsca055Log();
		logSesion.setNbEvento("MSCAVE01012_7");
		logSesion.setCdUsuario(tipoPrioridad.getCdUsuario());
		logSesion.setFhFecha(new GregorianCalendar().getTime());
		switch (accion) {
		case 1:
			logSesion.setNbAccionRealizad("Alta: "
					+ tipoPrioridad.getCdNumtipftePri());
			break;
		case 2:
			logSesion.setNbAccionRealizad("Modifica: "
					+ tipoPrioridad.getCdNumtipftePri());
			break;
		}
		return logSesion;
	}

}
