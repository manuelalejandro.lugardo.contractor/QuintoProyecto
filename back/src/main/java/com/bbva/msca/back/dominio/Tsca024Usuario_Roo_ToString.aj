// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import java.lang.String;

privileged aspect Tsca024Usuario_Roo_ToString {
    
    public String Tsca024Usuario.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CdGerencia: ").append(getCdGerencia()).append(", ");
        sb.append("CdPerfilA: ").append(getCdPerfilA()).append(", ");
        sb.append("CdPerfilB: ").append(getCdPerfilB()).append(", ");
        sb.append("CdSupervisor: ").append(getCdSupervisor()).append(", ");
        sb.append("CdUsuario: ").append(getCdUsuario()).append(", ");
        sb.append("NbInicial: ").append(getNbInicial()).append(", ");
        sb.append("NbUsrRed: ").append(getNbUsrRed()).append(", ");
        sb.append("StSistema: ").append(getStSistema()).append(", ");
        sb.append("Tsca007Tipologias: ").append(getTsca007Tipologias() == null ? "null" : getTsca007Tipologias().size());
        return sb.toString();
    }
    
}
