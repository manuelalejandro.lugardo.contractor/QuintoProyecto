package com.bbva.msca.back.dto;

import java.util.Date;

public class BusquedaCheckListDTO {
	
	private Integer nuOficio;
	private Date fhRecepcion;
	private Integer cdTipologia;
	
	public Integer getNuOficio() {
		return nuOficio;
	}
	public void setNuOficio(Integer nuOficio) {
		this.nuOficio = nuOficio;
	}
	public Date getFhRecepcion() {
		return fhRecepcion;
	}
	public void setFhRecepcion(Date fhRecepcion) {
		this.fhRecepcion = fhRecepcion;
	}
	public Integer getCdTipologia() {
		return cdTipologia;
	}
	public void setCdTipologia(Integer cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	
	
	
	

}
