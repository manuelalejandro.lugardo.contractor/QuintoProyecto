package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.bbva.msca.back.dto.FideicomisoDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;

public class FideicomisoDAO {
	
	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();
	
	public List<FideicomisoDTO> getFideicomiso(){
		
		 List<FideicomisoDTO> lstFideicomisoDTO = new ArrayList<FideicomisoDTO>();
		
		 FideicomisoDTO fideicomisoDTO = null;
		
			for(int i=0;i<10;i++){
			
				fideicomisoDTO = new FideicomisoDTO();
				
				fideicomisoDTO.setNuReferencia("");
				fideicomisoDTO.setNbFinalidad("");
				fideicomisoDTO.setNbLugConstitucion("");
				fideicomisoDTO.setFhConstitucion(new Date());
				fideicomisoDTO.setNbInsFiduciaria("");
				fideicomisoDTO.setNbPatFideicomitido("");
				fideicomisoDTO.setImAportaciones("");		
				
				
				lstFideicomisoDTO.add(fideicomisoDTO);
			
			}
			return lstFideicomisoDTO;
		}
	
	
	public static FideicomisoDTO getDetalleFideicomiso(){
		
		FideicomisoDTO detalleFideicomisoDTO = new FideicomisoDTO();
		
		detalleFideicomisoDTO.setNuReferencia("");
		detalleFideicomisoDTO.setNbFinalidad("");
		detalleFideicomisoDTO.setNbLugConstitucion("");
		detalleFideicomisoDTO.setFhConstitucion(new Date());
		detalleFideicomisoDTO.setNbInsFiduciaria("");
		detalleFideicomisoDTO.setNbPatFideicomitido("");
		detalleFideicomisoDTO.setImAportaciones("");
		
		
		
		return detalleFideicomisoDTO;
	}
	@SuppressWarnings("unchecked")
	public List<FideicomisoDTO> getfideicomiso(BigDecimal cdCaso) throws Exception{ 
	 	 List<FideicomisoDTO> datosFideicomiso = null;
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getFideicomisoCaso"));
			 queryCR.setParameter(1,cdCaso); 
			 	 queryCR.unwrap(org.hibernate.SQLQuery.class)
		 		.addScalar("nuReferencia")
		 		.addScalar("nbFinalidad")
		 		.addScalar("nbLugConstitucion",StringType.INSTANCE)
		 		.addScalar("fhConstitucion")
		 		.addScalar("nbInsFiduciaria",StringType.INSTANCE) 
		        .addScalar("nbPatFideicomitido")
		         .addScalar("imAportaciones",StringType.INSTANCE)
		        .addScalar("nuFideicomiso") 
 			.setResultTransformer(Transformers.aliasToBean(FideicomisoDTO.class));
			
			 	datosFideicomiso = (List<FideicomisoDTO>)queryCR.getResultList();

		return datosFideicomiso;
		}  
	
	@SuppressWarnings("unchecked")
	public List<FideicomisoDTO> getDetalleFideicomiso(BigDecimal cdCaso) throws Exception{ 
	 	 List<FideicomisoDTO> datosDetalleFideicomiso = null;
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getDetalleFideicomisoCaso"));
			 queryCR.setParameter(1,cdCaso); 
			 	 queryCR.unwrap(org.hibernate.SQLQuery.class)
		 		.addScalar("cdCliente",StringType.INSTANCE)
		 		.addScalar("nbCliente")
		 		.addScalar("tpRelacion",StringType.INSTANCE)
		 		.addScalar("cdRFC")
		 		.addScalar("nbClienteDet") 
		        .addScalar("nuFiscal")
		         .addScalar("cdPais")
		        .addScalar("nuFea") 
		        .addScalar("cdSexo",StringType.INSTANCE)
		 		.addScalar("fhNacimiento")
		 		.addScalar("cdEntidad",StringType.INSTANCE)
		        .addScalar("cdNacionalidad",StringType.INSTANCE)
		         .addScalar("cdOcupacion")
		        .addScalar("cdCurp") 
		        .addScalar("cdRFCDet")
		 		.addScalar("cdCorreo",StringType.INSTANCE)
		 		.addScalar("cdTelefono",StringType.INSTANCE)
		        .addScalar("nbCalle")
		         .addScalar("nuExterior")
		        .addScalar("nuInterior") 
		        .addScalar("cdPostal")
		 		.addScalar("nbColonia",StringType.INSTANCE)
		 		.addScalar("nbDelegacion",StringType.INSTANCE)
		 		.addScalar("nbEntidad")
		 		.addScalar("cdPaisDet") 
 			.setResultTransformer(Transformers.aliasToBean(FideicomisoDTO.class));
			
			 	datosDetalleFideicomiso = (List<FideicomisoDTO>)queryCR.getResultList();

		return datosDetalleFideicomiso;
		}  

}
