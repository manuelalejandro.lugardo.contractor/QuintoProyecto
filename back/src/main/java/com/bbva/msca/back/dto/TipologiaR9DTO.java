package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaR9DTO {

	@AnotacionAlerta(tipologia = "r9", idCampo = "imMontoAlerta", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_IMMONTOALERTA", formatter = "formatterMoney")
	private BigDecimal imMontoAlerta;
	@AnotacionAlerta(tipologia = "r9", idCampo = "nbCte", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_NBCTE", instanceType = "1")
	private String nbCte;
	@AnotacionAlerta(tipologia = "r9", idCampo = "nuOper", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_NUOPER")
	private BigDecimal nuOper;
	@AnotacionAlerta(tipologia = "r9", idCampo = "cdSector", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_CDSECTOR", instanceType = "1")
	private String cdSector;
	@AnotacionAlerta(tipologia = "r9", idCampo = "fhApertura", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_FHAPERTURA", instanceType = "2", formatter = "formatterDate")
	private Date fhApertura;
	@AnotacionAlerta(tipologia = "r9", idCampo = "nbSucApertura", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_NBSUCAPERTURA", instanceType = "1")
	private String nbSucApertura;
	@AnotacionAlerta(tipologia = "r9", idCampo = "nbSucApertura", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_NBSUCURSAL", instanceType = "1")
	private String nbSucursal;
	@AnotacionAlerta(tipologia = "r9", idCampo = "cdCentroSup", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_CDCENTROSUP", instanceType = "1")
	private String cdCentroSup;
	@AnotacionAlerta(tipologia = "r9", idCampo = "nbDivision", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_NBDIVISION", instanceType = "1")
	private String nbDivision;
	@AnotacionAlerta(tipologia = "r9", idCampo = "nbEstado", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_NBESTADO", instanceType = "1")
	private String nbEstado;
	@AnotacionAlerta(tipologia = "r9", idCampo = "nbActividad", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_TPACTIVIDAD", instanceType = "1")
	private String nbActividad;
	

	public BigDecimal getImMontoAlerta() {
		return imMontoAlerta;
	}
	public void setImMontoAlerta(BigDecimal imMontoAlerta) {
		this.imMontoAlerta = imMontoAlerta;
	}
	public String getNbCte() {
		return nbCte;
	}
	public void setNbCte(String nbCte) {
		this.nbCte = nbCte;
	}
	public BigDecimal getNuOper() {
		return nuOper;
	}
	public void setNuOper(BigDecimal nuOper) {
		this.nuOper = nuOper;
	}
	public String getCdSector() {
		return cdSector;
	}
	public void setCdSector(String cdSector) {
		this.cdSector = cdSector;
	}
	public Date getFhApertura() {
		return fhApertura;
	}
	public void setFhApertura(Date fhApertura) {
		this.fhApertura = fhApertura;
	}
	public String getNbSucApertura() {
		return nbSucApertura;
	}
	public void setNbSucApertura(String nbSucApertura) {
		this.nbSucApertura = nbSucApertura;
	}
	public String getNbSucursal() {
		return nbSucursal;
	}
	public void setNbSucursal(String nbSucursal) {
		this.nbSucursal = nbSucursal;
	}
	public String getCdCentroSup() {
		return cdCentroSup;
	}
	public void setCdCentroSup(String cdCentroSup) {
		this.cdCentroSup = cdCentroSup;
	}
	public String getNbDivision() {
		return nbDivision;
	}
	public void setNbDivision(String nbDivision) {
		this.nbDivision = nbDivision;
	}
	public String getNbEstado() {
		return nbEstado;
	}
	public void setNbEstado(String nbEstado) {
		this.nbEstado = nbEstado;
	}
	public String getNbActividad() {
		return nbActividad;
	}
	public void setNbActividad(String nbActividad) {
		this.nbActividad = nbActividad;
	}
}
