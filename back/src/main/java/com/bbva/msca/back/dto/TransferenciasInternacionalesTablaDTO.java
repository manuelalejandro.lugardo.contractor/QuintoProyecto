package com.bbva.msca.back.dto;

import java.math.BigDecimal;

public class TransferenciasInternacionalesTablaDTO {

	private String paisDestino;
	private String riesgo;
	private String paraisoFiscal;
	private BigDecimal montoAnioUno;
	private BigDecimal numOperAnioUno;
	private BigDecimal montoAnioDos;
	private BigDecimal numOperAnioDos;
	private BigDecimal montoAnioTres;
	private BigDecimal numOperAnioTres;
	private BigDecimal montoAnios;
	private BigDecimal totalAnios;
	
	private String paisDestinoD;
	private String riesgoD;
	private String paraisoFiscalD;
	private BigDecimal montoAnioUnoD;
	private BigDecimal numOperAnioUnoD;
	private BigDecimal montoAnioDosD;
	private BigDecimal numOperAnioDosD;
	private BigDecimal montoAnioTresD;
	private BigDecimal numOperAnioTresD;
	private BigDecimal montoAniosD;
	private BigDecimal totalAniosD;
	
	

	public String getPaisDestinoD() {
		return paisDestinoD;
	}

	public void setPaisDestinoD(String paisDestinoD) {
		this.paisDestinoD = paisDestinoD;
	}

	public String getRiesgoD() {
		return riesgoD;
	}

	public void setRiesgoD(String riesgoD) {
		this.riesgoD = riesgoD;
	}

	public String getParaisoFiscalD() {
		return paraisoFiscalD;
	}

	public void setParaisoFiscalD(String paraisoFiscalD) {
		this.paraisoFiscalD = paraisoFiscalD;
	}

	public BigDecimal getMontoAnioUnoD() {
		return montoAnioUnoD;
	}

	public void setMontoAnioUnoD(BigDecimal montoAnioUnoD) {
		this.montoAnioUnoD = montoAnioUnoD;
	}

	public BigDecimal getNumOperAnioUnoD() {
		return numOperAnioUnoD;
	}

	public void setNumOperAnioUnoD(BigDecimal numOperAnioUnoD) {
		this.numOperAnioUnoD = numOperAnioUnoD;
	}

	public BigDecimal getMontoAnioDosD() {
		return montoAnioDosD;
	}

	public void setMontoAnioDosD(BigDecimal montoAnioDosD) {
		this.montoAnioDosD = montoAnioDosD;
	}

	public BigDecimal getNumOperAnioDosD() {
		return numOperAnioDosD;
	}

	public void setNumOperAnioDosD(BigDecimal numOperAnioDosD) {
		this.numOperAnioDosD = numOperAnioDosD;
	}

	public BigDecimal getMontoAnioTresD() {
		return montoAnioTresD;
	}

	public void setMontoAnioTresD(BigDecimal montoAnioTresD) {
		this.montoAnioTresD = montoAnioTresD;
	}

	public BigDecimal getNumOperAnioTresD() {
		return numOperAnioTresD;
	}

	public void setNumOperAnioTresD(BigDecimal numOperAnioTresD) {
		this.numOperAnioTresD = numOperAnioTresD;
	}

	public BigDecimal getMontoAniosD() {
		return montoAniosD;
	}

	public void setMontoAniosD(BigDecimal montoAniosD) {
		this.montoAniosD = montoAniosD;
	}

	public BigDecimal getTotalAniosD() {
		return totalAniosD;
	}

	public void setTotalAniosD(BigDecimal totalAniosD) {
		this.totalAniosD = totalAniosD;
	}

	public String getPaisDestino() {
		return paisDestino;
	}

	public void setPaisDestino(String paisDestino) {
		this.paisDestino = paisDestino;
	}

	public String getRiesgo() {
		return riesgo;
	}

	public void setRiesgo(String riesgo) {
		this.riesgo = riesgo;
	}

	public String getParaisoFiscal() {
		return paraisoFiscal;
	}

	public void setParaisoFiscal(String paraisoFiscal) {
		this.paraisoFiscal = paraisoFiscal;
	}

	public BigDecimal getMontoAnioUno() {
		return montoAnioUno;
	}

	public void setMontoAnioUno(BigDecimal montoAnioUno) {
		this.montoAnioUno = montoAnioUno;
	}

	public BigDecimal getNumOperAnioUno() {
		return numOperAnioUno;
	}

	public void setNumOperAnioUno(BigDecimal numOperAnioUno) {
		this.numOperAnioUno = numOperAnioUno;
	}

	public BigDecimal getMontoAnioDos() {
		return montoAnioDos;
	}

	public void setMontoAnioDos(BigDecimal montoAnioDos) {
		this.montoAnioDos = montoAnioDos;
	}

	public BigDecimal getNumOperAnioDos() {
		return numOperAnioDos;
	}

	public void setNumOperAnioDos(BigDecimal numOperAnioDos) {
		this.numOperAnioDos = numOperAnioDos;
	}

	public BigDecimal getMontoAnioTres() {
		return montoAnioTres;
	}

	public void setMontoAnioTres(BigDecimal montoAnioTres) {
		this.montoAnioTres = montoAnioTres;
	}

	public BigDecimal getNumOperAnioTres() {
		return numOperAnioTres;
	}

	public void setNumOperAnioTres(BigDecimal numOperAnioTres) {
		this.numOperAnioTres = numOperAnioTres;
	}
	
	public BigDecimal getMontoAnios() {
		return montoAnios;
	}

	public void setMontoAnios(BigDecimal montoAnios) {
		this.montoAnios = montoAnios;
	}

	public BigDecimal getTotalAnios() {
		return totalAnios;
	}

	public void setTotalAnios(BigDecimal totalAnios) {
		this.totalAnios = totalAnios;
	}

	public String toString(){
		StringBuilder str = new StringBuilder();
		str.append( "\npaisDestino : " + paisDestino);
		str.append( "\nriesgo : " + riesgo);
		str.append( "\nparaisoFiscal : " + paraisoFiscal );
		str.append( "\nmontoAnioUno : " + montoAnioUno );
		str.append( "\nnumOperAnioUno : " + numOperAnioUno );
		str.append( "\nmontoAnioDos : " + montoAnioDos );
		str.append( "\nnumOperAnioDos : " + numOperAnioDos );
		str.append( "\nmontoAnioTres : " + montoAnioTres );
		str.append( "\nnumOperAnioTres : " + numOperAnioTres );	
		str.append( "\npaisDestino : " + paisDestinoD);
		str.append( "\nriesgo : " + riesgoD);
		str.append( "\nparaisoFiscal : " + paraisoFiscalD );
		str.append( "\nmontoAnioUno : " + montoAnioUnoD );
		str.append( "\nnumOperAnioUno : " + numOperAnioUnoD );
		str.append( "\nmontoAnioDos : " + montoAnioDosD );
		str.append( "\nnumOperAnioDos : " + numOperAnioDosD );
		str.append( "\nmontoAnioTres : " + montoAnioTresD );
		str.append( "\nnumOperAnioTres : " + numOperAnioTresD );
		return str.toString();
	}
			
	

}
