package com.bbva.msca.back.util;

public enum EnumTipologia {
	AI, B1, B2, B3, B4, B5, B6, B7, B8, B9, BX, C1, C6, C7, C8, DM, DS, E1, E3, E4, E5,
	E8, E9, F1, F2, FF, I1, IP, L2, L3, L4, L5, L6, L7, L8, L9, LB, LC, LD, LR, LS, M2,
	M3, M4, MB, MD, ME, MP, MR, MS, MT, NM, O3, O4, R1, R5, R7, R8, R9, RD, RP, RV, TC, TD, MF
}
