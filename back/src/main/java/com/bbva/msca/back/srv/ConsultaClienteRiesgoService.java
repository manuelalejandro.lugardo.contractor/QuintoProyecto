package com.bbva.msca.back.srv;

import java.math.BigDecimal;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Consumes;


import com.bbva.msca.back.dto.RiesgoDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/ConsultaClienteRiesgoService")
public interface ConsultaClienteRiesgoService {
	
	@GET
	@Path("/consultarRiesgos")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarRiesgos();
	

	
	@POST
	@Path("/consultarRiesgosFiltro")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarRiesgosFiltro(RiesgoDTO riesgoDTO);
	
	@GET
	@Path("/consultarPersonasReportadas/{cdClialtrie}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarPersonasReportadas(@PathParam("cdClialtrie") BigDecimal cdClialtrie);

	@GET
	@Path("/consultarDatosPersonaReportada/{nuCuenta}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDatosPersonaReportada(@PathParam("nuCuenta") String nuCuenta);
	
	@GET
	@Path("/consultarDatosOficinaPersonaReportada/{cdCliente}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarDatosOficinaPersonaReportada(@PathParam("cdCliente") String cdCliente);
	
	@GET
	@Path("/consultarOtrosClientesPersonaReportada/{cdCliente}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarOtrosClientesPersonaReportada(@PathParam("cdCliente") String cdCliente);
	
}
