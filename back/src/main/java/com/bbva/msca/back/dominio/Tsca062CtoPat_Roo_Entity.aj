// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca062CtoPat;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca062CtoPat_Roo_Entity {
    
    declare @type: Tsca062CtoPat: @Entity;
    
    declare @type: Tsca062CtoPat: @Table(name = "TSCA062_CTO_PAT", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca062CtoPat.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CD_CONTRATO")
    private BigDecimal Tsca062CtoPat.cdContrato;
    
    public BigDecimal Tsca062CtoPat.getCdContrato() {
        return this.cdContrato;
    }
    
    public void Tsca062CtoPat.setCdContrato(BigDecimal id) {
        this.cdContrato = id;
    }
    
    @Transactional
    public void Tsca062CtoPat.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca062CtoPat.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca062CtoPat attached = Tsca062CtoPat.findTsca062CtoPat(this.cdContrato);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca062CtoPat.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca062CtoPat.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca062CtoPat Tsca062CtoPat.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca062CtoPat merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca062CtoPat.entityManager() {
        EntityManager em = new Tsca062CtoPat().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca062CtoPat.countTsca062CtoPats() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca062CtoPat o", Long.class).getSingleResult();
    }
    
    public static List<Tsca062CtoPat> Tsca062CtoPat.findAllTsca062CtoPats() {
        return entityManager().createQuery("SELECT o FROM Tsca062CtoPat o", Tsca062CtoPat.class).getResultList();
    }
    
    public static Tsca062CtoPat Tsca062CtoPat.findTsca062CtoPat(BigDecimal cdContrato) {
        if (cdContrato == null) return null;
        return entityManager().find(Tsca062CtoPat.class, cdContrato);
    }
    
    public static List<Tsca062CtoPat> Tsca062CtoPat.findTsca062CtoPatEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca062CtoPat o", Tsca062CtoPat.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
