package com.bbva.msca.back.srv;

import java.util.List;
import com.bbva.msca.back.dao.AsignacionSesionDAO;
import com.bbva.msca.back.dto.AsignacionSesionDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class AsignacionSesionServiceImpl implements AsignacionSesionService {

	@Override
	public ResponseGeneric consultarAsignacionSesiones() {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionSesionDAO asignacionSesionDAO = new AsignacionSesionDAO();
		List<AsignacionSesionDTO> lstAsignacionSesionDTO = null;
		
		try {
			lstAsignacionSesionDTO = asignacionSesionDAO
					.getAsignacionSesiones();
			response.setResponse(lstAsignacionSesionDTO);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric creaAsigSesion(AsignacionSesionDTO asigSesion) {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionSesionDAO asigSesionDAO = new AsignacionSesionDAO();
		try {
			switch(asigSesionDAO.creaAsigSesion(asigSesion)){
			case Constantes.FIN_OK:
				response.setResponse(asigSesion);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_AV:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_ER_01012_03"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric modificaAsigSesion(AsignacionSesionDTO asigSesion) {
		ResponseGeneric response = new ResponseGeneric();
		AsignacionSesionDAO asigSesionDAO = new AsignacionSesionDAO();
		try {
			response.setResponse(asigSesionDAO.modificaAsigSesion(asigSesion));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

}
