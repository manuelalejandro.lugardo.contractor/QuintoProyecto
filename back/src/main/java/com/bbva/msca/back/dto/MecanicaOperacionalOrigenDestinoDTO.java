package com.bbva.msca.back.dto;

import java.util.Date;
public class MecanicaOperacionalOrigenDestinoDTO {

	//ORIGEN
	private Date fechaACVO;
	private String nbCuentaACVO;
	private String imImporteACVO;
	private String nbConceptoACVO;
	
	//DESTINO
	private Date fechaACVD;
	private String nbConceptoACVD;
	private String imImporteACVD;
	private String nbCuentaACVD;
	
	
	public Date getFechaACVO() {
		return fechaACVO;
	}
	public void setFechaACVO(Date fechaACVO) {
		this.fechaACVO = fechaACVO;
	}
	public String getNbCuentaACVO() {
		return nbCuentaACVO;
	}
	public void setNbCuentaACVO(String nbCuentaACVO) {
		this.nbCuentaACVO = nbCuentaACVO;
	}
	public String getImImporteACVO() {
		return imImporteACVO;
	}
	public void setImImporteACVO(String imImporteACVO) {
		this.imImporteACVO = imImporteACVO;
	}
	public String getNbConceptoACVO() {
		return nbConceptoACVO;
	}
	public void setNbConceptoACVO(String nbConceptoACVO) {
		this.nbConceptoACVO = nbConceptoACVO;
	}
	public Date getFechaACVD() {
		return fechaACVD;
	}
	public void setFechaACVD(Date fechaACVD) {
		this.fechaACVD = fechaACVD;
	}
	public String getNbConceptoACVD() {
		return nbConceptoACVD;
	}
	public void setNbConceptoACVD(String nbConceptoACVD) {
		this.nbConceptoACVD = nbConceptoACVD;
	}
	public String getImImporteACVD() {
		return imImporteACVD;
	}
	public void setImImporteACVD(String imImporteACVD) {
		this.imImporteACVD = imImporteACVD;
	}
	public String getNbCuentaACVD() {
		return nbCuentaACVD;
	}
	public void setNbCuentaACVD(String nbCuentaACVD) {
		this.nbCuentaACVD = nbCuentaACVD;
	}


	
	
	
}
