package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class CasosAsignadosDTO{
	
	private boolean stCasoAsignado;
	private BigDecimal cdCaso;
	private BigDecimal nuFolio;
	private BigDecimal nuCliente;
	private String nbCliente;
	private BigDecimal nuCuenta;
	private String nbOrigen;
	private String nbTipologia;
	private String nbSegmento;
	private Date fhAsignacion;
	private String nbPrioridad;
	private Date fhEnvio;
	private Date fhLimiteReporteA;
	private Date fhLimiteReporteB;
	private Date fhVencimientoRevision;
	private Date fhVencimientoLiberacion;
	private Date fhVencimientoReporte;
	
	
	public boolean getStCasoAsignado() {
		return stCasoAsignado;
	}
	public void setStCasoAsignado(boolean stCasoAsignado) {
		this.stCasoAsignado = stCasoAsignado;
	}
	public BigDecimal getCdCaso() {
		return cdCaso;
	}
	public void setCdCaso(BigDecimal cdCaso) {
		this.cdCaso = cdCaso;
	}
	public BigDecimal getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(BigDecimal nuFolio) {
		this.nuFolio = nuFolio;
	}
	public BigDecimal getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(BigDecimal nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public BigDecimal getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(BigDecimal nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public String getNbOrigen() {
		return nbOrigen;
	}
	public void setNbOrigen(String nbOrigen) {
		this.nbOrigen = nbOrigen;
	}
	public String getNbTipologia() {
		return nbTipologia;
	}
	public void setNbTipologia(String nbTipologia) {
		this.nbTipologia = nbTipologia;
	}
	public String getNbSegmento() {
		return nbSegmento;
	}
	public void setNbSegmento(String nbSegmento) {
		this.nbSegmento = nbSegmento;
	}
	public Date getFhAsignacion() {
		return fhAsignacion;
	}
	public void setFhAsignacion(Date fhAsignacion) {
		this.fhAsignacion = fhAsignacion;
	}
	public String getNbPrioridad() {
		return nbPrioridad;
	}
	public void setNbPrioridad(String nbPrioridad) {
		this.nbPrioridad = nbPrioridad;
	}
	public Date getFhEnvio() {
		return fhEnvio;
	}
	public void setFhEnvio(Date fhEnvio) {
		this.fhEnvio = fhEnvio;
	}
	public Date getFhLimiteReporteA() {
		return fhLimiteReporteA;
	}
	public void setFhLimiteReporteA(Date fhLimiteReporteA) {
		this.fhLimiteReporteA = fhLimiteReporteA;
	}
	public Date getFhLimiteReporteB() {
		return fhLimiteReporteB;
	}
	public void setFhLimiteReporteB(Date fhLimiteReporteB) {
		this.fhLimiteReporteB = fhLimiteReporteB;
	}
	public Date getFhVencimientoRevision() {
		return fhVencimientoRevision;
	}
	public void setFhVencimientoRevision(Date fhVencimientoRevision) {
		this.fhVencimientoRevision = fhVencimientoRevision;
	}
	public Date getFhVencimientoLiberacion() {
		return fhVencimientoLiberacion;
	}
	public void setFhVencimientoLiberacion(Date fhVencimientoLiberacion) {
		this.fhVencimientoLiberacion = fhVencimientoLiberacion;
	}
	public Date getFhVencimientoReporte() {
		return fhVencimientoReporte;
	}
	public void setFhVencimientoReporte(Date fhVencimientoReporte) {
		this.fhVencimientoReporte = fhVencimientoReporte;
	}
}
