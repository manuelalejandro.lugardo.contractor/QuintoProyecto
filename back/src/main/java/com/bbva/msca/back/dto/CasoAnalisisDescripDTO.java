package com.bbva.msca.back.dto;
 

public class CasoAnalisisDescripDTO {
	
	private String nbDesOpeReal;
	private String nbDesRazInv;
	
	
	public String getNbDesOpeReal() {
		return nbDesOpeReal;
	}
	public void setNbDesOpeReal(String nbDesOpeReal) {
		this.nbDesOpeReal = nbDesOpeReal;
	}
	 
	public String getNbDesRazInv() {
		return nbDesRazInv;
	}
	public void setNbDesRazInv(String nbDesRazInv) {
		this.nbDesRazInv = nbDesRazInv;
	}
	 
}
