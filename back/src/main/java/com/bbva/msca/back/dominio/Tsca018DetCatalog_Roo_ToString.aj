// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import java.lang.String;

privileged aspect Tsca018DetCatalog_Roo_ToString {
    
    public String Tsca018DetCatalog.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CdCatalogo: ").append(getCdCatalogo()).append(", ");
        sb.append("CdDetCatalogo: ").append(getCdDetCatalogo()).append(", ");
        sb.append("CdDetcatpad: ").append(getCdDetcatpad()).append(", ");
        sb.append("NbRazon: ").append(getNbRazon()).append(", ");
        sb.append("NbValor: ").append(getNbValor());
        return sb.toString();
    }
    
}
