// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca065CentroResp;
import java.lang.String;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca065CentroResp_Roo_Entity {
    
    declare @type: Tsca065CentroResp: @Entity;
    
    declare @type: Tsca065CentroResp: @Table(name = "TSCA065_CENTRO_RESP", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca065CentroResp.entityManager;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CD_OFICINA", length = 4)
    private String Tsca065CentroResp.cdOficina;
    
    public String Tsca065CentroResp.getCdOficina() {
        return this.cdOficina;
    }
    
    public void Tsca065CentroResp.setCdOficina(String id) {
        this.cdOficina = id;
    }
    
    @Transactional
    public void Tsca065CentroResp.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca065CentroResp.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca065CentroResp attached = Tsca065CentroResp.findTsca065CentroResp(this.cdOficina);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca065CentroResp.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca065CentroResp.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca065CentroResp Tsca065CentroResp.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca065CentroResp merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca065CentroResp.entityManager() {
        EntityManager em = new Tsca065CentroResp().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca065CentroResp.countTsca065CentroResps() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca065CentroResp o", Long.class).getSingleResult();
    }
    
    public static List<Tsca065CentroResp> Tsca065CentroResp.findAllTsca065CentroResps() {
        return entityManager().createQuery("SELECT o FROM Tsca065CentroResp o", Tsca065CentroResp.class).getResultList();
    }
    
    public static Tsca065CentroResp Tsca065CentroResp.findTsca065CentroResp(String cdOficina) {
        if (cdOficina == null || cdOficina.length() == 0) return null;
        return entityManager().find(Tsca065CentroResp.class, cdOficina);
    }
    
    public static List<Tsca065CentroResp> Tsca065CentroResp.findTsca065CentroRespEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca065CentroResp o", Tsca065CentroResp.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
