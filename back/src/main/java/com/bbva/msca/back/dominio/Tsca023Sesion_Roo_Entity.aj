// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca023Sesion;
import java.lang.String;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca023Sesion_Roo_Entity {
    
    declare @type: Tsca023Sesion: @Entity;
    
    declare @type: Tsca023Sesion: @Table(name = "TSCA023_SESION", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca023Sesion.entityManager;
    
    @Transactional
    public void Tsca023Sesion.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca023Sesion.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca023Sesion attached = Tsca023Sesion.findTsca023Sesion(this.cdSesion);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca023Sesion.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca023Sesion.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca023Sesion Tsca023Sesion.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca023Sesion merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca023Sesion.entityManager() {
        EntityManager em = new Tsca023Sesion().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca023Sesion.countTsca023Sesions() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca023Sesion o", Long.class).getSingleResult();
    }
    
    public static List<Tsca023Sesion> Tsca023Sesion.findAllTsca023Sesions() {
        return entityManager().createQuery("SELECT o FROM Tsca023Sesion o", Tsca023Sesion.class).getResultList();
    }
    
    public static Tsca023Sesion Tsca023Sesion.findTsca023Sesion(String cdSesion) {
        if (cdSesion == null || cdSesion.length() == 0) return null;
        return entityManager().find(Tsca023Sesion.class, cdSesion);
    }
    
    public static List<Tsca023Sesion> Tsca023Sesion.findTsca023SesionEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca023Sesion o", Tsca023Sesion.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
