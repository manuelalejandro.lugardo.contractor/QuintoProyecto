package com.bbva.msca.back.dto;

import java.util.Date;

public class DetalleAlertaDTO {
	
	private Integer nuFolio;
	private Integer nuCliente;
	private String nbCliente;
	private Integer nuCuenta;
	private Date fhAlerta;
	private Date fhEnvio;
	private Date fhVencimientoLiberacion;
	private String nbOrigen;
	private String nbTipologia;
	private String nbSegmento;
	private double imImporte;
	private String nbDivisa;
	
	
	public Integer getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(Integer nuFolio) {
		this.nuFolio = nuFolio;
	}
	public Integer getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(Integer nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public Integer getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(Integer nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public Date getFhAlerta() {
		return fhAlerta;
	}
	public void setFhAlerta(Date fhAlerta) {
		this.fhAlerta = fhAlerta;
	}
	public Date getFhEnvio() {
		return fhEnvio;
	}
	public void setFhEnvio(Date fhEnvio) {
		this.fhEnvio = fhEnvio;
	}
	public Date getFhVencimientoLiberacion() {
		return fhVencimientoLiberacion;
	}
	public void setFhVencimientoLiberacion(Date fhVencimientoLiberacion) {
		this.fhVencimientoLiberacion = fhVencimientoLiberacion;
	}
	public String getNbOrigen() {
		return nbOrigen;
	}
	public void setNbOrigen(String nbOrigen) {
		this.nbOrigen = nbOrigen;
	}
	public String getNbTipologia() {
		return nbTipologia;
	}
	public void setNbTipologia(String nbTipologia) {
		this.nbTipologia = nbTipologia;
	}
	public String getNbSegmento() {
		return nbSegmento;
	}
	public void setNbSegmento(String nbSegmento) {
		this.nbSegmento = nbSegmento;
	}
	public double getImImporte() {
		return imImporte;
	}
	public void setImImporte(double imImporte) {
		this.imImporte = imImporte;
	}
	public String getNbDivisa() {
		return nbDivisa;
	}
	public void setNbDivisa(String nbDivisa) {
		this.nbDivisa = nbDivisa;
	}

}
