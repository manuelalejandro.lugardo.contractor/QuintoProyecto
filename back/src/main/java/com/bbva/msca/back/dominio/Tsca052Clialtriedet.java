package com.bbva.msca.back.dominio;

import javax.persistence.Column;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(identifierType = Tsca052ClialtriedetPK.class, versionField = "", table = "TSCA052_CLIALTRIEDET", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca052Clialtriedet {
	
	 @Column(name = "TP_PERSONA")
	 private String cdTpPersona;
	
}
