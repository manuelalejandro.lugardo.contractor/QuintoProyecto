package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.dominio.Tsca020Perfil;
import com.bbva.msca.back.dominio.Tsca024Usuario;
import com.bbva.msca.back.dominio.Tsca030Gerencia;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dto.ModuloDTO;
import com.bbva.msca.back.dto.UsuarioDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

@Component
public class PerfiladoDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	/**
	 * Bitacora de movimientos Sesion - Tipologia
	 * */
	private Tsca055Log log;
	
	@SuppressWarnings("unchecked")
	public UsuarioDTO getUsuario(String nbCveRed) throws Exception{
		
		List<UsuarioDTO> resultado = new ArrayList<UsuarioDTO>();
		UsuarioDTO usuarioDTO = new UsuarioDTO();
		Query query = em.createNativeQuery(Consultas.getConsulta("getUsuario"));
		query.setParameter(1,nbCveRed);
		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdUsuario",StringType.INSTANCE)
		.addScalar("cdPerfil1")
        .addScalar("cdPerfil2")
        .addScalar("nbPerfil1")
        .addScalar("nbPerfil2")
        .addScalar("nbCveRed")
        .addScalar("nbPersona")
        .addScalar("nbPaterno")
        .addScalar("nbMaterno")
        .addScalar("cdStSistema",StringType.INSTANCE)
        .addScalar("cdSupervisor",StringType.INSTANCE)
        .addScalar("cdGerencia")
        .setResultTransformer(Transformers.aliasToBean(UsuarioDTO.class));
		
		resultado = (List<UsuarioDTO>)query.getResultList();
		if( resultado != null && !resultado.isEmpty() )
		{
			usuarioDTO = resultado.get(0);	
		}
		
		if(usuarioDTO.getCdPerfil1() != null){
			usuarioDTO.setLstModulos1(getLstModulosPerfil(usuarioDTO.getCdPerfil1()));
		}
		
		if(usuarioDTO.getCdPerfil2() != null){
			usuarioDTO.setLstModulos2(getLstModulosPerfil(usuarioDTO.getCdPerfil2()));
		}
		return usuarioDTO;
	}
	
	@SuppressWarnings("unchecked")
	public UsuarioDTO getInfoEmpleado(String nbCveRed) throws Exception{
		List<UsuarioDTO> resultado = new ArrayList<UsuarioDTO>();
		UsuarioDTO usuarioDTO = new UsuarioDTO();
		Query query = em.createNativeQuery(Consultas.getConsulta("getInfoEmpleado"));
		query.setParameter(1,nbCveRed);
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdUsuario",StringType.INSTANCE)
        .addScalar("nbCveRed")
        .addScalar("nbPersona")
        .addScalar("nbPaterno")
        .addScalar("nbMaterno")
        .addScalar("nbEmail")
        .setResultTransformer(Transformers.aliasToBean(UsuarioDTO.class));
		resultado = (List<UsuarioDTO>)query.getResultList();
		if( resultado != null && !resultado.isEmpty()){
			usuarioDTO = resultado.get(0);	
		}
		
		return usuarioDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<ModuloDTO> getLstModulosPerfil(BigDecimal cdPerfil) throws Exception{
		List<ModuloDTO> lstModulos = null;
		
		Query query = em.createNativeQuery(Consultas.getConsulta("getLstModulosPerfil"));
		query.setParameter(1,cdPerfil);
		
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdModulo")
        .addScalar("nbModulo")
        .addScalar("txModulo")
        .setResultTransformer(Transformers.aliasToBean(ModuloDTO.class));
		
		lstModulos = (List<ModuloDTO>)query.getResultList();

		return lstModulos;
	}
	
	@SuppressWarnings("unchecked")
	public List<UsuarioDTO> getUsuarios() throws Exception{
		List<UsuarioDTO> lstUsuarioDTO = new ArrayList<UsuarioDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("getUsuarios"));
		query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdUsuario", StringType.INSTANCE)
		.addScalar("cdPerfil1")
        .addScalar("cdPerfil2")
        .addScalar("nbPerfil1")
        .addScalar("nbPerfil2")
        .addScalar("nbCveRed")
        .addScalar("nbPersona")
        .addScalar("nbMaterno")
        .addScalar("nbPaterno")
        .addScalar("nbEmail")
        .addScalar("cdStSistema",StringType.INSTANCE)
        .addScalar("nbStSistema")
        .addScalar("cdSupervisor",StringType.INSTANCE)
        .addScalar("nbSupervisor",StringType.INSTANCE)
        .addScalar("nbCveRedSupervisor")
        .addScalar("cdGerenciaSupervisor")
        .addScalar("cdGerencia")
        .addScalar("nbCliente")
        .setResultTransformer(Transformers.aliasToBean(UsuarioDTO.class));
		lstUsuarioDTO = (List<UsuarioDTO>)query.getResultList();
		return lstUsuarioDTO;
	}
	
	@Transactional
	public int creaUsuario(UsuarioDTO usuario){
		int op = Constantes.FIN_ER;
		Tsca024Usuario nuevoUsuario =  Tsca024Usuario.findTsca024Usuario(usuario.getCdUsuario());
		if(nuevoUsuario != null){
			if(nuevoUsuario.getStSistema().equals(Constantes.CD_ST_SIS_I)){
				this.copiaABC(nuevoUsuario, usuario);
				nuevoUsuario.merge();
				nuevoUsuario.flush();
				this.log = this.generaLog(usuario, 1);
				this.log.persist();
				this.log.flush();
				op = Constantes.FIN_OK;
			}else{
				op = Constantes.FIN_AV;
			}
		}else{
			nuevoUsuario = new Tsca024Usuario();
			this.copiaABC(nuevoUsuario, usuario);
			nuevoUsuario.persist();
			nuevoUsuario.flush();
			this.log = this.generaLog(usuario, 1);
			this.log.persist();
			this.log.flush();
			op = Constantes.FIN_OK;
		}
		return op;
	}
	
	@Transactional
	public UsuarioDTO modificaUsuario(UsuarioDTO usuario){
		Tsca024Usuario nuevoUsuario = new Tsca024Usuario();
		this.copiaABC(nuevoUsuario, usuario);
		nuevoUsuario.merge();
		nuevoUsuario.flush();
		this.log = this.generaLog(usuario, 2);
		this.log.persist();
		this.log.flush();
		return usuario;
	}
	
	private void copiaABC(Tsca024Usuario nuevoUsuario, UsuarioDTO usuario){
		nuevoUsuario.setCdUsuario(usuario.getCdUsuario());
		if(usuario.getCdPerfil1().equals(new BigDecimal(Constantes.SUPERVISOR))){
			nuevoUsuario.setCdGerencia(Tsca030Gerencia.findTsca030Gerencia(usuario.getCdGerencia()));
		}else{
			Tsca024Usuario gerente =  Tsca024Usuario.findTsca024Usuario(usuario.getCdSupervisor());
			nuevoUsuario.setCdGerencia(gerente.getCdGerencia());
		}
		nuevoUsuario.setCdPerfilA(Tsca020Perfil.findTsca020Perfil(usuario.getCdPerfil1()));
		nuevoUsuario.setCdPerfilB(Tsca020Perfil.findTsca020Perfil(usuario.getCdPerfil2()));
		nuevoUsuario.setStSistema(usuario.getCdStSistema());
		nuevoUsuario.setCdSupervisor(usuario.getCdSupervisor());
		nuevoUsuario.setNbUsrRed(usuario.getNbCveRed());
	}
	
	private Tsca055Log generaLog(UsuarioDTO usuario, int accion){
		Tsca055Log logSesion = new Tsca055Log();
		logSesion.setNbEvento("MSCAVE01012_6");
		logSesion.setCdUsuario(usuario.getCdUsrAlta());
		logSesion.setFhFecha(new GregorianCalendar().getTime());
		switch(accion){
		case 1:
			logSesion.setNbAccionRealizad("Alta: " + usuario.getCdUsuario());
			break;
		case 2:		
			logSesion.setNbAccionRealizad("Modifica: " + usuario.getCdUsuario());
			break;
		}
		return logSesion;
	}

}
