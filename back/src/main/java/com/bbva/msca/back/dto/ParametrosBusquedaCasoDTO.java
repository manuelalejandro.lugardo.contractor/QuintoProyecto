package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class ParametrosBusquedaCasoDTO {
	private String nuCaso;
	private String nuFolio;
	private String nuCliente;
	private String nbCliente;
	private String nuCuenta;
	private String cdEscenario;
	private String cdSesion;
	private String cdSegmento;
	private String cdTipologia;
	private String cdRespuesta;
	private String cdCalificacion;
	private BigDecimal nuCalificacion;
	private String cdPtsMatriz;
	private BigDecimal nuPtsMatriz;
	private String cdConsultor;
	private String cdDictamenP;
	private String cdDictamenF;
	private String cdImporte;
	private BigDecimal imImporte;
	private Date    fhAlertaDel;
	private Date    fhAlertaAl;
	private Date    fhEnvioDel;
	private Date    fhEnvioAl;
	private Date    fhVencRevisionDel;
	private Date    fhVencRevisionAl;
	private Date    fhRevisionDel;
	private Date    fhRevisionAl;
	private Date    fhVencLiberacionDel;
	private Date    fhVencLiberacionAl;
	private Date    fhLiberacionDel;
	private Date    fhLiberacionAl;
	private Date    fhVencReporteDel;
	private Date    fhVencReporteAl;
	private Date    fhAsignacionDel;
	private Date    fhAsignacionAl;
	private String cdTPersona;
	
	public String getCdEscenario() {
		return cdEscenario;
	}
	public void setCdEscenario(String cdEscenario) {
		this.cdEscenario = cdEscenario;
	}
	public String getCdSesion() {
		return cdSesion;
	}
	public void setCdSesion(String cdSesion) {
		this.cdSesion = cdSesion;
	}
	public String getCdSegmento() {
		return cdSegmento;
	}
	public void setCdSegmento(String cdSegmento) {
		this.cdSegmento = cdSegmento;
	}
	public String getCdTipologia() {
		return cdTipologia;
	}
	public void setCdTipologia(String cdTipologia) {
		this.cdTipologia = cdTipologia;
	}
	public String getCdRespuesta() {
		return cdRespuesta;
	}
	public void setCdRespuesta(String cdRespuesta) {
		this.cdRespuesta = cdRespuesta;
	}
	public String getCdCalificacion() {
		return cdCalificacion;
	}

	public void setCdCalificacion(String cdCalificacion) {
		this.cdCalificacion = cdCalificacion;
	}
	public String getCdPtsMatriz() {
		return cdPtsMatriz;
	}
	public void setCdPtsMatriz(String cdPtsMatriz) {
		this.cdPtsMatriz = cdPtsMatriz;
	}
	public BigDecimal getNuPtsMatriz() {
		return nuPtsMatriz;
	}
	public void setNuPtsMatriz(BigDecimal nuPtsMatriz) {
		this.nuPtsMatriz = nuPtsMatriz;
	}
	public String getCdConsultor() {
		return cdConsultor;
	}
	public void setCdConsultor(String cdConsultor) {
		this.cdConsultor = cdConsultor;
	}
	public String getCdDictamenP() {
		return cdDictamenP;
	}
	public void setCdDictamenP(String cdDictamenP) {
		this.cdDictamenP = cdDictamenP;
	}
	public String getCdDictamenF() {
		return cdDictamenF;
	}
	public void setCdDictamenF(String cdDictamenF) {
		this.cdDictamenF = cdDictamenF;
	}
	public String getCdImporte() {
		return cdImporte;
	}
	public void setCdImporte(String cdImporte) {
		this.cdImporte = cdImporte;
	}
	public BigDecimal getImImporte() {
		return imImporte;
	}
	public void setImImporte(BigDecimal imImporte) {
		this.imImporte = imImporte;
	}
	public Date getFhAlertaDel() {
		return fhAlertaDel;
	}
	public void setFhAlertaDel(Date fhAlertaDel) {
		this.fhAlertaDel = fhAlertaDel;
	}
	public Date getFhAlertaAl() {
		return fhAlertaAl;
	}
	public void setFhAlertaAl(Date fhAlertaAl) {
		this.fhAlertaAl = fhAlertaAl;
	}
	public Date getFhEnvioDel() {
		return fhEnvioDel;
	}
	public void setFhEnvioDel(Date fhEnvioDel) {
		this.fhEnvioDel = fhEnvioDel;
	}
	public Date getFhEnvioAl() {
		return fhEnvioAl;
	}
	public void setFhEnvioAl(Date fhEnvioAl) {
		this.fhEnvioAl = fhEnvioAl;
	}
	public Date getFhVencRevisionDel() {
		return fhVencRevisionDel;
	}
	public void setFhVencRevisionDel(Date fhVencRevisionDel) {
		this.fhVencRevisionDel = fhVencRevisionDel;
	}
	public Date getFhVencRevisionAl() {
		return fhVencRevisionAl;
	}
	public void setFhVencRevisionAl(Date fhVencRevisionAl) {
		this.fhVencRevisionAl = fhVencRevisionAl;
	}
	public Date getFhRevisionDel() {
		return fhRevisionDel;
	}
	public void setFhRevisionDel(Date fhRevisionDel) {
		this.fhRevisionDel = fhRevisionDel;
	}
	public Date getFhRevisionAl() {
		return fhRevisionAl;
	}
	public void setFhRevisionAl(Date fhRevisionAl) {
		this.fhRevisionAl = fhRevisionAl;
	}
	public Date getFhVencLiberacionDel() {
		return fhVencLiberacionDel;
	}
	public void setFhVencLiberacionDel(Date fhVencLiberacionDel) {
		this.fhVencLiberacionDel = fhVencLiberacionDel;
	}
	public Date getFhVencLiberacionAl() {
		return fhVencLiberacionAl;
	}
	public void setFhVencLiberacionAl(Date fhVencLiberacionAl) {
		this.fhVencLiberacionAl = fhVencLiberacionAl;
	}
	public Date getFhLiberacionDel() {
		return fhLiberacionDel;
	}
	public void setFhLiberacionDel(Date fhLiberacionDel) {
		this.fhLiberacionDel = fhLiberacionDel;
	}
	public Date getFhLiberacionAl() {
		return fhLiberacionAl;
	}
	public void setFhLiberacionAl(Date fhLiberacionAl) {
		this.fhLiberacionAl = fhLiberacionAl;
	}
	public Date getFhVencReporteDel() {
		return fhVencReporteDel;
	}
	public void setFhVencReporteDel(Date fhVencReporteDel) {
		this.fhVencReporteDel = fhVencReporteDel;
	}
	public Date getFhVencReporteAl() {
		return fhVencReporteAl;
	}
	public void setFhVencReporteAl(Date fhVencReporteAl) {
		this.fhVencReporteAl = fhVencReporteAl;
	}
	public Date getFhAsignacionDel() {
		return fhAsignacionDel;
	}
	public void setFhAsignacionDel(Date fhAsignacionDel) {
		this.fhAsignacionDel = fhAsignacionDel;
	}
	public Date getFhAsignacionAl() {
		return fhAsignacionAl;
	}
	public void setFhAsignacionAl(Date fhAsignacionAl) {
		this.fhAsignacionAl = fhAsignacionAl;
	}
	public String getCdTPersona() {
		return cdTPersona;
	}
	public void setCdTPersona(String cdTPersona) {
		this.cdTPersona = cdTPersona;
	}
	public String getNuCaso() {
		return nuCaso;
	}
	public void setNuCaso(String nuCaso) {
		this.nuCaso = nuCaso;
	}
	public String getNuFolio() {
		return nuFolio;
	}
	public void setNuFolio(String nuFolio) {
		this.nuFolio = nuFolio;
	}
	public String getNuCliente() {
		return nuCliente;
	}
	public void setNuCliente(String nuCliente) {
		this.nuCliente = nuCliente;
	}
	public String getNbCliente() {
		return nbCliente;
	}
	public void setNbCliente(String nbCliente) {
		this.nbCliente = nbCliente;
	}
	public String getNuCuenta() {
		return nuCuenta;
	}
	public void setNuCuenta(String nuCuenta) {
		this.nuCuenta = nuCuenta;
	}
	public BigDecimal getNuCalificacion() {
		return nuCalificacion;
	}
	public void setNuCalificacion(BigDecimal nuCalificacion) {
		this.nuCalificacion = nuCalificacion;
	}
	
	
	
}



