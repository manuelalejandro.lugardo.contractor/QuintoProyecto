package com.bbva.msca.back.dominio;

import javax.persistence.PersistenceContext;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;
import com.bbva.msca.back.dominio.Tsca035AsigSesPK;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@PersistenceContext(unitName = "persistenceUnit")
@RooEntity(persistenceUnit = "persistenceUnit", table = "TSCA035_ASIG_SES", schema = "GORAPR", identifierType = Tsca035AsigSesPK.class, versionField = "")
public class Tsca035AsigSes {
	
}
