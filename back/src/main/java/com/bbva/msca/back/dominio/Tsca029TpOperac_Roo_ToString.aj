// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import java.lang.String;

privileged aspect Tsca029TpOperac_Roo_ToString {
    
    public String Tsca029TpOperac.toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("CdTpOperacion: ").append(getCdTpOperacion()).append(", ");
        sb.append("NbOperac: ").append(getNbOperac());
        return sb.toString();
    }
    
}
