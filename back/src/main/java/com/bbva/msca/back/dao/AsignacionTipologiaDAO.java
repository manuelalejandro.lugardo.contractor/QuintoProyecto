package com.bbva.msca.back.dao;


import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;

import com.bbva.msca.back.dominio.Tsca047AsigTplg;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dto.AsigTipologiaDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

public class AsignacionTipologiaDAO {

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	/**
	 * Bitacora de movimientos Sesion - Tipologia
	 * */
	private Tsca055Log log;

	@SuppressWarnings("unchecked")
	public List<AsigTipologiaDTO> getAsigTipologias() throws Exception {
		List<AsigTipologiaDTO> lstAsigTipologiaDTO = new ArrayList<AsigTipologiaDTO>();
		Query query = this.preparaQuery(0);
		lstAsigTipologiaDTO = (List<AsigTipologiaDTO>) query.getResultList();
		return lstAsigTipologiaDTO;
	}
	
	@SuppressWarnings("unchecked")
	public List<AsigTipologiaDTO> getAsigTipologiasGerencia(Integer cdGerencia) throws Exception {
		List<AsigTipologiaDTO> lstAsigTipologiaDTO = new ArrayList<AsigTipologiaDTO>();
		Query query = this.preparaQuery(cdGerencia);
		lstAsigTipologiaDTO = (List<AsigTipologiaDTO>) query.getResultList();
		return lstAsigTipologiaDTO;
	}

	public int creaAsigTipologia(AsigTipologiaDTO asigTipologia)
			throws Exception {
		int op = Constantes.FIN_ER;
		Tsca047AsigTplg nuevaAsigTipologia = null;
		if( asigTipologia.getCdAsigTipologia() != null ){
			nuevaAsigTipologia = Tsca047AsigTplg.findTsca047AsigTplg( asigTipologia.getCdAsigTipologia() );
		}
		
		if(nuevaAsigTipologia != null){
			if(nuevaAsigTipologia.getStSistema().equals(Constantes.CD_ST_SIS_I)){
				this.copiaABC(nuevaAsigTipologia, asigTipologia);
				nuevaAsigTipologia.merge();
				nuevaAsigTipologia.flush();
				this.log = this.generaLog(asigTipologia, 1);
				this.log.persist();
				this.log.flush();
				op = Constantes.FIN_OK;
			}else{
				op = Constantes.FIN_AV;
			}
		}else{
			nuevaAsigTipologia = new Tsca047AsigTplg();
			this.copiaABC(nuevaAsigTipologia, asigTipologia);
			nuevaAsigTipologia.persist();
			nuevaAsigTipologia.flush();
			asigTipologia.setCdAsigTipologia(nuevaAsigTipologia.getCdAsigTipologia());
			this.log = this.generaLog(asigTipologia, 1);
			this.log.persist();
			this.log.flush();
			op = Constantes.FIN_OK;
		}		
		
		return op;
	}

	public AsigTipologiaDTO eliminarAsigTipologia(AsigTipologiaDTO asigTipologia)
	throws Exception {
		Tsca047AsigTplg nuevaAsigTipologia = Tsca047AsigTplg.findTsca047AsigTplg( asigTipologia.getCdAsigTipologia() );
		nuevaAsigTipologia.setStSistema( Constantes.CD_ST_SIS_I );
		nuevaAsigTipologia.merge();
		nuevaAsigTipologia.flush();
		this.log = this.generaLog(asigTipologia, 2);
		this.log.persist();
		this.log.flush();
		return asigTipologia;
	}	
	
	public AsigTipologiaDTO modificaAsigTipologia(AsigTipologiaDTO asigTipologia)
			throws Exception {
		Tsca047AsigTplg nuevaAsigTipologia = Tsca047AsigTplg.findTsca047AsigTplg( asigTipologia.getCdAsigTipologia() );
		copiaABC(nuevaAsigTipologia, asigTipologia);
		nuevaAsigTipologia.merge();
		nuevaAsigTipologia.flush();
		this.log = this.generaLog(asigTipologia, 2);
		this.log.persist();
		this.log.flush();
		return asigTipologia;
	}
	
	private void copiaABC(Tsca047AsigTplg nuevaAsigTipologia, AsigTipologiaDTO asigTipologia){
		nuevaAsigTipologia.setCdAsigTipologia(asigTipologia.getCdAsigTipologia());
		nuevaAsigTipologia.setCdTipologia(asigTipologia.getCdTipologia());
		nuevaAsigTipologia.setCdPeriodicidad(asigTipologia.getCdPeriodicidad());
		nuevaAsigTipologia.setCdSeg(asigTipologia.getCdSegmento());
		nuevaAsigTipologia.setStSistema(asigTipologia.getCdStSistema());
		nuevaAsigTipologia.setCdAsigTipologia(asigTipologia.getCdAsigTipologia());
		nuevaAsigTipologia.setTpAsign(asigTipologia.getCdTpAsignacion());
		nuevaAsigTipologia.setCdUsrAlta(asigTipologia.getCdUsrAlta());
		nuevaAsigTipologia.setCdUsrConsultor(asigTipologia.getCdUsrConsultor());
		nuevaAsigTipologia.setFhAlta(asigTipologia.getFhAlta());
		nuevaAsigTipologia.setNuTopeAlerta(asigTipologia.getNuTopeAlertas());
	}
	
	private Query preparaQuery(Integer cdGerencia) throws Exception{
		Query query = null;
		if(cdGerencia > 0){
			query = em.createNativeQuery(Consultas
					.getConsulta("getAsignacionTipologiasGerenciaParametria"));
			query.setParameter(1, cdGerencia);
			query.setParameter(2, Constantes.CD_ST_SIS_A);
			query.setParameter(3, Constantes.CD_ST_SIS_A);
		}else{
			query = em.createNativeQuery(Consultas
					.getConsulta("getAsignacionTipologiasParametria"));
			query.setParameter(1, Constantes.CD_ST_SIS_A);
			query.setParameter(2, Constantes.CD_ST_SIS_A);
		}
		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdAsigTipologia")
				.addScalar("cdTipologia", StringType.INSTANCE)
				.addScalar("cdSegmento", StringType.INSTANCE)
				.addScalar("cdTpAsignacion", StringType.INSTANCE)
				.addScalar("cdUsrConsultor", StringType.INSTANCE)
				.addScalar("cdPeriodicidad", StringType.INSTANCE)
				.addScalar("nuTopeAlertas")
				.addScalar("fhAlta", TimestampType.INSTANCE)
				.addScalar("cdStSistema", StringType.INSTANCE)
				.addScalar("cdUsrAlta", StringType.INSTANCE)
				.addScalar("nbConsultor", StringType.INSTANCE)
				.addScalar("nbGerencia", StringType.INSTANCE)
				.addScalar("nbConsultor", StringType.INSTANCE)
				.addScalar("nbPeriodicidad", StringType.INSTANCE)
				.addScalar("nbTpAsignacion", StringType.INSTANCE)
				.addScalar("nbSegmento", StringType.INSTANCE)
				.setResultTransformer(
						Transformers.aliasToBean(AsigTipologiaDTO.class));
		return query;
	}
	
	private Tsca055Log generaLog(AsigTipologiaDTO asigTipologia, int accion){
		Tsca055Log logSesion = new Tsca055Log();
		logSesion.setNbEvento("MSCAVE01012_4");
		logSesion.setCdUsuario(asigTipologia.getCdUsrAlta());
		logSesion.setFhFecha(new GregorianCalendar().getTime());
		switch(accion){
		case 1:
			logSesion.setNbAccionRealizad("Alta: " + asigTipologia.getCdAsigTipologia());
			break;
		case 2:		
			logSesion.setNbAccionRealizad("Modifica: " + asigTipologia.getCdAsigTipologia());
			break;
		}
		return logSesion;
	}
	
 
}
