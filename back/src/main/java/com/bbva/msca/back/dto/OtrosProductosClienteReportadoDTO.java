package com.bbva.msca.back.dto;

import java.util.List;

public class OtrosProductosClienteReportadoDTO {

    private List<OtrosProductosClienteReportadoPatDTO> tablaPatrimonial;
    private List<OtrosProductosClienteReportadoTarCreDTO> tablaCredito;
    
	public List<OtrosProductosClienteReportadoPatDTO> getTablaPatrimonial() {
		return tablaPatrimonial;
	}
	public void setTablaPatrimonial(
			List<OtrosProductosClienteReportadoPatDTO> tablaPatrimonial) {
		this.tablaPatrimonial = tablaPatrimonial;
	}
	public List<OtrosProductosClienteReportadoTarCreDTO> getTablaCredito() {
		return tablaCredito;
	}
	public void setTablaCredito(List<OtrosProductosClienteReportadoTarCreDTO> tablaCredito) {
		this.tablaCredito = tablaCredito;
	}
    
	
}