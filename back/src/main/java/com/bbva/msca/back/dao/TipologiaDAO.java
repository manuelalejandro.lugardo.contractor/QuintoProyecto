package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.dominio.Tsca007Tipologia;
import com.bbva.msca.back.dominio.Tsca030Gerencia;
import com.bbva.msca.back.dominio.Tsca055Log;
import com.bbva.msca.back.dto.TipologiaDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.Consultas;

public class TipologiaDAO {

	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	/**
	 * Bitacora de movimientos Sesion - Tipologia
	 * */
	private Tsca055Log log;

	@SuppressWarnings("unchecked")
	public List<TipologiaDTO> getTipologia(int cdGerencia) throws Exception {
		List<TipologiaDTO> lstTipologiaDTO = new ArrayList<TipologiaDTO>();
		Query query = null;
		if(cdGerencia != 0){
			query = em.createNativeQuery(Consultas.getConsulta("getTipologiasParametriaGerencia"));
			query.setParameter(1, Constantes.CD_ST_SIS_A);
			query.setParameter(2, cdGerencia);
		}else{
			query = em.createNativeQuery(Consultas.getConsulta("getTipologiasParametria"));
			query.setParameter(1, Constantes.CD_ST_SIS_A);
		}
		

		query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("cdTipologia", StringType.INSTANCE)
				.addScalar("nbTipologia", StringType.INSTANCE)
				.addScalar("nuUmbralMC")
				.addScalar("cdGerencia")
				.addScalar("fhAlta", TimestampType.INSTANCE)
				.addScalar("cdUsrAlta", StringType.INSTANCE)
				.addScalar("cdStSistema", StringType.INSTANCE)
				.addScalar("nbUsrAlta", StringType.INSTANCE)
				.addScalar("nbGerencia", StringType.INSTANCE)
				.addScalar("cdTpTipologia", StringType.INSTANCE)
				.addScalar("tpCuestionario", StringType.INSTANCE)
				.setResultTransformer(Transformers.aliasToBean(TipologiaDTO.class));

		lstTipologiaDTO = (List<TipologiaDTO>) query.getResultList();

		return lstTipologiaDTO;
	}
	
	@Transactional
	public int creaTipologia(TipologiaDTO tipologia) throws Exception{
		int op = Constantes.FIN_ER;
		Tsca007Tipologia nuevaTipologia = Tsca007Tipologia.findTsca007Tipologia(tipologia.getCdTipologia());
		if(nuevaTipologia != null){
			if(nuevaTipologia.getStTipologia().equals(Constantes.CD_ST_SIS_I)){
				this.copiaABC(nuevaTipologia, tipologia);
				nuevaTipologia.merge();
				nuevaTipologia.flush();
				this.log = this.generaLog(nuevaTipologia, 1);
				this.log.persist();
				this.log.flush();
				op = Constantes.FIN_OK;
			}else{
				op = Constantes.FIN_AV;
			}
		}else{
			nuevaTipologia = new Tsca007Tipologia();
			this.copiaABC(nuevaTipologia, tipologia);
			nuevaTipologia.persist();
			nuevaTipologia.flush();
			this.log = this.generaLog(nuevaTipologia, 1);
			this.log.persist();
			this.log.flush();
			op = Constantes.FIN_OK;
		}
		return op;
	}
	
	@Transactional
	public TipologiaDTO modificaTipologia(TipologiaDTO tipologia) throws Exception{
		Tsca007Tipologia nuevaTipologia =  new Tsca007Tipologia();
		this.copiaABC(nuevaTipologia, tipologia);
		nuevaTipologia.merge();
		nuevaTipologia.flush();
		this.log = this.generaLog(nuevaTipologia, 2);
		this.log.persist();
		this.log.flush();
		return tipologia;
	}

	private void copiaABC(Tsca007Tipologia nuevaTipologia, TipologiaDTO tipologia){
		nuevaTipologia.setCdGerencia(Tsca030Gerencia.findTsca030Gerencia(tipologia.getCdGerencia()));
		nuevaTipologia.setStTipologia(tipologia.getCdStSistema());
		nuevaTipologia.setCdTipologia(tipologia.getCdTipologia());
		nuevaTipologia.setTpTipologia(tipologia.getCdTpTipologia());
		nuevaTipologia.setCdUsrAlta(tipologia.getCdUsrAlta());
		nuevaTipologia.setFhAlta(tipologia.getFhAlta());
		nuevaTipologia.setNbTipologia(tipologia.getNbTipologia());
		nuevaTipologia.setNuUmbralMc(tipologia.getNuUmbralMC());
	}
	
	private Tsca055Log generaLog(Tsca007Tipologia tipologia, int accion){
		Tsca055Log logSesion = new Tsca055Log();
		logSesion.setNbEvento("MSCAVE01012_2");
		logSesion.setCdUsuario(tipologia.getCdUsrAlta());
		logSesion.setFhFecha(new GregorianCalendar().getTime());
		switch(accion){
		case 1:
			logSesion.setNbAccionRealizad("Alta: " + tipologia.getCdTipologia().trim());
			break;
		case 2:		
			logSesion.setNbAccionRealizad("Modifica: " + tipologia.getCdTipologia().trim());
			break;
		}
		return logSesion;
	}

}
