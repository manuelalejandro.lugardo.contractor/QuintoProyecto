// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import java.lang.String;
import java.math.BigDecimal;

privileged aspect Tsca054Archivo_Roo_JavaBean {
    
    public BigDecimal Tsca054Archivo.getCdFolioArchivo() {
        return this.cdFolioArchivo;
    }
    
    public void Tsca054Archivo.setCdFolioArchivo(BigDecimal cdFolioArchivo) {
        this.cdFolioArchivo = cdFolioArchivo;
    }
    
    public String Tsca054Archivo.getCdUsuario() {
        return this.cdUsuario;
    }
    
    public void Tsca054Archivo.setCdUsuario(String cdUsuario) {
        this.cdUsuario = cdUsuario;
    }
    
    public String Tsca054Archivo.getCdUsuarioMdf() {
        return this.cdUsuarioMdf;
    }
    
    public void Tsca054Archivo.setCdUsuarioMdf(String cdUsuarioMdf) {
        this.cdUsuarioMdf = cdUsuarioMdf;
    }
    
}
