package com.bbva.msca.back.dao;

import com.bbva.msca.back.dto.CedulaCasoDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.h2.java.lang.System;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.springframework.transaction.annotation.Transactional;

@SuppressWarnings("unused")
public class CedulaCasoDAO {
	
	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();
	//String nuFolioAlertaDC = null;
 	
	@SuppressWarnings({ "unchecked" })
	public CedulaCasoDTO getCedulaCaso(BigDecimal cdCaso, String nuCliente, String nuFolioAlertaDC) throws Exception{
		 CedulaCasoDTO cedulaCasoDTO = new CedulaCasoDTO();
		 List<CedulaCasoDTO> lstCedulaCasoDTO = null;
			
		 Query query = em.createNativeQuery(Consultas.getConsulta("getDetalleCaso"));
		 query.setParameter(1,cdCaso);
		 //query.setParameter(2,nuCliente);
		 query.setParameter(2,nuFolioAlertaDC);
		 query.unwrap(org.hibernate.SQLQuery.class)
		.addScalar("cdCaso")
		.addScalar("nbConsultor")
		.addScalar("tipoAsig")
		.addScalar("nuFolioAlertaDC")
		.addScalar("tpPersona", StringType.INSTANCE)
		.addScalar("cdCliente", StringType.INSTANCE)

		.setResultTransformer(Transformers.aliasToBean(CedulaCasoDTO.class));
		 lstCedulaCasoDTO = (List<CedulaCasoDTO>)query.getResultList();
		 
		 if ( !lstCedulaCasoDTO.isEmpty() ) {
			 cedulaCasoDTO = lstCedulaCasoDTO.get( 0 );
			}/*else{
				cedulaCasoDTO.setCdCaso(cdCaso);
				cedulaCasoDTO.setNbConsultor("");
				cedulaCasoDTO.setTipoAsig("");
				cedulaCasoDTO.setNuFolioAlertaDC(nuFolioAlertaDC);
				cedulaCasoDTO.setTpPersona("");
				cedulaCasoDTO.setCdCliente(nuCliente);
			}*/
		 
		 //otras alertas
		 Query queryOA = em.createNativeQuery(Consultas.getConsulta("getDetalleCasoOtrasAlertas"));
		 queryOA.setParameter(1,cdCaso);
		 queryOA.setParameter(2,nuCliente); 
		 List<String> otrasAlertas = new ArrayList<String>((List<String>)queryOA.getResultList());
		 
		   StringBuilder alertas2 = new StringBuilder();
		   if(!otrasAlertas.isEmpty()){
			   alertas2.append(nuFolioAlertaDC+"   ");
			   for(int index=0;index<otrasAlertas.size();index++){
				   alertas2.append( String.valueOf( otrasAlertas.get( index ) ) );
				   if( index < otrasAlertas.size() - 1 )
				   {
					   alertas2.append( ", " );   
				   }
		        } 
		   }else{
			   alertas2.append(nuFolioAlertaDC);
		   }
		 cedulaCasoDTO.setOtrasAlertas( alertas2.toString() );
		 
		 //otros casos
		 Query queryOC = em.createNativeQuery(Consultas.getConsulta("getDetalleCasoOtrosCasos"));
		 queryOC.setParameter(1,cdCaso);
		 queryOC.setParameter(2,nuCliente); 
		 
		 List<BigDecimal> otrosCasos = (ArrayList<BigDecimal>)queryOC.getResultList();
		 StringBuilder casos = new StringBuilder();
		 if(!otrosCasos.isEmpty()){
			 for(int index=0;index<otrosCasos.size();index++){
				   casos.append( String.valueOf( otrosCasos.get( index ) ) );
				   if( index < otrosCasos.size() - 1 )
				   {
					   casos.append( ", " );   
				   }
			     } 
		   }else{
			   casos.append(" ");
		   }
		cedulaCasoDTO.setCdOtroCaso(casos.toString());
		
		
		
		//if otros clientes PERSONAS FISICAS Y MORALES
		///if(!cedulaCasoDTO.getCdCliente().isEmpty() && !cedulaCasoDTO.getTpPersona().isEmpty()){
		//if(!cedulaCasoDTO.getCdCliente().isEmpty()){
			String tpPersonaFisica = "F";
			String tpPersonaFisica2 = "f";
			if(tpPersonaFisica.equals(cedulaCasoDTO.getTpPersona()) || tpPersonaFisica2.equals(cedulaCasoDTO.getTpPersona()) ){
			//getDetalleCasoOtrosClientesF
			Query queryOCLF = em.createNativeQuery(Consultas.getConsulta("getDetalleCasoOtrosClientesFisica"));
			queryOCLF.setParameter(1,cedulaCasoDTO.getCdCliente()); 
			
			queryOCLF.unwrap(org.hibernate.SQLQuery.class).addScalar("otrosClientesF", StringType.INSTANCE);
			List<String> otrosClientesFs = (ArrayList<String>)queryOCLF.getResultList();
			StringBuilder clientesF = new StringBuilder();
				   
			if(!otrosClientesFs.isEmpty()){
					   for(int index=0;index<otrosClientesFs.size();index++){
						   clientesF.append(  otrosClientesFs.get( index ) );
						   if( index < otrosClientesFs.size() - 1 )
						   {
							   clientesF.append( ", " );   
						   }
				        } 
				   }else{
					   clientesF.append(" ");
				   }
				cedulaCasoDTO.setOtrosClientes(clientesF.toString());
			}
			//personas morales
			String tpPersonaMoral = "M";
			String tpPersonaMoral2 = "m";
				if(tpPersonaMoral.equals(cedulaCasoDTO.getTpPersona()) || tpPersonaMoral2.equals(cedulaCasoDTO.getTpPersona())){
					//getDetalleCasoOtrosClientesm
					Query queryOCLM = em.createNativeQuery(Consultas.getConsulta("getDetalleCasoOtrosClientesMoral"));
					queryOCLM.setParameter(1,cedulaCasoDTO.getCdCliente()); 
					queryOCLM.unwrap(org.hibernate.SQLQuery.class).addScalar("otrosClientesM", StringType.INSTANCE);
			
						 List<String> otrosClientesMs = (ArrayList<String>)queryOCLM.getResultList();
						   StringBuilder clientesM = new StringBuilder();
						   
						   if(!otrosClientesMs.isEmpty()){
							   for(int index=0;index<otrosClientesMs.size();index++){
								   clientesM.append(  otrosClientesMs.get( index ) );
								   if( index < otrosClientesMs.size() - 1 ){
									   clientesM.append( ", " );   
								   }
						        } 
						   }else{
							   clientesM.append(" ");
						   }
						cedulaCasoDTO.setOtrosClientes(clientesM.toString());
					}
		/*	}else{
				cedulaCasoDTO.setOtrosClientes(" ");
			}
			*/
		
		//SUPER CLIENTE FISICA O MORAL		
		///if(!cedulaCasoDTO.getCdCliente().isEmpty() && !cedulaCasoDTO.getTpPersona().isEmpty()){
	   //if(!cedulaCasoDTO.getCdCliente().isEmpty()){
			String tpPersona2Fisica = "F";
			String tpPersona2Fisica2 = "f";
			
			if(tpPersona2Fisica.equals(cedulaCasoDTO.getTpPersona()) || tpPersona2Fisica2.equals(cedulaCasoDTO.getTpPersona()) ){
				//getDetalleCasoNuSuperCliente
				Query queryNSCF = em.createNativeQuery(Consultas.getConsulta("getDetalleCasoNuSuperClienteFisica"));
				queryNSCF.setParameter(1,cedulaCasoDTO.getCdCliente()); 
				
				queryNSCF.unwrap(org.hibernate.SQLQuery.class).addScalar("nuSuperClienteF", StringType.INSTANCE);
				
				 List<String> lstSuperClienteF = (ArrayList<String>)queryNSCF.getResultList();
				
				 String superCF = null;
				   if(!lstSuperClienteF.isEmpty()){
					   superCF = lstSuperClienteF.get( 0 );
					   cedulaCasoDTO.setNuSuperCliente(superCF);
				   }else{
					   cedulaCasoDTO.setNuSuperCliente(" ");
				   }	  
			}//fin if fisica
			//personas morales
			String tpPersona2Moral = "M";
			String tpPersona2Moral2 = "m";
				if(tpPersona2Moral.equals(cedulaCasoDTO.getTpPersona()) || tpPersona2Moral2.equals(cedulaCasoDTO.getTpPersona())){
					//getDetalleCasoNuSuperCliente
					Query queryNSCM = em.createNativeQuery(Consultas.getConsulta("getDetalleCasoNuSuperClienteMoral"));
					queryNSCM.setParameter(1,cedulaCasoDTO.getCdCliente()); 
					
					queryNSCM.unwrap(org.hibernate.SQLQuery.class).addScalar("nuSuperClienteM", StringType.INSTANCE);
					
					 List<String> lstSuperClienteM = (ArrayList<String>)queryNSCM.getResultList();
					
					 String superCM = null;
					   if(!lstSuperClienteM.isEmpty()){
						   superCM = lstSuperClienteM.get( 0 );
						   cedulaCasoDTO.setNuSuperCliente(superCM);
					   }else{
						   cedulaCasoDTO.setNuSuperCliente("");
					   }	  					
				}			
		//}//fin if is empty principal
		
		
		
		return cedulaCasoDTO;
	}
	
	
	

	@SuppressWarnings("unchecked")
	public List<CedulaCasoDTO> reporteCedulaCaso(BigDecimal cdCaso, String nuCliente, String nuFolioAlertaDC) throws Exception {
		List<CedulaCasoDTO> lista = new ArrayList<CedulaCasoDTO>();
		
		Query query = em.createNativeQuery(Consultas.getConsulta("getDetalleCaso"));
			query.setParameter(1,cdCaso);
			query.setParameter(2,nuCliente);
			query.setParameter(3,nuFolioAlertaDC);
		query.unwrap(org.hibernate.SQLQuery.class)
		
		.addScalar("cdCaso")
		.addScalar("nbConsultor")
		.addScalar("tipoAsig")
		.addScalar("nuFolioAlertaDC")
		.addScalar("tpPersona", StringType.INSTANCE)
		.addScalar("cdCliente", StringType.INSTANCE)
	
		.setResultTransformer(Transformers.aliasToBean(CedulaCasoDTO.class));
		lista = (List<CedulaCasoDTO>)query.getResultList();
		 
		return lista;
	}
				
	
	
	
	
	
	
	
	
	
}//end
