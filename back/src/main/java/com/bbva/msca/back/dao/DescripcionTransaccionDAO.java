package com.bbva.msca.back.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import com.bbva.msca.back.dto.DescripcionTransaccionDTO;

public class DescripcionTransaccionDAO {

	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();
	
	@SuppressWarnings({ "unchecked"})
	public DescripcionTransaccionDTO getDescripcionTransaccion(String cdSistema, String folioAlerta) throws Exception{
		DescripcionTransaccionDTO descripcionTransaccionDTO = new DescripcionTransaccionDTO();
		List<DescripcionTransaccionDTO> lstDescripcionTransaccionDTO = null;

			 Query query = em.createNativeQuery(Consultas.getConsulta("getDescripcionTransaccion"));
			 
			 query.setParameter(1,cdSistema);
			 query.setParameter(2,folioAlerta);
			 
			 query.unwrap(org.hibernate.SQLQuery.class)
					.addScalar("nbDesTransaccion")

		        .setResultTransformer(Transformers.aliasToBean(DescripcionTransaccionDTO.class));
			
			 lstDescripcionTransaccionDTO = (List<DescripcionTransaccionDTO>)query.getResultList();
		if(!lstDescripcionTransaccionDTO.isEmpty()){
				 descripcionTransaccionDTO = lstDescripcionTransaccionDTO.get(0);
		}else{
			 descripcionTransaccionDTO.setNbDesTransaccion(" ");
		}
		return descripcionTransaccionDTO;
 }
	
}