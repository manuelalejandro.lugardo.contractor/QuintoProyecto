// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.bbva.msca.back.dominio;

import com.bbva.msca.back.dominio.Tsca021RelPerMn;
import com.bbva.msca.back.dominio.Tsca021RelPerMnPK;
import java.util.List;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Tsca021RelPerMn_Roo_Entity {
    
    declare @type: Tsca021RelPerMn: @Entity;
    
    declare @type: Tsca021RelPerMn: @Table(name = "TSCA021_REL_PER_MN", schema = "GORAPR");
    
    @PersistenceContext
    transient EntityManager Tsca021RelPerMn.entityManager;
    
    @EmbeddedId
    private Tsca021RelPerMnPK Tsca021RelPerMn.id;
    
    public Tsca021RelPerMnPK Tsca021RelPerMn.getId() {
        return this.id;
    }
    
    public void Tsca021RelPerMn.setId(Tsca021RelPerMnPK id) {
        this.id = id;
    }
    
    @Transactional
    public void Tsca021RelPerMn.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Tsca021RelPerMn.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Tsca021RelPerMn attached = Tsca021RelPerMn.findTsca021RelPerMn(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Tsca021RelPerMn.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Tsca021RelPerMn.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Tsca021RelPerMn Tsca021RelPerMn.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Tsca021RelPerMn merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
    public static final EntityManager Tsca021RelPerMn.entityManager() {
        EntityManager em = new Tsca021RelPerMn().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Tsca021RelPerMn.countTsca021RelPerMns() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Tsca021RelPerMn o", Long.class).getSingleResult();
    }
    
    public static List<Tsca021RelPerMn> Tsca021RelPerMn.findAllTsca021RelPerMns() {
        return entityManager().createQuery("SELECT o FROM Tsca021RelPerMn o", Tsca021RelPerMn.class).getResultList();
    }
    
    public static Tsca021RelPerMn Tsca021RelPerMn.findTsca021RelPerMn(Tsca021RelPerMnPK id) {
        if (id == null) return null;
        return entityManager().find(Tsca021RelPerMn.class, id);
    }
    
    public static List<Tsca021RelPerMn> Tsca021RelPerMn.findTsca021RelPerMnEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Tsca021RelPerMn o", Tsca021RelPerMn.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
}
