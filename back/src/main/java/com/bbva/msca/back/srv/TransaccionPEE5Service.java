package com.bbva.msca.back.srv;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.LocalizacionClienteDTO;
import com.bbva.msca.back.servicios.host.pee5.PeticionTransaccionPee5;
import com.bbva.msca.back.servicios.host.pee5.RespuestaTransaccionPee5;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/TransaccionPEE5Service")
@Consumes({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
public interface TransaccionPEE5Service {
	
	@POST
	@Path("/localizarClientes")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric invocar(LocalizacionClienteDTO clienteDTO);
	
	@POST
	@Path("cache")
	public ResponseGeneric invocarCache(LocalizacionClienteDTO clienteDTO);
	
	@DELETE
	@Path("cache")
	public void vaciarCache();	

}
