package com.bbva.msca.back.srv;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
 

import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRAbstractBeanDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.contexto.ArqSpringContext;
import com.bbva.jee.arq.spring.core.util.excepciones.PropiedadNoEncontradaExcepcion;
import com.bbva.msca.back.dto.CedulaCasoDTO;
import com.bbva.msca.back.dto.ConocimientoClienteDTO;
import com.bbva.msca.back.dto.CuentasClienteReportadoDTO;
import com.bbva.msca.back.dto.DatosGeneralesClienteDTO;
import com.bbva.msca.back.dto.DatosGeneralesClienteReportadoDTO;
import com.bbva.msca.back.dto.DescripcionTransaccionDTO;
import com.bbva.msca.back.dto.IdentificacionReporteDTO;
import com.bbva.msca.back.dto.InfoAccionistasyEmpresaDTO;
import com.bbva.msca.back.dto.MecanicaOperacionalConceptoDTO;
import com.bbva.msca.back.dto.OficiosListasNotasPeriodisticasDTO;
import com.bbva.msca.back.dto.OperacionesMedianasDTO;
import com.bbva.msca.back.dto.OperacionesMedianasRelevantesDTO;
import com.bbva.msca.back.dto.OperacionesRelevantesDTO;
import com.bbva.msca.back.dto.OpinionFuncionarioDTO;
import com.bbva.msca.back.dto.OtrosProductosClienteReportadoDTO;
import com.bbva.msca.back.dto.OtrosProductosClienteReportadoPatDTO;
import com.bbva.msca.back.dto.OtrosProductosClienteReportadoTarCreDTO;
import com.bbva.msca.back.dto.RelacionClienteFiguraDTO;
import com.bbva.msca.back.dto.ReportCCDatosGeneralesDTO;
import com.bbva.msca.back.dto.ReportConocimientoCliente;
import com.bbva.msca.back.dto.ReportOtrosProductosDTO;
import com.bbva.msca.back.dto.TransaccionClienteDTO;
import com.bbva.msca.back.dto.TransaccionClienteTdcDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.ResponseGeneric;

@Component
public class CedulaCasoPDFServiceImpl implements  CedulaCasoPDFService{
	
	@Autowired
	CasosService casosService;
	
	// Secciones Reporte
	private final String FECHA_CREACION = "FECHA_CREACION";
	private final String LOGO_BBVA = "LOGO_BBVA";
	private final String NUMERO_CASO = "NUMERO_CASO";


	public Response cedulaCasoPDF(BigDecimal cdCaso,String nuFolioAlerta,String cdSistema,String cdCliente,String nuCuenta ) throws Exception{
		System.out.println( "Cd Caso : " + cdCaso );
		System.out.println( "No. Folio Alerta : " + nuFolioAlerta );
		System.out.println( "Cd Sistema : " + cdSistema );
		System.out.println( "Cd Cliente : " + cdCliente );
		System.out.println( "No. Cuenta : " + nuCuenta );		
		return configurarReporte( cdCaso, nuFolioAlerta, cdSistema, cdCliente, nuCuenta );			
	}

	@SuppressWarnings({ "unchecked", "unused" })
	private Response configurarReporte(BigDecimal cdCaso,String nuFolioAlerta,String cdSistema,String cdCliente,String nuCuenta ){
		Response response = null;
		try
		{
			// Configuracion del Reporte
			@SuppressWarnings("rawtypes")
			Map<String,Object> parametrosReporte = new HashMap<String,Object>();
			
			// Subreporte No. 1 Cedula del Caso
			CedulaCasoDTO cedulaCaso = (CedulaCasoDTO)casosService.consultarCedulas(cdCaso, cdCliente, nuFolioAlerta).getResponse();			
			//CedulaCasoDTO cedulaCaso = new CedulaCasoDTO();
			List<CedulaCasoDTO> itemCedulaCaso = new ArrayList<CedulaCasoDTO>();
			if( cedulaCaso != null ){
				/*
				cedulaCaso.setNbConsultor( "Consultor" );
				cedulaCaso.setOtrasAlertas( "Alerta 1, Alerta 2, Alerta 3, etc." );
				cedulaCaso.setCdOtroCaso( "Caso A, Caso B, Caso C, etc.");
				cedulaCaso.setOtrosClientes( "Cliente A, Cliente B, Cliente C, etc. " );
				cedulaCaso.setTipoAsig( "Tipo de Asignacion" );
				cedulaCaso.setNuSuperCliente( "Nombre del super cliente" );
				*/
				itemCedulaCaso.add( cedulaCaso );
			};
			parametrosReporte.put( "srCedulaCaso", new JRBeanCollectionDataSource( itemCedulaCaso ));
			
			// Subreporte No. 2 Datos de Identificacion del Reporte						
			IdentificacionReporteDTO identificacionReporte = (IdentificacionReporteDTO)casosService.consultarIdentificacionReportes(nuFolioAlerta, cdSistema).getResponse();
			//IdentificacionReporteDTO identificacionReporte = new IdentificacionReporteDTO(); 
			List<IdentificacionReporteDTO> itemIdentificacion = new ArrayList<IdentificacionReporteDTO>();
			if( identificacionReporte != null ){	
				/*
				identificacionReporte.setNbSistemaIR( "Nombre Sistema" );
				identificacionReporte.setNuCuentaIR( "Cuenta" );
				identificacionReporte.setMontoDivisaIR( "$1,000,000.00" );
				identificacionReporte.setFhAlertaIR( new Date() );
				identificacionReporte.setFhEnvioIR( new Date() );
				identificacionReporte.setCdSesionIR( "Sesion" );
				identificacionReporte.setNbOficinaGestoraIR( "Oficina Gestora" );
				identificacionReporte.setNbMercadoIR( "Mercado" );
				identificacionReporte.setNbFuncionarioIR( "Funcionario" );
				identificacionReporte.setNbRiesgoIR( "Riesgo" );
				identificacionReporte.setNbDivisionIR( "Division" );
				*/
				itemIdentificacion.add( identificacionReporte );
			}
			parametrosReporte.put( "srIdentificacionReporte", new JRBeanCollectionDataSource( itemIdentificacion ));
			
			// Subreporte No. 3 Datos Generales del Cliente Reportado
			DatosGeneralesClienteReportadoDTO datosGenerales = (DatosGeneralesClienteReportadoDTO)casosService.consultarDatosGeneralesClienteReportado(cdSistema, nuFolioAlerta).getResponse();
			//DatosGeneralesClienteReportadoDTO datosGenerales = new DatosGeneralesClienteReportadoDTO();
			List<DatosGeneralesClienteReportadoDTO> itemDatGenCR = new ArrayList<DatosGeneralesClienteReportadoDTO>();
			if( datosGenerales != null ){
				//datosGenerales.setNbClienteCR( "Nombre del Cliente" );
				itemDatGenCR.add( datosGenerales );
			}
			parametrosReporte.put( "srDatosGenerales",new JRBeanCollectionDataSource( itemDatGenCR ) );
			
			
			// PAGINA 2 COSULTA SOBRE CONOCIMIENTO DEL CLIENTE
			// DTO Reporte
			ReportConocimientoCliente repConocimientoCte = new ReportConocimientoCliente();
			List<ReportConocimientoCliente> itmCCDatGen = new ArrayList<ReportConocimientoCliente>();
			
			// Subreporte No. 4 Consulta SObre conocimiento del cliente						
			// Seccion Datos Generales			
			DatosGeneralesClienteDTO datosCliente = (DatosGeneralesClienteDTO)casosService.consultarDatosGeneralesCliente(cdSistema, nuFolioAlerta).getResponse();
			//DatosGeneralesClienteDTO datosCliente = new DatosGeneralesClienteDTO();						
			if( datosCliente != null ){
				
				repConocimientoCte.setNbActEconomicaDC( datosCliente.getNbActEconomicaDC() );
				repConocimientoCte.setNbOcupacionDC( datosCliente.getNbOcupacionDC() );
				repConocimientoCte.setNbRegSimplificadoDC( datosCliente.getNbRegSimplificadoDC() );
				/*
				repConocimientoCte.setNbActEconomicaDC( "Actividad Econ�mica" );
				repConocimientoCte.setNbOcupacionDC( "Ocupaci�n" );
				repConocimientoCte.setNbRegSimplificadoDC( "R�gimen Simplificado" );
				*/				
			}
			
			// Seccion Transaccionalidad Subreporte 4_1
			TransaccionClienteDTO transacCte = (TransaccionClienteDTO)casosService.consultarTransaccionesCliente(cdSistema, nuFolioAlerta).getResponse();
			//TransaccionClienteDTO transacCte = new TransaccionClienteDTO();			
			if( transacCte != null){
				
				repConocimientoCte.setOperativaPesosTDC( transacCte.getOperativaPesosTDC() );
				repConocimientoCte.setMontoTotalDepositosTDC( transacCte.getMontoTotalDepositosTDC() );
				repConocimientoCte.setLocalidadOperarTDC( transacCte.getLocalidadOperarTDC() );
				/*
				repConocimientoCte.setOperativaPesosTDC( "Operativa Pesos" );
				repConocimientoCte.setMontoTotalDepositosTDC( "$1,000,000.00" );
				repConocimientoCte.setLocalidadOperarTDC( "Localidad a Operar" );
				*/
				
				List<TransaccionClienteTdcDTO> lstTmp = new ArrayList<TransaccionClienteTdcDTO>();
				if( transacCte.getTablaTDC() != null ){
					lstTmp = transacCte.getTablaTDC();
				}
				/*
				TransaccionClienteTdcDTO ob1 = new TransaccionClienteTdcDTO();
				ob1.setDestinoTransTDC( "Destino A");
				ob1.setImporteTDC( "$100,000.00");
				ob1.setInstrumentosMonetariosTDC( "MXN");
				ob1.setNoOperacionesTDC( "10");
				ob1.setOrigenTransTDC( "Origen A ");
				
				TransaccionClienteTdcDTO ob2 = new TransaccionClienteTdcDTO();
				ob2.setDestinoTransTDC( "Destino B");
				ob2.setImporteTDC( "$200,000.00");
				ob2.setInstrumentosMonetariosTDC( "MXN");
				ob2.setNoOperacionesTDC( "20");
				ob2.setOrigenTransTDC( "Origen A");					
				
				TransaccionClienteTdcDTO ob3 = new TransaccionClienteTdcDTO();
				ob3.setDestinoTransTDC( "Destino C");
				ob3.setImporteTDC( "$300,000.00");
				ob3.setInstrumentosMonetariosTDC( "MXN");
				ob3.setNoOperacionesTDC( "30");
				ob3.setOrigenTransTDC( "Origen C");	
				
				TransaccionClienteTdcDTO ob4 = new TransaccionClienteTdcDTO();
				ob4.setDestinoTransTDC( "Destino D");
				ob4.setImporteTDC( "$400,000.00");
				ob4.setInstrumentosMonetariosTDC( "MXN");
				ob4.setNoOperacionesTDC( "40");
				ob4.setOrigenTransTDC( "Origen D");						
				
				lstTmp.add( ob1 );
				lstTmp.add( ob2 );
				lstTmp.add( ob3 );
				lstTmp.add( ob4 );
				*/
				//repConocimientoCte.setLstTransacciones( transacCte.getTablaTDC());
				repConocimientoCte.setLstTransaccionesCliente( new JRBeanCollectionDataSource(lstTmp) );				
			}
			
			// Seccion Persona Relacionada
			RelacionClienteFiguraDTO personaRelacionada = (RelacionClienteFiguraDTO)casosService.consultarRelacionClienteFigura(cdSistema, nuFolioAlerta).getResponse();
			//RelacionClienteFiguraDTO personaRelacionada = new RelacionClienteFiguraDTO();
			if( personaRelacionada != null )
			{
				repConocimientoCte.setPuestoPEP( personaRelacionada.getPuestoPEP() );
				repConocimientoCte.setNombrePEP( personaRelacionada.getNombrePEP() );
				repConocimientoCte.setParentPEP( personaRelacionada.getParentPEP() );
				repConocimientoCte.setNombrePersRel( personaRelacionada.getNombrePersRel() );
				/*
				repConocimientoCte.setPuestoPEP( "Puesto PEP" );
				repConocimientoCte.setNombrePEP( "Nombre PEP" );
				repConocimientoCte.setParentPEP( "Parentezco PEP" );
				repConocimientoCte.setNombrePersRel( "Persona Relacionada PEP" );
				*/
			}
			
			// Se pasan al Reporte principal el parametro que contiene todos los elementos del subreporte conocimiento del cliente
			
			// Seccion Factores de riesgo del cliente
			ConocimientoClienteDTO conocimientoCliente = (ConocimientoClienteDTO)casosService.consultarConocimientoCliente(cdSistema, nuFolioAlerta).getResponse();			
			//ConocimientoClienteDTO conocimientoCliente = new ConocimientoClienteDTO();
			if( conocimientoCliente != null ){
				// Factores de riesgo
				repConocimientoCte.setPaisRecidenciaCAR( conocimientoCliente.getPaisRecidenciaCAR() );
				repConocimientoCte.setNacionalidadCAR(conocimientoCliente.getNacionalidadCAR());
				repConocimientoCte.setJustCuentaCAR(conocimientoCliente.getJustCuentaCAR());
				repConocimientoCte.setProcedenciaRecCAR(conocimientoCliente.getProcedenciaRecCAR());
				repConocimientoCte.setFuenteIngresoCAR(conocimientoCliente.getFuenteIngresoCAR());
				repConocimientoCte.setIndRiesgoCAR(conocimientoCliente.getIndRiesgoCAR());
				repConocimientoCte.setIndRiesgoActCAR(conocimientoCliente.getIndRiesgoActCAR());
				repConocimientoCte.setFhCamAltBajRiesgoCAR(conocimientoCliente.getFhCamAltBajRiesgoCAR());
				repConocimientoCte.setFhAltAltRiesgoCAR(conocimientoCliente.getFhAltAltRiesgoCAR());
				repConocimientoCte.setFhUltMovAltBajCAR(conocimientoCliente.getFhUltMovAltBajCAR());
				repConocimientoCte.setNbOrigenBajaCAR(conocimientoCliente.getNbOrigenBajaCAR());
				repConocimientoCte.setNbOrigenAltaCAR(conocimientoCliente.getNbOrigenAltaCAR());
				/*
				repConocimientoCte.setPaisRecidenciaCAR( "Pais Recidencia" );
				repConocimientoCte.setNacionalidadCAR("Nacionalidad");
				repConocimientoCte.setJustCuentaCAR("Just. Apertura Cuenta");
				repConocimientoCte.setProcedenciaRecCAR("Procedencia de los recursos");
				repConocimientoCte.setFuenteIngresoCAR("Fuente de Ingreso");
				repConocimientoCte.setIndRiesgoCAR("Indicador de Riesgo");
				repConocimientoCte.setFhCamAltBajRiesgoCAR("Fecha de cambio alto bajo riesgo");
				repConocimientoCte.setFhAltAltRiesgoCAR("Fecha de alta alto riesgo");
				repConocimientoCte.setFhUltMovAltBajCAR("Fecha ultimo movimiento alto/bajo");
				repConocimientoCte.setNbOrigenBajaCAR("Origen por baja");
				repConocimientoCte.setNbOrigenAltaCAR("Origen por alta");
				repConocimientoCte.setIndRiesgoActCAR("Riesgo de la actividad");
				*/
				
				// Factores RBA
				repConocimientoCte.setNuTipClienteRBA( conocimientoCliente.getNuTipClienteRBA() );
				repConocimientoCte.setNuActEconomicaRBA( conocimientoCliente.getNuActEconomicaRBA() );
				repConocimientoCte.setNuOcupacionRBA( conocimientoCliente.getNuOcupacionRBA() );
				repConocimientoCte.setNuRegPaisRBA( conocimientoCliente.getNuRegPaisRBA() );
				repConocimientoCte.setNuProServiciosRBA( conocimientoCliente.getNuProServiciosRBA() );
				repConocimientoCte.setNuOpeHabitualRBA( conocimientoCliente.getNuOpeHabitualRBA() );
				repConocimientoCte.setNuIntervinientesRBA( conocimientoCliente.getNuIntervinientesRBA() );
				repConocimientoCte.setNuSucursalRBA( conocimientoCliente.getNuSucursalRBA() );
				repConocimientoCte.setNuCanCaptacionRBA( conocimientoCliente.getNuCanCaptacionRBA() );
				repConocimientoCte.setNuAntiguedadPuntajeRBA( conocimientoCliente.getNuAntiguedadPuntajeRBA() );
				repConocimientoCte.setNuEdadRBA( conocimientoCliente.getNuEdadRBA() );
				repConocimientoCte.setNuTotalRBA( conocimientoCliente.getNuTotalRBA() );
				repConocimientoCte.setNbMotRiesgoRBA( conocimientoCliente.getNbMotRiesgoRBA() );
				/*
				repConocimientoCte.setNuTipClienteRBA( "Tipo de Cliente" );
				repConocimientoCte.setNuActEconomicaRBA( "Actividad Economica" );
				repConocimientoCte.setNuOcupacionRBA( "Ocupacion" );
				repConocimientoCte.setNuRegPaisRBA( "Registro del Pais" );
				repConocimientoCte.setNuProServiciosRBA( "Productos y Servicios" );
				repConocimientoCte.setNuOpeHabitualRBA( "Operativa Habitual" );
				repConocimientoCte.setNuIntervinientesRBA( "Itervinientes" );
				repConocimientoCte.setNuSucursalRBA( "Sucursal" );
				repConocimientoCte.setNuCanCaptacionRBA( "Canal de Captacion" );
				repConocimientoCte.setNuAntiguedadPuntajeRBA( "Antig�edad" );
				repConocimientoCte.setNuEdadRBA( "Edad" );
				repConocimientoCte.setNuTotalRBA( "Total" );
				repConocimientoCte.setNbMotRiesgoRBA( "Motivo de riesgo" );
				*/				
			}
			
			// Subreporte No. 8 Accionistas y Empresas Tabla No 2 TDC
			List<InfoAccionistasyEmpresaDTO> lstInfoAccionistasyEmpresaDTO = (List<InfoAccionistasyEmpresaDTO>) casosService.consultarInfoAccionistasyEmpresa(cdSistema, nuFolioAlerta).getResponse();
			//List<InfoAccionistasyEmpresaDTO> lstInfoAccionistasyEmpresaDTO = new ArrayList<InfoAccionistasyEmpresaDTO>();
			if( lstInfoAccionistasyEmpresaDTO != null )
			{
				/*
				InfoAccionistasyEmpresaDTO obA = new InfoAccionistasyEmpresaDTO();
				obA.setCdAccionista( "1" );
				obA.setNbCliente( "Cliente A" );
				obA.setNbDecision( "Decisi�n" );
				obA.setNbEmpresa( "Empresa A" );
				obA.setNbParticipacion( "Participaci�n A" );
				obA.setNuFolio( "No. Folio A" );
				obA.setPorParticipacion( new BigDecimal(10));
				InfoAccionistasyEmpresaDTO obB = new InfoAccionistasyEmpresaDTO();
				obB.setCdAccionista( "2" );
				obB.setNbCliente( "Cliente B" );
				obB.setNbDecision( "Decisi�n" );
				obB.setNbEmpresa( "Empresa B" );
				obB.setNbParticipacion( "Participaci�n B" );
				obB.setNuFolio( "No. Folio B" );
				obB.setPorParticipacion( new BigDecimal(10));				
				InfoAccionistasyEmpresaDTO obC = new InfoAccionistasyEmpresaDTO();
				obC.setCdAccionista( "3" );
				obC.setNbCliente( "Cliente C" );
				obC.setNbDecision( "Decisi�n" );
				obC.setNbEmpresa( "Empresa C" );
				obC.setNbParticipacion( "Participaci�n C" );
				obC.setNuFolio( "No. Folio C" );
				obC.setPorParticipacion( new BigDecimal(10));				
				InfoAccionistasyEmpresaDTO obD = new InfoAccionistasyEmpresaDTO();
				obD.setCdAccionista( "4" );
				obD.setNbCliente( "Cliente D" );
				obD.setNbDecision( "Decisi�n" );
				obD.setNbEmpresa( "Empresa D" );
				obD.setNbParticipacion( "Participaci�n D" );
				obD.setNuFolio( "No. Folio D" );
				lstInfoAccionistasyEmpresaDTO.add( obA );
				lstInfoAccionistasyEmpresaDTO.add( obB );
				lstInfoAccionistasyEmpresaDTO.add( obC );
				lstInfoAccionistasyEmpresaDTO.add( obD );
				*/
				repConocimientoCte.setLstAccionistas( new JRBeanCollectionDataSource( lstInfoAccionistasyEmpresaDTO ) );				
			}
			//parametrosReporte.put( "srAccionistasEmpresa",new JRBeanCollectionDataSource( lstInfoAccionistasyEmpresaDTO ) );
						
			// Seccion Otros Productos del cliente reportado 
			OtrosProductosClienteReportadoDTO otrosProductos = (OtrosProductosClienteReportadoDTO)casosService.consultarOtrosProductosCR(cdSistema, nuFolioAlerta, cdCaso).getResponse();
			//OtrosProductosClienteReportadoDTO otrosProductos = new OtrosProductosClienteReportadoDTO();
			if( otrosProductos != null)
			{
				List<OtrosProductosClienteReportadoTarCreDTO> lstOP_TC = new ArrayList<OtrosProductosClienteReportadoTarCreDTO>();
				List<OtrosProductosClienteReportadoPatDTO> lstOP_Pat = new ArrayList<OtrosProductosClienteReportadoPatDTO>();
				if( otrosProductos != null )
				{
					if( otrosProductos.getTablaCredito() != null ){
						lstOP_TC = otrosProductos.getTablaCredito();	
					}
					if( otrosProductos.getTablaPatrimonial() != null ){
						lstOP_Pat = otrosProductos.getTablaPatrimonial();	
					}
					
				}
				/*
				OtrosProductosClienteReportadoTarCreDTO opTc1 = new OtrosProductosClienteReportadoTarCreDTO();
				opTc1.setFhAperturaOPCR( new Date() );
				opTc1.setFhCancelacionOPCR( "14/12/2016" );
				opTc1.setImProductoOPCR( new BigDecimal( "1000" ) );
				opTc1.setImProPromOPCR( new BigDecimal( "1000" ) );
				opTc1.setNbEstatusOPCR( "Estatus" );
				opTc1.setNbParticipesOPCR( "Participes" );
				opTc1.setNbProductoOPCR( "Producto TC" );
				opTc1.setNbSubproductoOPCR( "Subproducto" );
				opTc1.setNbTitularOPCR( "Titular" );
				opTc1.setNuCtePartOPCR( "No. Cliente" );
				opTc1.setNuCuentaOPCR( "No. Cuenta" );
				
				OtrosProductosClienteReportadoTarCreDTO opTc2 = new OtrosProductosClienteReportadoTarCreDTO();
				opTc2.setFhAperturaOPCR( new Date() );
				opTc2.setFhCancelacionOPCR( "14/12/2016" );
				opTc2.setImProductoOPCR( new BigDecimal( "2000" ) );
				opTc2.setImProPromOPCR( new BigDecimal( "2000" ) );
				opTc2.setNbEstatusOPCR( "Estatus" );
				opTc2.setNbParticipesOPCR( "Participes" );
				opTc2.setNbProductoOPCR( "Producto TC" );
				opTc2.setNbSubproductoOPCR( "Subproducto" );
				opTc2.setNbTitularOPCR( "Titular" );
				opTc2.setNuCtePartOPCR( "No. Cliente" );
				opTc2.setNuCuentaOPCR( "No. Cuenta" );
				
				OtrosProductosClienteReportadoTarCreDTO opTc3 = new OtrosProductosClienteReportadoTarCreDTO();
				opTc3.setFhAperturaOPCR( new Date() );
				opTc3.setFhCancelacionOPCR( "14/12/2016" );
				opTc3.setImProductoOPCR( new BigDecimal( "3000" ) );
				opTc3.setImProPromOPCR( new BigDecimal( "3000" ) );
				opTc3.setNbEstatusOPCR( "Estatus" );
				opTc3.setNbParticipesOPCR( "Participes" );
				opTc3.setNbProductoOPCR( "Producto TC" );
				opTc3.setNbSubproductoOPCR( "Subproducto" );
				opTc3.setNbTitularOPCR( "Titular" );
				opTc3.setNuCtePartOPCR( "No. Cliente" );
				opTc3.setNuCuentaOPCR( "No. Cuenta" );
				lstOP_TC.add( opTc1 );
				lstOP_TC.add( opTc2 );
				lstOP_TC.add( opTc3 );
				
				repConocimientoCte.setLstOPTarjetaCredito( new JRBeanCollectionDataSource( lstOP_TC ) );
				
				OtrosProductosClienteReportadoPatDTO obOPP1 = new OtrosProductosClienteReportadoPatDTO();
				obOPP1.setFhAperturaOPCRP( new Date() );
				obOPP1.setImProductoOPCRP( new BigDecimal( "100000") );
				//obOPP1.setImProPromOPCRP( new BigDecimal( 100000 ) );
				obOPP1.setNbEstatusOPCRP( "Estatus" );
				obOPP1.setNbParticipesOPCRP( "Participes" );
				obOPP1.setNbProductoOPCRP( "Producto Pat" );
				obOPP1.setNbTitularOPCRP( "Titular" );
				obOPP1.setNuCtePartOPCRP( "Cliente" );
				obOPP1.setNuCuentaOPCRP( "Cuenta" );
				OtrosProductosClienteReportadoPatDTO obOPP2 = new OtrosProductosClienteReportadoPatDTO();
				obOPP2.setFhAperturaOPCRP( new Date() );
				obOPP2.setImProductoOPCRP( new BigDecimal( "100000") );
				//obOPP2.setImProPromOPCRP( new BigDecimal( 100000 ) );
				obOPP2.setNbEstatusOPCRP( "Estatus" );
				obOPP2.setNbParticipesOPCRP( "Participes" );
				obOPP2.setNbProductoOPCRP( "Producto Pat" );
				obOPP2.setNbTitularOPCRP( "Titular" );
				obOPP2.setNuCtePartOPCRP( "Cliente" );
				obOPP2.setNuCuentaOPCRP( "Cuenta" );				
				OtrosProductosClienteReportadoPatDTO obOPP3 = new OtrosProductosClienteReportadoPatDTO();
				obOPP3.setFhAperturaOPCRP( new Date() );
				obOPP3.setImProductoOPCRP( new BigDecimal( "100000") );
				//obOPP3.setImProPromOPCRP( new BigDecimal( 100000 ) );
				obOPP3.setNbEstatusOPCRP( "Estatus" );
				obOPP3.setNbParticipesOPCRP( "Participes" );
				obOPP3.setNbProductoOPCRP( "Producto Pat" );
				obOPP3.setNbTitularOPCRP( "Titular" );
				obOPP3.setNuCtePartOPCRP( "Cliente" );
				obOPP3.setNuCuentaOPCRP( "Cuenta" );		
				lstOP_Pat.add( obOPP1 );
				lstOP_Pat.add( obOPP2 );
				lstOP_Pat.add( obOPP3 );
				*/
				repConocimientoCte.setLstOPPatrimonial( new JRBeanCollectionDataSource( lstOP_Pat ) );				
			}
						
			// Subreporte No. 10 Cuentas del Cliente Reportado Tabla 4
			List<CuentasClienteReportadoDTO> lstCuentas = (List<CuentasClienteReportadoDTO>)casosService.consultarCuentasClienteReportado(cdSistema, nuFolioAlerta,cdCaso).getResponse();			

			//List<CuentasClienteReportadoDTO> lstCuentas = new ArrayList<CuentasClienteReportadoDTO>();
			if( lstCuentas != null )
			{
				/*
				CuentasClienteReportadoDTO cta1 = new CuentasClienteReportadoDTO();
				cta1.setFhAperturaCCR( new Date() );
				cta1.setNuCuentaCCR( "No. Cuenta");
				cta1.setNbProductoCCR( "Producto" );
				cta1.setNbSubproductoCCR( "Subproducto" );
				cta1.setNbEstatusCCR( "Estatus" );
				cta1.setFhCancelacionCCR( "fecha Cancelacion" );
				cta1.setNbTitularCCR( "Titular" );
				cta1.setNuCtePartCCR( "No. Cliente" );
				cta1.setNbParticipesCCR( "Partiipes" );
				cta1.setImSaldoCCR( "Saldo" );
				cta1.setImSaldoPromCCR( "Saldo Promedio");
				CuentasClienteReportadoDTO cta2 = new CuentasClienteReportadoDTO();
				cta2.setFhAperturaCCR( new Date() );
				cta2.setNuCuentaCCR( "No. Cuenta");
				cta2.setNbProductoCCR( "Producto" );
				cta2.setNbSubproductoCCR( "Subproducto" );
				cta2.setNbEstatusCCR( "Estatus" );
				cta2.setFhCancelacionCCR( "fecha Cancelacion" );
				cta2.setNbTitularCCR( "Titular" );
				cta2.setNuCtePartCCR( "No. Cliente" );
				cta2.setNbParticipesCCR( "Partiipes" );
				cta2.setImSaldoCCR( "Saldo" );
				cta2.setImSaldoPromCCR( "Saldo Promedio");
				CuentasClienteReportadoDTO cta3 = new CuentasClienteReportadoDTO();
				cta3.setFhAperturaCCR( new Date() );
				cta3.setNuCuentaCCR( "No. Cuenta");
				cta3.setNbProductoCCR( "Producto" );
				cta3.setNbSubproductoCCR( "Subproducto" );
				cta3.setNbEstatusCCR( "Estatus" );
				cta3.setFhCancelacionCCR( "fecha Cancelacion" );
				cta3.setNbTitularCCR( "Titular" );
				cta3.setNuCtePartCCR( "No. Cliente" );
				cta3.setNbParticipesCCR( "Partiipes" );
				cta3.setImSaldoCCR( "Saldo" );
				cta3.setImSaldoPromCCR( "Saldo Promedio");
				lstCuentas.add( cta1 );
				lstCuentas.add( cta2 );
				lstCuentas.add( cta3 );
				*/
				repConocimientoCte.setLstCuentasClienteRep( new JRBeanCollectionDataSource( lstCuentas ) );
			}
			

	
			/*
			// Subreporte No. 12 Tabla Cuatrimestral (No recibe Sistema que forma parte de la llave, no recibe sesion ) cdSesion
			// Mecanica Operacional de a cuenta
			List<MecanicaOperacionalConceptoDTO> lstMecanicaOperacionalConceptoDTO = (List<MecanicaOperacionalConceptoDTO>)casosService.consultarMecanicaOperacionalConceptoAnalisis(cdCaso, nuFolioAlerta, cdSistema).getResponse();			
			// Subreporte No. 13 Subreporte Mes Uno
			List<MecanicaOperacionalConceptoDTO> lstMesUnoDTO  = (List<MecanicaOperacionalConceptoDTO>) casosService.consultarConceptosMesUnoAnalisis(cdCaso, nuFolioAlerta, cdSistema).getResponse();		
			List<MecanicaOperacionalConceptoDTO> lstMesDosDTO = (List<MecanicaOperacionalConceptoDTO>) casosService.consultarConceptosMesDosAnalisis(cdCaso, nuFolioAlerta, cdSistema).getResponse();			
			*/

			
			// Descripcion de la transaccion
			//DescripcionTransaccionDTO descTransaccion = (DescripcionTransaccionDTO)casosService.consultarDescripcionTransaccion( cdSistema, nuFolioAlerta ).getResponse();
			DescripcionTransaccionDTO descTransaccion = new DescripcionTransaccionDTO();
			if( descTransaccion != null ){
				repConocimientoCte.setDescripcionTransaccion( descTransaccion.getNbDesTransaccion() );
				/*
				repConocimientoCte.setDescripcionTransaccion( "Descripci�n de la transacci�n............................." +
						"\n..............................................."
						+ "\n..............................................."
						+ "\n..............................................."
						+ "\n...............................................");
				 */
			}							
			
			// Subreporte No. 11 Opinion del Funcionario
			List<OpinionFuncionarioDTO> lstOpinionFuncionario = (List<OpinionFuncionarioDTO> ) casosService.consultarOpinionFuncionario(cdSistema, nuFolioAlerta).getResponse();
			//List<OpinionFuncionarioDTO> lstOpinionFuncionario = new ArrayList<OpinionFuncionarioDTO>();
			if( lstOpinionFuncionario != null )
			{
				/*
				OpinionFuncionarioDTO opF1 = new OpinionFuncionarioDTO();
				opF1.setNuPregunta( new BigDecimal( "1" ) );
				opF1.setNbPregunta( "�Pregunta 1?" );
				opF1.setTxtRespuesta( "Respuesta 1." );
				OpinionFuncionarioDTO opF2 = new OpinionFuncionarioDTO();
				opF2.setNuPregunta( new BigDecimal( "2" ) );
				opF2.setNbPregunta( "�Pregunta 2?" );
				opF2.setTxtRespuesta( "Respuesta 2." );								
				OpinionFuncionarioDTO opF3 = new OpinionFuncionarioDTO();
				opF3.setNuPregunta( new BigDecimal( "3" ) );
				opF3.setNbPregunta( "�Pregunta 3?" );
				opF3.setTxtRespuesta( "Respuesta 3." );
				OpinionFuncionarioDTO opF4 = new OpinionFuncionarioDTO();
				opF4.setNuPregunta( new BigDecimal( "4" ) );
				opF4.setNbPregunta( "�Pregunta 4?" );
				opF4.setTxtRespuesta( "Respuesta 4." );				
				lstOpinionFuncionario.add( opF1 );
				lstOpinionFuncionario.add( opF2 );
				lstOpinionFuncionario.add( opF3 );
				lstOpinionFuncionario.add( opF4 );
				*/
				repConocimientoCte.setLstOpinionFuncionario( new JRBeanCollectionDataSource( lstOpinionFuncionario ) );
			}			
			
			// Operaciones Medianas, Relevantes y Transferencias Internacionales
			ResponseGeneric resGenOperMed = casosService.consultarOperacionesMedianasRelevantes(cdCaso, cdSistema, nuFolioAlerta);
			OperacionesMedianasRelevantesDTO dtoOperMedRel = (OperacionesMedianasRelevantesDTO) resGenOperMed.getResponse();
			//OperacionesMedianasRelevantesDTO dtoOperMedRel = new OperacionesMedianasRelevantesDTO();
			List<OperacionesMedianasDTO> lstOperacionesMedianas = new ArrayList<OperacionesMedianasDTO>();
			List<OperacionesRelevantesDTO> lstOperacionesRelevantes = new ArrayList<OperacionesRelevantesDTO>();
			if( dtoOperMedRel != null ){
				
				if( dtoOperMedRel.getTablaMedianas() != null ){
					lstOperacionesMedianas = dtoOperMedRel.getTablaMedianas();
				}
				/*
				OperacionesMedianasDTO opM1 = new OperacionesMedianasDTO();
				opM1.setFhOperacionOMED( new Date() );
				opM1.setImImporteOMED( new BigDecimal( "20000"));
				opM1.setNbMonedaOMED( "MX");
				opM1.setNbOperacionesOMED( "Operaciones");
				opM1.setNuCuentaOMED( "No Cuenta");
				opM1.setNuOperacionesOMED( new BigDecimal( 10 ) );
				OperacionesMedianasDTO opM2 = new OperacionesMedianasDTO();
				opM2.setFhOperacionOMED( new Date() );
				opM2.setImImporteOMED( new BigDecimal( "30000"));
				opM2.setNbMonedaOMED( "MX");
				opM2.setNbOperacionesOMED( "Operaciones");
				opM2.setNuCuentaOMED( "No Cuenta");
				opM2.setNuOperacionesOMED( new BigDecimal( 10 ) );

				lstOperacionesMedianas.add( opM1 );
				lstOperacionesMedianas.add( opM2 );
				*/
				if( dtoOperMedRel.getTablaRelevantes() != null ){
					lstOperacionesRelevantes = dtoOperMedRel.getTablaRelevantes();	
				}
				/*
				OperacionesRelevantesDTO opR1 = new OperacionesRelevantesDTO();
				opR1.setFhOperacionOPREL( new Date() );
				opR1.setImporteOPREL( new BigDecimal( "50000") );
				opR1.setMonedaOPREL( "USD" );
				opR1.setNuCuentaOPREL( "No. Cuenta" );
				opR1.setNuOperacionesOPREL( new BigDecimal( "30"));
				opR1.setTpOperacionOPREL( "Tipo");
				OperacionesRelevantesDTO opR2 = new OperacionesRelevantesDTO();
				opR2.setFhOperacionOPREL( new Date() );
				opR2.setImporteOPREL( new BigDecimal( "90000") );
				opR2.setMonedaOPREL( "USD" );
				opR2.setNuCuentaOPREL( "No. Cuenta" );
				opR2.setNuOperacionesOPREL( new BigDecimal( "60"));
				opR2.setTpOperacionOPREL( "Tipo");				
				lstOperacionesRelevantes.add( opR1 );
				lstOperacionesRelevantes.add( opR2 );
				*/
				repConocimientoCte.setLstOperacionesMedianas( new JRBeanCollectionDataSource(lstOperacionesMedianas) );
				repConocimientoCte.setLstOperacionesRelevantes( new JRBeanCollectionDataSource(lstOperacionesRelevantes) );
			}
			
			// Oficios Listas y Notas Periodisticas
			OficiosListasNotasPeriodisticasDTO olnp = (OficiosListasNotasPeriodisticasDTO)casosService.consultarOficiosListasNotasPeriodisticas().getResponse();
			//OficiosListasNotasPeriodisticasDTO olnp = new OficiosListasNotasPeriodisticasDTO();
			if( olnp != null )
			{
				repConocimientoCte.setOficiosListasNotas( olnp.getTxtOficiosNotasPer() );
				//repConocimientoCte.setOficiosListasNotas( "Oficios Listas y Notas Periodisticas ..................................." );
			}
			
			// Se envian todos los resultados obtenidos al reporte
			itmCCDatGen.add(repConocimientoCte);			
			parametrosReporte.put( "srCCDatosCliente",new JRBeanCollectionDataSource( itmCCDatGen ) );			
			
			
			// Se agregan el logotipo de Bancomer y la fecha de creacion del reporte
			SimpleDateFormat formatoFecha = new SimpleDateFormat( 
					Constantes.FORMATO_FECHA_REPORTE_CEDULA_CASO, 
					new Locale(Constantes.LENGUAJE_FORMATO_FECHA_MIN, Constantes.LENGUAJE_FORMATO_FECHA_MAY ) );
			
	    	String fechaCreacion = formatoFecha.format( new Date( ) );
	    	parametrosReporte.put( LOGO_BBVA, ArqSpringContext.getPropiedad( Constantes.LOGO_BANCOMER));
			parametrosReporte.put( FECHA_CREACION, fechaCreacion );
			parametrosReporte.put( NUMERO_CASO, cdCaso );
			
			response = generarPDFCedulaCaso( cdCaso, nuFolioAlerta, cdSistema, cdCliente, nuCuenta, parametrosReporte );

		}
		catch( Exception exception)
		{
			exception.printStackTrace();
		}
		return response;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private Response generarPDFCedulaCaso( 
			BigDecimal cdCaso,String nuFolioAlerta,String cdSistema,String cdCliente,String nuCuenta,			
			Map<String, Object> parametrosReporte  ) throws JRException,PropiedadNoEncontradaExcepcion, IOException 
			{
		ResponseBuilder responseBuilder = Response.ok();
    	String printFileName = null;
    	StringBuilder rutaSalidaReporte = new StringBuilder();
    	String nombrePlantilla = ArqSpringContext.getPropiedad( Constantes.NOMBRE_PLANTILLA_CEDULA_CASO );
    	File tmpFile = null;
    	try{
    		// Se a�aden los datos al reporte	    		
    		printFileName = JasperFillManager.fillReportToFile( nombrePlantilla, parametrosReporte, new JREmptyDataSource() );
    		
    		System.out.println( "Print File Name : " + printFileName );
    		// Genera Archivo PDF
    		if (printFileName != null) {
    			rutaSalidaReporte.append( ArqSpringContext.getPropiedad(  Constantes.PATH_SALIDA_REPORTES ) );	
    			StringBuilder nombrePDF = new StringBuilder();
    			nombrePDF.append( ArqSpringContext.getPropiedad( Constantes.NOMBRE_REPORTE_SALIDA_CEDULA_CASO ) );    			
    			Calendar calendar = GregorianCalendar.getInstance();
    			int anio = calendar.get( Calendar.YEAR );
    			int mes = calendar.get( Calendar.MONTH ) + 1;
    			int day = calendar.get( Calendar.DAY_OF_MONTH );
    			nombrePDF.append( Constantes.NAME_SEPARATOR );
    			nombrePDF.append( cdCaso.toString() );
    			nombrePDF.append( "_" );
    			nombrePDF.append( anio );
    			nombrePDF.append( mes );
    			nombrePDF.append( day );
    			nombrePDF.append(  Constantes.EXTENSION_PDF );
    			rutaSalidaReporte.append( nombrePDF.toString() );
    			//System.out.println( "Ruta Salida Reporte : " + rutaSalidaReporte );
    			
    			JasperExportManager.exportReportToPdfFile( printFileName, rutaSalidaReporte.toString() );
    			
    			String file = new String(rutaSalidaReporte.toString());
        		Runtime.getRuntime().exec("cmd /c start "+ file);
        		
    			/*tmpFile = new File( rutaSalidaReporte.toString() );
    			responseBuilder = Response.ok( tmpFile );
    	        responseBuilder.header("Content-Disposition", "attachment; filename=\""+ nombrePDF.toString() +"\"");*/  			
    		}   
    	}
		catch (JRException jrException) 
		{
			jrException.printStackTrace();
			responseBuilder = Response.ok( 400 );
		}
		catch (PropiedadNoEncontradaExcepcion propiedadNoEncontradaExcepcion)
		{
			propiedadNoEncontradaExcepcion.printStackTrace();
			responseBuilder = Response.ok( 400 );
		}
		return responseBuilder.build();
	}	



}
