package com.bbva.msca.back.dominio;

import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooEntity(identifierType = Tsca071PrdSbprdPK.class, versionField = "", table = "TSCA071_PRD_SBPRD", schema = "GORAPR")
@RooDbManaged(automaticallyDelete = true)
public class Tsca071PrdSbprd {
}
