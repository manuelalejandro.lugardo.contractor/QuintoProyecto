package com.bbva.msca.back.dominio;

import javax.persistence.Column;
import javax.persistence.Id;
import org.springframework.roo.addon.dbre.RooDbManaged;
import org.springframework.roo.addon.entity.RooEntity;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooDbManaged(automaticallyDelete = true)
@RooEntity(table = "TSCA007_TIPOLOGIA", schema = "GORAPR", versionField = "")
public class Tsca007Tipologia {
	
	@Id
    @Column(name = "CD_TIPOLOGIA")
    private String cdTipologia;
	
	@Column(name = "CD_USUARIO")
    private String cdUsrAlta;
	
}
