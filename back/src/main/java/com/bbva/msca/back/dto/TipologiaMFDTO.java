package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaMFDTO {

	@AnotacionAlerta(tipologia = "mf", idCampo = "cdAccion", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_CDACCION", instanceType = "1")
	private String cdAccion;
	@AnotacionAlerta(tipologia = "mf", idCampo = "tpPerJuri", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_TPPERJURI", instanceType = "1")
	private String tpPerJuri;
	@AnotacionAlerta(tipologia = "mf", idCampo = "cdOficinaGest", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_CDOFICINAGEST", instanceType = "1")
	private String cdOficinaGest;
	@AnotacionAlerta(tipologia = "mf", idCampo = "imScore", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_IMSCORE")
	private BigDecimal imScore;
	@AnotacionAlerta(tipologia = "mf", idCampo = "ctMovimiento", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_CTMOVIMIENTO", instanceType = "1")
	private String ctMovimiento;
	@AnotacionAlerta(tipologia = "mf", idCampo = "nbSucursal", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_NBSUCURSAL", instanceType = "1")
	private String nbSucursal;
	@AnotacionAlerta(tipologia = "mf", idCampo = "nbEstado", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_NBESTADO", instanceType = "1")
	private String nbEstado;
	@AnotacionAlerta(tipologia = "mf", idCampo = "nbDescripcion", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_NBDESCRIPCION", instanceType = "1")
	private String nbDescripcion;
	@AnotacionAlerta(tipologia = "mf", idCampo = "nbFiller1", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_NBFILLER1", instanceType = "1")
	private String nbFiller1;
	@AnotacionAlerta(tipologia = "mf", idCampo = "nbFiller2", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_NBFILLER2", instanceType = "1")
	private String nbFiller2;
	@AnotacionAlerta(tipologia = "mf", idCampo = "cdFoco", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_CDFOCO", instanceType = "1")
	private String cdFoco;
	@AnotacionAlerta(tipologia = "mf", idCampo = "cdPriorSc", posicion = "12", nbEtiqueta = "MSCK_LBL_01002_CDPRIORSC", instanceType = "1")
	private String cdPriorSc;
	
	
	public String getCdAccion() {
		return cdAccion;
	}
	public void setCdAccion(String cdAccion) {
		this.cdAccion = cdAccion;
	}
	public String getTpPerJuri() {
		return tpPerJuri;
	}
	public void setTpPerJuri(String tpPerJuri) {
		this.tpPerJuri = tpPerJuri;
	}
	public String getCdOficinaGest() {
		return cdOficinaGest;
	}
	public void setCdOficinaGest(String cdOficinaGest) {
		this.cdOficinaGest = cdOficinaGest;
	}
	public BigDecimal getImScore() {
		return imScore;
	}
	public void setImScore(BigDecimal imScore) {
		this.imScore = imScore;
	}
	public String getCtMovimiento() {
		return ctMovimiento;
	}
	public void setCtMovimiento(String ctMovimiento) {
		this.ctMovimiento = ctMovimiento;
	}
	public String getNbSucursal() {
		return nbSucursal;
	}
	public void setNbSucursal(String nbSucursal) {
		this.nbSucursal = nbSucursal;
	}
	public String getNbEstado() {
		return nbEstado;
	}
	public void setNbEstado(String nbEstado) {
		this.nbEstado = nbEstado;
	}
	public String getNbDescripcion() {
		return nbDescripcion;
	}
	public void setNbDescripcion(String nbDescripcion) {
		this.nbDescripcion = nbDescripcion;
	}
	public String getNbFiller1() {
		return nbFiller1;
	}
	public void setNbFiller1(String nbFiller1) {
		this.nbFiller1 = nbFiller1;
	}
	public String getNbFiller2() {
		return nbFiller2;
	}
	public void setNbFiller2(String nbFiller2) {
		this.nbFiller2 = nbFiller2;
	}
	public String getCdFoco() {
		return cdFoco;
	}
	public void setCdFoco(String cdFoco) {
		this.cdFoco = cdFoco;
	}
	public String getCdPriorSc() {
		return cdPriorSc;
	}
	public void setCdPriorSc(String cdPriorSc) {
		this.cdPriorSc = cdPriorSc;
	}

}
