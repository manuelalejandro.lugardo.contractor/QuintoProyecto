package com.bbva.msca.back.srv;


import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.bbva.msca.back.dto.SesionDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/SesionService")
public interface SesionService {
	
	// PARAMETRIA -FN01 SESION
	@GET
	@Path("/consultarSesiones")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarSesiones();
	
	@POST
	@Path("/altaSesion")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric creaSesion(SesionDTO sesion);
	
	@POST
	@Path("/modificaSesion")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric modificaSesion(SesionDTO sesion);
}
