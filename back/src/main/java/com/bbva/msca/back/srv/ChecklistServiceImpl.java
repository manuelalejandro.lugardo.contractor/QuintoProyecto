package com.bbva.msca.back.srv;

import java.util.List;
import com.bbva.msca.back.dao.ChecklistDAO;
import com.bbva.msca.back.dto.ChecklistDTO;
import com.bbva.msca.back.util.Constantes;
import com.bbva.msca.back.util.MensajesI18n;
import com.bbva.msca.back.util.ResponseGeneric;

public class ChecklistServiceImpl implements ChecklistService {

	ChecklistDAO checklistDAO = new ChecklistDAO();

	@Override
	public ResponseGeneric consultarChecklists(ChecklistDTO checklistDTO) {
		
		ResponseGeneric response = new ResponseGeneric();
		ChecklistDAO checklistDAO = new ChecklistDAO();
		try{
			List<ChecklistDTO> checklistDTO2 = checklistDAO.getChecklist(checklistDTO);
			response.setResponse(checklistDTO2);
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
		
	}

	@Override
	public ResponseGeneric creaChecklist(ChecklistDTO checklistDTO) {
		ResponseGeneric response = new ResponseGeneric();
		try {
			switch (this.checklistDAO.registraChecklist(checklistDTO)) {
			case Constantes.FIN_OK:
				response.setResponse(checklistDTO);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_ER:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric modificaChecklist(ChecklistDTO checklistDTO) {
		ResponseGeneric response = new ResponseGeneric();
		try {
			switch (this.checklistDAO.actualizaChecklist(checklistDTO)) {
			case Constantes.FIN_OK:
				response.setResponse(checklistDTO);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_ER:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric modificaChecklists(List<ChecklistDTO> lstChecklistDTO) {
		ResponseGeneric response = new ResponseGeneric();
		try {
			switch (this.checklistDAO.actualizaChecklists(lstChecklistDTO)) {
			case Constantes.FIN_OK:
				response.setResponse(lstChecklistDTO);
				response.setCdMensaje("1");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			case Constantes.FIN_ER:
				response.setCdMensaje("0");
				response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
				break;
			}
		} catch (Exception e) {
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

}
