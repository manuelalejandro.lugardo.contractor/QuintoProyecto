package com.bbva.msca.back.dto;

public class TransaccionClienteTdcDTO {
	
	//tabla tdc parte1 transacciones
	private String instrumentosMonetariosTDC;
	private String importeTDC;
	private String noOperacionesTDC;
	private String origenTransTDC;
	private String destinoTransTDC;
	
	public String getInstrumentosMonetariosTDC() {
		return instrumentosMonetariosTDC;
	}
	public void setInstrumentosMonetariosTDC(String instrumentosMonetariosTDC) {
		this.instrumentosMonetariosTDC = instrumentosMonetariosTDC;
	}
	public String getImporteTDC() {
		return importeTDC;
	}
	public void setImporteTDC(String importeTDC) {
		this.importeTDC = importeTDC;
	}
	public String getOrigenTransTDC() {
		return origenTransTDC;
	}
	public void setOrigenTransTDC(String origenTransTDC) {
		this.origenTransTDC = origenTransTDC;
	}
	public String getDestinoTransTDC() {
		return destinoTransTDC;
	}
	public void setDestinoTransTDC(String destinoTransTDC) {
		this.destinoTransTDC = destinoTransTDC;
	}
	public String getNoOperacionesTDC() {
		return noOperacionesTDC;
	}
	public void setNoOperacionesTDC(String noOperacionesTDC) {
		this.noOperacionesTDC = noOperacionesTDC;
	}




}