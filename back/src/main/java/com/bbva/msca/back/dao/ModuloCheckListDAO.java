package com.bbva.msca.back.dao;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.bbva.msca.back.dto.BusquedaCheckListDTO;
import com.bbva.msca.back.dto.ModuloCheckListDTO;

public class ModuloCheckListDAO {
	
	public List<ModuloCheckListDTO> getModuloCheckList(){
		List<ModuloCheckListDTO> lstModuloCheckListDTO = new ArrayList<ModuloCheckListDTO>();
		ModuloCheckListDTO moduloCheckListDTO = null;
		
		for(int i=0;i<10;i++){
			moduloCheckListDTO = new ModuloCheckListDTO();
			
			moduloCheckListDTO.setNuCliente(139401 + i);
			moduloCheckListDTO.setNbCliente("Ramon");
			moduloCheckListDTO.setNbTipologia("M2");
			moduloCheckListDTO.setNbPregunta("Y");
			moduloCheckListDTO.setStLiberadaSIA(false);
			moduloCheckListDTO.setStCorreoPrevio(false);
			moduloCheckListDTO.setStCorreoFolio(false);
			moduloCheckListDTO.setStCalificacion(false);
			moduloCheckListDTO.setStInfoFiduciario(false);
			moduloCheckListDTO.setStRelevantes(false);
			moduloCheckListDTO.setStOpis(false);
			moduloCheckListDTO.setStListaBBVA(false);
			moduloCheckListDTO.setStBloqueoCtas(false);

			lstModuloCheckListDTO.add(moduloCheckListDTO);
			
		}

		return lstModuloCheckListDTO;
		
	}
	public static BusquedaCheckListDTO getBusquedaCheckList(){
		BusquedaCheckListDTO busquedaCheckListDTO = new BusquedaCheckListDTO();
		
		busquedaCheckListDTO.setNuOficio(1);
		busquedaCheckListDTO.setFhRecepcion(new Date());
		busquedaCheckListDTO.setCdTipologia(1);
		


		
		return busquedaCheckListDTO;

}

	
	
}
