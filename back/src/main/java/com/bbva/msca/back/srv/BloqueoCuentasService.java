package com.bbva.msca.back.srv;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.Consumes;
import com.bbva.msca.back.dto.BusquedaBloqueoCuentasDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/BloqueoCuentasService")

public interface BloqueoCuentasService {
	
	/*@GET
	@Path("/consultarBloqueoCuentas")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarBloqueoCuentas();
	
	@GET
	@Path("/busquedaBloqueoCuentas")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric busquedaBloqueoCuentas();*/
	
	@POST
	@Path("/consultarBloqueoCuentasFiltro/")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarBloqueoCuentasFiltro(BusquedaBloqueoCuentasDTO busquedaBloqueoCuentasDTO);
	
	@GET
	@Path("/consultarBloqueoCuentas/")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarBloqueoCuentas();

}
