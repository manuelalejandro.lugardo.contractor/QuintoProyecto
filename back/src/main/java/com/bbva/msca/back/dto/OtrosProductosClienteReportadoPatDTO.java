package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

public class OtrosProductosClienteReportadoPatDTO {

	private Date fhAperturaOPCRP;
    private String nuCuentaOPCRP;
    private String nbProductoOPCRP;
    private String nbEstatusOPCRP;
    private String nbTitularOPCRP;
    private String nuCtePartOPCRP;
    private String nbParticipesOPCRP;
    private BigDecimal imProductoOPCRP;
    private String nuCtaPartOPCRP;
    
    
	public Date getFhAperturaOPCRP() {
		return fhAperturaOPCRP;
	}
	public void setFhAperturaOPCRP(Date fhAperturaOPCRP) {
		this.fhAperturaOPCRP = fhAperturaOPCRP;
	}
	public String getNuCuentaOPCRP() {
		return nuCuentaOPCRP;
	}
	public void setNuCuentaOPCRP(String nuCuentaOPCRP) {
		this.nuCuentaOPCRP = nuCuentaOPCRP;
	}
	public String getNbProductoOPCRP() {
		return nbProductoOPCRP;
	}
	public void setNbProductoOPCRP(String nbProductoOPCRP) {
		this.nbProductoOPCRP = nbProductoOPCRP;
	}
	public String getNbEstatusOPCRP() {
		return nbEstatusOPCRP;
	}
	public void setNbEstatusOPCRP(String nbEstatusOPCRP) {
		this.nbEstatusOPCRP = nbEstatusOPCRP;
	}
	public String getNbTitularOPCRP() {
		return nbTitularOPCRP;
	}
	public void setNbTitularOPCRP(String nbTitularOPCRP) {
		this.nbTitularOPCRP = nbTitularOPCRP;
	}
	public String getNuCtePartOPCRP() {
		return nuCtePartOPCRP;
	}
	public void setNuCtePartOPCRP(String nuCtePartOPCRP) {
		this.nuCtePartOPCRP = nuCtePartOPCRP;
	}
	public String getNbParticipesOPCRP() {
		return nbParticipesOPCRP;
	}
	public void setNbParticipesOPCRP(String nbParticipesOPCRP) {
		this.nbParticipesOPCRP = nbParticipesOPCRP;
	}
	public BigDecimal getImProductoOPCRP() {
		return imProductoOPCRP;
	}
	public void setImProductoOPCRP(BigDecimal imProductoOPCRP) {
		this.imProductoOPCRP = imProductoOPCRP;
	}
	public String getNuCtaPartOPCRP() {
		return nuCtaPartOPCRP;
	}
	public void setNuCtaPartOPCRP(String nuCtaPartOPCRP) {
		this.nuCtaPartOPCRP = nuCtaPartOPCRP;
	}
    
    


    
    
	
	

}
