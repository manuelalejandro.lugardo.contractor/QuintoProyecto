package com.bbva.msca.back.dao;


import java.math.BigDecimal;
import java.text.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;



import org.hibernate.transform.Transformers;
import org.hibernate.type.TimestampType;
import org.hibernate.type.StringType;
import org.springframework.transaction.annotation.Transactional;

import com.bbva.msca.back.dominio.Tsca011EnvCuest;
import com.bbva.msca.back.dominio.Tsca011EnvCuestPK;
import com.bbva.msca.back.dto.OpinionFuncionarioDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

public class OpinionFuncionarioDAO {
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();

	@SuppressWarnings("unchecked")
	public List<OpinionFuncionarioDTO> getOpinionFuncionario(BigDecimal tpCuestionario,String cdSistema,String folioAlerta) throws Exception {
		
		List<OpinionFuncionarioDTO> opinionFuncionarioDTO = new ArrayList<OpinionFuncionarioDTO>();
		Query query = em.createNativeQuery(Consultas.getConsulta("getOpinionFuncionario"));
		
		query.setParameter(1, tpCuestionario);
		query.setParameter(2, tpCuestionario);
		query.setParameter(3, cdSistema);
		query.setParameter(4, folioAlerta);
		query.unwrap(org.hibernate.SQLQuery.class)

		.addScalar("nuPregunta")
		.addScalar("nbPregunta")
		.addScalar("txtRespuesta")

		.setResultTransformer(Transformers.aliasToBean(OpinionFuncionarioDTO.class));
		opinionFuncionarioDTO = (List<OpinionFuncionarioDTO>) query.getResultList();

		return opinionFuncionarioDTO;
	}
	
	@SuppressWarnings("unchecked")
	public BigDecimal getOpinionFuncionarioCuestionario(String cdSistema,String folioAlerta){
		BigDecimal tpCuestionario;
		Query query;
		try {
			query = em.createNativeQuery(Consultas.getConsulta("getOpinionFuncionarioCuestionario"));
			query.setParameter(1, cdSistema);
			query.setParameter(2, folioAlerta);
			
			tpCuestionario = (BigDecimal)query.getSingleResult();
		} catch (Exception e) {
			tpCuestionario = new BigDecimal(0);
			e.printStackTrace();
		}		
		return tpCuestionario;
	}
	
	
	List<OpinionFuncionarioDTO> opinionFuncionarioEncDTO = new ArrayList<OpinionFuncionarioDTO>();
	Date fechaRespuestaIni = null;
	@SuppressWarnings("unchecked")
	public List<OpinionFuncionarioDTO> getOpinionFuncionarioEncabezado(String cdSistema,String folioAlerta) throws Exception {
		
		
		Query query = em.createNativeQuery(Consultas.getConsulta("getOpinionFuncionarioAnalisisEncabezado"));
		
		query.setParameter(1, cdSistema);
		query.setParameter(2, folioAlerta);
		query.unwrap(org.hibernate.SQLQuery.class)
		
		.addScalar("fhEnvioCuest", TimestampType.INSTANCE)
		.addScalar("nbFuncionarioCuest")
		.addScalar("estatusCuest")
		.addScalar("fhRespuestaCuest", TimestampType.INSTANCE)
		.addScalar("fhVencimientoCuest", TimestampType.INSTANCE)
		.addScalar("fhVencimientoCuest2")
		
		.setResultTransformer(Transformers.aliasToBean(OpinionFuncionarioDTO.class));
		opinionFuncionarioEncDTO = (List<OpinionFuncionarioDTO>)query.getResultList();
			if(!opinionFuncionarioEncDTO.isEmpty()){
				
				fechaRespuestaIni = opinionFuncionarioEncDTO.get(0).getFhRespuestaCuest();
			
			}	else{
				fechaRespuestaIni = null;
			}
		return opinionFuncionarioEncDTO;
	}

	
	@SuppressWarnings({ "unchecked" })
	@Transactional
	public Tsca011EnvCuest addOpinionFuncionario( String cdSistema, String nuFolioAlerta, OpinionFuncionarioDTO opinionFuncionarioDTO) throws Exception{
		
		Tsca011EnvCuestPK pkTsca011EnvCuest = new Tsca011EnvCuestPK( nuFolioAlerta, cdSistema );	
		Tsca011EnvCuest tsca011EnvCuest = Tsca011EnvCuest.findTsca011EnvCuest( pkTsca011EnvCuest );
		
		Date fechaActual = new Date();
	
				if(tsca011EnvCuest != null && fechaRespuestaIni != null){//Cuando el registro existe y tiene fecha de respuesta
					
					tsca011EnvCuest.getId();
					
					tsca011EnvCuest.setFhEnvio(fechaActual);
					tsca011EnvCuest.setStCuestionario('2');
					tsca011EnvCuest.setTxComentario(opinionFuncionarioDTO.getTxtComentarioCuest());
					
					tsca011EnvCuest.merge();
					tsca011EnvCuest.flush();
					
				}else if(tsca011EnvCuest != null && fechaRespuestaIni == null){
					tsca011EnvCuest.getId();
					
					List<OpinionFuncionarioDTO> cdFuncionario = new ArrayList<OpinionFuncionarioDTO>();
					Query query = em.createNativeQuery(Consultas.getConsulta("getOpinionFuncionarioUsuario"));

					query.setParameter(1, opinionFuncionarioDTO.getCdOficinaGest());
					query.setParameter(2, cdSistema);
					query.setParameter(3, nuFolioAlerta);
					query.unwrap(org.hibernate.SQLQuery.class)
						.addScalar("cdFuncionarioOF")		
						.setResultTransformer(Transformers.aliasToBean(OpinionFuncionarioDTO.class));
					cdFuncionario = (List<OpinionFuncionarioDTO>)query.getResultList();
					
					tsca011EnvCuest.setCdUsuario(cdFuncionario.get(0).getCdFuncionarioOF());
					tsca011EnvCuest.setFhEnvio(fechaActual);
					tsca011EnvCuest.setFhRecordatorio(fechaRecordatorioOF());
					tsca011EnvCuest.setFhVencimiento(fechaVencimientoOF());
					tsca011EnvCuest.setStCuestionario('1');
					
					tsca011EnvCuest.merge();
					tsca011EnvCuest.flush();
				}else {//Cuando el registro no existe en la tabla
						//(CD_ALERTA, CD_SISTEMA, FH_ENVIO, FH_RECORDATORIO, FH_VENCIMIENTO, CD_USUARIO, ST_CUESTIONARIO)
	
					tsca011EnvCuest = new Tsca011EnvCuest();
					
					List<OpinionFuncionarioDTO> cdFuncionarioOF = new ArrayList<OpinionFuncionarioDTO>();
					Query query = em.createNativeQuery(Consultas.getConsulta("getOpinionFuncionarioUsuario"));
					
					query.setParameter(1, opinionFuncionarioDTO.getCdOficinaGest());
					query.setParameter(2, cdSistema);
					query.setParameter(3, nuFolioAlerta);
					query.unwrap(org.hibernate.SQLQuery.class)
						.addScalar("cdFuncionarioOF")		
						.setResultTransformer(Transformers.aliasToBean(OpinionFuncionarioDTO.class));
					cdFuncionarioOF = (List<OpinionFuncionarioDTO>)query.getResultList();
					
						tsca011EnvCuest.setId( pkTsca011EnvCuest );
						tsca011EnvCuest.setFhEnvio(fechaActual);
						tsca011EnvCuest.setFhRecordatorio(fechaRecordatorioOF());
						tsca011EnvCuest.setFhVencimiento(fechaVencimientoOF());
						tsca011EnvCuest.setCdUsuario(cdFuncionarioOF.get(0).getCdFuncionarioOF());
						tsca011EnvCuest.setStCuestionario('1');
						
						tsca011EnvCuest.persist();
						tsca011EnvCuest.flush();					
				}
				
		return tsca011EnvCuest;
	}
	
	
	
	public static Date fechaRecordatorioOF(){
		Date fecha=new Date();
	    Calendar calendario = Calendar.getInstance();
	    SimpleDateFormat formatter;

	    int dias = 5;
	    for(int i=0;i<dias;){
	        calendario.add(Calendar.DAY_OF_MONTH, 1);
	        if(calendario.get(Calendar.DAY_OF_WEEK) <=5)     {
	            i++;
	        }
	    }
	    fecha=calendario.getTime(); 
	    formatter = new SimpleDateFormat("dd/MM/yyyy");
	    Date fechaRecordatorio = new Date();
	    try{
	    	String fechaRec = formatter.format(fecha);
		    fechaRecordatorio = formatter.parse(fechaRec);
		    System.out.println(fechaRecordatorio);
	    
		}catch(ParseException  e) {
		  	System.out.println("fail");
		}
		
	    return fechaRecordatorio;
	}
	
	
	public static Date fechaVencimientoOF(){
		Date fecha=new Date();
	    Calendar calendario = Calendar.getInstance();
	    SimpleDateFormat formatter;

	    int dias = 10;
	    for(int i=0;i<dias;){
	        calendario.add(Calendar.DAY_OF_MONTH, 1);
	        if(calendario.get(Calendar.DAY_OF_WEEK) <=5){
	            i++;
	        }
	    }
	    fecha=calendario.getTime(); 
	    formatter = new SimpleDateFormat("dd/MM/yyyy");
	    Date fechaVencimiento = new Date();
	    try{
		    String fechaRec = formatter.format(fecha);
		    fechaVencimiento = formatter.parse(fechaRec);
		    System.out.println(fechaVencimiento);
		}catch(ParseException  e) {
		  	System.out.println("fail");
		}
		
	    return fechaVencimiento;
	}
	
	
	
	
}
