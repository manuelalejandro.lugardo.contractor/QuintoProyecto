package com.bbva.msca.back.srv;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import com.bbva.msca.back.dto.MatrizDTO;
import com.bbva.msca.back.util.ResponseGeneric;

@Path("/matrizService")
public interface MatrizService {
		
	// PARAMETRIA - FN05 MATRIZ DE CRITERIOS
	@GET
	@Path("/consultarMatrices")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarMatrices();
	
	@GET
	@Path("/consultarMatricesGerencia/{cdGerencia}")
	@Produces({ MediaType.APPLICATION_JSON })
	public ResponseGeneric consultarMatricesGerencia(@PathParam("cdGerencia") Integer cdGerencia);
	
	@POST
	@Path("/altaMatriz")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric creaMatriz(MatrizDTO matriz);
	
	@POST
	@Path("/modificaMatriz")
	@Consumes({MediaType.APPLICATION_JSON})
	@Produces({MediaType.APPLICATION_JSON})
	public ResponseGeneric modificaMatriz(MatrizDTO matriz);

}
