package com.bbva.msca.back.dto;

import java.math.BigDecimal;
import java.util.Date;

import com.bbva.msca.back.util.AnotacionAlerta;

public class TipologiaMDDTO {

	@AnotacionAlerta(tipologia = "md", idCampo = "cdStAlerta", posicion = "1", nbEtiqueta = "MSCK_LBL_01002_STALERTA")
	private BigDecimal cdStAlerta;
	@AnotacionAlerta(tipologia = "md", idCampo = "imMontoAlerta", posicion = "2", nbEtiqueta = "MSCK_LBL_01002_IMMONTOALERTA", formatter = "formatterMoney")
	private BigDecimal imMontoAlerta;
	@AnotacionAlerta(tipologia = "md", idCampo = "ctMovimiento", posicion = "3", nbEtiqueta = "MSCK_LBL_01002_CTMOVIMIENTO", instanceType = "1")
	private String ctMovimiento;
	@AnotacionAlerta(tipologia = "md", idCampo = "fhNac", posicion = "4", nbEtiqueta = "MSCK_LBL_01002_FHNAC", instanceType = "2")
	private Date fhNac;
	@AnotacionAlerta(tipologia = "md", idCampo = "nuEdad", posicion = "5", nbEtiqueta = "MSCK_LBL_01002_NUEDAD")
	private BigDecimal nuEdad;
	@AnotacionAlerta(tipologia = "md", idCampo = "cdSector", posicion = "6", nbEtiqueta = "MSCK_LBL_01002_CDSECTOR", instanceType = "1")
	private String cdSector;
	@AnotacionAlerta(tipologia = "md", idCampo = "nbEstado", posicion = "7", nbEtiqueta = "MSCK_LBL_01002_NBESTADO", instanceType = "1")
	private String nbEstado;
	@AnotacionAlerta(tipologia = "md", idCampo = "nuDep", posicion = "8", nbEtiqueta = "MSCK_LBL_01002_NUDEP")
	private BigDecimal nuDep;
	@AnotacionAlerta(tipologia = "md", idCampo = "imImporteDep", posicion = "9", nbEtiqueta = "MSCK_LBL_01002_IMIMPORTEDEP")
	private BigDecimal imImporteDep;
	@AnotacionAlerta(tipologia = "md", idCampo = "imImporteRet", posicion = "10", nbEtiqueta = "MSCK_LBL_01002_IMIMPORTERET")
	private BigDecimal imImporteRet;
	@AnotacionAlerta(tipologia = "md", idCampo = "nuRet", posicion = "11", nbEtiqueta = "MSCK_LBL_01002_NURET", formatter = "formatterMoney")
	private BigDecimal nuRet;
	@AnotacionAlerta(tipologia = "md", idCampo = "nuCv", posicion = "12", nbEtiqueta = "MSCK_LBL_01002_NUCV", formatter = "formatterMoney")
	private BigDecimal nuCv;
	@AnotacionAlerta(tipologia = "md", idCampo = "imImporteCv", posicion = "13", nbEtiqueta = "MSCK_LBL_01002_IMIMPORTECV")
	private BigDecimal imImporteCv;
	@AnotacionAlerta(tipologia = "md", idCampo = "cdMovOtro", posicion = "14", nbEtiqueta = "MSCK_LBL_01002_CDMOVOTRO")
	private BigDecimal cdMovOtro;
	@AnotacionAlerta(tipologia = "md", idCampo = "imImpOtro", posicion = "15", nbEtiqueta = "MSCK_LBL_01002_IMIMPOTRO", formatter = "formatterMoney")
	private BigDecimal imImpOtro;
	@AnotacionAlerta(tipologia = "md", idCampo = "fhUltima", posicion = "16", nbEtiqueta = "MSCK_LBL_01002_FHULTIMA", instanceType = "2", formatter = "formatterDate")
	private Date fhUltima;
	@AnotacionAlerta(tipologia = "md", idCampo = "tpCambio", posicion = "17", nbEtiqueta = "MSCK_LBL_01002_TPCAMBIO")
	private BigDecimal tpCambio;
	@AnotacionAlerta(tipologia = "md", idCampo = "crGestorCte", posicion = "18", nbEtiqueta = "MSCK_LBL_01002_CRGESTORCTE", instanceType = "1")
	private String crGestorCte;
	@AnotacionAlerta(tipologia = "md", idCampo = "nuPuntaje", posicion = "19", nbEtiqueta = "MSCK_LBL_01002_NUPUNTAJE")
	private BigDecimal nuPuntaje;
	@AnotacionAlerta(tipologia = "md", idCampo = "imMensAut", posicion = "20", nbEtiqueta = "MSCK_LBL_01002_IMMENSAUT", formatter = "formatterMoney")
	private BigDecimal imMensAut;
	
	
	public BigDecimal getCdStAlerta() {
		return cdStAlerta;
	}
	public void setCdStAlerta(BigDecimal cdStAlerta) {
		this.cdStAlerta = cdStAlerta;
	}
	public BigDecimal getImMontoAlerta() {
		return imMontoAlerta;
	}
	public void setImMontoAlerta(BigDecimal imMontoAlerta) {
		this.imMontoAlerta = imMontoAlerta;
	}
	public String getCtMovimiento() {
		return ctMovimiento;
	}
	public void setCtMovimiento(String ctMovimiento) {
		this.ctMovimiento = ctMovimiento;
	}
	public Date getFhNac() {
		return fhNac;
	}
	public void setFhNac(Date fhNac) {
		this.fhNac = fhNac;
	}
	public BigDecimal getNuEdad() {
		return nuEdad;
	}
	public void setNuEdad(BigDecimal nuEdad) {
		this.nuEdad = nuEdad;
	}
	public String getCdSector() {
		return cdSector;
	}
	public void setCdSector(String cdSector) {
		this.cdSector = cdSector;
	}
	public String getNbEstado() {
		return nbEstado;
	}
	public void setNbEstado(String nbEstado) {
		this.nbEstado = nbEstado;
	}
	public BigDecimal getNuDep() {
		return nuDep;
	}
	public void setNuDep(BigDecimal nuDep) {
		this.nuDep = nuDep;
	}
	public BigDecimal getImImporteDep() {
		return imImporteDep;
	}
	public void setImImporteDep(BigDecimal imImporteDep) {
		this.imImporteDep = imImporteDep;
	}
	public BigDecimal getImImporteRet() {
		return imImporteRet;
	}
	public void setImImporteRet(BigDecimal imImporteRet) {
		this.imImporteRet = imImporteRet;
	}
	public BigDecimal getNuRet() {
		return nuRet;
	}
	public void setNuRet(BigDecimal nuRet) {
		this.nuRet = nuRet;
	}
	public BigDecimal getNuCv() {
		return nuCv;
	}
	public void setNuCv(BigDecimal nuCv) {
		this.nuCv = nuCv;
	}
	public BigDecimal getImImporteCv() {
		return imImporteCv;
	}
	public void setImImporteCv(BigDecimal imImporteCv) {
		this.imImporteCv = imImporteCv;
	}
	public BigDecimal getCdMovOtro() {
		return cdMovOtro;
	}
	public void setCdMovOtro(BigDecimal cdMovOtro) {
		this.cdMovOtro = cdMovOtro;
	}
	public BigDecimal getImImpOtro() {
		return imImpOtro;
	}
	public void setImImpOtro(BigDecimal imImpOtro) {
		this.imImpOtro = imImpOtro;
	}
	public Date getFhUltima() {
		return fhUltima;
	}
	public void setFhUltima(Date fhUltima) {
		this.fhUltima = fhUltima;
	}
	public BigDecimal getTpCambio() {
		return tpCambio;
	}
	public void setTpCambio(BigDecimal tpCambio) {
		this.tpCambio = tpCambio;
	}
	public String getCrGestorCte() {
		return crGestorCte;
	}
	public void setCrGestorCte(String crGestorCte) {
		this.crGestorCte = crGestorCte;
	}
	public BigDecimal getNuPuntaje() {
		return nuPuntaje;
	}
	public void setNuPuntaje(BigDecimal nuPuntaje) {
		this.nuPuntaje = nuPuntaje;
	}
	public BigDecimal getImMensAut() {
		return imMensAut;
	}
	public void setImMensAut(BigDecimal imMensAut) {
		this.imMensAut = imMensAut;
	}
}
