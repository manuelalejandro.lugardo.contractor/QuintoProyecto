package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import com.bbva.msca.back.dto.OtrosClientesCasoDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import org.hibernate.type.StringType;

public class OtrosClientesCasoDAO {

	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();
	
	@SuppressWarnings("unchecked")
	public List<OtrosClientesCasoDTO> getOtrosClientes(BigDecimal cdCaso) throws Exception{ 
	 	 List<OtrosClientesCasoDTO> datosAccionista = null;
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getOtrosClientes"));
			 queryCR.setParameter(1,cdCaso); 
			 	 queryCR.unwrap(org.hibernate.SQLQuery.class)
		 		.addScalar("cdCliente",StringType.INSTANCE)
		        .addScalar("nbCliente")
		         .addScalar("cdRFC")
		        .addScalar("nbDomicilio")
		        .addScalar("nbSector",StringType.INSTANCE)
 			.setResultTransformer(Transformers.aliasToBean(OtrosClientesCasoDTO.class));
			
			 	datosAccionista = (List<OtrosClientesCasoDTO>)queryCR.getResultList();

		return datosAccionista;
		
} 

}