package com.bbva.msca.back.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.bbva.msca.back.dto.AlertaDTO;
import com.bbva.msca.back.dto.ParametrosBusquedaCasoDTO;


import javax.persistence.EntityManager;
import javax.persistence.Query;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;

import org.hibernate.transform.Transformers;
import org.hibernate.type.TimestampType;
import org.springframework.stereotype.Component;

@Component
public class DictamenDAO {
	
	BdSac bdSca = new BdSac();
	private EntityManager em = bdSca.getEntityManager();
	
	@SuppressWarnings("unchecked")
	public List<AlertaDTO> getConsultaCasosDictamenSupervisor(ParametrosBusquedaCasoDTO parametrosBusquedaCasoDTO,BigDecimal cdSupervisor) throws Exception{
		
		List<AlertaDTO> lstAlertasDTO = null;
		 String consulta = Consultas.getConsulta("getConsultaCasosDictamenSupervisor");
		 String filtro = "";
		 String condicion = " WHERE ";
		 boolean isFiltro = false;
		 SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy", new Locale("es", "ES"));
		 if( parametrosBusquedaCasoDTO != null){
			 
			 if(parametrosBusquedaCasoDTO.getNuCaso() != null){
				 filtro = filtro + " CP.cdCaso = "+parametrosBusquedaCasoDTO.getNuCaso();
				 isFiltro = true;
				
			 } 
			 if(parametrosBusquedaCasoDTO.getNuFolio() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.nuFolioAlerta = "+parametrosBusquedaCasoDTO.getNuFolio();
				 }else{
					 filtro = filtro + " AND CP.nuFolioAlerta = "+parametrosBusquedaCasoDTO.getNuFolio();
				 }
				 
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getNuCliente() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.nuCliente = '"+parametrosBusquedaCasoDTO.getNuCliente().toUpperCase()+"'";
				 }else{
					 filtro = filtro + " AND CP.nuCliente = '"+parametrosBusquedaCasoDTO.getNuCliente().toUpperCase()+"'";
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getNbCliente() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + "  CP.nbCliente LIKE '%"+ parametrosBusquedaCasoDTO.getNbCliente().toUpperCase()+"%'";
				 }else{
					 filtro = filtro + " AND CP.nbCliente LIKE '%"+ parametrosBusquedaCasoDTO.getNbCliente().toUpperCase()+"%'";
				 }
				 isFiltro = true;
				 
			 }
			 if(parametrosBusquedaCasoDTO.getNuCuenta() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " (SUBSTR(CP.nuCuenta,11,20) = '"+ parametrosBusquedaCasoDTO.getNuCuenta()+"'" + "OR CP.nuCuenta = '" + parametrosBusquedaCasoDTO.getNuCuenta() +"')";
				 }else{
					 filtro = filtro + " AND (SUBSTR(CP.nuCuenta,11,20) = '"+ parametrosBusquedaCasoDTO.getNuCuenta()+"'" + "OR CP.nuCuenta = '" + parametrosBusquedaCasoDTO.getNuCuenta() +"')";

				 }
				
		isFiltro = true;
		 }
			 if(parametrosBusquedaCasoDTO.getCdTipologia() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdTipologia = '"+ parametrosBusquedaCasoDTO.getCdTipologia().toUpperCase()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdTipologia = '"+ parametrosBusquedaCasoDTO.getCdTipologia().toUpperCase()+"'";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdSesion() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdSesion = '"+ parametrosBusquedaCasoDTO.getCdSesion()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdSesion = '"+ parametrosBusquedaCasoDTO.getCdSesion()+"'";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdTPersona() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdTPersona = '"+ parametrosBusquedaCasoDTO.getCdTPersona().toUpperCase()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdTPersona = '"+ parametrosBusquedaCasoDTO.getCdTPersona().toUpperCase()+"'";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdSegmento() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdSegmento = '"+ parametrosBusquedaCasoDTO.getCdSegmento().toUpperCase()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdSegmento = '"+ parametrosBusquedaCasoDTO.getCdSegmento().toUpperCase()+"'";
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getImImporte() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.imAlerta " + parametrosBusquedaCasoDTO.getCdImporte()  + parametrosBusquedaCasoDTO.getImImporte();
				 }else{
					 filtro = filtro + " AND CP.imAlerta "  + parametrosBusquedaCasoDTO.getCdImporte() +  parametrosBusquedaCasoDTO.getImImporte();
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getNuPtsMatriz() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.nuScoreMc " + parametrosBusquedaCasoDTO.getCdPtsMatriz() + parametrosBusquedaCasoDTO.getNuPtsMatriz();
				 }else{
					 filtro = filtro + " AND CP.nuScoreMc " + parametrosBusquedaCasoDTO.getCdPtsMatriz() + parametrosBusquedaCasoDTO.getNuPtsMatriz();
				 }
				 isFiltro = true;
				 
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getNuCalificacion() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.nuScoreMdl  " + parametrosBusquedaCasoDTO.getCdCalificacion() + parametrosBusquedaCasoDTO.getNuCalificacion();
				 }else{
					 filtro = filtro + " AND CP.nuScoreMdl "+ parametrosBusquedaCasoDTO.getCdCalificacion() + parametrosBusquedaCasoDTO.getNuCalificacion();
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdDictamenF() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdDictamenF = '"+ parametrosBusquedaCasoDTO.getCdDictamenF()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdDictamenF = '"+ parametrosBusquedaCasoDTO.getCdDictamenF()+"'";
				 }
				 isFiltro = true;
			
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdDictamenP() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdDictamenP = '"+ parametrosBusquedaCasoDTO.getCdDictamenP()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdDictamenP = '"+ parametrosBusquedaCasoDTO.getCdDictamenP()+"'";
				 }
				 isFiltro = true;
			
			 }
			 if(parametrosBusquedaCasoDTO.getCdConsultor() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdConsultor = '"+ parametrosBusquedaCasoDTO.getCdConsultor()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdConsultor = '"+ parametrosBusquedaCasoDTO.getCdConsultor()+"'";
				 }
				 isFiltro = true;
			
			 }
			 
			 if(parametrosBusquedaCasoDTO.getFhRevisionDel() != null || parametrosBusquedaCasoDTO.getFhRevisionAl() != null ){
				 if(parametrosBusquedaCasoDTO.getFhRevisionDel() != null && parametrosBusquedaCasoDTO.getFhRevisionAl() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhAsignacion BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhRevisionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhRevisionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND CP.fhAsignacion BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhRevisionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhRevisionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhRevisionDel() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhRevisionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND CP.fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhRevisionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhRevisionAl() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhRevisionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND CP.fhAsignacion = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhRevisionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getFhAlertaDel() != null || parametrosBusquedaCasoDTO.getFhAlertaAl() != null ){
				 if(parametrosBusquedaCasoDTO.getFhAlertaDel() != null && parametrosBusquedaCasoDTO.getFhAlertaAl() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhAlerta BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND CP.fhAlerta BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhAlertaDel() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND CP.fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhAlertaAl() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND CP.fhAlerta = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhAlertaAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			
			 if(parametrosBusquedaCasoDTO.getFhEnvioDel() != null || parametrosBusquedaCasoDTO.getFhEnvioAl() != null ){
				 if(parametrosBusquedaCasoDTO.getFhEnvioDel() != null && parametrosBusquedaCasoDTO.getFhEnvioAl() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhEnvio BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND CP.fhEnvio BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhEnvioDel() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND CP.fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhEnvioAl() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND CP.fhEnvio = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhEnvioAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getFhVencRevisionDel() != null || parametrosBusquedaCasoDTO.getFhVencRevisionAl() != null ){
				 if(parametrosBusquedaCasoDTO.getFhVencRevisionDel() != null && parametrosBusquedaCasoDTO.getFhVencRevisionAl() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhVenRev BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND CP.fhVenRev BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhVencRevisionDel() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND CP.fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhVencRevisionAl() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND CP.fhVenRev = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencRevisionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getFhVencLiberacionDel() != null || parametrosBusquedaCasoDTO.getFhVencLiberacionAl() != null ){
				 if(parametrosBusquedaCasoDTO.getFhVencLiberacionDel() != null && parametrosBusquedaCasoDTO.getFhVencLiberacionAl() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhVenLibSia BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND CP.fhVenLibSia BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhVencLiberacionDel() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND CP.fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhVencLiberacionAl() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND CP.fhVenLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencLiberacionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			  
			 if(parametrosBusquedaCasoDTO.getFhLiberacionDel() != null || parametrosBusquedaCasoDTO.getFhLiberacionAl() != null ){
				 if(parametrosBusquedaCasoDTO.getFhLiberacionDel() != null && parametrosBusquedaCasoDTO.getFhLiberacionAl() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhLibSia BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhLiberacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhLiberacionAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND CP.fhLibSia BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhLiberacionDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhLiberacionAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhLiberacionDel() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhLiberacionDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND CP.fhLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhLiberacionDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhLiberacionAl() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhLiberacionAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND CP.fhLibSia = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhLiberacionAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getFhVencReporteDel() != null || parametrosBusquedaCasoDTO.getFhVencReporteAl() != null ){
				 if(parametrosBusquedaCasoDTO.getFhVencReporteDel() != null && parametrosBusquedaCasoDTO.getFhVencReporteAl() != null){
					 if(filtro.equals("")){
						 filtro = filtro + " CP.fhRepAut BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
					 }else{
						 filtro = filtro + " AND CP.fhRepAut BETWEEN " + "TO_DATE('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') " + "AND  TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
					 }
				 }else{
					 if(parametrosBusquedaCasoDTO.getFhVencReporteDel() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro + " AND CP.fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteDel()) +"','DD/MM/YYYY') ";
						 }
					 }
					 if(parametrosBusquedaCasoDTO.getFhVencReporteAl() != null){
						 if(filtro.equals("")){
							 filtro = filtro + " CP.fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
						 }else{
							 filtro = filtro +  " AND CP.fhRepAut = TO_DATE ('"  + formato.format(parametrosBusquedaCasoDTO.getFhVencReporteAl()) +"','DD/MM/YYYY') ";
						 }
					 }
				 }
				 isFiltro = true;
				 
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdEscenario() != null ){
				 if(filtro.equals("")){
					 consulta+=" ,TSCA025_RELALRESC T25 ";
					 filtro = filtro + " (T25.CD_ALERTA = CP.nuFolioAlerta and T25.CD_SISTEMA = CP.cdSistema) AND T25.CD_ESCENARIO = '"+ parametrosBusquedaCasoDTO.getCdEscenario()+"'";
				 }else{
					 consulta+=" ,TSCA025_RELALRESC T25 ";
					 filtro = filtro + " AND (T25.CD_ALERTA = CP.nuFolioAlerta and T25.CD_SISTEMA = CP.cdSistema) AND T25.CD_ESCENARIO = '"+ parametrosBusquedaCasoDTO.getCdEscenario()+"'";
				 }
				 isFiltro = true;
			
			 }
			 
			 if(parametrosBusquedaCasoDTO.getCdRespuesta() != null ){
				 if(filtro.equals("")){
					 filtro = filtro + " CP.cdRespuesta = '"+ parametrosBusquedaCasoDTO.getCdRespuesta()+"'";
				 }else{
					 filtro = filtro + " AND CP.cdRespuesta = '"+ parametrosBusquedaCasoDTO.getCdRespuesta()+"'";
				 }
				 isFiltro = true;
			
			 }

			 if(isFiltro){
				consulta = consulta+condicion+filtro;
			 }
			
		 }
		 Query query = em.createNativeQuery(consulta);
		 query.setParameter(1,cdSupervisor);

		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("cdCaso")
			.addScalar("nuFolioAlerta")
		    .addScalar("nuCliente")
            .addScalar("nbCliente")
            .addScalar("nuCuenta")
            .addScalar("fhEnvio",TimestampType.INSTANCE)
            .addScalar("fhAlerta",TimestampType.INSTANCE)
            .addScalar("cdDivisa")
            .addScalar("cdSistema")
            .addScalar("nbSistema")
            .addScalar("cdTipologia")
            .addScalar("nbSegmento")
            .addScalar("fhAsignacion",TimestampType.INSTANCE)
            .addScalar("nbPrioriAsig")
            .addScalar("nbDictamenP")
            .addScalar("fhVenRev",TimestampType.INSTANCE)
            .addScalar("fhVenLibSia",TimestampType.INSTANCE)
            .addScalar("fhLibSia",TimestampType.INSTANCE)
            .addScalar("fhRepAut",TimestampType.INSTANCE)
            .addScalar("cdSesion")
            .addScalar("cdTPersona")
            .addScalar("cdSegmento" )
            .addScalar("imAlerta")
            .addScalar("nuScoreMc")
            .addScalar("nuScoreMdl")
            .addScalar("nbConsultor")
            .addScalar("cdRespuesta")
            .addScalar("nbRespuesta")
		 .setResultTransformer(Transformers.aliasToBean(AlertaDTO.class));
				
		 lstAlertasDTO = (List<AlertaDTO>)query.getResultList();
			
		
			return lstAlertasDTO;
		}
	
	@SuppressWarnings("unchecked")
	public List<AlertaDTO> getConsultaCasosDictamenSupervisor(BigDecimal cdSupervisor) throws Exception{
		
		 List<AlertaDTO> lstAlertasDTO = null;
		 Query query = em.createNativeQuery(Consultas.getConsulta("getConsultaCasosDictamenSupervisor"));
		 query.setParameter(1,cdSupervisor);
		
		 query.unwrap(org.hibernate.SQLQuery.class)
			.addScalar("cdCaso")
			.addScalar("nuFolioAlerta")
		    .addScalar("nuCliente")
            .addScalar("nbCliente")
            .addScalar("nuCuenta")
            .addScalar("fhEnvio",TimestampType.INSTANCE)
            .addScalar("fhAlerta",TimestampType.INSTANCE)
            .addScalar("cdDivisa")
            .addScalar("cdSistema")
            .addScalar("nbSistema")
            .addScalar("cdTipologia")
            .addScalar("nbSegmento")
            .addScalar("fhAsignacion",TimestampType.INSTANCE)
            .addScalar("nbPrioriAsig")
            .addScalar("nbDictamenP")
            .addScalar("fhVenRev",TimestampType.INSTANCE)
            .addScalar("fhVenLibSia",TimestampType.INSTANCE)
            .addScalar("fhRepAut",TimestampType.INSTANCE)
            .addScalar("fhLibSia",TimestampType.INSTANCE)
            .addScalar("cdSesion")
            .addScalar("cdTPersona")
            .addScalar("cdSegmento")
            .addScalar("imAlerta")
            .addScalar("nuScoreMc")
            .addScalar("nuScoreMdl")
            .addScalar("nbConsultor")
            .addScalar("cdRespuesta")
            .addScalar("nbRespuesta")
		 .setResultTransformer(Transformers.aliasToBean(AlertaDTO.class));
				
		 lstAlertasDTO = (List<AlertaDTO>)query.getResultList();
			
		
			return lstAlertasDTO;
		}
	
	@SuppressWarnings("unchecked")
	public Integer getNuCasosDictamenPre(BigDecimal cdSupervisor) throws Exception{
		 Integer nuCasosDictamenPre = 0;
		 List<Integer> resultado = new ArrayList<Integer>();
		 
		 String COUNT = " SELECT COUNT(*) FROM ( "+Consultas.getConsulta("getConsultaCasosDictamenSupervisor")+" ) WHERE cdDictamenP IS NOT NULL ";
		 Query query = em.createNativeQuery(COUNT);
		 query.setParameter(1,cdSupervisor);
		 
		 resultado =(List<Integer>) query.getResultList();
		 if( resultado != null && !resultado.isEmpty())
		 {
			 nuCasosDictamenPre = resultado.get( 0 );	 
		 }
		return nuCasosDictamenPre;
	}
	
	
}
