package com.bbva.msca.back.dao;


import java.util.List;
import com.bbva.msca.back.dto.DatosGeneralesClienteReportadoDTO;
import com.bbva.msca.back.util.BdSac;
import com.bbva.msca.back.util.Consultas;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import org.hibernate.transform.Transformers;
import org.hibernate.type.StringType;
import org.hibernate.type.TimestampType;


public class DatosGeneralesClienteReportadoDAO {
	
	BdSac bdSac = new BdSac();
	private EntityManager em = bdSac.getEntityManager();

	@SuppressWarnings({ "unchecked" })
	public DatosGeneralesClienteReportadoDTO getDatosGeneralesClienteReportado(String cdSistema, String folioAlerta) throws Exception{

		DatosGeneralesClienteReportadoDTO datosGeneralesCRDTO = new DatosGeneralesClienteReportadoDTO();
		
			 
			 Query queryCR = em.createNativeQuery(Consultas.getConsulta("getDatosGeneralesClienteReportado"));
			 
			 queryCR.setParameter(1,cdSistema);
			 queryCR.setParameter(2,folioAlerta);
			 
			 queryCR.unwrap(org.hibernate.SQLQuery.class)
			 
				.addScalar("nbClienteCR")
				.addScalar("cdClienteCR",StringType.INSTANCE)
				.addScalar("nbCiudadCR")
				.addScalar("nbDireccionCR")
				.addScalar("cdRfcCR")
				.addScalar("nbTelParCR")
				.addScalar("nbTelOfiCR")
				.addScalar("nbNacCR")
				.addScalar("fhAltaCR", TimestampType.INSTANCE)
				.addScalar("cdSectorCR",StringType.INSTANCE)
				.addScalar("fhAntNegoCR",StringType.INSTANCE)//mes dias a�os
				.addScalar("nbActividadBanxicoCR")
				.addScalar("nbCurpCR")
				//.addScalar("nbPaisNacimientoCR")
				.addScalar("nbPaisRecidenciaCR")
				
				.addScalar("fhAperturaCR", TimestampType.INSTANCE)
				//.addScalar("fhUltimaCR", TimestampType.INSTANCE)
				.addScalar("tipoFuenteCR")
				.addScalar("clasifCteCR",StringType.INSTANCE) //char
				//.addScalar("nbOficinaCR")
				.addScalar("nbRiesgoCR")
				
			.setResultTransformer(Transformers.aliasToBean(DatosGeneralesClienteReportadoDTO.class));
			 
			 List<DatosGeneralesClienteReportadoDTO> lstDatosGeneralesCRDTO = (List<DatosGeneralesClienteReportadoDTO>)queryCR.getResultList();
			
			 if(!lstDatosGeneralesCRDTO.isEmpty()){
				 datosGeneralesCRDTO = lstDatosGeneralesCRDTO.get( 0 ); 
			 }
			 
		return datosGeneralesCRDTO;
	}
	
	@SuppressWarnings({ "unchecked" })
	public DatosGeneralesClienteReportadoDTO getDatosGeneralesClienteReportado2(String cdSistema, String folioAlerta) throws Exception{
		DatosGeneralesClienteReportadoDTO datosGeneralesCRDTO = new DatosGeneralesClienteReportadoDTO();
			 
			 Query query = em.createNativeQuery(Consultas.getConsulta("getDatosGeneralesClienteReportado2"));
			 query.setParameter(1,cdSistema);
			 query.setParameter(2,folioAlerta);
			 
			 query.unwrap(org.hibernate.SQLQuery.class)
				.addScalar("nbClienteCR2")
				.addScalar("cdClienteCR2",StringType.INSTANCE)
				.addScalar("nbCiudadCR2")
				.addScalar("nbDireccionCR2")
				.addScalar("cdRfcCR2")
				.addScalar("nbTelParCR2")
				.addScalar("nbTelOfiCR2")
				.addScalar("nbNacCR2")
				.addScalar("fhAltaCR2", TimestampType.INSTANCE)
				.addScalar("cdSectorCR2",StringType.INSTANCE)
				.addScalar("fhAntNegoCR2",StringType.INSTANCE)
				.addScalar("nbActividadBanxicoCR2")
				.addScalar("nbCurpCR2")
				//.addScalar("nbPaisNacimientoCR2")
				.addScalar("nbPaisRecidenciaCR2")
				.addScalar("fhAperturaCR2", TimestampType.INSTANCE)
				//.addScalar("fhUltimaCR2", TimestampType.INSTANCE)
				.addScalar("tipoFuenteCR2")
				.addScalar("clasifCteCR2",StringType.INSTANCE)
				//.addScalar("nbOficinaCR2")
				.addScalar("nbRiesgoCR2")
				
			.setResultTransformer(Transformers.aliasToBean(DatosGeneralesClienteReportadoDTO.class));
			 List<DatosGeneralesClienteReportadoDTO> lstDatosGeneralesCRDTO = (List<DatosGeneralesClienteReportadoDTO>)query.getResultList();
			 
			 if(!lstDatosGeneralesCRDTO.isEmpty()){
				 datosGeneralesCRDTO = lstDatosGeneralesCRDTO.get(0);
			 }
		return datosGeneralesCRDTO;
	}
	
	
	
	
}
