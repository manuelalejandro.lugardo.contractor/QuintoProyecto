package com.bbva.msca.back.srv;

import com.bbva.msca.back.util.ResponseGeneric;

import java.util.List;

import com.bbva.msca.back.dao.AlertaDAO;
import com.bbva.msca.back.dao.AlertasPreviasDAO;
import com.bbva.msca.back.dto.AlertasPreviasDTO;
import com.bbva.msca.back.util.MensajesI18n;

public class AlertasServiceImpl implements AlertasService {

	@Override
	public ResponseGeneric crearAlerta(Object obj) throws Exception {
		ResponseGeneric response = new ResponseGeneric();
		return response;
	}

	@Override
	public ResponseGeneric modificarAlerta(Object obj) throws Exception {
		ResponseGeneric response = new ResponseGeneric();
		return response;
	}

	@Override
	public ResponseGeneric eliminarAlerta(Object obj) throws Exception {
		ResponseGeneric response = new ResponseGeneric();
		return response;
	}

	@Override
	public ResponseGeneric consultarAlerta(String cdAlerta, String cdSistema){
		ResponseGeneric response = new ResponseGeneric();
		try{
			AlertaDAO alertaDAO = new AlertaDAO();
			response.setResponse(alertaDAO.getAlerta(cdAlerta, cdSistema));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
	
	@Override
	public ResponseGeneric consultarAlertaDetalle(String cdAlerta, String cdSistema){
		ResponseGeneric response = new ResponseGeneric();
		try{
			AlertaDAO alertaDAO = new AlertaDAO();
			response.setResponse(alertaDAO.getAlertaEscenarioTipologia(cdAlerta, cdSistema));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}

	@Override
	public ResponseGeneric consultarAlertas(Object obj) throws Exception {
		ResponseGeneric response = new ResponseGeneric();
		return response;
	}
	
	
	//Implementacion de Alertas Previas de SIA y SICA 
	@Override
	public ResponseGeneric consultarAlertasPrevias(String cdCliente) {
		ResponseGeneric response = new ResponseGeneric();
		AlertasPreviasDAO alertasPreviasDAO = new AlertasPreviasDAO();
		
		try{
		List<AlertasPreviasDTO> lstAlertasPreviasDTO = alertasPreviasDAO.getAlertasPrevias(cdCliente);
		response.setResponse(lstAlertasPreviasDTO);
		response.setCdMensaje("1");
		response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		
		}catch(Exception e){
			e.printStackTrace();
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
		}
		return response;
	}

	@Override
	public ResponseGeneric consultarMatrizCriterioAlerta(String cdAlerta, String cdSistema) {
		ResponseGeneric response = new ResponseGeneric();
		try{
			AlertaDAO alertaDAO = new AlertaDAO();
			response.setResponse(alertaDAO.getMatrizCriterioAlerta(cdAlerta, cdSistema));
			response.setCdMensaje("1");
			response.setNbMensaje(MensajesI18n.getText("MSCA_OK"));
		}catch(Exception e){
			response.setCdMensaje("0");
			response.setNbMensaje(MensajesI18n.getText("MSCA_ER"));
			e.printStackTrace();
		}
		return response;
	}
}
