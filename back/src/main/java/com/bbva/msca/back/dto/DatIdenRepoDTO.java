package com.bbva.msca.back.dto;

import java.sql.Date;

public class DatIdenRepoDTO {
		
		private String cdSistema;//char
		private String nuCuenta;//char
		private String montoDivisa;
		private Date fhAlerta;
		private Date fhEnvio;
		private String cdSesion;
		private String oficinaGestora;
		private String cr;//char
		private String mercado;
		private String division;
		private String funcionario;
		
		public String getCdSistema() {
			return cdSistema;
		}
		public void setCdSistema(String cdSistema) {
			this.cdSistema = cdSistema;
		}
		//
		public String getNuCuenta() {
			return nuCuenta;
		}
		public void setNuCuenta(String nuCuenta) {
			this.nuCuenta = nuCuenta;
		}
		//
		public String getMontoDivisa() {
			return montoDivisa;
		}
		public void setMontoDivisa(String montoDivisa) {
			this.montoDivisa = montoDivisa;
		}
		//
		public Date getFhAlerta() {
			return fhAlerta;
		}
		public void setFhAlerta(Date fhAlerta) {
			this.fhAlerta = fhAlerta;
		}
		//
		public Date getFhEnvio() {
			return fhEnvio;
		}
		public void setFhEnvio(Date fhEnvio) {
			this.fhEnvio = fhEnvio;
		}
		//
		public String getCdSesion() {
			return cdSesion;
		}
		public void setCdSesion(String cdSesion) {
			this.cdSesion = cdSesion;
		}
		//
		public String getOficinaGestora() {
			return oficinaGestora;
		}
		public void setOficinaGestora(String oficinaGestora) {
			this.oficinaGestora = oficinaGestora;
		}
		public void setCr(String cr) {
			this.cr = cr;
		}
		public String getCr() {
			return cr;
		}

		public String getMercado() {
			return mercado;
		}
		public void setMercado(String mercado) {
			this.mercado = mercado;
		}
		public void setDivision(String division) {
			this.division = division;
		}
		public String getDivision() {
			return division;
		}
		public void setFuncionario(String funcionario) {
			this.funcionario = funcionario;
		}
		public String getFuncionario() {
			return funcionario;
		}	
	}





