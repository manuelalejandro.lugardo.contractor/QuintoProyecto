package com.bbva.msca.back.servicios.host.pee5;

import org.apache.commons.logging.Log;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.jee.arq.spring.core.filtro.InicioSesionMock;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionRespuestaHost;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionTransporte;

/**
 * Test de la transacci&oacute;n <code>PEE5</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/applicationContext.xml")
public class TestTransaccionPee5 {
	
	private static final Log LOG = I18nLogFactory.getLog(TestTransaccionPee5.class);
		
	@Autowired
	private TransaccionPee5 transaccion;
	
	@Test
	@InicioSesionMock
	public void test() throws ExcepcionTransaccion {
		
		PeticionTransaccionPee5 peticion = new PeticionTransaccionPee5();		
		FormatoPEM0E5E formatoEntrada = new FormatoPEM0E5E();
		//formatoEntrada.setNumclie( "D0022006" );
		formatoEntrada.setNombre( "Juan" );
		formatoEntrada.setPriape( "Perez" );
		peticion.getCuerpo().getPartes().add( formatoEntrada );
		/*
		 * TODO: poblar los campos de beanPeticion con valores adecuados
		 */
		
		try {
			LOG.info("Invocando transaccion, peticion: " + peticion);
			RespuestaTransaccionPee5 respuesta = transaccion.invocar(peticion);
			LOG.info("Recibida respuesta: " + respuesta);
		} catch ( ExcepcionRespuestaHost e ) {
			LOG.error("Error recibido desde host, codigoError: " + e.getCodigoError() + ", descripcion: " + e.getMessage());
		} catch ( ExcepcionTransporte e ) {
			LOG.error("Error de transporte", e);
		}
	}
}