package com.bbva.msca.back.servicios.host.peh1;

import org.apache.commons.logging.Log;
import com.bbva.jee.arq.spring.core.log.I18nLogFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bbva.jee.arq.spring.core.filtro.InicioSesionMock;
import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionRespuestaHost;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionTransporte;

/**
 * Test de la transacci&oacute;n <code>PEH1</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:META-INF/spring/applicationContext.xml")
public class TestTransaccionPeh1 {
	
	private static final Log LOG = I18nLogFactory.getLog(TestTransaccionPeh1.class);
		
	@Autowired
	private TransaccionPeh1 transaccion;
	
	@Test
	@InicioSesionMock
	public void test() throws ExcepcionTransaccion {
		
		PeticionTransaccionPeh1 peticion = new PeticionTransaccionPeh1();		
		
		/*
		 * TODO: poblar los campos de beanPeticion con valores adecuados
		 */
		
		try {
			LOG.info("Invocando transaccion, peticion: " + peticion);
			RespuestaTransaccionPeh1 respuesta = transaccion.invocar(peticion);
			LOG.info("Recibida respuesta: " + respuesta);
		} catch ( ExcepcionRespuestaHost e ) {
			LOG.error("Error recibido desde host, codigoError: " + e.getCodigoError() + ", descripcion: " + e.getMessage());
		} catch ( ExcepcionTransporte e ) {
			LOG.error("Error de transporte", e);
		}
	}
}